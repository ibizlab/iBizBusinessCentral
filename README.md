在开源运动的洪流中，中国从没有自己的高水平ERP开源软件，要么是业务深度不够，要么没有具备高度可定制性的技术支撑能力；iBiz希望能够做出中国自己的既具备足够业务覆盖，又拥有强大定制技术支撑的开源商业套件，服务于中小企业并开放给广大个人开发者和实施商，为中国乃至世界的开源事业做出自己的贡献，为中国数字经济的发展和企业数字化转型贡献自己的力量。

iBiz商业套件将是一个站在巨人肩膀上的产品，会吸收业界先进产品（微软Dynamics 365 、 Odoo 等）业务模型，保证业务的成熟可用；iBiz商业套件将拥有强大的技术支撑Paas平台，为定制实施和应用落地提供效率支撑；iBiz将始终秉承开源理念，立志成为中国的Odoo、更希望成为世界的iBiz。

iBiz商业套件全面采取中台模式、SpringBoot+VUE前后台分离架构、MDD/MDA全方位建模技术，致力于提供高可用度、全业务覆盖、高可定制性的重度开源项目。

本次开源的iBizBusinessCentral是商业套件管理企业各应用的大主板，目前已集成了HumanResources（人力资源）、AssetManagement（资产管理）、CustomerRelationshipManagement（客户关系管理）等应用，后续iBiz仍将陆续发布Commerce（电子商务）、 ProjectOperations（项目运行）、 UniversalResourceScheduling（资源管理）、 FieldService（现场服务）、 CustomerService（客户服务）等应用来满足企业全方位的管理需求，请大家持续关注！！！

# 系统地址

演示Demo地址：http://central.ebs.ibizlab.cn

演示账号/密码：ibzadmin/123456

配置平台: http://mos.ibizlab.cn/mos/#/common_mosindex/srfkeys=75356053-28BD-4651-B2C8-C228F6F9B588/sysdesign_pssystemmaintabexpview

**建模平台内测申请：[iBiz建模平台内测申请通道](https://gitee.com/ibizlab/iBizEHR/wikis/%E5%BB%BA%E6%A8%A1%E5%B9%B3%E5%8F%B0%E5%86%85%E6%B5%8B%E9%A1%BB%E7%9F%A5?sort_id=2992220)**

欢迎加入开源商业套件交流QQ群：1056401976

# 业务蓝图

![输入图片说明](https://images.gitee.com/uploads/images/2020/0903/110158_9945791a_7582525.png "iBiz业务蓝图.png")

iBiz开源商业套件是为中国中小型企业量身定制的企业管理解决方案，底层企业中心作为各业务应用集成的大主板并管理套件全部主数据，企业中心结合各运行支撑模块构成了整个套件的平台层；中间的应用层是根据企业各类管理业务需求组织成的套件标准业务应用，用户可根据自己的需要选择，目前已上线的为“客户关系管理” 、“人力资源”、“资产管理”等应用，今后更将陆续上线其他的业务应用模块；最上层的门户层包括为企业自身以及企业的客户、供应商准备的适配各类场景的客户端，以及用于数据通讯的API接口。

欢迎加入开源商业套件交流QQ群：1056401976

# 技术框架
**后台技术模板[iBiz4j Spring R7](http://demo.ibizlab.cn/ibizr7sfstdtempl/ibiz4jr7)**
* 核心框架：Spring Boot
* 持久层框架: Mybatis-plus
* 服务发现：Nacos
* 日志管理：Logback
* 项目管理框架: Maven

**前端技术模板[iBiz-Vue-Studio](https://gitee.com/ibizr7pfstdtempl/iBiz-Vue-Studio)**
* 前端MVVM框架：vue.js 2.6.10
* 路由：vue-router 3.1.3
* 状态管理：vue-router 3.1.3
* 国际化：vue-i18n 8.15.3
* 数据交互：axios 0.19.1
* UI框架：element-ui 2.13.0, view-design 4.1.0
* 工具库：qs, path-to-regexp, rxjs
* 图标库：font-awesome 4.7.0
* 引入组件： tinymce 4.8.5
* 代码风格检测：eslint


# 开发环境
* JDK
* Maven
* Node.js
* Yarn
* Vue Cli

# 开源说明
* 本系统100%开源，遵守MulanPSL-2.0协议

# 模型设计
* ER图设计
![输入图片说明](https://images.gitee.com/uploads/images/2020/0728/113603_6c964662_7582525.png "ER图.png")
* 故事板
![输入图片说明](https://images.gitee.com/uploads/images/2020/0728/113701_67faf125_7582525.png "故事板.png")
* 图表设计
![输入图片说明](https://images.gitee.com/uploads/images/2020/0728/115529_a0f4a6a9_7582525.png "图表设计.png")

# 系统美图

* 仪表盘
![输入图片说明](https://images.gitee.com/uploads/images/2020/0728/115727_00340650_7582525.png "仪表盘.png")

* 客户列表
![输入图片说明](https://images.gitee.com/uploads/images/2020/0728/115831_525b6986_7582525.png "客户列表.png")

* 客户主信息
![输入图片说明](https://images.gitee.com/uploads/images/2020/0728/115928_6e9618eb_7582525.png "客户主信息.png")


