package cn.ibizlab.businesscentral.core.asset.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.asset.domain.Asset;
import cn.ibizlab.businesscentral.core.asset.filter.AssetSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Asset] 服务对象接口
 */
public interface IAssetService extends IService<Asset>{

    boolean create(Asset et) ;
    void createBatch(List<Asset> list) ;
    boolean update(Asset et) ;
    void updateBatch(List<Asset> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    Asset get(String key) ;
    Asset getDraft(Asset et) ;
    boolean checkKey(Asset et) ;
    boolean save(Asset et) ;
    void saveBatch(List<Asset> list) ;
    Page<Asset> searchDefault(AssetSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Asset> getAssetByIds(List<String> ids) ;
    List<Asset> getAssetByEntities(List<Asset> entities) ;
}


