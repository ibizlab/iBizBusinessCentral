package cn.ibizlab.businesscentral.core.humanresource.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.humanresource.domain.Employee;
/**
 * 关系型数据实体[Employee] 查询条件对象
 */
@Slf4j
@Data
public class EmployeeSearchContext extends QueryWrapperContext<Employee> {

	private String n_employeename_like;//[员工名称]
	public void setN_employeename_like(String n_employeename_like) {
        this.n_employeename_like = n_employeename_like;
        if(!ObjectUtils.isEmpty(this.n_employeename_like)){
            this.getSearchCond().like("employeename", n_employeename_like);
        }
    }
	private String n_organizationid_eq;//[组织]
	public void setN_organizationid_eq(String n_organizationid_eq) {
        this.n_organizationid_eq = n_organizationid_eq;
        if(!ObjectUtils.isEmpty(this.n_organizationid_eq)){
            this.getSearchCond().eq("organizationid", n_organizationid_eq);
        }
    }
	private String n_bloodtype_eq;//[血型]
	public void setN_bloodtype_eq(String n_bloodtype_eq) {
        this.n_bloodtype_eq = n_bloodtype_eq;
        if(!ObjectUtils.isEmpty(this.n_bloodtype_eq)){
            this.getSearchCond().eq("bloodtype", n_bloodtype_eq);
        }
    }
	private String n_nation_eq;//[民族]
	public void setN_nation_eq(String n_nation_eq) {
        this.n_nation_eq = n_nation_eq;
        if(!ObjectUtils.isEmpty(this.n_nation_eq)){
            this.getSearchCond().eq("nation", n_nation_eq);
        }
    }
	private String n_certtype_eq;//[证件类型]
	public void setN_certtype_eq(String n_certtype_eq) {
        this.n_certtype_eq = n_certtype_eq;
        if(!ObjectUtils.isEmpty(this.n_certtype_eq)){
            this.getSearchCond().eq("certtype", n_certtype_eq);
        }
    }
	private String n_sex_eq;//[性别]
	public void setN_sex_eq(String n_sex_eq) {
        this.n_sex_eq = n_sex_eq;
        if(!ObjectUtils.isEmpty(this.n_sex_eq)){
            this.getSearchCond().eq("sex", n_sex_eq);
        }
    }
	private String n_marriage_eq;//[婚姻状况]
	public void setN_marriage_eq(String n_marriage_eq) {
        this.n_marriage_eq = n_marriage_eq;
        if(!ObjectUtils.isEmpty(this.n_marriage_eq)){
            this.getSearchCond().eq("marriage", n_marriage_eq);
        }
    }
	private String n_nativetype_eq;//[户口类型]
	public void setN_nativetype_eq(String n_nativetype_eq) {
        this.n_nativetype_eq = n_nativetype_eq;
        if(!ObjectUtils.isEmpty(this.n_nativetype_eq)){
            this.getSearchCond().eq("nativetype", n_nativetype_eq);
        }
    }
	private String n_political_eq;//[政治面貌]
	public void setN_political_eq(String n_political_eq) {
        this.n_political_eq = n_political_eq;
        if(!ObjectUtils.isEmpty(this.n_political_eq)){
            this.getSearchCond().eq("political", n_political_eq);
        }
    }
	private String n_organizationname_eq;//[组织]
	public void setN_organizationname_eq(String n_organizationname_eq) {
        this.n_organizationname_eq = n_organizationname_eq;
        if(!ObjectUtils.isEmpty(this.n_organizationname_eq)){
            this.getSearchCond().eq("organizationname", n_organizationname_eq);
        }
    }
	private String n_organizationname_like;//[组织]
	public void setN_organizationname_like(String n_organizationname_like) {
        this.n_organizationname_like = n_organizationname_like;
        if(!ObjectUtils.isEmpty(this.n_organizationname_like)){
            this.getSearchCond().like("organizationname", n_organizationname_like);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("employeename", query)   
            );
		 }
	}
}



