package cn.ibizlab.businesscentral.core.runtime.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.runtime.domain.BulkDeleteOperation;
/**
 * 关系型数据实体[BulkDeleteOperation] 查询条件对象
 */
@Slf4j
@Data
public class BulkDeleteOperationSearchContext extends QueryWrapperContext<BulkDeleteOperation> {

	private Integer n_statuscode_eq;//[状态描述]
	public void setN_statuscode_eq(Integer n_statuscode_eq) {
        this.n_statuscode_eq = n_statuscode_eq;
        if(!ObjectUtils.isEmpty(this.n_statuscode_eq)){
            this.getSearchCond().eq("statuscode", n_statuscode_eq);
        }
    }
	private Integer n_statecode_eq;//[状态]
	public void setN_statecode_eq(Integer n_statecode_eq) {
        this.n_statecode_eq = n_statecode_eq;
        if(!ObjectUtils.isEmpty(this.n_statecode_eq)){
            this.getSearchCond().eq("statecode", n_statecode_eq);
        }
    }
	private String n_bulkdeleteoperationname_like;//[批量删除操作名称]
	public void setN_bulkdeleteoperationname_like(String n_bulkdeleteoperationname_like) {
        this.n_bulkdeleteoperationname_like = n_bulkdeleteoperationname_like;
        if(!ObjectUtils.isEmpty(this.n_bulkdeleteoperationname_like)){
            this.getSearchCond().like("bulkdeleteoperationname", n_bulkdeleteoperationname_like);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("bulkdeleteoperationname", query)   
            );
		 }
	}
}



