package cn.ibizlab.businesscentral.core.scheduling.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.scheduling.domain.BookableResourceBooking;
/**
 * 关系型数据实体[BookableResourceBooking] 查询条件对象
 */
@Slf4j
@Data
public class BookableResourceBookingSearchContext extends QueryWrapperContext<BookableResourceBooking> {

	private String n_bookableresbookingname_like;//[可预订的资源预订名称]
	public void setN_bookableresbookingname_like(String n_bookableresbookingname_like) {
        this.n_bookableresbookingname_like = n_bookableresbookingname_like;
        if(!ObjectUtils.isEmpty(this.n_bookableresbookingname_like)){
            this.getSearchCond().like("bookableresbookingname", n_bookableresbookingname_like);
        }
    }
	private Integer n_statecode_eq;//[状态]
	public void setN_statecode_eq(Integer n_statecode_eq) {
        this.n_statecode_eq = n_statecode_eq;
        if(!ObjectUtils.isEmpty(this.n_statecode_eq)){
            this.getSearchCond().eq("statecode", n_statecode_eq);
        }
    }
	private Integer n_statuscode_eq;//[状态描述]
	public void setN_statuscode_eq(Integer n_statuscode_eq) {
        this.n_statuscode_eq = n_statuscode_eq;
        if(!ObjectUtils.isEmpty(this.n_statuscode_eq)){
            this.getSearchCond().eq("statuscode", n_statuscode_eq);
        }
    }
	private String n_bookingtype_eq;//[预订类型]
	public void setN_bookingtype_eq(String n_bookingtype_eq) {
        this.n_bookingtype_eq = n_bookingtype_eq;
        if(!ObjectUtils.isEmpty(this.n_bookingtype_eq)){
            this.getSearchCond().eq("bookingtype", n_bookingtype_eq);
        }
    }
	private String n_bookingstatus_eq;//[预订状态]
	public void setN_bookingstatus_eq(String n_bookingstatus_eq) {
        this.n_bookingstatus_eq = n_bookingstatus_eq;
        if(!ObjectUtils.isEmpty(this.n_bookingstatus_eq)){
            this.getSearchCond().eq("bookingstatus", n_bookingstatus_eq);
        }
    }
	private String n_resource_eq;//[资源]
	public void setN_resource_eq(String n_resource_eq) {
        this.n_resource_eq = n_resource_eq;
        if(!ObjectUtils.isEmpty(this.n_resource_eq)){
            this.getSearchCond().eq("resource", n_resource_eq);
        }
    }
	private String n_header_eq;//[页眉]
	public void setN_header_eq(String n_header_eq) {
        this.n_header_eq = n_header_eq;
        if(!ObjectUtils.isEmpty(this.n_header_eq)){
            this.getSearchCond().eq("header", n_header_eq);
        }
    }
	private String n_transactioncurrencyid_eq;//[货币]
	public void setN_transactioncurrencyid_eq(String n_transactioncurrencyid_eq) {
        this.n_transactioncurrencyid_eq = n_transactioncurrencyid_eq;
        if(!ObjectUtils.isEmpty(this.n_transactioncurrencyid_eq)){
            this.getSearchCond().eq("transactioncurrencyid", n_transactioncurrencyid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("bookableresbookingname", query)   
            );
		 }
	}
}



