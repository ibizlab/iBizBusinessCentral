package cn.ibizlab.businesscentral.core.service.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.service.domain.ResourceGroup;
/**
 * 关系型数据实体[ResourceGroup] 查询条件对象
 */
@Slf4j
@Data
public class ResourceGroupSearchContext extends QueryWrapperContext<ResourceGroup> {

	private String n_grouptypecode_eq;//[组类型代码]
	public void setN_grouptypecode_eq(String n_grouptypecode_eq) {
        this.n_grouptypecode_eq = n_grouptypecode_eq;
        if(!ObjectUtils.isEmpty(this.n_grouptypecode_eq)){
            this.getSearchCond().eq("grouptypecode", n_grouptypecode_eq);
        }
    }
	private String n_resourcegroupname_like;//[资源组名称]
	public void setN_resourcegroupname_like(String n_resourcegroupname_like) {
        this.n_resourcegroupname_like = n_resourcegroupname_like;
        if(!ObjectUtils.isEmpty(this.n_resourcegroupname_like)){
            this.getSearchCond().like("resourcegroupname", n_resourcegroupname_like);
        }
    }
	private String n_businessunitid_eq;//[Business Unit Id]
	public void setN_businessunitid_eq(String n_businessunitid_eq) {
        this.n_businessunitid_eq = n_businessunitid_eq;
        if(!ObjectUtils.isEmpty(this.n_businessunitid_eq)){
            this.getSearchCond().eq("businessunitid", n_businessunitid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("resourcegroupname", query)   
            );
		 }
	}
}



