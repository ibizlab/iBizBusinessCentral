package cn.ibizlab.businesscentral.core.stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.stock.domain.Inventory;
import cn.ibizlab.businesscentral.core.stock.filter.InventorySearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Inventory] 服务对象接口
 */
public interface IInventoryService extends IService<Inventory>{

    boolean create(Inventory et) ;
    void createBatch(List<Inventory> list) ;
    boolean update(Inventory et) ;
    void updateBatch(List<Inventory> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    Inventory get(String key) ;
    Inventory getDraft(Inventory et) ;
    boolean checkKey(Inventory et) ;
    boolean save(Inventory et) ;
    void saveBatch(List<Inventory> list) ;
    Page<Inventory> searchDefault(InventorySearchContext context) ;
    List<Inventory> selectByProductid(String productid) ;
    void removeByProductid(String productid) ;
    List<Inventory> selectByUomid(String uomid) ;
    void removeByUomid(String uomid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Inventory> getInventoryByIds(List<String> ids) ;
    List<Inventory> getInventoryByEntities(List<Inventory> entities) ;
}


