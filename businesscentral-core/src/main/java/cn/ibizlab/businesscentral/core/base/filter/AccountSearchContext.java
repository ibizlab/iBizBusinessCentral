package cn.ibizlab.businesscentral.core.base.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.base.domain.Account;
/**
 * 关系型数据实体[Account] 查询条件对象
 */
@Slf4j
@Data
public class AccountSearchContext extends QueryWrapperContext<Account> {

	private String n_address2_freighttermscode_eq;//[地址 2: 货运条款]
	public void setN_address2_freighttermscode_eq(String n_address2_freighttermscode_eq) {
        this.n_address2_freighttermscode_eq = n_address2_freighttermscode_eq;
        if(!ObjectUtils.isEmpty(this.n_address2_freighttermscode_eq)){
            this.getSearchCond().eq("address2_freighttermscode", n_address2_freighttermscode_eq);
        }
    }
	private String n_accountratingcode_eq;//[客户等级]
	public void setN_accountratingcode_eq(String n_accountratingcode_eq) {
        this.n_accountratingcode_eq = n_accountratingcode_eq;
        if(!ObjectUtils.isEmpty(this.n_accountratingcode_eq)){
            this.getSearchCond().eq("accountratingcode", n_accountratingcode_eq);
        }
    }
	private String n_customertypecode_eq;//[关系类型]
	public void setN_customertypecode_eq(String n_customertypecode_eq) {
        this.n_customertypecode_eq = n_customertypecode_eq;
        if(!ObjectUtils.isEmpty(this.n_customertypecode_eq)){
            this.getSearchCond().eq("customertypecode", n_customertypecode_eq);
        }
    }
	private String n_accountname_like;//[客户名称]
	public void setN_accountname_like(String n_accountname_like) {
        this.n_accountname_like = n_accountname_like;
        if(!ObjectUtils.isEmpty(this.n_accountname_like)){
            this.getSearchCond().like("accountname", n_accountname_like);
        }
    }
	private String n_preferredcontactmethodcode_eq;//[首选联系方式]
	public void setN_preferredcontactmethodcode_eq(String n_preferredcontactmethodcode_eq) {
        this.n_preferredcontactmethodcode_eq = n_preferredcontactmethodcode_eq;
        if(!ObjectUtils.isEmpty(this.n_preferredcontactmethodcode_eq)){
            this.getSearchCond().eq("preferredcontactmethodcode", n_preferredcontactmethodcode_eq);
        }
    }
	private String n_preferredappointmentdaycode_eq;//[首选日]
	public void setN_preferredappointmentdaycode_eq(String n_preferredappointmentdaycode_eq) {
        this.n_preferredappointmentdaycode_eq = n_preferredappointmentdaycode_eq;
        if(!ObjectUtils.isEmpty(this.n_preferredappointmentdaycode_eq)){
            this.getSearchCond().eq("preferredappointmentdaycode", n_preferredappointmentdaycode_eq);
        }
    }
	private String n_businesstypecode_eq;//[商业类型]
	public void setN_businesstypecode_eq(String n_businesstypecode_eq) {
        this.n_businesstypecode_eq = n_businesstypecode_eq;
        if(!ObjectUtils.isEmpty(this.n_businesstypecode_eq)){
            this.getSearchCond().eq("businesstypecode", n_businesstypecode_eq);
        }
    }
	private String n_ownershipcode_eq;//[所有权]
	public void setN_ownershipcode_eq(String n_ownershipcode_eq) {
        this.n_ownershipcode_eq = n_ownershipcode_eq;
        if(!ObjectUtils.isEmpty(this.n_ownershipcode_eq)){
            this.getSearchCond().eq("ownershipcode", n_ownershipcode_eq);
        }
    }
	private String n_address1_freighttermscode_eq;//[地址 1: 货运条款]
	public void setN_address1_freighttermscode_eq(String n_address1_freighttermscode_eq) {
        this.n_address1_freighttermscode_eq = n_address1_freighttermscode_eq;
        if(!ObjectUtils.isEmpty(this.n_address1_freighttermscode_eq)){
            this.getSearchCond().eq("address1_freighttermscode", n_address1_freighttermscode_eq);
        }
    }
	private String n_address1_shippingmethodcode_eq;//[地址 1: 送货方式]
	public void setN_address1_shippingmethodcode_eq(String n_address1_shippingmethodcode_eq) {
        this.n_address1_shippingmethodcode_eq = n_address1_shippingmethodcode_eq;
        if(!ObjectUtils.isEmpty(this.n_address1_shippingmethodcode_eq)){
            this.getSearchCond().eq("address1_shippingmethodcode", n_address1_shippingmethodcode_eq);
        }
    }
	private Integer n_statuscode_eq;//[状态描述]
	public void setN_statuscode_eq(Integer n_statuscode_eq) {
        this.n_statuscode_eq = n_statuscode_eq;
        if(!ObjectUtils.isEmpty(this.n_statuscode_eq)){
            this.getSearchCond().eq("statuscode", n_statuscode_eq);
        }
    }
	private String n_shippingmethodcode_eq;//[送货方式]
	public void setN_shippingmethodcode_eq(String n_shippingmethodcode_eq) {
        this.n_shippingmethodcode_eq = n_shippingmethodcode_eq;
        if(!ObjectUtils.isEmpty(this.n_shippingmethodcode_eq)){
            this.getSearchCond().eq("shippingmethodcode", n_shippingmethodcode_eq);
        }
    }
	private String n_territorycode_eq;//[区域代码]
	public void setN_territorycode_eq(String n_territorycode_eq) {
        this.n_territorycode_eq = n_territorycode_eq;
        if(!ObjectUtils.isEmpty(this.n_territorycode_eq)){
            this.getSearchCond().eq("territorycode", n_territorycode_eq);
        }
    }
	private String n_industrycode_eq;//[行业]
	public void setN_industrycode_eq(String n_industrycode_eq) {
        this.n_industrycode_eq = n_industrycode_eq;
        if(!ObjectUtils.isEmpty(this.n_industrycode_eq)){
            this.getSearchCond().eq("industrycode", n_industrycode_eq);
        }
    }
	private String n_accountclassificationcode_eq;//[分类]
	public void setN_accountclassificationcode_eq(String n_accountclassificationcode_eq) {
        this.n_accountclassificationcode_eq = n_accountclassificationcode_eq;
        if(!ObjectUtils.isEmpty(this.n_accountclassificationcode_eq)){
            this.getSearchCond().eq("accountclassificationcode", n_accountclassificationcode_eq);
        }
    }
	private String n_address1_addresstypecode_eq;//[地址 1: 地址类型]
	public void setN_address1_addresstypecode_eq(String n_address1_addresstypecode_eq) {
        this.n_address1_addresstypecode_eq = n_address1_addresstypecode_eq;
        if(!ObjectUtils.isEmpty(this.n_address1_addresstypecode_eq)){
            this.getSearchCond().eq("address1_addresstypecode", n_address1_addresstypecode_eq);
        }
    }
	private Integer n_statecode_eq;//[状态]
	public void setN_statecode_eq(Integer n_statecode_eq) {
        this.n_statecode_eq = n_statecode_eq;
        if(!ObjectUtils.isEmpty(this.n_statecode_eq)){
            this.getSearchCond().eq("statecode", n_statecode_eq);
        }
    }
	private String n_address2_addresstypecode_eq;//[地址 2: 地址类型]
	public void setN_address2_addresstypecode_eq(String n_address2_addresstypecode_eq) {
        this.n_address2_addresstypecode_eq = n_address2_addresstypecode_eq;
        if(!ObjectUtils.isEmpty(this.n_address2_addresstypecode_eq)){
            this.getSearchCond().eq("address2_addresstypecode", n_address2_addresstypecode_eq);
        }
    }
	private String n_accountcategorycode_eq;//[类别]
	public void setN_accountcategorycode_eq(String n_accountcategorycode_eq) {
        this.n_accountcategorycode_eq = n_accountcategorycode_eq;
        if(!ObjectUtils.isEmpty(this.n_accountcategorycode_eq)){
            this.getSearchCond().eq("accountcategorycode", n_accountcategorycode_eq);
        }
    }
	private String n_address2_shippingmethodcode_eq;//[地址 2: 送货方式]
	public void setN_address2_shippingmethodcode_eq(String n_address2_shippingmethodcode_eq) {
        this.n_address2_shippingmethodcode_eq = n_address2_shippingmethodcode_eq;
        if(!ObjectUtils.isEmpty(this.n_address2_shippingmethodcode_eq)){
            this.getSearchCond().eq("address2_shippingmethodcode", n_address2_shippingmethodcode_eq);
        }
    }
	private String n_customersizecode_eq;//[客户规模]
	public void setN_customersizecode_eq(String n_customersizecode_eq) {
        this.n_customersizecode_eq = n_customersizecode_eq;
        if(!ObjectUtils.isEmpty(this.n_customersizecode_eq)){
            this.getSearchCond().eq("customersizecode", n_customersizecode_eq);
        }
    }
	private String n_paymenttermscode_eq;//[付款方式]
	public void setN_paymenttermscode_eq(String n_paymenttermscode_eq) {
        this.n_paymenttermscode_eq = n_paymenttermscode_eq;
        if(!ObjectUtils.isEmpty(this.n_paymenttermscode_eq)){
            this.getSearchCond().eq("paymenttermscode", n_paymenttermscode_eq);
        }
    }
	private String n_preferredappointmenttimecode_eq;//[首选时间]
	public void setN_preferredappointmenttimecode_eq(String n_preferredappointmenttimecode_eq) {
        this.n_preferredappointmenttimecode_eq = n_preferredappointmenttimecode_eq;
        if(!ObjectUtils.isEmpty(this.n_preferredappointmenttimecode_eq)){
            this.getSearchCond().eq("preferredappointmenttimecode", n_preferredappointmenttimecode_eq);
        }
    }
	private String n_defaultpricelevelname_eq;//[价目表]
	public void setN_defaultpricelevelname_eq(String n_defaultpricelevelname_eq) {
        this.n_defaultpricelevelname_eq = n_defaultpricelevelname_eq;
        if(!ObjectUtils.isEmpty(this.n_defaultpricelevelname_eq)){
            this.getSearchCond().eq("defaultpricelevelname", n_defaultpricelevelname_eq);
        }
    }
	private String n_defaultpricelevelname_like;//[价目表]
	public void setN_defaultpricelevelname_like(String n_defaultpricelevelname_like) {
        this.n_defaultpricelevelname_like = n_defaultpricelevelname_like;
        if(!ObjectUtils.isEmpty(this.n_defaultpricelevelname_like)){
            this.getSearchCond().like("defaultpricelevelname", n_defaultpricelevelname_like);
        }
    }
	private String n_slaname_eq;//[SLAName]
	public void setN_slaname_eq(String n_slaname_eq) {
        this.n_slaname_eq = n_slaname_eq;
        if(!ObjectUtils.isEmpty(this.n_slaname_eq)){
            this.getSearchCond().eq("slaname", n_slaname_eq);
        }
    }
	private String n_slaname_like;//[SLAName]
	public void setN_slaname_like(String n_slaname_like) {
        this.n_slaname_like = n_slaname_like;
        if(!ObjectUtils.isEmpty(this.n_slaname_like)){
            this.getSearchCond().like("slaname", n_slaname_like);
        }
    }
	private String n_preferredequipmentname_eq;//[首选设施/设备]
	public void setN_preferredequipmentname_eq(String n_preferredequipmentname_eq) {
        this.n_preferredequipmentname_eq = n_preferredequipmentname_eq;
        if(!ObjectUtils.isEmpty(this.n_preferredequipmentname_eq)){
            this.getSearchCond().eq("preferredequipmentname", n_preferredequipmentname_eq);
        }
    }
	private String n_preferredequipmentname_like;//[首选设施/设备]
	public void setN_preferredequipmentname_like(String n_preferredequipmentname_like) {
        this.n_preferredequipmentname_like = n_preferredequipmentname_like;
        if(!ObjectUtils.isEmpty(this.n_preferredequipmentname_like)){
            this.getSearchCond().like("preferredequipmentname", n_preferredequipmentname_like);
        }
    }
	private String n_preferredservicename_eq;//[首选服务]
	public void setN_preferredservicename_eq(String n_preferredservicename_eq) {
        this.n_preferredservicename_eq = n_preferredservicename_eq;
        if(!ObjectUtils.isEmpty(this.n_preferredservicename_eq)){
            this.getSearchCond().eq("preferredservicename", n_preferredservicename_eq);
        }
    }
	private String n_preferredservicename_like;//[首选服务]
	public void setN_preferredservicename_like(String n_preferredservicename_like) {
        this.n_preferredservicename_like = n_preferredservicename_like;
        if(!ObjectUtils.isEmpty(this.n_preferredservicename_like)){
            this.getSearchCond().like("preferredservicename", n_preferredservicename_like);
        }
    }
	private String n_territoryname_eq;//[区域]
	public void setN_territoryname_eq(String n_territoryname_eq) {
        this.n_territoryname_eq = n_territoryname_eq;
        if(!ObjectUtils.isEmpty(this.n_territoryname_eq)){
            this.getSearchCond().eq("territoryname", n_territoryname_eq);
        }
    }
	private String n_territoryname_like;//[区域]
	public void setN_territoryname_like(String n_territoryname_like) {
        this.n_territoryname_like = n_territoryname_like;
        if(!ObjectUtils.isEmpty(this.n_territoryname_like)){
            this.getSearchCond().like("territoryname", n_territoryname_like);
        }
    }
	private String n_primarycontactname_eq;//[主要联系人]
	public void setN_primarycontactname_eq(String n_primarycontactname_eq) {
        this.n_primarycontactname_eq = n_primarycontactname_eq;
        if(!ObjectUtils.isEmpty(this.n_primarycontactname_eq)){
            this.getSearchCond().eq("primarycontactname", n_primarycontactname_eq);
        }
    }
	private String n_primarycontactname_like;//[主要联系人]
	public void setN_primarycontactname_like(String n_primarycontactname_like) {
        this.n_primarycontactname_like = n_primarycontactname_like;
        if(!ObjectUtils.isEmpty(this.n_primarycontactname_like)){
            this.getSearchCond().like("primarycontactname", n_primarycontactname_like);
        }
    }
	private String n_parentaccountname_eq;//[上级客户]
	public void setN_parentaccountname_eq(String n_parentaccountname_eq) {
        this.n_parentaccountname_eq = n_parentaccountname_eq;
        if(!ObjectUtils.isEmpty(this.n_parentaccountname_eq)){
            this.getSearchCond().eq("parentaccountname", n_parentaccountname_eq);
        }
    }
	private String n_parentaccountname_like;//[上级客户]
	public void setN_parentaccountname_like(String n_parentaccountname_like) {
        this.n_parentaccountname_like = n_parentaccountname_like;
        if(!ObjectUtils.isEmpty(this.n_parentaccountname_like)){
            this.getSearchCond().like("parentaccountname", n_parentaccountname_like);
        }
    }
	private String n_originatingleadname_eq;//[原始潜在顾客]
	public void setN_originatingleadname_eq(String n_originatingleadname_eq) {
        this.n_originatingleadname_eq = n_originatingleadname_eq;
        if(!ObjectUtils.isEmpty(this.n_originatingleadname_eq)){
            this.getSearchCond().eq("originatingleadname", n_originatingleadname_eq);
        }
    }
	private String n_originatingleadname_like;//[原始潜在顾客]
	public void setN_originatingleadname_like(String n_originatingleadname_like) {
        this.n_originatingleadname_like = n_originatingleadname_like;
        if(!ObjectUtils.isEmpty(this.n_originatingleadname_like)){
            this.getSearchCond().like("originatingleadname", n_originatingleadname_like);
        }
    }
	private String n_originatingleadid_eq;//[原始潜在顾客]
	public void setN_originatingleadid_eq(String n_originatingleadid_eq) {
        this.n_originatingleadid_eq = n_originatingleadid_eq;
        if(!ObjectUtils.isEmpty(this.n_originatingleadid_eq)){
            this.getSearchCond().eq("originatingleadid", n_originatingleadid_eq);
        }
    }
	private String n_transactioncurrencyid_eq;//[货币]
	public void setN_transactioncurrencyid_eq(String n_transactioncurrencyid_eq) {
        this.n_transactioncurrencyid_eq = n_transactioncurrencyid_eq;
        if(!ObjectUtils.isEmpty(this.n_transactioncurrencyid_eq)){
            this.getSearchCond().eq("transactioncurrencyid", n_transactioncurrencyid_eq);
        }
    }
	private String n_defaultpricelevelid_eq;//[价目表]
	public void setN_defaultpricelevelid_eq(String n_defaultpricelevelid_eq) {
        this.n_defaultpricelevelid_eq = n_defaultpricelevelid_eq;
        if(!ObjectUtils.isEmpty(this.n_defaultpricelevelid_eq)){
            this.getSearchCond().eq("defaultpricelevelid", n_defaultpricelevelid_eq);
        }
    }
	private String n_parentaccountid_eq;//[上级单位]
	public void setN_parentaccountid_eq(String n_parentaccountid_eq) {
        this.n_parentaccountid_eq = n_parentaccountid_eq;
        if(!ObjectUtils.isEmpty(this.n_parentaccountid_eq)){
            this.getSearchCond().eq("parentaccountid", n_parentaccountid_eq);
        }
    }
	private String n_preferredequipmentid_eq;//[首选设施/设备]
	public void setN_preferredequipmentid_eq(String n_preferredequipmentid_eq) {
        this.n_preferredequipmentid_eq = n_preferredequipmentid_eq;
        if(!ObjectUtils.isEmpty(this.n_preferredequipmentid_eq)){
            this.getSearchCond().eq("preferredequipmentid", n_preferredequipmentid_eq);
        }
    }
	private String n_territoryid_eq;//[区域]
	public void setN_territoryid_eq(String n_territoryid_eq) {
        this.n_territoryid_eq = n_territoryid_eq;
        if(!ObjectUtils.isEmpty(this.n_territoryid_eq)){
            this.getSearchCond().eq("territoryid", n_territoryid_eq);
        }
    }
	private String n_primarycontactid_eq;//[主要联系人]
	public void setN_primarycontactid_eq(String n_primarycontactid_eq) {
        this.n_primarycontactid_eq = n_primarycontactid_eq;
        if(!ObjectUtils.isEmpty(this.n_primarycontactid_eq)){
            this.getSearchCond().eq("primarycontactid", n_primarycontactid_eq);
        }
    }
	private String n_slaid_eq;//[SLA]
	public void setN_slaid_eq(String n_slaid_eq) {
        this.n_slaid_eq = n_slaid_eq;
        if(!ObjectUtils.isEmpty(this.n_slaid_eq)){
            this.getSearchCond().eq("slaid", n_slaid_eq);
        }
    }
	private String n_preferredserviceid_eq;//[首选服务]
	public void setN_preferredserviceid_eq(String n_preferredserviceid_eq) {
        this.n_preferredserviceid_eq = n_preferredserviceid_eq;
        if(!ObjectUtils.isEmpty(this.n_preferredserviceid_eq)){
            this.getSearchCond().eq("preferredserviceid", n_preferredserviceid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("accountname", query)   
            );
		 }
	}
}



