package cn.ibizlab.businesscentral.core.humanresource.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.humanresource.domain.Attendance;
/**
 * 关系型数据实体[Attendance] 查询条件对象
 */
@Slf4j
@Data
public class AttendanceSearchContext extends QueryWrapperContext<Attendance> {

	private String n_attendancename_like;//[考勤名称]
	public void setN_attendancename_like(String n_attendancename_like) {
        this.n_attendancename_like = n_attendancename_like;
        if(!ObjectUtils.isEmpty(this.n_attendancename_like)){
            this.getSearchCond().like("attendancename", n_attendancename_like);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("attendancename", query)   
            );
		 }
	}
}



