package cn.ibizlab.businesscentral.core.runtime.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.runtime.domain.Expiredprocess;
import cn.ibizlab.businesscentral.core.runtime.filter.ExpiredprocessSearchContext;
import cn.ibizlab.businesscentral.core.runtime.service.IExpiredprocessService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.runtime.mapper.ExpiredprocessMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[已过期流程] 服务对象接口实现
 */
@Slf4j
@Service("ExpiredprocessServiceImpl")
public class ExpiredprocessServiceImpl extends ServiceImpl<ExpiredprocessMapper, Expiredprocess> implements IExpiredprocessService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IKnowledgeArticleService knowledgearticleService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITransactionCurrencyService transactioncurrencyService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(Expiredprocess et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getBusinessprocessflowinstanceid()),et);
        return true;
    }

    @Override
    public void createBatch(List<Expiredprocess> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Expiredprocess et) {
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("businessprocessflowinstanceid",et.getBusinessprocessflowinstanceid())))
            return false;
        CachedBeanCopier.copy(get(et.getBusinessprocessflowinstanceid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<Expiredprocess> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Expiredprocess get(String key) {
        Expiredprocess et = getById(key);
        if(et==null){
            et=new Expiredprocess();
            et.setBusinessprocessflowinstanceid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Expiredprocess getDraft(Expiredprocess et) {
        return et;
    }

    @Override
    public boolean checkKey(Expiredprocess et) {
        return (!ObjectUtils.isEmpty(et.getBusinessprocessflowinstanceid()))&&(!Objects.isNull(this.getById(et.getBusinessprocessflowinstanceid())));
    }
    @Override
    @Transactional
    public boolean save(Expiredprocess et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Expiredprocess et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<Expiredprocess> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<Expiredprocess> list) {
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Expiredprocess> selectByKnowledgearticleid(String knowledgearticleid) {
        return baseMapper.selectByKnowledgearticleid(knowledgearticleid);
    }

    @Override
    public void removeByKnowledgearticleid(String knowledgearticleid) {
        this.remove(new QueryWrapper<Expiredprocess>().eq("knowledgearticleid",knowledgearticleid));
    }

	@Override
    public List<Expiredprocess> selectByTransactioncurrencyid(String transactioncurrencyid) {
        return baseMapper.selectByTransactioncurrencyid(transactioncurrencyid);
    }

    @Override
    public void removeByTransactioncurrencyid(String transactioncurrencyid) {
        this.remove(new QueryWrapper<Expiredprocess>().eq("transactioncurrencyid",transactioncurrencyid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<Expiredprocess> searchDefault(ExpiredprocessSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Expiredprocess> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Expiredprocess>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Expiredprocess> getExpiredprocessByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Expiredprocess> getExpiredprocessByEntities(List<Expiredprocess> entities) {
        List ids =new ArrayList();
        for(Expiredprocess entity : entities){
            Serializable id=entity.getBusinessprocessflowinstanceid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}



