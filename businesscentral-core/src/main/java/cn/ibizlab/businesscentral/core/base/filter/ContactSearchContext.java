package cn.ibizlab.businesscentral.core.base.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.base.domain.Contact;
/**
 * 关系型数据实体[Contact] 查询条件对象
 */
@Slf4j
@Data
public class ContactSearchContext extends QueryWrapperContext<Contact> {

	private String n_address1_freighttermscode_eq;//[地址 1: 货运条款]
	public void setN_address1_freighttermscode_eq(String n_address1_freighttermscode_eq) {
        this.n_address1_freighttermscode_eq = n_address1_freighttermscode_eq;
        if(!ObjectUtils.isEmpty(this.n_address1_freighttermscode_eq)){
            this.getSearchCond().eq("address1_freighttermscode", n_address1_freighttermscode_eq);
        }
    }
	private String n_familystatuscode_eq;//[婚姻状况]
	public void setN_familystatuscode_eq(String n_familystatuscode_eq) {
        this.n_familystatuscode_eq = n_familystatuscode_eq;
        if(!ObjectUtils.isEmpty(this.n_familystatuscode_eq)){
            this.getSearchCond().eq("familystatuscode", n_familystatuscode_eq);
        }
    }
	private String n_address3_shippingmethodcode_eq;//[地址 3: 送货方式]
	public void setN_address3_shippingmethodcode_eq(String n_address3_shippingmethodcode_eq) {
        this.n_address3_shippingmethodcode_eq = n_address3_shippingmethodcode_eq;
        if(!ObjectUtils.isEmpty(this.n_address3_shippingmethodcode_eq)){
            this.getSearchCond().eq("address3_shippingmethodcode", n_address3_shippingmethodcode_eq);
        }
    }
	private String n_educationcode_eq;//[教育]
	public void setN_educationcode_eq(String n_educationcode_eq) {
        this.n_educationcode_eq = n_educationcode_eq;
        if(!ObjectUtils.isEmpty(this.n_educationcode_eq)){
            this.getSearchCond().eq("educationcode", n_educationcode_eq);
        }
    }
	private String n_haschildrencode_eq;//[有子女]
	public void setN_haschildrencode_eq(String n_haschildrencode_eq) {
        this.n_haschildrencode_eq = n_haschildrencode_eq;
        if(!ObjectUtils.isEmpty(this.n_haschildrencode_eq)){
            this.getSearchCond().eq("haschildrencode", n_haschildrencode_eq);
        }
    }
	private String n_paymenttermscode_eq;//[付款方式]
	public void setN_paymenttermscode_eq(String n_paymenttermscode_eq) {
        this.n_paymenttermscode_eq = n_paymenttermscode_eq;
        if(!ObjectUtils.isEmpty(this.n_paymenttermscode_eq)){
            this.getSearchCond().eq("paymenttermscode", n_paymenttermscode_eq);
        }
    }
	private String n_address2_shippingmethodcode_eq;//[地址 2: 送货方式]
	public void setN_address2_shippingmethodcode_eq(String n_address2_shippingmethodcode_eq) {
        this.n_address2_shippingmethodcode_eq = n_address2_shippingmethodcode_eq;
        if(!ObjectUtils.isEmpty(this.n_address2_shippingmethodcode_eq)){
            this.getSearchCond().eq("address2_shippingmethodcode", n_address2_shippingmethodcode_eq);
        }
    }
	private String n_customersizecode_eq;//[客户规模]
	public void setN_customersizecode_eq(String n_customersizecode_eq) {
        this.n_customersizecode_eq = n_customersizecode_eq;
        if(!ObjectUtils.isEmpty(this.n_customersizecode_eq)){
            this.getSearchCond().eq("customersizecode", n_customersizecode_eq);
        }
    }
	private String n_address3_addresstypecode_eq;//[地址 3: 地址类型]
	public void setN_address3_addresstypecode_eq(String n_address3_addresstypecode_eq) {
        this.n_address3_addresstypecode_eq = n_address3_addresstypecode_eq;
        if(!ObjectUtils.isEmpty(this.n_address3_addresstypecode_eq)){
            this.getSearchCond().eq("address3_addresstypecode", n_address3_addresstypecode_eq);
        }
    }
	private String n_leadsourcecode_eq;//[潜在顾客来源]
	public void setN_leadsourcecode_eq(String n_leadsourcecode_eq) {
        this.n_leadsourcecode_eq = n_leadsourcecode_eq;
        if(!ObjectUtils.isEmpty(this.n_leadsourcecode_eq)){
            this.getSearchCond().eq("leadsourcecode", n_leadsourcecode_eq);
        }
    }
	private Integer n_statecode_eq;//[状态]
	public void setN_statecode_eq(Integer n_statecode_eq) {
        this.n_statecode_eq = n_statecode_eq;
        if(!ObjectUtils.isEmpty(this.n_statecode_eq)){
            this.getSearchCond().eq("statecode", n_statecode_eq);
        }
    }
	private String n_address2_freighttermscode_eq;//[地址 2: 货运条款]
	public void setN_address2_freighttermscode_eq(String n_address2_freighttermscode_eq) {
        this.n_address2_freighttermscode_eq = n_address2_freighttermscode_eq;
        if(!ObjectUtils.isEmpty(this.n_address2_freighttermscode_eq)){
            this.getSearchCond().eq("address2_freighttermscode", n_address2_freighttermscode_eq);
        }
    }
	private String n_address1_addresstypecode_eq;//[地址 1: 地址类型]
	public void setN_address1_addresstypecode_eq(String n_address1_addresstypecode_eq) {
        this.n_address1_addresstypecode_eq = n_address1_addresstypecode_eq;
        if(!ObjectUtils.isEmpty(this.n_address1_addresstypecode_eq)){
            this.getSearchCond().eq("address1_addresstypecode", n_address1_addresstypecode_eq);
        }
    }
	private String n_accountrolecode_eq;//[角色]
	public void setN_accountrolecode_eq(String n_accountrolecode_eq) {
        this.n_accountrolecode_eq = n_accountrolecode_eq;
        if(!ObjectUtils.isEmpty(this.n_accountrolecode_eq)){
            this.getSearchCond().eq("accountrolecode", n_accountrolecode_eq);
        }
    }
	private String n_shippingmethodcode_eq;//[送货方式]
	public void setN_shippingmethodcode_eq(String n_shippingmethodcode_eq) {
        this.n_shippingmethodcode_eq = n_shippingmethodcode_eq;
        if(!ObjectUtils.isEmpty(this.n_shippingmethodcode_eq)){
            this.getSearchCond().eq("shippingmethodcode", n_shippingmethodcode_eq);
        }
    }
	private String n_customertypecode_eq;//[关系类型]
	public void setN_customertypecode_eq(String n_customertypecode_eq) {
        this.n_customertypecode_eq = n_customertypecode_eq;
        if(!ObjectUtils.isEmpty(this.n_customertypecode_eq)){
            this.getSearchCond().eq("customertypecode", n_customertypecode_eq);
        }
    }
	private String n_fullname_like;//[全名]
	public void setN_fullname_like(String n_fullname_like) {
        this.n_fullname_like = n_fullname_like;
        if(!ObjectUtils.isEmpty(this.n_fullname_like)){
            this.getSearchCond().like("fullname", n_fullname_like);
        }
    }
	private String n_gendercode_eq;//[性别]
	public void setN_gendercode_eq(String n_gendercode_eq) {
        this.n_gendercode_eq = n_gendercode_eq;
        if(!ObjectUtils.isEmpty(this.n_gendercode_eq)){
            this.getSearchCond().eq("gendercode", n_gendercode_eq);
        }
    }
	private String n_territorycode_eq;//[区域]
	public void setN_territorycode_eq(String n_territorycode_eq) {
        this.n_territorycode_eq = n_territorycode_eq;
        if(!ObjectUtils.isEmpty(this.n_territorycode_eq)){
            this.getSearchCond().eq("territorycode", n_territorycode_eq);
        }
    }
	private String n_preferredappointmentdaycode_eq;//[首选日]
	public void setN_preferredappointmentdaycode_eq(String n_preferredappointmentdaycode_eq) {
        this.n_preferredappointmentdaycode_eq = n_preferredappointmentdaycode_eq;
        if(!ObjectUtils.isEmpty(this.n_preferredappointmentdaycode_eq)){
            this.getSearchCond().eq("preferredappointmentdaycode", n_preferredappointmentdaycode_eq);
        }
    }
	private String n_preferredappointmenttimecode_eq;//[首选时间]
	public void setN_preferredappointmenttimecode_eq(String n_preferredappointmenttimecode_eq) {
        this.n_preferredappointmenttimecode_eq = n_preferredappointmenttimecode_eq;
        if(!ObjectUtils.isEmpty(this.n_preferredappointmenttimecode_eq)){
            this.getSearchCond().eq("preferredappointmenttimecode", n_preferredappointmenttimecode_eq);
        }
    }
	private Integer n_statuscode_eq;//[状态描述]
	public void setN_statuscode_eq(Integer n_statuscode_eq) {
        this.n_statuscode_eq = n_statuscode_eq;
        if(!ObjectUtils.isEmpty(this.n_statuscode_eq)){
            this.getSearchCond().eq("statuscode", n_statuscode_eq);
        }
    }
	private String n_preferredcontactmethodcode_eq;//[首选联系方式]
	public void setN_preferredcontactmethodcode_eq(String n_preferredcontactmethodcode_eq) {
        this.n_preferredcontactmethodcode_eq = n_preferredcontactmethodcode_eq;
        if(!ObjectUtils.isEmpty(this.n_preferredcontactmethodcode_eq)){
            this.getSearchCond().eq("preferredcontactmethodcode", n_preferredcontactmethodcode_eq);
        }
    }
	private String n_address2_addresstypecode_eq;//[地址 2: 地址类型]
	public void setN_address2_addresstypecode_eq(String n_address2_addresstypecode_eq) {
        this.n_address2_addresstypecode_eq = n_address2_addresstypecode_eq;
        if(!ObjectUtils.isEmpty(this.n_address2_addresstypecode_eq)){
            this.getSearchCond().eq("address2_addresstypecode", n_address2_addresstypecode_eq);
        }
    }
	private String n_address1_shippingmethodcode_eq;//[地址 1: 送货方式]
	public void setN_address1_shippingmethodcode_eq(String n_address1_shippingmethodcode_eq) {
        this.n_address1_shippingmethodcode_eq = n_address1_shippingmethodcode_eq;
        if(!ObjectUtils.isEmpty(this.n_address1_shippingmethodcode_eq)){
            this.getSearchCond().eq("address1_shippingmethodcode", n_address1_shippingmethodcode_eq);
        }
    }
	private String n_address3_freighttermscode_eq;//[地址 3: 货运条款]
	public void setN_address3_freighttermscode_eq(String n_address3_freighttermscode_eq) {
        this.n_address3_freighttermscode_eq = n_address3_freighttermscode_eq;
        if(!ObjectUtils.isEmpty(this.n_address3_freighttermscode_eq)){
            this.getSearchCond().eq("address3_freighttermscode", n_address3_freighttermscode_eq);
        }
    }
	private String n_currencyname_eq;//[货币]
	public void setN_currencyname_eq(String n_currencyname_eq) {
        this.n_currencyname_eq = n_currencyname_eq;
        if(!ObjectUtils.isEmpty(this.n_currencyname_eq)){
            this.getSearchCond().eq("currencyname", n_currencyname_eq);
        }
    }
	private String n_currencyname_like;//[货币]
	public void setN_currencyname_like(String n_currencyname_like) {
        this.n_currencyname_like = n_currencyname_like;
        if(!ObjectUtils.isEmpty(this.n_currencyname_like)){
            this.getSearchCond().like("currencyname", n_currencyname_like);
        }
    }
	private String n_preferredservicename_eq;//[首选服务]
	public void setN_preferredservicename_eq(String n_preferredservicename_eq) {
        this.n_preferredservicename_eq = n_preferredservicename_eq;
        if(!ObjectUtils.isEmpty(this.n_preferredservicename_eq)){
            this.getSearchCond().eq("preferredservicename", n_preferredservicename_eq);
        }
    }
	private String n_preferredservicename_like;//[首选服务]
	public void setN_preferredservicename_like(String n_preferredservicename_like) {
        this.n_preferredservicename_like = n_preferredservicename_like;
        if(!ObjectUtils.isEmpty(this.n_preferredservicename_like)){
            this.getSearchCond().like("preferredservicename", n_preferredservicename_like);
        }
    }
	private String n_originatingleadname_eq;//[原始潜在顾客]
	public void setN_originatingleadname_eq(String n_originatingleadname_eq) {
        this.n_originatingleadname_eq = n_originatingleadname_eq;
        if(!ObjectUtils.isEmpty(this.n_originatingleadname_eq)){
            this.getSearchCond().eq("originatingleadname", n_originatingleadname_eq);
        }
    }
	private String n_originatingleadname_like;//[原始潜在顾客]
	public void setN_originatingleadname_like(String n_originatingleadname_like) {
        this.n_originatingleadname_like = n_originatingleadname_like;
        if(!ObjectUtils.isEmpty(this.n_originatingleadname_like)){
            this.getSearchCond().like("originatingleadname", n_originatingleadname_like);
        }
    }
	private String n_preferredequipmentname_eq;//[首选设施/设备]
	public void setN_preferredequipmentname_eq(String n_preferredequipmentname_eq) {
        this.n_preferredequipmentname_eq = n_preferredequipmentname_eq;
        if(!ObjectUtils.isEmpty(this.n_preferredequipmentname_eq)){
            this.getSearchCond().eq("preferredequipmentname", n_preferredequipmentname_eq);
        }
    }
	private String n_preferredequipmentname_like;//[首选设施/设备]
	public void setN_preferredequipmentname_like(String n_preferredequipmentname_like) {
        this.n_preferredequipmentname_like = n_preferredequipmentname_like;
        if(!ObjectUtils.isEmpty(this.n_preferredequipmentname_like)){
            this.getSearchCond().like("preferredequipmentname", n_preferredequipmentname_like);
        }
    }
	private String n_customername_eq;//[客户]
	public void setN_customername_eq(String n_customername_eq) {
        this.n_customername_eq = n_customername_eq;
        if(!ObjectUtils.isEmpty(this.n_customername_eq)){
            this.getSearchCond().eq("customername", n_customername_eq);
        }
    }
	private String n_customername_like;//[客户]
	public void setN_customername_like(String n_customername_like) {
        this.n_customername_like = n_customername_like;
        if(!ObjectUtils.isEmpty(this.n_customername_like)){
            this.getSearchCond().like("customername", n_customername_like);
        }
    }
	private String n_defaultpricelevelname_eq;//[价目表]
	public void setN_defaultpricelevelname_eq(String n_defaultpricelevelname_eq) {
        this.n_defaultpricelevelname_eq = n_defaultpricelevelname_eq;
        if(!ObjectUtils.isEmpty(this.n_defaultpricelevelname_eq)){
            this.getSearchCond().eq("defaultpricelevelname", n_defaultpricelevelname_eq);
        }
    }
	private String n_defaultpricelevelname_like;//[价目表]
	public void setN_defaultpricelevelname_like(String n_defaultpricelevelname_like) {
        this.n_defaultpricelevelname_like = n_defaultpricelevelname_like;
        if(!ObjectUtils.isEmpty(this.n_defaultpricelevelname_like)){
            this.getSearchCond().like("defaultpricelevelname", n_defaultpricelevelname_like);
        }
    }
	private String n_customerid_eq;//[客户]
	public void setN_customerid_eq(String n_customerid_eq) {
        this.n_customerid_eq = n_customerid_eq;
        if(!ObjectUtils.isEmpty(this.n_customerid_eq)){
            this.getSearchCond().eq("customerid", n_customerid_eq);
        }
    }
	private String n_defaultpricelevelid_eq;//[价目表]
	public void setN_defaultpricelevelid_eq(String n_defaultpricelevelid_eq) {
        this.n_defaultpricelevelid_eq = n_defaultpricelevelid_eq;
        if(!ObjectUtils.isEmpty(this.n_defaultpricelevelid_eq)){
            this.getSearchCond().eq("defaultpricelevelid", n_defaultpricelevelid_eq);
        }
    }
	private String n_preferredequipmentid_eq;//[首选设施/设备]
	public void setN_preferredequipmentid_eq(String n_preferredequipmentid_eq) {
        this.n_preferredequipmentid_eq = n_preferredequipmentid_eq;
        if(!ObjectUtils.isEmpty(this.n_preferredequipmentid_eq)){
            this.getSearchCond().eq("preferredequipmentid", n_preferredequipmentid_eq);
        }
    }
	private String n_transactioncurrencyid_eq;//[货币]
	public void setN_transactioncurrencyid_eq(String n_transactioncurrencyid_eq) {
        this.n_transactioncurrencyid_eq = n_transactioncurrencyid_eq;
        if(!ObjectUtils.isEmpty(this.n_transactioncurrencyid_eq)){
            this.getSearchCond().eq("transactioncurrencyid", n_transactioncurrencyid_eq);
        }
    }
	private String n_slaid_eq;//[SLA]
	public void setN_slaid_eq(String n_slaid_eq) {
        this.n_slaid_eq = n_slaid_eq;
        if(!ObjectUtils.isEmpty(this.n_slaid_eq)){
            this.getSearchCond().eq("slaid", n_slaid_eq);
        }
    }
	private String n_originatingleadid_eq;//[原始潜在顾客]
	public void setN_originatingleadid_eq(String n_originatingleadid_eq) {
        this.n_originatingleadid_eq = n_originatingleadid_eq;
        if(!ObjectUtils.isEmpty(this.n_originatingleadid_eq)){
            this.getSearchCond().eq("originatingleadid", n_originatingleadid_eq);
        }
    }
	private String n_preferredserviceid_eq;//[首选服务]
	public void setN_preferredserviceid_eq(String n_preferredserviceid_eq) {
        this.n_preferredserviceid_eq = n_preferredserviceid_eq;
        if(!ObjectUtils.isEmpty(this.n_preferredserviceid_eq)){
            this.getSearchCond().eq("preferredserviceid", n_preferredserviceid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("fullname", query)   
            );
		 }
	}
}



