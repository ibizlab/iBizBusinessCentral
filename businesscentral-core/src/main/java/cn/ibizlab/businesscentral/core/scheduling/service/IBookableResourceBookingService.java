package cn.ibizlab.businesscentral.core.scheduling.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.scheduling.domain.BookableResourceBooking;
import cn.ibizlab.businesscentral.core.scheduling.filter.BookableResourceBookingSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[BookableResourceBooking] 服务对象接口
 */
public interface IBookableResourceBookingService extends IService<BookableResourceBooking>{

    boolean create(BookableResourceBooking et) ;
    void createBatch(List<BookableResourceBooking> list) ;
    boolean update(BookableResourceBooking et) ;
    void updateBatch(List<BookableResourceBooking> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    BookableResourceBooking get(String key) ;
    BookableResourceBooking getDraft(BookableResourceBooking et) ;
    boolean checkKey(BookableResourceBooking et) ;
    boolean save(BookableResourceBooking et) ;
    void saveBatch(List<BookableResourceBooking> list) ;
    Page<BookableResourceBooking> searchDefault(BookableResourceBookingSearchContext context) ;
    List<BookableResourceBooking> selectByHeader(String bookableresourcebookingheaderid) ;
    void removeByHeader(String bookableresourcebookingheaderid) ;
    List<BookableResourceBooking> selectByResource(String bookableresourceid) ;
    void removeByResource(String bookableresourceid) ;
    List<BookableResourceBooking> selectByBookingstatus(String bookingstatusid) ;
    void removeByBookingstatus(String bookingstatusid) ;
    List<BookableResourceBooking> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<BookableResourceBooking> getBookableresourcebookingByIds(List<String> ids) ;
    List<BookableResourceBooking> getBookableresourcebookingByEntities(List<BookableResourceBooking> entities) ;
}


