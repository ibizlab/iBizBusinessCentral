package cn.ibizlab.businesscentral.core.service.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.service.domain.EntitlementTemplate;
/**
 * 关系型数据实体[EntitlementTemplate] 查询条件对象
 */
@Slf4j
@Data
public class EntitlementTemplateSearchContext extends QueryWrapperContext<EntitlementTemplate> {

	private String n_kbaccesslevel_eq;//[KB 访问级别]
	public void setN_kbaccesslevel_eq(String n_kbaccesslevel_eq) {
        this.n_kbaccesslevel_eq = n_kbaccesslevel_eq;
        if(!ObjectUtils.isEmpty(this.n_kbaccesslevel_eq)){
            this.getSearchCond().eq("kbaccesslevel", n_kbaccesslevel_eq);
        }
    }
	private String n_allocationtypecode_eq;//[分配类型]
	public void setN_allocationtypecode_eq(String n_allocationtypecode_eq) {
        this.n_allocationtypecode_eq = n_allocationtypecode_eq;
        if(!ObjectUtils.isEmpty(this.n_allocationtypecode_eq)){
            this.getSearchCond().eq("allocationtypecode", n_allocationtypecode_eq);
        }
    }
	private String n_decreaseremainingon_eq;//[缩短剩余期限]
	public void setN_decreaseremainingon_eq(String n_decreaseremainingon_eq) {
        this.n_decreaseremainingon_eq = n_decreaseremainingon_eq;
        if(!ObjectUtils.isEmpty(this.n_decreaseremainingon_eq)){
            this.getSearchCond().eq("decreaseremainingon", n_decreaseremainingon_eq);
        }
    }
	private String n_entitlementtemplatename_like;//[授权模板名称]
	public void setN_entitlementtemplatename_like(String n_entitlementtemplatename_like) {
        this.n_entitlementtemplatename_like = n_entitlementtemplatename_like;
        if(!ObjectUtils.isEmpty(this.n_entitlementtemplatename_like)){
            this.getSearchCond().like("entitlementtemplatename", n_entitlementtemplatename_like);
        }
    }
	private String n_entitytype_eq;//[entitytype]
	public void setN_entitytype_eq(String n_entitytype_eq) {
        this.n_entitytype_eq = n_entitytype_eq;
        if(!ObjectUtils.isEmpty(this.n_entitytype_eq)){
            this.getSearchCond().eq("entitytype", n_entitytype_eq);
        }
    }
	private String n_slaid_eq;//[SLA]
	public void setN_slaid_eq(String n_slaid_eq) {
        this.n_slaid_eq = n_slaid_eq;
        if(!ObjectUtils.isEmpty(this.n_slaid_eq)){
            this.getSearchCond().eq("slaid", n_slaid_eq);
        }
    }
	private String n_transactioncurrencyid_eq;//[货币]
	public void setN_transactioncurrencyid_eq(String n_transactioncurrencyid_eq) {
        this.n_transactioncurrencyid_eq = n_transactioncurrencyid_eq;
        if(!ObjectUtils.isEmpty(this.n_transactioncurrencyid_eq)){
            this.getSearchCond().eq("transactioncurrencyid", n_transactioncurrencyid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("entitlementtemplatename", query)   
            );
		 }
	}
}



