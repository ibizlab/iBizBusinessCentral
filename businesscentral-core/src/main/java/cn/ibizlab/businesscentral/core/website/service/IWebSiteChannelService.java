package cn.ibizlab.businesscentral.core.website.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.website.domain.WebSiteChannel;
import cn.ibizlab.businesscentral.core.website.filter.WebSiteChannelSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[WebSiteChannel] 服务对象接口
 */
public interface IWebSiteChannelService extends IService<WebSiteChannel>{

    boolean create(WebSiteChannel et) ;
    void createBatch(List<WebSiteChannel> list) ;
    boolean update(WebSiteChannel et) ;
    void updateBatch(List<WebSiteChannel> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    WebSiteChannel get(String key) ;
    WebSiteChannel getDraft(WebSiteChannel et) ;
    boolean checkKey(WebSiteChannel et) ;
    boolean save(WebSiteChannel et) ;
    void saveBatch(List<WebSiteChannel> list) ;
    Page<WebSiteChannel> searchDefault(WebSiteChannelSearchContext context) ;
    Page<WebSiteChannel> searchRoot(WebSiteChannelSearchContext context) ;
    List<WebSiteChannel> selectByPwebsitechannelid(String websitechannelid) ;
    void removeByPwebsitechannelid(String websitechannelid) ;
    List<WebSiteChannel> selectByWebsiteid(String websiteid) ;
    void removeByWebsiteid(String websiteid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<WebSiteChannel> getWebsitechannelByIds(List<String> ids) ;
    List<WebSiteChannel> getWebsitechannelByEntities(List<WebSiteChannel> entities) ;
}


