package cn.ibizlab.businesscentral.core.service.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[服务]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SERVICE",resultMap = "IBizServiceResultMap")
public class IBizService extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * Time Zone Rule Version Number
     */
    @TableField(value = "timezoneruleversionnumber")
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;
    /**
     * Record Created On
     */
    @TableField(value = "overriddencreatedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;
    /**
     * 定位偏差
     */
    @TableField(value = "anchoroffset")
    @JSONField(name = "anchoroffset")
    @JsonProperty("anchoroffset")
    private Integer anchoroffset;
    /**
     * 初始状态描述
     */
    @TableField(value = "initialstatuscode")
    @JSONField(name = "initialstatuscode")
    @JsonProperty("initialstatuscode")
    private Integer initialstatuscode;
    /**
     * 粒度
     */
    @TableField(value = "granularity")
    @JSONField(name = "granularity")
    @JsonProperty("granularity")
    private String granularity;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 说明
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 显示资源
     */
    @DEField(defaultValue = "0")
    @TableField(value = "showresources")
    @JSONField(name = "showresources")
    @JsonProperty("showresources")
    private Integer showresources;
    /**
     * service名称
     */
    @TableField(value = "servicename")
    @JSONField(name = "servicename")
    @JsonProperty("servicename")
    private String servicename;
    /**
     * Import Sequence Number
     */
    @TableField(value = "importsequencenumber")
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;
    /**
     * 可公开
     */
    @DEField(defaultValue = "0")
    @TableField(value = "visible")
    @JSONField(name = "visible")
    @JsonProperty("visible")
    private Integer visible;
    /**
     * 日历
     */
    @TableField(value = "calendarid")
    @JSONField(name = "calendarid")
    @JsonProperty("calendarid")
    private String calendarid;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 状态
     */
    @DEField(defaultValue = "1")
    @TableField(value = "schedulable")
    @JSONField(name = "schedulable")
    @JsonProperty("schedulable")
    private Integer schedulable;
    /**
     * 策略
     */
    @TableField(value = "strategyid")
    @JSONField(name = "strategyid")
    @JsonProperty("strategyid")
    private String strategyid;
    /**
     * Version Number
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * 服务
     */
    @DEField(isKeyField=true)
    @TableId(value= "serviceid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "serviceid")
    @JsonProperty("serviceid")
    private String serviceid;
    /**
     * 持续时间
     */
    @TableField(value = "duration")
    @JSONField(name = "duration")
    @JsonProperty("duration")
    private Integer duration;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * UTC Conversion Time Zone Code
     */
    @TableField(value = "utcconversiontimezonecode")
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;
    /**
     * 所需资源
     */
    @TableField(value = "resourcespecid")
    @JSONField(name = "resourcespecid")
    @JsonProperty("resourcespecid")
    private String resourcespecid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.service.domain.ResourceSpec resourcespec;



    /**
     * 设置 [Time Zone Rule Version Number]
     */
    public void setTimezoneruleversionnumber(Integer timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [Record Created On]
     */
    public void setOverriddencreatedon(Timestamp overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 格式化日期 [Record Created On]
     */
    public String formatOverriddencreatedon(){
        if (this.overriddencreatedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(overriddencreatedon);
    }
    /**
     * 设置 [定位偏差]
     */
    public void setAnchoroffset(Integer anchoroffset){
        this.anchoroffset = anchoroffset ;
        this.modify("anchoroffset",anchoroffset);
    }

    /**
     * 设置 [初始状态描述]
     */
    public void setInitialstatuscode(Integer initialstatuscode){
        this.initialstatuscode = initialstatuscode ;
        this.modify("initialstatuscode",initialstatuscode);
    }

    /**
     * 设置 [粒度]
     */
    public void setGranularity(String granularity){
        this.granularity = granularity ;
        this.modify("granularity",granularity);
    }

    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [显示资源]
     */
    public void setShowresources(Integer showresources){
        this.showresources = showresources ;
        this.modify("showresources",showresources);
    }

    /**
     * 设置 [service名称]
     */
    public void setServicename(String servicename){
        this.servicename = servicename ;
        this.modify("servicename",servicename);
    }

    /**
     * 设置 [Import Sequence Number]
     */
    public void setImportsequencenumber(Integer importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [可公开]
     */
    public void setVisible(Integer visible){
        this.visible = visible ;
        this.modify("visible",visible);
    }

    /**
     * 设置 [日历]
     */
    public void setCalendarid(String calendarid){
        this.calendarid = calendarid ;
        this.modify("calendarid",calendarid);
    }

    /**
     * 设置 [状态]
     */
    public void setSchedulable(Integer schedulable){
        this.schedulable = schedulable ;
        this.modify("schedulable",schedulable);
    }

    /**
     * 设置 [策略]
     */
    public void setStrategyid(String strategyid){
        this.strategyid = strategyid ;
        this.modify("strategyid",strategyid);
    }

    /**
     * 设置 [Version Number]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [持续时间]
     */
    public void setDuration(Integer duration){
        this.duration = duration ;
        this.modify("duration",duration);
    }

    /**
     * 设置 [UTC Conversion Time Zone Code]
     */
    public void setUtcconversiontimezonecode(Integer utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [所需资源]
     */
    public void setResourcespecid(String resourcespecid){
        this.resourcespecid = resourcespecid ;
        this.modify("resourcespecid",resourcespecid);
    }


}


