package cn.ibizlab.businesscentral.core.sales.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.sales.domain.CompetitorSalesLiterature;
import cn.ibizlab.businesscentral.core.sales.filter.CompetitorSalesLiteratureSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[CompetitorSalesLiterature] 服务对象接口
 */
public interface ICompetitorSalesLiteratureService extends IService<CompetitorSalesLiterature>{

    boolean create(CompetitorSalesLiterature et) ;
    void createBatch(List<CompetitorSalesLiterature> list) ;
    boolean update(CompetitorSalesLiterature et) ;
    void updateBatch(List<CompetitorSalesLiterature> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    CompetitorSalesLiterature get(String key) ;
    CompetitorSalesLiterature getDraft(CompetitorSalesLiterature et) ;
    boolean checkKey(CompetitorSalesLiterature et) ;
    boolean save(CompetitorSalesLiterature et) ;
    void saveBatch(List<CompetitorSalesLiterature> list) ;
    Page<CompetitorSalesLiterature> searchDefault(CompetitorSalesLiteratureSearchContext context) ;
    List<CompetitorSalesLiterature> selectByEntityid(String competitorid) ;
    void removeByEntityid(String competitorid) ;
    List<CompetitorSalesLiterature> selectByEntity2id(String salesliteratureid) ;
    void removeByEntity2id(String salesliteratureid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<CompetitorSalesLiterature> getCompetitorsalesliteratureByIds(List<String> ids) ;
    List<CompetitorSalesLiterature> getCompetitorsalesliteratureByEntities(List<CompetitorSalesLiterature> entities) ;
}


