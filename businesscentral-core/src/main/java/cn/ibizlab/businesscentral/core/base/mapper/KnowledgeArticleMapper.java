package cn.ibizlab.businesscentral.core.base.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.base.domain.KnowledgeArticle;
import cn.ibizlab.businesscentral.core.base.filter.KnowledgeArticleSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface KnowledgeArticleMapper extends BaseMapper<KnowledgeArticle>{

    Page<KnowledgeArticle> searchDefault(IPage page, @Param("srf") KnowledgeArticleSearchContext context, @Param("ew") Wrapper<KnowledgeArticle> wrapper) ;
    @Override
    KnowledgeArticle selectById(Serializable id);
    @Override
    int insert(KnowledgeArticle entity);
    @Override
    int updateById(@Param(Constants.ENTITY) KnowledgeArticle entity);
    @Override
    int update(@Param(Constants.ENTITY) KnowledgeArticle entity, @Param("ew") Wrapper<KnowledgeArticle> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<KnowledgeArticle> selectByParentarticlecontentid(@Param("knowledgearticleid") Serializable knowledgearticleid) ;

    List<KnowledgeArticle> selectByPreviousarticlecontentid(@Param("knowledgearticleid") Serializable knowledgearticleid) ;

    List<KnowledgeArticle> selectByRootarticleid(@Param("knowledgearticleid") Serializable knowledgearticleid) ;

    List<KnowledgeArticle> selectByLanguagelocaleid(@Param("languagelocaleid") Serializable languagelocaleid) ;

    List<KnowledgeArticle> selectBySubjectid(@Param("subjectid") Serializable subjectid) ;

    List<KnowledgeArticle> selectByTransactioncurrencyid(@Param("transactioncurrencyid") Serializable transactioncurrencyid) ;

}
