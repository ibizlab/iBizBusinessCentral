package cn.ibizlab.businesscentral.core.runtime.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.runtime.domain.Queue;
/**
 * 关系型数据实体[Queue] 查询条件对象
 */
@Slf4j
@Data
public class QueueSearchContext extends QueryWrapperContext<Queue> {

	private String n_incomingemaildeliverymethod_eq;//[接收电子邮件的传送方式]
	public void setN_incomingemaildeliverymethod_eq(String n_incomingemaildeliverymethod_eq) {
        this.n_incomingemaildeliverymethod_eq = n_incomingemaildeliverymethod_eq;
        if(!ObjectUtils.isEmpty(this.n_incomingemaildeliverymethod_eq)){
            this.getSearchCond().eq("incomingemaildeliverymethod", n_incomingemaildeliverymethod_eq);
        }
    }
	private String n_queuetypecode_eq;//[队列类型]
	public void setN_queuetypecode_eq(String n_queuetypecode_eq) {
        this.n_queuetypecode_eq = n_queuetypecode_eq;
        if(!ObjectUtils.isEmpty(this.n_queuetypecode_eq)){
            this.getSearchCond().eq("queuetypecode", n_queuetypecode_eq);
        }
    }
	private String n_incomingemailfilteringmethod_eq;//[将传入电子邮件转换为活动]
	public void setN_incomingemailfilteringmethod_eq(String n_incomingemailfilteringmethod_eq) {
        this.n_incomingemailfilteringmethod_eq = n_incomingemailfilteringmethod_eq;
        if(!ObjectUtils.isEmpty(this.n_incomingemailfilteringmethod_eq)){
            this.getSearchCond().eq("incomingemailfilteringmethod", n_incomingemailfilteringmethod_eq);
        }
    }
	private String n_queuename_like;//[队列名称]
	public void setN_queuename_like(String n_queuename_like) {
        this.n_queuename_like = n_queuename_like;
        if(!ObjectUtils.isEmpty(this.n_queuename_like)){
            this.getSearchCond().like("queuename", n_queuename_like);
        }
    }
	private String n_queueviewtype_eq;//[类型]
	public void setN_queueviewtype_eq(String n_queueviewtype_eq) {
        this.n_queueviewtype_eq = n_queueviewtype_eq;
        if(!ObjectUtils.isEmpty(this.n_queueviewtype_eq)){
            this.getSearchCond().eq("queueviewtype", n_queueviewtype_eq);
        }
    }
	private Integer n_statecode_eq;//[状态]
	public void setN_statecode_eq(Integer n_statecode_eq) {
        this.n_statecode_eq = n_statecode_eq;
        if(!ObjectUtils.isEmpty(this.n_statecode_eq)){
            this.getSearchCond().eq("statecode", n_statecode_eq);
        }
    }
	private Integer n_statuscode_eq;//[状态描述]
	public void setN_statuscode_eq(Integer n_statuscode_eq) {
        this.n_statuscode_eq = n_statuscode_eq;
        if(!ObjectUtils.isEmpty(this.n_statuscode_eq)){
            this.getSearchCond().eq("statuscode", n_statuscode_eq);
        }
    }
	private String n_outgoingemaildeliverymethod_eq;//[外发的电子邮件的传送方式]
	public void setN_outgoingemaildeliverymethod_eq(String n_outgoingemaildeliverymethod_eq) {
        this.n_outgoingemaildeliverymethod_eq = n_outgoingemaildeliverymethod_eq;
        if(!ObjectUtils.isEmpty(this.n_outgoingemaildeliverymethod_eq)){
            this.getSearchCond().eq("outgoingemaildeliverymethod", n_outgoingemaildeliverymethod_eq);
        }
    }
	private String n_emailrouteraccessapproval_eq;//[主要电子邮件状态]
	public void setN_emailrouteraccessapproval_eq(String n_emailrouteraccessapproval_eq) {
        this.n_emailrouteraccessapproval_eq = n_emailrouteraccessapproval_eq;
        if(!ObjectUtils.isEmpty(this.n_emailrouteraccessapproval_eq)){
            this.getSearchCond().eq("emailrouteraccessapproval", n_emailrouteraccessapproval_eq);
        }
    }
	private String n_transactioncurrencyid_eq;//[货币]
	public void setN_transactioncurrencyid_eq(String n_transactioncurrencyid_eq) {
        this.n_transactioncurrencyid_eq = n_transactioncurrencyid_eq;
        if(!ObjectUtils.isEmpty(this.n_transactioncurrencyid_eq)){
            this.getSearchCond().eq("transactioncurrencyid", n_transactioncurrencyid_eq);
        }
    }
	private String n_businessunitid_eq;//[业务部门]
	public void setN_businessunitid_eq(String n_businessunitid_eq) {
        this.n_businessunitid_eq = n_businessunitid_eq;
        if(!ObjectUtils.isEmpty(this.n_businessunitid_eq)){
            this.getSearchCond().eq("businessunitid", n_businessunitid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("queuename", query)   
            );
		 }
	}
}



