package cn.ibizlab.businesscentral.core.base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.base.domain.Account;
import cn.ibizlab.businesscentral.core.base.filter.AccountSearchContext;
import cn.ibizlab.businesscentral.core.base.service.IAccountService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.base.mapper.AccountMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[客户] 服务对象接口实现
 */
@Slf4j
@Service("AccountServiceImpl")
public class AccountServiceImpl extends ServiceImpl<AccountMapper, Account> implements IAccountService {


    protected cn.ibizlab.businesscentral.core.base.service.IAccountService accountService = this;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.scheduling.service.IBookableResourceService bookableresourceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IContactService contactService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.ILeadService leadService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.marketing.service.IListAccountService listaccountService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.IOpportunityService opportunityService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ISlaKpiInstanceService slakpiinstanceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IEquipmentService equipmentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.product.service.IPriceLevelService pricelevelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IIBizServiceService ibizserviceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ISlaService slaService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITerritoryService territoryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITransactionCurrencyService transactioncurrencyService;

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.logic.IAccountActiveLogic activeLogic;

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.logic.IAccountFillContactLogic fillcontactLogic;

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.logic.IAccountInactiveLogic inactiveLogic;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(Account et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getAccountid()),et);
        return true;
    }

    @Override
    public void createBatch(List<Account> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Account et) {
        fillParentData(et);
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("accountid",et.getAccountid())))
            return false;
        CachedBeanCopier.copy(get(et.getAccountid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<Account> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Account get(String key) {
        Account et = getById(key);
        if(et==null){
            et=new Account();
            et.setAccountid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Account getDraft(Account et) {
        fillParentData(et);
        return et;
    }

    @Override
    @Transactional
    public Account active(Account et) {
        activeLogic.execute(et);
         return et ;
    }

    @Override
    @Transactional
    public Account addList(Account et) {
        //自定义代码
        return et;
    }

    @Override
    public boolean checkKey(Account et) {
        return (!ObjectUtils.isEmpty(et.getAccountid()))&&(!Objects.isNull(this.getById(et.getAccountid())));
    }
    @Override
    @Transactional
    public Account inactive(Account et) {
        inactiveLogic.execute(et);
         return et ;
    }

    @Override
    @Transactional
    public boolean save(Account et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Account et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<Account> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<Account> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Account> selectByParentaccountid(String accountid) {
        return baseMapper.selectByParentaccountid(accountid);
    }

    @Override
    public void removeByParentaccountid(String accountid) {
        this.remove(new QueryWrapper<Account>().eq("parentaccountid",accountid));
    }

	@Override
    public List<Account> selectByPrimarycontactid(String contactid) {
        return baseMapper.selectByPrimarycontactid(contactid);
    }

    @Override
    public void removeByPrimarycontactid(String contactid) {
        this.remove(new QueryWrapper<Account>().eq("primarycontactid",contactid));
    }

	@Override
    public List<Account> selectByPreferredequipmentid(String equipmentid) {
        return baseMapper.selectByPreferredequipmentid(equipmentid);
    }

    @Override
    public void removeByPreferredequipmentid(String equipmentid) {
        this.remove(new QueryWrapper<Account>().eq("preferredequipmentid",equipmentid));
    }

	@Override
    public List<Account> selectByOriginatingleadid(String leadid) {
        return baseMapper.selectByOriginatingleadid(leadid);
    }

    @Override
    public void removeByOriginatingleadid(String leadid) {
        this.remove(new QueryWrapper<Account>().eq("originatingleadid",leadid));
    }

	@Override
    public List<Account> selectByDefaultpricelevelid(String pricelevelid) {
        return baseMapper.selectByDefaultpricelevelid(pricelevelid);
    }

    @Override
    public void removeByDefaultpricelevelid(String pricelevelid) {
        this.remove(new QueryWrapper<Account>().eq("defaultpricelevelid",pricelevelid));
    }

	@Override
    public List<Account> selectByPreferredserviceid(String serviceid) {
        return baseMapper.selectByPreferredserviceid(serviceid);
    }

    @Override
    public void removeByPreferredserviceid(String serviceid) {
        this.remove(new QueryWrapper<Account>().eq("preferredserviceid",serviceid));
    }

	@Override
    public List<Account> selectBySlaid(String slaid) {
        return baseMapper.selectBySlaid(slaid);
    }

    @Override
    public void removeBySlaid(String slaid) {
        this.remove(new QueryWrapper<Account>().eq("slaid",slaid));
    }

	@Override
    public List<Account> selectByTerritoryid(String territoryid) {
        return baseMapper.selectByTerritoryid(territoryid);
    }

    @Override
    public void removeByTerritoryid(String territoryid) {
        this.remove(new QueryWrapper<Account>().eq("territoryid",territoryid));
    }

	@Override
    public List<Account> selectByTransactioncurrencyid(String transactioncurrencyid) {
        return baseMapper.selectByTransactioncurrencyid(transactioncurrencyid);
    }

    @Override
    public void removeByTransactioncurrencyid(String transactioncurrencyid) {
        this.remove(new QueryWrapper<Account>().eq("transactioncurrencyid",transactioncurrencyid));
    }


    /**
     * 查询集合 ByParentKey
     */
    @Override
    public Page<Account> searchByParentKey(AccountSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Account> pages=baseMapper.searchByParentKey(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Account>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<Account> searchDefault(AccountSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Account> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Account>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 Root
     */
    @Override
    public Page<Account> searchRoot(AccountSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Account> pages=baseMapper.searchRoot(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Account>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 停用客户
     */
    @Override
    public Page<Account> searchStop(AccountSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Account> pages=baseMapper.searchStop(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Account>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 可用客户
     */
    @Override
    public Page<Account> searchUsable(AccountSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Account> pages=baseMapper.searchUsable(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Account>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Account et){
        //实体关系[DER1N_ACCOUNT__ACCOUNT__PARENTACCOUNTID]
        if(!ObjectUtils.isEmpty(et.getParentaccountid())){
            cn.ibizlab.businesscentral.core.base.domain.Account parentaccount=et.getParentaccount();
            if(ObjectUtils.isEmpty(parentaccount)){
                cn.ibizlab.businesscentral.core.base.domain.Account majorEntity=accountService.get(et.getParentaccountid());
                et.setParentaccount(majorEntity);
                parentaccount=majorEntity;
            }
            et.setParentaccountname(parentaccount.getAccountname());
        }
        //实体关系[DER1N_ACCOUNT__CONTACT__PRIMARYCONTACTID]
        if(!ObjectUtils.isEmpty(et.getPrimarycontactid())){
            cn.ibizlab.businesscentral.core.base.domain.Contact primarycontact=et.getPrimarycontact();
            if(ObjectUtils.isEmpty(primarycontact)){
                cn.ibizlab.businesscentral.core.base.domain.Contact majorEntity=contactService.get(et.getPrimarycontactid());
                et.setPrimarycontact(majorEntity);
                primarycontact=majorEntity;
            }
            et.setPrimarycontactname(primarycontact.getFullname());
        }
        //实体关系[DER1N_ACCOUNT__EQUIPMENT__PREFERREDEQUIPMENTID]
        if(!ObjectUtils.isEmpty(et.getPreferredequipmentid())){
            cn.ibizlab.businesscentral.core.service.domain.Equipment preferredequipment=et.getPreferredequipment();
            if(ObjectUtils.isEmpty(preferredequipment)){
                cn.ibizlab.businesscentral.core.service.domain.Equipment majorEntity=equipmentService.get(et.getPreferredequipmentid());
                et.setPreferredequipment(majorEntity);
                preferredequipment=majorEntity;
            }
            et.setPreferredequipmentname(preferredequipment.getEquipmentname());
        }
        //实体关系[DER1N_ACCOUNT__LEAD__ORIGINATINGLEADID]
        if(!ObjectUtils.isEmpty(et.getOriginatingleadid())){
            cn.ibizlab.businesscentral.core.sales.domain.Lead originatinglead=et.getOriginatinglead();
            if(ObjectUtils.isEmpty(originatinglead)){
                cn.ibizlab.businesscentral.core.sales.domain.Lead majorEntity=leadService.get(et.getOriginatingleadid());
                et.setOriginatinglead(majorEntity);
                originatinglead=majorEntity;
            }
            et.setOriginatingleadname(originatinglead.getFullname());
        }
        //实体关系[DER1N_ACCOUNT__PRICELEVEL__DEFAULTPRICELEVELID]
        if(!ObjectUtils.isEmpty(et.getDefaultpricelevelid())){
            cn.ibizlab.businesscentral.core.product.domain.PriceLevel defaultpricelevel=et.getDefaultpricelevel();
            if(ObjectUtils.isEmpty(defaultpricelevel)){
                cn.ibizlab.businesscentral.core.product.domain.PriceLevel majorEntity=pricelevelService.get(et.getDefaultpricelevelid());
                et.setDefaultpricelevel(majorEntity);
                defaultpricelevel=majorEntity;
            }
            et.setDefaultpricelevelname(defaultpricelevel.getPricelevelname());
        }
        //实体关系[DER1N_ACCOUNT__SERVICE__PREFERREDSERVICEID]
        if(!ObjectUtils.isEmpty(et.getPreferredserviceid())){
            cn.ibizlab.businesscentral.core.service.domain.IBizService preferredservice=et.getPreferredservice();
            if(ObjectUtils.isEmpty(preferredservice)){
                cn.ibizlab.businesscentral.core.service.domain.IBizService majorEntity=ibizserviceService.get(et.getPreferredserviceid());
                et.setPreferredservice(majorEntity);
                preferredservice=majorEntity;
            }
            et.setPreferredservicename(preferredservice.getServicename());
        }
        //实体关系[DER1N_ACCOUNT__SLA__SLAID]
        if(!ObjectUtils.isEmpty(et.getSlaid())){
            cn.ibizlab.businesscentral.core.base.domain.Sla sla=et.getSla();
            if(ObjectUtils.isEmpty(sla)){
                cn.ibizlab.businesscentral.core.base.domain.Sla majorEntity=slaService.get(et.getSlaid());
                et.setSla(majorEntity);
                sla=majorEntity;
            }
            et.setSlaname(sla.getSlaname());
        }
        //实体关系[DER1N_ACCOUNT__TERRITORY__TERRITORYID]
        if(!ObjectUtils.isEmpty(et.getTerritoryid())){
            cn.ibizlab.businesscentral.core.base.domain.Territory territory=et.getTerritory();
            if(ObjectUtils.isEmpty(territory)){
                cn.ibizlab.businesscentral.core.base.domain.Territory majorEntity=territoryService.get(et.getTerritoryid());
                et.setTerritory(majorEntity);
                territory=majorEntity;
            }
            et.setTerritoryname(territory.getTerritoryname());
        }
        //实体关系[DER1N_ACCOUNT__TRANSACTIONCURRENCY__TRANSACTIONCURRENCYID]
        if(!ObjectUtils.isEmpty(et.getTransactioncurrencyid())){
            cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency transactioncurrency=et.getTransactioncurrency();
            if(ObjectUtils.isEmpty(transactioncurrency)){
                cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency majorEntity=transactioncurrencyService.get(et.getTransactioncurrencyid());
                et.setTransactioncurrency(majorEntity);
                transactioncurrency=majorEntity;
            }
            et.setCurrencyname(transactioncurrency.getCurrencyname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Account> getAccountByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Account> getAccountByEntities(List<Account> entities) {
        List ids =new ArrayList();
        for(Account entity : entities){
            Serializable id=entity.getAccountid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}



