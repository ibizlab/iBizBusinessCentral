package cn.ibizlab.businesscentral.core.runtime.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.runtime.domain.Expiredprocess;
import cn.ibizlab.businesscentral.core.runtime.filter.ExpiredprocessSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Expiredprocess] 服务对象接口
 */
public interface IExpiredprocessService extends IService<Expiredprocess>{

    boolean create(Expiredprocess et) ;
    void createBatch(List<Expiredprocess> list) ;
    boolean update(Expiredprocess et) ;
    void updateBatch(List<Expiredprocess> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    Expiredprocess get(String key) ;
    Expiredprocess getDraft(Expiredprocess et) ;
    boolean checkKey(Expiredprocess et) ;
    boolean save(Expiredprocess et) ;
    void saveBatch(List<Expiredprocess> list) ;
    Page<Expiredprocess> searchDefault(ExpiredprocessSearchContext context) ;
    List<Expiredprocess> selectByKnowledgearticleid(String knowledgearticleid) ;
    void removeByKnowledgearticleid(String knowledgearticleid) ;
    List<Expiredprocess> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Expiredprocess> getExpiredprocessByIds(List<String> ids) ;
    List<Expiredprocess> getExpiredprocessByEntities(List<Expiredprocess> entities) ;
}


