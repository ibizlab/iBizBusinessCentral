package cn.ibizlab.businesscentral.core.sales.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.sales.domain.Discount;
import cn.ibizlab.businesscentral.core.sales.filter.DiscountSearchContext;
import cn.ibizlab.businesscentral.core.sales.service.IDiscountService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.sales.mapper.DiscountMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[折扣] 服务对象接口实现
 */
@Slf4j
@Service("DiscountServiceImpl")
public class DiscountServiceImpl extends ServiceImpl<DiscountMapper, Discount> implements IDiscountService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.IDiscountTypeService discounttypeService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(Discount et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getDiscountid()),et);
        return true;
    }

    @Override
    public void createBatch(List<Discount> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Discount et) {
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("discountid",et.getDiscountid())))
            return false;
        CachedBeanCopier.copy(get(et.getDiscountid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<Discount> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Discount get(String key) {
        Discount et = getById(key);
        if(et==null){
            et=new Discount();
            et.setDiscountid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Discount getDraft(Discount et) {
        return et;
    }

    @Override
    public boolean checkKey(Discount et) {
        return (!ObjectUtils.isEmpty(et.getDiscountid()))&&(!Objects.isNull(this.getById(et.getDiscountid())));
    }
    @Override
    @Transactional
    public boolean save(Discount et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Discount et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<Discount> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<Discount> list) {
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Discount> selectByDiscounttypeid(String discounttypeid) {
        return baseMapper.selectByDiscounttypeid(discounttypeid);
    }

    @Override
    public void removeByDiscounttypeid(String discounttypeid) {
        this.remove(new QueryWrapper<Discount>().eq("discounttypeid",discounttypeid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<Discount> searchDefault(DiscountSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Discount> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Discount>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Discount> getDiscountByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Discount> getDiscountByEntities(List<Discount> entities) {
        List ids =new ArrayList();
        for(Discount entity : entities){
            Serializable id=entity.getDiscountid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}



