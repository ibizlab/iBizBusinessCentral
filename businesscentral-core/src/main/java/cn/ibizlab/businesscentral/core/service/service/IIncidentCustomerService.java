package cn.ibizlab.businesscentral.core.service.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.service.domain.IncidentCustomer;
import cn.ibizlab.businesscentral.core.service.filter.IncidentCustomerSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[IncidentCustomer] 服务对象接口
 */
public interface IIncidentCustomerService extends IService<IncidentCustomer>{

    boolean create(IncidentCustomer et) ;
    void createBatch(List<IncidentCustomer> list) ;
    boolean update(IncidentCustomer et) ;
    void updateBatch(List<IncidentCustomer> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    IncidentCustomer get(String key) ;
    IncidentCustomer getDraft(IncidentCustomer et) ;
    boolean checkKey(IncidentCustomer et) ;
    boolean save(IncidentCustomer et) ;
    void saveBatch(List<IncidentCustomer> list) ;
    Page<IncidentCustomer> searchDefault(IncidentCustomerSearchContext context) ;
    Page<IncidentCustomer> searchIndexDER(IncidentCustomerSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

}


