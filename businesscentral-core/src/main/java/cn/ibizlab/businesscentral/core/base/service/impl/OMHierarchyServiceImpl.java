package cn.ibizlab.businesscentral.core.base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.base.domain.OMHierarchy;
import cn.ibizlab.businesscentral.core.base.filter.OMHierarchySearchContext;
import cn.ibizlab.businesscentral.core.base.service.IOMHierarchyService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.base.mapper.OMHierarchyMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[组织层次结构] 服务对象接口实现
 */
@Slf4j
@Service("OMHierarchyServiceImpl")
public class OMHierarchyServiceImpl extends ServiceImpl<OMHierarchyMapper, OMHierarchy> implements IOMHierarchyService {


    protected cn.ibizlab.businesscentral.core.base.service.IOMHierarchyService omhierarchyService = this;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IOMHierarchyCatService omhierarchycatService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IOrganizationService organizationService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(OMHierarchy et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getOmhierarchyid()),et);
        return true;
    }

    @Override
    public void createBatch(List<OMHierarchy> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(OMHierarchy et) {
        fillParentData(et);
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("omhierarchyid",et.getOmhierarchyid())))
            return false;
        CachedBeanCopier.copy(get(et.getOmhierarchyid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<OMHierarchy> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public OMHierarchy get(String key) {
        OMHierarchy et = getById(key);
        if(et==null){
            et=new OMHierarchy();
            et.setOmhierarchyid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public OMHierarchy getDraft(OMHierarchy et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(OMHierarchy et) {
        return (!ObjectUtils.isEmpty(et.getOmhierarchyid()))&&(!Objects.isNull(this.getById(et.getOmhierarchyid())));
    }
    @Override
    @Transactional
    public boolean save(OMHierarchy et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(OMHierarchy et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<OMHierarchy> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<OMHierarchy> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<OMHierarchy> selectByOmhierarchycatid(String omhierarchycatid) {
        return baseMapper.selectByOmhierarchycatid(omhierarchycatid);
    }

    @Override
    public void removeByOmhierarchycatid(String omhierarchycatid) {
        this.remove(new QueryWrapper<OMHierarchy>().eq("omhierarchycatid",omhierarchycatid));
    }

	@Override
    public List<OMHierarchy> selectByPomhierarchyid(String omhierarchyid) {
        return baseMapper.selectByPomhierarchyid(omhierarchyid);
    }

    @Override
    public void removeByPomhierarchyid(String omhierarchyid) {
        this.remove(new QueryWrapper<OMHierarchy>().eq("pomhierarchyid",omhierarchyid));
    }

	@Override
    public List<OMHierarchy> selectByOrganizationid(String organizationid) {
        return baseMapper.selectByOrganizationid(organizationid);
    }

    @Override
    public void removeByOrganizationid(String organizationid) {
        this.remove(new QueryWrapper<OMHierarchy>().eq("organizationid",organizationid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<OMHierarchy> searchDefault(OMHierarchySearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<OMHierarchy> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<OMHierarchy>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 顶级组织
     */
    @Override
    public Page<OMHierarchy> searchRootOrg(OMHierarchySearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<OMHierarchy> pages=baseMapper.searchRootOrg(context.getPages(),context,context.getSelectCond());
        return new PageImpl<OMHierarchy>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(OMHierarchy et){
        //实体关系[DER1N_OMHIERARCHY_OMHIERARCHYCAT_OMHIERARCHYCATID]
        if(!ObjectUtils.isEmpty(et.getOmhierarchycatid())){
            cn.ibizlab.businesscentral.core.base.domain.OMHierarchyCat omhierarchycat=et.getOmhierarchycat();
            if(ObjectUtils.isEmpty(omhierarchycat)){
                cn.ibizlab.businesscentral.core.base.domain.OMHierarchyCat majorEntity=omhierarchycatService.get(et.getOmhierarchycatid());
                et.setOmhierarchycat(majorEntity);
                omhierarchycat=majorEntity;
            }
            et.setOmhierarchycatname(omhierarchycat.getOmhierarchycatname());
        }
        //实体关系[DER1N_OMHIERARCHY_OMHIERARCHY_POMHIERARCHYID]
        if(!ObjectUtils.isEmpty(et.getPomhierarchyid())){
            cn.ibizlab.businesscentral.core.base.domain.OMHierarchy pomhierarchy=et.getPomhierarchy();
            if(ObjectUtils.isEmpty(pomhierarchy)){
                cn.ibizlab.businesscentral.core.base.domain.OMHierarchy majorEntity=omhierarchyService.get(et.getPomhierarchyid());
                et.setPomhierarchy(majorEntity);
                pomhierarchy=majorEntity;
            }
            et.setPomhierarchyname(pomhierarchy.getOmhierarchyname());
        }
        //实体关系[DER1N_OMHIERARCHY_ORGANIZATION_ORGANIZATIONID]
        if(!ObjectUtils.isEmpty(et.getOrganizationid())){
            cn.ibizlab.businesscentral.core.base.domain.Organization organization=et.getOrganization();
            if(ObjectUtils.isEmpty(organization)){
                cn.ibizlab.businesscentral.core.base.domain.Organization majorEntity=organizationService.get(et.getOrganizationid());
                et.setOrganization(majorEntity);
                organization=majorEntity;
            }
            et.setShortname(organization.getShortname());
            et.setOrganizationname(organization.getOrganizationname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<OMHierarchy> getOmhierarchyByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<OMHierarchy> getOmhierarchyByEntities(List<OMHierarchy> entities) {
        List ids =new ArrayList();
        for(OMHierarchy entity : entities){
            Serializable id=entity.getOmhierarchyid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}



