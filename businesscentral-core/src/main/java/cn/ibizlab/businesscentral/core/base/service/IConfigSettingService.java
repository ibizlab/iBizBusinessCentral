package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.ConfigSetting;
import cn.ibizlab.businesscentral.core.base.filter.ConfigSettingSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[ConfigSetting] 服务对象接口
 */
public interface IConfigSettingService extends IService<ConfigSetting>{

    boolean create(ConfigSetting et) ;
    void createBatch(List<ConfigSetting> list) ;
    boolean update(ConfigSetting et) ;
    void updateBatch(List<ConfigSetting> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    ConfigSetting get(String key) ;
    ConfigSetting getDraft(ConfigSetting et) ;
    boolean checkKey(ConfigSetting et) ;
    boolean save(ConfigSetting et) ;
    void saveBatch(List<ConfigSetting> list) ;
    Page<ConfigSetting> searchDefault(ConfigSettingSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<ConfigSetting> getConfigsettingByIds(List<String> ids) ;
    List<ConfigSetting> getConfigsettingByEntities(List<ConfigSetting> entities) ;
}


