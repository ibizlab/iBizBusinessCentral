package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.SlaItem;
import cn.ibizlab.businesscentral.core.base.filter.SlaItemSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[SlaItem] 服务对象接口
 */
public interface ISlaItemService extends IService<SlaItem>{

    boolean create(SlaItem et) ;
    void createBatch(List<SlaItem> list) ;
    boolean update(SlaItem et) ;
    void updateBatch(List<SlaItem> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    SlaItem get(String key) ;
    SlaItem getDraft(SlaItem et) ;
    boolean checkKey(SlaItem et) ;
    boolean save(SlaItem et) ;
    void saveBatch(List<SlaItem> list) ;
    Page<SlaItem> searchDefault(SlaItemSearchContext context) ;
    List<SlaItem> selectBySlaid(String slaid) ;
    void removeBySlaid(String slaid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<SlaItem> getSlaitemByIds(List<String> ids) ;
    List<SlaItem> getSlaitemByEntities(List<SlaItem> entities) ;
}


