package cn.ibizlab.businesscentral.core.sales.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.sales.domain.Goal;
import cn.ibizlab.businesscentral.core.sales.filter.GoalSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface GoalMapper extends BaseMapper<Goal>{

    Page<Goal> searchDefault(IPage page, @Param("srf") GoalSearchContext context, @Param("ew") Wrapper<Goal> wrapper) ;
    Page<Goal> searchStop(IPage page, @Param("srf") GoalSearchContext context, @Param("ew") Wrapper<Goal> wrapper) ;
    Page<Goal> searchUsable(IPage page, @Param("srf") GoalSearchContext context, @Param("ew") Wrapper<Goal> wrapper) ;
    @Override
    Goal selectById(Serializable id);
    @Override
    int insert(Goal entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Goal entity);
    @Override
    int update(@Param(Constants.ENTITY) Goal entity, @Param("ew") Wrapper<Goal> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Goal> selectByRollupqueryactualdecimalid(@Param("goalrollupqueryid") Serializable goalrollupqueryid) ;

    List<Goal> selectByRollupqueryactualintegerid(@Param("goalrollupqueryid") Serializable goalrollupqueryid) ;

    List<Goal> selectByRollupqueryactualmoneyid(@Param("goalrollupqueryid") Serializable goalrollupqueryid) ;

    List<Goal> selectByRollupquerycustomdecimalid(@Param("goalrollupqueryid") Serializable goalrollupqueryid) ;

    List<Goal> selectByRollupquerycustomintegerid(@Param("goalrollupqueryid") Serializable goalrollupqueryid) ;

    List<Goal> selectByRollupquerycustommoneyid(@Param("goalrollupqueryid") Serializable goalrollupqueryid) ;

    List<Goal> selectByRollupqueryinprogressdecimalid(@Param("goalrollupqueryid") Serializable goalrollupqueryid) ;

    List<Goal> selectByRollupqueryinprogressintegerid(@Param("goalrollupqueryid") Serializable goalrollupqueryid) ;

    List<Goal> selectByRollupqueryinprogressmoneyid(@Param("goalrollupqueryid") Serializable goalrollupqueryid) ;

    List<Goal> selectByGoalwitherrorid(@Param("goalid") Serializable goalid) ;

    List<Goal> selectByParentgoalid(@Param("goalid") Serializable goalid) ;

    List<Goal> selectByMetricid(@Param("metricid") Serializable metricid) ;

}
