package cn.ibizlab.businesscentral.core.base.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.base.domain.Team;
/**
 * 关系型数据实体[Team] 查询条件对象
 */
@Slf4j
@Data
public class TeamSearchContext extends QueryWrapperContext<Team> {

	private String n_teamtype_eq;//[团队类型]
	public void setN_teamtype_eq(String n_teamtype_eq) {
        this.n_teamtype_eq = n_teamtype_eq;
        if(!ObjectUtils.isEmpty(this.n_teamtype_eq)){
            this.getSearchCond().eq("teamtype", n_teamtype_eq);
        }
    }
	private String n_teamname_like;//[团队名称]
	public void setN_teamname_like(String n_teamname_like) {
        this.n_teamname_like = n_teamname_like;
        if(!ObjectUtils.isEmpty(this.n_teamname_like)){
            this.getSearchCond().like("teamname", n_teamname_like);
        }
    }
	private String n_transactioncurrencyid_eq;//[货币]
	public void setN_transactioncurrencyid_eq(String n_transactioncurrencyid_eq) {
        this.n_transactioncurrencyid_eq = n_transactioncurrencyid_eq;
        if(!ObjectUtils.isEmpty(this.n_transactioncurrencyid_eq)){
            this.getSearchCond().eq("transactioncurrencyid", n_transactioncurrencyid_eq);
        }
    }
	private String n_businessunitid_eq;//[业务部门]
	public void setN_businessunitid_eq(String n_businessunitid_eq) {
        this.n_businessunitid_eq = n_businessunitid_eq;
        if(!ObjectUtils.isEmpty(this.n_businessunitid_eq)){
            this.getSearchCond().eq("businessunitid", n_businessunitid_eq);
        }
    }
	private String n_teamtemplateid_eq;//[团队模板标识符]
	public void setN_teamtemplateid_eq(String n_teamtemplateid_eq) {
        this.n_teamtemplateid_eq = n_teamtemplateid_eq;
        if(!ObjectUtils.isEmpty(this.n_teamtemplateid_eq)){
            this.getSearchCond().eq("teamtemplateid", n_teamtemplateid_eq);
        }
    }
	private String n_queueid_eq;//[默认队列]
	public void setN_queueid_eq(String n_queueid_eq) {
        this.n_queueid_eq = n_queueid_eq;
        if(!ObjectUtils.isEmpty(this.n_queueid_eq)){
            this.getSearchCond().eq("queueid", n_queueid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("teamname", query)   
            );
		 }
	}
}



