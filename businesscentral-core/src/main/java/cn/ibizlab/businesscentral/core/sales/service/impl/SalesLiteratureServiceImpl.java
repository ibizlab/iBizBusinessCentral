package cn.ibizlab.businesscentral.core.sales.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.sales.domain.SalesLiterature;
import cn.ibizlab.businesscentral.core.sales.filter.SalesLiteratureSearchContext;
import cn.ibizlab.businesscentral.core.sales.service.ISalesLiteratureService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.sales.mapper.SalesLiteratureMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[销售宣传资料] 服务对象接口实现
 */
@Slf4j
@Service("SalesLiteratureServiceImpl")
public class SalesLiteratureServiceImpl extends ServiceImpl<SalesLiteratureMapper, SalesLiterature> implements ISalesLiteratureService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.ICompetitorSalesLiteratureService competitorsalesliteratureService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.product.service.IProductSalesLiteratureService productsalesliteratureService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.ISalesLiteratureItemService salesliteratureitemService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ISubjectService subjectService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITransactionCurrencyService transactioncurrencyService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(SalesLiterature et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getSalesliteratureid()),et);
        return true;
    }

    @Override
    public void createBatch(List<SalesLiterature> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(SalesLiterature et) {
        fillParentData(et);
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("salesliteratureid",et.getSalesliteratureid())))
            return false;
        CachedBeanCopier.copy(get(et.getSalesliteratureid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<SalesLiterature> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public SalesLiterature get(String key) {
        SalesLiterature et = getById(key);
        if(et==null){
            et=new SalesLiterature();
            et.setSalesliteratureid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public SalesLiterature getDraft(SalesLiterature et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(SalesLiterature et) {
        return (!ObjectUtils.isEmpty(et.getSalesliteratureid()))&&(!Objects.isNull(this.getById(et.getSalesliteratureid())));
    }
    @Override
    @Transactional
    public boolean save(SalesLiterature et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(SalesLiterature et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<SalesLiterature> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<SalesLiterature> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<SalesLiterature> selectBySubjectid(String subjectid) {
        return baseMapper.selectBySubjectid(subjectid);
    }

    @Override
    public void removeBySubjectid(String subjectid) {
        this.remove(new QueryWrapper<SalesLiterature>().eq("subjectid",subjectid));
    }

	@Override
    public List<SalesLiterature> selectByTransactioncurrencyid(String transactioncurrencyid) {
        return baseMapper.selectByTransactioncurrencyid(transactioncurrencyid);
    }

    @Override
    public void removeByTransactioncurrencyid(String transactioncurrencyid) {
        this.remove(new QueryWrapper<SalesLiterature>().eq("transactioncurrencyid",transactioncurrencyid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<SalesLiterature> searchDefault(SalesLiteratureSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<SalesLiterature> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<SalesLiterature>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(SalesLiterature et){
        //实体关系[DER1N_SALESLITERATURE__SUBJECT__SUBJECTID]
        if(!ObjectUtils.isEmpty(et.getSubjectid())){
            cn.ibizlab.businesscentral.core.base.domain.Subject subject=et.getSubject();
            if(ObjectUtils.isEmpty(subject)){
                cn.ibizlab.businesscentral.core.base.domain.Subject majorEntity=subjectService.get(et.getSubjectid());
                et.setSubject(majorEntity);
                subject=majorEntity;
            }
            et.setSubjectname(subject.getTitle());
        }
        //实体关系[DER1N_SALESLITERATURE__TRANSACTIONCURRENCY__TRANSACTIONCURRENCYID]
        if(!ObjectUtils.isEmpty(et.getTransactioncurrencyid())){
            cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency transactioncurrency=et.getTransactioncurrency();
            if(ObjectUtils.isEmpty(transactioncurrency)){
                cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency majorEntity=transactioncurrencyService.get(et.getTransactioncurrencyid());
                et.setTransactioncurrency(majorEntity);
                transactioncurrency=majorEntity;
            }
            et.setCurrencyname(transactioncurrency.getCurrencyname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<SalesLiterature> getSalesliteratureByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<SalesLiterature> getSalesliteratureByEntities(List<SalesLiterature> entities) {
        List ids =new ArrayList();
        for(SalesLiterature entity : entities){
            Serializable id=entity.getSalesliteratureid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}



