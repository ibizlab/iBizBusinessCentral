package cn.ibizlab.businesscentral.core.sales.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.sales.domain.OpportunityClose;
import cn.ibizlab.businesscentral.core.sales.filter.OpportunityCloseSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[OpportunityClose] 服务对象接口
 */
public interface IOpportunityCloseService extends IService<OpportunityClose>{

    boolean create(OpportunityClose et) ;
    void createBatch(List<OpportunityClose> list) ;
    boolean update(OpportunityClose et) ;
    void updateBatch(List<OpportunityClose> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    OpportunityClose get(String key) ;
    OpportunityClose getDraft(OpportunityClose et) ;
    boolean checkKey(OpportunityClose et) ;
    boolean save(OpportunityClose et) ;
    void saveBatch(List<OpportunityClose> list) ;
    Page<OpportunityClose> searchDefault(OpportunityCloseSearchContext context) ;
    List<OpportunityClose> selectByCompetitorid(String competitorid) ;
    void removeByCompetitorid(String competitorid) ;
    List<OpportunityClose> selectByOpportunityid(String opportunityid) ;
    void removeByOpportunityid(String opportunityid) ;
    List<OpportunityClose> selectByServiceid(String serviceid) ;
    void removeByServiceid(String serviceid) ;
    List<OpportunityClose> selectBySlaid(String slaid) ;
    void removeBySlaid(String slaid) ;
    List<OpportunityClose> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<OpportunityClose> getOpportunitycloseByIds(List<String> ids) ;
    List<OpportunityClose> getOpportunitycloseByEntities(List<OpportunityClose> entities) ;
}


