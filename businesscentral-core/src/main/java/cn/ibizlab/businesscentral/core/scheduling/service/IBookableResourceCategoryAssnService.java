package cn.ibizlab.businesscentral.core.scheduling.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.scheduling.domain.BookableResourceCategoryAssn;
import cn.ibizlab.businesscentral.core.scheduling.filter.BookableResourceCategoryAssnSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[BookableResourceCategoryAssn] 服务对象接口
 */
public interface IBookableResourceCategoryAssnService extends IService<BookableResourceCategoryAssn>{

    boolean create(BookableResourceCategoryAssn et) ;
    void createBatch(List<BookableResourceCategoryAssn> list) ;
    boolean update(BookableResourceCategoryAssn et) ;
    void updateBatch(List<BookableResourceCategoryAssn> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    BookableResourceCategoryAssn get(String key) ;
    BookableResourceCategoryAssn getDraft(BookableResourceCategoryAssn et) ;
    boolean checkKey(BookableResourceCategoryAssn et) ;
    boolean save(BookableResourceCategoryAssn et) ;
    void saveBatch(List<BookableResourceCategoryAssn> list) ;
    Page<BookableResourceCategoryAssn> searchDefault(BookableResourceCategoryAssnSearchContext context) ;
    List<BookableResourceCategoryAssn> selectByResourcecategory(String bookableresourcecategoryid) ;
    void removeByResourcecategory(String bookableresourcecategoryid) ;
    List<BookableResourceCategoryAssn> selectByResource(String bookableresourceid) ;
    void removeByResource(String bookableresourceid) ;
    List<BookableResourceCategoryAssn> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<BookableResourceCategoryAssn> getBookableresourcecategoryassnByIds(List<String> ids) ;
    List<BookableResourceCategoryAssn> getBookableresourcecategoryassnByEntities(List<BookableResourceCategoryAssn> entities) ;
}


