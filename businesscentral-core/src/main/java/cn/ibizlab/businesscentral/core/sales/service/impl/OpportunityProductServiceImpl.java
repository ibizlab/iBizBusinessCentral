package cn.ibizlab.businesscentral.core.sales.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.sales.domain.OpportunityProduct;
import cn.ibizlab.businesscentral.core.sales.filter.OpportunityProductSearchContext;
import cn.ibizlab.businesscentral.core.sales.service.IOpportunityProductService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.sales.mapper.OpportunityProductMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[商机产品] 服务对象接口实现
 */
@Slf4j
@Service("OpportunityProductServiceImpl")
public class OpportunityProductServiceImpl extends ServiceImpl<OpportunityProductMapper, OpportunityProduct> implements IOpportunityProductService {


    protected cn.ibizlab.businesscentral.core.sales.service.IOpportunityProductService opportunityproductService = this;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.IOpportunityService opportunityService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.product.service.IProductService productService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITransactionCurrencyService transactioncurrencyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IUomService uomService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(OpportunityProduct et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getOpportunityproductid()),et);
        return true;
    }

    @Override
    public void createBatch(List<OpportunityProduct> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(OpportunityProduct et) {
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("opportunityproductid",et.getOpportunityproductid())))
            return false;
        CachedBeanCopier.copy(get(et.getOpportunityproductid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<OpportunityProduct> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public OpportunityProduct get(String key) {
        OpportunityProduct et = getById(key);
        if(et==null){
            et=new OpportunityProduct();
            et.setOpportunityproductid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public OpportunityProduct getDraft(OpportunityProduct et) {
        return et;
    }

    @Override
    public boolean checkKey(OpportunityProduct et) {
        return (!ObjectUtils.isEmpty(et.getOpportunityproductid()))&&(!Objects.isNull(this.getById(et.getOpportunityproductid())));
    }
    @Override
    @Transactional
    public boolean save(OpportunityProduct et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(OpportunityProduct et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<OpportunityProduct> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<OpportunityProduct> list) {
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<OpportunityProduct> selectByParentbundleidref(String opportunityproductid) {
        return baseMapper.selectByParentbundleidref(opportunityproductid);
    }

    @Override
    public void removeByParentbundleidref(String opportunityproductid) {
        this.remove(new QueryWrapper<OpportunityProduct>().eq("parentbundleidref",opportunityproductid));
    }

	@Override
    public List<OpportunityProduct> selectByOpportunityid(String opportunityid) {
        return baseMapper.selectByOpportunityid(opportunityid);
    }

    @Override
    public void removeByOpportunityid(String opportunityid) {
        this.remove(new QueryWrapper<OpportunityProduct>().eq("opportunityid",opportunityid));
    }

	@Override
    public List<OpportunityProduct> selectByProductid(String productid) {
        return baseMapper.selectByProductid(productid);
    }

    @Override
    public void removeByProductid(String productid) {
        this.remove(new QueryWrapper<OpportunityProduct>().eq("productid",productid));
    }

	@Override
    public List<OpportunityProduct> selectByTransactioncurrencyid(String transactioncurrencyid) {
        return baseMapper.selectByTransactioncurrencyid(transactioncurrencyid);
    }

    @Override
    public void removeByTransactioncurrencyid(String transactioncurrencyid) {
        this.remove(new QueryWrapper<OpportunityProduct>().eq("transactioncurrencyid",transactioncurrencyid));
    }

	@Override
    public List<OpportunityProduct> selectByUomid(String uomid) {
        return baseMapper.selectByUomid(uomid);
    }

    @Override
    public void removeByUomid(String uomid) {
        this.remove(new QueryWrapper<OpportunityProduct>().eq("uomid",uomid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<OpportunityProduct> searchDefault(OpportunityProductSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<OpportunityProduct> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<OpportunityProduct>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<OpportunityProduct> getOpportunityproductByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<OpportunityProduct> getOpportunityproductByEntities(List<OpportunityProduct> entities) {
        List ids =new ArrayList();
        for(OpportunityProduct entity : entities){
            Serializable id=entity.getOpportunityproductid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}



