package cn.ibizlab.businesscentral.core.base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.base.domain.Email;
import cn.ibizlab.businesscentral.core.base.filter.EmailSearchContext;
import cn.ibizlab.businesscentral.core.base.service.IEmailService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.base.mapper.EmailMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[电子邮件] 服务对象接口实现
 */
@Slf4j
@Service("EmailServiceImpl")
public class EmailServiceImpl extends ServiceImpl<EmailMapper, Email> implements IEmailService {


    protected cn.ibizlab.businesscentral.core.base.service.IEmailService emailService = this;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IIBizServiceService ibizserviceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ISlaService slaService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITemplateService templateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITransactionCurrencyService transactioncurrencyService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(Email et) {
        createIndexMajorEntityData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getActivityid()),et);
        return true;
    }

    @Override
    public void createBatch(List<Email> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Email et) {
        activitypointerService.update(emailInheritMapping.toActivitypointer(et));
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("activityid",et.getActivityid())))
            return false;
        CachedBeanCopier.copy(get(et.getActivityid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<Email> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        activitypointerService.remove(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Email get(String key) {
        Email et = getById(key);
        if(et==null){
            et=new Email();
            et.setActivityid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Email getDraft(Email et) {
        return et;
    }

    @Override
    public boolean checkKey(Email et) {
        return (!ObjectUtils.isEmpty(et.getActivityid()))&&(!Objects.isNull(this.getById(et.getActivityid())));
    }
    @Override
    @Transactional
    public boolean save(Email et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Email et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<Email> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<Email> list) {
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Email> selectByParentactivityid(String activityid) {
        return baseMapper.selectByParentactivityid(activityid);
    }

    @Override
    public void removeByParentactivityid(String activityid) {
        this.remove(new QueryWrapper<Email>().eq("parentactivityid",activityid));
    }

	@Override
    public List<Email> selectByServiceid(String serviceid) {
        return baseMapper.selectByServiceid(serviceid);
    }

    @Override
    public void removeByServiceid(String serviceid) {
        this.remove(new QueryWrapper<Email>().eq("serviceid",serviceid));
    }

	@Override
    public List<Email> selectBySlaid(String slaid) {
        return baseMapper.selectBySlaid(slaid);
    }

    @Override
    public void removeBySlaid(String slaid) {
        this.remove(new QueryWrapper<Email>().eq("slaid",slaid));
    }

	@Override
    public List<Email> selectByTemplateid(String templateid) {
        return baseMapper.selectByTemplateid(templateid);
    }

    @Override
    public void removeByTemplateid(String templateid) {
        this.remove(new QueryWrapper<Email>().eq("templateid",templateid));
    }

	@Override
    public List<Email> selectByTransactioncurrencyid(String transactioncurrencyid) {
        return baseMapper.selectByTransactioncurrencyid(transactioncurrencyid);
    }

    @Override
    public void removeByTransactioncurrencyid(String transactioncurrencyid) {
        this.remove(new QueryWrapper<Email>().eq("transactioncurrencyid",transactioncurrencyid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<Email> searchDefault(EmailSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Email> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Email>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }






    @Autowired
    cn.ibizlab.businesscentral.core.base.mapping.EmailInheritMapping emailInheritMapping;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IActivityPointerService activitypointerService;

    /**
     * 创建索引主实体数据
     * @param et
     */
    private void createIndexMajorEntityData(Email et){
        if(ObjectUtils.isEmpty(et.getActivityid()))
            et.setActivityid((String)et.getDefaultKey(true));
        cn.ibizlab.businesscentral.core.base.domain.ActivityPointer activitypointer =emailInheritMapping.toActivitypointer(et);
        activitypointer.set("activitytypecode","EMAIL");
        activitypointerService.create(activitypointer);
    }

    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Email> getEmailByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Email> getEmailByEntities(List<Email> entities) {
        List ids =new ArrayList();
        for(Email entity : entities){
            Serializable id=entity.getActivityid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}



