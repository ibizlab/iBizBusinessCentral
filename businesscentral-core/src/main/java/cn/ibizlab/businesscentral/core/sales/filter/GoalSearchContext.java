package cn.ibizlab.businesscentral.core.sales.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.sales.domain.Goal;
/**
 * 关系型数据实体[Goal] 查询条件对象
 */
@Slf4j
@Data
public class GoalSearchContext extends QueryWrapperContext<Goal> {

	private Integer n_statecode_eq;//[状态]
	public void setN_statecode_eq(Integer n_statecode_eq) {
        this.n_statecode_eq = n_statecode_eq;
        if(!ObjectUtils.isEmpty(this.n_statecode_eq)){
            this.getSearchCond().eq("statecode", n_statecode_eq);
        }
    }
	private Integer n_statuscode_eq;//[状态描述]
	public void setN_statuscode_eq(Integer n_statuscode_eq) {
        this.n_statuscode_eq = n_statuscode_eq;
        if(!ObjectUtils.isEmpty(this.n_statuscode_eq)){
            this.getSearchCond().eq("statuscode", n_statuscode_eq);
        }
    }
	private String n_amountdatatype_eq;//[金额数据类型]
	public void setN_amountdatatype_eq(String n_amountdatatype_eq) {
        this.n_amountdatatype_eq = n_amountdatatype_eq;
        if(!ObjectUtils.isEmpty(this.n_amountdatatype_eq)){
            this.getSearchCond().eq("amountdatatype", n_amountdatatype_eq);
        }
    }
	private String n_fiscalyear_eq;//[会计年度]
	public void setN_fiscalyear_eq(String n_fiscalyear_eq) {
        this.n_fiscalyear_eq = n_fiscalyear_eq;
        if(!ObjectUtils.isEmpty(this.n_fiscalyear_eq)){
            this.getSearchCond().eq("fiscalyear", n_fiscalyear_eq);
        }
    }
	private String n_fiscalperiod_eq;//[会计期间]
	public void setN_fiscalperiod_eq(String n_fiscalperiod_eq) {
        this.n_fiscalperiod_eq = n_fiscalperiod_eq;
        if(!ObjectUtils.isEmpty(this.n_fiscalperiod_eq)){
            this.getSearchCond().eq("fiscalperiod", n_fiscalperiod_eq);
        }
    }
	private String n_title_like;//[名称]
	public void setN_title_like(String n_title_like) {
        this.n_title_like = n_title_like;
        if(!ObjectUtils.isEmpty(this.n_title_like)){
            this.getSearchCond().like("title", n_title_like);
        }
    }
	private String n_metricname_eq;//[目标度量]
	public void setN_metricname_eq(String n_metricname_eq) {
        this.n_metricname_eq = n_metricname_eq;
        if(!ObjectUtils.isEmpty(this.n_metricname_eq)){
            this.getSearchCond().eq("metricname", n_metricname_eq);
        }
    }
	private String n_metricname_like;//[目标度量]
	public void setN_metricname_like(String n_metricname_like) {
        this.n_metricname_like = n_metricname_like;
        if(!ObjectUtils.isEmpty(this.n_metricname_like)){
            this.getSearchCond().like("metricname", n_metricname_like);
        }
    }
	private String n_parentgoalname_eq;//[上级目标]
	public void setN_parentgoalname_eq(String n_parentgoalname_eq) {
        this.n_parentgoalname_eq = n_parentgoalname_eq;
        if(!ObjectUtils.isEmpty(this.n_parentgoalname_eq)){
            this.getSearchCond().eq("parentgoalname", n_parentgoalname_eq);
        }
    }
	private String n_parentgoalname_like;//[上级目标]
	public void setN_parentgoalname_like(String n_parentgoalname_like) {
        this.n_parentgoalname_like = n_parentgoalname_like;
        if(!ObjectUtils.isEmpty(this.n_parentgoalname_like)){
            this.getSearchCond().like("parentgoalname", n_parentgoalname_like);
        }
    }
	private String n_parentgoalid_eq;//[上级目标]
	public void setN_parentgoalid_eq(String n_parentgoalid_eq) {
        this.n_parentgoalid_eq = n_parentgoalid_eq;
        if(!ObjectUtils.isEmpty(this.n_parentgoalid_eq)){
            this.getSearchCond().eq("parentgoalid", n_parentgoalid_eq);
        }
    }
	private String n_rollupqueryinprogressmoneyid_eq;//[汇总查询 - 过程值(金额)]
	public void setN_rollupqueryinprogressmoneyid_eq(String n_rollupqueryinprogressmoneyid_eq) {
        this.n_rollupqueryinprogressmoneyid_eq = n_rollupqueryinprogressmoneyid_eq;
        if(!ObjectUtils.isEmpty(this.n_rollupqueryinprogressmoneyid_eq)){
            this.getSearchCond().eq("rollupqueryinprogressmoneyid", n_rollupqueryinprogressmoneyid_eq);
        }
    }
	private String n_rollupquerycustommoneyid_eq;//[汇总查询 - 自定义汇总字段(金额)]
	public void setN_rollupquerycustommoneyid_eq(String n_rollupquerycustommoneyid_eq) {
        this.n_rollupquerycustommoneyid_eq = n_rollupquerycustommoneyid_eq;
        if(!ObjectUtils.isEmpty(this.n_rollupquerycustommoneyid_eq)){
            this.getSearchCond().eq("rollupquerycustommoneyid", n_rollupquerycustommoneyid_eq);
        }
    }
	private String n_rollupqueryinprogressintegerid_eq;//[汇总查询 - 过程值(整数)]
	public void setN_rollupqueryinprogressintegerid_eq(String n_rollupqueryinprogressintegerid_eq) {
        this.n_rollupqueryinprogressintegerid_eq = n_rollupqueryinprogressintegerid_eq;
        if(!ObjectUtils.isEmpty(this.n_rollupqueryinprogressintegerid_eq)){
            this.getSearchCond().eq("rollupqueryinprogressintegerid", n_rollupqueryinprogressintegerid_eq);
        }
    }
	private String n_rollupquerycustomdecimalid_eq;//[汇总查询 - 自定义汇总字段(十进制)]
	public void setN_rollupquerycustomdecimalid_eq(String n_rollupquerycustomdecimalid_eq) {
        this.n_rollupquerycustomdecimalid_eq = n_rollupquerycustomdecimalid_eq;
        if(!ObjectUtils.isEmpty(this.n_rollupquerycustomdecimalid_eq)){
            this.getSearchCond().eq("rollupquerycustomdecimalid", n_rollupquerycustomdecimalid_eq);
        }
    }
	private String n_rollupqueryactualmoneyid_eq;//[汇总查询 - 实际值(金额)]
	public void setN_rollupqueryactualmoneyid_eq(String n_rollupqueryactualmoneyid_eq) {
        this.n_rollupqueryactualmoneyid_eq = n_rollupqueryactualmoneyid_eq;
        if(!ObjectUtils.isEmpty(this.n_rollupqueryactualmoneyid_eq)){
            this.getSearchCond().eq("rollupqueryactualmoneyid", n_rollupqueryactualmoneyid_eq);
        }
    }
	private String n_rollupquerycustomintegerid_eq;//[汇总查询 - 自定义汇总字段(整数)]
	public void setN_rollupquerycustomintegerid_eq(String n_rollupquerycustomintegerid_eq) {
        this.n_rollupquerycustomintegerid_eq = n_rollupquerycustomintegerid_eq;
        if(!ObjectUtils.isEmpty(this.n_rollupquerycustomintegerid_eq)){
            this.getSearchCond().eq("rollupquerycustomintegerid", n_rollupquerycustomintegerid_eq);
        }
    }
	private String n_goalwitherrorid_eq;//[出现错误的目标]
	public void setN_goalwitherrorid_eq(String n_goalwitherrorid_eq) {
        this.n_goalwitherrorid_eq = n_goalwitherrorid_eq;
        if(!ObjectUtils.isEmpty(this.n_goalwitherrorid_eq)){
            this.getSearchCond().eq("goalwitherrorid", n_goalwitherrorid_eq);
        }
    }
	private String n_metricid_eq;//[目标度量]
	public void setN_metricid_eq(String n_metricid_eq) {
        this.n_metricid_eq = n_metricid_eq;
        if(!ObjectUtils.isEmpty(this.n_metricid_eq)){
            this.getSearchCond().eq("metricid", n_metricid_eq);
        }
    }
	private String n_rollupqueryinprogressdecimalid_eq;//[汇总查询 - 过程值(十进制)]
	public void setN_rollupqueryinprogressdecimalid_eq(String n_rollupqueryinprogressdecimalid_eq) {
        this.n_rollupqueryinprogressdecimalid_eq = n_rollupqueryinprogressdecimalid_eq;
        if(!ObjectUtils.isEmpty(this.n_rollupqueryinprogressdecimalid_eq)){
            this.getSearchCond().eq("rollupqueryinprogressdecimalid", n_rollupqueryinprogressdecimalid_eq);
        }
    }
	private String n_rollupqueryactualintegerid_eq;//[汇总查询 - 实际值(整数)]
	public void setN_rollupqueryactualintegerid_eq(String n_rollupqueryactualintegerid_eq) {
        this.n_rollupqueryactualintegerid_eq = n_rollupqueryactualintegerid_eq;
        if(!ObjectUtils.isEmpty(this.n_rollupqueryactualintegerid_eq)){
            this.getSearchCond().eq("rollupqueryactualintegerid", n_rollupqueryactualintegerid_eq);
        }
    }
	private String n_rollupqueryactualdecimalid_eq;//[汇总查询 - 实际值(十进制)]
	public void setN_rollupqueryactualdecimalid_eq(String n_rollupqueryactualdecimalid_eq) {
        this.n_rollupqueryactualdecimalid_eq = n_rollupqueryactualdecimalid_eq;
        if(!ObjectUtils.isEmpty(this.n_rollupqueryactualdecimalid_eq)){
            this.getSearchCond().eq("rollupqueryactualdecimalid", n_rollupqueryactualdecimalid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("title", query)   
            );
		 }
	}
}



