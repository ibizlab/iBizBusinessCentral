package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.Organization;
import cn.ibizlab.businesscentral.core.base.filter.OrganizationSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Organization] 服务对象接口
 */
public interface IOrganizationService extends IService<Organization>{

    boolean create(Organization et) ;
    void createBatch(List<Organization> list) ;
    boolean update(Organization et) ;
    void updateBatch(List<Organization> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    Organization get(String key) ;
    Organization getDraft(Organization et) ;
    boolean checkKey(Organization et) ;
    boolean save(Organization et) ;
    void saveBatch(List<Organization> list) ;
    Page<Organization> searchDefault(OrganizationSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Organization> getOrganizationByIds(List<String> ids) ;
    List<Organization> getOrganizationByEntities(List<Organization> entities) ;
}


