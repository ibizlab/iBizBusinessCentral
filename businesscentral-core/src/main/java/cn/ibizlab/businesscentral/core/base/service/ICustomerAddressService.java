package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.CustomerAddress;
import cn.ibizlab.businesscentral.core.base.filter.CustomerAddressSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[CustomerAddress] 服务对象接口
 */
public interface ICustomerAddressService extends IService<CustomerAddress>{

    boolean create(CustomerAddress et) ;
    void createBatch(List<CustomerAddress> list) ;
    boolean update(CustomerAddress et) ;
    void updateBatch(List<CustomerAddress> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    CustomerAddress get(String key) ;
    CustomerAddress getDraft(CustomerAddress et) ;
    boolean checkKey(CustomerAddress et) ;
    boolean save(CustomerAddress et) ;
    void saveBatch(List<CustomerAddress> list) ;
    Page<CustomerAddress> searchDefault(CustomerAddressSearchContext context) ;
    List<CustomerAddress> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<CustomerAddress> getCustomeraddressByIds(List<String> ids) ;
    List<CustomerAddress> getCustomeraddressByEntities(List<CustomerAddress> entities) ;
}


