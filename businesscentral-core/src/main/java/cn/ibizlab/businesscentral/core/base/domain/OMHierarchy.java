package cn.ibizlab.businesscentral.core.base.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[组织层次结构]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "OMHIERARCHY",resultMap = "OMHierarchyResultMap")
public class OMHierarchy extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 组织层次结构标识
     */
    @DEField(isKeyField=true)
    @TableId(value= "omhierarchyid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "omhierarchyid")
    @JsonProperty("omhierarchyid")
    private String omhierarchyid;
    /**
     * 组织层次结构名称
     */
    @TableField(value = "omhierarchyname")
    @JSONField(name = "omhierarchyname")
    @JsonProperty("omhierarchyname")
    private String omhierarchyname;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 组织
     */
    @TableField(value = "organizationid")
    @JSONField(name = "organizationid")
    @JsonProperty("organizationid")
    private String organizationid;
    /**
     * 父组织层次结构标识
     */
    @TableField(value = "pomhierarchyid")
    @JSONField(name = "pomhierarchyid")
    @JsonProperty("pomhierarchyid")
    private String pomhierarchyid;
    /**
     * 结构层次类别标识
     */
    @TableField(value = "omhierarchycatid")
    @JSONField(name = "omhierarchycatid")
    @JsonProperty("omhierarchycatid")
    private String omhierarchycatid;
    /**
     * 是否有效
     */
    @DEField(defaultValue = "1")
    @TableField(value = "valid")
    @JSONField(name = "valid")
    @JsonProperty("valid")
    private Integer valid;
    /**
     * 显示名称
     */
    @TableField(value = "displayname")
    @JSONField(name = "displayname")
    @JsonProperty("displayname")
    private String displayname;
    /**
     * 序号
     */
    @TableField(value = "sn")
    @JSONField(name = "sn")
    @JsonProperty("sn")
    private Integer sn;
    /**
     * 代码
     */
    @TableField(value = "codevalue")
    @JSONField(name = "codevalue")
    @JsonProperty("codevalue")
    private String codevalue;
    /**
     * 排序号
     */
    @TableField(value = "showorder")
    @JSONField(name = "showorder")
    @JsonProperty("showorder")
    private BigInteger showorder;
    /**
     * 组织简称
     */
    @TableField(exist = false)
    @JSONField(name = "shortname")
    @JsonProperty("shortname")
    private String shortname;
    /**
     * 组织
     */
    @TableField(exist = false)
    @JSONField(name = "organizationname")
    @JsonProperty("organizationname")
    private String organizationname;
    /**
     * 层次类别
     */
    @TableField(exist = false)
    @JSONField(name = "omhierarchycatname")
    @JsonProperty("omhierarchycatname")
    private String omhierarchycatname;
    /**
     * 上级组织
     */
    @TableField(exist = false)
    @JSONField(name = "pomhierarchyname")
    @JsonProperty("pomhierarchyname")
    private String pomhierarchyname;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.OMHierarchyCat omhierarchycat;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.OMHierarchy pomhierarchy;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.Organization organization;



    /**
     * 设置 [组织层次结构名称]
     */
    public void setOmhierarchyname(String omhierarchyname){
        this.omhierarchyname = omhierarchyname ;
        this.modify("omhierarchyname",omhierarchyname);
    }

    /**
     * 设置 [组织]
     */
    public void setOrganizationid(String organizationid){
        this.organizationid = organizationid ;
        this.modify("organizationid",organizationid);
    }

    /**
     * 设置 [父组织层次结构标识]
     */
    public void setPomhierarchyid(String pomhierarchyid){
        this.pomhierarchyid = pomhierarchyid ;
        this.modify("pomhierarchyid",pomhierarchyid);
    }

    /**
     * 设置 [结构层次类别标识]
     */
    public void setOmhierarchycatid(String omhierarchycatid){
        this.omhierarchycatid = omhierarchycatid ;
        this.modify("omhierarchycatid",omhierarchycatid);
    }

    /**
     * 设置 [是否有效]
     */
    public void setValid(Integer valid){
        this.valid = valid ;
        this.modify("valid",valid);
    }

    /**
     * 设置 [显示名称]
     */
    public void setDisplayname(String displayname){
        this.displayname = displayname ;
        this.modify("displayname",displayname);
    }

    /**
     * 设置 [序号]
     */
    public void setSn(Integer sn){
        this.sn = sn ;
        this.modify("sn",sn);
    }

    /**
     * 设置 [代码]
     */
    public void setCodevalue(String codevalue){
        this.codevalue = codevalue ;
        this.modify("codevalue",codevalue);
    }

    /**
     * 设置 [排序号]
     */
    public void setShoworder(BigInteger showorder){
        this.showorder = showorder ;
        this.modify("showorder",showorder);
    }


}


