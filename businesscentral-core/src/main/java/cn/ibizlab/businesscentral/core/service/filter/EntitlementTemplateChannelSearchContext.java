package cn.ibizlab.businesscentral.core.service.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.service.domain.EntitlementTemplateChannel;
/**
 * 关系型数据实体[EntitlementTemplateChannel] 查询条件对象
 */
@Slf4j
@Data
public class EntitlementTemplateChannelSearchContext extends QueryWrapperContext<EntitlementTemplateChannel> {

	private String n_entitlementtemplatechannelname_like;//[授权模板渠道名称]
	public void setN_entitlementtemplatechannelname_like(String n_entitlementtemplatechannelname_like) {
        this.n_entitlementtemplatechannelname_like = n_entitlementtemplatechannelname_like;
        if(!ObjectUtils.isEmpty(this.n_entitlementtemplatechannelname_like)){
            this.getSearchCond().like("entitlementtemplatechannelname", n_entitlementtemplatechannelname_like);
        }
    }
	private String n_channel_eq;//[名称]
	public void setN_channel_eq(String n_channel_eq) {
        this.n_channel_eq = n_channel_eq;
        if(!ObjectUtils.isEmpty(this.n_channel_eq)){
            this.getSearchCond().eq("channel", n_channel_eq);
        }
    }
	private String n_transactioncurrencyid_eq;//[货币]
	public void setN_transactioncurrencyid_eq(String n_transactioncurrencyid_eq) {
        this.n_transactioncurrencyid_eq = n_transactioncurrencyid_eq;
        if(!ObjectUtils.isEmpty(this.n_transactioncurrencyid_eq)){
            this.getSearchCond().eq("transactioncurrencyid", n_transactioncurrencyid_eq);
        }
    }
	private String n_entitlementtemplateid_eq;//[权利模板 ID]
	public void setN_entitlementtemplateid_eq(String n_entitlementtemplateid_eq) {
        this.n_entitlementtemplateid_eq = n_entitlementtemplateid_eq;
        if(!ObjectUtils.isEmpty(this.n_entitlementtemplateid_eq)){
            this.getSearchCond().eq("entitlementtemplateid", n_entitlementtemplateid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("entitlementtemplatechannelname", query)   
            );
		 }
	}
}



