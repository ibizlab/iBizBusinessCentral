package cn.ibizlab.businesscentral.core.base.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[运营单位]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "OPERATIONUNIT",resultMap = "OperationUnitResultMap")
public class OperationUnit extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 运营单位标识
     */
    @DEField(isKeyField=true)
    @TableId(value= "operationunitid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "operationunitid")
    @JsonProperty("operationunitid")
    private String operationunitid;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 运营单位名称
     */
    @TableField(value = "operationunitname")
    @JSONField(name = "operationunitname")
    @JsonProperty("operationunitname")
    private String operationunitname;
    /**
     * 组织类型
     */
    @TableField(exist = false)
    @JSONField(name = "organizationtype")
    @JsonProperty("organizationtype")
    private String organizationtype;
    /**
     * 排序号
     */
    @TableField(exist = false)
    @JSONField(name = "showorder")
    @JsonProperty("showorder")
    private BigInteger showorder;
    /**
     * 组织层级
     */
    @TableField(exist = false)
    @JSONField(name = "orglevel")
    @JsonProperty("orglevel")
    private BigInteger orglevel;
    /**
     * 组织简称
     */
    @TableField(exist = false)
    @JSONField(name = "shortname")
    @JsonProperty("shortname")
    private String shortname;
    /**
     * 组织编码
     */
    @TableField(exist = false)
    @JSONField(name = "orgcode")
    @JsonProperty("orgcode")
    private String orgcode;
    /**
     * 运营单位类型
     */
    @TableField(value = "operationunittype")
    @JSONField(name = "operationunittype")
    @JsonProperty("operationunittype")
    private String operationunittype;



    /**
     * 设置 [运营单位名称]
     */
    public void setOperationunitname(String operationunitname){
        this.operationunitname = operationunitname ;
        this.modify("operationunitname",operationunitname);
    }

    /**
     * 设置 [运营单位类型]
     */
    public void setOperationunittype(String operationunittype){
        this.operationunittype = operationunittype ;
        this.modify("operationunittype",operationunittype);
    }


}


