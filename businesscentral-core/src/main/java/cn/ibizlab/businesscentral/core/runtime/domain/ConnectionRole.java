package cn.ibizlab.businesscentral.core.runtime.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[连接角色]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "CONNECTIONROLE",resultMap = "ConnectionRoleResultMap")
public class ConnectionRole extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 替代时间
     */
    @TableField(value = "overwritetime")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overwritetime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overwritetime")
    private Timestamp overwritetime;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 状态
     */
    @DEField(defaultValue = "0")
    @TableField(value = "managed")
    @JSONField(name = "managed")
    @JsonProperty("managed")
    private Integer managed;
    /**
     * 解决方案
     */
    @TableField(value = "supportingsolutionid")
    @JSONField(name = "supportingsolutionid")
    @JsonProperty("supportingsolutionid")
    private String supportingsolutionid;
    /**
     * 组件状态
     */
    @TableField(value = "componentstate")
    @JSONField(name = "componentstate")
    @JsonProperty("componentstate")
    private String componentstate;
    /**
     * 连接角色类别
     */
    @TableField(value = "category")
    @JSONField(name = "category")
    @JsonProperty("category")
    private String category;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 导入序列号
     */
    @TableField(value = "importsequencenumber")
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;
    /**
     * 关联角色名称
     */
    @TableField(value = "connectionrolename")
    @JSONField(name = "connectionrolename")
    @JsonProperty("connectionrolename")
    private String connectionrolename;
    /**
     * 状态描述
     */
    @TableField(value = "statuscode")
    @JSONField(name = "statuscode")
    @JsonProperty("statuscode")
    private Integer statuscode;
    /**
     * 连接角色
     */
    @DEField(isKeyField=true)
    @TableId(value= "connectionroleid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "connectionroleid")
    @JsonProperty("connectionroleid")
    private String connectionroleid;
    /**
     * 唯一 ID
     */
    @TableField(value = "connectionroleidunique")
    @JSONField(name = "connectionroleidunique")
    @JsonProperty("connectionroleidunique")
    private String connectionroleidunique;
    /**
     * 版本号
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 解决方案
     */
    @TableField(value = "solutionid")
    @JSONField(name = "solutionid")
    @JsonProperty("solutionid")
    private String solutionid;
    /**
     * 说明
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 可自定义
     */
    @TableField(value = "customizable")
    @JSONField(name = "customizable")
    @JsonProperty("customizable")
    private String customizable;
    /**
     * 引入的版本
     */
    @TableField(value = "introducedversion")
    @JSONField(name = "introducedversion")
    @JsonProperty("introducedversion")
    private String introducedversion;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 状态
     */
    @TableField(value = "statecode")
    @JSONField(name = "statecode")
    @JsonProperty("statecode")
    private Integer statecode;



    /**
     * 设置 [替代时间]
     */
    public void setOverwritetime(Timestamp overwritetime){
        this.overwritetime = overwritetime ;
        this.modify("overwritetime",overwritetime);
    }

    /**
     * 格式化日期 [替代时间]
     */
    public String formatOverwritetime(){
        if (this.overwritetime == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(overwritetime);
    }
    /**
     * 设置 [状态]
     */
    public void setManaged(Integer managed){
        this.managed = managed ;
        this.modify("managed",managed);
    }

    /**
     * 设置 [解决方案]
     */
    public void setSupportingsolutionid(String supportingsolutionid){
        this.supportingsolutionid = supportingsolutionid ;
        this.modify("supportingsolutionid",supportingsolutionid);
    }

    /**
     * 设置 [组件状态]
     */
    public void setComponentstate(String componentstate){
        this.componentstate = componentstate ;
        this.modify("componentstate",componentstate);
    }

    /**
     * 设置 [连接角色类别]
     */
    public void setCategory(String category){
        this.category = category ;
        this.modify("category",category);
    }

    /**
     * 设置 [导入序列号]
     */
    public void setImportsequencenumber(Integer importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [关联角色名称]
     */
    public void setConnectionrolename(String connectionrolename){
        this.connectionrolename = connectionrolename ;
        this.modify("connectionrolename",connectionrolename);
    }

    /**
     * 设置 [状态描述]
     */
    public void setStatuscode(Integer statuscode){
        this.statuscode = statuscode ;
        this.modify("statuscode",statuscode);
    }

    /**
     * 设置 [唯一 ID]
     */
    public void setConnectionroleidunique(String connectionroleidunique){
        this.connectionroleidunique = connectionroleidunique ;
        this.modify("connectionroleidunique",connectionroleidunique);
    }

    /**
     * 设置 [版本号]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [解决方案]
     */
    public void setSolutionid(String solutionid){
        this.solutionid = solutionid ;
        this.modify("solutionid",solutionid);
    }

    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [可自定义]
     */
    public void setCustomizable(String customizable){
        this.customizable = customizable ;
        this.modify("customizable",customizable);
    }

    /**
     * 设置 [引入的版本]
     */
    public void setIntroducedversion(String introducedversion){
        this.introducedversion = introducedversion ;
        this.modify("introducedversion",introducedversion);
    }

    /**
     * 设置 [状态]
     */
    public void setStatecode(Integer statecode){
        this.statecode = statecode ;
        this.modify("statecode",statecode);
    }


}


