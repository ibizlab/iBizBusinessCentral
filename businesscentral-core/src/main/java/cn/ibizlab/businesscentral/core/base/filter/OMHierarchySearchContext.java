package cn.ibizlab.businesscentral.core.base.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.base.domain.OMHierarchy;
/**
 * 关系型数据实体[OMHierarchy] 查询条件对象
 */
@Slf4j
@Data
public class OMHierarchySearchContext extends QueryWrapperContext<OMHierarchy> {

	private String n_omhierarchyname_like;//[组织层次结构名称]
	public void setN_omhierarchyname_like(String n_omhierarchyname_like) {
        this.n_omhierarchyname_like = n_omhierarchyname_like;
        if(!ObjectUtils.isEmpty(this.n_omhierarchyname_like)){
            this.getSearchCond().like("omhierarchyname", n_omhierarchyname_like);
        }
    }
	private String n_organizationid_eq;//[组织]
	public void setN_organizationid_eq(String n_organizationid_eq) {
        this.n_organizationid_eq = n_organizationid_eq;
        if(!ObjectUtils.isEmpty(this.n_organizationid_eq)){
            this.getSearchCond().eq("organizationid", n_organizationid_eq);
        }
    }
	private String n_pomhierarchyid_eq;//[父组织层次结构标识]
	public void setN_pomhierarchyid_eq(String n_pomhierarchyid_eq) {
        this.n_pomhierarchyid_eq = n_pomhierarchyid_eq;
        if(!ObjectUtils.isEmpty(this.n_pomhierarchyid_eq)){
            this.getSearchCond().eq("pomhierarchyid", n_pomhierarchyid_eq);
        }
    }
	private String n_omhierarchycatid_eq;//[结构层次类别标识]
	public void setN_omhierarchycatid_eq(String n_omhierarchycatid_eq) {
        this.n_omhierarchycatid_eq = n_omhierarchycatid_eq;
        if(!ObjectUtils.isEmpty(this.n_omhierarchycatid_eq)){
            this.getSearchCond().eq("omhierarchycatid", n_omhierarchycatid_eq);
        }
    }
	private String n_organizationname_eq;//[组织]
	public void setN_organizationname_eq(String n_organizationname_eq) {
        this.n_organizationname_eq = n_organizationname_eq;
        if(!ObjectUtils.isEmpty(this.n_organizationname_eq)){
            this.getSearchCond().eq("organizationname", n_organizationname_eq);
        }
    }
	private String n_organizationname_like;//[组织]
	public void setN_organizationname_like(String n_organizationname_like) {
        this.n_organizationname_like = n_organizationname_like;
        if(!ObjectUtils.isEmpty(this.n_organizationname_like)){
            this.getSearchCond().like("organizationname", n_organizationname_like);
        }
    }
	private String n_omhierarchycatname_eq;//[层次类别]
	public void setN_omhierarchycatname_eq(String n_omhierarchycatname_eq) {
        this.n_omhierarchycatname_eq = n_omhierarchycatname_eq;
        if(!ObjectUtils.isEmpty(this.n_omhierarchycatname_eq)){
            this.getSearchCond().eq("omhierarchycatname", n_omhierarchycatname_eq);
        }
    }
	private String n_omhierarchycatname_like;//[层次类别]
	public void setN_omhierarchycatname_like(String n_omhierarchycatname_like) {
        this.n_omhierarchycatname_like = n_omhierarchycatname_like;
        if(!ObjectUtils.isEmpty(this.n_omhierarchycatname_like)){
            this.getSearchCond().like("omhierarchycatname", n_omhierarchycatname_like);
        }
    }
	private String n_pomhierarchyname_eq;//[上级组织]
	public void setN_pomhierarchyname_eq(String n_pomhierarchyname_eq) {
        this.n_pomhierarchyname_eq = n_pomhierarchyname_eq;
        if(!ObjectUtils.isEmpty(this.n_pomhierarchyname_eq)){
            this.getSearchCond().eq("pomhierarchyname", n_pomhierarchyname_eq);
        }
    }
	private String n_pomhierarchyname_like;//[上级组织]
	public void setN_pomhierarchyname_like(String n_pomhierarchyname_like) {
        this.n_pomhierarchyname_like = n_pomhierarchyname_like;
        if(!ObjectUtils.isEmpty(this.n_pomhierarchyname_like)){
            this.getSearchCond().like("pomhierarchyname", n_pomhierarchyname_like);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("omhierarchyname", query)   
            );
		 }
	}
}



