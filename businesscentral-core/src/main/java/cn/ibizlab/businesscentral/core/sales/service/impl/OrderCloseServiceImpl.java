package cn.ibizlab.businesscentral.core.sales.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.sales.domain.OrderClose;
import cn.ibizlab.businesscentral.core.sales.filter.OrderCloseSearchContext;
import cn.ibizlab.businesscentral.core.sales.service.IOrderCloseService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.sales.mapper.OrderCloseMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[订单结束] 服务对象接口实现
 */
@Slf4j
@Service("OrderCloseServiceImpl")
public class OrderCloseServiceImpl extends ServiceImpl<OrderCloseMapper, OrderClose> implements IOrderCloseService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.ISalesOrderService salesorderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IIBizServiceService ibizserviceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ISlaService slaService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITransactionCurrencyService transactioncurrencyService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(OrderClose et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getActivityid()),et);
        return true;
    }

    @Override
    public void createBatch(List<OrderClose> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(OrderClose et) {
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("activityid",et.getActivityid())))
            return false;
        CachedBeanCopier.copy(get(et.getActivityid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<OrderClose> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public OrderClose get(String key) {
        OrderClose et = getById(key);
        if(et==null){
            et=new OrderClose();
            et.setActivityid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public OrderClose getDraft(OrderClose et) {
        return et;
    }

    @Override
    public boolean checkKey(OrderClose et) {
        return (!ObjectUtils.isEmpty(et.getActivityid()))&&(!Objects.isNull(this.getById(et.getActivityid())));
    }
    @Override
    @Transactional
    public boolean save(OrderClose et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(OrderClose et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<OrderClose> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<OrderClose> list) {
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<OrderClose> selectBySalesorderid(String salesorderid) {
        return baseMapper.selectBySalesorderid(salesorderid);
    }

    @Override
    public void removeBySalesorderid(String salesorderid) {
        this.remove(new QueryWrapper<OrderClose>().eq("salesorderid",salesorderid));
    }

	@Override
    public List<OrderClose> selectByServiceid(String serviceid) {
        return baseMapper.selectByServiceid(serviceid);
    }

    @Override
    public void removeByServiceid(String serviceid) {
        this.remove(new QueryWrapper<OrderClose>().eq("serviceid",serviceid));
    }

	@Override
    public List<OrderClose> selectBySlaid(String slaid) {
        return baseMapper.selectBySlaid(slaid);
    }

    @Override
    public void removeBySlaid(String slaid) {
        this.remove(new QueryWrapper<OrderClose>().eq("slaid",slaid));
    }

	@Override
    public List<OrderClose> selectByTransactioncurrencyid(String transactioncurrencyid) {
        return baseMapper.selectByTransactioncurrencyid(transactioncurrencyid);
    }

    @Override
    public void removeByTransactioncurrencyid(String transactioncurrencyid) {
        this.remove(new QueryWrapper<OrderClose>().eq("transactioncurrencyid",transactioncurrencyid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<OrderClose> searchDefault(OrderCloseSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<OrderClose> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<OrderClose>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<OrderClose> getOrdercloseByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<OrderClose> getOrdercloseByEntities(List<OrderClose> entities) {
        List ids =new ArrayList();
        for(OrderClose entity : entities){
            Serializable id=entity.getActivityid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}



