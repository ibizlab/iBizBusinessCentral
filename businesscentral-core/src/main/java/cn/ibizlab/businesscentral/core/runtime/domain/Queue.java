package cn.ibizlab.businesscentral.core.runtime.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[队列]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "QUEUE",resultMap = "QueueResultMap")
public class Queue extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 负责人
     */
    @TableField(value = "ownerid")
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;
    /**
     * EntityImage_URL
     */
    @DEField(name = "entityimage_url")
    @TableField(value = "entityimage_url")
    @JSONField(name = "entityimage_url")
    @JsonProperty("entityimage_url")
    private String entityimageUrl;
    /**
     * 传入电子邮件
     */
    @TableField(value = "emailaddress")
    @JSONField(name = "emailaddress")
    @JsonProperty("emailaddress")
    private String emailaddress;
    /**
     * 负责人(已弃用)
     */
    @TableField(value = "primaryuserid")
    @JSONField(name = "primaryuserid")
    @JsonProperty("primaryuserid")
    private String primaryuserid;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 汇率
     */
    @TableField(value = "exchangerate")
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;
    /**
     * 接收电子邮件的传送方式
     */
    @TableField(value = "incomingemaildeliverymethod")
    @JSONField(name = "incomingemaildeliverymethod")
    @JsonProperty("incomingemaildeliverymethod")
    private String incomingemaildeliverymethod;
    /**
     * 队列
     */
    @DEField(isKeyField=true)
    @TableId(value= "queueid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "queueid")
    @JsonProperty("queueid")
    private String queueid;
    /**
     * 说明
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 创建记录的时间
     */
    @TableField(value = "overriddencreatedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;
    /**
     * 传真队列
     */
    @DEField(defaultValue = "0")
    @TableField(value = "faxqueue")
    @JSONField(name = "faxqueue")
    @JsonProperty("faxqueue")
    private Integer faxqueue;
    /**
     * 负责人
     */
    @TableField(value = "ownername")
    @JSONField(name = "ownername")
    @JsonProperty("ownername")
    private String ownername;
    /**
     * 队列类型
     */
    @TableField(value = "queuetypecode")
    @JSONField(name = "queuetypecode")
    @JsonProperty("queuetypecode")
    private String queuetypecode;
    /**
     * 将传入电子邮件转换为活动
     */
    @TableField(value = "incomingemailfilteringmethod")
    @JSONField(name = "incomingemailfilteringmethod")
    @JsonProperty("incomingemailfilteringmethod")
    private String incomingemailfilteringmethod;
    /**
     * 成员数
     */
    @TableField(value = "numberofmembers")
    @JSONField(name = "numberofmembers")
    @JsonProperty("numberofmembers")
    private Integer numberofmembers;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 导入序列号
     */
    @TableField(value = "importsequencenumber")
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;
    /**
     * 队列名称
     */
    @TableField(value = "queuename")
    @JSONField(name = "queuename")
    @JsonProperty("queuename")
    private String queuename;
    /**
     * 版本号
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * EntityImage_Timestamp
     */
    @DEField(name = "entityimage_timestamp")
    @TableField(value = "entityimage_timestamp")
    @JSONField(name = "entityimage_timestamp")
    @JsonProperty("entityimage_timestamp")
    private BigInteger entityimageTimestamp;
    /**
     * 密码(已过时)
     */
    @TableField(value = "emailpassword")
    @JSONField(name = "emailpassword")
    @JsonProperty("emailpassword")
    private String emailpassword;
    /**
     * 允许使用凭据进行电子邮件处理(已过时)
     */
    @DEField(defaultValue = "0")
    @TableField(value = "allowemailcredentials")
    @JSONField(name = "allowemailcredentials")
    @JsonProperty("allowemailcredentials")
    private Integer allowemailcredentials;
    /**
     * 类型
     */
    @TableField(value = "queueviewtype")
    @JSONField(name = "queueviewtype")
    @JsonProperty("queueviewtype")
    private String queueviewtype;
    /**
     * 状态
     */
    @TableField(value = "statecode")
    @JSONField(name = "statecode")
    @JsonProperty("statecode")
    private Integer statecode;
    /**
     * 用户名(已过时)
     */
    @TableField(value = "emailusername")
    @JSONField(name = "emailusername")
    @JsonProperty("emailusername")
    private String emailusername;
    /**
     * 实体图像 ID
     */
    @TableField(value = "entityimageid")
    @JSONField(name = "entityimageid")
    @JsonProperty("entityimageid")
    private String entityimageid;
    /**
     * 负责人类型
     */
    @TableField(value = "ownertype")
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;
    /**
     * 队列项
     */
    @TableField(value = "numberofitems")
    @JSONField(name = "numberofitems")
    @JsonProperty("numberofitems")
    private Integer numberofitems;
    /**
     * 用户
     */
    @TableField(value = "primaryusername")
    @JSONField(name = "primaryusername")
    @JsonProperty("primaryusername")
    private String primaryusername;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 转换为电子邮件活动
     */
    @DEField(defaultValue = "0")
    @TableField(value = "ignoreunsolicitedemail")
    @JSONField(name = "ignoreunsolicitedemail")
    @JsonProperty("ignoreunsolicitedemail")
    private Integer ignoreunsolicitedemail;
    /**
     * 状态描述
     */
    @TableField(value = "statuscode")
    @JSONField(name = "statuscode")
    @JsonProperty("statuscode")
    private Integer statuscode;
    /**
     * DefaultMailboxName
     */
    @TableField(value = "defaultmailboxname")
    @JSONField(name = "defaultmailboxname")
    @JsonProperty("defaultmailboxname")
    private String defaultmailboxname;
    /**
     * 实体图像
     */
    @TableField(value = "entityimage")
    @JSONField(name = "entityimage")
    @JsonProperty("entityimage")
    private String entityimage;
    /**
     * 外发的电子邮件的传送方式
     */
    @TableField(value = "outgoingemaildeliverymethod")
    @JSONField(name = "outgoingemaildeliverymethod")
    @JsonProperty("outgoingemaildeliverymethod")
    private String outgoingemaildeliverymethod;
    /**
     * 主要电子邮件状态
     */
    @TableField(value = "emailrouteraccessapproval")
    @JSONField(name = "emailrouteraccessapproval")
    @JsonProperty("emailrouteraccessapproval")
    private String emailrouteraccessapproval;
    /**
     * 货币
     */
    @TableField(value = "transactioncurrencyid")
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;
    /**
     * 业务部门
     */
    @TableField(value = "businessunitid")
    @JSONField(name = "businessunitid")
    @JsonProperty("businessunitid")
    private String businessunitid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.BusinessUnit businessunit;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency transactioncurrency;



    /**
     * 设置 [负责人]
     */
    public void setOwnerid(String ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [EntityImage_URL]
     */
    public void setEntityimageUrl(String entityimageUrl){
        this.entityimageUrl = entityimageUrl ;
        this.modify("entityimage_url",entityimageUrl);
    }

    /**
     * 设置 [传入电子邮件]
     */
    public void setEmailaddress(String emailaddress){
        this.emailaddress = emailaddress ;
        this.modify("emailaddress",emailaddress);
    }

    /**
     * 设置 [负责人(已弃用)]
     */
    public void setPrimaryuserid(String primaryuserid){
        this.primaryuserid = primaryuserid ;
        this.modify("primaryuserid",primaryuserid);
    }

    /**
     * 设置 [汇率]
     */
    public void setExchangerate(BigDecimal exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [接收电子邮件的传送方式]
     */
    public void setIncomingemaildeliverymethod(String incomingemaildeliverymethod){
        this.incomingemaildeliverymethod = incomingemaildeliverymethod ;
        this.modify("incomingemaildeliverymethod",incomingemaildeliverymethod);
    }

    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [创建记录的时间]
     */
    public void setOverriddencreatedon(Timestamp overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 格式化日期 [创建记录的时间]
     */
    public String formatOverriddencreatedon(){
        if (this.overriddencreatedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(overriddencreatedon);
    }
    /**
     * 设置 [传真队列]
     */
    public void setFaxqueue(Integer faxqueue){
        this.faxqueue = faxqueue ;
        this.modify("faxqueue",faxqueue);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnername(String ownername){
        this.ownername = ownername ;
        this.modify("ownername",ownername);
    }

    /**
     * 设置 [队列类型]
     */
    public void setQueuetypecode(String queuetypecode){
        this.queuetypecode = queuetypecode ;
        this.modify("queuetypecode",queuetypecode);
    }

    /**
     * 设置 [将传入电子邮件转换为活动]
     */
    public void setIncomingemailfilteringmethod(String incomingemailfilteringmethod){
        this.incomingemailfilteringmethod = incomingemailfilteringmethod ;
        this.modify("incomingemailfilteringmethod",incomingemailfilteringmethod);
    }

    /**
     * 设置 [成员数]
     */
    public void setNumberofmembers(Integer numberofmembers){
        this.numberofmembers = numberofmembers ;
        this.modify("numberofmembers",numberofmembers);
    }

    /**
     * 设置 [导入序列号]
     */
    public void setImportsequencenumber(Integer importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [队列名称]
     */
    public void setQueuename(String queuename){
        this.queuename = queuename ;
        this.modify("queuename",queuename);
    }

    /**
     * 设置 [版本号]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [EntityImage_Timestamp]
     */
    public void setEntityimageTimestamp(BigInteger entityimageTimestamp){
        this.entityimageTimestamp = entityimageTimestamp ;
        this.modify("entityimage_timestamp",entityimageTimestamp);
    }

    /**
     * 设置 [密码(已过时)]
     */
    public void setEmailpassword(String emailpassword){
        this.emailpassword = emailpassword ;
        this.modify("emailpassword",emailpassword);
    }

    /**
     * 设置 [允许使用凭据进行电子邮件处理(已过时)]
     */
    public void setAllowemailcredentials(Integer allowemailcredentials){
        this.allowemailcredentials = allowemailcredentials ;
        this.modify("allowemailcredentials",allowemailcredentials);
    }

    /**
     * 设置 [类型]
     */
    public void setQueueviewtype(String queueviewtype){
        this.queueviewtype = queueviewtype ;
        this.modify("queueviewtype",queueviewtype);
    }

    /**
     * 设置 [状态]
     */
    public void setStatecode(Integer statecode){
        this.statecode = statecode ;
        this.modify("statecode",statecode);
    }

    /**
     * 设置 [用户名(已过时)]
     */
    public void setEmailusername(String emailusername){
        this.emailusername = emailusername ;
        this.modify("emailusername",emailusername);
    }

    /**
     * 设置 [实体图像 ID]
     */
    public void setEntityimageid(String entityimageid){
        this.entityimageid = entityimageid ;
        this.modify("entityimageid",entityimageid);
    }

    /**
     * 设置 [负责人类型]
     */
    public void setOwnertype(String ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [队列项]
     */
    public void setNumberofitems(Integer numberofitems){
        this.numberofitems = numberofitems ;
        this.modify("numberofitems",numberofitems);
    }

    /**
     * 设置 [用户]
     */
    public void setPrimaryusername(String primaryusername){
        this.primaryusername = primaryusername ;
        this.modify("primaryusername",primaryusername);
    }

    /**
     * 设置 [转换为电子邮件活动]
     */
    public void setIgnoreunsolicitedemail(Integer ignoreunsolicitedemail){
        this.ignoreunsolicitedemail = ignoreunsolicitedemail ;
        this.modify("ignoreunsolicitedemail",ignoreunsolicitedemail);
    }

    /**
     * 设置 [状态描述]
     */
    public void setStatuscode(Integer statuscode){
        this.statuscode = statuscode ;
        this.modify("statuscode",statuscode);
    }

    /**
     * 设置 [DefaultMailboxName]
     */
    public void setDefaultmailboxname(String defaultmailboxname){
        this.defaultmailboxname = defaultmailboxname ;
        this.modify("defaultmailboxname",defaultmailboxname);
    }

    /**
     * 设置 [实体图像]
     */
    public void setEntityimage(String entityimage){
        this.entityimage = entityimage ;
        this.modify("entityimage",entityimage);
    }

    /**
     * 设置 [外发的电子邮件的传送方式]
     */
    public void setOutgoingemaildeliverymethod(String outgoingemaildeliverymethod){
        this.outgoingemaildeliverymethod = outgoingemaildeliverymethod ;
        this.modify("outgoingemaildeliverymethod",outgoingemaildeliverymethod);
    }

    /**
     * 设置 [主要电子邮件状态]
     */
    public void setEmailrouteraccessapproval(String emailrouteraccessapproval){
        this.emailrouteraccessapproval = emailrouteraccessapproval ;
        this.modify("emailrouteraccessapproval",emailrouteraccessapproval);
    }

    /**
     * 设置 [货币]
     */
    public void setTransactioncurrencyid(String transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }

    /**
     * 设置 [业务部门]
     */
    public void setBusinessunitid(String businessunitid){
        this.businessunitid = businessunitid ;
        this.modify("businessunitid",businessunitid);
    }


}


