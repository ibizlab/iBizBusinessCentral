package cn.ibizlab.businesscentral.core.scheduling.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.scheduling.domain.BookableResourceCharacteristic;
/**
 * 关系型数据实体[BookableResourceCharacteristic] 查询条件对象
 */
@Slf4j
@Data
public class BookableResourceCharacteristicSearchContext extends QueryWrapperContext<BookableResourceCharacteristic> {

	private Integer n_statuscode_eq;//[状态描述]
	public void setN_statuscode_eq(Integer n_statuscode_eq) {
        this.n_statuscode_eq = n_statuscode_eq;
        if(!ObjectUtils.isEmpty(this.n_statuscode_eq)){
            this.getSearchCond().eq("statuscode", n_statuscode_eq);
        }
    }
	private Integer n_statecode_eq;//[状态]
	public void setN_statecode_eq(Integer n_statecode_eq) {
        this.n_statecode_eq = n_statecode_eq;
        if(!ObjectUtils.isEmpty(this.n_statecode_eq)){
            this.getSearchCond().eq("statecode", n_statecode_eq);
        }
    }
	private String n_bookablerescharacteristicname_like;//[可预订资源的特征名称]
	public void setN_bookablerescharacteristicname_like(String n_bookablerescharacteristicname_like) {
        this.n_bookablerescharacteristicname_like = n_bookablerescharacteristicname_like;
        if(!ObjectUtils.isEmpty(this.n_bookablerescharacteristicname_like)){
            this.getSearchCond().like("bookablerescharacteristicname", n_bookablerescharacteristicname_like);
        }
    }
	private String n_characteristic_eq;//[特征]
	public void setN_characteristic_eq(String n_characteristic_eq) {
        this.n_characteristic_eq = n_characteristic_eq;
        if(!ObjectUtils.isEmpty(this.n_characteristic_eq)){
            this.getSearchCond().eq("characteristic", n_characteristic_eq);
        }
    }
	private String n_transactioncurrencyid_eq;//[货币]
	public void setN_transactioncurrencyid_eq(String n_transactioncurrencyid_eq) {
        this.n_transactioncurrencyid_eq = n_transactioncurrencyid_eq;
        if(!ObjectUtils.isEmpty(this.n_transactioncurrencyid_eq)){
            this.getSearchCond().eq("transactioncurrencyid", n_transactioncurrencyid_eq);
        }
    }
	private String n_resource_eq;//[资源]
	public void setN_resource_eq(String n_resource_eq) {
        this.n_resource_eq = n_resource_eq;
        if(!ObjectUtils.isEmpty(this.n_resource_eq)){
            this.getSearchCond().eq("resource", n_resource_eq);
        }
    }
	private String n_ratingvalue_eq;//[评分值]
	public void setN_ratingvalue_eq(String n_ratingvalue_eq) {
        this.n_ratingvalue_eq = n_ratingvalue_eq;
        if(!ObjectUtils.isEmpty(this.n_ratingvalue_eq)){
            this.getSearchCond().eq("ratingvalue", n_ratingvalue_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("bookablerescharacteristicname", query)   
            );
		 }
	}
}



