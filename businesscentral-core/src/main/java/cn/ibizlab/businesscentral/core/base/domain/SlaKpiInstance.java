package cn.ibizlab.businesscentral.core.base.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[SLA KPI 实例]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SLAKPIINSTANCE",resultMap = "SlaKpiInstanceResultMap")
public class SlaKpiInstance extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * SLA KPI InstanceId
     */
    @DEField(isKeyField=true)
    @TableId(value= "slakpiinstanceid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "slakpiinstanceid")
    @JsonProperty("slakpiinstanceid")
    private String slakpiinstanceid;
    /**
     * 负责人类型
     */
    @TableField(value = "ownertype")
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;
    /**
     * 状态
     */
    @TableField(value = "status")
    @JSONField(name = "status")
    @JsonProperty("status")
    private String status;
    /**
     * 说明
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 计算出的警告时间
     */
    @TableField(value = "computedwarningtime")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "computedwarningtime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("computedwarningtime")
    private Timestamp computedwarningtime;
    /**
     * 负责人用户
     */
    @TableField(value = "owninguser")
    @JSONField(name = "owninguser")
    @JsonProperty("owninguser")
    private String owninguser;
    /**
     * 关于
     */
    @TableField(value = "regardingname")
    @JSONField(name = "regardingname")
    @JsonProperty("regardingname")
    private String regardingname;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 负责人
     */
    @TableField(value = "ownerid")
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;
    /**
     * RegardingObjectTypeCode
     */
    @TableField(value = "regardingobjecttypecode")
    @JSONField(name = "regardingobjecttypecode")
    @JsonProperty("regardingobjecttypecode")
    private String regardingobjecttypecode;
    /**
     * 服务协议KPI实例名称
     */
    @TableField(value = "slakpiinstancename")
    @JSONField(name = "slakpiinstancename")
    @JsonProperty("slakpiinstancename")
    private String slakpiinstancename;
    /**
     * 汇率
     */
    @TableField(value = "exchangerate")
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;
    /**
     * 已达到警告时间
     */
    @TableField(value = "warningtimereached")
    @JSONField(name = "warningtimereached")
    @JsonProperty("warningtimereached")
    private String warningtimereached;
    /**
     * 负责人
     */
    @TableField(value = "ownername")
    @JSONField(name = "ownername")
    @JsonProperty("ownername")
    private String ownername;
    /**
     * 成功时间
     */
    @TableField(value = "succeededon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "succeededon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("succeededon")
    private Timestamp succeededon;
    /**
     * 失败时间
     */
    @TableField(value = "failuretime")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "failuretime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("failuretime")
    private Timestamp failuretime;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 版本号
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * 计算出的失败时间
     */
    @TableField(value = "computedfailuretime")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "computedfailuretime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("computedfailuretime")
    private Timestamp computedfailuretime;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 警告时间
     */
    @TableField(value = "warningtime")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "warningtime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("warningtime")
    private Timestamp warningtime;
    /**
     * 负责团队
     */
    @TableField(value = "owningteam")
    @JSONField(name = "owningteam")
    @JsonProperty("owningteam")
    private String owningteam;
    /**
     * 货币
     */
    @TableField(value = "transactioncurrencyid")
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;
    /**
     * 负责的业务部门
     */
    @TableField(value = "owningbusinessunit")
    @JSONField(name = "owningbusinessunit")
    @JsonProperty("owningbusinessunit")
    private String owningbusinessunit;
    /**
     * 关于
     */
    @TableField(value = "regarding")
    @JSONField(name = "regarding")
    @JsonProperty("regarding")
    private String regarding;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.Account regardi;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.BusinessUnit owningbusinessun;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency transactioncurrency;



    /**
     * 设置 [负责人类型]
     */
    public void setOwnertype(String ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [状态]
     */
    public void setStatus(String status){
        this.status = status ;
        this.modify("status",status);
    }

    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [计算出的警告时间]
     */
    public void setComputedwarningtime(Timestamp computedwarningtime){
        this.computedwarningtime = computedwarningtime ;
        this.modify("computedwarningtime",computedwarningtime);
    }

    /**
     * 格式化日期 [计算出的警告时间]
     */
    public String formatComputedwarningtime(){
        if (this.computedwarningtime == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(computedwarningtime);
    }
    /**
     * 设置 [负责人用户]
     */
    public void setOwninguser(String owninguser){
        this.owninguser = owninguser ;
        this.modify("owninguser",owninguser);
    }

    /**
     * 设置 [关于]
     */
    public void setRegardingname(String regardingname){
        this.regardingname = regardingname ;
        this.modify("regardingname",regardingname);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnerid(String ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [RegardingObjectTypeCode]
     */
    public void setRegardingobjecttypecode(String regardingobjecttypecode){
        this.regardingobjecttypecode = regardingobjecttypecode ;
        this.modify("regardingobjecttypecode",regardingobjecttypecode);
    }

    /**
     * 设置 [服务协议KPI实例名称]
     */
    public void setSlakpiinstancename(String slakpiinstancename){
        this.slakpiinstancename = slakpiinstancename ;
        this.modify("slakpiinstancename",slakpiinstancename);
    }

    /**
     * 设置 [汇率]
     */
    public void setExchangerate(BigDecimal exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [已达到警告时间]
     */
    public void setWarningtimereached(String warningtimereached){
        this.warningtimereached = warningtimereached ;
        this.modify("warningtimereached",warningtimereached);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnername(String ownername){
        this.ownername = ownername ;
        this.modify("ownername",ownername);
    }

    /**
     * 设置 [成功时间]
     */
    public void setSucceededon(Timestamp succeededon){
        this.succeededon = succeededon ;
        this.modify("succeededon",succeededon);
    }

    /**
     * 格式化日期 [成功时间]
     */
    public String formatSucceededon(){
        if (this.succeededon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(succeededon);
    }
    /**
     * 设置 [失败时间]
     */
    public void setFailuretime(Timestamp failuretime){
        this.failuretime = failuretime ;
        this.modify("failuretime",failuretime);
    }

    /**
     * 格式化日期 [失败时间]
     */
    public String formatFailuretime(){
        if (this.failuretime == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(failuretime);
    }
    /**
     * 设置 [版本号]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [计算出的失败时间]
     */
    public void setComputedfailuretime(Timestamp computedfailuretime){
        this.computedfailuretime = computedfailuretime ;
        this.modify("computedfailuretime",computedfailuretime);
    }

    /**
     * 格式化日期 [计算出的失败时间]
     */
    public String formatComputedfailuretime(){
        if (this.computedfailuretime == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(computedfailuretime);
    }
    /**
     * 设置 [警告时间]
     */
    public void setWarningtime(Timestamp warningtime){
        this.warningtime = warningtime ;
        this.modify("warningtime",warningtime);
    }

    /**
     * 格式化日期 [警告时间]
     */
    public String formatWarningtime(){
        if (this.warningtime == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(warningtime);
    }
    /**
     * 设置 [负责团队]
     */
    public void setOwningteam(String owningteam){
        this.owningteam = owningteam ;
        this.modify("owningteam",owningteam);
    }

    /**
     * 设置 [货币]
     */
    public void setTransactioncurrencyid(String transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }

    /**
     * 设置 [负责的业务部门]
     */
    public void setOwningbusinessunit(String owningbusinessunit){
        this.owningbusinessunit = owningbusinessunit ;
        this.modify("owningbusinessunit",owningbusinessunit);
    }

    /**
     * 设置 [关于]
     */
    public void setRegarding(String regarding){
        this.regarding = regarding ;
        this.modify("regarding",regarding);
    }


}


