package cn.ibizlab.businesscentral.core.finance.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[年度会计日历]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "ANNUALFISCALCALENDAR",resultMap = "AnnualFiscalCalendarResultMap")
public class AnnualFiscalCalendar extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 销售员
     */
    @TableField(value = "salespersonname")
    @JSONField(name = "salespersonname")
    @JsonProperty("salespersonname")
    private String salespersonname;
    /**
     * SalesPersonId
     */
    @TableField(value = "salespersonid")
    @JSONField(name = "salespersonid")
    @JsonProperty("salespersonid")
    private String salespersonid;
    /**
     * Period1_Base
     */
    @DEField(name = "period1_base")
    @TableField(value = "period1_base")
    @JSONField(name = "period1_base")
    @JsonProperty("period1_base")
    private BigDecimal period1Base;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * EffectiveOn
     */
    @TableField(value = "effectiveon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "effectiveon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("effectiveon")
    private Timestamp effectiveon;
    /**
     * FiscalPeriodType
     */
    @TableField(value = "fiscalperiodtype")
    @JSONField(name = "fiscalperiodtype")
    @JsonProperty("fiscalperiodtype")
    private Integer fiscalperiodtype;
    /**
     * UTCConversionTimeZoneCode
     */
    @TableField(value = "utcconversiontimezonecode")
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;
    /**
     * TimeZoneRuleVersionNumber
     */
    @TableField(value = "timezoneruleversionnumber")
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;
    /**
     * UserFiscalCalendarId
     */
    @DEField(isKeyField=true)
    @TableId(value= "userfiscalcalendarid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "userfiscalcalendarid")
    @JsonProperty("userfiscalcalendarid")
    private String userfiscalcalendarid;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * Period1
     */
    @TableField(value = "period1")
    @JSONField(name = "period1")
    @JsonProperty("period1")
    private BigDecimal period1;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * ExchangeRate
     */
    @TableField(value = "exchangerate")
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;
    /**
     * TransactionCurrencyId
     */
    @TableField(value = "transactioncurrencyid")
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency transactioncurrency;



    /**
     * 设置 [销售员]
     */
    public void setSalespersonname(String salespersonname){
        this.salespersonname = salespersonname ;
        this.modify("salespersonname",salespersonname);
    }

    /**
     * 设置 [SalesPersonId]
     */
    public void setSalespersonid(String salespersonid){
        this.salespersonid = salespersonid ;
        this.modify("salespersonid",salespersonid);
    }

    /**
     * 设置 [Period1_Base]
     */
    public void setPeriod1Base(BigDecimal period1Base){
        this.period1Base = period1Base ;
        this.modify("period1_base",period1Base);
    }

    /**
     * 设置 [EffectiveOn]
     */
    public void setEffectiveon(Timestamp effectiveon){
        this.effectiveon = effectiveon ;
        this.modify("effectiveon",effectiveon);
    }

    /**
     * 格式化日期 [EffectiveOn]
     */
    public String formatEffectiveon(){
        if (this.effectiveon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(effectiveon);
    }
    /**
     * 设置 [FiscalPeriodType]
     */
    public void setFiscalperiodtype(Integer fiscalperiodtype){
        this.fiscalperiodtype = fiscalperiodtype ;
        this.modify("fiscalperiodtype",fiscalperiodtype);
    }

    /**
     * 设置 [UTCConversionTimeZoneCode]
     */
    public void setUtcconversiontimezonecode(Integer utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [TimeZoneRuleVersionNumber]
     */
    public void setTimezoneruleversionnumber(Integer timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [Period1]
     */
    public void setPeriod1(BigDecimal period1){
        this.period1 = period1 ;
        this.modify("period1",period1);
    }

    /**
     * 设置 [ExchangeRate]
     */
    public void setExchangerate(BigDecimal exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [TransactionCurrencyId]
     */
    public void setTransactioncurrencyid(String transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }


}


