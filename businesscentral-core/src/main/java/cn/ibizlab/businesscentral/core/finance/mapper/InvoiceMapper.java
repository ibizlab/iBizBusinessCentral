package cn.ibizlab.businesscentral.core.finance.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.finance.domain.Invoice;
import cn.ibizlab.businesscentral.core.finance.filter.InvoiceSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface InvoiceMapper extends BaseMapper<Invoice>{

    Page<Invoice> searchByParentKey(IPage page, @Param("srf") InvoiceSearchContext context, @Param("ew") Wrapper<Invoice> wrapper) ;
    Page<Invoice> searchCancel(IPage page, @Param("srf") InvoiceSearchContext context, @Param("ew") Wrapper<Invoice> wrapper) ;
    Page<Invoice> searchDefault(IPage page, @Param("srf") InvoiceSearchContext context, @Param("ew") Wrapper<Invoice> wrapper) ;
    Page<Invoice> searchPaid(IPage page, @Param("srf") InvoiceSearchContext context, @Param("ew") Wrapper<Invoice> wrapper) ;
    @Override
    Invoice selectById(Serializable id);
    @Override
    int insert(Invoice entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Invoice entity);
    @Override
    int update(@Param(Constants.ENTITY) Invoice entity, @Param("ew") Wrapper<Invoice> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Invoice> selectByOpportunityid(@Param("opportunityid") Serializable opportunityid) ;

    List<Invoice> selectByPricelevelid(@Param("pricelevelid") Serializable pricelevelid) ;

    List<Invoice> selectBySalesorderid(@Param("salesorderid") Serializable salesorderid) ;

    List<Invoice> selectBySlaid(@Param("slaid") Serializable slaid) ;

    List<Invoice> selectByTransactioncurrencyid(@Param("transactioncurrencyid") Serializable transactioncurrencyid) ;

}
