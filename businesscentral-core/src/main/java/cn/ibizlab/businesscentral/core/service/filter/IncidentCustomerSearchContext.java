package cn.ibizlab.businesscentral.core.service.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.service.domain.IncidentCustomer;
/**
 * 关系型数据实体[IncidentCustomer] 查询条件对象
 */
@Slf4j
@Data
public class IncidentCustomerSearchContext extends QueryWrapperContext<IncidentCustomer> {

	private String n_businesstypecode_eq;//[商业类型]
	public void setN_businesstypecode_eq(String n_businesstypecode_eq) {
        this.n_businesstypecode_eq = n_businesstypecode_eq;
        if(!ObjectUtils.isEmpty(this.n_businesstypecode_eq)){
            this.getSearchCond().eq("businesstypecode", n_businesstypecode_eq);
        }
    }
	private String n_customertype_eq;//[客户类型]
	public void setN_customertype_eq(String n_customertype_eq) {
        this.n_customertype_eq = n_customertype_eq;
        if(!ObjectUtils.isEmpty(this.n_customertype_eq)){
            this.getSearchCond().eq("customertype", n_customertype_eq);
        }
    }
	private String n_customername_like;//[客户]
	public void setN_customername_like(String n_customername_like) {
        this.n_customername_like = n_customername_like;
        if(!ObjectUtils.isEmpty(this.n_customername_like)){
            this.getSearchCond().like("customername", n_customername_like);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("customername", query)   
            );
		 }
	}
}



