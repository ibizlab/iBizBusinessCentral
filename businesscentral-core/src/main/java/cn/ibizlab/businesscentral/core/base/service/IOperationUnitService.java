package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.OperationUnit;
import cn.ibizlab.businesscentral.core.base.filter.OperationUnitSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[OperationUnit] 服务对象接口
 */
public interface IOperationUnitService extends IService<OperationUnit>{

    boolean create(OperationUnit et) ;
    void createBatch(List<OperationUnit> list) ;
    boolean update(OperationUnit et) ;
    void updateBatch(List<OperationUnit> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    OperationUnit get(String key) ;
    OperationUnit getDraft(OperationUnit et) ;
    boolean checkKey(OperationUnit et) ;
    boolean save(OperationUnit et) ;
    void saveBatch(List<OperationUnit> list) ;
    Page<OperationUnit> searchBusinessUnit(OperationUnitSearchContext context) ;
    Page<OperationUnit> searchDefault(OperationUnitSearchContext context) ;
    Page<OperationUnit> searchDepartment(OperationUnitSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<OperationUnit> getOperationunitByIds(List<String> ids) ;
    List<OperationUnit> getOperationunitByEntities(List<OperationUnit> entities) ;
}


