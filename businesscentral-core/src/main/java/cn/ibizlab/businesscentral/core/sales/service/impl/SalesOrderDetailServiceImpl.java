package cn.ibizlab.businesscentral.core.sales.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.sales.domain.SalesOrderDetail;
import cn.ibizlab.businesscentral.core.sales.filter.SalesOrderDetailSearchContext;
import cn.ibizlab.businesscentral.core.sales.service.ISalesOrderDetailService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.sales.mapper.SalesOrderDetailMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[订单产品] 服务对象接口实现
 */
@Slf4j
@Service("SalesOrderDetailServiceImpl")
public class SalesOrderDetailServiceImpl extends ServiceImpl<SalesOrderDetailMapper, SalesOrderDetail> implements ISalesOrderDetailService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.finance.service.IInvoiceDetailService invoicedetailService;

    protected cn.ibizlab.businesscentral.core.sales.service.ISalesOrderDetailService salesorderdetailService = this;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.product.service.IProductService productService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.IQuoteDetailService quotedetailService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.ISalesOrderService salesorderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITransactionCurrencyService transactioncurrencyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IUomService uomService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(SalesOrderDetail et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getSalesorderdetailid()),et);
        return true;
    }

    @Override
    public void createBatch(List<SalesOrderDetail> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(SalesOrderDetail et) {
        fillParentData(et);
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("salesorderdetailid",et.getSalesorderdetailid())))
            return false;
        CachedBeanCopier.copy(get(et.getSalesorderdetailid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<SalesOrderDetail> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public SalesOrderDetail get(String key) {
        SalesOrderDetail et = getById(key);
        if(et==null){
            et=new SalesOrderDetail();
            et.setSalesorderdetailid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public SalesOrderDetail getDraft(SalesOrderDetail et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(SalesOrderDetail et) {
        return (!ObjectUtils.isEmpty(et.getSalesorderdetailid()))&&(!Objects.isNull(this.getById(et.getSalesorderdetailid())));
    }
    @Override
    @Transactional
    public boolean save(SalesOrderDetail et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(SalesOrderDetail et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<SalesOrderDetail> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<SalesOrderDetail> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<SalesOrderDetail> selectByProductid(String productid) {
        return baseMapper.selectByProductid(productid);
    }

    @Override
    public void removeByProductid(String productid) {
        this.remove(new QueryWrapper<SalesOrderDetail>().eq("productid",productid));
    }

	@Override
    public List<SalesOrderDetail> selectByQuotedetailid(String quotedetailid) {
        return baseMapper.selectByQuotedetailid(quotedetailid);
    }

    @Override
    public void removeByQuotedetailid(String quotedetailid) {
        this.remove(new QueryWrapper<SalesOrderDetail>().eq("quotedetailid",quotedetailid));
    }

	@Override
    public List<SalesOrderDetail> selectByParentbundleidref(String salesorderdetailid) {
        return baseMapper.selectByParentbundleidref(salesorderdetailid);
    }

    @Override
    public void removeByParentbundleidref(String salesorderdetailid) {
        this.remove(new QueryWrapper<SalesOrderDetail>().eq("parentbundleidref",salesorderdetailid));
    }

	@Override
    public List<SalesOrderDetail> selectBySalesorderid(String salesorderid) {
        return baseMapper.selectBySalesorderid(salesorderid);
    }

    @Override
    public void removeBySalesorderid(String salesorderid) {
        this.remove(new QueryWrapper<SalesOrderDetail>().eq("salesorderid",salesorderid));
    }

	@Override
    public List<SalesOrderDetail> selectByTransactioncurrencyid(String transactioncurrencyid) {
        return baseMapper.selectByTransactioncurrencyid(transactioncurrencyid);
    }

    @Override
    public void removeByTransactioncurrencyid(String transactioncurrencyid) {
        this.remove(new QueryWrapper<SalesOrderDetail>().eq("transactioncurrencyid",transactioncurrencyid));
    }

	@Override
    public List<SalesOrderDetail> selectByUomid(String uomid) {
        return baseMapper.selectByUomid(uomid);
    }

    @Override
    public void removeByUomid(String uomid) {
        this.remove(new QueryWrapper<SalesOrderDetail>().eq("uomid",uomid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<SalesOrderDetail> searchDefault(SalesOrderDetailSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<SalesOrderDetail> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<SalesOrderDetail>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(SalesOrderDetail et){
        //实体关系[DER1N_SALESORDERDETAIL__PRODUCT__PRODUCTID]
        if(!ObjectUtils.isEmpty(et.getProductid())){
            cn.ibizlab.businesscentral.core.product.domain.Product product=et.getProduct();
            if(ObjectUtils.isEmpty(product)){
                cn.ibizlab.businesscentral.core.product.domain.Product majorEntity=productService.get(et.getProductid());
                et.setProduct(majorEntity);
                product=majorEntity;
            }
            et.setProductname(product.getProductname());
        }
        //实体关系[DER1N_SALESORDERDETAIL__TRANSACTIONCURRENCY__TRANSACTIONCURRENCYID]
        if(!ObjectUtils.isEmpty(et.getTransactioncurrencyid())){
            cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency transactioncurrency=et.getTransactioncurrency();
            if(ObjectUtils.isEmpty(transactioncurrency)){
                cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency majorEntity=transactioncurrencyService.get(et.getTransactioncurrencyid());
                et.setTransactioncurrency(majorEntity);
                transactioncurrency=majorEntity;
            }
            et.setCurrencyname(transactioncurrency.getCurrencyname());
        }
        //实体关系[DER1N_SALESORDERDETAIL__UOM__UOMID]
        if(!ObjectUtils.isEmpty(et.getUomid())){
            cn.ibizlab.businesscentral.core.base.domain.Uom uom=et.getUom();
            if(ObjectUtils.isEmpty(uom)){
                cn.ibizlab.businesscentral.core.base.domain.Uom majorEntity=uomService.get(et.getUomid());
                et.setUom(majorEntity);
                uom=majorEntity;
            }
            et.setUomname(uom.getUomname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<SalesOrderDetail> getSalesorderdetailByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<SalesOrderDetail> getSalesorderdetailByEntities(List<SalesOrderDetail> entities) {
        List ids =new ArrayList();
        for(SalesOrderDetail entity : entities){
            Serializable id=entity.getSalesorderdetailid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}



