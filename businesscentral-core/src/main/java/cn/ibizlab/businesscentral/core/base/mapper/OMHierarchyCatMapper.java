package cn.ibizlab.businesscentral.core.base.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.base.domain.OMHierarchyCat;
import cn.ibizlab.businesscentral.core.base.filter.OMHierarchyCatSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface OMHierarchyCatMapper extends BaseMapper<OMHierarchyCat>{

    Page<OMHierarchyCat> searchDefault(IPage page, @Param("srf") OMHierarchyCatSearchContext context, @Param("ew") Wrapper<OMHierarchyCat> wrapper) ;
    @Override
    OMHierarchyCat selectById(Serializable id);
    @Override
    int insert(OMHierarchyCat entity);
    @Override
    int updateById(@Param(Constants.ENTITY) OMHierarchyCat entity);
    @Override
    int update(@Param(Constants.ENTITY) OMHierarchyCat entity, @Param("ew") Wrapper<OMHierarchyCat> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

}
