package cn.ibizlab.businesscentral.core.runtime.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.runtime.domain.BulkOperationLog;
/**
 * 关系型数据实体[BulkOperationLog] 查询条件对象
 */
@Slf4j
@Data
public class BulkOperationLogSearchContext extends QueryWrapperContext<BulkOperationLog> {

	private String n_bulkoperationlogname_like;//[批除操作日志名称]
	public void setN_bulkoperationlogname_like(String n_bulkoperationlogname_like) {
        this.n_bulkoperationlogname_like = n_bulkoperationlogname_like;
        if(!ObjectUtils.isEmpty(this.n_bulkoperationlogname_like)){
            this.getSearchCond().like("bulkoperationlogname", n_bulkoperationlogname_like);
        }
    }
	private String n_bulkoperationid_eq;//[批量操作号]
	public void setN_bulkoperationid_eq(String n_bulkoperationid_eq) {
        this.n_bulkoperationid_eq = n_bulkoperationid_eq;
        if(!ObjectUtils.isEmpty(this.n_bulkoperationid_eq)){
            this.getSearchCond().eq("bulkoperationid", n_bulkoperationid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("bulkoperationlogname", query)   
            );
		 }
	}
}



