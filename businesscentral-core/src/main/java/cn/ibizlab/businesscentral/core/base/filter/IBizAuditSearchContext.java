package cn.ibizlab.businesscentral.core.base.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.base.domain.IBizAudit;
/**
 * 关系型数据实体[IBizAudit] 查询条件对象
 */
@Slf4j
@Data
public class IBizAuditSearchContext extends QueryWrapperContext<IBizAudit> {

	private String n_action_eq;//[事件]
	public void setN_action_eq(String n_action_eq) {
        this.n_action_eq = n_action_eq;
        if(!ObjectUtils.isEmpty(this.n_action_eq)){
            this.getSearchCond().eq("action", n_action_eq);
        }
    }
	private String n_operation_eq;//[操作]
	public void setN_operation_eq(String n_operation_eq) {
        this.n_operation_eq = n_operation_eq;
        if(!ObjectUtils.isEmpty(this.n_operation_eq)){
            this.getSearchCond().eq("operation", n_operation_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("action", query)   
            );
		 }
	}
}



