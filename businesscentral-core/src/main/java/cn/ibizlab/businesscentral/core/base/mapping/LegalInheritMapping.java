

package cn.ibizlab.businesscentral.core.base.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.base.domain.Legal;
import cn.ibizlab.businesscentral.core.base.domain.Organization;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface LegalInheritMapping {

    @Mappings({
        @Mapping(source ="legalid",target = "organizationid"),
        @Mapping(source ="legalname",target = "organizationname"),
        @Mapping(target ="focusNull",ignore = true),
    })
    Organization toOrganization(Legal legal);

    @Mappings({
        @Mapping(source ="organizationid" ,target = "legalid"),
        @Mapping(source ="organizationname" ,target = "legalname"),
        @Mapping(target ="focusNull",ignore = true),
    })
    Legal toLegal(Organization organization);

    List<Organization> toOrganization(List<Legal> legal);

    List<Legal> toLegal(List<Organization> organization);

}


