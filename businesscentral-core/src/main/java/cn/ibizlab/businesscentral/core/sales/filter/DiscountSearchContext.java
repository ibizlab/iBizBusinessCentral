package cn.ibizlab.businesscentral.core.sales.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.sales.domain.Discount;
/**
 * 关系型数据实体[Discount] 查询条件对象
 */
@Slf4j
@Data
public class DiscountSearchContext extends QueryWrapperContext<Discount> {

	private String n_discountname_like;//[折扣名称]
	public void setN_discountname_like(String n_discountname_like) {
        this.n_discountname_like = n_discountname_like;
        if(!ObjectUtils.isEmpty(this.n_discountname_like)){
            this.getSearchCond().like("discountname", n_discountname_like);
        }
    }
	private Integer n_statuscode_eq;//[状态描述]
	public void setN_statuscode_eq(Integer n_statuscode_eq) {
        this.n_statuscode_eq = n_statuscode_eq;
        if(!ObjectUtils.isEmpty(this.n_statuscode_eq)){
            this.getSearchCond().eq("statuscode", n_statuscode_eq);
        }
    }
	private String n_discounttypeid_eq;//[折扣类型]
	public void setN_discounttypeid_eq(String n_discounttypeid_eq) {
        this.n_discounttypeid_eq = n_discounttypeid_eq;
        if(!ObjectUtils.isEmpty(this.n_discounttypeid_eq)){
            this.getSearchCond().eq("discounttypeid", n_discounttypeid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("discountname", query)   
            );
		 }
	}
}



