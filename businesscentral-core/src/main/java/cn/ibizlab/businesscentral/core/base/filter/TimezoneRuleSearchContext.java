package cn.ibizlab.businesscentral.core.base.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.base.domain.TimezoneRule;
/**
 * 关系型数据实体[TimezoneRule] 查询条件对象
 */
@Slf4j
@Data
public class TimezoneRuleSearchContext extends QueryWrapperContext<TimezoneRule> {

	private Integer n_timezoneruleversionnumber_like;//[时区规则版本号]
	public void setN_timezoneruleversionnumber_like(Integer n_timezoneruleversionnumber_like) {
        this.n_timezoneruleversionnumber_like = n_timezoneruleversionnumber_like;
        if(!ObjectUtils.isEmpty(this.n_timezoneruleversionnumber_like)){
            this.getSearchCond().like("timezoneruleversionnumber", n_timezoneruleversionnumber_like);
        }
    }
	private String n_timezonedefinitionid_eq;//[时区定义]
	public void setN_timezonedefinitionid_eq(String n_timezonedefinitionid_eq) {
        this.n_timezonedefinitionid_eq = n_timezonedefinitionid_eq;
        if(!ObjectUtils.isEmpty(this.n_timezonedefinitionid_eq)){
            this.getSearchCond().eq("timezonedefinitionid", n_timezonedefinitionid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("timezoneruleversionnumber", query)   
            );
		 }
	}
}



