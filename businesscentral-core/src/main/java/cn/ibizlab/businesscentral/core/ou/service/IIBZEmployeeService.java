package cn.ibizlab.businesscentral.core.ou.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.ou.domain.IBZEmployee;
import cn.ibizlab.businesscentral.core.ou.filter.IBZEmployeeSearchContext;


/**
 * 实体[IBZEmployee] 服务对象接口
 */
public interface IIBZEmployeeService{

    boolean create(IBZEmployee et) ;
    @CacheEvict(value="ibzemployee",allEntries=true)
    void createBatch(List<IBZEmployee> list) ;
    boolean update(IBZEmployee et) ;
    @CacheEvict(value="ibzemployee",allEntries=true)
    void updateBatch(List<IBZEmployee> list) ;
    boolean remove(String key) ;
    @CacheEvict(value="ibzemployee",allEntries=true)
    void removeBatch(Collection<String> idList) ;
    IBZEmployee get(String key) ;
    IBZEmployee getDraft(IBZEmployee et) ;
    boolean checkKey(IBZEmployee et) ;
    IBZEmployee initPwd(IBZEmployee et) ;
    boolean save(IBZEmployee et) ;
    @CacheEvict(value="ibzemployee",allEntries=true)
    void saveBatch(List<IBZEmployee> list) ;
    Page<IBZEmployee> searchDefault(IBZEmployeeSearchContext context) ;
    List<IBZEmployee> selectByMdeptid(String deptid) ;
    @CacheEvict(value="ibzemployee",allEntries=true)
    void removeByMdeptid(String deptid) ;
    List<IBZEmployee> selectByOrgid(String orgid) ;
    @CacheEvict(value="ibzemployee",allEntries=true)
    void removeByOrgid(String orgid) ;
    List<IBZEmployee> selectByPostid(String postid) ;
    @CacheEvict(value="ibzemployee",allEntries=true)
    void removeByPostid(String postid) ;

}



