package cn.ibizlab.businesscentral.core.finance.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.finance.domain.SemiannualFiscalCalendar;
import cn.ibizlab.businesscentral.core.finance.filter.SemiannualFiscalCalendarSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[SemiannualFiscalCalendar] 服务对象接口
 */
public interface ISemiannualFiscalCalendarService extends IService<SemiannualFiscalCalendar>{

    boolean create(SemiannualFiscalCalendar et) ;
    void createBatch(List<SemiannualFiscalCalendar> list) ;
    boolean update(SemiannualFiscalCalendar et) ;
    void updateBatch(List<SemiannualFiscalCalendar> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    SemiannualFiscalCalendar get(String key) ;
    SemiannualFiscalCalendar getDraft(SemiannualFiscalCalendar et) ;
    boolean checkKey(SemiannualFiscalCalendar et) ;
    boolean save(SemiannualFiscalCalendar et) ;
    void saveBatch(List<SemiannualFiscalCalendar> list) ;
    Page<SemiannualFiscalCalendar> searchDefault(SemiannualFiscalCalendarSearchContext context) ;
    List<SemiannualFiscalCalendar> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<SemiannualFiscalCalendar> getSemiannualfiscalcalendarByIds(List<String> ids) ;
    List<SemiannualFiscalCalendar> getSemiannualfiscalcalendarByEntities(List<SemiannualFiscalCalendar> entities) ;
}


