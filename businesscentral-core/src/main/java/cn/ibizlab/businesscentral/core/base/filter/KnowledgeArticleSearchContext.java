package cn.ibizlab.businesscentral.core.base.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.base.domain.KnowledgeArticle;
/**
 * 关系型数据实体[KnowledgeArticle] 查询条件对象
 */
@Slf4j
@Data
public class KnowledgeArticleSearchContext extends QueryWrapperContext<KnowledgeArticle> {

	private Integer n_statuscode_eq;//[状态描述]
	public void setN_statuscode_eq(Integer n_statuscode_eq) {
        this.n_statuscode_eq = n_statuscode_eq;
        if(!ObjectUtils.isEmpty(this.n_statuscode_eq)){
            this.getSearchCond().eq("statuscode", n_statuscode_eq);
        }
    }
	private Integer n_statecode_eq;//[状态]
	public void setN_statecode_eq(Integer n_statecode_eq) {
        this.n_statecode_eq = n_statecode_eq;
        if(!ObjectUtils.isEmpty(this.n_statecode_eq)){
            this.getSearchCond().eq("statecode", n_statecode_eq);
        }
    }
	private String n_review_eq;//[审阅]
	public void setN_review_eq(String n_review_eq) {
        this.n_review_eq = n_review_eq;
        if(!ObjectUtils.isEmpty(this.n_review_eq)){
            this.getSearchCond().eq("review", n_review_eq);
        }
    }
	private String n_title_like;//[标题]
	public void setN_title_like(String n_title_like) {
        this.n_title_like = n_title_like;
        if(!ObjectUtils.isEmpty(this.n_title_like)){
            this.getSearchCond().like("title", n_title_like);
        }
    }
	private String n_expiredreviewoptions_eq;//[已过期审阅选项]
	public void setN_expiredreviewoptions_eq(String n_expiredreviewoptions_eq) {
        this.n_expiredreviewoptions_eq = n_expiredreviewoptions_eq;
        if(!ObjectUtils.isEmpty(this.n_expiredreviewoptions_eq)){
            this.getSearchCond().eq("expiredreviewoptions", n_expiredreviewoptions_eq);
        }
    }
	private String n_currencyname_eq;//[货币]
	public void setN_currencyname_eq(String n_currencyname_eq) {
        this.n_currencyname_eq = n_currencyname_eq;
        if(!ObjectUtils.isEmpty(this.n_currencyname_eq)){
            this.getSearchCond().eq("currencyname", n_currencyname_eq);
        }
    }
	private String n_currencyname_like;//[货币]
	public void setN_currencyname_like(String n_currencyname_like) {
        this.n_currencyname_like = n_currencyname_like;
        if(!ObjectUtils.isEmpty(this.n_currencyname_like)){
            this.getSearchCond().like("currencyname", n_currencyname_like);
        }
    }
	private String n_parentarticlecontentname_eq;//[上级文章]
	public void setN_parentarticlecontentname_eq(String n_parentarticlecontentname_eq) {
        this.n_parentarticlecontentname_eq = n_parentarticlecontentname_eq;
        if(!ObjectUtils.isEmpty(this.n_parentarticlecontentname_eq)){
            this.getSearchCond().eq("parentarticlecontentname", n_parentarticlecontentname_eq);
        }
    }
	private String n_parentarticlecontentname_like;//[上级文章]
	public void setN_parentarticlecontentname_like(String n_parentarticlecontentname_like) {
        this.n_parentarticlecontentname_like = n_parentarticlecontentname_like;
        if(!ObjectUtils.isEmpty(this.n_parentarticlecontentname_like)){
            this.getSearchCond().like("parentarticlecontentname", n_parentarticlecontentname_like);
        }
    }
	private String n_rootarticlename_eq;//[根文章]
	public void setN_rootarticlename_eq(String n_rootarticlename_eq) {
        this.n_rootarticlename_eq = n_rootarticlename_eq;
        if(!ObjectUtils.isEmpty(this.n_rootarticlename_eq)){
            this.getSearchCond().eq("rootarticlename", n_rootarticlename_eq);
        }
    }
	private String n_rootarticlename_like;//[根文章]
	public void setN_rootarticlename_like(String n_rootarticlename_like) {
        this.n_rootarticlename_like = n_rootarticlename_like;
        if(!ObjectUtils.isEmpty(this.n_rootarticlename_like)){
            this.getSearchCond().like("rootarticlename", n_rootarticlename_like);
        }
    }
	private String n_subjectname_eq;//[主题]
	public void setN_subjectname_eq(String n_subjectname_eq) {
        this.n_subjectname_eq = n_subjectname_eq;
        if(!ObjectUtils.isEmpty(this.n_subjectname_eq)){
            this.getSearchCond().eq("subjectname", n_subjectname_eq);
        }
    }
	private String n_subjectname_like;//[主题]
	public void setN_subjectname_like(String n_subjectname_like) {
        this.n_subjectname_like = n_subjectname_like;
        if(!ObjectUtils.isEmpty(this.n_subjectname_like)){
            this.getSearchCond().like("subjectname", n_subjectname_like);
        }
    }
	private String n_languagelocalename_eq;//[语言]
	public void setN_languagelocalename_eq(String n_languagelocalename_eq) {
        this.n_languagelocalename_eq = n_languagelocalename_eq;
        if(!ObjectUtils.isEmpty(this.n_languagelocalename_eq)){
            this.getSearchCond().eq("languagelocalename", n_languagelocalename_eq);
        }
    }
	private String n_languagelocalename_like;//[语言]
	public void setN_languagelocalename_like(String n_languagelocalename_like) {
        this.n_languagelocalename_like = n_languagelocalename_like;
        if(!ObjectUtils.isEmpty(this.n_languagelocalename_like)){
            this.getSearchCond().like("languagelocalename", n_languagelocalename_like);
        }
    }
	private String n_previousarticlecontentname_eq;//[上篇文章]
	public void setN_previousarticlecontentname_eq(String n_previousarticlecontentname_eq) {
        this.n_previousarticlecontentname_eq = n_previousarticlecontentname_eq;
        if(!ObjectUtils.isEmpty(this.n_previousarticlecontentname_eq)){
            this.getSearchCond().eq("previousarticlecontentname", n_previousarticlecontentname_eq);
        }
    }
	private String n_previousarticlecontentname_like;//[上篇文章]
	public void setN_previousarticlecontentname_like(String n_previousarticlecontentname_like) {
        this.n_previousarticlecontentname_like = n_previousarticlecontentname_like;
        if(!ObjectUtils.isEmpty(this.n_previousarticlecontentname_like)){
            this.getSearchCond().like("previousarticlecontentname", n_previousarticlecontentname_like);
        }
    }
	private String n_parentarticlecontentid_eq;//[上级文章]
	public void setN_parentarticlecontentid_eq(String n_parentarticlecontentid_eq) {
        this.n_parentarticlecontentid_eq = n_parentarticlecontentid_eq;
        if(!ObjectUtils.isEmpty(this.n_parentarticlecontentid_eq)){
            this.getSearchCond().eq("parentarticlecontentid", n_parentarticlecontentid_eq);
        }
    }
	private String n_previousarticlecontentid_eq;//[上篇文章内容 ID]
	public void setN_previousarticlecontentid_eq(String n_previousarticlecontentid_eq) {
        this.n_previousarticlecontentid_eq = n_previousarticlecontentid_eq;
        if(!ObjectUtils.isEmpty(this.n_previousarticlecontentid_eq)){
            this.getSearchCond().eq("previousarticlecontentid", n_previousarticlecontentid_eq);
        }
    }
	private String n_transactioncurrencyid_eq;//[货币]
	public void setN_transactioncurrencyid_eq(String n_transactioncurrencyid_eq) {
        this.n_transactioncurrencyid_eq = n_transactioncurrencyid_eq;
        if(!ObjectUtils.isEmpty(this.n_transactioncurrencyid_eq)){
            this.getSearchCond().eq("transactioncurrencyid", n_transactioncurrencyid_eq);
        }
    }
	private String n_rootarticleid_eq;//[根文章 ID]
	public void setN_rootarticleid_eq(String n_rootarticleid_eq) {
        this.n_rootarticleid_eq = n_rootarticleid_eq;
        if(!ObjectUtils.isEmpty(this.n_rootarticleid_eq)){
            this.getSearchCond().eq("rootarticleid", n_rootarticleid_eq);
        }
    }
	private String n_subjectid_eq;//[主题]
	public void setN_subjectid_eq(String n_subjectid_eq) {
        this.n_subjectid_eq = n_subjectid_eq;
        if(!ObjectUtils.isEmpty(this.n_subjectid_eq)){
            this.getSearchCond().eq("subjectid", n_subjectid_eq);
        }
    }
	private String n_languagelocaleid_eq;//[语言]
	public void setN_languagelocaleid_eq(String n_languagelocaleid_eq) {
        this.n_languagelocaleid_eq = n_languagelocaleid_eq;
        if(!ObjectUtils.isEmpty(this.n_languagelocaleid_eq)){
            this.getSearchCond().eq("languagelocaleid", n_languagelocaleid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("title", query)   
            );
		 }
	}
}



