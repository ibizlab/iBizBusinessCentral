package cn.ibizlab.businesscentral.core.product.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.product.domain.ProductAssociation;
import cn.ibizlab.businesscentral.core.product.filter.ProductAssociationSearchContext;
import cn.ibizlab.businesscentral.core.product.service.IProductAssociationService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.product.mapper.ProductAssociationMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[产品关联] 服务对象接口实现
 */
@Slf4j
@Service("ProductAssociationServiceImpl")
public class ProductAssociationServiceImpl extends ServiceImpl<ProductAssociationMapper, ProductAssociation> implements IProductAssociationService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.product.service.IProductService productService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITransactionCurrencyService transactioncurrencyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IUomService uomService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(ProductAssociation et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getProductassociationid()),et);
        return true;
    }

    @Override
    public void createBatch(List<ProductAssociation> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(ProductAssociation et) {
        fillParentData(et);
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("productassociationid",et.getProductassociationid())))
            return false;
        CachedBeanCopier.copy(get(et.getProductassociationid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<ProductAssociation> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public ProductAssociation get(String key) {
        ProductAssociation et = getById(key);
        if(et==null){
            et=new ProductAssociation();
            et.setProductassociationid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public ProductAssociation getDraft(ProductAssociation et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(ProductAssociation et) {
        return (!ObjectUtils.isEmpty(et.getProductassociationid()))&&(!Objects.isNull(this.getById(et.getProductassociationid())));
    }
    @Override
    @Transactional
    public boolean save(ProductAssociation et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(ProductAssociation et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<ProductAssociation> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<ProductAssociation> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<ProductAssociation> selectByAssociatedproduct(String productid) {
        return baseMapper.selectByAssociatedproduct(productid);
    }

    @Override
    public void removeByAssociatedproduct(String productid) {
        this.remove(new QueryWrapper<ProductAssociation>().eq("associatedproduct",productid));
    }

	@Override
    public List<ProductAssociation> selectByProductid(String productid) {
        return baseMapper.selectByProductid(productid);
    }

    @Override
    public void removeByProductid(String productid) {
        this.remove(new QueryWrapper<ProductAssociation>().eq("productid",productid));
    }

	@Override
    public List<ProductAssociation> selectByTransactioncurrencyid(String transactioncurrencyid) {
        return baseMapper.selectByTransactioncurrencyid(transactioncurrencyid);
    }

    @Override
    public void removeByTransactioncurrencyid(String transactioncurrencyid) {
        this.remove(new QueryWrapper<ProductAssociation>().eq("transactioncurrencyid",transactioncurrencyid));
    }

	@Override
    public List<ProductAssociation> selectByUomid(String uomid) {
        return baseMapper.selectByUomid(uomid);
    }

    @Override
    public void removeByUomid(String uomid) {
        this.remove(new QueryWrapper<ProductAssociation>().eq("uomid",uomid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<ProductAssociation> searchDefault(ProductAssociationSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<ProductAssociation> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<ProductAssociation>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(ProductAssociation et){
        //实体关系[DER1N_PRODUCTASSOCIATION__PRODUCT__ASSOCIATEDPRODUCT]
        if(!ObjectUtils.isEmpty(et.getAssociatedproduct())){
            cn.ibizlab.businesscentral.core.product.domain.Product associatedprodu=et.getAssociatedprodu();
            if(ObjectUtils.isEmpty(associatedprodu)){
                cn.ibizlab.businesscentral.core.product.domain.Product majorEntity=productService.get(et.getAssociatedproduct());
                et.setAssociatedprodu(majorEntity);
                associatedprodu=majorEntity;
            }
            et.setAssociatedproductname(associatedprodu.getProductname());
        }
        //实体关系[DER1N_PRODUCTASSOCIATION__PRODUCT__PRODUCTID]
        if(!ObjectUtils.isEmpty(et.getProductid())){
            cn.ibizlab.businesscentral.core.product.domain.Product product=et.getProduct();
            if(ObjectUtils.isEmpty(product)){
                cn.ibizlab.businesscentral.core.product.domain.Product majorEntity=productService.get(et.getProductid());
                et.setProduct(majorEntity);
                product=majorEntity;
            }
            et.setProductname(product.getProductname());
        }
        //实体关系[DER1N_PRODUCTASSOCIATION__TRANSACTIONCURRENCY__TRANSACTIONCURRENCYID]
        if(!ObjectUtils.isEmpty(et.getTransactioncurrencyid())){
            cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency transactioncurrency=et.getTransactioncurrency();
            if(ObjectUtils.isEmpty(transactioncurrency)){
                cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency majorEntity=transactioncurrencyService.get(et.getTransactioncurrencyid());
                et.setTransactioncurrency(majorEntity);
                transactioncurrency=majorEntity;
            }
            et.setCurrencyname(transactioncurrency.getCurrencyname());
        }
        //实体关系[DER1N_PRODUCTASSOCIATION__UOM__UOMID]
        if(!ObjectUtils.isEmpty(et.getUomid())){
            cn.ibizlab.businesscentral.core.base.domain.Uom uom=et.getUom();
            if(ObjectUtils.isEmpty(uom)){
                cn.ibizlab.businesscentral.core.base.domain.Uom majorEntity=uomService.get(et.getUomid());
                et.setUom(majorEntity);
                uom=majorEntity;
            }
            et.setUomname(uom.getUomname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<ProductAssociation> getProductassociationByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<ProductAssociation> getProductassociationByEntities(List<ProductAssociation> entities) {
        List ids =new ArrayList();
        for(ProductAssociation entity : entities){
            Serializable id=entity.getProductassociationid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}



