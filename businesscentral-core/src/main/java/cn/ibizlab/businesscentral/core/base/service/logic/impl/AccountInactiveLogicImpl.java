package cn.ibizlab.businesscentral.core.base.service.logic.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieContainer;

import cn.ibizlab.businesscentral.core.base.service.logic.IAccountInactiveLogic;
import cn.ibizlab.businesscentral.core.base.domain.Account;

/**
 * 关系型数据实体[Inactive] 对象
 */
@Slf4j
@Service
public class AccountInactiveLogicImpl implements IAccountInactiveLogic{

    @Autowired
    private KieContainer kieContainer;

    @Autowired
    private cn.ibizlab.businesscentral.core.base.service.IAccountService accountservice;

    public cn.ibizlab.businesscentral.core.base.service.IAccountService getAccountService() {
        return this.accountservice;
    }


    @Autowired
    private cn.ibizlab.businesscentral.core.base.service.IAccountService iBzSysDefaultService;

    public cn.ibizlab.businesscentral.core.base.service.IAccountService getIBzSysDefaultService() {
        return this.iBzSysDefaultService;
    }

    public void execute(Account et){

          KieSession kieSession = null;
        try{
           kieSession=kieContainer.newKieSession();
           kieSession.insert(et); 
           kieSession.setGlobal("accountinactivedefault",et);
           kieSession.setGlobal("accountservice",accountservice);
           kieSession.setGlobal("iBzSysAccountDefaultService",iBzSysDefaultService);
           kieSession.setGlobal("curuser", cn.ibizlab.businesscentral.util.security.AuthenticationUser.getAuthenticationUser());
           kieSession.startProcess("cn.ibizlab.businesscentral.core.base.service.logic.accountinactive");

        }catch(Exception e){
            throw new RuntimeException("执行[停用]处理逻辑发生异常"+e);
        }finally {
            if(kieSession!=null)
            kieSession.destroy();
        }
    }

}
