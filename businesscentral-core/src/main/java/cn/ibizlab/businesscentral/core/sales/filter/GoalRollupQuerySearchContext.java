package cn.ibizlab.businesscentral.core.sales.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.sales.domain.GoalRollupQuery;
/**
 * 关系型数据实体[GoalRollupQuery] 查询条件对象
 */
@Slf4j
@Data
public class GoalRollupQuerySearchContext extends QueryWrapperContext<GoalRollupQuery> {

	private Integer n_statecode_eq;//[状态]
	public void setN_statecode_eq(Integer n_statecode_eq) {
        this.n_statecode_eq = n_statecode_eq;
        if(!ObjectUtils.isEmpty(this.n_statecode_eq)){
            this.getSearchCond().eq("statecode", n_statecode_eq);
        }
    }
	private Integer n_statuscode_eq;//[状态描述]
	public void setN_statuscode_eq(Integer n_statuscode_eq) {
        this.n_statuscode_eq = n_statuscode_eq;
        if(!ObjectUtils.isEmpty(this.n_statuscode_eq)){
            this.getSearchCond().eq("statuscode", n_statuscode_eq);
        }
    }
	private String n_goalrollupqueryname_like;//[目标汇总查询名称]
	public void setN_goalrollupqueryname_like(String n_goalrollupqueryname_like) {
        this.n_goalrollupqueryname_like = n_goalrollupqueryname_like;
        if(!ObjectUtils.isEmpty(this.n_goalrollupqueryname_like)){
            this.getSearchCond().like("goalrollupqueryname", n_goalrollupqueryname_like);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("goalrollupqueryname", query)   
            );
		 }
	}
}



