package cn.ibizlab.businesscentral.core.base.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[知识文章]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "KNOWLEDGEARTICLE",resultMap = "KnowledgeArticleResultMap")
public class KnowledgeArticle extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 知识文章视图(上次更新时间)
     */
    @DEField(name = "knowledgearticleviews_date")
    @TableField(value = "knowledgearticleviews_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "knowledgearticleviews_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("knowledgearticleviews_date")
    private Timestamp knowledgearticleviewsDate;
    /**
     * 关键字
     */
    @TableField(value = "keywords")
    @JSONField(name = "keywords")
    @JsonProperty("keywords")
    private String keywords;
    /**
     * 状态描述
     */
    @TableField(value = "statuscode")
    @JSONField(name = "statuscode")
    @JsonProperty("statuscode")
    private Integer statuscode;
    /**
     * 过期状态 ID
     */
    @TableField(value = "expirationstateid")
    @JSONField(name = "expirationstateid")
    @JsonProperty("expirationstateid")
    private Integer expirationstateid;
    /**
     * 主要文章
     */
    @DEField(defaultValue = "0")
    @TableField(value = "isprimary")
    @JSONField(name = "isprimary")
    @JsonProperty("isprimary")
    private Integer isprimary;
    /**
     * Rating(Count)
     */
    @DEField(name = "rating_count")
    @TableField(value = "rating_count")
    @JSONField(name = "rating_count")
    @JsonProperty("rating_count")
    private Integer ratingCount;
    /**
     * 内容
     */
    @TableField(value = "content")
    @JSONField(name = "content")
    @JsonProperty("content")
    private String content;
    /**
     * 负责人
     */
    @TableField(value = "ownerid")
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;
    /**
     * RootArticled
     */
    @DEField(defaultValue = "0")
    @TableField(value = "rootarticle")
    @JSONField(name = "rootarticled")
    @JsonProperty("rootarticled")
    private Integer rootarticled;
    /**
     * 知识文章视图(状态)
     */
    @DEField(name = "knowledgearticleviews_state")
    @TableField(value = "knowledgearticleviews_state")
    @JSONField(name = "knowledgearticleviews_state")
    @JsonProperty("knowledgearticleviews_state")
    private Integer knowledgearticleviewsState;
    /**
     * Rating(sum)
     */
    @DEField(name = "rating_sum")
    @TableField(value = "rating_sum")
    @JSONField(name = "rating_sum")
    @JsonProperty("rating_sum")
    private BigDecimal ratingSum;
    /**
     * 状态
     */
    @TableField(value = "statecode")
    @JSONField(name = "statecode")
    @JsonProperty("statecode")
    private Integer statecode;
    /**
     * 遍历的路径
     */
    @TableField(value = "traversedpath")
    @JSONField(name = "traversedpath")
    @JsonProperty("traversedpath")
    private String traversedpath;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 已发布状态
     */
    @TableField(value = "publishstatusid")
    @JSONField(name = "publishstatusid")
    @JsonProperty("publishstatusid")
    private Integer publishstatusid;
    /**
     * 负责人 ID 类型
     */
    @TableField(value = "ownertype")
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;
    /**
     * 文章公共编号
     */
    @TableField(value = "articlepublicnumber")
    @JSONField(name = "articlepublicnumber")
    @JsonProperty("articlepublicnumber")
    private String articlepublicnumber;
    /**
     * primaryauthoridName
     */
    @TableField(value = "primaryauthoridname")
    @JSONField(name = "primaryauthoridname")
    @JsonProperty("primaryauthoridname")
    private String primaryauthoridname;
    /**
     * 记录创建日期
     */
    @TableField(value = "overriddencreatedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;
    /**
     * 主要作者 ID
     */
    @TableField(value = "primaryauthorid")
    @JSONField(name = "primaryauthorid")
    @JsonProperty("primaryauthorid")
    private String primaryauthorid;
    /**
     * 是最新版本
     */
    @DEField(defaultValue = "0")
    @TableField(value = "latestversion")
    @JSONField(name = "latestversion")
    @JsonProperty("latestversion")
    private Integer latestversion;
    /**
     * 评分(状态)
     */
    @DEField(name = "rating_state")
    @TableField(value = "rating_state")
    @JSONField(name = "rating_state")
    @JsonProperty("rating_state")
    private Integer ratingState;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * VersionNumber
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * UTC 转换时区代码
     */
    @TableField(value = "utcconversiontimezonecode")
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;
    /**
     * 已过期状态
     */
    @TableField(value = "expirationstatusid")
    @JSONField(name = "expirationstatusid")
    @JsonProperty("expirationstatusid")
    private Integer expirationstatusid;
    /**
     * 知识文章
     */
    @DEField(isKeyField=true)
    @TableId(value= "knowledgearticleid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "knowledgearticleid")
    @JsonProperty("knowledgearticleid")
    private String knowledgearticleid;
    /**
     * 设置类别关联
     */
    @DEField(defaultValue = "0")
    @TableField(value = "setcategoryassociations")
    @JSONField(name = "setcategoryassociations")
    @JsonProperty("setcategoryassociations")
    private Integer setcategoryassociations;
    /**
     * 说明
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 设置产品关联
     */
    @DEField(defaultValue = "0")
    @TableField(value = "setproductassociations")
    @JSONField(name = "setproductassociations")
    @JsonProperty("setproductassociations")
    private Integer setproductassociations;
    /**
     * 次要版本号
     */
    @TableField(value = "minorversionnumber")
    @JSONField(name = "minorversionnumber")
    @JsonProperty("minorversionnumber")
    private Integer minorversionnumber;
    /**
     * 阶段 ID
     */
    @TableField(value = "stageid")
    @JSONField(name = "stageid")
    @JsonProperty("stageid")
    private String stageid;
    /**
     * 进程 ID
     */
    @TableField(value = "processid")
    @JSONField(name = "processid")
    @JsonProperty("processid")
    private String processid;
    /**
     * 主要版本号
     */
    @TableField(value = "majorversionnumber")
    @JSONField(name = "majorversionnumber")
    @JsonProperty("majorversionnumber")
    private Integer majorversionnumber;
    /**
     * 评分
     */
    @TableField(value = "rating")
    @JSONField(name = "rating")
    @JsonProperty("rating")
    private BigDecimal rating;
    /**
     * 发布日期
     */
    @TableField(value = "publishon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "publishon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("publishon")
    private Timestamp publishon;
    /**
     * 知识文章视图
     */
    @TableField(value = "knowledgearticleviews")
    @JSONField(name = "knowledgearticleviews")
    @JsonProperty("knowledgearticleviews")
    private Integer knowledgearticleviews;
    /**
     * 评分(上次更新时间)
     */
    @DEField(name = "rating_date")
    @TableField(value = "rating_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "rating_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("rating_date")
    private Timestamp ratingDate;
    /**
     * 汇率
     */
    @TableField(value = "exchangerate")
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;
    /**
     * 为审阅做准备
     */
    @DEField(defaultValue = "0")
    @TableField(value = "readyforreview")
    @JSONField(name = "readyforreview")
    @JsonProperty("readyforreview")
    private Integer readyforreview;
    /**
     * 导入序号
     */
    @TableField(value = "importsequencenumber")
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;
    /**
     * SubjectIdDsc
     */
    @TableField(value = "subjectiddsc")
    @JSONField(name = "subjectiddsc")
    @JsonProperty("subjectiddsc")
    private Integer subjectiddsc;
    /**
     * 负责人
     */
    @TableField(value = "ownername")
    @JSONField(name = "ownername")
    @JsonProperty("ownername")
    private String ownername;
    /**
     * 审阅
     */
    @TableField(value = "review")
    @JSONField(name = "review")
    @JsonProperty("review")
    private String review;
    /**
     * LanguageLocaleIdLocaleId
     */
    @TableField(value = "languagelocaleidlocaleid")
    @JSONField(name = "languagelocaleidlocaleid")
    @JsonProperty("languagelocaleidlocaleid")
    private Integer languagelocaleidlocaleid;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 已计划状态
     */
    @TableField(value = "scheduledstatusid")
    @JSONField(name = "scheduledstatusid")
    @JsonProperty("scheduledstatusid")
    private Integer scheduledstatusid;
    /**
     * 时区规则版本号
     */
    @TableField(value = "timezoneruleversionnumber")
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;
    /**
     * 内部
     */
    @DEField(defaultValue = "0")
    @TableField(value = "internal")
    @JSONField(name = "internal")
    @JsonProperty("internal")
    private Integer internal;
    /**
     * 标题
     */
    @TableField(value = "title")
    @JSONField(name = "title")
    @JsonProperty("title")
    private String title;
    /**
     * 更新内容
     */
    @DEField(defaultValue = "0")
    @TableField(value = "updatecontent")
    @JSONField(name = "updatecontent")
    @JsonProperty("updatecontent")
    private Integer updatecontent;
    /**
     * 到期日期
     */
    @TableField(value = "expirationdate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "expirationdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("expirationdate")
    private Timestamp expirationdate;
    /**
     * 已过期审阅选项
     */
    @TableField(value = "expiredreviewoptions")
    @JSONField(name = "expiredreviewoptions")
    @JsonProperty("expiredreviewoptions")
    private String expiredreviewoptions;
    /**
     * 货币
     */
    @TableField(value = "currencyname")
    @JSONField(name = "currencyname")
    @JsonProperty("currencyname")
    private String currencyname;
    /**
     * 上级文章
     */
    @TableField(value = "parentarticlecontentname")
    @JSONField(name = "parentarticlecontentname")
    @JsonProperty("parentarticlecontentname")
    private String parentarticlecontentname;
    /**
     * 根文章
     */
    @TableField(value = "rootarticlename")
    @JSONField(name = "rootarticlename")
    @JsonProperty("rootarticlename")
    private String rootarticlename;
    /**
     * 主题
     */
    @TableField(value = "subjectname")
    @JSONField(name = "subjectname")
    @JsonProperty("subjectname")
    private String subjectname;
    /**
     * 语言
     */
    @TableField(value = "languagelocalename")
    @JSONField(name = "languagelocalename")
    @JsonProperty("languagelocalename")
    private String languagelocalename;
    /**
     * 上篇文章
     */
    @TableField(value = "previousarticlecontentname")
    @JSONField(name = "previousarticlecontentname")
    @JsonProperty("previousarticlecontentname")
    private String previousarticlecontentname;
    /**
     * 上级文章
     */
    @TableField(value = "parentarticlecontentid")
    @JSONField(name = "parentarticlecontentid")
    @JsonProperty("parentarticlecontentid")
    private String parentarticlecontentid;
    /**
     * 上篇文章内容 ID
     */
    @TableField(value = "previousarticlecontentid")
    @JSONField(name = "previousarticlecontentid")
    @JsonProperty("previousarticlecontentid")
    private String previousarticlecontentid;
    /**
     * 货币
     */
    @TableField(value = "transactioncurrencyid")
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;
    /**
     * 根文章 ID
     */
    @TableField(value = "rootarticleid")
    @JSONField(name = "rootarticleid")
    @JsonProperty("rootarticleid")
    private String rootarticleid;
    /**
     * 主题
     */
    @TableField(value = "subjectid")
    @JSONField(name = "subjectid")
    @JsonProperty("subjectid")
    private String subjectid;
    /**
     * 语言
     */
    @TableField(value = "languagelocaleid")
    @JSONField(name = "languagelocaleid")
    @JsonProperty("languagelocaleid")
    private String languagelocaleid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.KnowledgeArticle parentarticlecontent;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.KnowledgeArticle previousarticlecontent;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.KnowledgeArticle rootarticle;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.LanguageLocale languagelocale;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.Subject subject;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency transactioncurrency;



    /**
     * 设置 [知识文章视图(上次更新时间)]
     */
    public void setKnowledgearticleviewsDate(Timestamp knowledgearticleviewsDate){
        this.knowledgearticleviewsDate = knowledgearticleviewsDate ;
        this.modify("knowledgearticleviews_date",knowledgearticleviewsDate);
    }

    /**
     * 格式化日期 [知识文章视图(上次更新时间)]
     */
    public String formatKnowledgearticleviewsDate(){
        if (this.knowledgearticleviewsDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(knowledgearticleviewsDate);
    }
    /**
     * 设置 [关键字]
     */
    public void setKeywords(String keywords){
        this.keywords = keywords ;
        this.modify("keywords",keywords);
    }

    /**
     * 设置 [状态描述]
     */
    public void setStatuscode(Integer statuscode){
        this.statuscode = statuscode ;
        this.modify("statuscode",statuscode);
    }

    /**
     * 设置 [过期状态 ID]
     */
    public void setExpirationstateid(Integer expirationstateid){
        this.expirationstateid = expirationstateid ;
        this.modify("expirationstateid",expirationstateid);
    }

    /**
     * 设置 [主要文章]
     */
    public void setIsprimary(Integer isprimary){
        this.isprimary = isprimary ;
        this.modify("isprimary",isprimary);
    }

    /**
     * 设置 [Rating(Count)]
     */
    public void setRatingCount(Integer ratingCount){
        this.ratingCount = ratingCount ;
        this.modify("rating_count",ratingCount);
    }

    /**
     * 设置 [内容]
     */
    public void setContent(String content){
        this.content = content ;
        this.modify("content",content);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnerid(String ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [RootArticled]
     */
    public void setRootarticled(Integer rootarticled){
        this.rootarticled = rootarticled ;
        this.modify("rootarticle",rootarticled);
    }

    /**
     * 设置 [知识文章视图(状态)]
     */
    public void setKnowledgearticleviewsState(Integer knowledgearticleviewsState){
        this.knowledgearticleviewsState = knowledgearticleviewsState ;
        this.modify("knowledgearticleviews_state",knowledgearticleviewsState);
    }

    /**
     * 设置 [Rating(sum)]
     */
    public void setRatingSum(BigDecimal ratingSum){
        this.ratingSum = ratingSum ;
        this.modify("rating_sum",ratingSum);
    }

    /**
     * 设置 [状态]
     */
    public void setStatecode(Integer statecode){
        this.statecode = statecode ;
        this.modify("statecode",statecode);
    }

    /**
     * 设置 [遍历的路径]
     */
    public void setTraversedpath(String traversedpath){
        this.traversedpath = traversedpath ;
        this.modify("traversedpath",traversedpath);
    }

    /**
     * 设置 [已发布状态]
     */
    public void setPublishstatusid(Integer publishstatusid){
        this.publishstatusid = publishstatusid ;
        this.modify("publishstatusid",publishstatusid);
    }

    /**
     * 设置 [负责人 ID 类型]
     */
    public void setOwnertype(String ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [文章公共编号]
     */
    public void setArticlepublicnumber(String articlepublicnumber){
        this.articlepublicnumber = articlepublicnumber ;
        this.modify("articlepublicnumber",articlepublicnumber);
    }

    /**
     * 设置 [primaryauthoridName]
     */
    public void setPrimaryauthoridname(String primaryauthoridname){
        this.primaryauthoridname = primaryauthoridname ;
        this.modify("primaryauthoridname",primaryauthoridname);
    }

    /**
     * 设置 [记录创建日期]
     */
    public void setOverriddencreatedon(Timestamp overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 格式化日期 [记录创建日期]
     */
    public String formatOverriddencreatedon(){
        if (this.overriddencreatedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(overriddencreatedon);
    }
    /**
     * 设置 [主要作者 ID]
     */
    public void setPrimaryauthorid(String primaryauthorid){
        this.primaryauthorid = primaryauthorid ;
        this.modify("primaryauthorid",primaryauthorid);
    }

    /**
     * 设置 [是最新版本]
     */
    public void setLatestversion(Integer latestversion){
        this.latestversion = latestversion ;
        this.modify("latestversion",latestversion);
    }

    /**
     * 设置 [评分(状态)]
     */
    public void setRatingState(Integer ratingState){
        this.ratingState = ratingState ;
        this.modify("rating_state",ratingState);
    }

    /**
     * 设置 [VersionNumber]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [UTC 转换时区代码]
     */
    public void setUtcconversiontimezonecode(Integer utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [已过期状态]
     */
    public void setExpirationstatusid(Integer expirationstatusid){
        this.expirationstatusid = expirationstatusid ;
        this.modify("expirationstatusid",expirationstatusid);
    }

    /**
     * 设置 [设置类别关联]
     */
    public void setSetcategoryassociations(Integer setcategoryassociations){
        this.setcategoryassociations = setcategoryassociations ;
        this.modify("setcategoryassociations",setcategoryassociations);
    }

    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [设置产品关联]
     */
    public void setSetproductassociations(Integer setproductassociations){
        this.setproductassociations = setproductassociations ;
        this.modify("setproductassociations",setproductassociations);
    }

    /**
     * 设置 [次要版本号]
     */
    public void setMinorversionnumber(Integer minorversionnumber){
        this.minorversionnumber = minorversionnumber ;
        this.modify("minorversionnumber",minorversionnumber);
    }

    /**
     * 设置 [阶段 ID]
     */
    public void setStageid(String stageid){
        this.stageid = stageid ;
        this.modify("stageid",stageid);
    }

    /**
     * 设置 [进程 ID]
     */
    public void setProcessid(String processid){
        this.processid = processid ;
        this.modify("processid",processid);
    }

    /**
     * 设置 [主要版本号]
     */
    public void setMajorversionnumber(Integer majorversionnumber){
        this.majorversionnumber = majorversionnumber ;
        this.modify("majorversionnumber",majorversionnumber);
    }

    /**
     * 设置 [评分]
     */
    public void setRating(BigDecimal rating){
        this.rating = rating ;
        this.modify("rating",rating);
    }

    /**
     * 设置 [发布日期]
     */
    public void setPublishon(Timestamp publishon){
        this.publishon = publishon ;
        this.modify("publishon",publishon);
    }

    /**
     * 格式化日期 [发布日期]
     */
    public String formatPublishon(){
        if (this.publishon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(publishon);
    }
    /**
     * 设置 [知识文章视图]
     */
    public void setKnowledgearticleviews(Integer knowledgearticleviews){
        this.knowledgearticleviews = knowledgearticleviews ;
        this.modify("knowledgearticleviews",knowledgearticleviews);
    }

    /**
     * 设置 [评分(上次更新时间)]
     */
    public void setRatingDate(Timestamp ratingDate){
        this.ratingDate = ratingDate ;
        this.modify("rating_date",ratingDate);
    }

    /**
     * 格式化日期 [评分(上次更新时间)]
     */
    public String formatRatingDate(){
        if (this.ratingDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(ratingDate);
    }
    /**
     * 设置 [汇率]
     */
    public void setExchangerate(BigDecimal exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [为审阅做准备]
     */
    public void setReadyforreview(Integer readyforreview){
        this.readyforreview = readyforreview ;
        this.modify("readyforreview",readyforreview);
    }

    /**
     * 设置 [导入序号]
     */
    public void setImportsequencenumber(Integer importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [SubjectIdDsc]
     */
    public void setSubjectiddsc(Integer subjectiddsc){
        this.subjectiddsc = subjectiddsc ;
        this.modify("subjectiddsc",subjectiddsc);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnername(String ownername){
        this.ownername = ownername ;
        this.modify("ownername",ownername);
    }

    /**
     * 设置 [审阅]
     */
    public void setReview(String review){
        this.review = review ;
        this.modify("review",review);
    }

    /**
     * 设置 [LanguageLocaleIdLocaleId]
     */
    public void setLanguagelocaleidlocaleid(Integer languagelocaleidlocaleid){
        this.languagelocaleidlocaleid = languagelocaleidlocaleid ;
        this.modify("languagelocaleidlocaleid",languagelocaleidlocaleid);
    }

    /**
     * 设置 [已计划状态]
     */
    public void setScheduledstatusid(Integer scheduledstatusid){
        this.scheduledstatusid = scheduledstatusid ;
        this.modify("scheduledstatusid",scheduledstatusid);
    }

    /**
     * 设置 [时区规则版本号]
     */
    public void setTimezoneruleversionnumber(Integer timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [内部]
     */
    public void setInternal(Integer internal){
        this.internal = internal ;
        this.modify("internal",internal);
    }

    /**
     * 设置 [标题]
     */
    public void setTitle(String title){
        this.title = title ;
        this.modify("title",title);
    }

    /**
     * 设置 [更新内容]
     */
    public void setUpdatecontent(Integer updatecontent){
        this.updatecontent = updatecontent ;
        this.modify("updatecontent",updatecontent);
    }

    /**
     * 设置 [到期日期]
     */
    public void setExpirationdate(Timestamp expirationdate){
        this.expirationdate = expirationdate ;
        this.modify("expirationdate",expirationdate);
    }

    /**
     * 格式化日期 [到期日期]
     */
    public String formatExpirationdate(){
        if (this.expirationdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(expirationdate);
    }
    /**
     * 设置 [已过期审阅选项]
     */
    public void setExpiredreviewoptions(String expiredreviewoptions){
        this.expiredreviewoptions = expiredreviewoptions ;
        this.modify("expiredreviewoptions",expiredreviewoptions);
    }

    /**
     * 设置 [货币]
     */
    public void setCurrencyname(String currencyname){
        this.currencyname = currencyname ;
        this.modify("currencyname",currencyname);
    }

    /**
     * 设置 [上级文章]
     */
    public void setParentarticlecontentname(String parentarticlecontentname){
        this.parentarticlecontentname = parentarticlecontentname ;
        this.modify("parentarticlecontentname",parentarticlecontentname);
    }

    /**
     * 设置 [根文章]
     */
    public void setRootarticlename(String rootarticlename){
        this.rootarticlename = rootarticlename ;
        this.modify("rootarticlename",rootarticlename);
    }

    /**
     * 设置 [主题]
     */
    public void setSubjectname(String subjectname){
        this.subjectname = subjectname ;
        this.modify("subjectname",subjectname);
    }

    /**
     * 设置 [语言]
     */
    public void setLanguagelocalename(String languagelocalename){
        this.languagelocalename = languagelocalename ;
        this.modify("languagelocalename",languagelocalename);
    }

    /**
     * 设置 [上篇文章]
     */
    public void setPreviousarticlecontentname(String previousarticlecontentname){
        this.previousarticlecontentname = previousarticlecontentname ;
        this.modify("previousarticlecontentname",previousarticlecontentname);
    }

    /**
     * 设置 [上级文章]
     */
    public void setParentarticlecontentid(String parentarticlecontentid){
        this.parentarticlecontentid = parentarticlecontentid ;
        this.modify("parentarticlecontentid",parentarticlecontentid);
    }

    /**
     * 设置 [上篇文章内容 ID]
     */
    public void setPreviousarticlecontentid(String previousarticlecontentid){
        this.previousarticlecontentid = previousarticlecontentid ;
        this.modify("previousarticlecontentid",previousarticlecontentid);
    }

    /**
     * 设置 [货币]
     */
    public void setTransactioncurrencyid(String transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }

    /**
     * 设置 [根文章 ID]
     */
    public void setRootarticleid(String rootarticleid){
        this.rootarticleid = rootarticleid ;
        this.modify("rootarticleid",rootarticleid);
    }

    /**
     * 设置 [主题]
     */
    public void setSubjectid(String subjectid){
        this.subjectid = subjectid ;
        this.modify("subjectid",subjectid);
    }

    /**
     * 设置 [语言]
     */
    public void setLanguagelocaleid(String languagelocaleid){
        this.languagelocaleid = languagelocaleid ;
        this.modify("languagelocaleid",languagelocaleid);
    }


}


