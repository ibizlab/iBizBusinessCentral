package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.OMHierarchyPurposeRef;
import cn.ibizlab.businesscentral.core.base.filter.OMHierarchyPurposeRefSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[OMHierarchyPurposeRef] 服务对象接口
 */
public interface IOMHierarchyPurposeRefService extends IService<OMHierarchyPurposeRef>{

    boolean create(OMHierarchyPurposeRef et) ;
    void createBatch(List<OMHierarchyPurposeRef> list) ;
    boolean update(OMHierarchyPurposeRef et) ;
    void updateBatch(List<OMHierarchyPurposeRef> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    OMHierarchyPurposeRef get(String key) ;
    OMHierarchyPurposeRef getDraft(OMHierarchyPurposeRef et) ;
    boolean checkKey(OMHierarchyPurposeRef et) ;
    boolean save(OMHierarchyPurposeRef et) ;
    void saveBatch(List<OMHierarchyPurposeRef> list) ;
    Page<OMHierarchyPurposeRef> searchDefault(OMHierarchyPurposeRefSearchContext context) ;
    List<OMHierarchyPurposeRef> selectByOmhierarchycatid(String omhierarchycatid) ;
    void removeByOmhierarchycatid(String omhierarchycatid) ;
    List<OMHierarchyPurposeRef> selectByOmhierarchypurposeid(String omhierarchypurposeid) ;
    void removeByOmhierarchypurposeid(String omhierarchypurposeid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<OMHierarchyPurposeRef> getOmhierarchypurposerefByIds(List<String> ids) ;
    List<OMHierarchyPurposeRef> getOmhierarchypurposerefByEntities(List<OMHierarchyPurposeRef> entities) ;
}


