package cn.ibizlab.businesscentral.core.product.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.product.domain.ProductPriceLevel;
import cn.ibizlab.businesscentral.core.product.filter.ProductPriceLevelSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[ProductPriceLevel] 服务对象接口
 */
public interface IProductPriceLevelService extends IService<ProductPriceLevel>{

    boolean create(ProductPriceLevel et) ;
    void createBatch(List<ProductPriceLevel> list) ;
    boolean update(ProductPriceLevel et) ;
    void updateBatch(List<ProductPriceLevel> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    ProductPriceLevel get(String key) ;
    ProductPriceLevel getDraft(ProductPriceLevel et) ;
    boolean checkKey(ProductPriceLevel et) ;
    boolean save(ProductPriceLevel et) ;
    void saveBatch(List<ProductPriceLevel> list) ;
    Page<ProductPriceLevel> searchDefault(ProductPriceLevelSearchContext context) ;
    List<ProductPriceLevel> selectByDiscounttypeid(String discounttypeid) ;
    void removeByDiscounttypeid(String discounttypeid) ;
    List<ProductPriceLevel> selectByPricelevelid(String pricelevelid) ;
    void removeByPricelevelid(String pricelevelid) ;
    List<ProductPriceLevel> selectByProductid(String productid) ;
    void removeByProductid(String productid) ;
    List<ProductPriceLevel> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    List<ProductPriceLevel> selectByUomscheduleid(String uomscheduleid) ;
    void removeByUomscheduleid(String uomscheduleid) ;
    List<ProductPriceLevel> selectByUomid(String uomid) ;
    void removeByUomid(String uomid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<ProductPriceLevel> getProductpricelevelByIds(List<String> ids) ;
    List<ProductPriceLevel> getProductpricelevelByEntities(List<ProductPriceLevel> entities) ;
}


