package cn.ibizlab.businesscentral.core.runtime.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[已过期流程]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "EXPIREDPROCESS",resultMap = "ExpiredprocessResultMap")
public class Expiredprocess extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 活动阶段开始日期
     */
    @TableField(value = "activestagestartedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activestagestartedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("activestagestartedon")
    private Timestamp activestagestartedon;
    /**
     * 过程阶段 ID 的以逗号分隔的字符串，这些 ID 表示业务流程实例的已访问阶段。
     */
    @TableField(value = "traversedpath")
    @JSONField(name = "traversedpath")
    @JsonProperty("traversedpath")
    private String traversedpath;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 状态
     */
    @TableField(value = "statecode")
    @JSONField(name = "statecode")
    @JsonProperty("statecode")
    private Integer statecode;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 汇率
     */
    @TableField(value = "exchangerate")
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 版本号
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * 流程
     */
    @TableField(value = "processid")
    @JSONField(name = "processid")
    @JsonProperty("processid")
    private String processid;
    /**
     * 持续时间
     */
    @TableField(value = "duration")
    @JSONField(name = "duration")
    @JsonProperty("duration")
    private Integer duration;
    /**
     * 活动阶段
     */
    @TableField(value = "activestageid")
    @JSONField(name = "activestageid")
    @JsonProperty("activestageid")
    private String activestageid;
    /**
     * 完成日期
     */
    @TableField(value = "completedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "completedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("completedon")
    private Timestamp completedon;
    /**
     * 已过期流程实例 ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "businessprocessflowinstanceid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "businessprocessflowinstanceid")
    @JsonProperty("businessprocessflowinstanceid")
    private String businessprocessflowinstanceid;
    /**
     * 导入序号
     */
    @TableField(value = "importsequencenumber")
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;
    /**
     * 状态描述
     */
    @TableField(value = "statuscode")
    @JSONField(name = "statuscode")
    @JsonProperty("statuscode")
    private Integer statuscode;
    /**
     * 记录创建时间
     */
    @TableField(value = "overriddencreatedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;
    /**
     * 过期进程名称
     */
    @TableField(value = "expiredprocessname")
    @JSONField(name = "expiredprocessname")
    @JsonProperty("expiredprocessname")
    private String expiredprocessname;
    /**
     * 知识文章
     */
    @TableField(value = "knowledgearticleid")
    @JSONField(name = "knowledgearticleid")
    @JsonProperty("knowledgearticleid")
    private String knowledgearticleid;
    /**
     * 货币
     */
    @TableField(value = "transactioncurrencyid")
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.KnowledgeArticle knowledgearticle;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency transactioncurrency;



    /**
     * 设置 [活动阶段开始日期]
     */
    public void setActivestagestartedon(Timestamp activestagestartedon){
        this.activestagestartedon = activestagestartedon ;
        this.modify("activestagestartedon",activestagestartedon);
    }

    /**
     * 格式化日期 [活动阶段开始日期]
     */
    public String formatActivestagestartedon(){
        if (this.activestagestartedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(activestagestartedon);
    }
    /**
     * 设置 [过程阶段 ID 的以逗号分隔的字符串，这些 ID 表示业务流程实例的已访问阶段。]
     */
    public void setTraversedpath(String traversedpath){
        this.traversedpath = traversedpath ;
        this.modify("traversedpath",traversedpath);
    }

    /**
     * 设置 [状态]
     */
    public void setStatecode(Integer statecode){
        this.statecode = statecode ;
        this.modify("statecode",statecode);
    }

    /**
     * 设置 [汇率]
     */
    public void setExchangerate(BigDecimal exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [版本号]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [流程]
     */
    public void setProcessid(String processid){
        this.processid = processid ;
        this.modify("processid",processid);
    }

    /**
     * 设置 [持续时间]
     */
    public void setDuration(Integer duration){
        this.duration = duration ;
        this.modify("duration",duration);
    }

    /**
     * 设置 [活动阶段]
     */
    public void setActivestageid(String activestageid){
        this.activestageid = activestageid ;
        this.modify("activestageid",activestageid);
    }

    /**
     * 设置 [完成日期]
     */
    public void setCompletedon(Timestamp completedon){
        this.completedon = completedon ;
        this.modify("completedon",completedon);
    }

    /**
     * 格式化日期 [完成日期]
     */
    public String formatCompletedon(){
        if (this.completedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(completedon);
    }
    /**
     * 设置 [导入序号]
     */
    public void setImportsequencenumber(Integer importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [状态描述]
     */
    public void setStatuscode(Integer statuscode){
        this.statuscode = statuscode ;
        this.modify("statuscode",statuscode);
    }

    /**
     * 设置 [记录创建时间]
     */
    public void setOverriddencreatedon(Timestamp overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 格式化日期 [记录创建时间]
     */
    public String formatOverriddencreatedon(){
        if (this.overriddencreatedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(overriddencreatedon);
    }
    /**
     * 设置 [过期进程名称]
     */
    public void setExpiredprocessname(String expiredprocessname){
        this.expiredprocessname = expiredprocessname ;
        this.modify("expiredprocessname",expiredprocessname);
    }

    /**
     * 设置 [知识文章]
     */
    public void setKnowledgearticleid(String knowledgearticleid){
        this.knowledgearticleid = knowledgearticleid ;
        this.modify("knowledgearticleid",knowledgearticleid);
    }

    /**
     * 设置 [货币]
     */
    public void setTransactioncurrencyid(String transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }


}


