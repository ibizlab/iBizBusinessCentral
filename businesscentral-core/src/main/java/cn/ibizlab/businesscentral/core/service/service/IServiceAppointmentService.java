package cn.ibizlab.businesscentral.core.service.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.service.domain.ServiceAppointment;
import cn.ibizlab.businesscentral.core.service.filter.ServiceAppointmentSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[ServiceAppointment] 服务对象接口
 */
public interface IServiceAppointmentService extends IService<ServiceAppointment>{

    boolean create(ServiceAppointment et) ;
    void createBatch(List<ServiceAppointment> list) ;
    boolean update(ServiceAppointment et) ;
    void updateBatch(List<ServiceAppointment> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    ServiceAppointment get(String key) ;
    ServiceAppointment getDraft(ServiceAppointment et) ;
    boolean checkKey(ServiceAppointment et) ;
    boolean save(ServiceAppointment et) ;
    void saveBatch(List<ServiceAppointment> list) ;
    Page<ServiceAppointment> searchDefault(ServiceAppointmentSearchContext context) ;
    List<ServiceAppointment> selectByServiceid(String serviceid) ;
    void removeByServiceid(String serviceid) ;
    List<ServiceAppointment> selectBySiteid(String siteid) ;
    void removeBySiteid(String siteid) ;
    List<ServiceAppointment> selectBySlaid(String slaid) ;
    void removeBySlaid(String slaid) ;
    List<ServiceAppointment> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<ServiceAppointment> getServiceappointmentByIds(List<String> ids) ;
    List<ServiceAppointment> getServiceappointmentByEntities(List<ServiceAppointment> entities) ;
}


