package cn.ibizlab.businesscentral.core.product.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.product.domain.ProductSubstitute;
/**
 * 关系型数据实体[ProductSubstitute] 查询条件对象
 */
@Slf4j
@Data
public class ProductSubstituteSearchContext extends QueryWrapperContext<ProductSubstitute> {

	private String n_productsubstitutename_like;//[产品子套件名称]
	public void setN_productsubstitutename_like(String n_productsubstitutename_like) {
        this.n_productsubstitutename_like = n_productsubstitutename_like;
        if(!ObjectUtils.isEmpty(this.n_productsubstitutename_like)){
            this.getSearchCond().like("productsubstitutename", n_productsubstitutename_like);
        }
    }
	private Integer n_statuscode_eq;//[状态描述]
	public void setN_statuscode_eq(Integer n_statuscode_eq) {
        this.n_statuscode_eq = n_statuscode_eq;
        if(!ObjectUtils.isEmpty(this.n_statuscode_eq)){
            this.getSearchCond().eq("statuscode", n_statuscode_eq);
        }
    }
	private String n_direction_eq;//[方向]
	public void setN_direction_eq(String n_direction_eq) {
        this.n_direction_eq = n_direction_eq;
        if(!ObjectUtils.isEmpty(this.n_direction_eq)){
            this.getSearchCond().eq("direction", n_direction_eq);
        }
    }
	private String n_salesrelationshiptype_eq;//[销售关系类型]
	public void setN_salesrelationshiptype_eq(String n_salesrelationshiptype_eq) {
        this.n_salesrelationshiptype_eq = n_salesrelationshiptype_eq;
        if(!ObjectUtils.isEmpty(this.n_salesrelationshiptype_eq)){
            this.getSearchCond().eq("salesrelationshiptype", n_salesrelationshiptype_eq);
        }
    }
	private Integer n_statecode_eq;//[状态]
	public void setN_statecode_eq(Integer n_statecode_eq) {
        this.n_statecode_eq = n_statecode_eq;
        if(!ObjectUtils.isEmpty(this.n_statecode_eq)){
            this.getSearchCond().eq("statecode", n_statecode_eq);
        }
    }
	private String n_productname_eq;//[产品]
	public void setN_productname_eq(String n_productname_eq) {
        this.n_productname_eq = n_productname_eq;
        if(!ObjectUtils.isEmpty(this.n_productname_eq)){
            this.getSearchCond().eq("productname", n_productname_eq);
        }
    }
	private String n_productname_like;//[产品]
	public void setN_productname_like(String n_productname_like) {
        this.n_productname_like = n_productname_like;
        if(!ObjectUtils.isEmpty(this.n_productname_like)){
            this.getSearchCond().like("productname", n_productname_like);
        }
    }
	private String n_substitutedproductname_eq;//[相关产品]
	public void setN_substitutedproductname_eq(String n_substitutedproductname_eq) {
        this.n_substitutedproductname_eq = n_substitutedproductname_eq;
        if(!ObjectUtils.isEmpty(this.n_substitutedproductname_eq)){
            this.getSearchCond().eq("substitutedproductname", n_substitutedproductname_eq);
        }
    }
	private String n_substitutedproductname_like;//[相关产品]
	public void setN_substitutedproductname_like(String n_substitutedproductname_like) {
        this.n_substitutedproductname_like = n_substitutedproductname_like;
        if(!ObjectUtils.isEmpty(this.n_substitutedproductname_like)){
            this.getSearchCond().like("substitutedproductname", n_substitutedproductname_like);
        }
    }
	private String n_currencyname_eq;//[货币]
	public void setN_currencyname_eq(String n_currencyname_eq) {
        this.n_currencyname_eq = n_currencyname_eq;
        if(!ObjectUtils.isEmpty(this.n_currencyname_eq)){
            this.getSearchCond().eq("currencyname", n_currencyname_eq);
        }
    }
	private String n_currencyname_like;//[货币]
	public void setN_currencyname_like(String n_currencyname_like) {
        this.n_currencyname_like = n_currencyname_like;
        if(!ObjectUtils.isEmpty(this.n_currencyname_like)){
            this.getSearchCond().like("currencyname", n_currencyname_like);
        }
    }
	private String n_productid_eq;//[产品]
	public void setN_productid_eq(String n_productid_eq) {
        this.n_productid_eq = n_productid_eq;
        if(!ObjectUtils.isEmpty(this.n_productid_eq)){
            this.getSearchCond().eq("productid", n_productid_eq);
        }
    }
	private String n_substitutedproductid_eq;//[相关产品]
	public void setN_substitutedproductid_eq(String n_substitutedproductid_eq) {
        this.n_substitutedproductid_eq = n_substitutedproductid_eq;
        if(!ObjectUtils.isEmpty(this.n_substitutedproductid_eq)){
            this.getSearchCond().eq("substitutedproductid", n_substitutedproductid_eq);
        }
    }
	private String n_transactioncurrencyid_eq;//[货币]
	public void setN_transactioncurrencyid_eq(String n_transactioncurrencyid_eq) {
        this.n_transactioncurrencyid_eq = n_transactioncurrencyid_eq;
        if(!ObjectUtils.isEmpty(this.n_transactioncurrencyid_eq)){
            this.getSearchCond().eq("transactioncurrencyid", n_transactioncurrencyid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("productsubstitutename", query)   
            );
		 }
	}
}



