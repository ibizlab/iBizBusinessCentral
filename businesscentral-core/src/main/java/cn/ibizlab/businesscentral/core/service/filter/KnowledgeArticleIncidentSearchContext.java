package cn.ibizlab.businesscentral.core.service.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.service.domain.KnowledgeArticleIncident;
/**
 * 关系型数据实体[KnowledgeArticleIncident] 查询条件对象
 */
@Slf4j
@Data
public class KnowledgeArticleIncidentSearchContext extends QueryWrapperContext<KnowledgeArticleIncident> {

	private String n_knowledgeusage_eq;//[知识使用情况]
	public void setN_knowledgeusage_eq(String n_knowledgeusage_eq) {
        this.n_knowledgeusage_eq = n_knowledgeusage_eq;
        if(!ObjectUtils.isEmpty(this.n_knowledgeusage_eq)){
            this.getSearchCond().eq("knowledgeusage", n_knowledgeusage_eq);
        }
    }
	private Integer n_statecode_eq;//[状态]
	public void setN_statecode_eq(Integer n_statecode_eq) {
        this.n_statecode_eq = n_statecode_eq;
        if(!ObjectUtils.isEmpty(this.n_statecode_eq)){
            this.getSearchCond().eq("statecode", n_statecode_eq);
        }
    }
	private Integer n_statuscode_eq;//[状态描述]
	public void setN_statuscode_eq(Integer n_statuscode_eq) {
        this.n_statuscode_eq = n_statuscode_eq;
        if(!ObjectUtils.isEmpty(this.n_statuscode_eq)){
            this.getSearchCond().eq("statuscode", n_statuscode_eq);
        }
    }
	private String n_knowledgearticlename_eq;//[知识文章]
	public void setN_knowledgearticlename_eq(String n_knowledgearticlename_eq) {
        this.n_knowledgearticlename_eq = n_knowledgearticlename_eq;
        if(!ObjectUtils.isEmpty(this.n_knowledgearticlename_eq)){
            this.getSearchCond().eq("knowledgearticlename", n_knowledgearticlename_eq);
        }
    }
	private String n_knowledgearticlename_like;//[知识文章]
	public void setN_knowledgearticlename_like(String n_knowledgearticlename_like) {
        this.n_knowledgearticlename_like = n_knowledgearticlename_like;
        if(!ObjectUtils.isEmpty(this.n_knowledgearticlename_like)){
            this.getSearchCond().like("knowledgearticlename", n_knowledgearticlename_like);
        }
    }
	private String n_incidentname_eq;//[服务案例]
	public void setN_incidentname_eq(String n_incidentname_eq) {
        this.n_incidentname_eq = n_incidentname_eq;
        if(!ObjectUtils.isEmpty(this.n_incidentname_eq)){
            this.getSearchCond().eq("incidentname", n_incidentname_eq);
        }
    }
	private String n_incidentname_like;//[服务案例]
	public void setN_incidentname_like(String n_incidentname_like) {
        this.n_incidentname_like = n_incidentname_like;
        if(!ObjectUtils.isEmpty(this.n_incidentname_like)){
            this.getSearchCond().like("incidentname", n_incidentname_like);
        }
    }
	private String n_incidentid_eq;//[事件]
	public void setN_incidentid_eq(String n_incidentid_eq) {
        this.n_incidentid_eq = n_incidentid_eq;
        if(!ObjectUtils.isEmpty(this.n_incidentid_eq)){
            this.getSearchCond().eq("incidentid", n_incidentid_eq);
        }
    }
	private String n_knowledgearticleid_eq;//[知识文章]
	public void setN_knowledgearticleid_eq(String n_knowledgearticleid_eq) {
        this.n_knowledgearticleid_eq = n_knowledgearticleid_eq;
        if(!ObjectUtils.isEmpty(this.n_knowledgearticleid_eq)){
            this.getSearchCond().eq("knowledgearticleid", n_knowledgearticleid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("statecode", query)   
            );
		 }
	}
}



