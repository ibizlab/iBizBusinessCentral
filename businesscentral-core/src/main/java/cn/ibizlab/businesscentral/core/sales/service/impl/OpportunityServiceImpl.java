package cn.ibizlab.businesscentral.core.sales.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.sales.domain.Opportunity;
import cn.ibizlab.businesscentral.core.sales.filter.OpportunitySearchContext;
import cn.ibizlab.businesscentral.core.sales.service.IOpportunityService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.sales.mapper.OpportunityMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[商机] 服务对象接口实现
 */
@Slf4j
@Service("OpportunityServiceImpl")
public class OpportunityServiceImpl extends ServiceImpl<OpportunityMapper, Opportunity> implements IOpportunityService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.finance.service.IInvoiceService invoiceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.ILeadService leadService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.IOpportunityCloseService opportunitycloseService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.IOpportunityCompetitorService opportunitycompetitorService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.IOpportunityProductService opportunityproductService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.IQuoteService quoteService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.ISalesOrderService salesorderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IAccountService accountService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.marketing.service.ICampaignService campaignService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IContactService contactService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.product.service.IPriceLevelService pricelevelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ISlaService slaService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITransactionCurrencyService transactioncurrencyService;

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.logic.IOpportunityActiveLogic activeLogic;

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.logic.IOpportunityLostLogic lostLogic;

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.logic.IOpportunityWinLogic winLogic;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(Opportunity et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getOpportunityid()),et);
        return true;
    }

    @Override
    public void createBatch(List<Opportunity> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Opportunity et) {
        fillParentData(et);
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("opportunityid",et.getOpportunityid())))
            return false;
        CachedBeanCopier.copy(get(et.getOpportunityid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<Opportunity> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Opportunity get(String key) {
        Opportunity et = getById(key);
        if(et==null){
            et=new Opportunity();
            et.setOpportunityid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Opportunity getDraft(Opportunity et) {
        fillParentData(et);
        return et;
    }

    @Override
    @Transactional
    public Opportunity active(Opportunity et) {
        activeLogic.execute(et);
         return et ;
    }

    @Override
    public boolean checkKey(Opportunity et) {
        return (!ObjectUtils.isEmpty(et.getOpportunityid()))&&(!Objects.isNull(this.getById(et.getOpportunityid())));
    }
    @Override
    @Transactional
    public Opportunity lose(Opportunity et) {
        lostLogic.execute(et);
         return et ;
    }

    @Override
    @Transactional
    public boolean save(Opportunity et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Opportunity et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<Opportunity> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<Opportunity> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }

    @Override
    @Transactional
    public Opportunity win(Opportunity et) {
        winLogic.execute(et);
         return et ;
    }


	@Override
    public List<Opportunity> selectByParentaccountid(String accountid) {
        return baseMapper.selectByParentaccountid(accountid);
    }

    @Override
    public void removeByParentaccountid(String accountid) {
        this.remove(new QueryWrapper<Opportunity>().eq("parentaccountid",accountid));
    }

	@Override
    public List<Opportunity> selectByCampaignid(String campaignid) {
        return baseMapper.selectByCampaignid(campaignid);
    }

    @Override
    public void removeByCampaignid(String campaignid) {
        this.remove(new QueryWrapper<Opportunity>().eq("campaignid",campaignid));
    }

	@Override
    public List<Opportunity> selectByParentcontactid(String contactid) {
        return baseMapper.selectByParentcontactid(contactid);
    }

    @Override
    public void removeByParentcontactid(String contactid) {
        this.remove(new QueryWrapper<Opportunity>().eq("parentcontactid",contactid));
    }

	@Override
    public List<Opportunity> selectByOriginatingleadid(String leadid) {
        return baseMapper.selectByOriginatingleadid(leadid);
    }

    @Override
    public void removeByOriginatingleadid(String leadid) {
        this.remove(new QueryWrapper<Opportunity>().eq("originatingleadid",leadid));
    }

	@Override
    public List<Opportunity> selectByPricelevelid(String pricelevelid) {
        return baseMapper.selectByPricelevelid(pricelevelid);
    }

    @Override
    public void removeByPricelevelid(String pricelevelid) {
        this.remove(new QueryWrapper<Opportunity>().eq("pricelevelid",pricelevelid));
    }

	@Override
    public List<Opportunity> selectBySlaid(String slaid) {
        return baseMapper.selectBySlaid(slaid);
    }

    @Override
    public void removeBySlaid(String slaid) {
        this.remove(new QueryWrapper<Opportunity>().eq("slaid",slaid));
    }

	@Override
    public List<Opportunity> selectByTransactioncurrencyid(String transactioncurrencyid) {
        return baseMapper.selectByTransactioncurrencyid(transactioncurrencyid);
    }

    @Override
    public void removeByTransactioncurrencyid(String transactioncurrencyid) {
        this.remove(new QueryWrapper<Opportunity>().eq("transactioncurrencyid",transactioncurrencyid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<Opportunity> searchDefault(OpportunitySearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Opportunity> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Opportunity>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 丢单商机
     */
    @Override
    public Page<Opportunity> searchLost(OpportunitySearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Opportunity> pages=baseMapper.searchLost(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Opportunity>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 Top5
     */
    @Override
    public Page<Opportunity> searchTop5(OpportunitySearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Opportunity> pages=baseMapper.searchTop5(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Opportunity>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 赢单商机
     */
    @Override
    public Page<Opportunity> searchWin(OpportunitySearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Opportunity> pages=baseMapper.searchWin(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Opportunity>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Opportunity et){
        //实体关系[DER1N_OPPORTUNITY__ACCOUNT__PARENTACCOUNTID]
        if(!ObjectUtils.isEmpty(et.getParentaccountid())){
            cn.ibizlab.businesscentral.core.base.domain.Account parentaccount=et.getParentaccount();
            if(ObjectUtils.isEmpty(parentaccount)){
                cn.ibizlab.businesscentral.core.base.domain.Account majorEntity=accountService.get(et.getParentaccountid());
                et.setParentaccount(majorEntity);
                parentaccount=majorEntity;
            }
            et.setParentaccountname(parentaccount.getAccountname());
        }
        //实体关系[DER1N_OPPORTUNITY__CAMPAIGN__CAMPAIGNID]
        if(!ObjectUtils.isEmpty(et.getCampaignid())){
            cn.ibizlab.businesscentral.core.marketing.domain.Campaign campaign=et.getCampaign();
            if(ObjectUtils.isEmpty(campaign)){
                cn.ibizlab.businesscentral.core.marketing.domain.Campaign majorEntity=campaignService.get(et.getCampaignid());
                et.setCampaign(majorEntity);
                campaign=majorEntity;
            }
            et.setCampaignname(campaign.getCampaignname());
        }
        //实体关系[DER1N_OPPORTUNITY__CONTACT__PARENTCONTACTID]
        if(!ObjectUtils.isEmpty(et.getParentcontactid())){
            cn.ibizlab.businesscentral.core.base.domain.Contact parentcontact=et.getParentcontact();
            if(ObjectUtils.isEmpty(parentcontact)){
                cn.ibizlab.businesscentral.core.base.domain.Contact majorEntity=contactService.get(et.getParentcontactid());
                et.setParentcontact(majorEntity);
                parentcontact=majorEntity;
            }
            et.setParentcontactname(parentcontact.getFullname());
        }
        //实体关系[DER1N_OPPORTUNITY__LEAD__ORIGINATINGLEADID]
        if(!ObjectUtils.isEmpty(et.getOriginatingleadid())){
            cn.ibizlab.businesscentral.core.sales.domain.Lead originatinglead=et.getOriginatinglead();
            if(ObjectUtils.isEmpty(originatinglead)){
                cn.ibizlab.businesscentral.core.sales.domain.Lead majorEntity=leadService.get(et.getOriginatingleadid());
                et.setOriginatinglead(majorEntity);
                originatinglead=majorEntity;
            }
            et.setOriginatingleadname(originatinglead.getFullname());
        }
        //实体关系[DER1N_OPPORTUNITY__PRICELEVEL__PRICELEVELID]
        if(!ObjectUtils.isEmpty(et.getPricelevelid())){
            cn.ibizlab.businesscentral.core.product.domain.PriceLevel pricelevel=et.getPricelevel();
            if(ObjectUtils.isEmpty(pricelevel)){
                cn.ibizlab.businesscentral.core.product.domain.PriceLevel majorEntity=pricelevelService.get(et.getPricelevelid());
                et.setPricelevel(majorEntity);
                pricelevel=majorEntity;
            }
            et.setPricelevelname(pricelevel.getPricelevelname());
        }
        //实体关系[DER1N_OPPORTUNITY__SLA__SLAID]
        if(!ObjectUtils.isEmpty(et.getSlaid())){
            cn.ibizlab.businesscentral.core.base.domain.Sla sla=et.getSla();
            if(ObjectUtils.isEmpty(sla)){
                cn.ibizlab.businesscentral.core.base.domain.Sla majorEntity=slaService.get(et.getSlaid());
                et.setSla(majorEntity);
                sla=majorEntity;
            }
            et.setSlaname(sla.getSlaname());
        }
        //实体关系[DER1N_OPPORTUNITY__TRANSACTIONCURRENCY__TRANSACTIONCURRENCYID]
        if(!ObjectUtils.isEmpty(et.getTransactioncurrencyid())){
            cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency transactioncurrency=et.getTransactioncurrency();
            if(ObjectUtils.isEmpty(transactioncurrency)){
                cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency majorEntity=transactioncurrencyService.get(et.getTransactioncurrencyid());
                et.setTransactioncurrency(majorEntity);
                transactioncurrency=majorEntity;
            }
            et.setCurrencyname(transactioncurrency.getCurrencyname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Opportunity> getOpportunityByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Opportunity> getOpportunityByEntities(List<Opportunity> entities) {
        List ids =new ArrayList();
        for(Opportunity entity : entities){
            Serializable id=entity.getOpportunityid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}



