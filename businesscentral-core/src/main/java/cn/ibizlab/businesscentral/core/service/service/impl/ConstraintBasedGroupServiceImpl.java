package cn.ibizlab.businesscentral.core.service.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.service.domain.ConstraintBasedGroup;
import cn.ibizlab.businesscentral.core.service.filter.ConstraintBasedGroupSearchContext;
import cn.ibizlab.businesscentral.core.service.service.IConstraintBasedGroupService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.service.mapper.ConstraintBasedGroupMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[资源组] 服务对象接口实现
 */
@Slf4j
@Service("ConstraintBasedGroupServiceImpl")
public class ConstraintBasedGroupServiceImpl extends ServiceImpl<ConstraintBasedGroupMapper, ConstraintBasedGroup> implements IConstraintBasedGroupService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IBusinessUnitService businessunitService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(ConstraintBasedGroup et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getConstraintbasedgroupid()),et);
        return true;
    }

    @Override
    public void createBatch(List<ConstraintBasedGroup> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(ConstraintBasedGroup et) {
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("constraintbasedgroupid",et.getConstraintbasedgroupid())))
            return false;
        CachedBeanCopier.copy(get(et.getConstraintbasedgroupid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<ConstraintBasedGroup> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public ConstraintBasedGroup get(String key) {
        ConstraintBasedGroup et = getById(key);
        if(et==null){
            et=new ConstraintBasedGroup();
            et.setConstraintbasedgroupid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public ConstraintBasedGroup getDraft(ConstraintBasedGroup et) {
        return et;
    }

    @Override
    public boolean checkKey(ConstraintBasedGroup et) {
        return (!ObjectUtils.isEmpty(et.getConstraintbasedgroupid()))&&(!Objects.isNull(this.getById(et.getConstraintbasedgroupid())));
    }
    @Override
    @Transactional
    public boolean save(ConstraintBasedGroup et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(ConstraintBasedGroup et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<ConstraintBasedGroup> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<ConstraintBasedGroup> list) {
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<ConstraintBasedGroup> selectByBusinessunitid(String businessunitid) {
        return baseMapper.selectByBusinessunitid(businessunitid);
    }

    @Override
    public void removeByBusinessunitid(String businessunitid) {
        this.remove(new QueryWrapper<ConstraintBasedGroup>().eq("businessunitid",businessunitid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<ConstraintBasedGroup> searchDefault(ConstraintBasedGroupSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<ConstraintBasedGroup> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<ConstraintBasedGroup>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<ConstraintBasedGroup> getConstraintbasedgroupByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<ConstraintBasedGroup> getConstraintbasedgroupByEntities(List<ConstraintBasedGroup> entities) {
        List ids =new ArrayList();
        for(ConstraintBasedGroup entity : entities){
            Serializable id=entity.getConstraintbasedgroupid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}



