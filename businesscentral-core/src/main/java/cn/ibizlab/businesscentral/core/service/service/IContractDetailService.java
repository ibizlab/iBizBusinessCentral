package cn.ibizlab.businesscentral.core.service.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.service.domain.ContractDetail;
import cn.ibizlab.businesscentral.core.service.filter.ContractDetailSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[ContractDetail] 服务对象接口
 */
public interface IContractDetailService extends IService<ContractDetail>{

    boolean create(ContractDetail et) ;
    void createBatch(List<ContractDetail> list) ;
    boolean update(ContractDetail et) ;
    void updateBatch(List<ContractDetail> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    ContractDetail get(String key) ;
    ContractDetail getDraft(ContractDetail et) ;
    boolean checkKey(ContractDetail et) ;
    boolean save(ContractDetail et) ;
    void saveBatch(List<ContractDetail> list) ;
    Page<ContractDetail> searchDefault(ContractDetailSearchContext context) ;
    List<ContractDetail> selectByContractid(String contractid) ;
    void removeByContractid(String contractid) ;
    List<ContractDetail> selectByServiceaddress(String customeraddressid) ;
    void removeByServiceaddress(String customeraddressid) ;
    List<ContractDetail> selectByProductid(String productid) ;
    void removeByProductid(String productid) ;
    List<ContractDetail> selectByUomscheduleid(String uomscheduleid) ;
    void removeByUomscheduleid(String uomscheduleid) ;
    List<ContractDetail> selectByUomid(String uomid) ;
    void removeByUomid(String uomid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<ContractDetail> getContractdetailByIds(List<String> ids) ;
    List<ContractDetail> getContractdetailByEntities(List<ContractDetail> entities) ;
}


