package cn.ibizlab.businesscentral.core.sales.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.sales.domain.LeadCompetitor;
import cn.ibizlab.businesscentral.core.sales.filter.LeadCompetitorSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[LeadCompetitor] 服务对象接口
 */
public interface ILeadCompetitorService extends IService<LeadCompetitor>{

    boolean create(LeadCompetitor et) ;
    void createBatch(List<LeadCompetitor> list) ;
    boolean update(LeadCompetitor et) ;
    void updateBatch(List<LeadCompetitor> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    LeadCompetitor get(String key) ;
    LeadCompetitor getDraft(LeadCompetitor et) ;
    boolean checkKey(LeadCompetitor et) ;
    boolean save(LeadCompetitor et) ;
    void saveBatch(List<LeadCompetitor> list) ;
    Page<LeadCompetitor> searchDefault(LeadCompetitorSearchContext context) ;
    List<LeadCompetitor> selectByEntity2id(String competitorid) ;
    void removeByEntity2id(String competitorid) ;
    List<LeadCompetitor> selectByEntityid(String leadid) ;
    void removeByEntityid(String leadid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<LeadCompetitor> getLeadcompetitorByIds(List<String> ids) ;
    List<LeadCompetitor> getLeadcompetitorByEntities(List<LeadCompetitor> entities) ;
}


