

package cn.ibizlab.businesscentral.core.base.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.base.domain.Contact;
import cn.ibizlab.businesscentral.core.service.domain.IncidentCustomer;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface ContactInheritMapping {

    @Mappings({
        @Mapping(source ="contactid",target = "customerid"),
        @Mapping(source ="fullname",target = "customername"),
        @Mapping(target ="focusNull",ignore = true),
    })
    IncidentCustomer toIncidentcustomer(Contact contact);

    @Mappings({
        @Mapping(source ="customerid" ,target = "contactid"),
        @Mapping(source ="customername" ,target = "fullname"),
        @Mapping(target ="focusNull",ignore = true),
    })
    Contact toContact(IncidentCustomer incidentcustomer);

    List<IncidentCustomer> toIncidentcustomer(List<Contact> contact);

    List<Contact> toContact(List<IncidentCustomer> incidentcustomer);

}


