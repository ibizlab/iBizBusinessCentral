package cn.ibizlab.businesscentral.core.base.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[审核]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "AUDIT",resultMap = "IBizAuditResultMap")
public class IBizAudit extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 关于
     */
    @TableField(value = "regardingobjectidname")
    @JSONField(name = "regardingobjectidname")
    @JsonProperty("regardingobjectidname")
    private String regardingobjectidname;
    /**
     * 已更改字段
     */
    @TableField(value = "attributemask")
    @JSONField(name = "attributemask")
    @JsonProperty("attributemask")
    private String attributemask;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 关于
     */
    @TableField(value = "regardingobjectid")
    @JSONField(name = "regardingobjectid")
    @JsonProperty("regardingobjectid")
    private String regardingobjectid;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 记录 ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "auditid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "auditid")
    @JsonProperty("auditid")
    private String auditid;
    /**
     * 事务 ID
     */
    @TableField(value = "transactionid")
    @JSONField(name = "transactionid")
    @JsonProperty("transactionid")
    private String transactionid;
    /**
     * 用户信息
     */
    @TableField(value = "useradditionalinfo")
    @JSONField(name = "useradditionalinfo")
    @JsonProperty("useradditionalinfo")
    private String useradditionalinfo;
    /**
     * 事件
     */
    @TableField(value = "action")
    @JSONField(name = "action")
    @JsonProperty("action")
    private String action;
    /**
     * 操作
     */
    @TableField(value = "operation")
    @JSONField(name = "operation")
    @JsonProperty("operation")
    private String operation;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 更改数据
     */
    @TableField(value = "changedata")
    @JSONField(name = "changedata")
    @JsonProperty("changedata")
    private String changedata;
    /**
     * 实体
     */
    @TableField(value = "objecttypecode")
    @JSONField(name = "objecttypecode")
    @JsonProperty("objecttypecode")
    private String objecttypecode;



    /**
     * 设置 [关于]
     */
    public void setRegardingobjectidname(String regardingobjectidname){
        this.regardingobjectidname = regardingobjectidname ;
        this.modify("regardingobjectidname",regardingobjectidname);
    }

    /**
     * 设置 [已更改字段]
     */
    public void setAttributemask(String attributemask){
        this.attributemask = attributemask ;
        this.modify("attributemask",attributemask);
    }

    /**
     * 设置 [关于]
     */
    public void setRegardingobjectid(String regardingobjectid){
        this.regardingobjectid = regardingobjectid ;
        this.modify("regardingobjectid",regardingobjectid);
    }

    /**
     * 设置 [事务 ID]
     */
    public void setTransactionid(String transactionid){
        this.transactionid = transactionid ;
        this.modify("transactionid",transactionid);
    }

    /**
     * 设置 [用户信息]
     */
    public void setUseradditionalinfo(String useradditionalinfo){
        this.useradditionalinfo = useradditionalinfo ;
        this.modify("useradditionalinfo",useradditionalinfo);
    }

    /**
     * 设置 [事件]
     */
    public void setAction(String action){
        this.action = action ;
        this.modify("action",action);
    }

    /**
     * 设置 [操作]
     */
    public void setOperation(String operation){
        this.operation = operation ;
        this.modify("operation",operation);
    }

    /**
     * 设置 [更改数据]
     */
    public void setChangedata(String changedata){
        this.changedata = changedata ;
        this.modify("changedata",changedata);
    }

    /**
     * 设置 [实体]
     */
    public void setObjecttypecode(String objecttypecode){
        this.objecttypecode = objecttypecode ;
        this.modify("objecttypecode",objecttypecode);
    }


}


