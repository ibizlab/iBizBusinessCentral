package cn.ibizlab.businesscentral.core.base.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.base.domain.TeamTemplate;
/**
 * 关系型数据实体[TeamTemplate] 查询条件对象
 */
@Slf4j
@Data
public class TeamTemplateSearchContext extends QueryWrapperContext<TeamTemplate> {

	private String n_teamtemplatename_like;//[名称]
	public void setN_teamtemplatename_like(String n_teamtemplatename_like) {
        this.n_teamtemplatename_like = n_teamtemplatename_like;
        if(!ObjectUtils.isEmpty(this.n_teamtemplatename_like)){
            this.getSearchCond().like("teamtemplatename", n_teamtemplatename_like);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("teamtemplatename", query)   
            );
		 }
	}
}



