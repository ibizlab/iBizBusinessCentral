package cn.ibizlab.businesscentral.core.service.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[案例解决]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "INCIDENTRESOLUTION",resultMap = "IncidentreSolutionResultMap")
public class IncidentreSolution extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主题
     */
    @TableField(value = "subject")
    @JSONField(name = "subject")
    @JsonProperty("subject")
    private String subject;
    /**
     * 保留的语音邮件
     */
    @DEField(defaultValue = "0")
    @TableField(value = "leftvoicemail")
    @JSONField(name = "leftvoicemail")
    @JsonProperty("leftvoicemail")
    private Integer leftvoicemail;
    /**
     * 隐藏
     */
    @DEField(defaultValue = "0")
    @TableField(value = "mapiprivate")
    @JSONField(name = "mapiprivate")
    @JsonProperty("mapiprivate")
    private Integer mapiprivate;
    /**
     * 是定期活动
     */
    @DEField(defaultValue = "0")
    @TableField(value = "regularactivity")
    @JSONField(name = "regularactivity")
    @JsonProperty("regularactivity")
    private Integer regularactivity;
    /**
     * Exchange WebLink
     */
    @TableField(value = "exchangeweblink")
    @JSONField(name = "exchangeweblink")
    @JsonProperty("exchangeweblink")
    private String exchangeweblink;
    /**
     * 流程阶段
     */
    @TableField(value = "stageid")
    @JSONField(name = "stageid")
    @JsonProperty("stageid")
    private String stageid;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * Resources
     */
    @TableField(value = "resources")
    @JSONField(name = "resources")
    @JsonProperty("resources")
    private String resources;
    /**
     * 实际持续时间
     */
    @TableField(value = "actualdurationminutes")
    @JSONField(name = "actualdurationminutes")
    @JsonProperty("actualdurationminutes")
    private Integer actualdurationminutes;
    /**
     * Customers
     */
    @TableField(value = "customers")
    @JSONField(name = "customers")
    @JsonProperty("customers")
    private String customers;
    /**
     * 发送日期
     */
    @TableField(value = "senton")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "senton" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("senton")
    private Timestamp senton;
    /**
     * Record Created On
     */
    @TableField(value = "overriddencreatedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;
    /**
     * 流程
     */
    @TableField(value = "processid")
    @JSONField(name = "processid")
    @JsonProperty("processid")
    private String processid;
    /**
     * 计划结束时间
     */
    @TableField(value = "scheduledend")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "scheduledend" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("scheduledend")
    private Timestamp scheduledend;
    /**
     * 排序日期
     */
    @TableField(value = "sortdate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "sortdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("sortdate")
    private Timestamp sortdate;
    /**
     * 关于
     */
    @TableField(value = "regardingobjectidname")
    @JSONField(name = "regardingobjectidname")
    @JsonProperty("regardingobjectidname")
    private String regardingobjectidname;
    /**
     * 活动类型
     */
    @TableField(value = "activitytypecode")
    @JSONField(name = "activitytypecode")
    @JsonProperty("activitytypecode")
    private String activitytypecode;
    /**
     * RegardingObjectTypeCode
     */
    @TableField(value = "regardingobjecttypecode")
    @JSONField(name = "regardingobjecttypecode")
    @JsonProperty("regardingobjecttypecode")
    private String regardingobjecttypecode;
    /**
     * 状态描述
     */
    @TableField(value = "statuscode")
    @JSONField(name = "statuscode")
    @JsonProperty("statuscode")
    private Integer statuscode;
    /**
     * Outsource Vendors
     */
    @TableField(value = "partners")
    @JSONField(name = "partners")
    @JsonProperty("partners")
    private String partners;
    /**
     * 关于
     */
    @TableField(value = "regardingobjectname")
    @JSONField(name = "regardingobjectname")
    @JsonProperty("regardingobjectname")
    private String regardingobjectname;
    /**
     * CC
     */
    @TableField(value = "cc")
    @JSONField(name = "cc")
    @JsonProperty("cc")
    private String cc;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 暂候时间(分钟)
     */
    @TableField(value = "onholdtime")
    @JSONField(name = "onholdtime")
    @JsonProperty("onholdtime")
    private Integer onholdtime;
    /**
     * 计划持续时间
     */
    @TableField(value = "scheduleddurationminutes")
    @JSONField(name = "scheduleddurationminutes")
    @JsonProperty("scheduleddurationminutes")
    private Integer scheduleddurationminutes;
    /**
     * IncidentType
     */
    @TableField(value = "incidenttype")
    @JSONField(name = "incidenttype")
    @JsonProperty("incidenttype")
    private String incidenttype;
    /**
     * Organizer
     */
    @TableField(value = "organizer")
    @JSONField(name = "organizer")
    @JsonProperty("organizer")
    private String organizer;
    /**
     * 实际开始时间
     */
    @TableField(value = "actualstart")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "actualstart" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("actualstart")
    private Timestamp actualstart;
    /**
     * 上次尝试传递的日期
     */
    @TableField(value = "deliverylastattemptedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "deliverylastattemptedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("deliverylastattemptedon")
    private Timestamp deliverylastattemptedon;
    /**
     * Optional Attendees
     */
    @TableField(value = "optionalattendees")
    @JSONField(name = "optionalattendees")
    @JsonProperty("optionalattendees")
    private String optionalattendees;
    /**
     * 子类别
     */
    @TableField(value = "subcategory")
    @JSONField(name = "subcategory")
    @JsonProperty("subcategory")
    private String subcategory;
    /**
     * 汇率
     */
    @TableField(value = "exchangerate")
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;
    /**
     * BCC
     */
    @TableField(value = "bcc")
    @JSONField(name = "bcc")
    @JsonProperty("bcc")
    private String bcc;
    /**
     * 所用时间
     */
    @TableField(value = "timespent")
    @JSONField(name = "timespent")
    @JsonProperty("timespent")
    private Integer timespent;
    /**
     * 系列 ID
     */
    @TableField(value = "seriesid")
    @JSONField(name = "seriesid")
    @JsonProperty("seriesid")
    private String seriesid;
    /**
     * Import Sequence Number
     */
    @TableField(value = "importsequencenumber")
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;
    /**
     * 定期实例类型
     */
    @TableField(value = "instancetypecode")
    @JSONField(name = "instancetypecode")
    @JsonProperty("instancetypecode")
    private String instancetypecode;
    /**
     * 活动附加参数
     */
    @TableField(value = "activityadditionalparams")
    @JSONField(name = "activityadditionalparams")
    @JsonProperty("activityadditionalparams")
    private String activityadditionalparams;
    /**
     * 传递优先级
     */
    @TableField(value = "deliveryprioritycode")
    @JSONField(name = "deliveryprioritycode")
    @JsonProperty("deliveryprioritycode")
    private String deliveryprioritycode;
    /**
     * 状态
     */
    @TableField(value = "statecode")
    @JSONField(name = "statecode")
    @JsonProperty("statecode")
    private Integer statecode;
    /**
     * SLAName
     */
    @TableField(value = "slaname")
    @JSONField(name = "slaname")
    @JsonProperty("slaname")
    private String slaname;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 类别
     */
    @TableField(value = "category")
    @JSONField(name = "category")
    @JsonProperty("category")
    private String category;
    /**
     * To
     */
    @TableField(value = "to")
    @JSONField(name = "to")
    @JsonProperty("to")
    private String to;
    /**
     * 时区规则版本号
     */
    @TableField(value = "timezoneruleversionnumber")
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;
    /**
     * 计划开始时间
     */
    @TableField(value = "scheduledstart")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "scheduledstart" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("scheduledstart")
    private Timestamp scheduledstart;
    /**
     * 上一暂候时间
     */
    @TableField(value = "lastonholdtime")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "lastonholdtime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("lastonholdtime")
    private Timestamp lastonholdtime;
    /**
     * UTC 转换时区代码
     */
    @TableField(value = "utcconversiontimezonecode")
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;
    /**
     * Exchange 项目 ID
     */
    @TableField(value = "exchangeitemid")
    @JSONField(name = "exchangeitemid")
    @JsonProperty("exchangeitemid")
    private String exchangeitemid;
    /**
     * 由工作流创建
     */
    @DEField(defaultValue = "0")
    @TableField(value = "workflowcreated")
    @JSONField(name = "workflowcreated")
    @JsonProperty("workflowcreated")
    private Integer workflowcreated;
    /**
     * 实际结束时间
     */
    @TableField(value = "actualend")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "actualend" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("actualend")
    private Timestamp actualend;
    /**
     * 案例解决
     */
    @DEField(isKeyField=true)
    @TableId(value= "activityid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "activityid")
    @JsonProperty("activityid")
    private String activityid;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 已记帐
     */
    @DEField(defaultValue = "0")
    @TableField(value = "billed")
    @JSONField(name = "billed")
    @JsonProperty("billed")
    private Integer billed;
    /**
     * 负责人
     */
    @TableField(value = "ownerid")
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;
    /**
     * 关于
     */
    @TableField(value = "regardingobjectid")
    @JSONField(name = "regardingobjectid")
    @JsonProperty("regardingobjectid")
    private String regardingobjectid;
    /**
     * 优先级
     */
    @TableField(value = "prioritycode")
    @JSONField(name = "prioritycode")
    @JsonProperty("prioritycode")
    private String prioritycode;
    /**
     * 说明
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 负责人
     */
    @TableField(value = "ownername")
    @JSONField(name = "ownername")
    @JsonProperty("ownername")
    private String ownername;
    /**
     * Required Attendees
     */
    @TableField(value = "requiredattendees")
    @JSONField(name = "requiredattendees")
    @JsonProperty("requiredattendees")
    private String requiredattendees;
    /**
     * 社交渠道
     */
    @TableField(value = "community")
    @JSONField(name = "community")
    @JsonProperty("community")
    private String community;
    /**
     * From
     */
    @TableField(value = "from")
    @JSONField(name = "from")
    @JsonProperty("from")
    private String from;
    /**
     * 负责人类型
     */
    @TableField(value = "ownertype")
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;
    /**
     * 版本号
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * 遍历的路径
     */
    @TableField(value = "traversedpath")
    @JSONField(name = "traversedpath")
    @JsonProperty("traversedpath")
    private String traversedpath;
    /**
     * 货币
     */
    @TableField(value = "transactioncurrencyid")
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;
    /**
     * 服务
     */
    @TableField(value = "serviceid")
    @JSONField(name = "serviceid")
    @JsonProperty("serviceid")
    private String serviceid;
    /**
     * 案例
     */
    @TableField(value = "incidentid")
    @JSONField(name = "incidentid")
    @JsonProperty("incidentid")
    private String incidentid;
    /**
     * SLA
     */
    @TableField(value = "slaid")
    @JSONField(name = "slaid")
    @JsonProperty("slaid")
    private String slaid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.service.domain.Incident incident;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.service.domain.IBizService service;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.Sla sla;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency transactioncurrency;



    /**
     * 设置 [主题]
     */
    public void setSubject(String subject){
        this.subject = subject ;
        this.modify("subject",subject);
    }

    /**
     * 设置 [保留的语音邮件]
     */
    public void setLeftvoicemail(Integer leftvoicemail){
        this.leftvoicemail = leftvoicemail ;
        this.modify("leftvoicemail",leftvoicemail);
    }

    /**
     * 设置 [隐藏]
     */
    public void setMapiprivate(Integer mapiprivate){
        this.mapiprivate = mapiprivate ;
        this.modify("mapiprivate",mapiprivate);
    }

    /**
     * 设置 [是定期活动]
     */
    public void setRegularactivity(Integer regularactivity){
        this.regularactivity = regularactivity ;
        this.modify("regularactivity",regularactivity);
    }

    /**
     * 设置 [Exchange WebLink]
     */
    public void setExchangeweblink(String exchangeweblink){
        this.exchangeweblink = exchangeweblink ;
        this.modify("exchangeweblink",exchangeweblink);
    }

    /**
     * 设置 [流程阶段]
     */
    public void setStageid(String stageid){
        this.stageid = stageid ;
        this.modify("stageid",stageid);
    }

    /**
     * 设置 [Resources]
     */
    public void setResources(String resources){
        this.resources = resources ;
        this.modify("resources",resources);
    }

    /**
     * 设置 [实际持续时间]
     */
    public void setActualdurationminutes(Integer actualdurationminutes){
        this.actualdurationminutes = actualdurationminutes ;
        this.modify("actualdurationminutes",actualdurationminutes);
    }

    /**
     * 设置 [Customers]
     */
    public void setCustomers(String customers){
        this.customers = customers ;
        this.modify("customers",customers);
    }

    /**
     * 设置 [发送日期]
     */
    public void setSenton(Timestamp senton){
        this.senton = senton ;
        this.modify("senton",senton);
    }

    /**
     * 格式化日期 [发送日期]
     */
    public String formatSenton(){
        if (this.senton == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(senton);
    }
    /**
     * 设置 [Record Created On]
     */
    public void setOverriddencreatedon(Timestamp overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 格式化日期 [Record Created On]
     */
    public String formatOverriddencreatedon(){
        if (this.overriddencreatedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(overriddencreatedon);
    }
    /**
     * 设置 [流程]
     */
    public void setProcessid(String processid){
        this.processid = processid ;
        this.modify("processid",processid);
    }

    /**
     * 设置 [计划结束时间]
     */
    public void setScheduledend(Timestamp scheduledend){
        this.scheduledend = scheduledend ;
        this.modify("scheduledend",scheduledend);
    }

    /**
     * 格式化日期 [计划结束时间]
     */
    public String formatScheduledend(){
        if (this.scheduledend == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(scheduledend);
    }
    /**
     * 设置 [排序日期]
     */
    public void setSortdate(Timestamp sortdate){
        this.sortdate = sortdate ;
        this.modify("sortdate",sortdate);
    }

    /**
     * 格式化日期 [排序日期]
     */
    public String formatSortdate(){
        if (this.sortdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(sortdate);
    }
    /**
     * 设置 [关于]
     */
    public void setRegardingobjectidname(String regardingobjectidname){
        this.regardingobjectidname = regardingobjectidname ;
        this.modify("regardingobjectidname",regardingobjectidname);
    }

    /**
     * 设置 [活动类型]
     */
    public void setActivitytypecode(String activitytypecode){
        this.activitytypecode = activitytypecode ;
        this.modify("activitytypecode",activitytypecode);
    }

    /**
     * 设置 [RegardingObjectTypeCode]
     */
    public void setRegardingobjecttypecode(String regardingobjecttypecode){
        this.regardingobjecttypecode = regardingobjecttypecode ;
        this.modify("regardingobjecttypecode",regardingobjecttypecode);
    }

    /**
     * 设置 [状态描述]
     */
    public void setStatuscode(Integer statuscode){
        this.statuscode = statuscode ;
        this.modify("statuscode",statuscode);
    }

    /**
     * 设置 [Outsource Vendors]
     */
    public void setPartners(String partners){
        this.partners = partners ;
        this.modify("partners",partners);
    }

    /**
     * 设置 [关于]
     */
    public void setRegardingobjectname(String regardingobjectname){
        this.regardingobjectname = regardingobjectname ;
        this.modify("regardingobjectname",regardingobjectname);
    }

    /**
     * 设置 [CC]
     */
    public void setCc(String cc){
        this.cc = cc ;
        this.modify("cc",cc);
    }

    /**
     * 设置 [暂候时间(分钟)]
     */
    public void setOnholdtime(Integer onholdtime){
        this.onholdtime = onholdtime ;
        this.modify("onholdtime",onholdtime);
    }

    /**
     * 设置 [计划持续时间]
     */
    public void setScheduleddurationminutes(Integer scheduleddurationminutes){
        this.scheduleddurationminutes = scheduleddurationminutes ;
        this.modify("scheduleddurationminutes",scheduleddurationminutes);
    }

    /**
     * 设置 [IncidentType]
     */
    public void setIncidenttype(String incidenttype){
        this.incidenttype = incidenttype ;
        this.modify("incidenttype",incidenttype);
    }

    /**
     * 设置 [Organizer]
     */
    public void setOrganizer(String organizer){
        this.organizer = organizer ;
        this.modify("organizer",organizer);
    }

    /**
     * 设置 [实际开始时间]
     */
    public void setActualstart(Timestamp actualstart){
        this.actualstart = actualstart ;
        this.modify("actualstart",actualstart);
    }

    /**
     * 格式化日期 [实际开始时间]
     */
    public String formatActualstart(){
        if (this.actualstart == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(actualstart);
    }
    /**
     * 设置 [上次尝试传递的日期]
     */
    public void setDeliverylastattemptedon(Timestamp deliverylastattemptedon){
        this.deliverylastattemptedon = deliverylastattemptedon ;
        this.modify("deliverylastattemptedon",deliverylastattemptedon);
    }

    /**
     * 格式化日期 [上次尝试传递的日期]
     */
    public String formatDeliverylastattemptedon(){
        if (this.deliverylastattemptedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(deliverylastattemptedon);
    }
    /**
     * 设置 [Optional Attendees]
     */
    public void setOptionalattendees(String optionalattendees){
        this.optionalattendees = optionalattendees ;
        this.modify("optionalattendees",optionalattendees);
    }

    /**
     * 设置 [子类别]
     */
    public void setSubcategory(String subcategory){
        this.subcategory = subcategory ;
        this.modify("subcategory",subcategory);
    }

    /**
     * 设置 [汇率]
     */
    public void setExchangerate(BigDecimal exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [BCC]
     */
    public void setBcc(String bcc){
        this.bcc = bcc ;
        this.modify("bcc",bcc);
    }

    /**
     * 设置 [所用时间]
     */
    public void setTimespent(Integer timespent){
        this.timespent = timespent ;
        this.modify("timespent",timespent);
    }

    /**
     * 设置 [系列 ID]
     */
    public void setSeriesid(String seriesid){
        this.seriesid = seriesid ;
        this.modify("seriesid",seriesid);
    }

    /**
     * 设置 [Import Sequence Number]
     */
    public void setImportsequencenumber(Integer importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [定期实例类型]
     */
    public void setInstancetypecode(String instancetypecode){
        this.instancetypecode = instancetypecode ;
        this.modify("instancetypecode",instancetypecode);
    }

    /**
     * 设置 [活动附加参数]
     */
    public void setActivityadditionalparams(String activityadditionalparams){
        this.activityadditionalparams = activityadditionalparams ;
        this.modify("activityadditionalparams",activityadditionalparams);
    }

    /**
     * 设置 [传递优先级]
     */
    public void setDeliveryprioritycode(String deliveryprioritycode){
        this.deliveryprioritycode = deliveryprioritycode ;
        this.modify("deliveryprioritycode",deliveryprioritycode);
    }

    /**
     * 设置 [状态]
     */
    public void setStatecode(Integer statecode){
        this.statecode = statecode ;
        this.modify("statecode",statecode);
    }

    /**
     * 设置 [SLAName]
     */
    public void setSlaname(String slaname){
        this.slaname = slaname ;
        this.modify("slaname",slaname);
    }

    /**
     * 设置 [类别]
     */
    public void setCategory(String category){
        this.category = category ;
        this.modify("category",category);
    }

    /**
     * 设置 [To]
     */
    public void setTo(String to){
        this.to = to ;
        this.modify("to",to);
    }

    /**
     * 设置 [时区规则版本号]
     */
    public void setTimezoneruleversionnumber(Integer timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [计划开始时间]
     */
    public void setScheduledstart(Timestamp scheduledstart){
        this.scheduledstart = scheduledstart ;
        this.modify("scheduledstart",scheduledstart);
    }

    /**
     * 格式化日期 [计划开始时间]
     */
    public String formatScheduledstart(){
        if (this.scheduledstart == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(scheduledstart);
    }
    /**
     * 设置 [上一暂候时间]
     */
    public void setLastonholdtime(Timestamp lastonholdtime){
        this.lastonholdtime = lastonholdtime ;
        this.modify("lastonholdtime",lastonholdtime);
    }

    /**
     * 格式化日期 [上一暂候时间]
     */
    public String formatLastonholdtime(){
        if (this.lastonholdtime == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(lastonholdtime);
    }
    /**
     * 设置 [UTC 转换时区代码]
     */
    public void setUtcconversiontimezonecode(Integer utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [Exchange 项目 ID]
     */
    public void setExchangeitemid(String exchangeitemid){
        this.exchangeitemid = exchangeitemid ;
        this.modify("exchangeitemid",exchangeitemid);
    }

    /**
     * 设置 [由工作流创建]
     */
    public void setWorkflowcreated(Integer workflowcreated){
        this.workflowcreated = workflowcreated ;
        this.modify("workflowcreated",workflowcreated);
    }

    /**
     * 设置 [实际结束时间]
     */
    public void setActualend(Timestamp actualend){
        this.actualend = actualend ;
        this.modify("actualend",actualend);
    }

    /**
     * 格式化日期 [实际结束时间]
     */
    public String formatActualend(){
        if (this.actualend == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(actualend);
    }
    /**
     * 设置 [已记帐]
     */
    public void setBilled(Integer billed){
        this.billed = billed ;
        this.modify("billed",billed);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnerid(String ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [关于]
     */
    public void setRegardingobjectid(String regardingobjectid){
        this.regardingobjectid = regardingobjectid ;
        this.modify("regardingobjectid",regardingobjectid);
    }

    /**
     * 设置 [优先级]
     */
    public void setPrioritycode(String prioritycode){
        this.prioritycode = prioritycode ;
        this.modify("prioritycode",prioritycode);
    }

    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnername(String ownername){
        this.ownername = ownername ;
        this.modify("ownername",ownername);
    }

    /**
     * 设置 [Required Attendees]
     */
    public void setRequiredattendees(String requiredattendees){
        this.requiredattendees = requiredattendees ;
        this.modify("requiredattendees",requiredattendees);
    }

    /**
     * 设置 [社交渠道]
     */
    public void setCommunity(String community){
        this.community = community ;
        this.modify("community",community);
    }

    /**
     * 设置 [From]
     */
    public void setFrom(String from){
        this.from = from ;
        this.modify("from",from);
    }

    /**
     * 设置 [负责人类型]
     */
    public void setOwnertype(String ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [版本号]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [遍历的路径]
     */
    public void setTraversedpath(String traversedpath){
        this.traversedpath = traversedpath ;
        this.modify("traversedpath",traversedpath);
    }

    /**
     * 设置 [货币]
     */
    public void setTransactioncurrencyid(String transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }

    /**
     * 设置 [服务]
     */
    public void setServiceid(String serviceid){
        this.serviceid = serviceid ;
        this.modify("serviceid",serviceid);
    }

    /**
     * 设置 [案例]
     */
    public void setIncidentid(String incidentid){
        this.incidentid = incidentid ;
        this.modify("incidentid",incidentid);
    }

    /**
     * 设置 [SLA]
     */
    public void setSlaid(String slaid){
        this.slaid = slaid ;
        this.modify("slaid",slaid);
    }


}


