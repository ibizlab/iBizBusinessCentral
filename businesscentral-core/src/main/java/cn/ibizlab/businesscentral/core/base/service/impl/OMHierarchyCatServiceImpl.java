package cn.ibizlab.businesscentral.core.base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.base.domain.OMHierarchyCat;
import cn.ibizlab.businesscentral.core.base.filter.OMHierarchyCatSearchContext;
import cn.ibizlab.businesscentral.core.base.service.IOMHierarchyCatService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.base.mapper.OMHierarchyCatMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[结构层次类别] 服务对象接口实现
 */
@Slf4j
@Service("OMHierarchyCatServiceImpl")
public class OMHierarchyCatServiceImpl extends ServiceImpl<OMHierarchyCatMapper, OMHierarchyCat> implements IOMHierarchyCatService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IOMHierarchyPurposeRefService omhierarchypurposerefService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IOMHierarchyService omhierarchyService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(OMHierarchyCat et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getOmhierarchycatid()),et);
        return true;
    }

    @Override
    public void createBatch(List<OMHierarchyCat> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(OMHierarchyCat et) {
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("omhierarchycatid",et.getOmhierarchycatid())))
            return false;
        CachedBeanCopier.copy(get(et.getOmhierarchycatid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<OMHierarchyCat> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public OMHierarchyCat get(String key) {
        OMHierarchyCat et = getById(key);
        if(et==null){
            et=new OMHierarchyCat();
            et.setOmhierarchycatid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public OMHierarchyCat getDraft(OMHierarchyCat et) {
        return et;
    }

    @Override
    public boolean checkKey(OMHierarchyCat et) {
        return (!ObjectUtils.isEmpty(et.getOmhierarchycatid()))&&(!Objects.isNull(this.getById(et.getOmhierarchycatid())));
    }
    @Override
    @Transactional
    public boolean save(OMHierarchyCat et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(OMHierarchyCat et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<OMHierarchyCat> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<OMHierarchyCat> list) {
        saveOrUpdateBatch(list,batchSize);
    }



    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<OMHierarchyCat> searchDefault(OMHierarchyCatSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<OMHierarchyCat> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<OMHierarchyCat>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<OMHierarchyCat> getOmhierarchycatByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<OMHierarchyCat> getOmhierarchycatByEntities(List<OMHierarchyCat> entities) {
        List ids =new ArrayList();
        for(OMHierarchyCat entity : entities){
            Serializable id=entity.getOmhierarchycatid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}



