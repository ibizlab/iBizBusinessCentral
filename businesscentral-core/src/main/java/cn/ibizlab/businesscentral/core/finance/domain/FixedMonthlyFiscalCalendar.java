package cn.ibizlab.businesscentral.core.finance.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[固定月度会计日历]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "FIXEDMONTHLYFISCALCALENDAR",resultMap = "FixedMonthlyFiscalCalendarResultMap")
public class FixedMonthlyFiscalCalendar extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Period3_Base
     */
    @DEField(name = "period3_base")
    @TableField(value = "period3_base")
    @JSONField(name = "period3_base")
    @JsonProperty("period3_base")
    private BigDecimal period3Base;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * Period6
     */
    @TableField(value = "period6")
    @JSONField(name = "period6")
    @JsonProperty("period6")
    private BigDecimal period6;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * UserFiscalCalendarId
     */
    @DEField(isKeyField=true)
    @TableId(value= "userfiscalcalendarid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "userfiscalcalendarid")
    @JsonProperty("userfiscalcalendarid")
    private String userfiscalcalendarid;
    /**
     * FiscalPeriodType
     */
    @TableField(value = "fiscalperiodtype")
    @JSONField(name = "fiscalperiodtype")
    @JsonProperty("fiscalperiodtype")
    private Integer fiscalperiodtype;
    /**
     * Period11_Base
     */
    @DEField(name = "period11_base")
    @TableField(value = "period11_base")
    @JSONField(name = "period11_base")
    @JsonProperty("period11_base")
    private BigDecimal period11Base;
    /**
     * Period6_Base
     */
    @DEField(name = "period6_base")
    @TableField(value = "period6_base")
    @JSONField(name = "period6_base")
    @JsonProperty("period6_base")
    private BigDecimal period6Base;
    /**
     * Period8_Base
     */
    @DEField(name = "period8_base")
    @TableField(value = "period8_base")
    @JSONField(name = "period8_base")
    @JsonProperty("period8_base")
    private BigDecimal period8Base;
    /**
     * ExchangeRate
     */
    @TableField(value = "exchangerate")
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;
    /**
     * Period1_Base
     */
    @DEField(name = "period1_base")
    @TableField(value = "period1_base")
    @JSONField(name = "period1_base")
    @JsonProperty("period1_base")
    private BigDecimal period1Base;
    /**
     * Period7
     */
    @TableField(value = "period7")
    @JSONField(name = "period7")
    @JsonProperty("period7")
    private BigDecimal period7;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * Period5_Base
     */
    @DEField(name = "period5_base")
    @TableField(value = "period5_base")
    @JSONField(name = "period5_base")
    @JsonProperty("period5_base")
    private BigDecimal period5Base;
    /**
     * Period13
     */
    @TableField(value = "period13")
    @JSONField(name = "period13")
    @JsonProperty("period13")
    private BigDecimal period13;
    /**
     * Period2_Base
     */
    @DEField(name = "period2_base")
    @TableField(value = "period2_base")
    @JSONField(name = "period2_base")
    @JsonProperty("period2_base")
    private BigDecimal period2Base;
    /**
     * Period11
     */
    @TableField(value = "period11")
    @JSONField(name = "period11")
    @JsonProperty("period11")
    private BigDecimal period11;
    /**
     * Period10
     */
    @TableField(value = "period10")
    @JSONField(name = "period10")
    @JsonProperty("period10")
    private BigDecimal period10;
    /**
     * Period12
     */
    @TableField(value = "period12")
    @JSONField(name = "period12")
    @JsonProperty("period12")
    private BigDecimal period12;
    /**
     * SalesPersonId
     */
    @TableField(value = "salespersonid")
    @JSONField(name = "salespersonid")
    @JsonProperty("salespersonid")
    private String salespersonid;
    /**
     * Period12_Base
     */
    @DEField(name = "period12_base")
    @TableField(value = "period12_base")
    @JSONField(name = "period12_base")
    @JsonProperty("period12_base")
    private BigDecimal period12Base;
    /**
     * Period9_Base
     */
    @DEField(name = "period9_base")
    @TableField(value = "period9_base")
    @JSONField(name = "period9_base")
    @JsonProperty("period9_base")
    private BigDecimal period9Base;
    /**
     * Period8
     */
    @TableField(value = "period8")
    @JSONField(name = "period8")
    @JsonProperty("period8")
    private BigDecimal period8;
    /**
     * Period5
     */
    @TableField(value = "period5")
    @JSONField(name = "period5")
    @JsonProperty("period5")
    private BigDecimal period5;
    /**
     * Period3
     */
    @TableField(value = "period3")
    @JSONField(name = "period3")
    @JsonProperty("period3")
    private BigDecimal period3;
    /**
     * Period2
     */
    @TableField(value = "period2")
    @JSONField(name = "period2")
    @JsonProperty("period2")
    private BigDecimal period2;
    /**
     * Period10_Base
     */
    @DEField(name = "period10_base")
    @TableField(value = "period10_base")
    @JSONField(name = "period10_base")
    @JsonProperty("period10_base")
    private BigDecimal period10Base;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * Period1
     */
    @TableField(value = "period1")
    @JSONField(name = "period1")
    @JsonProperty("period1")
    private BigDecimal period1;
    /**
     * Period13_Base
     */
    @DEField(name = "period13_base")
    @TableField(value = "period13_base")
    @JSONField(name = "period13_base")
    @JsonProperty("period13_base")
    private BigDecimal period13Base;
    /**
     * 销售员
     */
    @TableField(value = "salespersonname")
    @JSONField(name = "salespersonname")
    @JsonProperty("salespersonname")
    private String salespersonname;
    /**
     * Period7_Base
     */
    @DEField(name = "period7_base")
    @TableField(value = "period7_base")
    @JSONField(name = "period7_base")
    @JsonProperty("period7_base")
    private BigDecimal period7Base;
    /**
     * TimeZoneRuleVersionNumber
     */
    @TableField(value = "timezoneruleversionnumber")
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;
    /**
     * Period4_Base
     */
    @DEField(name = "period4_base")
    @TableField(value = "period4_base")
    @JSONField(name = "period4_base")
    @JsonProperty("period4_base")
    private BigDecimal period4Base;
    /**
     * Period9
     */
    @TableField(value = "period9")
    @JSONField(name = "period9")
    @JsonProperty("period9")
    private BigDecimal period9;
    /**
     * Period4
     */
    @TableField(value = "period4")
    @JSONField(name = "period4")
    @JsonProperty("period4")
    private BigDecimal period4;
    /**
     * EffectiveOn
     */
    @TableField(value = "effectiveon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "effectiveon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("effectiveon")
    private Timestamp effectiveon;
    /**
     * UTCConversionTimeZoneCode
     */
    @TableField(value = "utcconversiontimezonecode")
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;
    /**
     * TransactionCurrencyId
     */
    @TableField(value = "transactioncurrencyid")
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency transactioncurrency;



    /**
     * 设置 [Period3_Base]
     */
    public void setPeriod3Base(BigDecimal period3Base){
        this.period3Base = period3Base ;
        this.modify("period3_base",period3Base);
    }

    /**
     * 设置 [Period6]
     */
    public void setPeriod6(BigDecimal period6){
        this.period6 = period6 ;
        this.modify("period6",period6);
    }

    /**
     * 设置 [FiscalPeriodType]
     */
    public void setFiscalperiodtype(Integer fiscalperiodtype){
        this.fiscalperiodtype = fiscalperiodtype ;
        this.modify("fiscalperiodtype",fiscalperiodtype);
    }

    /**
     * 设置 [Period11_Base]
     */
    public void setPeriod11Base(BigDecimal period11Base){
        this.period11Base = period11Base ;
        this.modify("period11_base",period11Base);
    }

    /**
     * 设置 [Period6_Base]
     */
    public void setPeriod6Base(BigDecimal period6Base){
        this.period6Base = period6Base ;
        this.modify("period6_base",period6Base);
    }

    /**
     * 设置 [Period8_Base]
     */
    public void setPeriod8Base(BigDecimal period8Base){
        this.period8Base = period8Base ;
        this.modify("period8_base",period8Base);
    }

    /**
     * 设置 [ExchangeRate]
     */
    public void setExchangerate(BigDecimal exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [Period1_Base]
     */
    public void setPeriod1Base(BigDecimal period1Base){
        this.period1Base = period1Base ;
        this.modify("period1_base",period1Base);
    }

    /**
     * 设置 [Period7]
     */
    public void setPeriod7(BigDecimal period7){
        this.period7 = period7 ;
        this.modify("period7",period7);
    }

    /**
     * 设置 [Period5_Base]
     */
    public void setPeriod5Base(BigDecimal period5Base){
        this.period5Base = period5Base ;
        this.modify("period5_base",period5Base);
    }

    /**
     * 设置 [Period13]
     */
    public void setPeriod13(BigDecimal period13){
        this.period13 = period13 ;
        this.modify("period13",period13);
    }

    /**
     * 设置 [Period2_Base]
     */
    public void setPeriod2Base(BigDecimal period2Base){
        this.period2Base = period2Base ;
        this.modify("period2_base",period2Base);
    }

    /**
     * 设置 [Period11]
     */
    public void setPeriod11(BigDecimal period11){
        this.period11 = period11 ;
        this.modify("period11",period11);
    }

    /**
     * 设置 [Period10]
     */
    public void setPeriod10(BigDecimal period10){
        this.period10 = period10 ;
        this.modify("period10",period10);
    }

    /**
     * 设置 [Period12]
     */
    public void setPeriod12(BigDecimal period12){
        this.period12 = period12 ;
        this.modify("period12",period12);
    }

    /**
     * 设置 [SalesPersonId]
     */
    public void setSalespersonid(String salespersonid){
        this.salespersonid = salespersonid ;
        this.modify("salespersonid",salespersonid);
    }

    /**
     * 设置 [Period12_Base]
     */
    public void setPeriod12Base(BigDecimal period12Base){
        this.period12Base = period12Base ;
        this.modify("period12_base",period12Base);
    }

    /**
     * 设置 [Period9_Base]
     */
    public void setPeriod9Base(BigDecimal period9Base){
        this.period9Base = period9Base ;
        this.modify("period9_base",period9Base);
    }

    /**
     * 设置 [Period8]
     */
    public void setPeriod8(BigDecimal period8){
        this.period8 = period8 ;
        this.modify("period8",period8);
    }

    /**
     * 设置 [Period5]
     */
    public void setPeriod5(BigDecimal period5){
        this.period5 = period5 ;
        this.modify("period5",period5);
    }

    /**
     * 设置 [Period3]
     */
    public void setPeriod3(BigDecimal period3){
        this.period3 = period3 ;
        this.modify("period3",period3);
    }

    /**
     * 设置 [Period2]
     */
    public void setPeriod2(BigDecimal period2){
        this.period2 = period2 ;
        this.modify("period2",period2);
    }

    /**
     * 设置 [Period10_Base]
     */
    public void setPeriod10Base(BigDecimal period10Base){
        this.period10Base = period10Base ;
        this.modify("period10_base",period10Base);
    }

    /**
     * 设置 [Period1]
     */
    public void setPeriod1(BigDecimal period1){
        this.period1 = period1 ;
        this.modify("period1",period1);
    }

    /**
     * 设置 [Period13_Base]
     */
    public void setPeriod13Base(BigDecimal period13Base){
        this.period13Base = period13Base ;
        this.modify("period13_base",period13Base);
    }

    /**
     * 设置 [销售员]
     */
    public void setSalespersonname(String salespersonname){
        this.salespersonname = salespersonname ;
        this.modify("salespersonname",salespersonname);
    }

    /**
     * 设置 [Period7_Base]
     */
    public void setPeriod7Base(BigDecimal period7Base){
        this.period7Base = period7Base ;
        this.modify("period7_base",period7Base);
    }

    /**
     * 设置 [TimeZoneRuleVersionNumber]
     */
    public void setTimezoneruleversionnumber(Integer timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [Period4_Base]
     */
    public void setPeriod4Base(BigDecimal period4Base){
        this.period4Base = period4Base ;
        this.modify("period4_base",period4Base);
    }

    /**
     * 设置 [Period9]
     */
    public void setPeriod9(BigDecimal period9){
        this.period9 = period9 ;
        this.modify("period9",period9);
    }

    /**
     * 设置 [Period4]
     */
    public void setPeriod4(BigDecimal period4){
        this.period4 = period4 ;
        this.modify("period4",period4);
    }

    /**
     * 设置 [EffectiveOn]
     */
    public void setEffectiveon(Timestamp effectiveon){
        this.effectiveon = effectiveon ;
        this.modify("effectiveon",effectiveon);
    }

    /**
     * 格式化日期 [EffectiveOn]
     */
    public String formatEffectiveon(){
        if (this.effectiveon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(effectiveon);
    }
    /**
     * 设置 [UTCConversionTimeZoneCode]
     */
    public void setUtcconversiontimezonecode(Integer utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [TransactionCurrencyId]
     */
    public void setTransactioncurrencyid(String transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }


}


