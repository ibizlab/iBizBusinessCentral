package cn.ibizlab.businesscentral.core.website.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[站点内容]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "WEBSITECONTENT",resultMap = "WebSiteContentResultMap")
public class WebSiteContent extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 内容
     */
    @TableField(value = "content")
    @JSONField(name = "content")
    @JsonProperty("content")
    private String content;
    /**
     * 内容标识
     */
    @DEField(isKeyField=true)
    @TableId(value= "websitecontentid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "websitecontentid")
    @JsonProperty("websitecontentid")
    private String websitecontentid;
    /**
     * 业务标识
     */
    @TableField(value = "contentcode")
    @JSONField(name = "contentcode")
    @JsonProperty("contentcode")
    private String contentcode;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 备注
     */
    @TableField(value = "memo")
    @JSONField(name = "memo")
    @JsonProperty("memo")
    private String memo;
    /**
     * 作者
     */
    @TableField(value = "author")
    @JSONField(name = "author")
    @JsonProperty("author")
    private String author;
    /**
     * 是否启用
     */
    @TableField(value = "validflag")
    @JSONField(name = "validflag")
    @JsonProperty("validflag")
    private Integer validflag;
    /**
     * 标题
     */
    @TableField(value = "title")
    @JSONField(name = "title")
    @JsonProperty("title")
    private String title;
    /**
     * 内容链接
     */
    @TableField(value = "link")
    @JSONField(name = "link")
    @JsonProperty("link")
    private String link;
    /**
     * 序号
     */
    @TableField(value = "sn")
    @JSONField(name = "sn")
    @JsonProperty("sn")
    private Integer sn;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 子标题
     */
    @TableField(value = "subtitle")
    @JSONField(name = "subtitle")
    @JsonProperty("subtitle")
    private String subtitle;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 内容名称
     */
    @TableField(value = "websitecontentname")
    @JSONField(name = "websitecontentname")
    @JsonProperty("websitecontentname")
    private String websitecontentname;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 内容类型
     */
    @TableField(value = "contenttype")
    @JSONField(name = "contenttype")
    @JsonProperty("contenttype")
    private String contenttype;
    /**
     * 频道
     */
    @TableField(exist = false)
    @JSONField(name = "websitechannelname")
    @JsonProperty("websitechannelname")
    private String websitechannelname;
    /**
     * 站点
     */
    @TableField(exist = false)
    @JSONField(name = "websitename")
    @JsonProperty("websitename")
    private String websitename;
    /**
     * 网站标识
     */
    @TableField(value = "websiteid")
    @JSONField(name = "websiteid")
    @JsonProperty("websiteid")
    private String websiteid;
    /**
     * 频道标识
     */
    @TableField(value = "websitechannelid")
    @JSONField(name = "websitechannelid")
    @JsonProperty("websitechannelid")
    private String websitechannelid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.website.domain.WebSiteChannel websitechannel;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.website.domain.WebSite website;



    /**
     * 设置 [内容]
     */
    public void setContent(String content){
        this.content = content ;
        this.modify("content",content);
    }

    /**
     * 设置 [业务标识]
     */
    public void setContentcode(String contentcode){
        this.contentcode = contentcode ;
        this.modify("contentcode",contentcode);
    }

    /**
     * 设置 [备注]
     */
    public void setMemo(String memo){
        this.memo = memo ;
        this.modify("memo",memo);
    }

    /**
     * 设置 [作者]
     */
    public void setAuthor(String author){
        this.author = author ;
        this.modify("author",author);
    }

    /**
     * 设置 [是否启用]
     */
    public void setValidflag(Integer validflag){
        this.validflag = validflag ;
        this.modify("validflag",validflag);
    }

    /**
     * 设置 [标题]
     */
    public void setTitle(String title){
        this.title = title ;
        this.modify("title",title);
    }

    /**
     * 设置 [内容链接]
     */
    public void setLink(String link){
        this.link = link ;
        this.modify("link",link);
    }

    /**
     * 设置 [序号]
     */
    public void setSn(Integer sn){
        this.sn = sn ;
        this.modify("sn",sn);
    }

    /**
     * 设置 [子标题]
     */
    public void setSubtitle(String subtitle){
        this.subtitle = subtitle ;
        this.modify("subtitle",subtitle);
    }

    /**
     * 设置 [内容名称]
     */
    public void setWebsitecontentname(String websitecontentname){
        this.websitecontentname = websitecontentname ;
        this.modify("websitecontentname",websitecontentname);
    }

    /**
     * 设置 [内容类型]
     */
    public void setContenttype(String contenttype){
        this.contenttype = contenttype ;
        this.modify("contenttype",contenttype);
    }

    /**
     * 设置 [网站标识]
     */
    public void setWebsiteid(String websiteid){
        this.websiteid = websiteid ;
        this.modify("websiteid",websiteid);
    }

    /**
     * 设置 [频道标识]
     */
    public void setWebsitechannelid(String websitechannelid){
        this.websitechannelid = websitechannelid ;
        this.modify("websitechannelid",websitechannelid);
    }


}


