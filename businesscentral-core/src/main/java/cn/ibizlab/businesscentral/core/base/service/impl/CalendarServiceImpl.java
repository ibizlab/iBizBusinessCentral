package cn.ibizlab.businesscentral.core.base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.base.domain.Calendar;
import cn.ibizlab.businesscentral.core.base.filter.CalendarSearchContext;
import cn.ibizlab.businesscentral.core.base.service.ICalendarService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.base.mapper.CalendarMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[日历] 服务对象接口实现
 */
@Slf4j
@Service("CalendarServiceImpl")
public class CalendarServiceImpl extends ServiceImpl<CalendarMapper, Calendar> implements ICalendarService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.scheduling.service.IBookableResourceService bookableresourceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IBusinessUnitService businessunitService;

    protected cn.ibizlab.businesscentral.core.base.service.ICalendarService calendarService = this;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IEquipmentService equipmentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ISlaService slaService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(Calendar et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getCalendarid()),et);
        return true;
    }

    @Override
    public void createBatch(List<Calendar> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Calendar et) {
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("calendarid",et.getCalendarid())))
            return false;
        CachedBeanCopier.copy(get(et.getCalendarid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<Calendar> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Calendar get(String key) {
        Calendar et = getById(key);
        if(et==null){
            et=new Calendar();
            et.setCalendarid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Calendar getDraft(Calendar et) {
        return et;
    }

    @Override
    public boolean checkKey(Calendar et) {
        return (!ObjectUtils.isEmpty(et.getCalendarid()))&&(!Objects.isNull(this.getById(et.getCalendarid())));
    }
    @Override
    @Transactional
    public boolean save(Calendar et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Calendar et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<Calendar> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<Calendar> list) {
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Calendar> selectByBusinessunitid(String businessunitid) {
        return baseMapper.selectByBusinessunitid(businessunitid);
    }

    @Override
    public void removeByBusinessunitid(String businessunitid) {
        this.remove(new QueryWrapper<Calendar>().eq("businessunitid",businessunitid));
    }

	@Override
    public List<Calendar> selectByHolidayschedulecalendarid(String calendarid) {
        return baseMapper.selectByHolidayschedulecalendarid(calendarid);
    }

    @Override
    public void removeByHolidayschedulecalendarid(String calendarid) {
        this.remove(new QueryWrapper<Calendar>().eq("holidayschedulecalendarid",calendarid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<Calendar> searchDefault(CalendarSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Calendar> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Calendar>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Calendar> getCalendarByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Calendar> getCalendarByEntities(List<Calendar> entities) {
        List ids =new ArrayList();
        for(Calendar entity : entities){
            Serializable id=entity.getCalendarid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}



