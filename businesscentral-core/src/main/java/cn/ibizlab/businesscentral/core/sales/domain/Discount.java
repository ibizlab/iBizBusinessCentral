package cn.ibizlab.businesscentral.core.sales.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[折扣]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "DISCOUNT",resultMap = "DiscountResultMap")
public class Discount extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 购物量上限
     */
    @TableField(value = "highquantity")
    @JSONField(name = "highquantity")
    @JsonProperty("highquantity")
    private BigDecimal highquantity;
    /**
     * 折扣名称
     */
    @TableField(value = "discountname")
    @JSONField(name = "discountname")
    @JsonProperty("discountname")
    private String discountname;
    /**
     * Version Number
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * Record Created On
     */
    @TableField(value = "overriddencreatedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;
    /**
     * 折扣
     */
    @DEField(isKeyField=true)
    @TableId(value= "discountid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "discountid")
    @JsonProperty("discountid")
    private String discountid;
    /**
     * 金额 (Base)
     */
    @DEField(name = "amount_base")
    @TableField(value = "amount_base")
    @JSONField(name = "amount_base")
    @JsonProperty("amount_base")
    private BigDecimal amountBase;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 金额
     */
    @TableField(value = "amount")
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private BigDecimal amount;
    /**
     * 状态描述
     */
    @TableField(value = "statuscode")
    @JSONField(name = "statuscode")
    @JsonProperty("statuscode")
    private Integer statuscode;
    /**
     * 购物量下限
     */
    @TableField(value = "lowquantity")
    @JSONField(name = "lowquantity")
    @JsonProperty("lowquantity")
    private BigDecimal lowquantity;
    /**
     * 汇率
     */
    @TableField(value = "exchangerate")
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;
    /**
     * 金额类型
     */
    @DEField(defaultValue = "0")
    @TableField(value = "amounttype")
    @JSONField(name = "amounttype")
    @JsonProperty("amounttype")
    private Integer amounttype;
    /**
     * 组织
     */
    @TableField(value = "organizationid")
    @JSONField(name = "organizationid")
    @JsonProperty("organizationid")
    private String organizationid;
    /**
     * Import Sequence Number
     */
    @TableField(value = "importsequencenumber")
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;
    /**
     * UTC Conversion Time Zone Code
     */
    @TableField(value = "utcconversiontimezonecode")
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;
    /**
     * 百分比
     */
    @TableField(value = "percentage")
    @JSONField(name = "percentage")
    @JsonProperty("percentage")
    private BigDecimal percentage;
    /**
     * 折扣类型
     */
    @TableField(value = "discounttypeid")
    @JSONField(name = "discounttypeid")
    @JsonProperty("discounttypeid")
    private String discounttypeid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.sales.domain.DiscountType discounttype;



    /**
     * 设置 [购物量上限]
     */
    public void setHighquantity(BigDecimal highquantity){
        this.highquantity = highquantity ;
        this.modify("highquantity",highquantity);
    }

    /**
     * 设置 [折扣名称]
     */
    public void setDiscountname(String discountname){
        this.discountname = discountname ;
        this.modify("discountname",discountname);
    }

    /**
     * 设置 [Version Number]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [Record Created On]
     */
    public void setOverriddencreatedon(Timestamp overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 格式化日期 [Record Created On]
     */
    public String formatOverriddencreatedon(){
        if (this.overriddencreatedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(overriddencreatedon);
    }
    /**
     * 设置 [金额 (Base)]
     */
    public void setAmountBase(BigDecimal amountBase){
        this.amountBase = amountBase ;
        this.modify("amount_base",amountBase);
    }

    /**
     * 设置 [金额]
     */
    public void setAmount(BigDecimal amount){
        this.amount = amount ;
        this.modify("amount",amount);
    }

    /**
     * 设置 [状态描述]
     */
    public void setStatuscode(Integer statuscode){
        this.statuscode = statuscode ;
        this.modify("statuscode",statuscode);
    }

    /**
     * 设置 [购物量下限]
     */
    public void setLowquantity(BigDecimal lowquantity){
        this.lowquantity = lowquantity ;
        this.modify("lowquantity",lowquantity);
    }

    /**
     * 设置 [汇率]
     */
    public void setExchangerate(BigDecimal exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [金额类型]
     */
    public void setAmounttype(Integer amounttype){
        this.amounttype = amounttype ;
        this.modify("amounttype",amounttype);
    }

    /**
     * 设置 [组织]
     */
    public void setOrganizationid(String organizationid){
        this.organizationid = organizationid ;
        this.modify("organizationid",organizationid);
    }

    /**
     * 设置 [Import Sequence Number]
     */
    public void setImportsequencenumber(Integer importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [UTC Conversion Time Zone Code]
     */
    public void setUtcconversiontimezonecode(Integer utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [百分比]
     */
    public void setPercentage(BigDecimal percentage){
        this.percentage = percentage ;
        this.modify("percentage",percentage);
    }

    /**
     * 设置 [折扣类型]
     */
    public void setDiscounttypeid(String discounttypeid){
        this.discounttypeid = discounttypeid ;
        this.modify("discounttypeid",discounttypeid);
    }


}


