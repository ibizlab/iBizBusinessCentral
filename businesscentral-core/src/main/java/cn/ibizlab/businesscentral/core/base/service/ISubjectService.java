package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.Subject;
import cn.ibizlab.businesscentral.core.base.filter.SubjectSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Subject] 服务对象接口
 */
public interface ISubjectService extends IService<Subject>{

    boolean create(Subject et) ;
    void createBatch(List<Subject> list) ;
    boolean update(Subject et) ;
    void updateBatch(List<Subject> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    Subject get(String key) ;
    Subject getDraft(Subject et) ;
    boolean checkKey(Subject et) ;
    boolean save(Subject et) ;
    void saveBatch(List<Subject> list) ;
    Page<Subject> searchDefault(SubjectSearchContext context) ;
    List<Subject> selectByParentsubject(String subjectid) ;
    void removeByParentsubject(String subjectid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Subject> getSubjectByIds(List<String> ids) ;
    List<Subject> getSubjectByEntities(List<Subject> entities) ;
}


