package cn.ibizlab.businesscentral.core.base.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.base.domain.Relationships;
/**
 * 关系型数据实体[Relationships] 查询条件对象
 */
@Slf4j
@Data
public class RelationshipsSearchContext extends QueryWrapperContext<Relationships> {

	private String n_relationshipstype_eq;//[关系类型]
	public void setN_relationshipstype_eq(String n_relationshipstype_eq) {
        this.n_relationshipstype_eq = n_relationshipstype_eq;
        if(!ObjectUtils.isEmpty(this.n_relationshipstype_eq)){
            this.getSearchCond().eq("relationshipstype", n_relationshipstype_eq);
        }
    }
	private String n_relationshipsname_like;//[关系名称]
	public void setN_relationshipsname_like(String n_relationshipsname_like) {
        this.n_relationshipsname_like = n_relationshipsname_like;
        if(!ObjectUtils.isEmpty(this.n_relationshipsname_like)){
            this.getSearchCond().like("relationshipsname", n_relationshipsname_like);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("relationshipsname", query)   
            );
		 }
	}
}



