package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.Metric;
import cn.ibizlab.businesscentral.core.base.filter.MetricSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Metric] 服务对象接口
 */
public interface IMetricService extends IService<Metric>{

    boolean create(Metric et) ;
    void createBatch(List<Metric> list) ;
    boolean update(Metric et) ;
    void updateBatch(List<Metric> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    Metric get(String key) ;
    Metric getDraft(Metric et) ;
    boolean checkKey(Metric et) ;
    boolean save(Metric et) ;
    void saveBatch(List<Metric> list) ;
    Page<Metric> searchDefault(MetricSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Metric> getMetricByIds(List<String> ids) ;
    List<Metric> getMetricByEntities(List<Metric> entities) ;
}


