package cn.ibizlab.businesscentral.core.base.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.base.domain.Uom;
/**
 * 关系型数据实体[Uom] 查询条件对象
 */
@Slf4j
@Data
public class UomSearchContext extends QueryWrapperContext<Uom> {

	private String n_uomname_like;//[计量单位名称]
	public void setN_uomname_like(String n_uomname_like) {
        this.n_uomname_like = n_uomname_like;
        if(!ObjectUtils.isEmpty(this.n_uomname_like)){
            this.getSearchCond().like("uomname", n_uomname_like);
        }
    }
	private String n_uomscheduleid_eq;//[单位计划]
	public void setN_uomscheduleid_eq(String n_uomscheduleid_eq) {
        this.n_uomscheduleid_eq = n_uomscheduleid_eq;
        if(!ObjectUtils.isEmpty(this.n_uomscheduleid_eq)){
            this.getSearchCond().eq("uomscheduleid", n_uomscheduleid_eq);
        }
    }
	private String n_baseuom_eq;//[基础单位]
	public void setN_baseuom_eq(String n_baseuom_eq) {
        this.n_baseuom_eq = n_baseuom_eq;
        if(!ObjectUtils.isEmpty(this.n_baseuom_eq)){
            this.getSearchCond().eq("baseuom", n_baseuom_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("uomname", query)   
            );
		 }
	}
}



