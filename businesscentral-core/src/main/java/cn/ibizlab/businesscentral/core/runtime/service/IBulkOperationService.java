package cn.ibizlab.businesscentral.core.runtime.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.runtime.domain.BulkOperation;
import cn.ibizlab.businesscentral.core.runtime.filter.BulkOperationSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[BulkOperation] 服务对象接口
 */
public interface IBulkOperationService extends IService<BulkOperation>{

    boolean create(BulkOperation et) ;
    void createBatch(List<BulkOperation> list) ;
    boolean update(BulkOperation et) ;
    void updateBatch(List<BulkOperation> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    BulkOperation get(String key) ;
    BulkOperation getDraft(BulkOperation et) ;
    boolean checkKey(BulkOperation et) ;
    boolean save(BulkOperation et) ;
    void saveBatch(List<BulkOperation> list) ;
    Page<BulkOperation> searchDefault(BulkOperationSearchContext context) ;
    List<BulkOperation> selectBySlaid(String slaid) ;
    void removeBySlaid(String slaid) ;
    List<BulkOperation> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<BulkOperation> getBulkoperationByIds(List<String> ids) ;
    List<BulkOperation> getBulkoperationByEntities(List<BulkOperation> entities) ;
}


