package cn.ibizlab.businesscentral.core.sales.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.sales.domain.QuoteDetail;
import cn.ibizlab.businesscentral.core.sales.filter.QuoteDetailSearchContext;
import cn.ibizlab.businesscentral.core.sales.service.IQuoteDetailService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.sales.mapper.QuoteDetailMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[报价单产品] 服务对象接口实现
 */
@Slf4j
@Service("QuoteDetailServiceImpl")
public class QuoteDetailServiceImpl extends ServiceImpl<QuoteDetailMapper, QuoteDetail> implements IQuoteDetailService {


    protected cn.ibizlab.businesscentral.core.sales.service.IQuoteDetailService quotedetailService = this;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.ISalesOrderDetailService salesorderdetailService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.product.service.IProductService productService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.IQuoteService quoteService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITransactionCurrencyService transactioncurrencyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IUomService uomService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(QuoteDetail et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getQuotedetailid()),et);
        return true;
    }

    @Override
    public void createBatch(List<QuoteDetail> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(QuoteDetail et) {
        fillParentData(et);
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("quotedetailid",et.getQuotedetailid())))
            return false;
        CachedBeanCopier.copy(get(et.getQuotedetailid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<QuoteDetail> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public QuoteDetail get(String key) {
        QuoteDetail et = getById(key);
        if(et==null){
            et=new QuoteDetail();
            et.setQuotedetailid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public QuoteDetail getDraft(QuoteDetail et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(QuoteDetail et) {
        return (!ObjectUtils.isEmpty(et.getQuotedetailid()))&&(!Objects.isNull(this.getById(et.getQuotedetailid())));
    }
    @Override
    @Transactional
    public boolean save(QuoteDetail et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(QuoteDetail et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<QuoteDetail> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<QuoteDetail> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<QuoteDetail> selectByProductid(String productid) {
        return baseMapper.selectByProductid(productid);
    }

    @Override
    public void removeByProductid(String productid) {
        this.remove(new QueryWrapper<QuoteDetail>().eq("productid",productid));
    }

	@Override
    public List<QuoteDetail> selectByParentbundleidref(String quotedetailid) {
        return baseMapper.selectByParentbundleidref(quotedetailid);
    }

    @Override
    public void removeByParentbundleidref(String quotedetailid) {
        this.remove(new QueryWrapper<QuoteDetail>().eq("parentbundleidref",quotedetailid));
    }

	@Override
    public List<QuoteDetail> selectByQuoteid(String quoteid) {
        return baseMapper.selectByQuoteid(quoteid);
    }

    @Override
    public void removeByQuoteid(String quoteid) {
        this.remove(new QueryWrapper<QuoteDetail>().eq("quoteid",quoteid));
    }

	@Override
    public List<QuoteDetail> selectByTransactioncurrencyid(String transactioncurrencyid) {
        return baseMapper.selectByTransactioncurrencyid(transactioncurrencyid);
    }

    @Override
    public void removeByTransactioncurrencyid(String transactioncurrencyid) {
        this.remove(new QueryWrapper<QuoteDetail>().eq("transactioncurrencyid",transactioncurrencyid));
    }

	@Override
    public List<QuoteDetail> selectByUomid(String uomid) {
        return baseMapper.selectByUomid(uomid);
    }

    @Override
    public void removeByUomid(String uomid) {
        this.remove(new QueryWrapper<QuoteDetail>().eq("uomid",uomid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<QuoteDetail> searchDefault(QuoteDetailSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<QuoteDetail> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<QuoteDetail>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(QuoteDetail et){
        //实体关系[DER1N_QUOTEDETAIL__PRODUCT__PRODUCTID]
        if(!ObjectUtils.isEmpty(et.getProductid())){
            cn.ibizlab.businesscentral.core.product.domain.Product product=et.getProduct();
            if(ObjectUtils.isEmpty(product)){
                cn.ibizlab.businesscentral.core.product.domain.Product majorEntity=productService.get(et.getProductid());
                et.setProduct(majorEntity);
                product=majorEntity;
            }
            et.setProductname(product.getProductname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<QuoteDetail> getQuotedetailByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<QuoteDetail> getQuotedetailByEntities(List<QuoteDetail> entities) {
        List ids =new ArrayList();
        for(QuoteDetail entity : entities){
            Serializable id=entity.getQuotedetailid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}



