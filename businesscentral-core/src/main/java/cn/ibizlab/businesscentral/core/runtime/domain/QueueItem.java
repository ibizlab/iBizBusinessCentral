package cn.ibizlab.businesscentral.core.runtime.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[队列项]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "QUEUEITEM",resultMap = "QueueItemResultMap")
public class QueueItem extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 操作时间
     */
    @TableField(value = "workeridmodifiedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "workeridmodifiedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("workeridmodifiedon")
    private Timestamp workeridmodifiedon;
    /**
     * 优先级
     */
    @TableField(value = "priority")
    @JSONField(name = "priority")
    @JsonProperty("priority")
    private Integer priority;
    /**
     * 工作人员类型
     */
    @TableField(value = "workertype")
    @JSONField(name = "workertype")
    @JsonProperty("workertype")
    private String workertype;
    /**
     * 时区规则版本号
     */
    @TableField(value = "timezoneruleversionnumber")
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;
    /**
     * 发件人
     */
    @TableField(value = "sender")
    @JSONField(name = "sender")
    @JsonProperty("sender")
    private String sender;
    /**
     * UTC 转换时区代码
     */
    @TableField(value = "utcconversiontimezonecode")
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;
    /**
     * 负责人
     */
    @TableField(value = "ownerid")
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;
    /**
     * 收件人
     */
    @TableField(value = "torecipients")
    @JSONField(name = "torecipients")
    @JsonProperty("torecipients")
    private String torecipients;
    /**
     * 对象
     */
    @TableField(value = "objectid")
    @JSONField(name = "objectid")
    @JsonProperty("objectid")
    private String objectid;
    /**
     * 状态(已弃用)
     */
    @TableField(value = "state")
    @JSONField(name = "state")
    @JsonProperty("state")
    private Integer state;
    /**
     * 进入队列时间
     */
    @TableField(value = "enteredon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "enteredon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("enteredon")
    private Timestamp enteredon;
    /**
     * WorkerName
     */
    @TableField(value = "workername")
    @JSONField(name = "workername")
    @JsonProperty("workername")
    private String workername;
    /**
     * 状态描述
     */
    @TableField(value = "statuscode")
    @JSONField(name = "statuscode")
    @JsonProperty("statuscode")
    private Integer statuscode;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 版本号
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 导入序列号
     */
    @TableField(value = "importsequencenumber")
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;
    /**
     * 创建记录的时间
     */
    @TableField(value = "overriddencreatedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;
    /**
     * 队列项
     */
    @DEField(isKeyField=true)
    @TableId(value= "queueitemid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "queueitemid")
    @JsonProperty("queueitemid")
    private String queueitemid;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 汇率
     */
    @TableField(value = "exchangerate")
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;
    /**
     * 负责人类型
     */
    @TableField(value = "ownertype")
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;
    /**
     * 状态
     */
    @TableField(value = "statecode")
    @JSONField(name = "statecode")
    @JsonProperty("statecode")
    private Integer statecode;
    /**
     * 相关对象类型
     */
    @TableField(value = "objecttypecode")
    @JSONField(name = "objecttypecode")
    @JsonProperty("objecttypecode")
    private String objecttypecode;
    /**
     * 状态描述(已弃用)
     */
    @TableField(value = "status")
    @JSONField(name = "status")
    @JsonProperty("status")
    private Integer status;
    /**
     * 操作人员
     */
    @TableField(value = "workerid")
    @JSONField(name = "workerid")
    @JsonProperty("workerid")
    private String workerid;
    /**
     * 货币
     */
    @TableField(value = "transactioncurrencyid")
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;
    /**
     * 队列
     */
    @TableField(value = "queueid")
    @JSONField(name = "queueid")
    @JsonProperty("queueid")
    private String queueid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.runtime.domain.Queue queue;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency transactioncurrency;



    /**
     * 设置 [操作时间]
     */
    public void setWorkeridmodifiedon(Timestamp workeridmodifiedon){
        this.workeridmodifiedon = workeridmodifiedon ;
        this.modify("workeridmodifiedon",workeridmodifiedon);
    }

    /**
     * 格式化日期 [操作时间]
     */
    public String formatWorkeridmodifiedon(){
        if (this.workeridmodifiedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(workeridmodifiedon);
    }
    /**
     * 设置 [优先级]
     */
    public void setPriority(Integer priority){
        this.priority = priority ;
        this.modify("priority",priority);
    }

    /**
     * 设置 [工作人员类型]
     */
    public void setWorkertype(String workertype){
        this.workertype = workertype ;
        this.modify("workertype",workertype);
    }

    /**
     * 设置 [时区规则版本号]
     */
    public void setTimezoneruleversionnumber(Integer timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [发件人]
     */
    public void setSender(String sender){
        this.sender = sender ;
        this.modify("sender",sender);
    }

    /**
     * 设置 [UTC 转换时区代码]
     */
    public void setUtcconversiontimezonecode(Integer utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnerid(String ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [收件人]
     */
    public void setTorecipients(String torecipients){
        this.torecipients = torecipients ;
        this.modify("torecipients",torecipients);
    }

    /**
     * 设置 [对象]
     */
    public void setObjectid(String objectid){
        this.objectid = objectid ;
        this.modify("objectid",objectid);
    }

    /**
     * 设置 [状态(已弃用)]
     */
    public void setState(Integer state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [进入队列时间]
     */
    public void setEnteredon(Timestamp enteredon){
        this.enteredon = enteredon ;
        this.modify("enteredon",enteredon);
    }

    /**
     * 格式化日期 [进入队列时间]
     */
    public String formatEnteredon(){
        if (this.enteredon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(enteredon);
    }
    /**
     * 设置 [WorkerName]
     */
    public void setWorkername(String workername){
        this.workername = workername ;
        this.modify("workername",workername);
    }

    /**
     * 设置 [状态描述]
     */
    public void setStatuscode(Integer statuscode){
        this.statuscode = statuscode ;
        this.modify("statuscode",statuscode);
    }

    /**
     * 设置 [版本号]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [导入序列号]
     */
    public void setImportsequencenumber(Integer importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [创建记录的时间]
     */
    public void setOverriddencreatedon(Timestamp overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 格式化日期 [创建记录的时间]
     */
    public String formatOverriddencreatedon(){
        if (this.overriddencreatedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(overriddencreatedon);
    }
    /**
     * 设置 [汇率]
     */
    public void setExchangerate(BigDecimal exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [负责人类型]
     */
    public void setOwnertype(String ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [状态]
     */
    public void setStatecode(Integer statecode){
        this.statecode = statecode ;
        this.modify("statecode",statecode);
    }

    /**
     * 设置 [相关对象类型]
     */
    public void setObjecttypecode(String objecttypecode){
        this.objecttypecode = objecttypecode ;
        this.modify("objecttypecode",objecttypecode);
    }

    /**
     * 设置 [状态描述(已弃用)]
     */
    public void setStatus(Integer status){
        this.status = status ;
        this.modify("status",status);
    }

    /**
     * 设置 [操作人员]
     */
    public void setWorkerid(String workerid){
        this.workerid = workerid ;
        this.modify("workerid",workerid);
    }

    /**
     * 设置 [货币]
     */
    public void setTransactioncurrencyid(String transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }

    /**
     * 设置 [队列]
     */
    public void setQueueid(String queueid){
        this.queueid = queueid ;
        this.modify("queueid",queueid);
    }


}


