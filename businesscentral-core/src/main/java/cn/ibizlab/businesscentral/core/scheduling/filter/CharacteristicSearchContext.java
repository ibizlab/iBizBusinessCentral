package cn.ibizlab.businesscentral.core.scheduling.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.scheduling.domain.Characteristic;
/**
 * 关系型数据实体[Characteristic] 查询条件对象
 */
@Slf4j
@Data
public class CharacteristicSearchContext extends QueryWrapperContext<Characteristic> {

	private String n_characteristicname_like;//[特征名称]
	public void setN_characteristicname_like(String n_characteristicname_like) {
        this.n_characteristicname_like = n_characteristicname_like;
        if(!ObjectUtils.isEmpty(this.n_characteristicname_like)){
            this.getSearchCond().like("characteristicname", n_characteristicname_like);
        }
    }
	private Integer n_statecode_eq;//[状态]
	public void setN_statecode_eq(Integer n_statecode_eq) {
        this.n_statecode_eq = n_statecode_eq;
        if(!ObjectUtils.isEmpty(this.n_statecode_eq)){
            this.getSearchCond().eq("statecode", n_statecode_eq);
        }
    }
	private String n_characteristictype_eq;//[特征类型]
	public void setN_characteristictype_eq(String n_characteristictype_eq) {
        this.n_characteristictype_eq = n_characteristictype_eq;
        if(!ObjectUtils.isEmpty(this.n_characteristictype_eq)){
            this.getSearchCond().eq("characteristictype", n_characteristictype_eq);
        }
    }
	private Integer n_statuscode_eq;//[状态描述]
	public void setN_statuscode_eq(Integer n_statuscode_eq) {
        this.n_statuscode_eq = n_statuscode_eq;
        if(!ObjectUtils.isEmpty(this.n_statuscode_eq)){
            this.getSearchCond().eq("statuscode", n_statuscode_eq);
        }
    }
	private String n_transactioncurrencyid_eq;//[货币]
	public void setN_transactioncurrencyid_eq(String n_transactioncurrencyid_eq) {
        this.n_transactioncurrencyid_eq = n_transactioncurrencyid_eq;
        if(!ObjectUtils.isEmpty(this.n_transactioncurrencyid_eq)){
            this.getSearchCond().eq("transactioncurrencyid", n_transactioncurrencyid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("characteristicname", query)   
            );
		 }
	}
}



