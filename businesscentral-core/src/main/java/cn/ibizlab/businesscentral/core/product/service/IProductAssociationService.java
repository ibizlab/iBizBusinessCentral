package cn.ibizlab.businesscentral.core.product.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.product.domain.ProductAssociation;
import cn.ibizlab.businesscentral.core.product.filter.ProductAssociationSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[ProductAssociation] 服务对象接口
 */
public interface IProductAssociationService extends IService<ProductAssociation>{

    boolean create(ProductAssociation et) ;
    void createBatch(List<ProductAssociation> list) ;
    boolean update(ProductAssociation et) ;
    void updateBatch(List<ProductAssociation> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    ProductAssociation get(String key) ;
    ProductAssociation getDraft(ProductAssociation et) ;
    boolean checkKey(ProductAssociation et) ;
    boolean save(ProductAssociation et) ;
    void saveBatch(List<ProductAssociation> list) ;
    Page<ProductAssociation> searchDefault(ProductAssociationSearchContext context) ;
    List<ProductAssociation> selectByAssociatedproduct(String productid) ;
    void removeByAssociatedproduct(String productid) ;
    List<ProductAssociation> selectByProductid(String productid) ;
    void removeByProductid(String productid) ;
    List<ProductAssociation> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    List<ProductAssociation> selectByUomid(String uomid) ;
    void removeByUomid(String uomid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<ProductAssociation> getProductassociationByIds(List<String> ids) ;
    List<ProductAssociation> getProductassociationByEntities(List<ProductAssociation> entities) ;
}


