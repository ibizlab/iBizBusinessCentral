package cn.ibizlab.businesscentral.core.base.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[电子邮件模板]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "TEMPLATE",resultMap = "TemplateResultMap")
public class Template extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 解决方案
     */
    @TableField(value = "solutionid")
    @JSONField(name = "solutionid")
    @JsonProperty("solutionid")
    private String solutionid;
    /**
     * OwnerType
     */
    @TableField(value = "ownertype")
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;
    /**
     * 负责人
     */
    @TableField(value = "ownerid")
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;
    /**
     * 回复率
     */
    @TableField(value = "replyrate")
    @JSONField(name = "replyrate")
    @JsonProperty("replyrate")
    private Integer replyrate;
    /**
     * 查看者
     */
    @DEField(defaultValue = "1")
    @TableField(value = "personal")
    @JSONField(name = "personal")
    @JsonProperty("personal")
    private Integer personal;
    /**
     * 模板类型
     */
    @TableField(value = "templatetypecode")
    @JSONField(name = "templatetypecode")
    @JsonProperty("templatetypecode")
    private String templatetypecode;
    /**
     * 托管
     */
    @DEField(defaultValue = "0")
    @TableField(value = "managed")
    @JSONField(name = "managed")
    @JsonProperty("managed")
    private Integer managed;
    /**
     * 导入序列号
     */
    @TableField(value = "importsequencenumber")
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 回复计数
     */
    @TableField(value = "replycount")
    @JSONField(name = "replycount")
    @JsonProperty("replycount")
    private Integer replycount;
    /**
     * 标题
     */
    @TableField(value = "title")
    @JSONField(name = "title")
    @JsonProperty("title")
    private String title;
    /**
     * 打开计数
     */
    @TableField(value = "opencount")
    @JSONField(name = "opencount")
    @JsonProperty("opencount")
    private Integer opencount;
    /**
     * 引入的版本
     */
    @TableField(value = "introducedversion")
    @JSONField(name = "introducedversion")
    @JsonProperty("introducedversion")
    private String introducedversion;
    /**
     * MIME 类型
     */
    @TableField(value = "mimetype")
    @JSONField(name = "mimetype")
    @JsonProperty("mimetype")
    private String mimetype;
    /**
     * 电子邮件模板
     */
    @DEField(isKeyField=true)
    @TableId(value= "templateid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "templateid")
    @JsonProperty("templateid")
    private String templateid;
    /**
     * 推荐
     */
    @DEField(defaultValue = "0")
    @TableField(value = "recommended")
    @JSONField(name = "recommended")
    @JsonProperty("recommended")
    private Integer recommended;
    /**
     * 演示文稿 XML
     */
    @TableField(value = "presentationxml")
    @JSONField(name = "presentationxml")
    @JsonProperty("presentationxml")
    private String presentationxml;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 负责人
     */
    @TableField(value = "ownername")
    @JSONField(name = "ownername")
    @JsonProperty("ownername")
    private String ownername;
    /**
     * 记录替代时间
     */
    @TableField(value = "overwritetime")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overwritetime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overwritetime")
    private Timestamp overwritetime;
    /**
     * 可自定义
     */
    @TableField(value = "customizable")
    @JSONField(name = "customizable")
    @JsonProperty("customizable")
    private String customizable;
    /**
     * 组件状态
     */
    @TableField(value = "componentstate")
    @JSONField(name = "componentstate")
    @JsonProperty("componentstate")
    private String componentstate;
    /**
     * 正文
     */
    @TableField(value = "body")
    @JSONField(name = "body")
    @JsonProperty("body")
    private String body;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 说明
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 主题 XML
     */
    @TableField(value = "subjectpresentationxml")
    @JSONField(name = "subjectpresentationxml")
    @JsonProperty("subjectpresentationxml")
    private String subjectpresentationxml;
    /**
     * 解决方案
     */
    @TableField(value = "supportingsolutionid")
    @JSONField(name = "supportingsolutionid")
    @JsonProperty("supportingsolutionid")
    private String supportingsolutionid;
    /**
     * 生成类型代码
     */
    @TableField(value = "generationtypecode")
    @JSONField(name = "generationtypecode")
    @JsonProperty("generationtypecode")
    private Integer generationtypecode;
    /**
     * 打开率
     */
    @TableField(value = "openrate")
    @JSONField(name = "openrate")
    @JsonProperty("openrate")
    private Integer openrate;
    /**
     * 仅供内部使用。
     */
    @TableField(value = "templateidunique")
    @JSONField(name = "templateidunique")
    @JsonProperty("templateidunique")
    private String templateidunique;
    /**
     * 语言
     */
    @TableField(value = "languagecode")
    @JSONField(name = "languagecode")
    @JsonProperty("languagecode")
    private Integer languagecode;
    /**
     * 已发送电子邮件计数
     */
    @TableField(value = "usedcount")
    @JSONField(name = "usedcount")
    @JsonProperty("usedcount")
    private Integer usedcount;
    /**
     * 版本号
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * 主题
     */
    @TableField(value = "subject")
    @JSONField(name = "subject")
    @JsonProperty("subject")
    private String subject;



    /**
     * 设置 [解决方案]
     */
    public void setSolutionid(String solutionid){
        this.solutionid = solutionid ;
        this.modify("solutionid",solutionid);
    }

    /**
     * 设置 [OwnerType]
     */
    public void setOwnertype(String ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnerid(String ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [回复率]
     */
    public void setReplyrate(Integer replyrate){
        this.replyrate = replyrate ;
        this.modify("replyrate",replyrate);
    }

    /**
     * 设置 [查看者]
     */
    public void setPersonal(Integer personal){
        this.personal = personal ;
        this.modify("personal",personal);
    }

    /**
     * 设置 [模板类型]
     */
    public void setTemplatetypecode(String templatetypecode){
        this.templatetypecode = templatetypecode ;
        this.modify("templatetypecode",templatetypecode);
    }

    /**
     * 设置 [托管]
     */
    public void setManaged(Integer managed){
        this.managed = managed ;
        this.modify("managed",managed);
    }

    /**
     * 设置 [导入序列号]
     */
    public void setImportsequencenumber(Integer importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [回复计数]
     */
    public void setReplycount(Integer replycount){
        this.replycount = replycount ;
        this.modify("replycount",replycount);
    }

    /**
     * 设置 [标题]
     */
    public void setTitle(String title){
        this.title = title ;
        this.modify("title",title);
    }

    /**
     * 设置 [打开计数]
     */
    public void setOpencount(Integer opencount){
        this.opencount = opencount ;
        this.modify("opencount",opencount);
    }

    /**
     * 设置 [引入的版本]
     */
    public void setIntroducedversion(String introducedversion){
        this.introducedversion = introducedversion ;
        this.modify("introducedversion",introducedversion);
    }

    /**
     * 设置 [MIME 类型]
     */
    public void setMimetype(String mimetype){
        this.mimetype = mimetype ;
        this.modify("mimetype",mimetype);
    }

    /**
     * 设置 [推荐]
     */
    public void setRecommended(Integer recommended){
        this.recommended = recommended ;
        this.modify("recommended",recommended);
    }

    /**
     * 设置 [演示文稿 XML]
     */
    public void setPresentationxml(String presentationxml){
        this.presentationxml = presentationxml ;
        this.modify("presentationxml",presentationxml);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnername(String ownername){
        this.ownername = ownername ;
        this.modify("ownername",ownername);
    }

    /**
     * 设置 [记录替代时间]
     */
    public void setOverwritetime(Timestamp overwritetime){
        this.overwritetime = overwritetime ;
        this.modify("overwritetime",overwritetime);
    }

    /**
     * 格式化日期 [记录替代时间]
     */
    public String formatOverwritetime(){
        if (this.overwritetime == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(overwritetime);
    }
    /**
     * 设置 [可自定义]
     */
    public void setCustomizable(String customizable){
        this.customizable = customizable ;
        this.modify("customizable",customizable);
    }

    /**
     * 设置 [组件状态]
     */
    public void setComponentstate(String componentstate){
        this.componentstate = componentstate ;
        this.modify("componentstate",componentstate);
    }

    /**
     * 设置 [正文]
     */
    public void setBody(String body){
        this.body = body ;
        this.modify("body",body);
    }

    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [主题 XML]
     */
    public void setSubjectpresentationxml(String subjectpresentationxml){
        this.subjectpresentationxml = subjectpresentationxml ;
        this.modify("subjectpresentationxml",subjectpresentationxml);
    }

    /**
     * 设置 [解决方案]
     */
    public void setSupportingsolutionid(String supportingsolutionid){
        this.supportingsolutionid = supportingsolutionid ;
        this.modify("supportingsolutionid",supportingsolutionid);
    }

    /**
     * 设置 [生成类型代码]
     */
    public void setGenerationtypecode(Integer generationtypecode){
        this.generationtypecode = generationtypecode ;
        this.modify("generationtypecode",generationtypecode);
    }

    /**
     * 设置 [打开率]
     */
    public void setOpenrate(Integer openrate){
        this.openrate = openrate ;
        this.modify("openrate",openrate);
    }

    /**
     * 设置 [仅供内部使用。]
     */
    public void setTemplateidunique(String templateidunique){
        this.templateidunique = templateidunique ;
        this.modify("templateidunique",templateidunique);
    }

    /**
     * 设置 [语言]
     */
    public void setLanguagecode(Integer languagecode){
        this.languagecode = languagecode ;
        this.modify("languagecode",languagecode);
    }

    /**
     * 设置 [已发送电子邮件计数]
     */
    public void setUsedcount(Integer usedcount){
        this.usedcount = usedcount ;
        this.modify("usedcount",usedcount);
    }

    /**
     * 设置 [版本号]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [主题]
     */
    public void setSubject(String subject){
        this.subject = subject ;
        this.modify("subject",subject);
    }


}


