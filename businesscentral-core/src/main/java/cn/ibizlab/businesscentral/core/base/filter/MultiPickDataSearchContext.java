package cn.ibizlab.businesscentral.core.base.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.base.domain.MultiPickData;
/**
 * 关系型数据实体[MultiPickData] 查询条件对象
 */
@Slf4j
@Data
public class MultiPickDataSearchContext extends QueryWrapperContext<MultiPickData> {

	private String n_pickdataname_like;//[数据名称]
	public void setN_pickdataname_like(String n_pickdataname_like) {
        this.n_pickdataname_like = n_pickdataname_like;
        if(!ObjectUtils.isEmpty(this.n_pickdataname_like)){
            this.getSearchCond().like("pickdataname", n_pickdataname_like);
        }
    }
	private String n_pickdatatype_eq;//[数据类型]
	public void setN_pickdatatype_eq(String n_pickdatatype_eq) {
        this.n_pickdatatype_eq = n_pickdatatype_eq;
        if(!ObjectUtils.isEmpty(this.n_pickdatatype_eq)){
            this.getSearchCond().eq("pickdatatype", n_pickdatatype_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("pickdataname", query)   
            );
		 }
	}
}



