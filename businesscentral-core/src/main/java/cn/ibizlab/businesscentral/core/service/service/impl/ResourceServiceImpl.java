package cn.ibizlab.businesscentral.core.service.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.service.domain.Resource;
import cn.ibizlab.businesscentral.core.service.filter.ResourceSearchContext;
import cn.ibizlab.businesscentral.core.service.service.IResourceService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.service.mapper.ResourceMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[资源] 服务对象接口实现
 */
@Slf4j
@Service("ResourceServiceImpl")
public class ResourceServiceImpl extends ServiceImpl<ResourceMapper, Resource> implements IResourceService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IBusinessUnitService businessunitService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IOrganizationService organizationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ISiteService siteService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(Resource et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getResourceid()),et);
        return true;
    }

    @Override
    public void createBatch(List<Resource> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Resource et) {
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("resourceid",et.getResourceid())))
            return false;
        CachedBeanCopier.copy(get(et.getResourceid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<Resource> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Resource get(String key) {
        Resource et = getById(key);
        if(et==null){
            et=new Resource();
            et.setResourceid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Resource getDraft(Resource et) {
        return et;
    }

    @Override
    public boolean checkKey(Resource et) {
        return (!ObjectUtils.isEmpty(et.getResourceid()))&&(!Objects.isNull(this.getById(et.getResourceid())));
    }
    @Override
    @Transactional
    public boolean save(Resource et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Resource et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<Resource> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<Resource> list) {
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Resource> selectByBusinessunitid(String businessunitid) {
        return baseMapper.selectByBusinessunitid(businessunitid);
    }

    @Override
    public void removeByBusinessunitid(String businessunitid) {
        this.remove(new QueryWrapper<Resource>().eq("businessunitid",businessunitid));
    }

	@Override
    public List<Resource> selectByOrganizationid(String organizationid) {
        return baseMapper.selectByOrganizationid(organizationid);
    }

    @Override
    public void removeByOrganizationid(String organizationid) {
        this.remove(new QueryWrapper<Resource>().eq("organizationid",organizationid));
    }

	@Override
    public List<Resource> selectBySiteid(String siteid) {
        return baseMapper.selectBySiteid(siteid);
    }

    @Override
    public void removeBySiteid(String siteid) {
        this.remove(new QueryWrapper<Resource>().eq("siteid",siteid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<Resource> searchDefault(ResourceSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Resource> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Resource>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Resource> getResourceByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Resource> getResourceByEntities(List<Resource> entities) {
        List ids =new ArrayList();
        for(Resource entity : entities){
            Serializable id=entity.getResourceid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}



