package cn.ibizlab.businesscentral.core.service.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.service.domain.IBizService;
import cn.ibizlab.businesscentral.core.service.filter.IBizServiceSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[IBizService] 服务对象接口
 */
public interface IIBizServiceService extends IService<IBizService>{

    boolean create(IBizService et) ;
    void createBatch(List<IBizService> list) ;
    boolean update(IBizService et) ;
    void updateBatch(List<IBizService> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    IBizService get(String key) ;
    IBizService getDraft(IBizService et) ;
    boolean checkKey(IBizService et) ;
    boolean save(IBizService et) ;
    void saveBatch(List<IBizService> list) ;
    Page<IBizService> searchDefault(IBizServiceSearchContext context) ;
    List<IBizService> selectByResourcespecid(String resourcespecid) ;
    void removeByResourcespecid(String resourcespecid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<IBizService> getIbizserviceByIds(List<String> ids) ;
    List<IBizService> getIbizserviceByEntities(List<IBizService> entities) ;
}


