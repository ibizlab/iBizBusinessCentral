package cn.ibizlab.businesscentral.core.marketing.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[市场活动]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "CAMPAIGN",resultMap = "CampaignResultMap")
public class Campaign extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 预期响应百分比
     */
    @TableField(value = "expectedresponse")
    @JSONField(name = "expectedresponse")
    @JsonProperty("expectedresponse")
    private Integer expectedresponse;
    /**
     * 活动类型
     */
    @TableField(value = "typecode")
    @JSONField(name = "typecode")
    @JsonProperty("typecode")
    private String typecode;
    /**
     * 预算分配
     */
    @TableField(value = "budgetedcost")
    @JSONField(name = "budgetedcost")
    @JsonProperty("budgetedcost")
    private BigDecimal budgetedcost;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 负责人
     */
    @TableField(value = "ownername")
    @JSONField(name = "ownername")
    @JsonProperty("ownername")
    private String ownername;
    /**
     * 市场活动总费用
     */
    @TableField(value = "totalactualcost")
    @JSONField(name = "totalactualcost")
    @JsonProperty("totalactualcost")
    private BigDecimal totalactualcost;
    /**
     * UTC Conversion Time Zone Code
     */
    @TableField(value = "utcconversiontimezonecode")
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;
    /**
     * Import Sequence Number
     */
    @TableField(value = "importsequencenumber")
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;
    /**
     * 其他费用
     */
    @TableField(value = "othercost")
    @JSONField(name = "othercost")
    @JsonProperty("othercost")
    private BigDecimal othercost;
    /**
     * Time Zone Rule Version Number
     */
    @TableField(value = "timezoneruleversionnumber")
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;
    /**
     * 活动名称
     */
    @TableField(value = "campaignname")
    @JSONField(name = "campaignname")
    @JsonProperty("campaignname")
    private String campaignname;
    /**
     * EntityImage_URL
     */
    @DEField(name = "entityimage_url")
    @TableField(value = "entityimage_url")
    @JSONField(name = "entityimage_url")
    @JsonProperty("entityimage_url")
    private String entityimageUrl;
    /**
     * 状态
     */
    @TableField(value = "statecode")
    @JSONField(name = "statecode")
    @JsonProperty("statecode")
    private Integer statecode;
    /**
     * 模板
     */
    @DEField(defaultValue = "0")
    @TableField(value = "template")
    @JSONField(name = "template")
    @JsonProperty("template")
    private Integer template;
    /**
     * 负责人类型
     */
    @TableField(value = "ownertype")
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;
    /**
     * 汇率
     */
    @TableField(value = "exchangerate")
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;
    /**
     * 市场活动
     */
    @DEField(isKeyField=true)
    @TableId(value= "campaignid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "campaignid")
    @JsonProperty("campaignid")
    private String campaignid;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 活动内容
     */
    @TableField(value = "objective")
    @JSONField(name = "objective")
    @JsonProperty("objective")
    private String objective;
    /**
     * 预算分配 (Base)
     */
    @DEField(name = "budgetedcost_base")
    @TableField(value = "budgetedcost_base")
    @JSONField(name = "budgetedcost_base")
    @JsonProperty("budgetedcost_base")
    private BigDecimal budgetedcostBase;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 估计收入 (Base)
     */
    @DEField(name = "expectedrevenue_base")
    @TableField(value = "expectedrevenue_base")
    @JSONField(name = "expectedrevenue_base")
    @JsonProperty("expectedrevenue_base")
    private BigDecimal expectedrevenueBase;
    /**
     * EntityImage_Timestamp
     */
    @DEField(name = "entityimage_timestamp")
    @TableField(value = "entityimage_timestamp")
    @JSONField(name = "entityimage_timestamp")
    @JsonProperty("entityimage_timestamp")
    private BigInteger entityimageTimestamp;
    /**
     * Traversed Path
     */
    @TableField(value = "traversedpath")
    @JSONField(name = "traversedpath")
    @JsonProperty("traversedpath")
    private String traversedpath;
    /**
     * 其他费用 (Base)
     */
    @DEField(name = "othercost_base")
    @TableField(value = "othercost_base")
    @JSONField(name = "othercost_base")
    @JsonProperty("othercost_base")
    private BigDecimal othercostBase;
    /**
     * 拟定结束日期
     */
    @TableField(value = "proposedend")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "proposedend" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("proposedend")
    private Timestamp proposedend;
    /**
     * Version Number
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * 实体图像
     */
    @TableField(value = "entityimage")
    @JSONField(name = "entityimage")
    @JsonProperty("entityimage")
    private String entityimage;
    /**
     * 活动代码
     */
    @TableField(value = "codename")
    @JSONField(name = "codename")
    @JsonProperty("codename")
    private String codename;
    /**
     * 说明
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 实际结束日期
     */
    @TableField(value = "actualend")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "actualend" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("actualend")
    private Timestamp actualend;
    /**
     * 估计收入
     */
    @TableField(value = "expectedrevenue")
    @JSONField(name = "expectedrevenue")
    @JsonProperty("expectedrevenue")
    private BigDecimal expectedrevenue;
    /**
     * PriceListName
     */
    @TableField(value = "pricelistname")
    @JSONField(name = "pricelistname")
    @JsonProperty("pricelistname")
    private String pricelistname;
    /**
     * EntityImageId
     */
    @TableField(value = "entityimageid")
    @JSONField(name = "entityimageid")
    @JsonProperty("entityimageid")
    private String entityimageid;
    /**
     * Process Id
     */
    @TableField(value = "processid")
    @JSONField(name = "processid")
    @JsonProperty("processid")
    private String processid;
    /**
     * 实际开始日期
     */
    @TableField(value = "actualstart")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "actualstart" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("actualstart")
    private Timestamp actualstart;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 负责人
     */
    @TableField(value = "ownerid")
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;
    /**
     * 消息
     */
    @TableField(value = "message")
    @JSONField(name = "message")
    @JsonProperty("message")
    private String message;
    /**
     * Stage Id
     */
    @TableField(value = "stageid")
    @JSONField(name = "stageid")
    @JsonProperty("stageid")
    private String stageid;
    /**
     * 状态描述
     */
    @TableField(value = "statuscode")
    @JSONField(name = "statuscode")
    @JsonProperty("statuscode")
    private Integer statuscode;
    /**
     * Record Created On
     */
    @TableField(value = "overriddencreatedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;
    /**
     * 拟定日期
     */
    @TableField(value = "proposedstart")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "proposedstart" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("proposedstart")
    private Timestamp proposedstart;
    /**
     * 市场活动总费用 (Base)
     */
    @DEField(name = "totalactualcost_base")
    @TableField(value = "totalactualcost_base")
    @JSONField(name = "totalactualcost_base")
    @JsonProperty("totalactualcost_base")
    private BigDecimal totalactualcostBase;
    /**
     * Email Address
     */
    @TableField(value = "emailaddress")
    @JSONField(name = "emailaddress")
    @JsonProperty("emailaddress")
    private String emailaddress;
    /**
     * 促销代码
     */
    @TableField(value = "promotioncodename")
    @JSONField(name = "promotioncodename")
    @JsonProperty("promotioncodename")
    private String promotioncodename;
    /**
     * 货币
     */
    @TableField(value = "currencyname")
    @JSONField(name = "currencyname")
    @JsonProperty("currencyname")
    private String currencyname;
    /**
     * 价目表
     */
    @TableField(value = "pricelistid")
    @JSONField(name = "pricelistid")
    @JsonProperty("pricelistid")
    private String pricelistid;
    /**
     * 货币
     */
    @TableField(value = "transactioncurrencyid")
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.product.domain.PriceLevel pricelist;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency transactioncurrency;



    /**
     * 设置 [预期响应百分比]
     */
    public void setExpectedresponse(Integer expectedresponse){
        this.expectedresponse = expectedresponse ;
        this.modify("expectedresponse",expectedresponse);
    }

    /**
     * 设置 [活动类型]
     */
    public void setTypecode(String typecode){
        this.typecode = typecode ;
        this.modify("typecode",typecode);
    }

    /**
     * 设置 [预算分配]
     */
    public void setBudgetedcost(BigDecimal budgetedcost){
        this.budgetedcost = budgetedcost ;
        this.modify("budgetedcost",budgetedcost);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnername(String ownername){
        this.ownername = ownername ;
        this.modify("ownername",ownername);
    }

    /**
     * 设置 [市场活动总费用]
     */
    public void setTotalactualcost(BigDecimal totalactualcost){
        this.totalactualcost = totalactualcost ;
        this.modify("totalactualcost",totalactualcost);
    }

    /**
     * 设置 [UTC Conversion Time Zone Code]
     */
    public void setUtcconversiontimezonecode(Integer utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [Import Sequence Number]
     */
    public void setImportsequencenumber(Integer importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [其他费用]
     */
    public void setOthercost(BigDecimal othercost){
        this.othercost = othercost ;
        this.modify("othercost",othercost);
    }

    /**
     * 设置 [Time Zone Rule Version Number]
     */
    public void setTimezoneruleversionnumber(Integer timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [活动名称]
     */
    public void setCampaignname(String campaignname){
        this.campaignname = campaignname ;
        this.modify("campaignname",campaignname);
    }

    /**
     * 设置 [EntityImage_URL]
     */
    public void setEntityimageUrl(String entityimageUrl){
        this.entityimageUrl = entityimageUrl ;
        this.modify("entityimage_url",entityimageUrl);
    }

    /**
     * 设置 [状态]
     */
    public void setStatecode(Integer statecode){
        this.statecode = statecode ;
        this.modify("statecode",statecode);
    }

    /**
     * 设置 [模板]
     */
    public void setTemplate(Integer template){
        this.template = template ;
        this.modify("template",template);
    }

    /**
     * 设置 [负责人类型]
     */
    public void setOwnertype(String ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [汇率]
     */
    public void setExchangerate(BigDecimal exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [活动内容]
     */
    public void setObjective(String objective){
        this.objective = objective ;
        this.modify("objective",objective);
    }

    /**
     * 设置 [预算分配 (Base)]
     */
    public void setBudgetedcostBase(BigDecimal budgetedcostBase){
        this.budgetedcostBase = budgetedcostBase ;
        this.modify("budgetedcost_base",budgetedcostBase);
    }

    /**
     * 设置 [估计收入 (Base)]
     */
    public void setExpectedrevenueBase(BigDecimal expectedrevenueBase){
        this.expectedrevenueBase = expectedrevenueBase ;
        this.modify("expectedrevenue_base",expectedrevenueBase);
    }

    /**
     * 设置 [EntityImage_Timestamp]
     */
    public void setEntityimageTimestamp(BigInteger entityimageTimestamp){
        this.entityimageTimestamp = entityimageTimestamp ;
        this.modify("entityimage_timestamp",entityimageTimestamp);
    }

    /**
     * 设置 [Traversed Path]
     */
    public void setTraversedpath(String traversedpath){
        this.traversedpath = traversedpath ;
        this.modify("traversedpath",traversedpath);
    }

    /**
     * 设置 [其他费用 (Base)]
     */
    public void setOthercostBase(BigDecimal othercostBase){
        this.othercostBase = othercostBase ;
        this.modify("othercost_base",othercostBase);
    }

    /**
     * 设置 [拟定结束日期]
     */
    public void setProposedend(Timestamp proposedend){
        this.proposedend = proposedend ;
        this.modify("proposedend",proposedend);
    }

    /**
     * 格式化日期 [拟定结束日期]
     */
    public String formatProposedend(){
        if (this.proposedend == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(proposedend);
    }
    /**
     * 设置 [Version Number]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [实体图像]
     */
    public void setEntityimage(String entityimage){
        this.entityimage = entityimage ;
        this.modify("entityimage",entityimage);
    }

    /**
     * 设置 [活动代码]
     */
    public void setCodename(String codename){
        this.codename = codename ;
        this.modify("codename",codename);
    }

    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [实际结束日期]
     */
    public void setActualend(Timestamp actualend){
        this.actualend = actualend ;
        this.modify("actualend",actualend);
    }

    /**
     * 格式化日期 [实际结束日期]
     */
    public String formatActualend(){
        if (this.actualend == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(actualend);
    }
    /**
     * 设置 [估计收入]
     */
    public void setExpectedrevenue(BigDecimal expectedrevenue){
        this.expectedrevenue = expectedrevenue ;
        this.modify("expectedrevenue",expectedrevenue);
    }

    /**
     * 设置 [PriceListName]
     */
    public void setPricelistname(String pricelistname){
        this.pricelistname = pricelistname ;
        this.modify("pricelistname",pricelistname);
    }

    /**
     * 设置 [EntityImageId]
     */
    public void setEntityimageid(String entityimageid){
        this.entityimageid = entityimageid ;
        this.modify("entityimageid",entityimageid);
    }

    /**
     * 设置 [Process Id]
     */
    public void setProcessid(String processid){
        this.processid = processid ;
        this.modify("processid",processid);
    }

    /**
     * 设置 [实际开始日期]
     */
    public void setActualstart(Timestamp actualstart){
        this.actualstart = actualstart ;
        this.modify("actualstart",actualstart);
    }

    /**
     * 格式化日期 [实际开始日期]
     */
    public String formatActualstart(){
        if (this.actualstart == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(actualstart);
    }
    /**
     * 设置 [负责人]
     */
    public void setOwnerid(String ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [消息]
     */
    public void setMessage(String message){
        this.message = message ;
        this.modify("message",message);
    }

    /**
     * 设置 [Stage Id]
     */
    public void setStageid(String stageid){
        this.stageid = stageid ;
        this.modify("stageid",stageid);
    }

    /**
     * 设置 [状态描述]
     */
    public void setStatuscode(Integer statuscode){
        this.statuscode = statuscode ;
        this.modify("statuscode",statuscode);
    }

    /**
     * 设置 [Record Created On]
     */
    public void setOverriddencreatedon(Timestamp overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 格式化日期 [Record Created On]
     */
    public String formatOverriddencreatedon(){
        if (this.overriddencreatedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(overriddencreatedon);
    }
    /**
     * 设置 [拟定日期]
     */
    public void setProposedstart(Timestamp proposedstart){
        this.proposedstart = proposedstart ;
        this.modify("proposedstart",proposedstart);
    }

    /**
     * 格式化日期 [拟定日期]
     */
    public String formatProposedstart(){
        if (this.proposedstart == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(proposedstart);
    }
    /**
     * 设置 [市场活动总费用 (Base)]
     */
    public void setTotalactualcostBase(BigDecimal totalactualcostBase){
        this.totalactualcostBase = totalactualcostBase ;
        this.modify("totalactualcost_base",totalactualcostBase);
    }

    /**
     * 设置 [Email Address]
     */
    public void setEmailaddress(String emailaddress){
        this.emailaddress = emailaddress ;
        this.modify("emailaddress",emailaddress);
    }

    /**
     * 设置 [促销代码]
     */
    public void setPromotioncodename(String promotioncodename){
        this.promotioncodename = promotioncodename ;
        this.modify("promotioncodename",promotioncodename);
    }

    /**
     * 设置 [货币]
     */
    public void setCurrencyname(String currencyname){
        this.currencyname = currencyname ;
        this.modify("currencyname",currencyname);
    }

    /**
     * 设置 [价目表]
     */
    public void setPricelistid(String pricelistid){
        this.pricelistid = pricelistid ;
        this.modify("pricelistid",pricelistid);
    }

    /**
     * 设置 [货币]
     */
    public void setTransactioncurrencyid(String transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }


}


