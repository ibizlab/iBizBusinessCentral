package cn.ibizlab.businesscentral.core.marketing.service.logic.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieContainer;

import cn.ibizlab.businesscentral.core.marketing.service.logic.IIBizListStopLogic;
import cn.ibizlab.businesscentral.core.marketing.domain.IBizList;

/**
 * 关系型数据实体[Stop] 对象
 */
@Slf4j
@Service
public class IBizListStopLogicImpl implements IIBizListStopLogic{

    @Autowired
    private KieContainer kieContainer;

    @Autowired
    private cn.ibizlab.businesscentral.core.marketing.service.IIBizListService ibizlistservice;

    public cn.ibizlab.businesscentral.core.marketing.service.IIBizListService getIbizlistService() {
        return this.ibizlistservice;
    }


    @Autowired
    private cn.ibizlab.businesscentral.core.marketing.service.IIBizListService iBzSysDefaultService;

    public cn.ibizlab.businesscentral.core.marketing.service.IIBizListService getIBzSysDefaultService() {
        return this.iBzSysDefaultService;
    }

    public void execute(IBizList et){

          KieSession kieSession = null;
        try{
           kieSession=kieContainer.newKieSession();
           kieSession.insert(et); 
           kieSession.setGlobal("ibizliststopdefault",et);
           kieSession.setGlobal("ibizlistservice",ibizlistservice);
           kieSession.setGlobal("iBzSysIbizlistDefaultService",iBzSysDefaultService);
           kieSession.setGlobal("curuser", cn.ibizlab.businesscentral.util.security.AuthenticationUser.getAuthenticationUser());
           kieSession.startProcess("cn.ibizlab.businesscentral.core.marketing.service.logic.ibizliststop");

        }catch(Exception e){
            throw new RuntimeException("执行[停用]处理逻辑发生异常"+e);
        }finally {
            if(kieSession!=null)
            kieSession.destroy();
        }
    }

}
