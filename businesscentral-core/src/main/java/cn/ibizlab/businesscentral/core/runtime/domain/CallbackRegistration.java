package cn.ibizlab.businesscentral.core.runtime.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[回拨注册]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "CALLBACKREGISTRATION",resultMap = "CallbackRegistrationResultMap")
public class CallbackRegistration extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * CallbackRegistrationId
     */
    @DEField(isKeyField=true)
    @TableId(value= "callbackregistrationid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "callbackregistrationid")
    @JsonProperty("callbackregistrationid")
    private String callbackregistrationid;
    /**
     * OwningBusinessUnitName
     */
    @TableField(value = "owningbusinessunitname")
    @JSONField(name = "owningbusinessunitname")
    @JsonProperty("owningbusinessunitname")
    private String owningbusinessunitname;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 负责人类型
     */
    @TableField(value = "ownertype")
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;
    /**
     * EntityName
     */
    @TableField(value = "entityname")
    @JSONField(name = "entityname")
    @JsonProperty("entityname")
    private String entityname;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * FilteringAttributes
     */
    @TableField(value = "filteringattributes")
    @JSONField(name = "filteringattributes")
    @JsonProperty("filteringattributes")
    private String filteringattributes;
    /**
     * Url
     */
    @TableField(value = "url")
    @JSONField(name = "url")
    @JsonProperty("url")
    private String url;
    /**
     * 回拨注册名称
     */
    @TableField(value = "callbackregistrationname")
    @JSONField(name = "callbackregistrationname")
    @JsonProperty("callbackregistrationname")
    private String callbackregistrationname;
    /**
     * Scope
     */
    @TableField(value = "scope")
    @JSONField(name = "scope")
    @JsonProperty("scope")
    private String scope;
    /**
     * Message
     */
    @TableField(value = "message")
    @JSONField(name = "message")
    @JsonProperty("message")
    private String message;
    /**
     * Version
     */
    @TableField(value = "version")
    @JSONField(name = "version")
    @JsonProperty("version")
    private String version;
    /**
     * 负责人
     */
    @TableField(value = "ownerid")
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;



    /**
     * 设置 [OwningBusinessUnitName]
     */
    public void setOwningbusinessunitname(String owningbusinessunitname){
        this.owningbusinessunitname = owningbusinessunitname ;
        this.modify("owningbusinessunitname",owningbusinessunitname);
    }

    /**
     * 设置 [负责人类型]
     */
    public void setOwnertype(String ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [EntityName]
     */
    public void setEntityname(String entityname){
        this.entityname = entityname ;
        this.modify("entityname",entityname);
    }

    /**
     * 设置 [FilteringAttributes]
     */
    public void setFilteringattributes(String filteringattributes){
        this.filteringattributes = filteringattributes ;
        this.modify("filteringattributes",filteringattributes);
    }

    /**
     * 设置 [Url]
     */
    public void setUrl(String url){
        this.url = url ;
        this.modify("url",url);
    }

    /**
     * 设置 [回拨注册名称]
     */
    public void setCallbackregistrationname(String callbackregistrationname){
        this.callbackregistrationname = callbackregistrationname ;
        this.modify("callbackregistrationname",callbackregistrationname);
    }

    /**
     * 设置 [Scope]
     */
    public void setScope(String scope){
        this.scope = scope ;
        this.modify("scope",scope);
    }

    /**
     * 设置 [Message]
     */
    public void setMessage(String message){
        this.message = message ;
        this.modify("message",message);
    }

    /**
     * 设置 [Version]
     */
    public void setVersion(String version){
        this.version = version ;
        this.modify("version",version);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnerid(String ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }


}


