package cn.ibizlab.businesscentral.core.marketing.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.marketing.domain.CampaignCampaign;
import cn.ibizlab.businesscentral.core.marketing.filter.CampaignCampaignSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[CampaignCampaign] 服务对象接口
 */
public interface ICampaignCampaignService extends IService<CampaignCampaign>{

    boolean create(CampaignCampaign et) ;
    void createBatch(List<CampaignCampaign> list) ;
    boolean update(CampaignCampaign et) ;
    void updateBatch(List<CampaignCampaign> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    CampaignCampaign get(String key) ;
    CampaignCampaign getDraft(CampaignCampaign et) ;
    boolean checkKey(CampaignCampaign et) ;
    boolean save(CampaignCampaign et) ;
    void saveBatch(List<CampaignCampaign> list) ;
    Page<CampaignCampaign> searchDefault(CampaignCampaignSearchContext context) ;
    List<CampaignCampaign> selectByEntity2id(String campaignid) ;
    void removeByEntity2id(String campaignid) ;
    List<CampaignCampaign> selectByEntityid(String campaignid) ;
    void removeByEntityid(String campaignid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<CampaignCampaign> getCampaigncampaignByIds(List<String> ids) ;
    List<CampaignCampaign> getCampaigncampaignByEntities(List<CampaignCampaign> entities) ;
}


