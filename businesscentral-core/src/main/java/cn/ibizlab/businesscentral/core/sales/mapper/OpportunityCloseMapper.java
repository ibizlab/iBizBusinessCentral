package cn.ibizlab.businesscentral.core.sales.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.sales.domain.OpportunityClose;
import cn.ibizlab.businesscentral.core.sales.filter.OpportunityCloseSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface OpportunityCloseMapper extends BaseMapper<OpportunityClose>{

    Page<OpportunityClose> searchDefault(IPage page, @Param("srf") OpportunityCloseSearchContext context, @Param("ew") Wrapper<OpportunityClose> wrapper) ;
    @Override
    OpportunityClose selectById(Serializable id);
    @Override
    int insert(OpportunityClose entity);
    @Override
    int updateById(@Param(Constants.ENTITY) OpportunityClose entity);
    @Override
    int update(@Param(Constants.ENTITY) OpportunityClose entity, @Param("ew") Wrapper<OpportunityClose> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<OpportunityClose> selectByCompetitorid(@Param("competitorid") Serializable competitorid) ;

    List<OpportunityClose> selectByOpportunityid(@Param("opportunityid") Serializable opportunityid) ;

    List<OpportunityClose> selectByServiceid(@Param("serviceid") Serializable serviceid) ;

    List<OpportunityClose> selectBySlaid(@Param("slaid") Serializable slaid) ;

    List<OpportunityClose> selectByTransactioncurrencyid(@Param("transactioncurrencyid") Serializable transactioncurrencyid) ;

}
