package cn.ibizlab.businesscentral.core.base.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.base.domain.Email;
/**
 * 关系型数据实体[Email] 查询条件对象
 */
@Slf4j
@Data
public class EmailSearchContext extends QueryWrapperContext<Email> {

	private String n_emailremindertype_eq;//[电子邮件提醒类型]
	public void setN_emailremindertype_eq(String n_emailremindertype_eq) {
        this.n_emailremindertype_eq = n_emailremindertype_eq;
        if(!ObjectUtils.isEmpty(this.n_emailremindertype_eq)){
            this.getSearchCond().eq("emailremindertype", n_emailremindertype_eq);
        }
    }
	private String n_correlationmethod_eq;//[相关性方法]
	public void setN_correlationmethod_eq(String n_correlationmethod_eq) {
        this.n_correlationmethod_eq = n_correlationmethod_eq;
        if(!ObjectUtils.isEmpty(this.n_correlationmethod_eq)){
            this.getSearchCond().eq("correlationmethod", n_correlationmethod_eq);
        }
    }
	private Integer n_statecode_eq;//[活动状态]
	public void setN_statecode_eq(Integer n_statecode_eq) {
        this.n_statecode_eq = n_statecode_eq;
        if(!ObjectUtils.isEmpty(this.n_statecode_eq)){
            this.getSearchCond().eq("statecode", n_statecode_eq);
        }
    }
	private String n_emailreminderstatus_eq;//[电子邮件提醒状态]
	public void setN_emailreminderstatus_eq(String n_emailreminderstatus_eq) {
        this.n_emailreminderstatus_eq = n_emailreminderstatus_eq;
        if(!ObjectUtils.isEmpty(this.n_emailreminderstatus_eq)){
            this.getSearchCond().eq("emailreminderstatus", n_emailreminderstatus_eq);
        }
    }
	private String n_notifications_eq;//[通知]
	public void setN_notifications_eq(String n_notifications_eq) {
        this.n_notifications_eq = n_notifications_eq;
        if(!ObjectUtils.isEmpty(this.n_notifications_eq)){
            this.getSearchCond().eq("notifications", n_notifications_eq);
        }
    }
	private String n_prioritycode_eq;//[优先级]
	public void setN_prioritycode_eq(String n_prioritycode_eq) {
        this.n_prioritycode_eq = n_prioritycode_eq;
        if(!ObjectUtils.isEmpty(this.n_prioritycode_eq)){
            this.getSearchCond().eq("prioritycode", n_prioritycode_eq);
        }
    }
	private String n_deliveryprioritycode_eq;//[传递优先级]
	public void setN_deliveryprioritycode_eq(String n_deliveryprioritycode_eq) {
        this.n_deliveryprioritycode_eq = n_deliveryprioritycode_eq;
        if(!ObjectUtils.isEmpty(this.n_deliveryprioritycode_eq)){
            this.getSearchCond().eq("deliveryprioritycode", n_deliveryprioritycode_eq);
        }
    }
	private Integer n_statuscode_eq;//[状态描述]
	public void setN_statuscode_eq(Integer n_statuscode_eq) {
        this.n_statuscode_eq = n_statuscode_eq;
        if(!ObjectUtils.isEmpty(this.n_statuscode_eq)){
            this.getSearchCond().eq("statuscode", n_statuscode_eq);
        }
    }
	private String n_subject_like;//[主题]
	public void setN_subject_like(String n_subject_like) {
        this.n_subject_like = n_subject_like;
        if(!ObjectUtils.isEmpty(this.n_subject_like)){
            this.getSearchCond().like("subject", n_subject_like);
        }
    }
	private String n_parentactivityid_eq;//[父活动 ID]
	public void setN_parentactivityid_eq(String n_parentactivityid_eq) {
        this.n_parentactivityid_eq = n_parentactivityid_eq;
        if(!ObjectUtils.isEmpty(this.n_parentactivityid_eq)){
            this.getSearchCond().eq("parentactivityid", n_parentactivityid_eq);
        }
    }
	private String n_serviceid_eq;//[服务]
	public void setN_serviceid_eq(String n_serviceid_eq) {
        this.n_serviceid_eq = n_serviceid_eq;
        if(!ObjectUtils.isEmpty(this.n_serviceid_eq)){
            this.getSearchCond().eq("serviceid", n_serviceid_eq);
        }
    }
	private String n_templateid_eq;//[使用的模板的 ID。]
	public void setN_templateid_eq(String n_templateid_eq) {
        this.n_templateid_eq = n_templateid_eq;
        if(!ObjectUtils.isEmpty(this.n_templateid_eq)){
            this.getSearchCond().eq("templateid", n_templateid_eq);
        }
    }
	private String n_slaid_eq;//[SLA]
	public void setN_slaid_eq(String n_slaid_eq) {
        this.n_slaid_eq = n_slaid_eq;
        if(!ObjectUtils.isEmpty(this.n_slaid_eq)){
            this.getSearchCond().eq("slaid", n_slaid_eq);
        }
    }
	private String n_transactioncurrencyid_eq;//[货币]
	public void setN_transactioncurrencyid_eq(String n_transactioncurrencyid_eq) {
        this.n_transactioncurrencyid_eq = n_transactioncurrencyid_eq;
        if(!ObjectUtils.isEmpty(this.n_transactioncurrencyid_eq)){
            this.getSearchCond().eq("transactioncurrencyid", n_transactioncurrencyid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("subject", query)   
            );
		 }
	}
}



