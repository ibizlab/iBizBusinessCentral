package cn.ibizlab.businesscentral.core.finance.service.logic.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieContainer;

import cn.ibizlab.businesscentral.core.finance.service.logic.IInvoicePaidLogic;
import cn.ibizlab.businesscentral.core.finance.domain.Invoice;

/**
 * 关系型数据实体[Paid] 对象
 */
@Slf4j
@Service
public class InvoicePaidLogicImpl implements IInvoicePaidLogic{

    @Autowired
    private KieContainer kieContainer;

    @Autowired
    private cn.ibizlab.businesscentral.core.finance.service.IInvoiceService invoiceservice;

    public cn.ibizlab.businesscentral.core.finance.service.IInvoiceService getInvoiceService() {
        return this.invoiceservice;
    }


    @Autowired
    private cn.ibizlab.businesscentral.core.finance.service.IInvoiceService iBzSysDefaultService;

    public cn.ibizlab.businesscentral.core.finance.service.IInvoiceService getIBzSysDefaultService() {
        return this.iBzSysDefaultService;
    }

    public void execute(Invoice et){

          KieSession kieSession = null;
        try{
           kieSession=kieContainer.newKieSession();
           kieSession.insert(et); 
           kieSession.setGlobal("invoicepaiddefault",et);
           kieSession.setGlobal("invoiceservice",invoiceservice);
           kieSession.setGlobal("iBzSysInvoiceDefaultService",iBzSysDefaultService);
           kieSession.setGlobal("curuser", cn.ibizlab.businesscentral.util.security.AuthenticationUser.getAuthenticationUser());
           kieSession.startProcess("cn.ibizlab.businesscentral.core.finance.service.logic.invoicepaid");

        }catch(Exception e){
            throw new RuntimeException("执行[发票已支付]处理逻辑发生异常"+e);
        }finally {
            if(kieSession!=null)
            kieSession.destroy();
        }
    }

}
