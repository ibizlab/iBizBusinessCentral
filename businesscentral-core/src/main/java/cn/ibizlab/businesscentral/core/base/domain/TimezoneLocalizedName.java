package cn.ibizlab.businesscentral.core.base.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[时区的本地化名称]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "TIMEZONELOCALIZEDNAME",resultMap = "TimezoneLocalizedNameResultMap")
public class TimezoneLocalizedName extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * VersionNumber
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * 用户界面名称
     */
    @TableField(value = "userinterfacename")
    @JSONField(name = "userinterfacename")
    @JsonProperty("userinterfacename")
    private String userinterfacename;
    /**
     * 时区的本地化名称
     */
    @DEField(isKeyField=true)
    @TableId(value= "timezonelocalizednameid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "timezonelocalizednameid")
    @JsonProperty("timezonelocalizednameid")
    private String timezonelocalizednameid;
    /**
     * 标准名称
     */
    @TableField(value = "standardname")
    @JSONField(name = "standardname")
    @JsonProperty("standardname")
    private String standardname;
    /**
     * 区域性
     */
    @TableField(value = "cultureid")
    @JSONField(name = "cultureid")
    @JsonProperty("cultureid")
    private Integer cultureid;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 夏令名称
     */
    @TableField(value = "daylightname")
    @JSONField(name = "daylightname")
    @JsonProperty("daylightname")
    private String daylightname;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 时区定义
     */
    @TableField(value = "timezonedefinitionid")
    @JSONField(name = "timezonedefinitionid")
    @JsonProperty("timezonedefinitionid")
    private String timezonedefinitionid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.TimezoneDefinition timezonedefinition;



    /**
     * 设置 [VersionNumber]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [用户界面名称]
     */
    public void setUserinterfacename(String userinterfacename){
        this.userinterfacename = userinterfacename ;
        this.modify("userinterfacename",userinterfacename);
    }

    /**
     * 设置 [标准名称]
     */
    public void setStandardname(String standardname){
        this.standardname = standardname ;
        this.modify("standardname",standardname);
    }

    /**
     * 设置 [区域性]
     */
    public void setCultureid(Integer cultureid){
        this.cultureid = cultureid ;
        this.modify("cultureid",cultureid);
    }

    /**
     * 设置 [夏令名称]
     */
    public void setDaylightname(String daylightname){
        this.daylightname = daylightname ;
        this.modify("daylightname",daylightname);
    }

    /**
     * 设置 [时区定义]
     */
    public void setTimezonedefinitionid(String timezonedefinitionid){
        this.timezonedefinitionid = timezonedefinitionid ;
        this.modify("timezonedefinitionid",timezonedefinitionid);
    }


}


