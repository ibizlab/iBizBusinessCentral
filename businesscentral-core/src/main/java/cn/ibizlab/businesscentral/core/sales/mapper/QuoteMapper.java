package cn.ibizlab.businesscentral.core.sales.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.sales.domain.Quote;
import cn.ibizlab.businesscentral.core.sales.filter.QuoteSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface QuoteMapper extends BaseMapper<Quote>{

    Page<Quote> searchByParentKey(IPage page, @Param("srf") QuoteSearchContext context, @Param("ew") Wrapper<Quote> wrapper) ;
    Page<Quote> searchClosed(IPage page, @Param("srf") QuoteSearchContext context, @Param("ew") Wrapper<Quote> wrapper) ;
    Page<Quote> searchDefault(IPage page, @Param("srf") QuoteSearchContext context, @Param("ew") Wrapper<Quote> wrapper) ;
    Page<Quote> searchDraft(IPage page, @Param("srf") QuoteSearchContext context, @Param("ew") Wrapper<Quote> wrapper) ;
    Page<Quote> searchEffective(IPage page, @Param("srf") QuoteSearchContext context, @Param("ew") Wrapper<Quote> wrapper) ;
    Page<Quote> searchWin(IPage page, @Param("srf") QuoteSearchContext context, @Param("ew") Wrapper<Quote> wrapper) ;
    @Override
    Quote selectById(Serializable id);
    @Override
    int insert(Quote entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Quote entity);
    @Override
    int update(@Param(Constants.ENTITY) Quote entity, @Param("ew") Wrapper<Quote> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Quote> selectByCampaignid(@Param("campaignid") Serializable campaignid) ;

    List<Quote> selectByOpportunityid(@Param("opportunityid") Serializable opportunityid) ;

    List<Quote> selectByPricelevelid(@Param("pricelevelid") Serializable pricelevelid) ;

    List<Quote> selectBySlaid(@Param("slaid") Serializable slaid) ;

    List<Quote> selectByTransactioncurrencyid(@Param("transactioncurrencyid") Serializable transactioncurrencyid) ;

}
