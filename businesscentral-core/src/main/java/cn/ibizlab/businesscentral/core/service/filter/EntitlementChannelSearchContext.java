package cn.ibizlab.businesscentral.core.service.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.service.domain.EntitlementChannel;
/**
 * 关系型数据实体[EntitlementChannel] 查询条件对象
 */
@Slf4j
@Data
public class EntitlementChannelSearchContext extends QueryWrapperContext<EntitlementChannel> {

	private String n_channel_eq;//[名称]
	public void setN_channel_eq(String n_channel_eq) {
        this.n_channel_eq = n_channel_eq;
        if(!ObjectUtils.isEmpty(this.n_channel_eq)){
            this.getSearchCond().eq("channel", n_channel_eq);
        }
    }
	private String n_entitlementchannelname_like;//[授权渠道名称]
	public void setN_entitlementchannelname_like(String n_entitlementchannelname_like) {
        this.n_entitlementchannelname_like = n_entitlementchannelname_like;
        if(!ObjectUtils.isEmpty(this.n_entitlementchannelname_like)){
            this.getSearchCond().like("entitlementchannelname", n_entitlementchannelname_like);
        }
    }
	private String n_transactioncurrencyid_eq;//[货币]
	public void setN_transactioncurrencyid_eq(String n_transactioncurrencyid_eq) {
        this.n_transactioncurrencyid_eq = n_transactioncurrencyid_eq;
        if(!ObjectUtils.isEmpty(this.n_transactioncurrencyid_eq)){
            this.getSearchCond().eq("transactioncurrencyid", n_transactioncurrencyid_eq);
        }
    }
	private String n_entitlementid_eq;//[权利]
	public void setN_entitlementid_eq(String n_entitlementid_eq) {
        this.n_entitlementid_eq = n_entitlementid_eq;
        if(!ObjectUtils.isEmpty(this.n_entitlementid_eq)){
            this.getSearchCond().eq("entitlementid", n_entitlementid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("entitlementchannelname", query)   
            );
		 }
	}
}



