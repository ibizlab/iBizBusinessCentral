package cn.ibizlab.businesscentral.core.scheduling.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.scheduling.domain.BookableResourceBookingHeader;
import cn.ibizlab.businesscentral.core.scheduling.filter.BookableResourceBookingHeaderSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[BookableResourceBookingHeader] 服务对象接口
 */
public interface IBookableResourceBookingHeaderService extends IService<BookableResourceBookingHeader>{

    boolean create(BookableResourceBookingHeader et) ;
    void createBatch(List<BookableResourceBookingHeader> list) ;
    boolean update(BookableResourceBookingHeader et) ;
    void updateBatch(List<BookableResourceBookingHeader> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    BookableResourceBookingHeader get(String key) ;
    BookableResourceBookingHeader getDraft(BookableResourceBookingHeader et) ;
    boolean checkKey(BookableResourceBookingHeader et) ;
    boolean save(BookableResourceBookingHeader et) ;
    void saveBatch(List<BookableResourceBookingHeader> list) ;
    Page<BookableResourceBookingHeader> searchDefault(BookableResourceBookingHeaderSearchContext context) ;
    List<BookableResourceBookingHeader> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<BookableResourceBookingHeader> getBookableresourcebookingheaderByIds(List<String> ids) ;
    List<BookableResourceBookingHeader> getBookableresourcebookingheaderByEntities(List<BookableResourceBookingHeader> entities) ;
}


