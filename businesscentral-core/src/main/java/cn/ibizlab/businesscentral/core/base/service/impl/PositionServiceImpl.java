package cn.ibizlab.businesscentral.core.base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.base.domain.Position;
import cn.ibizlab.businesscentral.core.base.filter.PositionSearchContext;
import cn.ibizlab.businesscentral.core.base.service.IPositionService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.base.mapper.PositionMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[位置] 服务对象接口实现
 */
@Slf4j
@Service("PositionServiceImpl")
public class PositionServiceImpl extends ServiceImpl<PositionMapper, Position> implements IPositionService {


    protected cn.ibizlab.businesscentral.core.base.service.IPositionService positionService = this;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITransactionCurrencyService transactioncurrencyService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(Position et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getPositionid()),et);
        return true;
    }

    @Override
    public void createBatch(List<Position> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Position et) {
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("positionid",et.getPositionid())))
            return false;
        CachedBeanCopier.copy(get(et.getPositionid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<Position> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Position get(String key) {
        Position et = getById(key);
        if(et==null){
            et=new Position();
            et.setPositionid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Position getDraft(Position et) {
        return et;
    }

    @Override
    public boolean checkKey(Position et) {
        return (!ObjectUtils.isEmpty(et.getPositionid()))&&(!Objects.isNull(this.getById(et.getPositionid())));
    }
    @Override
    @Transactional
    public boolean save(Position et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Position et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<Position> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<Position> list) {
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Position> selectByParentpositionid(String positionid) {
        return baseMapper.selectByParentpositionid(positionid);
    }

    @Override
    public void removeByParentpositionid(String positionid) {
        this.remove(new QueryWrapper<Position>().eq("parentpositionid",positionid));
    }

	@Override
    public List<Position> selectByTransactioncurrencyid(String transactioncurrencyid) {
        return baseMapper.selectByTransactioncurrencyid(transactioncurrencyid);
    }

    @Override
    public void removeByTransactioncurrencyid(String transactioncurrencyid) {
        this.remove(new QueryWrapper<Position>().eq("transactioncurrencyid",transactioncurrencyid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<Position> searchDefault(PositionSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Position> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Position>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Position> getPositionByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Position> getPositionByEntities(List<Position> entities) {
        List ids =new ArrayList();
        for(Position entity : entities){
            Serializable id=entity.getPositionid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}



