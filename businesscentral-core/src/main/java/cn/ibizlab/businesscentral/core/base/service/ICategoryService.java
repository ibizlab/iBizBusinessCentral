package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.Category;
import cn.ibizlab.businesscentral.core.base.filter.CategorySearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Category] 服务对象接口
 */
public interface ICategoryService extends IService<Category>{

    boolean create(Category et) ;
    void createBatch(List<Category> list) ;
    boolean update(Category et) ;
    void updateBatch(List<Category> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    Category get(String key) ;
    Category getDraft(Category et) ;
    boolean checkKey(Category et) ;
    boolean save(Category et) ;
    void saveBatch(List<Category> list) ;
    Page<Category> searchDefault(CategorySearchContext context) ;
    List<Category> selectByParentcategoryid(String categoryid) ;
    void removeByParentcategoryid(String categoryid) ;
    List<Category> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Category> getCategoryByIds(List<String> ids) ;
    List<Category> getCategoryByEntities(List<Category> entities) ;
}


