package cn.ibizlab.businesscentral.core.sales.service.logic.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieContainer;

import cn.ibizlab.businesscentral.core.sales.service.logic.IOpportunityActiveLogic;
import cn.ibizlab.businesscentral.core.sales.domain.Opportunity;

/**
 * 关系型数据实体[Active] 对象
 */
@Slf4j
@Service
public class OpportunityActiveLogicImpl implements IOpportunityActiveLogic{

    @Autowired
    private KieContainer kieContainer;

    @Autowired
    private cn.ibizlab.businesscentral.core.sales.service.IOpportunityService opportunityservice;

    public cn.ibizlab.businesscentral.core.sales.service.IOpportunityService getOpportunityService() {
        return this.opportunityservice;
    }


    @Autowired
    private cn.ibizlab.businesscentral.core.sales.service.IOpportunityService iBzSysDefaultService;

    public cn.ibizlab.businesscentral.core.sales.service.IOpportunityService getIBzSysDefaultService() {
        return this.iBzSysDefaultService;
    }

    public void execute(Opportunity et){

          KieSession kieSession = null;
        try{
           kieSession=kieContainer.newKieSession();
           kieSession.insert(et); 
           kieSession.setGlobal("opportunityactivedefault",et);
           kieSession.setGlobal("opportunityservice",opportunityservice);
           kieSession.setGlobal("iBzSysOpportunityDefaultService",iBzSysDefaultService);
           kieSession.setGlobal("curuser", cn.ibizlab.businesscentral.util.security.AuthenticationUser.getAuthenticationUser());
           kieSession.startProcess("cn.ibizlab.businesscentral.core.sales.service.logic.opportunityactive");

        }catch(Exception e){
            throw new RuntimeException("执行[重新开启商机]处理逻辑发生异常"+e);
        }finally {
            if(kieSession!=null)
            kieSession.destroy();
        }
    }

}
