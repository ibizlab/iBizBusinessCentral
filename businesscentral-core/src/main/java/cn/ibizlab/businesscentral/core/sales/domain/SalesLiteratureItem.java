package cn.ibizlab.businesscentral.core.sales.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[销售附件]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SALESLITERATUREITEM",resultMap = "SalesLiteratureItemResultMap")
public class SalesLiteratureItem extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 作者姓名
     */
    @TableField(value = "authorname")
    @JSONField(name = "authorname")
    @JsonProperty("authorname")
    private String authorname;
    /**
     * Time Zone Rule Version Number
     */
    @TableField(value = "timezoneruleversionnumber")
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;
    /**
     * 标题
     */
    @TableField(value = "title")
    @JSONField(name = "title")
    @JsonProperty("title")
    private String title;
    /**
     * 文件大小(字节)
     */
    @TableField(value = "filesize")
    @JSONField(name = "filesize")
    @JsonProperty("filesize")
    private Integer filesize;
    /**
     * Mode
     */
    @TableField(value = "mode")
    @JSONField(name = "mode")
    @JsonProperty("mode")
    private String mode;
    /**
     * 文件类型
     */
    @TableField(value = "filetypecode")
    @JSONField(name = "filetypecode")
    @JsonProperty("filetypecode")
    private String filetypecode;
    /**
     * Import Sequence Number
     */
    @TableField(value = "importsequencenumber")
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;
    /**
     * 销售宣传资料项
     */
    @DEField(isKeyField=true)
    @TableId(value= "salesliteratureitemid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "salesliteratureitemid")
    @JsonProperty("salesliteratureitemid")
    private String salesliteratureitemid;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * MIME 类型
     */
    @TableField(value = "mimetype")
    @JSONField(name = "mimetype")
    @JsonProperty("mimetype")
    private String mimetype;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 关键字
     */
    @TableField(value = "keywords")
    @JSONField(name = "keywords")
    @JsonProperty("keywords")
    private String keywords;
    /**
     * FileType
     */
    @TableField(value = "filetype")
    @JSONField(name = "filetype")
    @JsonProperty("filetype")
    private Integer filetype;
    /**
     * 附加文档 URL
     */
    @TableField(value = "attacheddocumenturl")
    @JSONField(name = "attacheddocumenturl")
    @JsonProperty("attacheddocumenturl")
    private String attacheddocumenturl;
    /**
     * 显示销售宣传资料文档附件的编码内容。
     */
    @TableField(value = "documentbody")
    @JSONField(name = "documentbody")
    @JsonProperty("documentbody")
    private String documentbody;
    /**
     * Version Number
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * 是否抽象
     */
    @DEField(name = "abstract")
    @TableField(value = "abstract")
    @JSONField(name = "ibizabstract")
    @JsonProperty("ibizabstract")
    private String ibizabstract;
    /**
     * Record Created On
     */
    @TableField(value = "overriddencreatedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;
    /**
     * 向客户公开
     */
    @DEField(defaultValue = "0")
    @TableField(value = "customerviewable")
    @JSONField(name = "customerviewable")
    @JsonProperty("customerviewable")
    private Integer customerviewable;
    /**
     * 文件名
     */
    @TableField(value = "filename")
    @JSONField(name = "filename")
    @JsonProperty("filename")
    private String filename;
    /**
     * 组织
     */
    @TableField(value = "organizationid")
    @JSONField(name = "organizationid")
    @JsonProperty("organizationid")
    private String organizationid;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * UTC Conversion Time Zone Code
     */
    @TableField(value = "utcconversiontimezonecode")
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;
    /**
     * 销售宣传资料
     */
    @TableField(value = "salesliteratureid")
    @JSONField(name = "salesliteratureid")
    @JsonProperty("salesliteratureid")
    private String salesliteratureid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.sales.domain.SalesLiterature salesliterature;



    /**
     * 设置 [作者姓名]
     */
    public void setAuthorname(String authorname){
        this.authorname = authorname ;
        this.modify("authorname",authorname);
    }

    /**
     * 设置 [Time Zone Rule Version Number]
     */
    public void setTimezoneruleversionnumber(Integer timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [标题]
     */
    public void setTitle(String title){
        this.title = title ;
        this.modify("title",title);
    }

    /**
     * 设置 [文件大小(字节)]
     */
    public void setFilesize(Integer filesize){
        this.filesize = filesize ;
        this.modify("filesize",filesize);
    }

    /**
     * 设置 [Mode]
     */
    public void setMode(String mode){
        this.mode = mode ;
        this.modify("mode",mode);
    }

    /**
     * 设置 [文件类型]
     */
    public void setFiletypecode(String filetypecode){
        this.filetypecode = filetypecode ;
        this.modify("filetypecode",filetypecode);
    }

    /**
     * 设置 [Import Sequence Number]
     */
    public void setImportsequencenumber(Integer importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [MIME 类型]
     */
    public void setMimetype(String mimetype){
        this.mimetype = mimetype ;
        this.modify("mimetype",mimetype);
    }

    /**
     * 设置 [关键字]
     */
    public void setKeywords(String keywords){
        this.keywords = keywords ;
        this.modify("keywords",keywords);
    }

    /**
     * 设置 [FileType]
     */
    public void setFiletype(Integer filetype){
        this.filetype = filetype ;
        this.modify("filetype",filetype);
    }

    /**
     * 设置 [附加文档 URL]
     */
    public void setAttacheddocumenturl(String attacheddocumenturl){
        this.attacheddocumenturl = attacheddocumenturl ;
        this.modify("attacheddocumenturl",attacheddocumenturl);
    }

    /**
     * 设置 [显示销售宣传资料文档附件的编码内容。]
     */
    public void setDocumentbody(String documentbody){
        this.documentbody = documentbody ;
        this.modify("documentbody",documentbody);
    }

    /**
     * 设置 [Version Number]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [是否抽象]
     */
    public void setIbizabstract(String ibizabstract){
        this.ibizabstract = ibizabstract ;
        this.modify("abstract",ibizabstract);
    }

    /**
     * 设置 [Record Created On]
     */
    public void setOverriddencreatedon(Timestamp overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 格式化日期 [Record Created On]
     */
    public String formatOverriddencreatedon(){
        if (this.overriddencreatedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(overriddencreatedon);
    }
    /**
     * 设置 [向客户公开]
     */
    public void setCustomerviewable(Integer customerviewable){
        this.customerviewable = customerviewable ;
        this.modify("customerviewable",customerviewable);
    }

    /**
     * 设置 [文件名]
     */
    public void setFilename(String filename){
        this.filename = filename ;
        this.modify("filename",filename);
    }

    /**
     * 设置 [组织]
     */
    public void setOrganizationid(String organizationid){
        this.organizationid = organizationid ;
        this.modify("organizationid",organizationid);
    }

    /**
     * 设置 [UTC Conversion Time Zone Code]
     */
    public void setUtcconversiontimezonecode(Integer utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [销售宣传资料]
     */
    public void setSalesliteratureid(String salesliteratureid){
        this.salesliteratureid = salesliteratureid ;
        this.modify("salesliteratureid",salesliteratureid);
    }


}


