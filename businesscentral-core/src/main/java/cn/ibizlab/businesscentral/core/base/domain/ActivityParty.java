package cn.ibizlab.businesscentral.core.base.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[活动方]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "ACTIVITYPARTY",resultMap = "ActivityPartyResultMap")
public class ActivityParty extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 关联方的电子邮件列号
     */
    @TableField(value = "addressusedemailcolumnnumber")
    @JSONField(name = "addressusedemailcolumnnumber")
    @JsonProperty("addressusedemailcolumnnumber")
    private Integer addressusedemailcolumnnumber;
    /**
     * 不允许使用电子邮件
     */
    @DEField(defaultValue = "0")
    @TableField(value = "donotemail")
    @JSONField(name = "donotemail")
    @JsonProperty("donotemail")
    private Integer donotemail;
    /**
     * VersionNumber
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * 负责人
     */
    @TableField(value = "ownerid")
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;
    /**
     * 约会类型
     */
    @TableField(value = "instancetypecode")
    @JSONField(name = "instancetypecode")
    @JsonProperty("instancetypecode")
    private String instancetypecode;
    /**
     * 活动方
     */
    @DEField(isKeyField=true)
    @TableId(value= "activitypartyid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "activitypartyid")
    @JsonProperty("activitypartyid")
    private String activitypartyid;
    /**
     * 不允许邮递
     */
    @DEField(defaultValue = "0")
    @TableField(value = "donotpostalmail")
    @JSONField(name = "donotpostalmail")
    @JsonProperty("donotpostalmail")
    private Integer donotpostalmail;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 不允许使用传真
     */
    @DEField(defaultValue = "0")
    @TableField(value = "donotfax")
    @JSONField(name = "donotfax")
    @JsonProperty("donotfax")
    private Integer donotfax;
    /**
     * 活动方
     */
    @TableField(value = "partyid")
    @JSONField(name = "partyid")
    @JsonProperty("partyid")
    private String partyid;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 不允许电话联络
     */
    @DEField(defaultValue = "0")
    @TableField(value = "donotphone")
    @JSONField(name = "donotphone")
    @JsonProperty("donotphone")
    private Integer donotphone;
    /**
     * 付出的精力
     */
    @TableField(value = "effort")
    @JSONField(name = "effort")
    @JsonProperty("effort")
    private Double effort;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * PartyIdName
     */
    @TableField(value = "partyidname")
    @JSONField(name = "partyidname")
    @JsonProperty("partyidname")
    private String partyidname;
    /**
     * PartyObjectTypeCode
     */
    @TableField(value = "partyobjecttypecode")
    @JSONField(name = "partyobjecttypecode")
    @JsonProperty("partyobjecttypecode")
    private String partyobjecttypecode;
    /**
     * 负责人类型
     */
    @TableField(value = "ownertype")
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;
    /**
     * 计划结束时间
     */
    @TableField(value = "scheduledend")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "scheduledend" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("scheduledend")
    private Timestamp scheduledend;
    /**
     * 计划开始时间
     */
    @TableField(value = "scheduledstart")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "scheduledstart" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("scheduledstart")
    private Timestamp scheduledstart;
    /**
     * 参与类型
     */
    @TableField(value = "participationtypemask")
    @JSONField(name = "participationtypemask")
    @JsonProperty("participationtypemask")
    private String participationtypemask;
    /**
     * OwningUser
     */
    @TableField(value = "owninguser")
    @JSONField(name = "owninguser")
    @JsonProperty("owninguser")
    private String owninguser;
    /**
     * 交流记录
     */
    @TableField(value = "exchangeentryid")
    @JSONField(name = "exchangeentryid")
    @JsonProperty("exchangeentryid")
    private String exchangeentryid;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 地址
     */
    @TableField(value = "addressused")
    @JSONField(name = "addressused")
    @JsonProperty("addressused")
    private String addressused;
    /**
     * OwningBusinessUnit
     */
    @TableField(value = "owningbusinessunit")
    @JSONField(name = "owningbusinessunit")
    @JsonProperty("owningbusinessunit")
    private String owningbusinessunit;
    /**
     * 已删除参与方
     */
    @DEField(defaultValue = "0")
    @TableField(value = "partydeleted")
    @JSONField(name = "partydeleted")
    @JsonProperty("partydeleted")
    private Integer partydeleted;
    /**
     * 资源规格
     */
    @TableField(value = "resourcespecid")
    @JSONField(name = "resourcespecid")
    @JsonProperty("resourcespecid")
    private String resourcespecid;
    /**
     * 活动
     */
    @TableField(value = "activityid")
    @JSONField(name = "activityid")
    @JsonProperty("activityid")
    private String activityid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.ActivityPointer activity;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.service.domain.ResourceSpec resourcespec;



    /**
     * 设置 [关联方的电子邮件列号]
     */
    public void setAddressusedemailcolumnnumber(Integer addressusedemailcolumnnumber){
        this.addressusedemailcolumnnumber = addressusedemailcolumnnumber ;
        this.modify("addressusedemailcolumnnumber",addressusedemailcolumnnumber);
    }

    /**
     * 设置 [不允许使用电子邮件]
     */
    public void setDonotemail(Integer donotemail){
        this.donotemail = donotemail ;
        this.modify("donotemail",donotemail);
    }

    /**
     * 设置 [VersionNumber]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnerid(String ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [约会类型]
     */
    public void setInstancetypecode(String instancetypecode){
        this.instancetypecode = instancetypecode ;
        this.modify("instancetypecode",instancetypecode);
    }

    /**
     * 设置 [不允许邮递]
     */
    public void setDonotpostalmail(Integer donotpostalmail){
        this.donotpostalmail = donotpostalmail ;
        this.modify("donotpostalmail",donotpostalmail);
    }

    /**
     * 设置 [不允许使用传真]
     */
    public void setDonotfax(Integer donotfax){
        this.donotfax = donotfax ;
        this.modify("donotfax",donotfax);
    }

    /**
     * 设置 [活动方]
     */
    public void setPartyid(String partyid){
        this.partyid = partyid ;
        this.modify("partyid",partyid);
    }

    /**
     * 设置 [不允许电话联络]
     */
    public void setDonotphone(Integer donotphone){
        this.donotphone = donotphone ;
        this.modify("donotphone",donotphone);
    }

    /**
     * 设置 [付出的精力]
     */
    public void setEffort(Double effort){
        this.effort = effort ;
        this.modify("effort",effort);
    }

    /**
     * 设置 [PartyIdName]
     */
    public void setPartyidname(String partyidname){
        this.partyidname = partyidname ;
        this.modify("partyidname",partyidname);
    }

    /**
     * 设置 [PartyObjectTypeCode]
     */
    public void setPartyobjecttypecode(String partyobjecttypecode){
        this.partyobjecttypecode = partyobjecttypecode ;
        this.modify("partyobjecttypecode",partyobjecttypecode);
    }

    /**
     * 设置 [负责人类型]
     */
    public void setOwnertype(String ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [计划结束时间]
     */
    public void setScheduledend(Timestamp scheduledend){
        this.scheduledend = scheduledend ;
        this.modify("scheduledend",scheduledend);
    }

    /**
     * 格式化日期 [计划结束时间]
     */
    public String formatScheduledend(){
        if (this.scheduledend == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(scheduledend);
    }
    /**
     * 设置 [计划开始时间]
     */
    public void setScheduledstart(Timestamp scheduledstart){
        this.scheduledstart = scheduledstart ;
        this.modify("scheduledstart",scheduledstart);
    }

    /**
     * 格式化日期 [计划开始时间]
     */
    public String formatScheduledstart(){
        if (this.scheduledstart == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(scheduledstart);
    }
    /**
     * 设置 [参与类型]
     */
    public void setParticipationtypemask(String participationtypemask){
        this.participationtypemask = participationtypemask ;
        this.modify("participationtypemask",participationtypemask);
    }

    /**
     * 设置 [OwningUser]
     */
    public void setOwninguser(String owninguser){
        this.owninguser = owninguser ;
        this.modify("owninguser",owninguser);
    }

    /**
     * 设置 [交流记录]
     */
    public void setExchangeentryid(String exchangeentryid){
        this.exchangeentryid = exchangeentryid ;
        this.modify("exchangeentryid",exchangeentryid);
    }

    /**
     * 设置 [地址]
     */
    public void setAddressused(String addressused){
        this.addressused = addressused ;
        this.modify("addressused",addressused);
    }

    /**
     * 设置 [OwningBusinessUnit]
     */
    public void setOwningbusinessunit(String owningbusinessunit){
        this.owningbusinessunit = owningbusinessunit ;
        this.modify("owningbusinessunit",owningbusinessunit);
    }

    /**
     * 设置 [已删除参与方]
     */
    public void setPartydeleted(Integer partydeleted){
        this.partydeleted = partydeleted ;
        this.modify("partydeleted",partydeleted);
    }

    /**
     * 设置 [资源规格]
     */
    public void setResourcespecid(String resourcespecid){
        this.resourcespecid = resourcespecid ;
        this.modify("resourcespecid",resourcespecid);
    }

    /**
     * 设置 [活动]
     */
    public void setActivityid(String activityid){
        this.activityid = activityid ;
        this.modify("activityid",activityid);
    }


}


