package cn.ibizlab.businesscentral.core.runtime.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[批量删除失败]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "BULKDELETEFAILURE",resultMap = "BulkDeleteFailureResultMap")
public class BulkDeleteFailure extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 批量删除失败
     */
    @DEField(isKeyField=true)
    @TableId(value= "bulkdeletefailureid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "bulkdeletefailureid")
    @JsonProperty("bulkdeletefailureid")
    private String bulkdeletefailureid;
    /**
     * 负责人类型
     */
    @TableField(value = "ownertype")
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 索引
     */
    @TableField(value = "orderedqueryindex")
    @JSONField(name = "orderedqueryindex")
    @JsonProperty("orderedqueryindex")
    private Integer orderedqueryindex;
    /**
     * 负责人
     */
    @TableField(value = "ownerid")
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;
    /**
     * 错误描述
     */
    @TableField(value = "errordescription")
    @JSONField(name = "errordescription")
    @JsonProperty("errordescription")
    private String errordescription;
    /**
     * 关于
     */
    @TableField(value = "regardingobjectname")
    @JSONField(name = "regardingobjectname")
    @JsonProperty("regardingobjectname")
    private String regardingobjectname;
    /**
     * RegardingObjectTypeCode
     */
    @TableField(value = "regardingobjecttypecode")
    @JSONField(name = "regardingobjecttypecode")
    @JsonProperty("regardingobjecttypecode")
    private String regardingobjecttypecode;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 负责的业务部门
     */
    @TableField(value = "owningbusinessunit")
    @JSONField(name = "owningbusinessunit")
    @JsonProperty("owningbusinessunit")
    private String owningbusinessunit;
    /**
     * 负责人用户
     */
    @TableField(value = "owninguser")
    @JSONField(name = "owninguser")
    @JsonProperty("owninguser")
    private String owninguser;
    /**
     * 错误代码
     */
    @TableField(value = "errornumber")
    @JSONField(name = "errornumber")
    @JsonProperty("errornumber")
    private Integer errornumber;



    /**
     * 设置 [负责人类型]
     */
    public void setOwnertype(String ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [索引]
     */
    public void setOrderedqueryindex(Integer orderedqueryindex){
        this.orderedqueryindex = orderedqueryindex ;
        this.modify("orderedqueryindex",orderedqueryindex);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnerid(String ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [错误描述]
     */
    public void setErrordescription(String errordescription){
        this.errordescription = errordescription ;
        this.modify("errordescription",errordescription);
    }

    /**
     * 设置 [关于]
     */
    public void setRegardingobjectname(String regardingobjectname){
        this.regardingobjectname = regardingobjectname ;
        this.modify("regardingobjectname",regardingobjectname);
    }

    /**
     * 设置 [RegardingObjectTypeCode]
     */
    public void setRegardingobjecttypecode(String regardingobjecttypecode){
        this.regardingobjecttypecode = regardingobjecttypecode ;
        this.modify("regardingobjecttypecode",regardingobjecttypecode);
    }

    /**
     * 设置 [负责的业务部门]
     */
    public void setOwningbusinessunit(String owningbusinessunit){
        this.owningbusinessunit = owningbusinessunit ;
        this.modify("owningbusinessunit",owningbusinessunit);
    }

    /**
     * 设置 [负责人用户]
     */
    public void setOwninguser(String owninguser){
        this.owninguser = owninguser ;
        this.modify("owninguser",owninguser);
    }

    /**
     * 设置 [错误代码]
     */
    public void setErrornumber(Integer errornumber){
        this.errornumber = errornumber ;
        this.modify("errornumber",errornumber);
    }


}


