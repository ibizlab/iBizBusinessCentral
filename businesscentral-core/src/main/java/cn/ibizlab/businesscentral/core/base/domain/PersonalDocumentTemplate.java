package cn.ibizlab.businesscentral.core.base.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[个人文档模板]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "PERSONALDOCUMENTTEMPLATE",resultMap = "PersonalDocumentTemplateResultMap")
public class PersonalDocumentTemplate extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 语言
     */
    @TableField(value = "languagecode")
    @JSONField(name = "languagecode")
    @JsonProperty("languagecode")
    private Integer languagecode;
    /**
     * 个人文档模板的字节数。
     */
    @TableField(value = "content")
    @JSONField(name = "content")
    @JsonProperty("content")
    private String content;
    /**
     * 负责人
     */
    @TableField(value = "ownerid")
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;
    /**
     * 有关此个人文档模板的客户端数据。
     */
    @TableField(value = "clientdata")
    @JSONField(name = "clientdata")
    @JsonProperty("clientdata")
    private String clientdata;
    /**
     * 类型
     */
    @TableField(value = "documenttype")
    @JSONField(name = "documenttype")
    @JsonProperty("documenttype")
    private String documenttype;
    /**
     * 关联的实体类型代码
     */
    @TableField(value = "associatedentitytypecode")
    @JSONField(name = "associatedentitytypecode")
    @JsonProperty("associatedentitytypecode")
    private String associatedentitytypecode;
    /**
     * 说明
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * VersionNumber
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * 负责人类型
     */
    @TableField(value = "ownertype")
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 个人文档模板名称
     */
    @TableField(value = "personaldocumenttemplatename")
    @JSONField(name = "personaldocumenttemplatename")
    @JsonProperty("personaldocumenttemplatename")
    private String personaldocumenttemplatename;
    /**
     * 个人文档模板标识符
     */
    @DEField(isKeyField=true)
    @TableId(value= "personaldocumenttemplateid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "personaldocumenttemplateid")
    @JsonProperty("personaldocumenttemplateid")
    private String personaldocumenttemplateid;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 状态
     */
    @DEField(defaultValue = "0")
    @TableField(value = "status")
    @JSONField(name = "status")
    @JsonProperty("status")
    private Integer status;



    /**
     * 设置 [语言]
     */
    public void setLanguagecode(Integer languagecode){
        this.languagecode = languagecode ;
        this.modify("languagecode",languagecode);
    }

    /**
     * 设置 [个人文档模板的字节数。]
     */
    public void setContent(String content){
        this.content = content ;
        this.modify("content",content);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnerid(String ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [有关此个人文档模板的客户端数据。]
     */
    public void setClientdata(String clientdata){
        this.clientdata = clientdata ;
        this.modify("clientdata",clientdata);
    }

    /**
     * 设置 [类型]
     */
    public void setDocumenttype(String documenttype){
        this.documenttype = documenttype ;
        this.modify("documenttype",documenttype);
    }

    /**
     * 设置 [关联的实体类型代码]
     */
    public void setAssociatedentitytypecode(String associatedentitytypecode){
        this.associatedentitytypecode = associatedentitytypecode ;
        this.modify("associatedentitytypecode",associatedentitytypecode);
    }

    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [VersionNumber]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [负责人类型]
     */
    public void setOwnertype(String ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [个人文档模板名称]
     */
    public void setPersonaldocumenttemplatename(String personaldocumenttemplatename){
        this.personaldocumenttemplatename = personaldocumenttemplatename ;
        this.modify("personaldocumenttemplatename",personaldocumenttemplatename);
    }

    /**
     * 设置 [状态]
     */
    public void setStatus(Integer status){
        this.status = status ;
        this.modify("status",status);
    }


}


