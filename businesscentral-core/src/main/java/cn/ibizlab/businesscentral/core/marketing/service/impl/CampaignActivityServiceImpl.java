package cn.ibizlab.businesscentral.core.marketing.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.marketing.domain.CampaignActivity;
import cn.ibizlab.businesscentral.core.marketing.filter.CampaignActivitySearchContext;
import cn.ibizlab.businesscentral.core.marketing.service.ICampaignActivityService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.marketing.mapper.CampaignActivityMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[市场活动项目] 服务对象接口实现
 */
@Slf4j
@Service("CampaignActivityServiceImpl")
public class CampaignActivityServiceImpl extends ServiceImpl<CampaignActivityMapper, CampaignActivity> implements ICampaignActivityService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ISlaService slaService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(CampaignActivity et) {
        createIndexMajorEntityData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getActivityid()),et);
        return true;
    }

    @Override
    public void createBatch(List<CampaignActivity> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(CampaignActivity et) {
        activitypointerService.update(campaignactivityInheritMapping.toActivitypointer(et));
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("activityid",et.getActivityid())))
            return false;
        CachedBeanCopier.copy(get(et.getActivityid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<CampaignActivity> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        activitypointerService.remove(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public CampaignActivity get(String key) {
        CampaignActivity et = getById(key);
        if(et==null){
            et=new CampaignActivity();
            et.setActivityid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public CampaignActivity getDraft(CampaignActivity et) {
        return et;
    }

    @Override
    public boolean checkKey(CampaignActivity et) {
        return (!ObjectUtils.isEmpty(et.getActivityid()))&&(!Objects.isNull(this.getById(et.getActivityid())));
    }
    @Override
    @Transactional
    public boolean save(CampaignActivity et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(CampaignActivity et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<CampaignActivity> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<CampaignActivity> list) {
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<CampaignActivity> selectBySlaid(String slaid) {
        return baseMapper.selectBySlaid(slaid);
    }

    @Override
    public void removeBySlaid(String slaid) {
        this.remove(new QueryWrapper<CampaignActivity>().eq("slaid",slaid));
    }


    /**
     * 查询集合 ByParentKey
     */
    @Override
    public Page<CampaignActivity> searchByParentKey(CampaignActivitySearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<CampaignActivity> pages=baseMapper.searchByParentKey(context.getPages(),context,context.getSelectCond());
        return new PageImpl<CampaignActivity>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<CampaignActivity> searchDefault(CampaignActivitySearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<CampaignActivity> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<CampaignActivity>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }






    @Autowired
    cn.ibizlab.businesscentral.core.marketing.mapping.CampaignActivityInheritMapping campaignactivityInheritMapping;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IActivityPointerService activitypointerService;

    /**
     * 创建索引主实体数据
     * @param et
     */
    private void createIndexMajorEntityData(CampaignActivity et){
        if(ObjectUtils.isEmpty(et.getActivityid()))
            et.setActivityid((String)et.getDefaultKey(true));
        cn.ibizlab.businesscentral.core.base.domain.ActivityPointer activitypointer =campaignactivityInheritMapping.toActivitypointer(et);
        activitypointer.set("activitytypecode","CAMPAIGNACTIVITY");
        activitypointerService.create(activitypointer);
    }

    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<CampaignActivity> getCampaignactivityByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<CampaignActivity> getCampaignactivityByEntities(List<CampaignActivity> entities) {
        List ids =new ArrayList();
        for(CampaignActivity entity : entities){
            Serializable id=entity.getActivityid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}



