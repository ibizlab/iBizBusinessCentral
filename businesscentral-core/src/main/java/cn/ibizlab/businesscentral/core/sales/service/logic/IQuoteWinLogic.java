package cn.ibizlab.businesscentral.core.sales.service.logic;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.businesscentral.core.sales.domain.Quote;

/**
 * 关系型数据实体[Win] 对象
 */
public interface IQuoteWinLogic {

    void execute(Quote et) ;

}
