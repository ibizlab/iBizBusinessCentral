package cn.ibizlab.businesscentral.core.humanresource.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.humanresource.domain.Leave;
import cn.ibizlab.businesscentral.core.humanresource.filter.LeaveSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Leave] 服务对象接口
 */
public interface ILeaveService extends IService<Leave>{

    boolean create(Leave et) ;
    void createBatch(List<Leave> list) ;
    boolean update(Leave et) ;
    void updateBatch(List<Leave> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    Leave get(String key) ;
    Leave getDraft(Leave et) ;
    boolean checkKey(Leave et) ;
    boolean save(Leave et) ;
    void saveBatch(List<Leave> list) ;
    Page<Leave> searchDefault(LeaveSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Leave> getLeaveByIds(List<String> ids) ;
    List<Leave> getLeaveByEntities(List<Leave> entities) ;
}


