package cn.ibizlab.businesscentral.core.scheduling.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.scheduling.domain.BookableResourceBooking;
import cn.ibizlab.businesscentral.core.scheduling.filter.BookableResourceBookingSearchContext;
import cn.ibizlab.businesscentral.core.scheduling.service.IBookableResourceBookingService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.scheduling.mapper.BookableResourceBookingMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[可预订的资源预订] 服务对象接口实现
 */
@Slf4j
@Service("BookableResourceBookingServiceImpl")
public class BookableResourceBookingServiceImpl extends ServiceImpl<BookableResourceBookingMapper, BookableResourceBooking> implements IBookableResourceBookingService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.scheduling.service.IBookableResourceBookingHeaderService bookableresourcebookingheaderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.scheduling.service.IBookableResourceService bookableresourceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.scheduling.service.IBookingStatusService bookingstatusService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITransactionCurrencyService transactioncurrencyService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(BookableResourceBooking et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getBookableresourcebookingid()),et);
        return true;
    }

    @Override
    public void createBatch(List<BookableResourceBooking> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(BookableResourceBooking et) {
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("bookableresbookingid",et.getBookableresourcebookingid())))
            return false;
        CachedBeanCopier.copy(get(et.getBookableresourcebookingid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<BookableResourceBooking> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public BookableResourceBooking get(String key) {
        BookableResourceBooking et = getById(key);
        if(et==null){
            et=new BookableResourceBooking();
            et.setBookableresourcebookingid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public BookableResourceBooking getDraft(BookableResourceBooking et) {
        return et;
    }

    @Override
    public boolean checkKey(BookableResourceBooking et) {
        return (!ObjectUtils.isEmpty(et.getBookableresourcebookingid()))&&(!Objects.isNull(this.getById(et.getBookableresourcebookingid())));
    }
    @Override
    @Transactional
    public boolean save(BookableResourceBooking et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(BookableResourceBooking et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<BookableResourceBooking> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<BookableResourceBooking> list) {
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<BookableResourceBooking> selectByHeader(String bookableresourcebookingheaderid) {
        return baseMapper.selectByHeader(bookableresourcebookingheaderid);
    }

    @Override
    public void removeByHeader(String bookableresourcebookingheaderid) {
        this.remove(new QueryWrapper<BookableResourceBooking>().eq("header",bookableresourcebookingheaderid));
    }

	@Override
    public List<BookableResourceBooking> selectByResource(String bookableresourceid) {
        return baseMapper.selectByResource(bookableresourceid);
    }

    @Override
    public void removeByResource(String bookableresourceid) {
        this.remove(new QueryWrapper<BookableResourceBooking>().eq("resource",bookableresourceid));
    }

	@Override
    public List<BookableResourceBooking> selectByBookingstatus(String bookingstatusid) {
        return baseMapper.selectByBookingstatus(bookingstatusid);
    }

    @Override
    public void removeByBookingstatus(String bookingstatusid) {
        this.remove(new QueryWrapper<BookableResourceBooking>().eq("bookingstatus",bookingstatusid));
    }

	@Override
    public List<BookableResourceBooking> selectByTransactioncurrencyid(String transactioncurrencyid) {
        return baseMapper.selectByTransactioncurrencyid(transactioncurrencyid);
    }

    @Override
    public void removeByTransactioncurrencyid(String transactioncurrencyid) {
        this.remove(new QueryWrapper<BookableResourceBooking>().eq("transactioncurrencyid",transactioncurrencyid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<BookableResourceBooking> searchDefault(BookableResourceBookingSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<BookableResourceBooking> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<BookableResourceBooking>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<BookableResourceBooking> getBookableresourcebookingByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<BookableResourceBooking> getBookableresourcebookingByEntities(List<BookableResourceBooking> entities) {
        List ids =new ArrayList();
        for(BookableResourceBooking entity : entities){
            Serializable id=entity.getBookableresourcebookingid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}



