package cn.ibizlab.businesscentral.core.base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.base.domain.BusinessUnit;
import cn.ibizlab.businesscentral.core.base.filter.BusinessUnitSearchContext;
import cn.ibizlab.businesscentral.core.base.service.IBusinessUnitService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.base.mapper.BusinessUnitMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[业务部门] 服务对象接口实现
 */
@Slf4j
@Service("BusinessUnitServiceImpl")
public class BusinessUnitServiceImpl extends ServiceImpl<BusinessUnitMapper, BusinessUnit> implements IBusinessUnitService {


    protected cn.ibizlab.businesscentral.core.base.service.IBusinessUnitService businessunitService = this;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ICalendarService calendarService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IConstraintBasedGroupService constraintbasedgroupService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IEquipmentService equipmentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.runtime.service.IQueueService queueService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IResourceGroupService resourcegroupService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IResourceSpecService resourcespecService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IResourceService resourceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ISlaKpiInstanceService slakpiinstanceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ISlaService slaService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITeamService teamService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITransactionCurrencyService transactioncurrencyService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(BusinessUnit et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getBusinessunitid()),et);
        return true;
    }

    @Override
    public void createBatch(List<BusinessUnit> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(BusinessUnit et) {
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("businessunitid",et.getBusinessunitid())))
            return false;
        CachedBeanCopier.copy(get(et.getBusinessunitid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<BusinessUnit> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public BusinessUnit get(String key) {
        BusinessUnit et = getById(key);
        if(et==null){
            et=new BusinessUnit();
            et.setBusinessunitid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public BusinessUnit getDraft(BusinessUnit et) {
        return et;
    }

    @Override
    public boolean checkKey(BusinessUnit et) {
        return (!ObjectUtils.isEmpty(et.getBusinessunitid()))&&(!Objects.isNull(this.getById(et.getBusinessunitid())));
    }
    @Override
    @Transactional
    public boolean save(BusinessUnit et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(BusinessUnit et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<BusinessUnit> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<BusinessUnit> list) {
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<BusinessUnit> selectByParentbusinessunitid(String businessunitid) {
        return baseMapper.selectByParentbusinessunitid(businessunitid);
    }

    @Override
    public void removeByParentbusinessunitid(String businessunitid) {
        this.remove(new QueryWrapper<BusinessUnit>().eq("parentbusinessunitid",businessunitid));
    }

	@Override
    public List<BusinessUnit> selectByCalendarid(String calendarid) {
        return baseMapper.selectByCalendarid(calendarid);
    }

    @Override
    public void removeByCalendarid(String calendarid) {
        this.remove(new QueryWrapper<BusinessUnit>().eq("calendarid",calendarid));
    }

	@Override
    public List<BusinessUnit> selectByTransactioncurrencyid(String transactioncurrencyid) {
        return baseMapper.selectByTransactioncurrencyid(transactioncurrencyid);
    }

    @Override
    public void removeByTransactioncurrencyid(String transactioncurrencyid) {
        this.remove(new QueryWrapper<BusinessUnit>().eq("transactioncurrencyid",transactioncurrencyid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<BusinessUnit> searchDefault(BusinessUnitSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<BusinessUnit> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<BusinessUnit>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<BusinessUnit> getBusinessunitByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<BusinessUnit> getBusinessunitByEntities(List<BusinessUnit> entities) {
        List ids =new ArrayList();
        for(BusinessUnit entity : entities){
            Serializable id=entity.getBusinessunitid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}



