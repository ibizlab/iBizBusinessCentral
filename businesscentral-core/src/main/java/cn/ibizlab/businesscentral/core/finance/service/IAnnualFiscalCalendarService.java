package cn.ibizlab.businesscentral.core.finance.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.finance.domain.AnnualFiscalCalendar;
import cn.ibizlab.businesscentral.core.finance.filter.AnnualFiscalCalendarSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[AnnualFiscalCalendar] 服务对象接口
 */
public interface IAnnualFiscalCalendarService extends IService<AnnualFiscalCalendar>{

    boolean create(AnnualFiscalCalendar et) ;
    void createBatch(List<AnnualFiscalCalendar> list) ;
    boolean update(AnnualFiscalCalendar et) ;
    void updateBatch(List<AnnualFiscalCalendar> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    AnnualFiscalCalendar get(String key) ;
    AnnualFiscalCalendar getDraft(AnnualFiscalCalendar et) ;
    boolean checkKey(AnnualFiscalCalendar et) ;
    boolean save(AnnualFiscalCalendar et) ;
    void saveBatch(List<AnnualFiscalCalendar> list) ;
    Page<AnnualFiscalCalendar> searchDefault(AnnualFiscalCalendarSearchContext context) ;
    List<AnnualFiscalCalendar> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<AnnualFiscalCalendar> getAnnualfiscalcalendarByIds(List<String> ids) ;
    List<AnnualFiscalCalendar> getAnnualfiscalcalendarByEntities(List<AnnualFiscalCalendar> entities) ;
}


