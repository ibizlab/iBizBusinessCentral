package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.BusinessUnitNewsArticle;
import cn.ibizlab.businesscentral.core.base.filter.BusinessUnitNewsArticleSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[BusinessUnitNewsArticle] 服务对象接口
 */
public interface IBusinessUnitNewsArticleService extends IService<BusinessUnitNewsArticle>{

    boolean create(BusinessUnitNewsArticle et) ;
    void createBatch(List<BusinessUnitNewsArticle> list) ;
    boolean update(BusinessUnitNewsArticle et) ;
    void updateBatch(List<BusinessUnitNewsArticle> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    BusinessUnitNewsArticle get(String key) ;
    BusinessUnitNewsArticle getDraft(BusinessUnitNewsArticle et) ;
    boolean checkKey(BusinessUnitNewsArticle et) ;
    boolean save(BusinessUnitNewsArticle et) ;
    void saveBatch(List<BusinessUnitNewsArticle> list) ;
    Page<BusinessUnitNewsArticle> searchDefault(BusinessUnitNewsArticleSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<BusinessUnitNewsArticle> getBusinessunitnewsarticleByIds(List<String> ids) ;
    List<BusinessUnitNewsArticle> getBusinessunitnewsarticleByEntities(List<BusinessUnitNewsArticle> entities) ;
}


