

package cn.ibizlab.businesscentral.core.base.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.base.domain.PhoneCall;
import cn.ibizlab.businesscentral.core.base.domain.ActivityPointer;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface PhoneCallInheritMapping {

    @Mappings({
        @Mapping(source ="activityid",target = "activityid"),
        @Mapping(source ="subject",target = "subject"),
        @Mapping(target ="focusNull",ignore = true),
    })
    ActivityPointer toActivitypointer(PhoneCall phonecall);

    @Mappings({
        @Mapping(source ="activityid" ,target = "activityid"),
        @Mapping(source ="subject" ,target = "subject"),
        @Mapping(target ="focusNull",ignore = true),
    })
    PhoneCall toPhonecall(ActivityPointer activitypointer);

    List<ActivityPointer> toActivitypointer(List<PhoneCall> phonecall);

    List<PhoneCall> toPhonecall(List<ActivityPointer> activitypointer);

}


