package cn.ibizlab.businesscentral.core.humanresource.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[员工]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "EMPLOYEE",resultMap = "EmployeeResultMap")
public class Employee extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 员工名称
     */
    @TableField(value = "employeename")
    @JSONField(name = "employeename")
    @JsonProperty("employeename")
    private String employeename;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 员工标识
     */
    @DEField(isKeyField=true)
    @TableId(value= "employeeid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "employeeid")
    @JsonProperty("employeeid")
    private String employeeid;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 组织
     */
    @TableField(value = "organizationid")
    @JSONField(name = "organizationid")
    @JsonProperty("organizationid")
    private String organizationid;
    /**
     * 组织编码
     */
    @TableField(value = "orgcode")
    @JSONField(name = "orgcode")
    @JsonProperty("orgcode")
    private String orgcode;
    /**
     * 组织层级
     */
    @TableField(value = "orglevel")
    @JSONField(name = "orglevel")
    @JsonProperty("orglevel")
    private BigInteger orglevel;
    /**
     * 排序号
     */
    @TableField(value = "showorder")
    @JSONField(name = "showorder")
    @JsonProperty("showorder")
    private BigInteger showorder;
    /**
     * 组织简称
     */
    @TableField(value = "shortname")
    @JSONField(name = "shortname")
    @JsonProperty("shortname")
    private String shortname;
    /**
     * 员工编号
     */
    @TableField(value = "employeecode")
    @JSONField(name = "employeecode")
    @JsonProperty("employeecode")
    private String employeecode;
    /**
     * 出生日期
     */
    @TableField(value = "birthday")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "birthday" , format="yyyy-MM-dd")
    @JsonProperty("birthday")
    private Timestamp birthday;
    /**
     * 证件号码
     */
    @TableField(value = "certnum")
    @JSONField(name = "certnum")
    @JsonProperty("certnum")
    private String certnum;
    /**
     * 年龄
     */
    @TableField(exist = false)
    @JSONField(name = "age")
    @JsonProperty("age")
    private String age;
    /**
     * 移动电话
     */
    @TableField(value = "mobile")
    @JSONField(name = "mobile")
    @JsonProperty("mobile")
    private String mobile;
    /**
     * 电子邮箱
     */
    @TableField(value = "email")
    @JSONField(name = "email")
    @JsonProperty("email")
    private String email;
    /**
     * 出生地
     */
    @TableField(value = "birthaddress")
    @JSONField(name = "birthaddress")
    @JsonProperty("birthaddress")
    private String birthaddress;
    /**
     * 通讯地址
     */
    @TableField(value = "postaladdress")
    @JSONField(name = "postaladdress")
    @JsonProperty("postaladdress")
    private String postaladdress;
    /**
     * 爱好特长
     */
    @TableField(value = "hobby")
    @JSONField(name = "hobby")
    @JsonProperty("hobby")
    private String hobby;
    /**
     * 户籍地址
     */
    @TableField(value = "nativeaddress")
    @JSONField(name = "nativeaddress")
    @JsonProperty("nativeaddress")
    private String nativeaddress;
    /**
     * 到本单位时间
     */
    @TableField(value = "startorgtime")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "startorgtime" , format="yyyy-MM-dd")
    @JsonProperty("startorgtime")
    private Timestamp startorgtime;
    /**
     * 健康状况
     */
    @TableField(value = "health")
    @JSONField(name = "health")
    @JsonProperty("health")
    private String health;
    /**
     * 参加工作时间
     */
    @TableField(value = "startworktime")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "startworktime" , format="yyyy-MM-dd")
    @JsonProperty("startworktime")
    private Timestamp startworktime;
    /**
     * 照片
     */
    @TableField(value = "photo")
    @JSONField(name = "photo")
    @JsonProperty("photo")
    private String photo;
    /**
     * 技术职称
     */
    @TableField(value = "technicaltitle")
    @JSONField(name = "technicaltitle")
    @JsonProperty("technicaltitle")
    private String technicaltitle;
    /**
     * 籍贯
     */
    @TableField(value = "nativeplace")
    @JSONField(name = "nativeplace")
    @JsonProperty("nativeplace")
    private String nativeplace;
    /**
     * 执业证书
     */
    @TableField(value = "certificates")
    @JSONField(name = "certificates")
    @JsonProperty("certificates")
    private String certificates;
    /**
     * 固定电话
     */
    @TableField(value = "telephone")
    @JSONField(name = "telephone")
    @JsonProperty("telephone")
    private String telephone;
    /**
     * 血型
     */
    @TableField(value = "bloodtype")
    @JSONField(name = "bloodtype")
    @JsonProperty("bloodtype")
    private String bloodtype;
    /**
     * 民族
     */
    @TableField(value = "nation")
    @JSONField(name = "nation")
    @JsonProperty("nation")
    private String nation;
    /**
     * 证件类型
     */
    @TableField(value = "certtype")
    @JSONField(name = "certtype")
    @JsonProperty("certtype")
    private String certtype;
    /**
     * 性别
     */
    @TableField(value = "sex")
    @JSONField(name = "sex")
    @JsonProperty("sex")
    private String sex;
    /**
     * 婚姻状况
     */
    @TableField(value = "marriage")
    @JSONField(name = "marriage")
    @JsonProperty("marriage")
    private String marriage;
    /**
     * 户口类型
     */
    @TableField(value = "nativetype")
    @JSONField(name = "nativetype")
    @JsonProperty("nativetype")
    private String nativetype;
    /**
     * 政治面貌
     */
    @TableField(value = "political")
    @JSONField(name = "political")
    @JsonProperty("political")
    private String political;
    /**
     * 组织
     */
    @TableField(exist = false)
    @JSONField(name = "organizationname")
    @JsonProperty("organizationname")
    private String organizationname;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.Organization organization;



    /**
     * 设置 [员工名称]
     */
    public void setEmployeename(String employeename){
        this.employeename = employeename ;
        this.modify("employeename",employeename);
    }

    /**
     * 设置 [组织]
     */
    public void setOrganizationid(String organizationid){
        this.organizationid = organizationid ;
        this.modify("organizationid",organizationid);
    }

    /**
     * 设置 [组织编码]
     */
    public void setOrgcode(String orgcode){
        this.orgcode = orgcode ;
        this.modify("orgcode",orgcode);
    }

    /**
     * 设置 [组织层级]
     */
    public void setOrglevel(BigInteger orglevel){
        this.orglevel = orglevel ;
        this.modify("orglevel",orglevel);
    }

    /**
     * 设置 [排序号]
     */
    public void setShoworder(BigInteger showorder){
        this.showorder = showorder ;
        this.modify("showorder",showorder);
    }

    /**
     * 设置 [组织简称]
     */
    public void setShortname(String shortname){
        this.shortname = shortname ;
        this.modify("shortname",shortname);
    }

    /**
     * 设置 [员工编号]
     */
    public void setEmployeecode(String employeecode){
        this.employeecode = employeecode ;
        this.modify("employeecode",employeecode);
    }

    /**
     * 设置 [出生日期]
     */
    public void setBirthday(Timestamp birthday){
        this.birthday = birthday ;
        this.modify("birthday",birthday);
    }

    /**
     * 格式化日期 [出生日期]
     */
    public String formatBirthday(){
        if (this.birthday == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(birthday);
    }
    /**
     * 设置 [证件号码]
     */
    public void setCertnum(String certnum){
        this.certnum = certnum ;
        this.modify("certnum",certnum);
    }

    /**
     * 设置 [移动电话]
     */
    public void setMobile(String mobile){
        this.mobile = mobile ;
        this.modify("mobile",mobile);
    }

    /**
     * 设置 [电子邮箱]
     */
    public void setEmail(String email){
        this.email = email ;
        this.modify("email",email);
    }

    /**
     * 设置 [出生地]
     */
    public void setBirthaddress(String birthaddress){
        this.birthaddress = birthaddress ;
        this.modify("birthaddress",birthaddress);
    }

    /**
     * 设置 [通讯地址]
     */
    public void setPostaladdress(String postaladdress){
        this.postaladdress = postaladdress ;
        this.modify("postaladdress",postaladdress);
    }

    /**
     * 设置 [爱好特长]
     */
    public void setHobby(String hobby){
        this.hobby = hobby ;
        this.modify("hobby",hobby);
    }

    /**
     * 设置 [户籍地址]
     */
    public void setNativeaddress(String nativeaddress){
        this.nativeaddress = nativeaddress ;
        this.modify("nativeaddress",nativeaddress);
    }

    /**
     * 设置 [到本单位时间]
     */
    public void setStartorgtime(Timestamp startorgtime){
        this.startorgtime = startorgtime ;
        this.modify("startorgtime",startorgtime);
    }

    /**
     * 格式化日期 [到本单位时间]
     */
    public String formatStartorgtime(){
        if (this.startorgtime == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(startorgtime);
    }
    /**
     * 设置 [健康状况]
     */
    public void setHealth(String health){
        this.health = health ;
        this.modify("health",health);
    }

    /**
     * 设置 [参加工作时间]
     */
    public void setStartworktime(Timestamp startworktime){
        this.startworktime = startworktime ;
        this.modify("startworktime",startworktime);
    }

    /**
     * 格式化日期 [参加工作时间]
     */
    public String formatStartworktime(){
        if (this.startworktime == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(startworktime);
    }
    /**
     * 设置 [照片]
     */
    public void setPhoto(String photo){
        this.photo = photo ;
        this.modify("photo",photo);
    }

    /**
     * 设置 [技术职称]
     */
    public void setTechnicaltitle(String technicaltitle){
        this.technicaltitle = technicaltitle ;
        this.modify("technicaltitle",technicaltitle);
    }

    /**
     * 设置 [籍贯]
     */
    public void setNativeplace(String nativeplace){
        this.nativeplace = nativeplace ;
        this.modify("nativeplace",nativeplace);
    }

    /**
     * 设置 [执业证书]
     */
    public void setCertificates(String certificates){
        this.certificates = certificates ;
        this.modify("certificates",certificates);
    }

    /**
     * 设置 [固定电话]
     */
    public void setTelephone(String telephone){
        this.telephone = telephone ;
        this.modify("telephone",telephone);
    }

    /**
     * 设置 [血型]
     */
    public void setBloodtype(String bloodtype){
        this.bloodtype = bloodtype ;
        this.modify("bloodtype",bloodtype);
    }

    /**
     * 设置 [民族]
     */
    public void setNation(String nation){
        this.nation = nation ;
        this.modify("nation",nation);
    }

    /**
     * 设置 [证件类型]
     */
    public void setCerttype(String certtype){
        this.certtype = certtype ;
        this.modify("certtype",certtype);
    }

    /**
     * 设置 [性别]
     */
    public void setSex(String sex){
        this.sex = sex ;
        this.modify("sex",sex);
    }

    /**
     * 设置 [婚姻状况]
     */
    public void setMarriage(String marriage){
        this.marriage = marriage ;
        this.modify("marriage",marriage);
    }

    /**
     * 设置 [户口类型]
     */
    public void setNativetype(String nativetype){
        this.nativetype = nativetype ;
        this.modify("nativetype",nativetype);
    }

    /**
     * 设置 [政治面貌]
     */
    public void setPolitical(String political){
        this.political = political ;
        this.modify("political",political);
    }


}


