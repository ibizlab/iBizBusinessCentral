package cn.ibizlab.businesscentral.core.sales.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.sales.domain.OrderClose;
import cn.ibizlab.businesscentral.core.sales.filter.OrderCloseSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[OrderClose] 服务对象接口
 */
public interface IOrderCloseService extends IService<OrderClose>{

    boolean create(OrderClose et) ;
    void createBatch(List<OrderClose> list) ;
    boolean update(OrderClose et) ;
    void updateBatch(List<OrderClose> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    OrderClose get(String key) ;
    OrderClose getDraft(OrderClose et) ;
    boolean checkKey(OrderClose et) ;
    boolean save(OrderClose et) ;
    void saveBatch(List<OrderClose> list) ;
    Page<OrderClose> searchDefault(OrderCloseSearchContext context) ;
    List<OrderClose> selectBySalesorderid(String salesorderid) ;
    void removeBySalesorderid(String salesorderid) ;
    List<OrderClose> selectByServiceid(String serviceid) ;
    void removeByServiceid(String serviceid) ;
    List<OrderClose> selectBySlaid(String slaid) ;
    void removeBySlaid(String slaid) ;
    List<OrderClose> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<OrderClose> getOrdercloseByIds(List<String> ids) ;
    List<OrderClose> getOrdercloseByEntities(List<OrderClose> entities) ;
}


