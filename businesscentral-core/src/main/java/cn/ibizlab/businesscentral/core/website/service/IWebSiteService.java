package cn.ibizlab.businesscentral.core.website.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.website.domain.WebSite;
import cn.ibizlab.businesscentral.core.website.filter.WebSiteSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[WebSite] 服务对象接口
 */
public interface IWebSiteService extends IService<WebSite>{

    boolean create(WebSite et) ;
    void createBatch(List<WebSite> list) ;
    boolean update(WebSite et) ;
    void updateBatch(List<WebSite> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    WebSite get(String key) ;
    WebSite getDraft(WebSite et) ;
    boolean checkKey(WebSite et) ;
    boolean save(WebSite et) ;
    void saveBatch(List<WebSite> list) ;
    Page<WebSite> searchDefault(WebSiteSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<WebSite> getWebsiteByIds(List<String> ids) ;
    List<WebSite> getWebsiteByEntities(List<WebSite> entities) ;
}


