package cn.ibizlab.businesscentral.core.base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.base.domain.KnowledgeArticle;
import cn.ibizlab.businesscentral.core.base.filter.KnowledgeArticleSearchContext;
import cn.ibizlab.businesscentral.core.base.service.IKnowledgeArticleService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.base.mapper.KnowledgeArticleMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[知识文章] 服务对象接口实现
 */
@Slf4j
@Service("KnowledgeArticleServiceImpl")
public class KnowledgeArticleServiceImpl extends ServiceImpl<KnowledgeArticleMapper, KnowledgeArticle> implements IKnowledgeArticleService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.runtime.service.IExpiredprocessService expiredprocessService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IKnowledgeArticleIncidentService knowledgearticleincidentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IKnowledgeArticleViewsService knowledgearticleviewsService;

    protected cn.ibizlab.businesscentral.core.base.service.IKnowledgeArticleService knowledgearticleService = this;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ILanguageLocaleService languagelocaleService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ISubjectService subjectService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITransactionCurrencyService transactioncurrencyService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(KnowledgeArticle et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getKnowledgearticleid()),et);
        return true;
    }

    @Override
    public void createBatch(List<KnowledgeArticle> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(KnowledgeArticle et) {
        fillParentData(et);
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("knowledgearticleid",et.getKnowledgearticleid())))
            return false;
        CachedBeanCopier.copy(get(et.getKnowledgearticleid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<KnowledgeArticle> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public KnowledgeArticle get(String key) {
        KnowledgeArticle et = getById(key);
        if(et==null){
            et=new KnowledgeArticle();
            et.setKnowledgearticleid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public KnowledgeArticle getDraft(KnowledgeArticle et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(KnowledgeArticle et) {
        return (!ObjectUtils.isEmpty(et.getKnowledgearticleid()))&&(!Objects.isNull(this.getById(et.getKnowledgearticleid())));
    }
    @Override
    @Transactional
    public boolean save(KnowledgeArticle et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(KnowledgeArticle et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<KnowledgeArticle> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<KnowledgeArticle> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<KnowledgeArticle> selectByParentarticlecontentid(String knowledgearticleid) {
        return baseMapper.selectByParentarticlecontentid(knowledgearticleid);
    }

    @Override
    public void removeByParentarticlecontentid(String knowledgearticleid) {
        this.remove(new QueryWrapper<KnowledgeArticle>().eq("parentarticlecontentid",knowledgearticleid));
    }

	@Override
    public List<KnowledgeArticle> selectByPreviousarticlecontentid(String knowledgearticleid) {
        return baseMapper.selectByPreviousarticlecontentid(knowledgearticleid);
    }

    @Override
    public void removeByPreviousarticlecontentid(String knowledgearticleid) {
        this.remove(new QueryWrapper<KnowledgeArticle>().eq("previousarticlecontentid",knowledgearticleid));
    }

	@Override
    public List<KnowledgeArticle> selectByRootarticleid(String knowledgearticleid) {
        return baseMapper.selectByRootarticleid(knowledgearticleid);
    }

    @Override
    public void removeByRootarticleid(String knowledgearticleid) {
        this.remove(new QueryWrapper<KnowledgeArticle>().eq("rootarticleid",knowledgearticleid));
    }

	@Override
    public List<KnowledgeArticle> selectByLanguagelocaleid(String languagelocaleid) {
        return baseMapper.selectByLanguagelocaleid(languagelocaleid);
    }

    @Override
    public void removeByLanguagelocaleid(String languagelocaleid) {
        this.remove(new QueryWrapper<KnowledgeArticle>().eq("languagelocaleid",languagelocaleid));
    }

	@Override
    public List<KnowledgeArticle> selectBySubjectid(String subjectid) {
        return baseMapper.selectBySubjectid(subjectid);
    }

    @Override
    public void removeBySubjectid(String subjectid) {
        this.remove(new QueryWrapper<KnowledgeArticle>().eq("subjectid",subjectid));
    }

	@Override
    public List<KnowledgeArticle> selectByTransactioncurrencyid(String transactioncurrencyid) {
        return baseMapper.selectByTransactioncurrencyid(transactioncurrencyid);
    }

    @Override
    public void removeByTransactioncurrencyid(String transactioncurrencyid) {
        this.remove(new QueryWrapper<KnowledgeArticle>().eq("transactioncurrencyid",transactioncurrencyid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<KnowledgeArticle> searchDefault(KnowledgeArticleSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<KnowledgeArticle> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<KnowledgeArticle>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(KnowledgeArticle et){
        //实体关系[DER1N_KNOWLEDGEARTICLE__KNOWLEDGEARTICLE__PARENTARTICLECONTENTID]
        if(!ObjectUtils.isEmpty(et.getParentarticlecontentid())){
            cn.ibizlab.businesscentral.core.base.domain.KnowledgeArticle parentarticlecontent=et.getParentarticlecontent();
            if(ObjectUtils.isEmpty(parentarticlecontent)){
                cn.ibizlab.businesscentral.core.base.domain.KnowledgeArticle majorEntity=knowledgearticleService.get(et.getParentarticlecontentid());
                et.setParentarticlecontent(majorEntity);
                parentarticlecontent=majorEntity;
            }
            et.setParentarticlecontentname(parentarticlecontent.getTitle());
        }
        //实体关系[DER1N_KNOWLEDGEARTICLE__KNOWLEDGEARTICLE__PREVIOUSARTICLECONTENTID]
        if(!ObjectUtils.isEmpty(et.getPreviousarticlecontentid())){
            cn.ibizlab.businesscentral.core.base.domain.KnowledgeArticle previousarticlecontent=et.getPreviousarticlecontent();
            if(ObjectUtils.isEmpty(previousarticlecontent)){
                cn.ibizlab.businesscentral.core.base.domain.KnowledgeArticle majorEntity=knowledgearticleService.get(et.getPreviousarticlecontentid());
                et.setPreviousarticlecontent(majorEntity);
                previousarticlecontent=majorEntity;
            }
            et.setPreviousarticlecontentname(previousarticlecontent.getTitle());
        }
        //实体关系[DER1N_KNOWLEDGEARTICLE__KNOWLEDGEARTICLE__ROOTARTICLEID]
        if(!ObjectUtils.isEmpty(et.getRootarticleid())){
            cn.ibizlab.businesscentral.core.base.domain.KnowledgeArticle rootarticle=et.getRootarticle();
            if(ObjectUtils.isEmpty(rootarticle)){
                cn.ibizlab.businesscentral.core.base.domain.KnowledgeArticle majorEntity=knowledgearticleService.get(et.getRootarticleid());
                et.setRootarticle(majorEntity);
                rootarticle=majorEntity;
            }
            et.setRootarticlename(rootarticle.getTitle());
        }
        //实体关系[DER1N_KNOWLEDGEARTICLE__LANGUAGELOCALE__LANGUAGELOCALEID]
        if(!ObjectUtils.isEmpty(et.getLanguagelocaleid())){
            cn.ibizlab.businesscentral.core.base.domain.LanguageLocale languagelocale=et.getLanguagelocale();
            if(ObjectUtils.isEmpty(languagelocale)){
                cn.ibizlab.businesscentral.core.base.domain.LanguageLocale majorEntity=languagelocaleService.get(et.getLanguagelocaleid());
                et.setLanguagelocale(majorEntity);
                languagelocale=majorEntity;
            }
            et.setLanguagelocalename(languagelocale.getLanguagelocalename());
        }
        //实体关系[DER1N_KNOWLEDGEARTICLE__SUBJECT__SUBJECTID]
        if(!ObjectUtils.isEmpty(et.getSubjectid())){
            cn.ibizlab.businesscentral.core.base.domain.Subject subject=et.getSubject();
            if(ObjectUtils.isEmpty(subject)){
                cn.ibizlab.businesscentral.core.base.domain.Subject majorEntity=subjectService.get(et.getSubjectid());
                et.setSubject(majorEntity);
                subject=majorEntity;
            }
            et.setSubjectname(subject.getTitle());
        }
        //实体关系[DER1N_KNOWLEDGEARTICLE__TRANSACTIONCURRENCY__TRANSACTIONCURRENCYID]
        if(!ObjectUtils.isEmpty(et.getTransactioncurrencyid())){
            cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency transactioncurrency=et.getTransactioncurrency();
            if(ObjectUtils.isEmpty(transactioncurrency)){
                cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency majorEntity=transactioncurrencyService.get(et.getTransactioncurrencyid());
                et.setTransactioncurrency(majorEntity);
                transactioncurrency=majorEntity;
            }
            et.setCurrencyname(transactioncurrency.getCurrencyname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<KnowledgeArticle> getKnowledgearticleByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<KnowledgeArticle> getKnowledgearticleByEntities(List<KnowledgeArticle> entities) {
        List ids =new ArrayList();
        for(KnowledgeArticle entity : entities){
            Serializable id=entity.getKnowledgearticleid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}



