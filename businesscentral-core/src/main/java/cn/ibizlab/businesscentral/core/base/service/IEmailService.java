package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.Email;
import cn.ibizlab.businesscentral.core.base.filter.EmailSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Email] 服务对象接口
 */
public interface IEmailService extends IService<Email>{

    boolean create(Email et) ;
    void createBatch(List<Email> list) ;
    boolean update(Email et) ;
    void updateBatch(List<Email> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    Email get(String key) ;
    Email getDraft(Email et) ;
    boolean checkKey(Email et) ;
    boolean save(Email et) ;
    void saveBatch(List<Email> list) ;
    Page<Email> searchDefault(EmailSearchContext context) ;
    List<Email> selectByParentactivityid(String activityid) ;
    void removeByParentactivityid(String activityid) ;
    List<Email> selectByServiceid(String serviceid) ;
    void removeByServiceid(String serviceid) ;
    List<Email> selectBySlaid(String slaid) ;
    void removeBySlaid(String slaid) ;
    List<Email> selectByTemplateid(String templateid) ;
    void removeByTemplateid(String templateid) ;
    List<Email> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Email> getEmailByIds(List<String> ids) ;
    List<Email> getEmailByEntities(List<Email> entities) ;
}


