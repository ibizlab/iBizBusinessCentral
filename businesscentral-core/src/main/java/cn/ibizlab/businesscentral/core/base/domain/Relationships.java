package cn.ibizlab.businesscentral.core.base.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[关系数据对象]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "RELATIONSHIPS",resultMap = "RelationshipsResultMap")
public class Relationships extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 关系类型
     */
    @TableField(value = "relationshipstype")
    @JSONField(name = "relationshipstype")
    @JsonProperty("relationshipstype")
    private String relationshipstype;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 存储对象2名称
     */
    @TableField(value = "entity2name")
    @JSONField(name = "entity2name")
    @JsonProperty("entity2name")
    private String entity2name;
    /**
     * 关系名称
     */
    @TableField(value = "relationshipsname")
    @JSONField(name = "relationshipsname")
    @JsonProperty("relationshipsname")
    private String relationshipsname;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 存储对象标识
     */
    @TableField(value = "entityid")
    @JSONField(name = "entityid")
    @JsonProperty("entityid")
    private String entityid;
    /**
     * 存储对象名称
     */
    @TableField(value = "entityname")
    @JSONField(name = "entityname")
    @JsonProperty("entityname")
    private String entityname;
    /**
     * 关系标识
     */
    @DEField(isKeyField=true)
    @TableId(value= "relationshipsid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "relationshipsid")
    @JsonProperty("relationshipsid")
    private String relationshipsid;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 存储对象2标识
     */
    @TableField(value = "entity2id")
    @JSONField(name = "entity2id")
    @JsonProperty("entity2id")
    private String entity2id;



    /**
     * 设置 [关系类型]
     */
    public void setRelationshipstype(String relationshipstype){
        this.relationshipstype = relationshipstype ;
        this.modify("relationshipstype",relationshipstype);
    }

    /**
     * 设置 [存储对象2名称]
     */
    public void setEntity2name(String entity2name){
        this.entity2name = entity2name ;
        this.modify("entity2name",entity2name);
    }

    /**
     * 设置 [关系名称]
     */
    public void setRelationshipsname(String relationshipsname){
        this.relationshipsname = relationshipsname ;
        this.modify("relationshipsname",relationshipsname);
    }

    /**
     * 设置 [存储对象标识]
     */
    public void setEntityid(String entityid){
        this.entityid = entityid ;
        this.modify("entityid",entityid);
    }

    /**
     * 设置 [存储对象名称]
     */
    public void setEntityname(String entityname){
        this.entityname = entityname ;
        this.modify("entityname",entityname);
    }

    /**
     * 设置 [存储对象2标识]
     */
    public void setEntity2id(String entity2id){
        this.entity2id = entity2id ;
        this.modify("entity2id",entity2id);
    }


}


