package cn.ibizlab.businesscentral.core.product.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.product.domain.ProductAssociation;
/**
 * 关系型数据实体[ProductAssociation] 查询条件对象
 */
@Slf4j
@Data
public class ProductAssociationSearchContext extends QueryWrapperContext<ProductAssociation> {

	private String n_propertycustomizationstatus_eq;//[属性自定义]
	public void setN_propertycustomizationstatus_eq(String n_propertycustomizationstatus_eq) {
        this.n_propertycustomizationstatus_eq = n_propertycustomizationstatus_eq;
        if(!ObjectUtils.isEmpty(this.n_propertycustomizationstatus_eq)){
            this.getSearchCond().eq("propertycustomizationstatus", n_propertycustomizationstatus_eq);
        }
    }
	private String n_productisrequired_eq;//[必填]
	public void setN_productisrequired_eq(String n_productisrequired_eq) {
        this.n_productisrequired_eq = n_productisrequired_eq;
        if(!ObjectUtils.isEmpty(this.n_productisrequired_eq)){
            this.getSearchCond().eq("productisrequired", n_productisrequired_eq);
        }
    }
	private Integer n_statuscode_eq;//[状态描述]
	public void setN_statuscode_eq(Integer n_statuscode_eq) {
        this.n_statuscode_eq = n_statuscode_eq;
        if(!ObjectUtils.isEmpty(this.n_statuscode_eq)){
            this.getSearchCond().eq("statuscode", n_statuscode_eq);
        }
    }
	private Integer n_statecode_eq;//[状态]
	public void setN_statecode_eq(Integer n_statecode_eq) {
        this.n_statecode_eq = n_statecode_eq;
        if(!ObjectUtils.isEmpty(this.n_statecode_eq)){
            this.getSearchCond().eq("statecode", n_statecode_eq);
        }
    }
	private String n_uomname_eq;//[计量单位]
	public void setN_uomname_eq(String n_uomname_eq) {
        this.n_uomname_eq = n_uomname_eq;
        if(!ObjectUtils.isEmpty(this.n_uomname_eq)){
            this.getSearchCond().eq("uomname", n_uomname_eq);
        }
    }
	private String n_uomname_like;//[计量单位]
	public void setN_uomname_like(String n_uomname_like) {
        this.n_uomname_like = n_uomname_like;
        if(!ObjectUtils.isEmpty(this.n_uomname_like)){
            this.getSearchCond().like("uomname", n_uomname_like);
        }
    }
	private String n_associatedproductname_eq;//[关联的产品]
	public void setN_associatedproductname_eq(String n_associatedproductname_eq) {
        this.n_associatedproductname_eq = n_associatedproductname_eq;
        if(!ObjectUtils.isEmpty(this.n_associatedproductname_eq)){
            this.getSearchCond().eq("associatedproductname", n_associatedproductname_eq);
        }
    }
	private String n_associatedproductname_like;//[关联的产品]
	public void setN_associatedproductname_like(String n_associatedproductname_like) {
        this.n_associatedproductname_like = n_associatedproductname_like;
        if(!ObjectUtils.isEmpty(this.n_associatedproductname_like)){
            this.getSearchCond().like("associatedproductname", n_associatedproductname_like);
        }
    }
	private String n_currencyname_eq;//[货币名称]
	public void setN_currencyname_eq(String n_currencyname_eq) {
        this.n_currencyname_eq = n_currencyname_eq;
        if(!ObjectUtils.isEmpty(this.n_currencyname_eq)){
            this.getSearchCond().eq("currencyname", n_currencyname_eq);
        }
    }
	private String n_currencyname_like;//[货币名称]
	public void setN_currencyname_like(String n_currencyname_like) {
        this.n_currencyname_like = n_currencyname_like;
        if(!ObjectUtils.isEmpty(this.n_currencyname_like)){
            this.getSearchCond().like("currencyname", n_currencyname_like);
        }
    }
	private String n_productname_eq;//[产品名称]
	public void setN_productname_eq(String n_productname_eq) {
        this.n_productname_eq = n_productname_eq;
        if(!ObjectUtils.isEmpty(this.n_productname_eq)){
            this.getSearchCond().eq("productname", n_productname_eq);
        }
    }
	private String n_productname_like;//[产品名称]
	public void setN_productname_like(String n_productname_like) {
        this.n_productname_like = n_productname_like;
        if(!ObjectUtils.isEmpty(this.n_productname_like)){
            this.getSearchCond().like("productname", n_productname_like);
        }
    }
	private String n_productid_eq;//[产品]
	public void setN_productid_eq(String n_productid_eq) {
        this.n_productid_eq = n_productid_eq;
        if(!ObjectUtils.isEmpty(this.n_productid_eq)){
            this.getSearchCond().eq("productid", n_productid_eq);
        }
    }
	private String n_uomid_eq;//[计价单位]
	public void setN_uomid_eq(String n_uomid_eq) {
        this.n_uomid_eq = n_uomid_eq;
        if(!ObjectUtils.isEmpty(this.n_uomid_eq)){
            this.getSearchCond().eq("uomid", n_uomid_eq);
        }
    }
	private String n_transactioncurrencyid_eq;//[货币]
	public void setN_transactioncurrencyid_eq(String n_transactioncurrencyid_eq) {
        this.n_transactioncurrencyid_eq = n_transactioncurrencyid_eq;
        if(!ObjectUtils.isEmpty(this.n_transactioncurrencyid_eq)){
            this.getSearchCond().eq("transactioncurrencyid", n_transactioncurrencyid_eq);
        }
    }
	private String n_associatedproduct_eq;//[关联的产品]
	public void setN_associatedproduct_eq(String n_associatedproduct_eq) {
        this.n_associatedproduct_eq = n_associatedproduct_eq;
        if(!ObjectUtils.isEmpty(this.n_associatedproduct_eq)){
            this.getSearchCond().eq("associatedproduct", n_associatedproduct_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("productname", query)   
            );
		 }
	}
}



