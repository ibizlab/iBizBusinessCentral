package cn.ibizlab.businesscentral.core.scheduling.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.scheduling.domain.BookableResourceGroup;
import cn.ibizlab.businesscentral.core.scheduling.filter.BookableResourceGroupSearchContext;
import cn.ibizlab.businesscentral.core.scheduling.service.IBookableResourceGroupService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.scheduling.mapper.BookableResourceGroupMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[可预订资源组] 服务对象接口实现
 */
@Slf4j
@Service("BookableResourceGroupServiceImpl")
public class BookableResourceGroupServiceImpl extends ServiceImpl<BookableResourceGroupMapper, BookableResourceGroup> implements IBookableResourceGroupService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.scheduling.service.IBookableResourceService bookableresourceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITransactionCurrencyService transactioncurrencyService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(BookableResourceGroup et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getBookableresourcegroupid()),et);
        return true;
    }

    @Override
    public void createBatch(List<BookableResourceGroup> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(BookableResourceGroup et) {
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("bookableresgroupid",et.getBookableresourcegroupid())))
            return false;
        CachedBeanCopier.copy(get(et.getBookableresourcegroupid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<BookableResourceGroup> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public BookableResourceGroup get(String key) {
        BookableResourceGroup et = getById(key);
        if(et==null){
            et=new BookableResourceGroup();
            et.setBookableresourcegroupid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public BookableResourceGroup getDraft(BookableResourceGroup et) {
        return et;
    }

    @Override
    public boolean checkKey(BookableResourceGroup et) {
        return (!ObjectUtils.isEmpty(et.getBookableresourcegroupid()))&&(!Objects.isNull(this.getById(et.getBookableresourcegroupid())));
    }
    @Override
    @Transactional
    public boolean save(BookableResourceGroup et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(BookableResourceGroup et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<BookableResourceGroup> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<BookableResourceGroup> list) {
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<BookableResourceGroup> selectByChildresource(String bookableresourceid) {
        return baseMapper.selectByChildresource(bookableresourceid);
    }

    @Override
    public void removeByChildresource(String bookableresourceid) {
        this.remove(new QueryWrapper<BookableResourceGroup>().eq("childresource",bookableresourceid));
    }

	@Override
    public List<BookableResourceGroup> selectByParentresource(String bookableresourceid) {
        return baseMapper.selectByParentresource(bookableresourceid);
    }

    @Override
    public void removeByParentresource(String bookableresourceid) {
        this.remove(new QueryWrapper<BookableResourceGroup>().eq("parentresource",bookableresourceid));
    }

	@Override
    public List<BookableResourceGroup> selectByTransactioncurrencyid(String transactioncurrencyid) {
        return baseMapper.selectByTransactioncurrencyid(transactioncurrencyid);
    }

    @Override
    public void removeByTransactioncurrencyid(String transactioncurrencyid) {
        this.remove(new QueryWrapper<BookableResourceGroup>().eq("transactioncurrencyid",transactioncurrencyid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<BookableResourceGroup> searchDefault(BookableResourceGroupSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<BookableResourceGroup> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<BookableResourceGroup>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<BookableResourceGroup> getBookableresourcegroupByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<BookableResourceGroup> getBookableresourcegroupByEntities(List<BookableResourceGroup> entities) {
        List ids =new ArrayList();
        for(BookableResourceGroup entity : entities){
            Serializable id=entity.getBookableresourcegroupid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}



