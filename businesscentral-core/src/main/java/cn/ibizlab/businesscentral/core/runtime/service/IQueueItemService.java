package cn.ibizlab.businesscentral.core.runtime.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.runtime.domain.QueueItem;
import cn.ibizlab.businesscentral.core.runtime.filter.QueueItemSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[QueueItem] 服务对象接口
 */
public interface IQueueItemService extends IService<QueueItem>{

    boolean create(QueueItem et) ;
    void createBatch(List<QueueItem> list) ;
    boolean update(QueueItem et) ;
    void updateBatch(List<QueueItem> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    QueueItem get(String key) ;
    QueueItem getDraft(QueueItem et) ;
    boolean checkKey(QueueItem et) ;
    boolean save(QueueItem et) ;
    void saveBatch(List<QueueItem> list) ;
    Page<QueueItem> searchDefault(QueueItemSearchContext context) ;
    List<QueueItem> selectByQueueid(String queueid) ;
    void removeByQueueid(String queueid) ;
    List<QueueItem> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<QueueItem> getQueueitemByIds(List<String> ids) ;
    List<QueueItem> getQueueitemByEntities(List<QueueItem> entities) ;
}


