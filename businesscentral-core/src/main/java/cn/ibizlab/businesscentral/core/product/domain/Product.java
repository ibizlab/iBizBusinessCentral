package cn.ibizlab.businesscentral.core.product.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[产品]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "PRODUCT",resultMap = "ProductResultMap")
public class Product extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 供应商 ID
     */
    @TableField(value = "vendorid")
    @JSONField(name = "vendorid")
    @JsonProperty("vendorid")
    private String vendorid;
    /**
     * 产品结构
     */
    @TableField(value = "productstructure")
    @JSONField(name = "productstructure")
    @JsonProperty("productstructure")
    private String productstructure;
    /**
     * Version Number
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * Stage Id
     */
    @TableField(value = "stageid")
    @JSONField(name = "stageid")
    @JsonProperty("stageid")
    private String stageid;
    /**
     * 状态描述
     */
    @TableField(value = "statuscode")
    @JSONField(name = "statuscode")
    @JsonProperty("statuscode")
    private Integer statuscode;
    /**
     * 实体图像
     */
    @TableField(value = "entityimage")
    @JSONField(name = "entityimage")
    @JsonProperty("entityimage")
    private String entityimage;
    /**
     * Record Created On
     */
    @TableField(value = "overriddencreatedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;
    /**
     * 库存项
     */
    @DEField(defaultValue = "0")
    @TableField(value = "stockitem")
    @JSONField(name = "stockitem")
    @JsonProperty("stockitem")
    private Integer stockitem;
    /**
     * Import Sequence Number
     */
    @TableField(value = "importsequencenumber")
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;
    /**
     * 有效期的开始日期
     */
    @TableField(value = "validfromdate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "validfromdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("validfromdate")
    private Timestamp validfromdate;
    /**
     * 提供商名称
     */
    @TableField(value = "suppliername")
    @JSONField(name = "suppliername")
    @JsonProperty("suppliername")
    private String suppliername;
    /**
     * 产品类型
     */
    @TableField(value = "producttypecode")
    @JSONField(name = "producttypecode")
    @JsonProperty("producttypecode")
    private String producttypecode;
    /**
     * EntityImageId
     */
    @TableField(value = "entityimageid")
    @JSONField(name = "entityimageid")
    @JsonProperty("entityimageid")
    private String entityimageid;
    /**
     * EntityImage_Timestamp
     */
    @DEField(name = "entityimage_timestamp")
    @TableField(value = "entityimage_timestamp")
    @JSONField(name = "entityimage_timestamp")
    @JsonProperty("entityimage_timestamp")
    private BigInteger entityimageTimestamp;
    /**
     * URL
     */
    @TableField(value = "producturl")
    @JSONField(name = "producturl")
    @JsonProperty("producturl")
    private String producturl;
    /**
     * 标准成本
     */
    @TableField(value = "standardcost")
    @JSONField(name = "standardcost")
    @JsonProperty("standardcost")
    private BigDecimal standardcost;
    /**
     * 现存数量
     */
    @TableField(value = "quantityonhand")
    @JSONField(name = "quantityonhand")
    @JsonProperty("quantityonhand")
    private BigDecimal quantityonhand;
    /**
     * 当前成本
     */
    @TableField(value = "currentcost")
    @JSONField(name = "currentcost")
    @JsonProperty("currentcost")
    private BigDecimal currentcost;
    /**
     * 汇率
     */
    @TableField(value = "exchangerate")
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;
    /**
     * 产品
     */
    @DEField(isKeyField=true)
    @TableId(value= "productid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "productid")
    @JsonProperty("productid")
    private String productid;
    /**
     * 说明
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 供应商
     */
    @TableField(value = "vendorname")
    @JSONField(name = "vendorname")
    @JsonProperty("vendorname")
    private String vendorname;
    /**
     * 仅供内部使用
     */
    @TableField(value = "dmtimportstate")
    @JSONField(name = "dmtimportstate")
    @JsonProperty("dmtimportstate")
    private Integer dmtimportstate;
    /**
     * EntityImage_URL
     */
    @DEField(name = "entityimage_url")
    @TableField(value = "entityimage_url")
    @JSONField(name = "entityimage_url")
    @JsonProperty("entityimage_url")
    private String entityimageUrl;
    /**
     * 库存容量
     */
    @TableField(value = "stockvolume")
    @JSONField(name = "stockvolume")
    @JsonProperty("stockvolume")
    private BigDecimal stockvolume;
    /**
     * 支持小数
     */
    @TableField(value = "quantitydecimal")
    @JSONField(name = "quantitydecimal")
    @JsonProperty("quantitydecimal")
    private Integer quantitydecimal;
    /**
     * 供应商名称
     */
    @TableField(value = "vendorpartnumber")
    @JSONField(name = "vendorpartnumber")
    @JsonProperty("vendorpartnumber")
    private String vendorpartnumber;
    /**
     * 有效期的结束日期
     */
    @TableField(value = "validtodate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "validtodate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("validtodate")
    private Timestamp validtodate;
    /**
     * Reparented
     */
    @DEField(defaultValue = "0")
    @TableField(value = "reparented")
    @JSONField(name = "reparented")
    @JsonProperty("reparented")
    private Integer reparented;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * Process Id
     */
    @TableField(value = "processid")
    @JSONField(name = "processid")
    @JsonProperty("processid")
    private String processid;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 产品 ID
     */
    @TableField(value = "productnumber")
    @JSONField(name = "productnumber")
    @JsonProperty("productnumber")
    private String productnumber;
    /**
     * 为配套件
     */
    @DEField(defaultValue = "0")
    @TableField(value = "kit")
    @JSONField(name = "kit")
    @JsonProperty("kit")
    private Integer kit;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 当前成本 (Base)
     */
    @DEField(name = "currentcost_base")
    @TableField(value = "currentcost_base")
    @JSONField(name = "currentcost_base")
    @JsonProperty("currentcost_base")
    private BigDecimal currentcostBase;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 库存重量
     */
    @TableField(value = "stockweight")
    @JSONField(name = "stockweight")
    @JsonProperty("stockweight")
    private BigDecimal stockweight;
    /**
     * 大小
     */
    @TableField(value = "size")
    @JSONField(name = "size")
    @JsonProperty("size")
    private String size;
    /**
     * Time Zone Rule Version Number
     */
    @TableField(value = "timezoneruleversionnumber")
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;
    /**
     * UTC Conversion Time Zone Code
     */
    @TableField(value = "utcconversiontimezonecode")
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;
    /**
     * 产品名称
     */
    @TableField(value = "productname")
    @JSONField(name = "productname")
    @JsonProperty("productname")
    private String productname;
    /**
     * 定价 (Base)
     */
    @DEField(name = "price_base")
    @TableField(value = "price_base")
    @JSONField(name = "price_base")
    @JsonProperty("price_base")
    private BigDecimal priceBase;
    /**
     * 定价
     */
    @TableField(value = "price")
    @JSONField(name = "price")
    @JsonProperty("price")
    private BigDecimal price;
    /**
     * Traversed Path
     */
    @TableField(value = "traversedpath")
    @JSONField(name = "traversedpath")
    @JsonProperty("traversedpath")
    private String traversedpath;
    /**
     * 层次结构路径
     */
    @TableField(value = "hierarchypath")
    @JSONField(name = "hierarchypath")
    @JsonProperty("hierarchypath")
    private String hierarchypath;
    /**
     * 标准成本 (Base)
     */
    @DEField(name = "standardcost_base")
    @TableField(value = "standardcost_base")
    @JSONField(name = "standardcost_base")
    @JsonProperty("standardcost_base")
    private BigDecimal standardcostBase;
    /**
     * 状态
     */
    @TableField(value = "statecode")
    @JSONField(name = "statecode")
    @JsonProperty("statecode")
    private Integer statecode;
    /**
     * 计价单位组
     */
    @TableField(value = "defaultuomschedulename")
    @JSONField(name = "defaultuomschedulename")
    @JsonProperty("defaultuomschedulename")
    private String defaultuomschedulename;
    /**
     * 主题
     */
    @TableField(value = "subjectname")
    @JSONField(name = "subjectname")
    @JsonProperty("subjectname")
    private String subjectname;
    /**
     * 货币
     */
    @TableField(value = "currencyname")
    @JSONField(name = "currencyname")
    @JsonProperty("currencyname")
    private String currencyname;
    /**
     * 默认价目表
     */
    @TableField(value = "pricelevelname")
    @JSONField(name = "pricelevelname")
    @JsonProperty("pricelevelname")
    private String pricelevelname;
    /**
     * 默认计价单位
     */
    @TableField(value = "defaultuomname")
    @JSONField(name = "defaultuomname")
    @JsonProperty("defaultuomname")
    private String defaultuomname;
    /**
     * 父级
     */
    @TableField(value = "parentproductname")
    @JSONField(name = "parentproductname")
    @JsonProperty("parentproductname")
    private String parentproductname;
    /**
     * 父级
     */
    @TableField(value = "parentproductid")
    @JSONField(name = "parentproductid")
    @JsonProperty("parentproductid")
    private String parentproductid;
    /**
     * 货币
     */
    @TableField(value = "transactioncurrencyid")
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;
    /**
     * 默认价目表
     */
    @TableField(value = "pricelevelid")
    @JSONField(name = "pricelevelid")
    @JsonProperty("pricelevelid")
    private String pricelevelid;
    /**
     * 默认计价单位
     */
    @TableField(value = "defaultuomid")
    @JSONField(name = "defaultuomid")
    @JsonProperty("defaultuomid")
    private String defaultuomid;
    /**
     * 计价单位组
     */
    @TableField(value = "defaultuomscheduleid")
    @JSONField(name = "defaultuomscheduleid")
    @JsonProperty("defaultuomscheduleid")
    private String defaultuomscheduleid;
    /**
     * 主题
     */
    @TableField(value = "subjectid")
    @JSONField(name = "subjectid")
    @JsonProperty("subjectid")
    private String subjectid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.product.domain.PriceLevel pricelevel;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.product.domain.Product parentproduct;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.Subject subject;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency transactioncurrency;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.UomSchedule defaultuomschedule;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.Uom defaultuom;



    /**
     * 设置 [供应商 ID]
     */
    public void setVendorid(String vendorid){
        this.vendorid = vendorid ;
        this.modify("vendorid",vendorid);
    }

    /**
     * 设置 [产品结构]
     */
    public void setProductstructure(String productstructure){
        this.productstructure = productstructure ;
        this.modify("productstructure",productstructure);
    }

    /**
     * 设置 [Version Number]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [Stage Id]
     */
    public void setStageid(String stageid){
        this.stageid = stageid ;
        this.modify("stageid",stageid);
    }

    /**
     * 设置 [状态描述]
     */
    public void setStatuscode(Integer statuscode){
        this.statuscode = statuscode ;
        this.modify("statuscode",statuscode);
    }

    /**
     * 设置 [实体图像]
     */
    public void setEntityimage(String entityimage){
        this.entityimage = entityimage ;
        this.modify("entityimage",entityimage);
    }

    /**
     * 设置 [Record Created On]
     */
    public void setOverriddencreatedon(Timestamp overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 格式化日期 [Record Created On]
     */
    public String formatOverriddencreatedon(){
        if (this.overriddencreatedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(overriddencreatedon);
    }
    /**
     * 设置 [库存项]
     */
    public void setStockitem(Integer stockitem){
        this.stockitem = stockitem ;
        this.modify("stockitem",stockitem);
    }

    /**
     * 设置 [Import Sequence Number]
     */
    public void setImportsequencenumber(Integer importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [有效期的开始日期]
     */
    public void setValidfromdate(Timestamp validfromdate){
        this.validfromdate = validfromdate ;
        this.modify("validfromdate",validfromdate);
    }

    /**
     * 格式化日期 [有效期的开始日期]
     */
    public String formatValidfromdate(){
        if (this.validfromdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(validfromdate);
    }
    /**
     * 设置 [提供商名称]
     */
    public void setSuppliername(String suppliername){
        this.suppliername = suppliername ;
        this.modify("suppliername",suppliername);
    }

    /**
     * 设置 [产品类型]
     */
    public void setProducttypecode(String producttypecode){
        this.producttypecode = producttypecode ;
        this.modify("producttypecode",producttypecode);
    }

    /**
     * 设置 [EntityImageId]
     */
    public void setEntityimageid(String entityimageid){
        this.entityimageid = entityimageid ;
        this.modify("entityimageid",entityimageid);
    }

    /**
     * 设置 [EntityImage_Timestamp]
     */
    public void setEntityimageTimestamp(BigInteger entityimageTimestamp){
        this.entityimageTimestamp = entityimageTimestamp ;
        this.modify("entityimage_timestamp",entityimageTimestamp);
    }

    /**
     * 设置 [URL]
     */
    public void setProducturl(String producturl){
        this.producturl = producturl ;
        this.modify("producturl",producturl);
    }

    /**
     * 设置 [标准成本]
     */
    public void setStandardcost(BigDecimal standardcost){
        this.standardcost = standardcost ;
        this.modify("standardcost",standardcost);
    }

    /**
     * 设置 [现存数量]
     */
    public void setQuantityonhand(BigDecimal quantityonhand){
        this.quantityonhand = quantityonhand ;
        this.modify("quantityonhand",quantityonhand);
    }

    /**
     * 设置 [当前成本]
     */
    public void setCurrentcost(BigDecimal currentcost){
        this.currentcost = currentcost ;
        this.modify("currentcost",currentcost);
    }

    /**
     * 设置 [汇率]
     */
    public void setExchangerate(BigDecimal exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [供应商]
     */
    public void setVendorname(String vendorname){
        this.vendorname = vendorname ;
        this.modify("vendorname",vendorname);
    }

    /**
     * 设置 [仅供内部使用]
     */
    public void setDmtimportstate(Integer dmtimportstate){
        this.dmtimportstate = dmtimportstate ;
        this.modify("dmtimportstate",dmtimportstate);
    }

    /**
     * 设置 [EntityImage_URL]
     */
    public void setEntityimageUrl(String entityimageUrl){
        this.entityimageUrl = entityimageUrl ;
        this.modify("entityimage_url",entityimageUrl);
    }

    /**
     * 设置 [库存容量]
     */
    public void setStockvolume(BigDecimal stockvolume){
        this.stockvolume = stockvolume ;
        this.modify("stockvolume",stockvolume);
    }

    /**
     * 设置 [支持小数]
     */
    public void setQuantitydecimal(Integer quantitydecimal){
        this.quantitydecimal = quantitydecimal ;
        this.modify("quantitydecimal",quantitydecimal);
    }

    /**
     * 设置 [供应商名称]
     */
    public void setVendorpartnumber(String vendorpartnumber){
        this.vendorpartnumber = vendorpartnumber ;
        this.modify("vendorpartnumber",vendorpartnumber);
    }

    /**
     * 设置 [有效期的结束日期]
     */
    public void setValidtodate(Timestamp validtodate){
        this.validtodate = validtodate ;
        this.modify("validtodate",validtodate);
    }

    /**
     * 格式化日期 [有效期的结束日期]
     */
    public String formatValidtodate(){
        if (this.validtodate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(validtodate);
    }
    /**
     * 设置 [Reparented]
     */
    public void setReparented(Integer reparented){
        this.reparented = reparented ;
        this.modify("reparented",reparented);
    }

    /**
     * 设置 [Process Id]
     */
    public void setProcessid(String processid){
        this.processid = processid ;
        this.modify("processid",processid);
    }

    /**
     * 设置 [产品 ID]
     */
    public void setProductnumber(String productnumber){
        this.productnumber = productnumber ;
        this.modify("productnumber",productnumber);
    }

    /**
     * 设置 [为配套件]
     */
    public void setKit(Integer kit){
        this.kit = kit ;
        this.modify("kit",kit);
    }

    /**
     * 设置 [当前成本 (Base)]
     */
    public void setCurrentcostBase(BigDecimal currentcostBase){
        this.currentcostBase = currentcostBase ;
        this.modify("currentcost_base",currentcostBase);
    }

    /**
     * 设置 [库存重量]
     */
    public void setStockweight(BigDecimal stockweight){
        this.stockweight = stockweight ;
        this.modify("stockweight",stockweight);
    }

    /**
     * 设置 [大小]
     */
    public void setSize(String size){
        this.size = size ;
        this.modify("size",size);
    }

    /**
     * 设置 [Time Zone Rule Version Number]
     */
    public void setTimezoneruleversionnumber(Integer timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [UTC Conversion Time Zone Code]
     */
    public void setUtcconversiontimezonecode(Integer utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [产品名称]
     */
    public void setProductname(String productname){
        this.productname = productname ;
        this.modify("productname",productname);
    }

    /**
     * 设置 [定价 (Base)]
     */
    public void setPriceBase(BigDecimal priceBase){
        this.priceBase = priceBase ;
        this.modify("price_base",priceBase);
    }

    /**
     * 设置 [定价]
     */
    public void setPrice(BigDecimal price){
        this.price = price ;
        this.modify("price",price);
    }

    /**
     * 设置 [Traversed Path]
     */
    public void setTraversedpath(String traversedpath){
        this.traversedpath = traversedpath ;
        this.modify("traversedpath",traversedpath);
    }

    /**
     * 设置 [层次结构路径]
     */
    public void setHierarchypath(String hierarchypath){
        this.hierarchypath = hierarchypath ;
        this.modify("hierarchypath",hierarchypath);
    }

    /**
     * 设置 [标准成本 (Base)]
     */
    public void setStandardcostBase(BigDecimal standardcostBase){
        this.standardcostBase = standardcostBase ;
        this.modify("standardcost_base",standardcostBase);
    }

    /**
     * 设置 [状态]
     */
    public void setStatecode(Integer statecode){
        this.statecode = statecode ;
        this.modify("statecode",statecode);
    }

    /**
     * 设置 [计价单位组]
     */
    public void setDefaultuomschedulename(String defaultuomschedulename){
        this.defaultuomschedulename = defaultuomschedulename ;
        this.modify("defaultuomschedulename",defaultuomschedulename);
    }

    /**
     * 设置 [主题]
     */
    public void setSubjectname(String subjectname){
        this.subjectname = subjectname ;
        this.modify("subjectname",subjectname);
    }

    /**
     * 设置 [货币]
     */
    public void setCurrencyname(String currencyname){
        this.currencyname = currencyname ;
        this.modify("currencyname",currencyname);
    }

    /**
     * 设置 [默认价目表]
     */
    public void setPricelevelname(String pricelevelname){
        this.pricelevelname = pricelevelname ;
        this.modify("pricelevelname",pricelevelname);
    }

    /**
     * 设置 [默认计价单位]
     */
    public void setDefaultuomname(String defaultuomname){
        this.defaultuomname = defaultuomname ;
        this.modify("defaultuomname",defaultuomname);
    }

    /**
     * 设置 [父级]
     */
    public void setParentproductname(String parentproductname){
        this.parentproductname = parentproductname ;
        this.modify("parentproductname",parentproductname);
    }

    /**
     * 设置 [父级]
     */
    public void setParentproductid(String parentproductid){
        this.parentproductid = parentproductid ;
        this.modify("parentproductid",parentproductid);
    }

    /**
     * 设置 [货币]
     */
    public void setTransactioncurrencyid(String transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }

    /**
     * 设置 [默认价目表]
     */
    public void setPricelevelid(String pricelevelid){
        this.pricelevelid = pricelevelid ;
        this.modify("pricelevelid",pricelevelid);
    }

    /**
     * 设置 [默认计价单位]
     */
    public void setDefaultuomid(String defaultuomid){
        this.defaultuomid = defaultuomid ;
        this.modify("defaultuomid",defaultuomid);
    }

    /**
     * 设置 [计价单位组]
     */
    public void setDefaultuomscheduleid(String defaultuomscheduleid){
        this.defaultuomscheduleid = defaultuomscheduleid ;
        this.modify("defaultuomscheduleid",defaultuomscheduleid);
    }

    /**
     * 设置 [主题]
     */
    public void setSubjectid(String subjectid){
        this.subjectid = subjectid ;
        this.modify("subjectid",subjectid);
    }


}


