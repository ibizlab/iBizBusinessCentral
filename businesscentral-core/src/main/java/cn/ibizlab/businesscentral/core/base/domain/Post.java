package cn.ibizlab.businesscentral.core.base.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[公告]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "POST",resultMap = "PostResultMap")
public class Post extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 类型
     */
    @TableField(value = "type")
    @JSONField(name = "type")
    @JsonProperty("type")
    private String type;
    /**
     * 负责人
     */
    @TableField(value = "regardingobjectownerid")
    @JSONField(name = "regardingobjectownerid")
    @JsonProperty("regardingobjectownerid")
    private String regardingobjectownerid;
    /**
     * 时区规则版本号
     */
    @TableField(value = "timezoneruleversionnumber")
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;
    /**
     * 关于
     */
    @TableField(value = "regardingobjectidname")
    @JSONField(name = "regardingobjectidname")
    @JsonProperty("regardingobjectidname")
    private String regardingobjectidname;
    /**
     * RegardingObjectOwnerType
     */
    @TableField(value = "regardingobjectownertype")
    @JSONField(name = "regardingobjectownertype")
    @JsonProperty("regardingobjectownertype")
    private String regardingobjectownertype;
    /**
     * 公告
     */
    @DEField(isKeyField=true)
    @TableId(value= "postid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "postid")
    @JsonProperty("postid")
    private String postid;
    /**
     * 公布到 Yammer
     */
    @DEField(defaultValue = "0")
    @TableField(value = "posttoyammer")
    @JSONField(name = "posttoyammer")
    @JsonProperty("posttoyammer")
    private Integer posttoyammer;
    /**
     * RegardingObjectTypeCode
     */
    @TableField(value = "regardingobjecttypecode")
    @JSONField(name = "regardingobjecttypecode")
    @JsonProperty("regardingobjecttypecode")
    private String regardingobjecttypecode;
    /**
     * 关于
     */
    @TableField(value = "regardingobjectid")
    @JSONField(name = "regardingobjectid")
    @JsonProperty("regardingobjectid")
    private String regardingobjectid;
    /**
     * 关于
     */
    @TableField(value = "regardingobjectname")
    @JSONField(name = "regardingobjectname")
    @JsonProperty("regardingobjectname")
    private String regardingobjectname;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 文本
     */
    @TableField(value = "text")
    @JSONField(name = "text")
    @JsonProperty("text")
    private String text;
    /**
     * Yammer 重试计数
     */
    @TableField(value = "yammerretrycount")
    @JSONField(name = "yammerretrycount")
    @JsonProperty("yammerretrycount")
    private Integer yammerretrycount;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * UTC 转换时区代码
     */
    @TableField(value = "utcconversiontimezonecode")
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;
    /**
     * Yammer 公布状态
     */
    @TableField(value = "yammerpoststate")
    @JSONField(name = "yammerpoststate")
    @JsonProperty("yammerpoststate")
    private Integer yammerpoststate;
    /**
     * 来源
     */
    @TableField(value = "source")
    @JSONField(name = "source")
    @JsonProperty("source")
    private String source;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;



    /**
     * 设置 [类型]
     */
    public void setType(String type){
        this.type = type ;
        this.modify("type",type);
    }

    /**
     * 设置 [负责人]
     */
    public void setRegardingobjectownerid(String regardingobjectownerid){
        this.regardingobjectownerid = regardingobjectownerid ;
        this.modify("regardingobjectownerid",regardingobjectownerid);
    }

    /**
     * 设置 [时区规则版本号]
     */
    public void setTimezoneruleversionnumber(Integer timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [关于]
     */
    public void setRegardingobjectidname(String regardingobjectidname){
        this.regardingobjectidname = regardingobjectidname ;
        this.modify("regardingobjectidname",regardingobjectidname);
    }

    /**
     * 设置 [RegardingObjectOwnerType]
     */
    public void setRegardingobjectownertype(String regardingobjectownertype){
        this.regardingobjectownertype = regardingobjectownertype ;
        this.modify("regardingobjectownertype",regardingobjectownertype);
    }

    /**
     * 设置 [公布到 Yammer]
     */
    public void setPosttoyammer(Integer posttoyammer){
        this.posttoyammer = posttoyammer ;
        this.modify("posttoyammer",posttoyammer);
    }

    /**
     * 设置 [RegardingObjectTypeCode]
     */
    public void setRegardingobjecttypecode(String regardingobjecttypecode){
        this.regardingobjecttypecode = regardingobjecttypecode ;
        this.modify("regardingobjecttypecode",regardingobjecttypecode);
    }

    /**
     * 设置 [关于]
     */
    public void setRegardingobjectid(String regardingobjectid){
        this.regardingobjectid = regardingobjectid ;
        this.modify("regardingobjectid",regardingobjectid);
    }

    /**
     * 设置 [关于]
     */
    public void setRegardingobjectname(String regardingobjectname){
        this.regardingobjectname = regardingobjectname ;
        this.modify("regardingobjectname",regardingobjectname);
    }

    /**
     * 设置 [文本]
     */
    public void setText(String text){
        this.text = text ;
        this.modify("text",text);
    }

    /**
     * 设置 [Yammer 重试计数]
     */
    public void setYammerretrycount(Integer yammerretrycount){
        this.yammerretrycount = yammerretrycount ;
        this.modify("yammerretrycount",yammerretrycount);
    }

    /**
     * 设置 [UTC 转换时区代码]
     */
    public void setUtcconversiontimezonecode(Integer utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [Yammer 公布状态]
     */
    public void setYammerpoststate(Integer yammerpoststate){
        this.yammerpoststate = yammerpoststate ;
        this.modify("yammerpoststate",yammerpoststate);
    }

    /**
     * 设置 [来源]
     */
    public void setSource(String source){
        this.source = source ;
        this.modify("source",source);
    }


}


