package cn.ibizlab.businesscentral.core.scheduling.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.scheduling.domain.BookableResourceCharacteristic;
import cn.ibizlab.businesscentral.core.scheduling.filter.BookableResourceCharacteristicSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[BookableResourceCharacteristic] 服务对象接口
 */
public interface IBookableResourceCharacteristicService extends IService<BookableResourceCharacteristic>{

    boolean create(BookableResourceCharacteristic et) ;
    void createBatch(List<BookableResourceCharacteristic> list) ;
    boolean update(BookableResourceCharacteristic et) ;
    void updateBatch(List<BookableResourceCharacteristic> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    BookableResourceCharacteristic get(String key) ;
    BookableResourceCharacteristic getDraft(BookableResourceCharacteristic et) ;
    boolean checkKey(BookableResourceCharacteristic et) ;
    boolean save(BookableResourceCharacteristic et) ;
    void saveBatch(List<BookableResourceCharacteristic> list) ;
    Page<BookableResourceCharacteristic> searchDefault(BookableResourceCharacteristicSearchContext context) ;
    List<BookableResourceCharacteristic> selectByResource(String bookableresourceid) ;
    void removeByResource(String bookableresourceid) ;
    List<BookableResourceCharacteristic> selectByCharacteristic(String characteristicid) ;
    void removeByCharacteristic(String characteristicid) ;
    List<BookableResourceCharacteristic> selectByRatingvalue(String ratingvalueid) ;
    void removeByRatingvalue(String ratingvalueid) ;
    List<BookableResourceCharacteristic> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<BookableResourceCharacteristic> getBookableresourcecharacteristicByIds(List<String> ids) ;
    List<BookableResourceCharacteristic> getBookableresourcecharacteristicByEntities(List<BookableResourceCharacteristic> entities) ;
}


