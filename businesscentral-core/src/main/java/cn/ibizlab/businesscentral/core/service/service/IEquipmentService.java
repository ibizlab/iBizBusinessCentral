package cn.ibizlab.businesscentral.core.service.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.service.domain.Equipment;
import cn.ibizlab.businesscentral.core.service.filter.EquipmentSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Equipment] 服务对象接口
 */
public interface IEquipmentService extends IService<Equipment>{

    boolean create(Equipment et) ;
    void createBatch(List<Equipment> list) ;
    boolean update(Equipment et) ;
    void updateBatch(List<Equipment> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    Equipment get(String key) ;
    Equipment getDraft(Equipment et) ;
    boolean checkKey(Equipment et) ;
    boolean save(Equipment et) ;
    void saveBatch(List<Equipment> list) ;
    Page<Equipment> searchDefault(EquipmentSearchContext context) ;
    List<Equipment> selectByBusinessunitid(String businessunitid) ;
    void removeByBusinessunitid(String businessunitid) ;
    List<Equipment> selectByCalendarid(String calendarid) ;
    void removeByCalendarid(String calendarid) ;
    List<Equipment> selectBySiteid(String siteid) ;
    void removeBySiteid(String siteid) ;
    List<Equipment> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Equipment> getEquipmentByIds(List<String> ids) ;
    List<Equipment> getEquipmentByEntities(List<Equipment> entities) ;
}


