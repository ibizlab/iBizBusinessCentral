package cn.ibizlab.businesscentral.core.service.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[案例]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "INCIDENT",resultMap = "IncidentResultMap")
public class Incident extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 上一暂候时间
     */
    @TableField(value = "lastonholdtime")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "lastonholdtime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("lastonholdtime")
    private Timestamp lastonholdtime;
    /**
     * Version Number
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * 第一个响应 SLA 状态
     */
    @TableField(value = "firstresponseslastatus")
    @JSONField(name = "firstresponseslastatus")
    @JsonProperty("firstresponseslastatus")
    private String firstresponseslastatus;
    /**
     * 案例阶段
     */
    @TableField(value = "incidentstagecode")
    @JSONField(name = "incidentstagecode")
    @JsonProperty("incidentstagecode")
    private String incidentstagecode;
    /**
     * 影响分数
     */
    @TableField(value = "influencescore")
    @JSONField(name = "influencescore")
    @JsonProperty("influencescore")
    private Double influencescore;
    /**
     * 社交个人资料
     */
    @TableField(value = "socialprofileid")
    @JSONField(name = "socialprofileid")
    @JsonProperty("socialprofileid")
    private String socialprofileid;
    /**
     * 案例号
     */
    @TableField(value = "ticketnumber")
    @JSONField(name = "ticketnumber")
    @JsonProperty("ticketnumber")
    private String ticketnumber;
    /**
     * 仅供内部使用
     */
    @DEField(defaultValue = "0")
    @TableField(value = "merged")
    @JSONField(name = "merged")
    @JsonProperty("merged")
    private Integer merged;
    /**
     * 序列号
     */
    @TableField(value = "productserialnumber")
    @JSONField(name = "productserialnumber")
    @JsonProperty("productserialnumber")
    private String productserialnumber;
    /**
     * 客户
     */
    @TableField(value = "accountname")
    @JSONField(name = "accountname")
    @JsonProperty("accountname")
    private String accountname;
    /**
     * 说明
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 已升级
     */
    @DEField(defaultValue = "0")
    @TableField(value = "escalated")
    @JSONField(name = "escalated")
    @JsonProperty("escalated")
    private Integer escalated;
    /**
     * Stage Id
     */
    @TableField(value = "stageid")
    @JSONField(name = "stageid")
    @JsonProperty("stageid")
    private String stageid;
    /**
     * 案例类型
     */
    @TableField(value = "casetypecode")
    @JSONField(name = "casetypecode")
    @JsonProperty("casetypecode")
    private String casetypecode;
    /**
     * Email Address
     */
    @TableField(value = "emailaddress")
    @JSONField(name = "emailaddress")
    @JsonProperty("emailaddress")
    private String emailaddress;
    /**
     * 检查电子邮件
     */
    @DEField(defaultValue = "0")
    @TableField(value = "checkemail")
    @JSONField(name = "checkemail")
    @JsonProperty("checkemail")
    private Integer checkemail;
    /**
     * 严重性
     */
    @TableField(value = "severitycode")
    @JSONField(name = "severitycode")
    @JsonProperty("severitycode")
    private String severitycode;
    /**
     * 呈报日期
     */
    @TableField(value = "escalatedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "escalatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("escalatedon")
    private Timestamp escalatedon;
    /**
     * UTC Conversion Time Zone Code
     */
    @TableField(value = "utcconversiontimezonecode")
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;
    /**
     * 负责人
     */
    @TableField(value = "ownerid")
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;
    /**
     * 记帐服务计价单位
     */
    @TableField(value = "billedserviceunits")
    @JSONField(name = "billedserviceunits")
    @JsonProperty("billedserviceunits")
    private Integer billedserviceunits;
    /**
     * 优先级
     */
    @TableField(value = "prioritycode")
    @JSONField(name = "prioritycode")
    @JsonProperty("prioritycode")
    private String prioritycode;
    /**
     * 实体图像
     */
    @TableField(value = "entityimage")
    @JSONField(name = "entityimage")
    @JsonProperty("entityimage")
    private String entityimage;
    /**
     * 暂候时间(分钟)
     */
    @TableField(value = "onholdtime")
    @JSONField(name = "onholdtime")
    @JsonProperty("onholdtime")
    private Integer onholdtime;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 知识库文章
     */
    @TableField(value = "kbarticleid")
    @JSONField(name = "kbarticleid")
    @JsonProperty("kbarticleid")
    private String kbarticleid;
    /**
     * 服务级别
     */
    @TableField(value = "contractservicelevelcode")
    @JSONField(name = "contractservicelevelcode")
    @JsonProperty("contractservicelevelcode")
    private String contractservicelevelcode;
    /**
     * 负责人类型
     */
    @TableField(value = "ownertype")
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;
    /**
     * 负责人
     */
    @TableField(value = "ownername")
    @JSONField(name = "ownername")
    @JsonProperty("ownername")
    private String ownername;
    /**
     * EntityImage_URL
     */
    @DEField(name = "entityimage_url")
    @TableField(value = "entityimage_url")
    @JSONField(name = "entityimage_url")
    @JsonProperty("entityimage_url")
    private String entityimageUrl;
    /**
     * 已联系客户
     */
    @DEField(defaultValue = "0")
    @TableField(value = "customercontacted")
    @JSONField(name = "customercontacted")
    @JsonProperty("customercontacted")
    private Integer customercontacted;
    /**
     * Process Id
     */
    @TableField(value = "processid")
    @JSONField(name = "processid")
    @JsonProperty("processid")
    private String processid;
    /**
     * Time Zone Rule Version Number
     */
    @TableField(value = "timezoneruleversionnumber")
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 减少
     */
    @DEField(defaultValue = "0")
    @TableField(value = "decrementing")
    @JSONField(name = "decrementing")
    @JsonProperty("decrementing")
    private Integer decrementing;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 状态描述
     */
    @TableField(value = "statuscode")
    @JSONField(name = "statuscode")
    @JsonProperty("statuscode")
    private Integer statuscode;
    /**
     * 解决方
     */
    @TableField(value = "resolveby")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "resolveby" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("resolveby")
    private Timestamp resolveby;
    /**
     * 第一个响应者
     */
    @TableField(value = "responseby")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "responseby" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("responseby")
    private Timestamp responseby;
    /**
     * EntityImageId
     */
    @TableField(value = "entityimageid")
    @JSONField(name = "entityimageid")
    @JsonProperty("entityimageid")
    private String entityimageid;
    /**
     * 已创建跟进任务
     */
    @DEField(defaultValue = "0")
    @TableField(value = "followuptaskcreated")
    @JSONField(name = "followuptaskcreated")
    @JsonProperty("followuptaskcreated")
    private Integer followuptaskcreated;
    /**
     * 第一个响应已发送
     */
    @DEField(defaultValue = "0")
    @TableField(value = "firstresponsesent")
    @JSONField(name = "firstresponsesent")
    @JsonProperty("firstresponsesent")
    private Integer firstresponsesent;
    /**
     * 跟进工作截止日期
     */
    @TableField(value = "followupby")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "followupby" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("followupby")
    private Timestamp followupby;
    /**
     * 案例
     */
    @DEField(isKeyField=true)
    @TableId(value= "incidentid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "incidentid")
    @JsonProperty("incidentid")
    private String incidentid;
    /**
     * 服务阶段
     */
    @TableField(value = "servicestage")
    @JSONField(name = "servicestage")
    @JsonProperty("servicestage")
    private String servicestage;
    /**
     * Traversed Path
     */
    @TableField(value = "traversedpath")
    @JSONField(name = "traversedpath")
    @JsonProperty("traversedpath")
    private String traversedpath;
    /**
     * 子案例
     */
    @TableField(value = "numberofchildincidents")
    @JSONField(name = "numberofchildincidents")
    @JsonProperty("numberofchildincidents")
    private Integer numberofchildincidents;
    /**
     * 已阻止的配置文件
     */
    @DEField(defaultValue = "0")
    @TableField(value = "blockedprofile")
    @JSONField(name = "blockedprofile")
    @JsonProperty("blockedprofile")
    private Integer blockedprofile;
    /**
     * Record Created On
     */
    @TableField(value = "overriddencreatedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;
    /**
     * 实际服务计价单位
     */
    @TableField(value = "actualserviceunits")
    @JSONField(name = "actualserviceunits")
    @JsonProperty("actualserviceunits")
    private Integer actualserviceunits;
    /**
     * 接收为
     */
    @TableField(value = "messagetypecode")
    @JSONField(name = "messagetypecode")
    @JsonProperty("messagetypecode")
    private String messagetypecode;
    /**
     * 联系人
     */
    @TableField(value = "contactname")
    @JSONField(name = "contactname")
    @JsonProperty("contactname")
    private String contactname;
    /**
     * 传递案例
     */
    @DEField(defaultValue = "1")
    @TableField(value = "routecase")
    @JSONField(name = "routecase")
    @JsonProperty("routecase")
    private Integer routecase;
    /**
     * 状态
     */
    @TableField(value = "statecode")
    @JSONField(name = "statecode")
    @JsonProperty("statecode")
    private Integer statecode;
    /**
     * Import Sequence Number
     */
    @TableField(value = "importsequencenumber")
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;
    /**
     * 解决方 SLA 状态
     */
    @TableField(value = "resolvebyslastatus")
    @JSONField(name = "resolvebyslastatus")
    @JsonProperty("resolvebyslastatus")
    private String resolvebyslastatus;
    /**
     * 起源
     */
    @TableField(value = "caseorigincode")
    @JSONField(name = "caseorigincode")
    @JsonProperty("caseorigincode")
    private String caseorigincode;
    /**
     * 活动完成
     */
    @DEField(defaultValue = "0")
    @TableField(value = "activitiescomplete")
    @JSONField(name = "activitiescomplete")
    @JsonProperty("activitiescomplete")
    private Integer activitiescomplete;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 减少权利条件
     */
    @DEField(defaultValue = "1")
    @TableField(value = "decremententitlementterm")
    @JSONField(name = "decremententitlementterm")
    @JsonProperty("decremententitlementterm")
    private Integer decremententitlementterm;
    /**
     * 满意度
     */
    @TableField(value = "customersatisfactioncode")
    @JSONField(name = "customersatisfactioncode")
    @JsonProperty("customersatisfactioncode")
    private String customersatisfactioncode;
    /**
     * 情绪值
     */
    @TableField(value = "sentimentvalue")
    @JSONField(name = "sentimentvalue")
    @JsonProperty("sentimentvalue")
    private Double sentimentvalue;
    /**
     * EntityImage_Timestamp
     */
    @DEField(name = "entityimage_timestamp")
    @TableField(value = "entityimage_timestamp")
    @JSONField(name = "entityimage_timestamp")
    @JsonProperty("entityimage_timestamp")
    private BigInteger entityimageTimestamp;
    /**
     * 案例标题
     */
    @TableField(value = "title")
    @JSONField(name = "title")
    @JsonProperty("title")
    private String title;
    /**
     * 汇率
     */
    @TableField(value = "exchangerate")
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;
    /**
     * 现有服务案例
     */
    @TableField(value = "existingcasename")
    @JSONField(name = "existingcasename")
    @JsonProperty("existingcasename")
    private String existingcasename;
    /**
     * 主题
     */
    @TableField(value = "subjectname")
    @JSONField(name = "subjectname")
    @JsonProperty("subjectname")
    private String subjectname;
    /**
     * 合同子项
     */
    @TableField(value = "contractdetailname")
    @JSONField(name = "contractdetailname")
    @JsonProperty("contractdetailname")
    private String contractdetailname;
    /**
     * 上级案例
     */
    @TableField(value = "parentcasename")
    @JSONField(name = "parentcasename")
    @JsonProperty("parentcasename")
    private String parentcasename;
    /**
     * 权利
     */
    @TableField(value = "entitlementname")
    @JSONField(name = "entitlementname")
    @JsonProperty("entitlementname")
    private String entitlementname;
    /**
     * 主案例
     */
    @TableField(value = "mastername")
    @JSONField(name = "mastername")
    @JsonProperty("mastername")
    private String mastername;
    /**
     * 合同
     */
    @TableField(value = "contractname")
    @JSONField(name = "contractname")
    @JsonProperty("contractname")
    private String contractname;
    /**
     * 客户
     */
    @TableField(value = "customername")
    @JSONField(name = "customername")
    @JsonProperty("customername")
    private String customername;
    /**
     * 客户类型
     */
    @TableField(value = "customertype")
    @JSONField(name = "customertype")
    @JsonProperty("customertype")
    private String customertype;
    /**
     * 货币
     */
    @TableField(value = "currencyname")
    @JSONField(name = "currencyname")
    @JsonProperty("currencyname")
    private String currencyname;
    /**
     * 产品
     */
    @TableField(value = "productname")
    @JSONField(name = "productname")
    @JsonProperty("productname")
    private String productname;
    /**
     * SLA
     */
    @TableField(value = "slaname")
    @JSONField(name = "slaname")
    @JsonProperty("slaname")
    private String slaname;
    /**
     * 第一个响应者（按KPI）
     */
    @TableField(value = "firstresponsebykpiname")
    @JSONField(name = "firstresponsebykpiname")
    @JsonProperty("firstresponsebykpiname")
    private String firstresponsebykpiname;
    /**
     * 解决方KPI
     */
    @TableField(value = "resolvebykpiname")
    @JSONField(name = "resolvebykpiname")
    @JsonProperty("resolvebykpiname")
    private String resolvebykpiname;
    /**
     * 联系人
     */
    @TableField(value = "primarycontactname")
    @JSONField(name = "primarycontactname")
    @JsonProperty("primarycontactname")
    private String primarycontactname;
    /**
     * 责任联系人
     */
    @TableField(value = "responsiblecontactname")
    @JSONField(name = "responsiblecontactname")
    @JsonProperty("responsiblecontactname")
    private String responsiblecontactname;
    /**
     * 权利
     */
    @TableField(value = "entitlementid")
    @JSONField(name = "entitlementid")
    @JsonProperty("entitlementid")
    private String entitlementid;
    /**
     * 客户
     */
    @TableField(value = "customerid")
    @JSONField(name = "customerid")
    @JsonProperty("customerid")
    private String customerid;
    /**
     * 第一个响应者(按 KPI)
     */
    @TableField(value = "firstresponsebykpiid")
    @JSONField(name = "firstresponsebykpiid")
    @JsonProperty("firstresponsebykpiid")
    private String firstresponsebykpiid;
    /**
     * SLA
     */
    @TableField(value = "slaid")
    @JSONField(name = "slaid")
    @JsonProperty("slaid")
    private String slaid;
    /**
     * 现有服务案例
     */
    @TableField(value = "existingcase")
    @JSONField(name = "existingcase")
    @JsonProperty("existingcase")
    private String existingcase;
    /**
     * 产品
     */
    @TableField(value = "productid")
    @JSONField(name = "productid")
    @JsonProperty("productid")
    private String productid;
    /**
     * 合同
     */
    @TableField(value = "contractid")
    @JSONField(name = "contractid")
    @JsonProperty("contractid")
    private String contractid;
    /**
     * 责任联系人
     */
    @TableField(value = "responsiblecontactid")
    @JSONField(name = "responsiblecontactid")
    @JsonProperty("responsiblecontactid")
    private String responsiblecontactid;
    /**
     * 解决方 KPI
     */
    @TableField(value = "resolvebykpiid")
    @JSONField(name = "resolvebykpiid")
    @JsonProperty("resolvebykpiid")
    private String resolvebykpiid;
    /**
     * 合同子项
     */
    @TableField(value = "contractdetailid")
    @JSONField(name = "contractdetailid")
    @JsonProperty("contractdetailid")
    private String contractdetailid;
    /**
     * 主题
     */
    @TableField(value = "subjectid")
    @JSONField(name = "subjectid")
    @JsonProperty("subjectid")
    private String subjectid;
    /**
     * 主案例
     */
    @TableField(value = "masterid")
    @JSONField(name = "masterid")
    @JsonProperty("masterid")
    private String masterid;
    /**
     * 上级案例
     */
    @TableField(value = "parentcaseid")
    @JSONField(name = "parentcaseid")
    @JsonProperty("parentcaseid")
    private String parentcaseid;
    /**
     * 联系人
     */
    @TableField(value = "primarycontactid")
    @JSONField(name = "primarycontactid")
    @JsonProperty("primarycontactid")
    private String primarycontactid;
    /**
     * 货币
     */
    @TableField(value = "transactioncurrencyid")
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.service.domain.IncidentCustomer customer;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.Contact primarycontact;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.Contact responsiblecontact;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.service.domain.ContractDetail contractdetail;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.service.domain.Contract contract;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.service.domain.Entitlement entitlement;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.service.domain.Incident existingca;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.service.domain.Incident master;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.service.domain.Incident parentcase;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.product.domain.Product product;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.SlaKpiInstance firstresponsebykpi;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.SlaKpiInstance resolvebykpi;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.Sla sla;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.Subject subject;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency transactioncurrency;



    /**
     * 设置 [上一暂候时间]
     */
    public void setLastonholdtime(Timestamp lastonholdtime){
        this.lastonholdtime = lastonholdtime ;
        this.modify("lastonholdtime",lastonholdtime);
    }

    /**
     * 格式化日期 [上一暂候时间]
     */
    public String formatLastonholdtime(){
        if (this.lastonholdtime == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(lastonholdtime);
    }
    /**
     * 设置 [Version Number]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [第一个响应 SLA 状态]
     */
    public void setFirstresponseslastatus(String firstresponseslastatus){
        this.firstresponseslastatus = firstresponseslastatus ;
        this.modify("firstresponseslastatus",firstresponseslastatus);
    }

    /**
     * 设置 [案例阶段]
     */
    public void setIncidentstagecode(String incidentstagecode){
        this.incidentstagecode = incidentstagecode ;
        this.modify("incidentstagecode",incidentstagecode);
    }

    /**
     * 设置 [影响分数]
     */
    public void setInfluencescore(Double influencescore){
        this.influencescore = influencescore ;
        this.modify("influencescore",influencescore);
    }

    /**
     * 设置 [社交个人资料]
     */
    public void setSocialprofileid(String socialprofileid){
        this.socialprofileid = socialprofileid ;
        this.modify("socialprofileid",socialprofileid);
    }

    /**
     * 设置 [案例号]
     */
    public void setTicketnumber(String ticketnumber){
        this.ticketnumber = ticketnumber ;
        this.modify("ticketnumber",ticketnumber);
    }

    /**
     * 设置 [仅供内部使用]
     */
    public void setMerged(Integer merged){
        this.merged = merged ;
        this.modify("merged",merged);
    }

    /**
     * 设置 [序列号]
     */
    public void setProductserialnumber(String productserialnumber){
        this.productserialnumber = productserialnumber ;
        this.modify("productserialnumber",productserialnumber);
    }

    /**
     * 设置 [客户]
     */
    public void setAccountname(String accountname){
        this.accountname = accountname ;
        this.modify("accountname",accountname);
    }

    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [已升级]
     */
    public void setEscalated(Integer escalated){
        this.escalated = escalated ;
        this.modify("escalated",escalated);
    }

    /**
     * 设置 [Stage Id]
     */
    public void setStageid(String stageid){
        this.stageid = stageid ;
        this.modify("stageid",stageid);
    }

    /**
     * 设置 [案例类型]
     */
    public void setCasetypecode(String casetypecode){
        this.casetypecode = casetypecode ;
        this.modify("casetypecode",casetypecode);
    }

    /**
     * 设置 [Email Address]
     */
    public void setEmailaddress(String emailaddress){
        this.emailaddress = emailaddress ;
        this.modify("emailaddress",emailaddress);
    }

    /**
     * 设置 [检查电子邮件]
     */
    public void setCheckemail(Integer checkemail){
        this.checkemail = checkemail ;
        this.modify("checkemail",checkemail);
    }

    /**
     * 设置 [严重性]
     */
    public void setSeveritycode(String severitycode){
        this.severitycode = severitycode ;
        this.modify("severitycode",severitycode);
    }

    /**
     * 设置 [呈报日期]
     */
    public void setEscalatedon(Timestamp escalatedon){
        this.escalatedon = escalatedon ;
        this.modify("escalatedon",escalatedon);
    }

    /**
     * 格式化日期 [呈报日期]
     */
    public String formatEscalatedon(){
        if (this.escalatedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(escalatedon);
    }
    /**
     * 设置 [UTC Conversion Time Zone Code]
     */
    public void setUtcconversiontimezonecode(Integer utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnerid(String ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [记帐服务计价单位]
     */
    public void setBilledserviceunits(Integer billedserviceunits){
        this.billedserviceunits = billedserviceunits ;
        this.modify("billedserviceunits",billedserviceunits);
    }

    /**
     * 设置 [优先级]
     */
    public void setPrioritycode(String prioritycode){
        this.prioritycode = prioritycode ;
        this.modify("prioritycode",prioritycode);
    }

    /**
     * 设置 [实体图像]
     */
    public void setEntityimage(String entityimage){
        this.entityimage = entityimage ;
        this.modify("entityimage",entityimage);
    }

    /**
     * 设置 [暂候时间(分钟)]
     */
    public void setOnholdtime(Integer onholdtime){
        this.onholdtime = onholdtime ;
        this.modify("onholdtime",onholdtime);
    }

    /**
     * 设置 [知识库文章]
     */
    public void setKbarticleid(String kbarticleid){
        this.kbarticleid = kbarticleid ;
        this.modify("kbarticleid",kbarticleid);
    }

    /**
     * 设置 [服务级别]
     */
    public void setContractservicelevelcode(String contractservicelevelcode){
        this.contractservicelevelcode = contractservicelevelcode ;
        this.modify("contractservicelevelcode",contractservicelevelcode);
    }

    /**
     * 设置 [负责人类型]
     */
    public void setOwnertype(String ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnername(String ownername){
        this.ownername = ownername ;
        this.modify("ownername",ownername);
    }

    /**
     * 设置 [EntityImage_URL]
     */
    public void setEntityimageUrl(String entityimageUrl){
        this.entityimageUrl = entityimageUrl ;
        this.modify("entityimage_url",entityimageUrl);
    }

    /**
     * 设置 [已联系客户]
     */
    public void setCustomercontacted(Integer customercontacted){
        this.customercontacted = customercontacted ;
        this.modify("customercontacted",customercontacted);
    }

    /**
     * 设置 [Process Id]
     */
    public void setProcessid(String processid){
        this.processid = processid ;
        this.modify("processid",processid);
    }

    /**
     * 设置 [Time Zone Rule Version Number]
     */
    public void setTimezoneruleversionnumber(Integer timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [减少]
     */
    public void setDecrementing(Integer decrementing){
        this.decrementing = decrementing ;
        this.modify("decrementing",decrementing);
    }

    /**
     * 设置 [状态描述]
     */
    public void setStatuscode(Integer statuscode){
        this.statuscode = statuscode ;
        this.modify("statuscode",statuscode);
    }

    /**
     * 设置 [解决方]
     */
    public void setResolveby(Timestamp resolveby){
        this.resolveby = resolveby ;
        this.modify("resolveby",resolveby);
    }

    /**
     * 格式化日期 [解决方]
     */
    public String formatResolveby(){
        if (this.resolveby == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(resolveby);
    }
    /**
     * 设置 [第一个响应者]
     */
    public void setResponseby(Timestamp responseby){
        this.responseby = responseby ;
        this.modify("responseby",responseby);
    }

    /**
     * 格式化日期 [第一个响应者]
     */
    public String formatResponseby(){
        if (this.responseby == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(responseby);
    }
    /**
     * 设置 [EntityImageId]
     */
    public void setEntityimageid(String entityimageid){
        this.entityimageid = entityimageid ;
        this.modify("entityimageid",entityimageid);
    }

    /**
     * 设置 [已创建跟进任务]
     */
    public void setFollowuptaskcreated(Integer followuptaskcreated){
        this.followuptaskcreated = followuptaskcreated ;
        this.modify("followuptaskcreated",followuptaskcreated);
    }

    /**
     * 设置 [第一个响应已发送]
     */
    public void setFirstresponsesent(Integer firstresponsesent){
        this.firstresponsesent = firstresponsesent ;
        this.modify("firstresponsesent",firstresponsesent);
    }

    /**
     * 设置 [跟进工作截止日期]
     */
    public void setFollowupby(Timestamp followupby){
        this.followupby = followupby ;
        this.modify("followupby",followupby);
    }

    /**
     * 格式化日期 [跟进工作截止日期]
     */
    public String formatFollowupby(){
        if (this.followupby == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(followupby);
    }
    /**
     * 设置 [服务阶段]
     */
    public void setServicestage(String servicestage){
        this.servicestage = servicestage ;
        this.modify("servicestage",servicestage);
    }

    /**
     * 设置 [Traversed Path]
     */
    public void setTraversedpath(String traversedpath){
        this.traversedpath = traversedpath ;
        this.modify("traversedpath",traversedpath);
    }

    /**
     * 设置 [子案例]
     */
    public void setNumberofchildincidents(Integer numberofchildincidents){
        this.numberofchildincidents = numberofchildincidents ;
        this.modify("numberofchildincidents",numberofchildincidents);
    }

    /**
     * 设置 [已阻止的配置文件]
     */
    public void setBlockedprofile(Integer blockedprofile){
        this.blockedprofile = blockedprofile ;
        this.modify("blockedprofile",blockedprofile);
    }

    /**
     * 设置 [Record Created On]
     */
    public void setOverriddencreatedon(Timestamp overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 格式化日期 [Record Created On]
     */
    public String formatOverriddencreatedon(){
        if (this.overriddencreatedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(overriddencreatedon);
    }
    /**
     * 设置 [实际服务计价单位]
     */
    public void setActualserviceunits(Integer actualserviceunits){
        this.actualserviceunits = actualserviceunits ;
        this.modify("actualserviceunits",actualserviceunits);
    }

    /**
     * 设置 [接收为]
     */
    public void setMessagetypecode(String messagetypecode){
        this.messagetypecode = messagetypecode ;
        this.modify("messagetypecode",messagetypecode);
    }

    /**
     * 设置 [联系人]
     */
    public void setContactname(String contactname){
        this.contactname = contactname ;
        this.modify("contactname",contactname);
    }

    /**
     * 设置 [传递案例]
     */
    public void setRoutecase(Integer routecase){
        this.routecase = routecase ;
        this.modify("routecase",routecase);
    }

    /**
     * 设置 [状态]
     */
    public void setStatecode(Integer statecode){
        this.statecode = statecode ;
        this.modify("statecode",statecode);
    }

    /**
     * 设置 [Import Sequence Number]
     */
    public void setImportsequencenumber(Integer importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [解决方 SLA 状态]
     */
    public void setResolvebyslastatus(String resolvebyslastatus){
        this.resolvebyslastatus = resolvebyslastatus ;
        this.modify("resolvebyslastatus",resolvebyslastatus);
    }

    /**
     * 设置 [起源]
     */
    public void setCaseorigincode(String caseorigincode){
        this.caseorigincode = caseorigincode ;
        this.modify("caseorigincode",caseorigincode);
    }

    /**
     * 设置 [活动完成]
     */
    public void setActivitiescomplete(Integer activitiescomplete){
        this.activitiescomplete = activitiescomplete ;
        this.modify("activitiescomplete",activitiescomplete);
    }

    /**
     * 设置 [减少权利条件]
     */
    public void setDecremententitlementterm(Integer decremententitlementterm){
        this.decremententitlementterm = decremententitlementterm ;
        this.modify("decremententitlementterm",decremententitlementterm);
    }

    /**
     * 设置 [满意度]
     */
    public void setCustomersatisfactioncode(String customersatisfactioncode){
        this.customersatisfactioncode = customersatisfactioncode ;
        this.modify("customersatisfactioncode",customersatisfactioncode);
    }

    /**
     * 设置 [情绪值]
     */
    public void setSentimentvalue(Double sentimentvalue){
        this.sentimentvalue = sentimentvalue ;
        this.modify("sentimentvalue",sentimentvalue);
    }

    /**
     * 设置 [EntityImage_Timestamp]
     */
    public void setEntityimageTimestamp(BigInteger entityimageTimestamp){
        this.entityimageTimestamp = entityimageTimestamp ;
        this.modify("entityimage_timestamp",entityimageTimestamp);
    }

    /**
     * 设置 [案例标题]
     */
    public void setTitle(String title){
        this.title = title ;
        this.modify("title",title);
    }

    /**
     * 设置 [汇率]
     */
    public void setExchangerate(BigDecimal exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [现有服务案例]
     */
    public void setExistingcasename(String existingcasename){
        this.existingcasename = existingcasename ;
        this.modify("existingcasename",existingcasename);
    }

    /**
     * 设置 [主题]
     */
    public void setSubjectname(String subjectname){
        this.subjectname = subjectname ;
        this.modify("subjectname",subjectname);
    }

    /**
     * 设置 [合同子项]
     */
    public void setContractdetailname(String contractdetailname){
        this.contractdetailname = contractdetailname ;
        this.modify("contractdetailname",contractdetailname);
    }

    /**
     * 设置 [上级案例]
     */
    public void setParentcasename(String parentcasename){
        this.parentcasename = parentcasename ;
        this.modify("parentcasename",parentcasename);
    }

    /**
     * 设置 [权利]
     */
    public void setEntitlementname(String entitlementname){
        this.entitlementname = entitlementname ;
        this.modify("entitlementname",entitlementname);
    }

    /**
     * 设置 [主案例]
     */
    public void setMastername(String mastername){
        this.mastername = mastername ;
        this.modify("mastername",mastername);
    }

    /**
     * 设置 [合同]
     */
    public void setContractname(String contractname){
        this.contractname = contractname ;
        this.modify("contractname",contractname);
    }

    /**
     * 设置 [客户]
     */
    public void setCustomername(String customername){
        this.customername = customername ;
        this.modify("customername",customername);
    }

    /**
     * 设置 [客户类型]
     */
    public void setCustomertype(String customertype){
        this.customertype = customertype ;
        this.modify("customertype",customertype);
    }

    /**
     * 设置 [货币]
     */
    public void setCurrencyname(String currencyname){
        this.currencyname = currencyname ;
        this.modify("currencyname",currencyname);
    }

    /**
     * 设置 [产品]
     */
    public void setProductname(String productname){
        this.productname = productname ;
        this.modify("productname",productname);
    }

    /**
     * 设置 [SLA]
     */
    public void setSlaname(String slaname){
        this.slaname = slaname ;
        this.modify("slaname",slaname);
    }

    /**
     * 设置 [第一个响应者（按KPI）]
     */
    public void setFirstresponsebykpiname(String firstresponsebykpiname){
        this.firstresponsebykpiname = firstresponsebykpiname ;
        this.modify("firstresponsebykpiname",firstresponsebykpiname);
    }

    /**
     * 设置 [解决方KPI]
     */
    public void setResolvebykpiname(String resolvebykpiname){
        this.resolvebykpiname = resolvebykpiname ;
        this.modify("resolvebykpiname",resolvebykpiname);
    }

    /**
     * 设置 [联系人]
     */
    public void setPrimarycontactname(String primarycontactname){
        this.primarycontactname = primarycontactname ;
        this.modify("primarycontactname",primarycontactname);
    }

    /**
     * 设置 [责任联系人]
     */
    public void setResponsiblecontactname(String responsiblecontactname){
        this.responsiblecontactname = responsiblecontactname ;
        this.modify("responsiblecontactname",responsiblecontactname);
    }

    /**
     * 设置 [权利]
     */
    public void setEntitlementid(String entitlementid){
        this.entitlementid = entitlementid ;
        this.modify("entitlementid",entitlementid);
    }

    /**
     * 设置 [客户]
     */
    public void setCustomerid(String customerid){
        this.customerid = customerid ;
        this.modify("customerid",customerid);
    }

    /**
     * 设置 [第一个响应者(按 KPI)]
     */
    public void setFirstresponsebykpiid(String firstresponsebykpiid){
        this.firstresponsebykpiid = firstresponsebykpiid ;
        this.modify("firstresponsebykpiid",firstresponsebykpiid);
    }

    /**
     * 设置 [SLA]
     */
    public void setSlaid(String slaid){
        this.slaid = slaid ;
        this.modify("slaid",slaid);
    }

    /**
     * 设置 [现有服务案例]
     */
    public void setExistingcase(String existingcase){
        this.existingcase = existingcase ;
        this.modify("existingcase",existingcase);
    }

    /**
     * 设置 [产品]
     */
    public void setProductid(String productid){
        this.productid = productid ;
        this.modify("productid",productid);
    }

    /**
     * 设置 [合同]
     */
    public void setContractid(String contractid){
        this.contractid = contractid ;
        this.modify("contractid",contractid);
    }

    /**
     * 设置 [责任联系人]
     */
    public void setResponsiblecontactid(String responsiblecontactid){
        this.responsiblecontactid = responsiblecontactid ;
        this.modify("responsiblecontactid",responsiblecontactid);
    }

    /**
     * 设置 [解决方 KPI]
     */
    public void setResolvebykpiid(String resolvebykpiid){
        this.resolvebykpiid = resolvebykpiid ;
        this.modify("resolvebykpiid",resolvebykpiid);
    }

    /**
     * 设置 [合同子项]
     */
    public void setContractdetailid(String contractdetailid){
        this.contractdetailid = contractdetailid ;
        this.modify("contractdetailid",contractdetailid);
    }

    /**
     * 设置 [主题]
     */
    public void setSubjectid(String subjectid){
        this.subjectid = subjectid ;
        this.modify("subjectid",subjectid);
    }

    /**
     * 设置 [主案例]
     */
    public void setMasterid(String masterid){
        this.masterid = masterid ;
        this.modify("masterid",masterid);
    }

    /**
     * 设置 [上级案例]
     */
    public void setParentcaseid(String parentcaseid){
        this.parentcaseid = parentcaseid ;
        this.modify("parentcaseid",parentcaseid);
    }

    /**
     * 设置 [联系人]
     */
    public void setPrimarycontactid(String primarycontactid){
        this.primarycontactid = primarycontactid ;
        this.modify("primarycontactid",primarycontactid);
    }

    /**
     * 设置 [货币]
     */
    public void setTransactioncurrencyid(String transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }


}


