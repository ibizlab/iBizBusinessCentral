package cn.ibizlab.businesscentral.core.sales.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.sales.domain.LeadCompetitor;
/**
 * 关系型数据实体[LeadCompetitor] 查询条件对象
 */
@Slf4j
@Data
public class LeadCompetitorSearchContext extends QueryWrapperContext<LeadCompetitor> {

	private String n_relationshipstype_eq;//[关系类型]
	public void setN_relationshipstype_eq(String n_relationshipstype_eq) {
        this.n_relationshipstype_eq = n_relationshipstype_eq;
        if(!ObjectUtils.isEmpty(this.n_relationshipstype_eq)){
            this.getSearchCond().eq("relationshipstype", n_relationshipstype_eq);
        }
    }
	private String n_relationshipsname_like;//[关系名称]
	public void setN_relationshipsname_like(String n_relationshipsname_like) {
        this.n_relationshipsname_like = n_relationshipsname_like;
        if(!ObjectUtils.isEmpty(this.n_relationshipsname_like)){
            this.getSearchCond().like("relationshipsname", n_relationshipsname_like);
        }
    }
	private String n_entityname_eq;//[潜在客户]
	public void setN_entityname_eq(String n_entityname_eq) {
        this.n_entityname_eq = n_entityname_eq;
        if(!ObjectUtils.isEmpty(this.n_entityname_eq)){
            this.getSearchCond().eq("entityname", n_entityname_eq);
        }
    }
	private String n_entityname_like;//[潜在客户]
	public void setN_entityname_like(String n_entityname_like) {
        this.n_entityname_like = n_entityname_like;
        if(!ObjectUtils.isEmpty(this.n_entityname_like)){
            this.getSearchCond().like("entityname", n_entityname_like);
        }
    }
	private String n_entity2name_eq;//[对手]
	public void setN_entity2name_eq(String n_entity2name_eq) {
        this.n_entity2name_eq = n_entity2name_eq;
        if(!ObjectUtils.isEmpty(this.n_entity2name_eq)){
            this.getSearchCond().eq("entity2name", n_entity2name_eq);
        }
    }
	private String n_entity2name_like;//[对手]
	public void setN_entity2name_like(String n_entity2name_like) {
        this.n_entity2name_like = n_entity2name_like;
        if(!ObjectUtils.isEmpty(this.n_entity2name_like)){
            this.getSearchCond().like("entity2name", n_entity2name_like);
        }
    }
	private String n_entityid_eq;//[潜在顾客]
	public void setN_entityid_eq(String n_entityid_eq) {
        this.n_entityid_eq = n_entityid_eq;
        if(!ObjectUtils.isEmpty(this.n_entityid_eq)){
            this.getSearchCond().eq("entityid", n_entityid_eq);
        }
    }
	private String n_entity2id_eq;//[竞争对手]
	public void setN_entity2id_eq(String n_entity2id_eq) {
        this.n_entity2id_eq = n_entity2id_eq;
        if(!ObjectUtils.isEmpty(this.n_entity2id_eq)){
            this.getSearchCond().eq("entity2id", n_entity2id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("relationshipsname", query)   
            );
		 }
	}
}



