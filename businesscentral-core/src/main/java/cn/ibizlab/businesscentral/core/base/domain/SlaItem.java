package cn.ibizlab.businesscentral.core.base.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[SLA 项]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SLAITEM",resultMap = "SlaItemResultMap")
public class SlaItem extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * SuccessConditionsXml
     */
    @TableField(value = "successconditionsxml")
    @JSONField(name = "successconditionsxml")
    @JsonProperty("successconditionsxml")
    private String successconditionsxml;
    /**
     * 警告超时时间
     */
    @TableField(value = "warnafter")
    @JSONField(name = "warnafter")
    @JsonProperty("warnafter")
    private Integer warnafter;
    /**
     * WorkflowIdName
     */
    @TableField(value = "workflowidname")
    @JSONField(name = "workflowidname")
    @JsonProperty("workflowidname")
    private String workflowidname;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 工作流 ID
     */
    @TableField(value = "workflowid")
    @JSONField(name = "workflowid")
    @JsonProperty("workflowid")
    private String workflowid;
    /**
     * 托管
     */
    @DEField(defaultValue = "0")
    @TableField(value = "managed")
    @JSONField(name = "managed")
    @JsonProperty("managed")
    private Integer managed;
    /**
     * 名称
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 失败超时时间
     */
    @TableField(value = "failureafter")
    @JSONField(name = "failureafter")
    @JsonProperty("failureafter")
    private Integer failureafter;
    /**
     * 解决方案
     */
    @TableField(value = "solutionid")
    @JSONField(name = "solutionid")
    @JsonProperty("solutionid")
    private String solutionid;
    /**
     * 版本号
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * 唯一 ID
     */
    @TableField(value = "slaitemidunique")
    @JSONField(name = "slaitemidunique")
    @JsonProperty("slaitemidunique")
    private String slaitemidunique;
    /**
     * 说明
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 记录替代时间
     */
    @TableField(value = "overwritetime")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overwritetime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overwritetime")
    private Timestamp overwritetime;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 负责人类型
     */
    @TableField(value = "ownertype")
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;
    /**
     * 序列
     */
    @TableField(value = "sequencenumber")
    @JSONField(name = "sequencenumber")
    @JsonProperty("sequencenumber")
    private Integer sequencenumber;
    /**
     * SLA 项
     */
    @DEField(isKeyField=true)
    @TableId(value= "slaitemid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "slaitemid")
    @JsonProperty("slaitemid")
    private String slaitemid;
    /**
     * 负责人用户
     */
    @TableField(value = "owninguser")
    @JSONField(name = "owninguser")
    @JsonProperty("owninguser")
    private String owninguser;
    /**
     * 相关案例字段
     */
    @TableField(value = "relatedfield")
    @JSONField(name = "relatedfield")
    @JsonProperty("relatedfield")
    private String relatedfield;
    /**
     * ApplicableWhenXml
     */
    @TableField(value = "applicablewhenxml")
    @JSONField(name = "applicablewhenxml")
    @JsonProperty("applicablewhenxml")
    private String applicablewhenxml;
    /**
     * 汇率
     */
    @TableField(value = "exchangerate")
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;
    /**
     * 负责人
     */
    @TableField(value = "ownerid")
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;
    /**
     * 组件状态
     */
    @TableField(value = "componentstate")
    @JSONField(name = "componentstate")
    @JsonProperty("componentstate")
    private String componentstate;
    /**
     * 解决方案
     */
    @TableField(value = "supportingsolutionid")
    @JSONField(name = "supportingsolutionid")
    @JsonProperty("supportingsolutionid")
    private String supportingsolutionid;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * SLA
     */
    @TableField(value = "slaid")
    @JSONField(name = "slaid")
    @JsonProperty("slaid")
    private String slaid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.Sla sla;



    /**
     * 设置 [SuccessConditionsXml]
     */
    public void setSuccessconditionsxml(String successconditionsxml){
        this.successconditionsxml = successconditionsxml ;
        this.modify("successconditionsxml",successconditionsxml);
    }

    /**
     * 设置 [警告超时时间]
     */
    public void setWarnafter(Integer warnafter){
        this.warnafter = warnafter ;
        this.modify("warnafter",warnafter);
    }

    /**
     * 设置 [WorkflowIdName]
     */
    public void setWorkflowidname(String workflowidname){
        this.workflowidname = workflowidname ;
        this.modify("workflowidname",workflowidname);
    }

    /**
     * 设置 [工作流 ID]
     */
    public void setWorkflowid(String workflowid){
        this.workflowid = workflowid ;
        this.modify("workflowid",workflowid);
    }

    /**
     * 设置 [托管]
     */
    public void setManaged(Integer managed){
        this.managed = managed ;
        this.modify("managed",managed);
    }

    /**
     * 设置 [名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [失败超时时间]
     */
    public void setFailureafter(Integer failureafter){
        this.failureafter = failureafter ;
        this.modify("failureafter",failureafter);
    }

    /**
     * 设置 [解决方案]
     */
    public void setSolutionid(String solutionid){
        this.solutionid = solutionid ;
        this.modify("solutionid",solutionid);
    }

    /**
     * 设置 [版本号]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [唯一 ID]
     */
    public void setSlaitemidunique(String slaitemidunique){
        this.slaitemidunique = slaitemidunique ;
        this.modify("slaitemidunique",slaitemidunique);
    }

    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [记录替代时间]
     */
    public void setOverwritetime(Timestamp overwritetime){
        this.overwritetime = overwritetime ;
        this.modify("overwritetime",overwritetime);
    }

    /**
     * 格式化日期 [记录替代时间]
     */
    public String formatOverwritetime(){
        if (this.overwritetime == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(overwritetime);
    }
    /**
     * 设置 [负责人类型]
     */
    public void setOwnertype(String ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [序列]
     */
    public void setSequencenumber(Integer sequencenumber){
        this.sequencenumber = sequencenumber ;
        this.modify("sequencenumber",sequencenumber);
    }

    /**
     * 设置 [负责人用户]
     */
    public void setOwninguser(String owninguser){
        this.owninguser = owninguser ;
        this.modify("owninguser",owninguser);
    }

    /**
     * 设置 [相关案例字段]
     */
    public void setRelatedfield(String relatedfield){
        this.relatedfield = relatedfield ;
        this.modify("relatedfield",relatedfield);
    }

    /**
     * 设置 [ApplicableWhenXml]
     */
    public void setApplicablewhenxml(String applicablewhenxml){
        this.applicablewhenxml = applicablewhenxml ;
        this.modify("applicablewhenxml",applicablewhenxml);
    }

    /**
     * 设置 [汇率]
     */
    public void setExchangerate(BigDecimal exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnerid(String ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [组件状态]
     */
    public void setComponentstate(String componentstate){
        this.componentstate = componentstate ;
        this.modify("componentstate",componentstate);
    }

    /**
     * 设置 [解决方案]
     */
    public void setSupportingsolutionid(String supportingsolutionid){
        this.supportingsolutionid = supportingsolutionid ;
        this.modify("supportingsolutionid",supportingsolutionid);
    }

    /**
     * 设置 [SLA]
     */
    public void setSlaid(String slaid){
        this.slaid = slaid ;
        this.modify("slaid",slaid);
    }


}


