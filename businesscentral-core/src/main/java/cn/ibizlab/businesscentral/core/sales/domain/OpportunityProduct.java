package cn.ibizlab.businesscentral.core.sales.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[商机产品]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "OPPORTUNITYPRODUCT",resultMap = "OpportunityProductResultMap")
public class OpportunityProduct extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 自定义的价格
     */
    @DEField(defaultValue = "0")
    @TableField(value = "priceoverridden")
    @JSONField(name = "priceoverridden")
    @JsonProperty("priceoverridden")
    private Integer priceoverridden;
    /**
     * 税 (Base)
     */
    @DEField(name = "tax_base")
    @TableField(value = "tax_base")
    @JSONField(name = "tax_base")
    @JsonProperty("tax_base")
    private BigDecimal taxBase;
    /**
     * SkipPriceCalculation
     */
    @TableField(value = "skippricecalculation")
    @JSONField(name = "skippricecalculation")
    @JsonProperty("skippricecalculation")
    private String skippricecalculation;
    /**
     * 批发折扣 (Base)
     */
    @DEField(name = "volumediscountamount_base")
    @TableField(value = "volumediscountamount_base")
    @JSONField(name = "volumediscountamount_base")
    @JsonProperty("volumediscountamount_base")
    private BigDecimal volumediscountamountBase;
    /**
     * 零售折扣金额
     */
    @TableField(value = "manualdiscountamount")
    @JSONField(name = "manualdiscountamount")
    @JsonProperty("manualdiscountamount")
    private BigDecimal manualdiscountamount;
    /**
     * EntityImage_Timestamp
     */
    @DEField(name = "entityimage_timestamp")
    @TableField(value = "entityimage_timestamp")
    @JSONField(name = "entityimage_timestamp")
    @JsonProperty("entityimage_timestamp")
    private BigInteger entityimageTimestamp;
    /**
     * 商机状态
     */
    @TableField(value = "opportunitystatecode")
    @JSONField(name = "opportunitystatecode")
    @JsonProperty("opportunitystatecode")
    private String opportunitystatecode;
    /**
     * 负责人类型
     */
    @TableField(value = "ownertype")
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;
    /**
     * 批发折扣
     */
    @TableField(value = "volumediscountamount")
    @JSONField(name = "volumediscountamount")
    @JsonProperty("volumediscountamount")
    private BigDecimal volumediscountamount;
    /**
     * 应收净额 (Base)
     */
    @DEField(name = "extendedamount_base")
    @TableField(value = "extendedamount_base")
    @JSONField(name = "extendedamount_base")
    @JsonProperty("extendedamount_base")
    private BigDecimal extendedamountBase;
    /**
     * 负责人
     */
    @TableField(value = "ownername")
    @JSONField(name = "ownername")
    @JsonProperty("ownername")
    private String ownername;
    /**
     * 单价 (Base)
     */
    @DEField(name = "priceperunit_base")
    @TableField(value = "priceperunit_base")
    @JSONField(name = "priceperunit_base")
    @JsonProperty("priceperunit_base")
    private BigDecimal priceperunitBase;
    /**
     * 商机产品
     */
    @DEField(isKeyField=true)
    @TableId(value= "opportunityproductid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "opportunityproductid")
    @JsonProperty("opportunityproductid")
    private String opportunityproductid;
    /**
     * Version Number
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * EntityImageId
     */
    @TableField(value = "entityimageid")
    @JSONField(name = "entityimageid")
    @JsonProperty("entityimageid")
    private String entityimageid;
    /**
     * 说明
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 零售折扣金额 (Base)
     */
    @DEField(name = "manualdiscountamount_base")
    @TableField(value = "manualdiscountamount_base")
    @JSONField(name = "manualdiscountamount_base")
    @JsonProperty("manualdiscountamount_base")
    private BigDecimal manualdiscountamountBase;
    /**
     * 序号
     */
    @TableField(value = "sequencenumber")
    @JSONField(name = "sequencenumber")
    @JsonProperty("sequencenumber")
    private Integer sequencenumber;
    /**
     * 目录外产品
     */
    @TableField(value = "productdescription")
    @JSONField(name = "productdescription")
    @JsonProperty("productdescription")
    private String productdescription;
    /**
     * 选择产品
     */
    @DEField(defaultValue = "0")
    @TableField(value = "productoverridden")
    @JSONField(name = "productoverridden")
    @JsonProperty("productoverridden")
    private Integer productoverridden;
    /**
     * Product Name
     */
    @TableField(value = "productname")
    @JSONField(name = "productname")
    @JsonProperty("productname")
    private String productname;
    /**
     * 汇率
     */
    @TableField(value = "exchangerate")
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;
    /**
     * 名称
     */
    @TableField(value = "opportunityproductname")
    @JSONField(name = "opportunityproductname")
    @JsonProperty("opportunityproductname")
    private String opportunityproductname;
    /**
     * 属性配置
     */
    @TableField(value = "propertyconfigurationstatus")
    @JSONField(name = "propertyconfigurationstatus")
    @JsonProperty("propertyconfigurationstatus")
    private String propertyconfigurationstatus;
    /**
     * 税
     */
    @TableField(value = "tax")
    @JSONField(name = "tax")
    @JsonProperty("tax")
    private BigDecimal tax;
    /**
     * 单价
     */
    @TableField(value = "priceperunit")
    @JSONField(name = "priceperunit")
    @JsonProperty("priceperunit")
    private BigDecimal priceperunit;
    /**
     * 父捆绑销售
     */
    @TableField(value = "parentbundleid")
    @JSONField(name = "parentbundleid")
    @JsonProperty("parentbundleid")
    private String parentbundleid;
    /**
     * UTC Conversion Time Zone Code
     */
    @TableField(value = "utcconversiontimezonecode")
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;
    /**
     * 捆绑销售项关联
     */
    @TableField(value = "productassociationid")
    @JSONField(name = "productassociationid")
    @JsonProperty("productassociationid")
    private String productassociationid;
    /**
     * 金额
     */
    @TableField(value = "baseamount")
    @JSONField(name = "baseamount")
    @JsonProperty("baseamount")
    private BigDecimal baseamount;
    /**
     * 产品类型
     */
    @TableField(value = "producttypecode")
    @JSONField(name = "producttypecode")
    @JsonProperty("producttypecode")
    private String producttypecode;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * Time Zone Rule Version Number
     */
    @TableField(value = "timezoneruleversionnumber")
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;
    /**
     * 数量
     */
    @TableField(value = "quantity")
    @JSONField(name = "quantity")
    @JsonProperty("quantity")
    private BigDecimal quantity;
    /**
     * 明细项目编号
     */
    @TableField(value = "lineitemnumber")
    @JSONField(name = "lineitemnumber")
    @JsonProperty("lineitemnumber")
    private Integer lineitemnumber;
    /**
     * Record Created On
     */
    @TableField(value = "overriddencreatedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 应收净额
     */
    @TableField(value = "extendedamount")
    @JSONField(name = "extendedamount")
    @JsonProperty("extendedamount")
    private BigDecimal extendedamount;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * EntityImage_URL
     */
    @DEField(name = "entityimage_url")
    @TableField(value = "entityimage_url")
    @JSONField(name = "entityimage_url")
    @JsonProperty("entityimage_url")
    private String entityimageUrl;
    /**
     * 定价错误
     */
    @TableField(value = "pricingerrorcode")
    @JSONField(name = "pricingerrorcode")
    @JsonProperty("pricingerrorcode")
    private String pricingerrorcode;
    /**
     * 实体图像
     */
    @TableField(value = "entityimage")
    @JSONField(name = "entityimage")
    @JsonProperty("entityimage")
    private String entityimage;
    /**
     * 金额 (Base)
     */
    @DEField(name = "baseamount_base")
    @TableField(value = "baseamount_base")
    @JSONField(name = "baseamount_base")
    @JsonProperty("baseamount_base")
    private BigDecimal baseamountBase;
    /**
     * Import Sequence Number
     */
    @TableField(value = "importsequencenumber")
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;
    /**
     * 现有产品
     */
    @TableField(value = "productid")
    @JSONField(name = "productid")
    @JsonProperty("productid")
    private String productid;
    /**
     * Parent bundle product
     */
    @TableField(value = "parentbundleidref")
    @JSONField(name = "parentbundleidref")
    @JsonProperty("parentbundleidref")
    private String parentbundleidref;
    /**
     * 计价单位
     */
    @TableField(value = "uomid")
    @JSONField(name = "uomid")
    @JsonProperty("uomid")
    private String uomid;
    /**
     * 货币
     */
    @TableField(value = "transactioncurrencyid")
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;
    /**
     * 商机
     */
    @TableField(value = "opportunityid")
    @JSONField(name = "opportunityid")
    @JsonProperty("opportunityid")
    private String opportunityid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.sales.domain.OpportunityProduct parentbundleidr;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.sales.domain.Opportunity opportunity;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.product.domain.Product product;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency transactioncurrency;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.Uom uom;



    /**
     * 设置 [自定义的价格]
     */
    public void setPriceoverridden(Integer priceoverridden){
        this.priceoverridden = priceoverridden ;
        this.modify("priceoverridden",priceoverridden);
    }

    /**
     * 设置 [税 (Base)]
     */
    public void setTaxBase(BigDecimal taxBase){
        this.taxBase = taxBase ;
        this.modify("tax_base",taxBase);
    }

    /**
     * 设置 [SkipPriceCalculation]
     */
    public void setSkippricecalculation(String skippricecalculation){
        this.skippricecalculation = skippricecalculation ;
        this.modify("skippricecalculation",skippricecalculation);
    }

    /**
     * 设置 [批发折扣 (Base)]
     */
    public void setVolumediscountamountBase(BigDecimal volumediscountamountBase){
        this.volumediscountamountBase = volumediscountamountBase ;
        this.modify("volumediscountamount_base",volumediscountamountBase);
    }

    /**
     * 设置 [零售折扣金额]
     */
    public void setManualdiscountamount(BigDecimal manualdiscountamount){
        this.manualdiscountamount = manualdiscountamount ;
        this.modify("manualdiscountamount",manualdiscountamount);
    }

    /**
     * 设置 [EntityImage_Timestamp]
     */
    public void setEntityimageTimestamp(BigInteger entityimageTimestamp){
        this.entityimageTimestamp = entityimageTimestamp ;
        this.modify("entityimage_timestamp",entityimageTimestamp);
    }

    /**
     * 设置 [商机状态]
     */
    public void setOpportunitystatecode(String opportunitystatecode){
        this.opportunitystatecode = opportunitystatecode ;
        this.modify("opportunitystatecode",opportunitystatecode);
    }

    /**
     * 设置 [负责人类型]
     */
    public void setOwnertype(String ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [批发折扣]
     */
    public void setVolumediscountamount(BigDecimal volumediscountamount){
        this.volumediscountamount = volumediscountamount ;
        this.modify("volumediscountamount",volumediscountamount);
    }

    /**
     * 设置 [应收净额 (Base)]
     */
    public void setExtendedamountBase(BigDecimal extendedamountBase){
        this.extendedamountBase = extendedamountBase ;
        this.modify("extendedamount_base",extendedamountBase);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnername(String ownername){
        this.ownername = ownername ;
        this.modify("ownername",ownername);
    }

    /**
     * 设置 [单价 (Base)]
     */
    public void setPriceperunitBase(BigDecimal priceperunitBase){
        this.priceperunitBase = priceperunitBase ;
        this.modify("priceperunit_base",priceperunitBase);
    }

    /**
     * 设置 [Version Number]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [EntityImageId]
     */
    public void setEntityimageid(String entityimageid){
        this.entityimageid = entityimageid ;
        this.modify("entityimageid",entityimageid);
    }

    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [零售折扣金额 (Base)]
     */
    public void setManualdiscountamountBase(BigDecimal manualdiscountamountBase){
        this.manualdiscountamountBase = manualdiscountamountBase ;
        this.modify("manualdiscountamount_base",manualdiscountamountBase);
    }

    /**
     * 设置 [序号]
     */
    public void setSequencenumber(Integer sequencenumber){
        this.sequencenumber = sequencenumber ;
        this.modify("sequencenumber",sequencenumber);
    }

    /**
     * 设置 [目录外产品]
     */
    public void setProductdescription(String productdescription){
        this.productdescription = productdescription ;
        this.modify("productdescription",productdescription);
    }

    /**
     * 设置 [选择产品]
     */
    public void setProductoverridden(Integer productoverridden){
        this.productoverridden = productoverridden ;
        this.modify("productoverridden",productoverridden);
    }

    /**
     * 设置 [Product Name]
     */
    public void setProductname(String productname){
        this.productname = productname ;
        this.modify("productname",productname);
    }

    /**
     * 设置 [汇率]
     */
    public void setExchangerate(BigDecimal exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [名称]
     */
    public void setOpportunityproductname(String opportunityproductname){
        this.opportunityproductname = opportunityproductname ;
        this.modify("opportunityproductname",opportunityproductname);
    }

    /**
     * 设置 [属性配置]
     */
    public void setPropertyconfigurationstatus(String propertyconfigurationstatus){
        this.propertyconfigurationstatus = propertyconfigurationstatus ;
        this.modify("propertyconfigurationstatus",propertyconfigurationstatus);
    }

    /**
     * 设置 [税]
     */
    public void setTax(BigDecimal tax){
        this.tax = tax ;
        this.modify("tax",tax);
    }

    /**
     * 设置 [单价]
     */
    public void setPriceperunit(BigDecimal priceperunit){
        this.priceperunit = priceperunit ;
        this.modify("priceperunit",priceperunit);
    }

    /**
     * 设置 [父捆绑销售]
     */
    public void setParentbundleid(String parentbundleid){
        this.parentbundleid = parentbundleid ;
        this.modify("parentbundleid",parentbundleid);
    }

    /**
     * 设置 [UTC Conversion Time Zone Code]
     */
    public void setUtcconversiontimezonecode(Integer utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [捆绑销售项关联]
     */
    public void setProductassociationid(String productassociationid){
        this.productassociationid = productassociationid ;
        this.modify("productassociationid",productassociationid);
    }

    /**
     * 设置 [金额]
     */
    public void setBaseamount(BigDecimal baseamount){
        this.baseamount = baseamount ;
        this.modify("baseamount",baseamount);
    }

    /**
     * 设置 [产品类型]
     */
    public void setProducttypecode(String producttypecode){
        this.producttypecode = producttypecode ;
        this.modify("producttypecode",producttypecode);
    }

    /**
     * 设置 [Time Zone Rule Version Number]
     */
    public void setTimezoneruleversionnumber(Integer timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [数量]
     */
    public void setQuantity(BigDecimal quantity){
        this.quantity = quantity ;
        this.modify("quantity",quantity);
    }

    /**
     * 设置 [明细项目编号]
     */
    public void setLineitemnumber(Integer lineitemnumber){
        this.lineitemnumber = lineitemnumber ;
        this.modify("lineitemnumber",lineitemnumber);
    }

    /**
     * 设置 [Record Created On]
     */
    public void setOverriddencreatedon(Timestamp overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 格式化日期 [Record Created On]
     */
    public String formatOverriddencreatedon(){
        if (this.overriddencreatedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(overriddencreatedon);
    }
    /**
     * 设置 [应收净额]
     */
    public void setExtendedamount(BigDecimal extendedamount){
        this.extendedamount = extendedamount ;
        this.modify("extendedamount",extendedamount);
    }

    /**
     * 设置 [EntityImage_URL]
     */
    public void setEntityimageUrl(String entityimageUrl){
        this.entityimageUrl = entityimageUrl ;
        this.modify("entityimage_url",entityimageUrl);
    }

    /**
     * 设置 [定价错误]
     */
    public void setPricingerrorcode(String pricingerrorcode){
        this.pricingerrorcode = pricingerrorcode ;
        this.modify("pricingerrorcode",pricingerrorcode);
    }

    /**
     * 设置 [实体图像]
     */
    public void setEntityimage(String entityimage){
        this.entityimage = entityimage ;
        this.modify("entityimage",entityimage);
    }

    /**
     * 设置 [金额 (Base)]
     */
    public void setBaseamountBase(BigDecimal baseamountBase){
        this.baseamountBase = baseamountBase ;
        this.modify("baseamount_base",baseamountBase);
    }

    /**
     * 设置 [Import Sequence Number]
     */
    public void setImportsequencenumber(Integer importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [现有产品]
     */
    public void setProductid(String productid){
        this.productid = productid ;
        this.modify("productid",productid);
    }

    /**
     * 设置 [Parent bundle product]
     */
    public void setParentbundleidref(String parentbundleidref){
        this.parentbundleidref = parentbundleidref ;
        this.modify("parentbundleidref",parentbundleidref);
    }

    /**
     * 设置 [计价单位]
     */
    public void setUomid(String uomid){
        this.uomid = uomid ;
        this.modify("uomid",uomid);
    }

    /**
     * 设置 [货币]
     */
    public void setTransactioncurrencyid(String transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }

    /**
     * 设置 [商机]
     */
    public void setOpportunityid(String opportunityid){
        this.opportunityid = opportunityid ;
        this.modify("opportunityid",opportunityid);
    }


}


