package cn.ibizlab.businesscentral.core.base.service.logic;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.businesscentral.core.base.domain.Contact;

/**
 * 关系型数据实体[Inactive] 对象
 */
public interface IContactInactiveLogic {

    void execute(Contact et) ;

}
