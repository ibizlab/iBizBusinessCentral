package cn.ibizlab.businesscentral.core.finance.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.finance.domain.QuarterlyFiscalCalendar;
import cn.ibizlab.businesscentral.core.finance.filter.QuarterlyFiscalCalendarSearchContext;
import cn.ibizlab.businesscentral.core.finance.service.IQuarterlyFiscalCalendarService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.finance.mapper.QuarterlyFiscalCalendarMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[季度会计日历] 服务对象接口实现
 */
@Slf4j
@Service("QuarterlyFiscalCalendarServiceImpl")
public class QuarterlyFiscalCalendarServiceImpl extends ServiceImpl<QuarterlyFiscalCalendarMapper, QuarterlyFiscalCalendar> implements IQuarterlyFiscalCalendarService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITransactionCurrencyService transactioncurrencyService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(QuarterlyFiscalCalendar et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getUserfiscalcalendarid()),et);
        return true;
    }

    @Override
    public void createBatch(List<QuarterlyFiscalCalendar> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(QuarterlyFiscalCalendar et) {
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("userfiscalcalendarid",et.getUserfiscalcalendarid())))
            return false;
        CachedBeanCopier.copy(get(et.getUserfiscalcalendarid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<QuarterlyFiscalCalendar> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public QuarterlyFiscalCalendar get(String key) {
        QuarterlyFiscalCalendar et = getById(key);
        if(et==null){
            et=new QuarterlyFiscalCalendar();
            et.setUserfiscalcalendarid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public QuarterlyFiscalCalendar getDraft(QuarterlyFiscalCalendar et) {
        return et;
    }

    @Override
    public boolean checkKey(QuarterlyFiscalCalendar et) {
        return (!ObjectUtils.isEmpty(et.getUserfiscalcalendarid()))&&(!Objects.isNull(this.getById(et.getUserfiscalcalendarid())));
    }
    @Override
    @Transactional
    public boolean save(QuarterlyFiscalCalendar et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(QuarterlyFiscalCalendar et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<QuarterlyFiscalCalendar> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<QuarterlyFiscalCalendar> list) {
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<QuarterlyFiscalCalendar> selectByTransactioncurrencyid(String transactioncurrencyid) {
        return baseMapper.selectByTransactioncurrencyid(transactioncurrencyid);
    }

    @Override
    public void removeByTransactioncurrencyid(String transactioncurrencyid) {
        this.remove(new QueryWrapper<QuarterlyFiscalCalendar>().eq("transactioncurrencyid",transactioncurrencyid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<QuarterlyFiscalCalendar> searchDefault(QuarterlyFiscalCalendarSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<QuarterlyFiscalCalendar> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<QuarterlyFiscalCalendar>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<QuarterlyFiscalCalendar> getQuarterlyfiscalcalendarByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<QuarterlyFiscalCalendar> getQuarterlyfiscalcalendarByEntities(List<QuarterlyFiscalCalendar> entities) {
        List ids =new ArrayList();
        for(QuarterlyFiscalCalendar entity : entities){
            Serializable id=entity.getUserfiscalcalendarid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}



