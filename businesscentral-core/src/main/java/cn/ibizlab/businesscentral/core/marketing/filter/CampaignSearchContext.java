package cn.ibizlab.businesscentral.core.marketing.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.marketing.domain.Campaign;
/**
 * 关系型数据实体[Campaign] 查询条件对象
 */
@Slf4j
@Data
public class CampaignSearchContext extends QueryWrapperContext<Campaign> {

	private String n_typecode_eq;//[活动类型]
	public void setN_typecode_eq(String n_typecode_eq) {
        this.n_typecode_eq = n_typecode_eq;
        if(!ObjectUtils.isEmpty(this.n_typecode_eq)){
            this.getSearchCond().eq("typecode", n_typecode_eq);
        }
    }
	private String n_campaignname_like;//[活动名称]
	public void setN_campaignname_like(String n_campaignname_like) {
        this.n_campaignname_like = n_campaignname_like;
        if(!ObjectUtils.isEmpty(this.n_campaignname_like)){
            this.getSearchCond().like("campaignname", n_campaignname_like);
        }
    }
	private Integer n_statecode_eq;//[状态]
	public void setN_statecode_eq(Integer n_statecode_eq) {
        this.n_statecode_eq = n_statecode_eq;
        if(!ObjectUtils.isEmpty(this.n_statecode_eq)){
            this.getSearchCond().eq("statecode", n_statecode_eq);
        }
    }
	private Integer n_statuscode_eq;//[状态描述]
	public void setN_statuscode_eq(Integer n_statuscode_eq) {
        this.n_statuscode_eq = n_statuscode_eq;
        if(!ObjectUtils.isEmpty(this.n_statuscode_eq)){
            this.getSearchCond().eq("statuscode", n_statuscode_eq);
        }
    }
	private String n_currencyname_eq;//[货币]
	public void setN_currencyname_eq(String n_currencyname_eq) {
        this.n_currencyname_eq = n_currencyname_eq;
        if(!ObjectUtils.isEmpty(this.n_currencyname_eq)){
            this.getSearchCond().eq("currencyname", n_currencyname_eq);
        }
    }
	private String n_currencyname_like;//[货币]
	public void setN_currencyname_like(String n_currencyname_like) {
        this.n_currencyname_like = n_currencyname_like;
        if(!ObjectUtils.isEmpty(this.n_currencyname_like)){
            this.getSearchCond().like("currencyname", n_currencyname_like);
        }
    }
	private String n_pricelistid_eq;//[价目表]
	public void setN_pricelistid_eq(String n_pricelistid_eq) {
        this.n_pricelistid_eq = n_pricelistid_eq;
        if(!ObjectUtils.isEmpty(this.n_pricelistid_eq)){
            this.getSearchCond().eq("pricelistid", n_pricelistid_eq);
        }
    }
	private String n_transactioncurrencyid_eq;//[货币]
	public void setN_transactioncurrencyid_eq(String n_transactioncurrencyid_eq) {
        this.n_transactioncurrencyid_eq = n_transactioncurrencyid_eq;
        if(!ObjectUtils.isEmpty(this.n_transactioncurrencyid_eq)){
            this.getSearchCond().eq("transactioncurrencyid", n_transactioncurrencyid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("campaignname", query)   
            );
		 }
	}
}



