package cn.ibizlab.businesscentral.core.website.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.website.domain.WebSiteContent;
import cn.ibizlab.businesscentral.core.website.filter.WebSiteContentSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface WebSiteContentMapper extends BaseMapper<WebSiteContent>{

    Page<WebSiteContent> searchDefault(IPage page, @Param("srf") WebSiteContentSearchContext context, @Param("ew") Wrapper<WebSiteContent> wrapper) ;
    @Override
    WebSiteContent selectById(Serializable id);
    @Override
    int insert(WebSiteContent entity);
    @Override
    int updateById(@Param(Constants.ENTITY) WebSiteContent entity);
    @Override
    int update(@Param(Constants.ENTITY) WebSiteContent entity, @Param("ew") Wrapper<WebSiteContent> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<WebSiteContent> selectByWebsitechannelid(@Param("websitechannelid") Serializable websitechannelid) ;

    List<WebSiteContent> selectByWebsiteid(@Param("websiteid") Serializable websiteid) ;

}
