package cn.ibizlab.businesscentral.core.service.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.service.domain.IncidentreSolution;
import cn.ibizlab.businesscentral.core.service.filter.IncidentreSolutionSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[IncidentreSolution] 服务对象接口
 */
public interface IIncidentreSolutionService extends IService<IncidentreSolution>{

    boolean create(IncidentreSolution et) ;
    void createBatch(List<IncidentreSolution> list) ;
    boolean update(IncidentreSolution et) ;
    void updateBatch(List<IncidentreSolution> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    IncidentreSolution get(String key) ;
    IncidentreSolution getDraft(IncidentreSolution et) ;
    boolean checkKey(IncidentreSolution et) ;
    boolean save(IncidentreSolution et) ;
    void saveBatch(List<IncidentreSolution> list) ;
    Page<IncidentreSolution> searchDefault(IncidentreSolutionSearchContext context) ;
    List<IncidentreSolution> selectByIncidentid(String incidentid) ;
    void removeByIncidentid(String incidentid) ;
    List<IncidentreSolution> selectByServiceid(String serviceid) ;
    void removeByServiceid(String serviceid) ;
    List<IncidentreSolution> selectBySlaid(String slaid) ;
    void removeBySlaid(String slaid) ;
    List<IncidentreSolution> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<IncidentreSolution> getIncidentresolutionByIds(List<String> ids) ;
    List<IncidentreSolution> getIncidentresolutionByEntities(List<IncidentreSolution> entities) ;
}


