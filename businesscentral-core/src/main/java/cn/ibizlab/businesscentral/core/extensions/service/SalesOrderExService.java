package cn.ibizlab.businesscentral.core.extensions.service;

import cn.ibizlab.businesscentral.core.sales.service.impl.SalesOrderServiceImpl;
import lombok.extern.slf4j.Slf4j;
import cn.ibizlab.businesscentral.core.sales.domain.SalesOrder;
import org.springframework.stereotype.Service;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Primary;
import java.util.*;

/**
 * 实体[订单] 自定义服务对象
 */
@Slf4j
@Primary
@Service("SalesOrderExService")
public class SalesOrderExService extends SalesOrderServiceImpl {

    @Override
    protected Class currentModelClass() {
        return com.baomidou.mybatisplus.core.toolkit.ReflectionKit.getSuperClassGenericType(this.getClass().getSuperclass(), 1);
    }

    /**
     * 自定义行为[GenInvoice]用户扩展
     * @param et
     * @return
     */
    @Override
    @Transactional
    public SalesOrder genInvoice(SalesOrder et) {
        return super.genInvoice(et);
    }
}

