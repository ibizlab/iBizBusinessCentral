package cn.ibizlab.businesscentral.core.base.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[公告]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "BUSINESSUNITNEWSARTICLE",resultMap = "BusinessUnitNewsArticleResultMap")
public class BusinessUnitNewsArticle extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 公告
     */
    @DEField(isKeyField=true)
    @TableId(value= "businessunitnewsarticleid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "businessunitnewsarticleid")
    @JsonProperty("businessunitnewsarticleid")
    private String businessunitnewsarticleid;
    /**
     * 详细信息 URL
     */
    @TableField(value = "articleurl")
    @JSONField(name = "articleurl")
    @JsonProperty("articleurl")
    private String articleurl;
    /**
     * 创建记录的时间
     */
    @TableField(value = "overriddencreatedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;
    /**
     * 说明
     */
    @TableField(value = "newsarticle")
    @JSONField(name = "newsarticle")
    @JsonProperty("newsarticle")
    private String newsarticle;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 仅供内部使用。
     */
    @TableField(value = "timezoneruleversionnumber")
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;
    /**
     * 创建记录时所使用的时区代码。
     */
    @TableField(value = "utcconversiontimezonecode")
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;
    /**
     * 在工作区中显示
     */
    @DEField(defaultValue = "0")
    @TableField(value = "showonhomepage")
    @JSONField(name = "showonhomepage")
    @JsonProperty("showonhomepage")
    private Integer showonhomepage;
    /**
     * VersionNumber
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * 到期日期
     */
    @TableField(value = "activeuntil")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activeuntil" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("activeuntil")
    private Timestamp activeuntil;
    /**
     * 标题
     */
    @TableField(value = "articletitle")
    @JSONField(name = "articletitle")
    @JsonProperty("articletitle")
    private String articletitle;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 公开对象
     */
    @TableField(value = "articletypecode")
    @JSONField(name = "articletypecode")
    @JsonProperty("articletypecode")
    private String articletypecode;
    /**
     * 导入序列号
     */
    @TableField(value = "importsequencenumber")
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 可用时间
     */
    @TableField(value = "activeon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activeon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("activeon")
    private Timestamp activeon;



    /**
     * 设置 [详细信息 URL]
     */
    public void setArticleurl(String articleurl){
        this.articleurl = articleurl ;
        this.modify("articleurl",articleurl);
    }

    /**
     * 设置 [创建记录的时间]
     */
    public void setOverriddencreatedon(Timestamp overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 格式化日期 [创建记录的时间]
     */
    public String formatOverriddencreatedon(){
        if (this.overriddencreatedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(overriddencreatedon);
    }
    /**
     * 设置 [说明]
     */
    public void setNewsarticle(String newsarticle){
        this.newsarticle = newsarticle ;
        this.modify("newsarticle",newsarticle);
    }

    /**
     * 设置 [仅供内部使用。]
     */
    public void setTimezoneruleversionnumber(Integer timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [创建记录时所使用的时区代码。]
     */
    public void setUtcconversiontimezonecode(Integer utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [在工作区中显示]
     */
    public void setShowonhomepage(Integer showonhomepage){
        this.showonhomepage = showonhomepage ;
        this.modify("showonhomepage",showonhomepage);
    }

    /**
     * 设置 [VersionNumber]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [到期日期]
     */
    public void setActiveuntil(Timestamp activeuntil){
        this.activeuntil = activeuntil ;
        this.modify("activeuntil",activeuntil);
    }

    /**
     * 格式化日期 [到期日期]
     */
    public String formatActiveuntil(){
        if (this.activeuntil == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(activeuntil);
    }
    /**
     * 设置 [标题]
     */
    public void setArticletitle(String articletitle){
        this.articletitle = articletitle ;
        this.modify("articletitle",articletitle);
    }

    /**
     * 设置 [公开对象]
     */
    public void setArticletypecode(String articletypecode){
        this.articletypecode = articletypecode ;
        this.modify("articletypecode",articletypecode);
    }

    /**
     * 设置 [导入序列号]
     */
    public void setImportsequencenumber(Integer importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [可用时间]
     */
    public void setActiveon(Timestamp activeon){
        this.activeon = activeon ;
        this.modify("activeon",activeon);
    }

    /**
     * 格式化日期 [可用时间]
     */
    public String formatActiveon(){
        if (this.activeon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(activeon);
    }

}


