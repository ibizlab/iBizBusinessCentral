package cn.ibizlab.businesscentral.core.service.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.service.domain.Entitlement;
import cn.ibizlab.businesscentral.core.service.filter.EntitlementSearchContext;
import cn.ibizlab.businesscentral.core.service.service.IEntitlementService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.service.mapper.EntitlementMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[权利] 服务对象接口实现
 */
@Slf4j
@Service("EntitlementServiceImpl")
public class EntitlementServiceImpl extends ServiceImpl<EntitlementMapper, Entitlement> implements IEntitlementService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IEntitlementChannelService entitlementchannelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IIncidentService incidentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IEntitlementTemplateService entitlementtemplateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ISlaService slaService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITransactionCurrencyService transactioncurrencyService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(Entitlement et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getEntitlementid()),et);
        return true;
    }

    @Override
    public void createBatch(List<Entitlement> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Entitlement et) {
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("entitlementid",et.getEntitlementid())))
            return false;
        CachedBeanCopier.copy(get(et.getEntitlementid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<Entitlement> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Entitlement get(String key) {
        Entitlement et = getById(key);
        if(et==null){
            et=new Entitlement();
            et.setEntitlementid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Entitlement getDraft(Entitlement et) {
        return et;
    }

    @Override
    public boolean checkKey(Entitlement et) {
        return (!ObjectUtils.isEmpty(et.getEntitlementid()))&&(!Objects.isNull(this.getById(et.getEntitlementid())));
    }
    @Override
    @Transactional
    public boolean save(Entitlement et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Entitlement et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<Entitlement> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<Entitlement> list) {
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Entitlement> selectByEntitlementtemplateid(String entitlementtemplateid) {
        return baseMapper.selectByEntitlementtemplateid(entitlementtemplateid);
    }

    @Override
    public void removeByEntitlementtemplateid(String entitlementtemplateid) {
        this.remove(new QueryWrapper<Entitlement>().eq("entitlementtemplateid",entitlementtemplateid));
    }

	@Override
    public List<Entitlement> selectBySlaid(String slaid) {
        return baseMapper.selectBySlaid(slaid);
    }

    @Override
    public void removeBySlaid(String slaid) {
        this.remove(new QueryWrapper<Entitlement>().eq("slaid",slaid));
    }

	@Override
    public List<Entitlement> selectByTransactioncurrencyid(String transactioncurrencyid) {
        return baseMapper.selectByTransactioncurrencyid(transactioncurrencyid);
    }

    @Override
    public void removeByTransactioncurrencyid(String transactioncurrencyid) {
        this.remove(new QueryWrapper<Entitlement>().eq("transactioncurrencyid",transactioncurrencyid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<Entitlement> searchDefault(EntitlementSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Entitlement> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Entitlement>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Entitlement> getEntitlementByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Entitlement> getEntitlementByEntities(List<Entitlement> entities) {
        List ids =new ArrayList();
        for(Entitlement entity : entities){
            Serializable id=entity.getEntitlementid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}



