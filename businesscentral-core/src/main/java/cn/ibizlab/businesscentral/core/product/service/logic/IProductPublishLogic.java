package cn.ibizlab.businesscentral.core.product.service.logic;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.businesscentral.core.product.domain.Product;

/**
 * 关系型数据实体[Publish] 对象
 */
public interface IProductPublishLogic {

    void execute(Product et) ;

}
