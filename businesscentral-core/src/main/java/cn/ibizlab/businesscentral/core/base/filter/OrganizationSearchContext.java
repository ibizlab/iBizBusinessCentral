package cn.ibizlab.businesscentral.core.base.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.base.domain.Organization;
/**
 * 关系型数据实体[Organization] 查询条件对象
 */
@Slf4j
@Data
public class OrganizationSearchContext extends QueryWrapperContext<Organization> {

	private String n_organizationname_like;//[组织名称]
	public void setN_organizationname_like(String n_organizationname_like) {
        this.n_organizationname_like = n_organizationname_like;
        if(!ObjectUtils.isEmpty(this.n_organizationname_like)){
            this.getSearchCond().like("organizationname", n_organizationname_like);
        }
    }
	private String n_organizationtype_eq;//[组织类型]
	public void setN_organizationtype_eq(String n_organizationtype_eq) {
        this.n_organizationtype_eq = n_organizationtype_eq;
        if(!ObjectUtils.isEmpty(this.n_organizationtype_eq)){
            this.getSearchCond().eq("organizationtype", n_organizationtype_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("organizationname", query)   
            );
		 }
	}
}



