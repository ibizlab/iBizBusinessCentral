

package cn.ibizlab.businesscentral.core.marketing.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.marketing.domain.CampaignActivity;
import cn.ibizlab.businesscentral.core.base.domain.ActivityPointer;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface CampaignActivityInheritMapping {

    @Mappings({
        @Mapping(source ="activityid",target = "activityid"),
        @Mapping(source ="subject",target = "subject"),
        @Mapping(target ="focusNull",ignore = true),
    })
    ActivityPointer toActivitypointer(CampaignActivity campaignactivity);

    @Mappings({
        @Mapping(source ="activityid" ,target = "activityid"),
        @Mapping(source ="subject" ,target = "subject"),
        @Mapping(target ="focusNull",ignore = true),
    })
    CampaignActivity toCampaignactivity(ActivityPointer activitypointer);

    List<ActivityPointer> toActivitypointer(List<CampaignActivity> campaignactivity);

    List<CampaignActivity> toCampaignactivity(List<ActivityPointer> activitypointer);

}


