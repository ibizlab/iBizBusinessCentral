package cn.ibizlab.businesscentral.core.product.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[产品关联]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "PRODUCTASSOCIATION",resultMap = "ProductAssociationResultMap")
public class ProductAssociation extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 数量
     */
    @TableField(value = "quantity")
    @JSONField(name = "quantity")
    @JsonProperty("quantity")
    private BigDecimal quantity;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * UTC Conversion Time Zone Code
     */
    @TableField(value = "utcconversiontimezonecode")
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;
    /**
     * Version Number
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * 属性自定义
     */
    @TableField(value = "propertycustomizationstatus")
    @JSONField(name = "propertycustomizationstatus")
    @JsonProperty("propertycustomizationstatus")
    private String propertycustomizationstatus;
    /**
     * Import Sequence Number
     */
    @TableField(value = "importsequencenumber")
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;
    /**
     * Time Zone Rule Version Number
     */
    @TableField(value = "timezoneruleversionnumber")
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;
    /**
     * 必填
     */
    @TableField(value = "productisrequired")
    @JSONField(name = "productisrequired")
    @JsonProperty("productisrequired")
    private String productisrequired;
    /**
     * 仅供内部使用
     */
    @TableField(value = "dmtimportstate")
    @JSONField(name = "dmtimportstate")
    @JsonProperty("dmtimportstate")
    private Integer dmtimportstate;
    /**
     * 状态描述
     */
    @TableField(value = "statuscode")
    @JSONField(name = "statuscode")
    @JsonProperty("statuscode")
    private Integer statuscode;
    /**
     * 产品关联 ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "productassociationid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "productassociationid")
    @JsonProperty("productassociationid")
    private String productassociationid;
    /**
     * 汇率
     */
    @TableField(value = "exchangerate")
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;
    /**
     * Record Created On
     */
    @TableField(value = "overriddencreatedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 状态
     */
    @TableField(value = "statecode")
    @JSONField(name = "statecode")
    @JsonProperty("statecode")
    private Integer statecode;
    /**
     * 计量单位
     */
    @TableField(value = "uomname")
    @JSONField(name = "uomname")
    @JsonProperty("uomname")
    private String uomname;
    /**
     * 关联的产品
     */
    @TableField(value = "associatedproductname")
    @JSONField(name = "associatedproductname")
    @JsonProperty("associatedproductname")
    private String associatedproductname;
    /**
     * 货币名称
     */
    @TableField(value = "currencyname")
    @JSONField(name = "currencyname")
    @JsonProperty("currencyname")
    private String currencyname;
    /**
     * 产品名称
     */
    @TableField(value = "productname")
    @JSONField(name = "productname")
    @JsonProperty("productname")
    private String productname;
    /**
     * 产品
     */
    @TableField(value = "productid")
    @JSONField(name = "productid")
    @JsonProperty("productid")
    private String productid;
    /**
     * 计价单位
     */
    @TableField(value = "uomid")
    @JSONField(name = "uomid")
    @JsonProperty("uomid")
    private String uomid;
    /**
     * 货币
     */
    @TableField(value = "transactioncurrencyid")
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;
    /**
     * 关联的产品
     */
    @TableField(value = "associatedproduct")
    @JSONField(name = "associatedproduct")
    @JsonProperty("associatedproduct")
    private String associatedproduct;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.product.domain.Product associatedprodu;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.product.domain.Product product;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency transactioncurrency;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.Uom uom;



    /**
     * 设置 [数量]
     */
    public void setQuantity(BigDecimal quantity){
        this.quantity = quantity ;
        this.modify("quantity",quantity);
    }

    /**
     * 设置 [UTC Conversion Time Zone Code]
     */
    public void setUtcconversiontimezonecode(Integer utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [Version Number]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [属性自定义]
     */
    public void setPropertycustomizationstatus(String propertycustomizationstatus){
        this.propertycustomizationstatus = propertycustomizationstatus ;
        this.modify("propertycustomizationstatus",propertycustomizationstatus);
    }

    /**
     * 设置 [Import Sequence Number]
     */
    public void setImportsequencenumber(Integer importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [Time Zone Rule Version Number]
     */
    public void setTimezoneruleversionnumber(Integer timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [必填]
     */
    public void setProductisrequired(String productisrequired){
        this.productisrequired = productisrequired ;
        this.modify("productisrequired",productisrequired);
    }

    /**
     * 设置 [仅供内部使用]
     */
    public void setDmtimportstate(Integer dmtimportstate){
        this.dmtimportstate = dmtimportstate ;
        this.modify("dmtimportstate",dmtimportstate);
    }

    /**
     * 设置 [状态描述]
     */
    public void setStatuscode(Integer statuscode){
        this.statuscode = statuscode ;
        this.modify("statuscode",statuscode);
    }

    /**
     * 设置 [汇率]
     */
    public void setExchangerate(BigDecimal exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [Record Created On]
     */
    public void setOverriddencreatedon(Timestamp overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 格式化日期 [Record Created On]
     */
    public String formatOverriddencreatedon(){
        if (this.overriddencreatedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(overriddencreatedon);
    }
    /**
     * 设置 [状态]
     */
    public void setStatecode(Integer statecode){
        this.statecode = statecode ;
        this.modify("statecode",statecode);
    }

    /**
     * 设置 [计量单位]
     */
    public void setUomname(String uomname){
        this.uomname = uomname ;
        this.modify("uomname",uomname);
    }

    /**
     * 设置 [关联的产品]
     */
    public void setAssociatedproductname(String associatedproductname){
        this.associatedproductname = associatedproductname ;
        this.modify("associatedproductname",associatedproductname);
    }

    /**
     * 设置 [货币名称]
     */
    public void setCurrencyname(String currencyname){
        this.currencyname = currencyname ;
        this.modify("currencyname",currencyname);
    }

    /**
     * 设置 [产品名称]
     */
    public void setProductname(String productname){
        this.productname = productname ;
        this.modify("productname",productname);
    }

    /**
     * 设置 [产品]
     */
    public void setProductid(String productid){
        this.productid = productid ;
        this.modify("productid",productid);
    }

    /**
     * 设置 [计价单位]
     */
    public void setUomid(String uomid){
        this.uomid = uomid ;
        this.modify("uomid",uomid);
    }

    /**
     * 设置 [货币]
     */
    public void setTransactioncurrencyid(String transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }

    /**
     * 设置 [关联的产品]
     */
    public void setAssociatedproduct(String associatedproduct){
        this.associatedproduct = associatedproduct ;
        this.modify("associatedproduct",associatedproduct);
    }


}


