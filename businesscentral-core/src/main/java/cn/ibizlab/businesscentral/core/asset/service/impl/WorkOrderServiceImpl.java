package cn.ibizlab.businesscentral.core.asset.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.asset.domain.WorkOrder;
import cn.ibizlab.businesscentral.core.asset.filter.WorkOrderSearchContext;
import cn.ibizlab.businesscentral.core.asset.service.IWorkOrderService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.asset.mapper.WorkOrderMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[工单] 服务对象接口实现
 */
@Slf4j
@Service("WorkOrderServiceImpl")
public class WorkOrderServiceImpl extends ServiceImpl<WorkOrderMapper, WorkOrder> implements IWorkOrderService {


    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(WorkOrder et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getWorkorderid()),et);
        return true;
    }

    @Override
    public void createBatch(List<WorkOrder> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(WorkOrder et) {
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("workorderid",et.getWorkorderid())))
            return false;
        CachedBeanCopier.copy(get(et.getWorkorderid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<WorkOrder> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public WorkOrder get(String key) {
        WorkOrder et = getById(key);
        if(et==null){
            et=new WorkOrder();
            et.setWorkorderid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public WorkOrder getDraft(WorkOrder et) {
        return et;
    }

    @Override
    public boolean checkKey(WorkOrder et) {
        return (!ObjectUtils.isEmpty(et.getWorkorderid()))&&(!Objects.isNull(this.getById(et.getWorkorderid())));
    }
    @Override
    @Transactional
    public boolean save(WorkOrder et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(WorkOrder et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<WorkOrder> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<WorkOrder> list) {
        saveOrUpdateBatch(list,batchSize);
    }



    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<WorkOrder> searchDefault(WorkOrderSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<WorkOrder> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<WorkOrder>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<WorkOrder> getWorkorderByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<WorkOrder> getWorkorderByEntities(List<WorkOrder> entities) {
        List ids =new ArrayList();
        for(WorkOrder entity : entities){
            Serializable id=entity.getWorkorderid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}



