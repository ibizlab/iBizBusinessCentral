package cn.ibizlab.businesscentral.core.sales.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.sales.domain.SalesOrderDetail;
import cn.ibizlab.businesscentral.core.sales.filter.SalesOrderDetailSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[SalesOrderDetail] 服务对象接口
 */
public interface ISalesOrderDetailService extends IService<SalesOrderDetail>{

    boolean create(SalesOrderDetail et) ;
    void createBatch(List<SalesOrderDetail> list) ;
    boolean update(SalesOrderDetail et) ;
    void updateBatch(List<SalesOrderDetail> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    SalesOrderDetail get(String key) ;
    SalesOrderDetail getDraft(SalesOrderDetail et) ;
    boolean checkKey(SalesOrderDetail et) ;
    boolean save(SalesOrderDetail et) ;
    void saveBatch(List<SalesOrderDetail> list) ;
    Page<SalesOrderDetail> searchDefault(SalesOrderDetailSearchContext context) ;
    List<SalesOrderDetail> selectByProductid(String productid) ;
    void removeByProductid(String productid) ;
    List<SalesOrderDetail> selectByQuotedetailid(String quotedetailid) ;
    void removeByQuotedetailid(String quotedetailid) ;
    List<SalesOrderDetail> selectByParentbundleidref(String salesorderdetailid) ;
    void removeByParentbundleidref(String salesorderdetailid) ;
    List<SalesOrderDetail> selectBySalesorderid(String salesorderid) ;
    void removeBySalesorderid(String salesorderid) ;
    List<SalesOrderDetail> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    List<SalesOrderDetail> selectByUomid(String uomid) ;
    void removeByUomid(String uomid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<SalesOrderDetail> getSalesorderdetailByIds(List<String> ids) ;
    List<SalesOrderDetail> getSalesorderdetailByEntities(List<SalesOrderDetail> entities) ;
}


