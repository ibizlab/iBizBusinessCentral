package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.OMHierarchyPurpose;
import cn.ibizlab.businesscentral.core.base.filter.OMHierarchyPurposeSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[OMHierarchyPurpose] 服务对象接口
 */
public interface IOMHierarchyPurposeService extends IService<OMHierarchyPurpose>{

    boolean create(OMHierarchyPurpose et) ;
    void createBatch(List<OMHierarchyPurpose> list) ;
    boolean update(OMHierarchyPurpose et) ;
    void updateBatch(List<OMHierarchyPurpose> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    OMHierarchyPurpose get(String key) ;
    OMHierarchyPurpose getDraft(OMHierarchyPurpose et) ;
    boolean checkKey(OMHierarchyPurpose et) ;
    boolean save(OMHierarchyPurpose et) ;
    void saveBatch(List<OMHierarchyPurpose> list) ;
    Page<OMHierarchyPurpose> searchDefault(OMHierarchyPurposeSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<OMHierarchyPurpose> getOmhierarchypurposeByIds(List<String> ids) ;
    List<OMHierarchyPurpose> getOmhierarchypurposeByEntities(List<OMHierarchyPurpose> entities) ;
}


