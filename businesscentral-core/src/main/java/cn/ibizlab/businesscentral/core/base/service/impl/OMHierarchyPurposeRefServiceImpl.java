package cn.ibizlab.businesscentral.core.base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.base.domain.OMHierarchyPurposeRef;
import cn.ibizlab.businesscentral.core.base.filter.OMHierarchyPurposeRefSearchContext;
import cn.ibizlab.businesscentral.core.base.service.IOMHierarchyPurposeRefService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.base.mapper.OMHierarchyPurposeRefMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[组织层次结构分配] 服务对象接口实现
 */
@Slf4j
@Service("OMHierarchyPurposeRefServiceImpl")
public class OMHierarchyPurposeRefServiceImpl extends ServiceImpl<OMHierarchyPurposeRefMapper, OMHierarchyPurposeRef> implements IOMHierarchyPurposeRefService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IOMHierarchyCatService omhierarchycatService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IOMHierarchyPurposeService omhierarchypurposeService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(OMHierarchyPurposeRef et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getOmhierarchypurposerefid()),et);
        return true;
    }

    @Override
    public void createBatch(List<OMHierarchyPurposeRef> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(OMHierarchyPurposeRef et) {
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("omhierarchypurposerefid",et.getOmhierarchypurposerefid())))
            return false;
        CachedBeanCopier.copy(get(et.getOmhierarchypurposerefid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<OMHierarchyPurposeRef> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public OMHierarchyPurposeRef get(String key) {
        OMHierarchyPurposeRef et = getById(key);
        if(et==null){
            et=new OMHierarchyPurposeRef();
            et.setOmhierarchypurposerefid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public OMHierarchyPurposeRef getDraft(OMHierarchyPurposeRef et) {
        return et;
    }

    @Override
    public boolean checkKey(OMHierarchyPurposeRef et) {
        return (!ObjectUtils.isEmpty(et.getOmhierarchypurposerefid()))&&(!Objects.isNull(this.getById(et.getOmhierarchypurposerefid())));
    }
    @Override
    @Transactional
    public boolean save(OMHierarchyPurposeRef et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(OMHierarchyPurposeRef et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<OMHierarchyPurposeRef> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<OMHierarchyPurposeRef> list) {
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<OMHierarchyPurposeRef> selectByOmhierarchycatid(String omhierarchycatid) {
        return baseMapper.selectByOmhierarchycatid(omhierarchycatid);
    }

    @Override
    public void removeByOmhierarchycatid(String omhierarchycatid) {
        this.remove(new QueryWrapper<OMHierarchyPurposeRef>().eq("omhierarchycatid",omhierarchycatid));
    }

	@Override
    public List<OMHierarchyPurposeRef> selectByOmhierarchypurposeid(String omhierarchypurposeid) {
        return baseMapper.selectByOmhierarchypurposeid(omhierarchypurposeid);
    }

    @Override
    public void removeByOmhierarchypurposeid(String omhierarchypurposeid) {
        this.remove(new QueryWrapper<OMHierarchyPurposeRef>().eq("omhierarchypurposeid",omhierarchypurposeid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<OMHierarchyPurposeRef> searchDefault(OMHierarchyPurposeRefSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<OMHierarchyPurposeRef> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<OMHierarchyPurposeRef>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<OMHierarchyPurposeRef> getOmhierarchypurposerefByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<OMHierarchyPurposeRef> getOmhierarchypurposerefByEntities(List<OMHierarchyPurposeRef> entities) {
        List ids =new ArrayList();
        for(OMHierarchyPurposeRef entity : entities){
            Serializable id=entity.getOmhierarchypurposerefid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}



