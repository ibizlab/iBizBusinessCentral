package cn.ibizlab.businesscentral.core.marketing.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.marketing.domain.ListAccount;
import cn.ibizlab.businesscentral.core.marketing.filter.ListAccountSearchContext;
import cn.ibizlab.businesscentral.core.marketing.service.IListAccountService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.marketing.mapper.ListAccountMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[营销列表-账户] 服务对象接口实现
 */
@Slf4j
@Service("ListAccountServiceImpl")
public class ListAccountServiceImpl extends ServiceImpl<ListAccountMapper, ListAccount> implements IListAccountService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IAccountService accountService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.marketing.service.IIBizListService ibizlistService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(ListAccount et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getRelationshipsid()),et);
        return true;
    }

    @Override
    public void createBatch(List<ListAccount> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(ListAccount et) {
        fillParentData(et);
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("relationshipsid",et.getRelationshipsid())))
            return false;
        CachedBeanCopier.copy(get(et.getRelationshipsid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<ListAccount> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public ListAccount get(String key) {
        ListAccount et = getById(key);
        if(et==null){
            et=new ListAccount();
            et.setRelationshipsid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public ListAccount getDraft(ListAccount et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(ListAccount et) {
        return (!ObjectUtils.isEmpty(et.getRelationshipsid()))&&(!Objects.isNull(this.getById(et.getRelationshipsid())));
    }
    @Override
    @Transactional
    public boolean save(ListAccount et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(ListAccount et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<ListAccount> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<ListAccount> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<ListAccount> selectByEntity2id(String accountid) {
        return baseMapper.selectByEntity2id(accountid);
    }

    @Override
    public void removeByEntity2id(String accountid) {
        this.remove(new QueryWrapper<ListAccount>().eq("entity2id",accountid));
    }

	@Override
    public List<ListAccount> selectByEntityid(String listid) {
        return baseMapper.selectByEntityid(listid);
    }

    @Override
    public void removeByEntityid(String listid) {
        this.remove(new QueryWrapper<ListAccount>().eq("entityid",listid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<ListAccount> searchDefault(ListAccountSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<ListAccount> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<ListAccount>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(ListAccount et){
        //实体关系[DER1N_LISTACCOUNT_ACCOUNT_ACCOUNTID]
        if(!ObjectUtils.isEmpty(et.getEntity2id())){
            cn.ibizlab.businesscentral.core.base.domain.Account account=et.getAccount();
            if(ObjectUtils.isEmpty(account)){
                cn.ibizlab.businesscentral.core.base.domain.Account majorEntity=accountService.get(et.getEntity2id());
                et.setAccount(majorEntity);
                account=majorEntity;
            }
            et.setStatecode(account.getStatecode());
            et.setPrimarycontactid(account.getPrimarycontactid());
            et.setTelephone1(account.getTelephone1());
            et.setPrimarycontactname(account.getPrimarycontactname());
            et.setEntity2name(account.getAccountname());
        }
        //实体关系[DER1N_LISTACCOUNT_LIST_LISTID]
        if(!ObjectUtils.isEmpty(et.getEntityid())){
            cn.ibizlab.businesscentral.core.marketing.domain.IBizList list=et.getList();
            if(ObjectUtils.isEmpty(list)){
                cn.ibizlab.businesscentral.core.marketing.domain.IBizList majorEntity=ibizlistService.get(et.getEntityid());
                et.setList(majorEntity);
                list=majorEntity;
            }
            et.setEntityname(list.getListname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<ListAccount> getListaccountByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<ListAccount> getListaccountByEntities(List<ListAccount> entities) {
        List ids =new ArrayList();
        for(ListAccount entity : entities){
            Serializable id=entity.getRelationshipsid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}



