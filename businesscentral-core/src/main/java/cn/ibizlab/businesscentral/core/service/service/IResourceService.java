package cn.ibizlab.businesscentral.core.service.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.service.domain.Resource;
import cn.ibizlab.businesscentral.core.service.filter.ResourceSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Resource] 服务对象接口
 */
public interface IResourceService extends IService<Resource>{

    boolean create(Resource et) ;
    void createBatch(List<Resource> list) ;
    boolean update(Resource et) ;
    void updateBatch(List<Resource> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    Resource get(String key) ;
    Resource getDraft(Resource et) ;
    boolean checkKey(Resource et) ;
    boolean save(Resource et) ;
    void saveBatch(List<Resource> list) ;
    Page<Resource> searchDefault(ResourceSearchContext context) ;
    List<Resource> selectByBusinessunitid(String businessunitid) ;
    void removeByBusinessunitid(String businessunitid) ;
    List<Resource> selectByOrganizationid(String organizationid) ;
    void removeByOrganizationid(String organizationid) ;
    List<Resource> selectBySiteid(String siteid) ;
    void removeBySiteid(String siteid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Resource> getResourceByIds(List<String> ids) ;
    List<Resource> getResourceByEntities(List<Resource> entities) ;
}


