package cn.ibizlab.businesscentral.core.website.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[站点资源]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "WEBSITERESOURCE",resultMap = "WebSiteResourceResultMap")
public class WebSiteResource extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 网站资源标识
     */
    @DEField(isKeyField=true)
    @TableId(value= "websiteresourceid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "websiteresourceid")
    @JsonProperty("websiteresourceid")
    private String websiteresourceid;
    /**
     * 网站资源名称
     */
    @TableField(value = "websiteresourcename")
    @JSONField(name = "websiteresourcename")
    @JsonProperty("websiteresourcename")
    private String websiteresourcename;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 站点
     */
    @TableField(exist = false)
    @JSONField(name = "websitename")
    @JsonProperty("websitename")
    private String websitename;
    /**
     * 网站标识
     */
    @TableField(value = "websiteid")
    @JSONField(name = "websiteid")
    @JsonProperty("websiteid")
    private String websiteid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.website.domain.WebSite website;



    /**
     * 设置 [网站资源名称]
     */
    public void setWebsiteresourcename(String websiteresourcename){
        this.websiteresourcename = websiteresourcename ;
        this.modify("websiteresourcename",websiteresourcename);
    }

    /**
     * 设置 [网站标识]
     */
    public void setWebsiteid(String websiteid){
        this.websiteid = websiteid ;
        this.modify("websiteid",websiteid);
    }


}


