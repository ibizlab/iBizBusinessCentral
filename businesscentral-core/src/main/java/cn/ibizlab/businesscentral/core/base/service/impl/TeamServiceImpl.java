package cn.ibizlab.businesscentral.core.base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.base.domain.Team;
import cn.ibizlab.businesscentral.core.base.filter.TeamSearchContext;
import cn.ibizlab.businesscentral.core.base.service.ITeamService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.base.mapper.TeamMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[团队] 服务对象接口实现
 */
@Slf4j
@Service("TeamServiceImpl")
public class TeamServiceImpl extends ServiceImpl<TeamMapper, Team> implements ITeamService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ISlaService slaService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IBusinessUnitService businessunitService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.runtime.service.IQueueService queueService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITeamTemplateService teamtemplateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITransactionCurrencyService transactioncurrencyService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(Team et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getTeamid()),et);
        return true;
    }

    @Override
    public void createBatch(List<Team> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Team et) {
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("teamid",et.getTeamid())))
            return false;
        CachedBeanCopier.copy(get(et.getTeamid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<Team> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Team get(String key) {
        Team et = getById(key);
        if(et==null){
            et=new Team();
            et.setTeamid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Team getDraft(Team et) {
        return et;
    }

    @Override
    public boolean checkKey(Team et) {
        return (!ObjectUtils.isEmpty(et.getTeamid()))&&(!Objects.isNull(this.getById(et.getTeamid())));
    }
    @Override
    @Transactional
    public boolean save(Team et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Team et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<Team> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<Team> list) {
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Team> selectByBusinessunitid(String businessunitid) {
        return baseMapper.selectByBusinessunitid(businessunitid);
    }

    @Override
    public void removeByBusinessunitid(String businessunitid) {
        this.remove(new QueryWrapper<Team>().eq("businessunitid",businessunitid));
    }

	@Override
    public List<Team> selectByQueueid(String queueid) {
        return baseMapper.selectByQueueid(queueid);
    }

    @Override
    public void removeByQueueid(String queueid) {
        this.remove(new QueryWrapper<Team>().eq("queueid",queueid));
    }

	@Override
    public List<Team> selectByTeamtemplateid(String teamtemplateid) {
        return baseMapper.selectByTeamtemplateid(teamtemplateid);
    }

    @Override
    public void removeByTeamtemplateid(String teamtemplateid) {
        this.remove(new QueryWrapper<Team>().eq("teamtemplateid",teamtemplateid));
    }

	@Override
    public List<Team> selectByTransactioncurrencyid(String transactioncurrencyid) {
        return baseMapper.selectByTransactioncurrencyid(transactioncurrencyid);
    }

    @Override
    public void removeByTransactioncurrencyid(String transactioncurrencyid) {
        this.remove(new QueryWrapper<Team>().eq("transactioncurrencyid",transactioncurrencyid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<Team> searchDefault(TeamSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Team> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Team>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Team> getTeamByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Team> getTeamByEntities(List<Team> entities) {
        List ids =new ArrayList();
        for(Team entity : entities){
            Serializable id=entity.getTeamid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}



