package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.BusinessUnit;
import cn.ibizlab.businesscentral.core.base.filter.BusinessUnitSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[BusinessUnit] 服务对象接口
 */
public interface IBusinessUnitService extends IService<BusinessUnit>{

    boolean create(BusinessUnit et) ;
    void createBatch(List<BusinessUnit> list) ;
    boolean update(BusinessUnit et) ;
    void updateBatch(List<BusinessUnit> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    BusinessUnit get(String key) ;
    BusinessUnit getDraft(BusinessUnit et) ;
    boolean checkKey(BusinessUnit et) ;
    boolean save(BusinessUnit et) ;
    void saveBatch(List<BusinessUnit> list) ;
    Page<BusinessUnit> searchDefault(BusinessUnitSearchContext context) ;
    List<BusinessUnit> selectByParentbusinessunitid(String businessunitid) ;
    void removeByParentbusinessunitid(String businessunitid) ;
    List<BusinessUnit> selectByCalendarid(String calendarid) ;
    void removeByCalendarid(String calendarid) ;
    List<BusinessUnit> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<BusinessUnit> getBusinessunitByIds(List<String> ids) ;
    List<BusinessUnit> getBusinessunitByEntities(List<BusinessUnit> entities) ;
}


