package cn.ibizlab.businesscentral.core.service.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.service.domain.ConstraintBasedGroup;
/**
 * 关系型数据实体[ConstraintBasedGroup] 查询条件对象
 */
@Slf4j
@Data
public class ConstraintBasedGroupSearchContext extends QueryWrapperContext<ConstraintBasedGroup> {

	private String n_constraintbasedgroupname_like;//[约束组名称]
	public void setN_constraintbasedgroupname_like(String n_constraintbasedgroupname_like) {
        this.n_constraintbasedgroupname_like = n_constraintbasedgroupname_like;
        if(!ObjectUtils.isEmpty(this.n_constraintbasedgroupname_like)){
            this.getSearchCond().like("constraintbasedgroupname", n_constraintbasedgroupname_like);
        }
    }
	private String n_grouptypecode_eq;//[组类型]
	public void setN_grouptypecode_eq(String n_grouptypecode_eq) {
        this.n_grouptypecode_eq = n_grouptypecode_eq;
        if(!ObjectUtils.isEmpty(this.n_grouptypecode_eq)){
            this.getSearchCond().eq("grouptypecode", n_grouptypecode_eq);
        }
    }
	private String n_businessunitid_eq;//[业务部门]
	public void setN_businessunitid_eq(String n_businessunitid_eq) {
        this.n_businessunitid_eq = n_businessunitid_eq;
        if(!ObjectUtils.isEmpty(this.n_businessunitid_eq)){
            this.getSearchCond().eq("businessunitid", n_businessunitid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("constraintbasedgroupname", query)   
            );
		 }
	}
}



