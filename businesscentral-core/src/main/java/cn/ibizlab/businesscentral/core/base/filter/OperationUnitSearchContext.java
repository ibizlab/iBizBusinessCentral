package cn.ibizlab.businesscentral.core.base.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.base.domain.OperationUnit;
/**
 * 关系型数据实体[OperationUnit] 查询条件对象
 */
@Slf4j
@Data
public class OperationUnitSearchContext extends QueryWrapperContext<OperationUnit> {

	private String n_operationunitname_like;//[运营单位名称]
	public void setN_operationunitname_like(String n_operationunitname_like) {
        this.n_operationunitname_like = n_operationunitname_like;
        if(!ObjectUtils.isEmpty(this.n_operationunitname_like)){
            this.getSearchCond().like("operationunitname", n_operationunitname_like);
        }
    }
	private String n_operationunittype_eq;//[运营单位类型]
	public void setN_operationunittype_eq(String n_operationunittype_eq) {
        this.n_operationunittype_eq = n_operationunittype_eq;
        if(!ObjectUtils.isEmpty(this.n_operationunittype_eq)){
            this.getSearchCond().eq("operationunittype", n_operationunittype_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("operationunitname", query)   
            );
		 }
	}
}



