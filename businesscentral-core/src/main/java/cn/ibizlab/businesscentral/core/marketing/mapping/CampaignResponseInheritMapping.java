

package cn.ibizlab.businesscentral.core.marketing.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.marketing.domain.CampaignResponse;
import cn.ibizlab.businesscentral.core.base.domain.ActivityPointer;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface CampaignResponseInheritMapping {

    @Mappings({
        @Mapping(source ="activityid",target = "activityid"),
        @Mapping(source ="subject",target = "subject"),
        @Mapping(target ="focusNull",ignore = true),
    })
    ActivityPointer toActivitypointer(CampaignResponse campaignresponse);

    @Mappings({
        @Mapping(source ="activityid" ,target = "activityid"),
        @Mapping(source ="subject" ,target = "subject"),
        @Mapping(target ="focusNull",ignore = true),
    })
    CampaignResponse toCampaignresponse(ActivityPointer activitypointer);

    List<ActivityPointer> toActivitypointer(List<CampaignResponse> campaignresponse);

    List<CampaignResponse> toCampaignresponse(List<ActivityPointer> activitypointer);

}


