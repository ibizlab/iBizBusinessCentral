package cn.ibizlab.businesscentral.core.finance.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.finance.domain.MonthlyFiscalCalendar;
import cn.ibizlab.businesscentral.core.finance.filter.MonthlyFiscalCalendarSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[MonthlyFiscalCalendar] 服务对象接口
 */
public interface IMonthlyFiscalCalendarService extends IService<MonthlyFiscalCalendar>{

    boolean create(MonthlyFiscalCalendar et) ;
    void createBatch(List<MonthlyFiscalCalendar> list) ;
    boolean update(MonthlyFiscalCalendar et) ;
    void updateBatch(List<MonthlyFiscalCalendar> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    MonthlyFiscalCalendar get(String key) ;
    MonthlyFiscalCalendar getDraft(MonthlyFiscalCalendar et) ;
    boolean checkKey(MonthlyFiscalCalendar et) ;
    boolean save(MonthlyFiscalCalendar et) ;
    void saveBatch(List<MonthlyFiscalCalendar> list) ;
    Page<MonthlyFiscalCalendar> searchDefault(MonthlyFiscalCalendarSearchContext context) ;
    List<MonthlyFiscalCalendar> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<MonthlyFiscalCalendar> getMonthlyfiscalcalendarByIds(List<String> ids) ;
    List<MonthlyFiscalCalendar> getMonthlyfiscalcalendarByEntities(List<MonthlyFiscalCalendar> entities) ;
}


