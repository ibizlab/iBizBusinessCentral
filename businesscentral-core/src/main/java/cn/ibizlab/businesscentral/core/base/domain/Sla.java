package cn.ibizlab.businesscentral.core.base.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[SLA]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SLA",resultMap = "SlaResultMap")
public class Sla extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 工作流 ID
     */
    @TableField(value = "workflowid")
    @JSONField(name = "workflowid")
    @JsonProperty("workflowid")
    private String workflowid;
    /**
     * SLA
     */
    @DEField(isKeyField=true)
    @TableId(value= "slaid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "slaid")
    @JsonProperty("slaid")
    private String slaid;
    /**
     * 组件状态
     */
    @TableField(value = "componentstate")
    @JSONField(name = "componentstate")
    @JsonProperty("componentstate")
    private String componentstate;
    /**
     * 适用来源
     */
    @TableField(value = "applicablefrom")
    @JSONField(name = "applicablefrom")
    @JsonProperty("applicablefrom")
    private String applicablefrom;
    /**
     * 状态
     */
    @TableField(value = "statecode")
    @JSONField(name = "statecode")
    @JsonProperty("statecode")
    private Integer statecode;
    /**
     * 是否默认
     */
    @DEField(defaultValue = "0")
    @TableField(value = "default")
    @JSONField(name = "ibizdefault")
    @JsonProperty("ibizdefault")
    private Integer ibizdefault;
    /**
     * 说明
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 适用来源
     */
    @TableField(value = "applicablefrompicklist")
    @JSONField(name = "applicablefrompicklist")
    @JsonProperty("applicablefrompicklist")
    private String applicablefrompicklist;
    /**
     * 负责人
     */
    @TableField(value = "ownername")
    @JSONField(name = "ownername")
    @JsonProperty("ownername")
    private String ownername;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * SLA 类型
     */
    @TableField(value = "slatype")
    @JSONField(name = "slatype")
    @JsonProperty("slatype")
    private String slatype;
    /**
     * 负责人类型
     */
    @TableField(value = "ownertype")
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;
    /**
     * 版本号
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * WorkflowIdName
     */
    @TableField(value = "workflowidname")
    @JSONField(name = "workflowidname")
    @JsonProperty("workflowidname")
    private String workflowidname;
    /**
     * 解决方案
     */
    @TableField(value = "supportingsolutionid")
    @JSONField(name = "supportingsolutionid")
    @JsonProperty("supportingsolutionid")
    private String supportingsolutionid;
    /**
     * 汇率
     */
    @TableField(value = "exchangerate")
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;
    /**
     * ChangedAttributeList
     */
    @TableField(value = "changedattributelist")
    @JSONField(name = "changedattributelist")
    @JsonProperty("changedattributelist")
    private String changedattributelist;
    /**
     * 状态描述
     */
    @TableField(value = "statuscode")
    @JSONField(name = "statuscode")
    @JsonProperty("statuscode")
    private Integer statuscode;
    /**
     * 解决方案
     */
    @TableField(value = "solutionid")
    @JSONField(name = "solutionid")
    @JsonProperty("solutionid")
    private String solutionid;
    /**
     * 托管
     */
    @DEField(defaultValue = "0")
    @TableField(value = "managed")
    @JSONField(name = "managed")
    @JsonProperty("managed")
    private Integer managed;
    /**
     * 允许暂停和恢复
     */
    @DEField(defaultValue = "0")
    @TableField(value = "allowpauseresume")
    @JSONField(name = "allowpauseresume")
    @JsonProperty("allowpauseresume")
    private Integer allowpauseresume;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 主要实体
     */
    @TableField(value = "primaryentityotc")
    @JSONField(name = "primaryentityotc")
    @JsonProperty("primaryentityotc")
    private Integer primaryentityotc;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 对象类型代码
     */
    @TableField(value = "objecttypecode")
    @JSONField(name = "objecttypecode")
    @JsonProperty("objecttypecode")
    private String objecttypecode;
    /**
     * 服务协议名称
     */
    @TableField(value = "slaname")
    @JSONField(name = "slaname")
    @JsonProperty("slaname")
    private String slaname;
    /**
     * 记录替代时间
     */
    @TableField(value = "overwritetime")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overwritetime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overwritetime")
    private Timestamp overwritetime;
    /**
     * 负责人用户
     */
    @TableField(value = "owninguser")
    @JSONField(name = "owninguser")
    @JsonProperty("owninguser")
    private String owninguser;
    /**
     * 负责人
     */
    @TableField(value = "ownerid")
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;
    /**
     * 唯一 ID
     */
    @TableField(value = "slaidunique")
    @JSONField(name = "slaidunique")
    @JsonProperty("slaidunique")
    private String slaidunique;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 负责团队
     */
    @TableField(value = "owningteam")
    @JSONField(name = "owningteam")
    @JsonProperty("owningteam")
    private String owningteam;
    /**
     * 负责的业务部门
     */
    @TableField(value = "owningbusinessunit")
    @JSONField(name = "owningbusinessunit")
    @JsonProperty("owningbusinessunit")
    private String owningbusinessunit;
    /**
     * 工作时间
     */
    @TableField(value = "businesshoursid")
    @JSONField(name = "businesshoursid")
    @JsonProperty("businesshoursid")
    private String businesshoursid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.BusinessUnit owningbusinessun;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.Calendar businesshours;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.Team owningte;



    /**
     * 设置 [工作流 ID]
     */
    public void setWorkflowid(String workflowid){
        this.workflowid = workflowid ;
        this.modify("workflowid",workflowid);
    }

    /**
     * 设置 [组件状态]
     */
    public void setComponentstate(String componentstate){
        this.componentstate = componentstate ;
        this.modify("componentstate",componentstate);
    }

    /**
     * 设置 [适用来源]
     */
    public void setApplicablefrom(String applicablefrom){
        this.applicablefrom = applicablefrom ;
        this.modify("applicablefrom",applicablefrom);
    }

    /**
     * 设置 [状态]
     */
    public void setStatecode(Integer statecode){
        this.statecode = statecode ;
        this.modify("statecode",statecode);
    }

    /**
     * 设置 [是否默认]
     */
    public void setIbizdefault(Integer ibizdefault){
        this.ibizdefault = ibizdefault ;
        this.modify("default",ibizdefault);
    }

    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [适用来源]
     */
    public void setApplicablefrompicklist(String applicablefrompicklist){
        this.applicablefrompicklist = applicablefrompicklist ;
        this.modify("applicablefrompicklist",applicablefrompicklist);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnername(String ownername){
        this.ownername = ownername ;
        this.modify("ownername",ownername);
    }

    /**
     * 设置 [SLA 类型]
     */
    public void setSlatype(String slatype){
        this.slatype = slatype ;
        this.modify("slatype",slatype);
    }

    /**
     * 设置 [负责人类型]
     */
    public void setOwnertype(String ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [版本号]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [WorkflowIdName]
     */
    public void setWorkflowidname(String workflowidname){
        this.workflowidname = workflowidname ;
        this.modify("workflowidname",workflowidname);
    }

    /**
     * 设置 [解决方案]
     */
    public void setSupportingsolutionid(String supportingsolutionid){
        this.supportingsolutionid = supportingsolutionid ;
        this.modify("supportingsolutionid",supportingsolutionid);
    }

    /**
     * 设置 [汇率]
     */
    public void setExchangerate(BigDecimal exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [ChangedAttributeList]
     */
    public void setChangedattributelist(String changedattributelist){
        this.changedattributelist = changedattributelist ;
        this.modify("changedattributelist",changedattributelist);
    }

    /**
     * 设置 [状态描述]
     */
    public void setStatuscode(Integer statuscode){
        this.statuscode = statuscode ;
        this.modify("statuscode",statuscode);
    }

    /**
     * 设置 [解决方案]
     */
    public void setSolutionid(String solutionid){
        this.solutionid = solutionid ;
        this.modify("solutionid",solutionid);
    }

    /**
     * 设置 [托管]
     */
    public void setManaged(Integer managed){
        this.managed = managed ;
        this.modify("managed",managed);
    }

    /**
     * 设置 [允许暂停和恢复]
     */
    public void setAllowpauseresume(Integer allowpauseresume){
        this.allowpauseresume = allowpauseresume ;
        this.modify("allowpauseresume",allowpauseresume);
    }

    /**
     * 设置 [主要实体]
     */
    public void setPrimaryentityotc(Integer primaryentityotc){
        this.primaryentityotc = primaryentityotc ;
        this.modify("primaryentityotc",primaryentityotc);
    }

    /**
     * 设置 [对象类型代码]
     */
    public void setObjecttypecode(String objecttypecode){
        this.objecttypecode = objecttypecode ;
        this.modify("objecttypecode",objecttypecode);
    }

    /**
     * 设置 [服务协议名称]
     */
    public void setSlaname(String slaname){
        this.slaname = slaname ;
        this.modify("slaname",slaname);
    }

    /**
     * 设置 [记录替代时间]
     */
    public void setOverwritetime(Timestamp overwritetime){
        this.overwritetime = overwritetime ;
        this.modify("overwritetime",overwritetime);
    }

    /**
     * 格式化日期 [记录替代时间]
     */
    public String formatOverwritetime(){
        if (this.overwritetime == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(overwritetime);
    }
    /**
     * 设置 [负责人用户]
     */
    public void setOwninguser(String owninguser){
        this.owninguser = owninguser ;
        this.modify("owninguser",owninguser);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnerid(String ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [唯一 ID]
     */
    public void setSlaidunique(String slaidunique){
        this.slaidunique = slaidunique ;
        this.modify("slaidunique",slaidunique);
    }

    /**
     * 设置 [负责团队]
     */
    public void setOwningteam(String owningteam){
        this.owningteam = owningteam ;
        this.modify("owningteam",owningteam);
    }

    /**
     * 设置 [负责的业务部门]
     */
    public void setOwningbusinessunit(String owningbusinessunit){
        this.owningbusinessunit = owningbusinessunit ;
        this.modify("owningbusinessunit",owningbusinessunit);
    }

    /**
     * 设置 [工作时间]
     */
    public void setBusinesshoursid(String businesshoursid){
        this.businesshoursid = businesshoursid ;
        this.modify("businesshoursid",businesshoursid);
    }


}


