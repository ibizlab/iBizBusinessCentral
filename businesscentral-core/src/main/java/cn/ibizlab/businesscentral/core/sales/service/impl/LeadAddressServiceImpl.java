package cn.ibizlab.businesscentral.core.sales.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.sales.domain.LeadAddress;
import cn.ibizlab.businesscentral.core.sales.filter.LeadAddressSearchContext;
import cn.ibizlab.businesscentral.core.sales.service.ILeadAddressService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.sales.mapper.LeadAddressMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[潜在顾客地址] 服务对象接口实现
 */
@Slf4j
@Service("LeadAddressServiceImpl")
public class LeadAddressServiceImpl extends ServiceImpl<LeadAddressMapper, LeadAddress> implements ILeadAddressService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.ILeadService leadService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITransactionCurrencyService transactioncurrencyService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(LeadAddress et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getLeadaddressid()),et);
        return true;
    }

    @Override
    public void createBatch(List<LeadAddress> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(LeadAddress et) {
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("leadaddressid",et.getLeadaddressid())))
            return false;
        CachedBeanCopier.copy(get(et.getLeadaddressid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<LeadAddress> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public LeadAddress get(String key) {
        LeadAddress et = getById(key);
        if(et==null){
            et=new LeadAddress();
            et.setLeadaddressid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public LeadAddress getDraft(LeadAddress et) {
        return et;
    }

    @Override
    public boolean checkKey(LeadAddress et) {
        return (!ObjectUtils.isEmpty(et.getLeadaddressid()))&&(!Objects.isNull(this.getById(et.getLeadaddressid())));
    }
    @Override
    @Transactional
    public boolean save(LeadAddress et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(LeadAddress et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<LeadAddress> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<LeadAddress> list) {
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<LeadAddress> selectByParentid(String leadid) {
        return baseMapper.selectByParentid(leadid);
    }

    @Override
    public void removeByParentid(String leadid) {
        this.remove(new QueryWrapper<LeadAddress>().eq("parentid",leadid));
    }

	@Override
    public List<LeadAddress> selectByTransactioncurrencyid(String transactioncurrencyid) {
        return baseMapper.selectByTransactioncurrencyid(transactioncurrencyid);
    }

    @Override
    public void removeByTransactioncurrencyid(String transactioncurrencyid) {
        this.remove(new QueryWrapper<LeadAddress>().eq("transactioncurrencyid",transactioncurrencyid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<LeadAddress> searchDefault(LeadAddressSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<LeadAddress> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<LeadAddress>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<LeadAddress> getLeadaddressByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<LeadAddress> getLeadaddressByEntities(List<LeadAddress> entities) {
        List ids =new ArrayList();
        for(LeadAddress entity : entities){
            Serializable id=entity.getLeadaddressid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}



