package cn.ibizlab.businesscentral.core.base.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.base.domain.LanguageLocale;
/**
 * 关系型数据实体[LanguageLocale] 查询条件对象
 */
@Slf4j
@Data
public class LanguageLocaleSearchContext extends QueryWrapperContext<LanguageLocale> {

	private String n_languagelocalename_like;//[语言环境名称]
	public void setN_languagelocalename_like(String n_languagelocalename_like) {
        this.n_languagelocalename_like = n_languagelocalename_like;
        if(!ObjectUtils.isEmpty(this.n_languagelocalename_like)){
            this.getSearchCond().like("languagelocalename", n_languagelocalename_like);
        }
    }
	private Integer n_statuscode_eq;//[语言状态代码]
	public void setN_statuscode_eq(Integer n_statuscode_eq) {
        this.n_statuscode_eq = n_statuscode_eq;
        if(!ObjectUtils.isEmpty(this.n_statuscode_eq)){
            this.getSearchCond().eq("statuscode", n_statuscode_eq);
        }
    }
	private Integer n_statecode_eq;//[状态代码]
	public void setN_statecode_eq(Integer n_statecode_eq) {
        this.n_statecode_eq = n_statecode_eq;
        if(!ObjectUtils.isEmpty(this.n_statecode_eq)){
            this.getSearchCond().eq("statecode", n_statecode_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("languagelocalename", query)   
            );
		 }
	}
}



