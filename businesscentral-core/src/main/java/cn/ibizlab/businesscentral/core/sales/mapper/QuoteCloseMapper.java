package cn.ibizlab.businesscentral.core.sales.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.sales.domain.QuoteClose;
import cn.ibizlab.businesscentral.core.sales.filter.QuoteCloseSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface QuoteCloseMapper extends BaseMapper<QuoteClose>{

    Page<QuoteClose> searchDefault(IPage page, @Param("srf") QuoteCloseSearchContext context, @Param("ew") Wrapper<QuoteClose> wrapper) ;
    @Override
    QuoteClose selectById(Serializable id);
    @Override
    int insert(QuoteClose entity);
    @Override
    int updateById(@Param(Constants.ENTITY) QuoteClose entity);
    @Override
    int update(@Param(Constants.ENTITY) QuoteClose entity, @Param("ew") Wrapper<QuoteClose> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<QuoteClose> selectByQuoteid(@Param("quoteid") Serializable quoteid) ;

    List<QuoteClose> selectByServiceid(@Param("serviceid") Serializable serviceid) ;

    List<QuoteClose> selectBySlaid(@Param("slaid") Serializable slaid) ;

    List<QuoteClose> selectByTransactioncurrencyid(@Param("transactioncurrencyid") Serializable transactioncurrencyid) ;

}
