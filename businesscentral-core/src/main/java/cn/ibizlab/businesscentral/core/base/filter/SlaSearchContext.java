package cn.ibizlab.businesscentral.core.base.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.base.domain.Sla;
/**
 * 关系型数据实体[Sla] 查询条件对象
 */
@Slf4j
@Data
public class SlaSearchContext extends QueryWrapperContext<Sla> {

	private String n_componentstate_eq;//[组件状态]
	public void setN_componentstate_eq(String n_componentstate_eq) {
        this.n_componentstate_eq = n_componentstate_eq;
        if(!ObjectUtils.isEmpty(this.n_componentstate_eq)){
            this.getSearchCond().eq("componentstate", n_componentstate_eq);
        }
    }
	private Integer n_statecode_eq;//[状态]
	public void setN_statecode_eq(Integer n_statecode_eq) {
        this.n_statecode_eq = n_statecode_eq;
        if(!ObjectUtils.isEmpty(this.n_statecode_eq)){
            this.getSearchCond().eq("statecode", n_statecode_eq);
        }
    }
	private String n_applicablefrompicklist_eq;//[适用来源]
	public void setN_applicablefrompicklist_eq(String n_applicablefrompicklist_eq) {
        this.n_applicablefrompicklist_eq = n_applicablefrompicklist_eq;
        if(!ObjectUtils.isEmpty(this.n_applicablefrompicklist_eq)){
            this.getSearchCond().eq("applicablefrompicklist", n_applicablefrompicklist_eq);
        }
    }
	private String n_slatype_eq;//[SLA 类型]
	public void setN_slatype_eq(String n_slatype_eq) {
        this.n_slatype_eq = n_slatype_eq;
        if(!ObjectUtils.isEmpty(this.n_slatype_eq)){
            this.getSearchCond().eq("slatype", n_slatype_eq);
        }
    }
	private Integer n_statuscode_eq;//[状态描述]
	public void setN_statuscode_eq(Integer n_statuscode_eq) {
        this.n_statuscode_eq = n_statuscode_eq;
        if(!ObjectUtils.isEmpty(this.n_statuscode_eq)){
            this.getSearchCond().eq("statuscode", n_statuscode_eq);
        }
    }
	private String n_objecttypecode_eq;//[对象类型代码]
	public void setN_objecttypecode_eq(String n_objecttypecode_eq) {
        this.n_objecttypecode_eq = n_objecttypecode_eq;
        if(!ObjectUtils.isEmpty(this.n_objecttypecode_eq)){
            this.getSearchCond().eq("objecttypecode", n_objecttypecode_eq);
        }
    }
	private String n_slaname_like;//[服务协议名称]
	public void setN_slaname_like(String n_slaname_like) {
        this.n_slaname_like = n_slaname_like;
        if(!ObjectUtils.isEmpty(this.n_slaname_like)){
            this.getSearchCond().like("slaname", n_slaname_like);
        }
    }
	private String n_owningteam_eq;//[负责团队]
	public void setN_owningteam_eq(String n_owningteam_eq) {
        this.n_owningteam_eq = n_owningteam_eq;
        if(!ObjectUtils.isEmpty(this.n_owningteam_eq)){
            this.getSearchCond().eq("owningteam", n_owningteam_eq);
        }
    }
	private String n_owningbusinessunit_eq;//[负责的业务部门]
	public void setN_owningbusinessunit_eq(String n_owningbusinessunit_eq) {
        this.n_owningbusinessunit_eq = n_owningbusinessunit_eq;
        if(!ObjectUtils.isEmpty(this.n_owningbusinessunit_eq)){
            this.getSearchCond().eq("owningbusinessunit", n_owningbusinessunit_eq);
        }
    }
	private String n_businesshoursid_eq;//[工作时间]
	public void setN_businesshoursid_eq(String n_businesshoursid_eq) {
        this.n_businesshoursid_eq = n_businesshoursid_eq;
        if(!ObjectUtils.isEmpty(this.n_businesshoursid_eq)){
            this.getSearchCond().eq("businesshoursid", n_businesshoursid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("slaname", query)   
            );
		 }
	}
}



