package cn.ibizlab.businesscentral.core.sales.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.sales.domain.Lead;
import cn.ibizlab.businesscentral.core.sales.filter.LeadSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Lead] 服务对象接口
 */
public interface ILeadService extends IService<Lead>{

    boolean create(Lead et) ;
    void createBatch(List<Lead> list) ;
    boolean update(Lead et) ;
    void updateBatch(List<Lead> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    Lead get(String key) ;
    Lead getDraft(Lead et) ;
    Lead active(Lead et) ;
    Lead addList(Lead et) ;
    Lead cancel(Lead et) ;
    boolean checkKey(Lead et) ;
    Lead disQualification(Lead et) ;
    Lead lostOrder(Lead et) ;
    Lead noInterested(Lead et) ;
    Lead qualification(Lead et) ;
    boolean save(Lead et) ;
    void saveBatch(List<Lead> list) ;
    Lead unable(Lead et) ;
    Page<Lead> searchDefault(LeadSearchContext context) ;
    Page<Lead> searchExcluded(LeadSearchContext context) ;
    Page<Lead> searchOn(LeadSearchContext context) ;
    List<Lead> selectByParentaccountid(String accountid) ;
    void removeByParentaccountid(String accountid) ;
    List<Lead> selectByRelatedobjectid(String activityid) ;
    void removeByRelatedobjectid(String activityid) ;
    List<Lead> selectByCampaignid(String campaignid) ;
    void removeByCampaignid(String campaignid) ;
    List<Lead> selectByParentcontactid(String contactid) ;
    void removeByParentcontactid(String contactid) ;
    List<Lead> selectByOriginatingcaseid(String incidentid) ;
    void removeByOriginatingcaseid(String incidentid) ;
    List<Lead> selectByQualifyingopportunityid(String opportunityid) ;
    void removeByQualifyingopportunityid(String opportunityid) ;
    List<Lead> selectBySlaid(String slaid) ;
    void removeBySlaid(String slaid) ;
    List<Lead> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Lead> getLeadByIds(List<String> ids) ;
    List<Lead> getLeadByEntities(List<Lead> entities) ;
}


