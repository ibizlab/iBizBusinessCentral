package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.Post;
import cn.ibizlab.businesscentral.core.base.filter.PostSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Post] 服务对象接口
 */
public interface IPostService extends IService<Post>{

    boolean create(Post et) ;
    void createBatch(List<Post> list) ;
    boolean update(Post et) ;
    void updateBatch(List<Post> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    Post get(String key) ;
    Post getDraft(Post et) ;
    boolean checkKey(Post et) ;
    boolean save(Post et) ;
    void saveBatch(List<Post> list) ;
    Page<Post> searchDefault(PostSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Post> getPostByIds(List<String> ids) ;
    List<Post> getPostByEntities(List<Post> entities) ;
}


