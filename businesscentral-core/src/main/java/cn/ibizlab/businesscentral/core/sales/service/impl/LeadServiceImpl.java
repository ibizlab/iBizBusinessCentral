package cn.ibizlab.businesscentral.core.sales.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.sales.domain.Lead;
import cn.ibizlab.businesscentral.core.sales.filter.LeadSearchContext;
import cn.ibizlab.businesscentral.core.sales.service.ILeadService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.sales.mapper.LeadMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[潜在顾客] 服务对象接口实现
 */
@Slf4j
@Service("LeadServiceImpl")
public class LeadServiceImpl extends ServiceImpl<LeadMapper, Lead> implements ILeadService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IAccountService accountService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IContactService contactService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.ILeadAddressService leadaddressService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.ILeadCompetitorService leadcompetitorService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.marketing.service.IListLeadService listleadService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.IOpportunityService opportunityService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.marketing.service.ICampaignResponseService campaignresponseService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.marketing.service.ICampaignService campaignService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IIncidentService incidentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ISlaService slaService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITransactionCurrencyService transactioncurrencyService;

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.logic.ILeadActiveLogic activeLogic;

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.logic.ILeadCancelLogic cancelLogic;

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.logic.ILeadLostOrderLogic lostorderLogic;

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.logic.ILeadNoInterestedLogic nointerestedLogic;

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.sales.service.logic.ILeadUnableLogic unableLogic;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(Lead et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getLeadid()),et);
        return true;
    }

    @Override
    public void createBatch(List<Lead> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Lead et) {
        fillParentData(et);
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("leadid",et.getLeadid())))
            return false;
        CachedBeanCopier.copy(get(et.getLeadid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<Lead> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Lead get(String key) {
        Lead et = getById(key);
        if(et==null){
            et=new Lead();
            et.setLeadid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Lead getDraft(Lead et) {
        fillParentData(et);
        return et;
    }

    @Override
    @Transactional
    public Lead active(Lead et) {
        activeLogic.execute(et);
         return et ;
    }

    @Override
    @Transactional
    public Lead addList(Lead et) {
        //自定义代码
        return et;
    }

    @Override
    @Transactional
    public Lead cancel(Lead et) {
        cancelLogic.execute(et);
         return et ;
    }

    @Override
    public boolean checkKey(Lead et) {
        return (!ObjectUtils.isEmpty(et.getLeadid()))&&(!Objects.isNull(this.getById(et.getLeadid())));
    }
    @Override
    @Transactional
    public Lead disQualification(Lead et) {
        //自定义代码
        return et;
    }

    @Override
    @Transactional
    public Lead lostOrder(Lead et) {
        lostorderLogic.execute(et);
         return et ;
    }

    @Override
    @Transactional
    public Lead noInterested(Lead et) {
        nointerestedLogic.execute(et);
         return et ;
    }

    @Override
    @Transactional
    public Lead qualification(Lead et) {
        //自定义代码
        return et;
    }

    @Override
    @Transactional
    public boolean save(Lead et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Lead et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<Lead> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<Lead> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }

    @Override
    @Transactional
    public Lead unable(Lead et) {
        unableLogic.execute(et);
         return et ;
    }


	@Override
    public List<Lead> selectByParentaccountid(String accountid) {
        return baseMapper.selectByParentaccountid(accountid);
    }

    @Override
    public void removeByParentaccountid(String accountid) {
        this.remove(new QueryWrapper<Lead>().eq("parentaccountid",accountid));
    }

	@Override
    public List<Lead> selectByRelatedobjectid(String activityid) {
        return baseMapper.selectByRelatedobjectid(activityid);
    }

    @Override
    public void removeByRelatedobjectid(String activityid) {
        this.remove(new QueryWrapper<Lead>().eq("relatedobjectid",activityid));
    }

	@Override
    public List<Lead> selectByCampaignid(String campaignid) {
        return baseMapper.selectByCampaignid(campaignid);
    }

    @Override
    public void removeByCampaignid(String campaignid) {
        this.remove(new QueryWrapper<Lead>().eq("campaignid",campaignid));
    }

	@Override
    public List<Lead> selectByParentcontactid(String contactid) {
        return baseMapper.selectByParentcontactid(contactid);
    }

    @Override
    public void removeByParentcontactid(String contactid) {
        this.remove(new QueryWrapper<Lead>().eq("parentcontactid",contactid));
    }

	@Override
    public List<Lead> selectByOriginatingcaseid(String incidentid) {
        return baseMapper.selectByOriginatingcaseid(incidentid);
    }

    @Override
    public void removeByOriginatingcaseid(String incidentid) {
        this.remove(new QueryWrapper<Lead>().eq("originatingcaseid",incidentid));
    }

	@Override
    public List<Lead> selectByQualifyingopportunityid(String opportunityid) {
        return baseMapper.selectByQualifyingopportunityid(opportunityid);
    }

    @Override
    public void removeByQualifyingopportunityid(String opportunityid) {
        this.remove(new QueryWrapper<Lead>().eq("qualifyingopportunityid",opportunityid));
    }

	@Override
    public List<Lead> selectBySlaid(String slaid) {
        return baseMapper.selectBySlaid(slaid);
    }

    @Override
    public void removeBySlaid(String slaid) {
        this.remove(new QueryWrapper<Lead>().eq("slaid",slaid));
    }

	@Override
    public List<Lead> selectByTransactioncurrencyid(String transactioncurrencyid) {
        return baseMapper.selectByTransactioncurrencyid(transactioncurrencyid);
    }

    @Override
    public void removeByTransactioncurrencyid(String transactioncurrencyid) {
        this.remove(new QueryWrapper<Lead>().eq("transactioncurrencyid",transactioncurrencyid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<Lead> searchDefault(LeadSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Lead> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Lead>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 已排除潜在顾客
     */
    @Override
    public Page<Lead> searchExcluded(LeadSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Lead> pages=baseMapper.searchExcluded(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Lead>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 已开启潜在顾客
     */
    @Override
    public Page<Lead> searchOn(LeadSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Lead> pages=baseMapper.searchOn(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Lead>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Lead et){
        //实体关系[DER1N_LEAD__ACCOUNT__PARENTACCOUNTID]
        if(!ObjectUtils.isEmpty(et.getParentaccountid())){
            cn.ibizlab.businesscentral.core.base.domain.Account parentaccount=et.getParentaccount();
            if(ObjectUtils.isEmpty(parentaccount)){
                cn.ibizlab.businesscentral.core.base.domain.Account majorEntity=accountService.get(et.getParentaccountid());
                et.setParentaccount(majorEntity);
                parentaccount=majorEntity;
            }
            et.setParentaccountname(parentaccount.getAccountname());
        }
        //实体关系[DER1N_LEAD__CAMPAIGNRESPONSE__RELATEDOBJECTID]
        if(!ObjectUtils.isEmpty(et.getRelatedobjectid())){
            cn.ibizlab.businesscentral.core.marketing.domain.CampaignResponse relatedobject=et.getRelatedobject();
            if(ObjectUtils.isEmpty(relatedobject)){
                cn.ibizlab.businesscentral.core.marketing.domain.CampaignResponse majorEntity=campaignresponseService.get(et.getRelatedobjectid());
                et.setRelatedobject(majorEntity);
                relatedobject=majorEntity;
            }
            et.setRelatedobjectname(relatedobject.getSubject());
        }
        //实体关系[DER1N_LEAD__CAMPAIGN__CAMPAIGNID]
        if(!ObjectUtils.isEmpty(et.getCampaignid())){
            cn.ibizlab.businesscentral.core.marketing.domain.Campaign campaign=et.getCampaign();
            if(ObjectUtils.isEmpty(campaign)){
                cn.ibizlab.businesscentral.core.marketing.domain.Campaign majorEntity=campaignService.get(et.getCampaignid());
                et.setCampaign(majorEntity);
                campaign=majorEntity;
            }
            et.setCampaignname(campaign.getCampaignname());
        }
        //实体关系[DER1N_LEAD__CONTACT__PARENTCONTACTID]
        if(!ObjectUtils.isEmpty(et.getParentcontactid())){
            cn.ibizlab.businesscentral.core.base.domain.Contact parentcontact=et.getParentcontact();
            if(ObjectUtils.isEmpty(parentcontact)){
                cn.ibizlab.businesscentral.core.base.domain.Contact majorEntity=contactService.get(et.getParentcontactid());
                et.setParentcontact(majorEntity);
                parentcontact=majorEntity;
            }
            et.setParentcontactname(parentcontact.getFullname());
        }
        //实体关系[DER1N_LEAD__INCIDENT__ORIGINATINGCASEID]
        if(!ObjectUtils.isEmpty(et.getOriginatingcaseid())){
            cn.ibizlab.businesscentral.core.service.domain.Incident originatingcase=et.getOriginatingcase();
            if(ObjectUtils.isEmpty(originatingcase)){
                cn.ibizlab.businesscentral.core.service.domain.Incident majorEntity=incidentService.get(et.getOriginatingcaseid());
                et.setOriginatingcase(majorEntity);
                originatingcase=majorEntity;
            }
            et.setOriginatingcasename(originatingcase.getTitle());
        }
        //实体关系[DER1N_LEAD__OPPORTUNITY__QUALIFYINGOPPORTUNITYID]
        if(!ObjectUtils.isEmpty(et.getQualifyingopportunityid())){
            cn.ibizlab.businesscentral.core.sales.domain.Opportunity qualifyingopportunity=et.getQualifyingopportunity();
            if(ObjectUtils.isEmpty(qualifyingopportunity)){
                cn.ibizlab.businesscentral.core.sales.domain.Opportunity majorEntity=opportunityService.get(et.getQualifyingopportunityid());
                et.setQualifyingopportunity(majorEntity);
                qualifyingopportunity=majorEntity;
            }
            et.setQualifyingopportunityname(qualifyingopportunity.getOpportunityname());
        }
        //实体关系[DER1N_LEAD__SLA__SLAID]
        if(!ObjectUtils.isEmpty(et.getSlaid())){
            cn.ibizlab.businesscentral.core.base.domain.Sla sla=et.getSla();
            if(ObjectUtils.isEmpty(sla)){
                cn.ibizlab.businesscentral.core.base.domain.Sla majorEntity=slaService.get(et.getSlaid());
                et.setSla(majorEntity);
                sla=majorEntity;
            }
            et.setSlaname(sla.getSlaname());
        }
        //实体关系[DER1N_LEAD__TRANSACTIONCURRENCY__TRANSACTIONCURRENCYID]
        if(!ObjectUtils.isEmpty(et.getTransactioncurrencyid())){
            cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency transactioncurrency=et.getTransactioncurrency();
            if(ObjectUtils.isEmpty(transactioncurrency)){
                cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency majorEntity=transactioncurrencyService.get(et.getTransactioncurrencyid());
                et.setTransactioncurrency(majorEntity);
                transactioncurrency=majorEntity;
            }
            et.setCurrencyname(transactioncurrency.getCurrencyname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Lead> getLeadByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Lead> getLeadByEntities(List<Lead> entities) {
        List ids =new ArrayList();
        for(Lead entity : entities){
            Serializable id=entity.getLeadid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}



