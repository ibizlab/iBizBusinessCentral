package cn.ibizlab.businesscentral.core.runtime.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.runtime.domain.Queue;
import cn.ibizlab.businesscentral.core.runtime.filter.QueueSearchContext;
import cn.ibizlab.businesscentral.core.runtime.service.IQueueService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.runtime.mapper.QueueMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[队列] 服务对象接口实现
 */
@Slf4j
@Service("QueueServiceImpl")
public class QueueServiceImpl extends ServiceImpl<QueueMapper, Queue> implements IQueueService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.runtime.service.IQueueItemService queueitemService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITeamService teamService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IBusinessUnitService businessunitService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITransactionCurrencyService transactioncurrencyService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(Queue et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getQueueid()),et);
        return true;
    }

    @Override
    public void createBatch(List<Queue> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Queue et) {
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("queueid",et.getQueueid())))
            return false;
        CachedBeanCopier.copy(get(et.getQueueid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<Queue> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Queue get(String key) {
        Queue et = getById(key);
        if(et==null){
            et=new Queue();
            et.setQueueid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Queue getDraft(Queue et) {
        return et;
    }

    @Override
    public boolean checkKey(Queue et) {
        return (!ObjectUtils.isEmpty(et.getQueueid()))&&(!Objects.isNull(this.getById(et.getQueueid())));
    }
    @Override
    @Transactional
    public boolean save(Queue et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Queue et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<Queue> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<Queue> list) {
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Queue> selectByBusinessunitid(String businessunitid) {
        return baseMapper.selectByBusinessunitid(businessunitid);
    }

    @Override
    public void removeByBusinessunitid(String businessunitid) {
        this.remove(new QueryWrapper<Queue>().eq("businessunitid",businessunitid));
    }

	@Override
    public List<Queue> selectByTransactioncurrencyid(String transactioncurrencyid) {
        return baseMapper.selectByTransactioncurrencyid(transactioncurrencyid);
    }

    @Override
    public void removeByTransactioncurrencyid(String transactioncurrencyid) {
        this.remove(new QueryWrapper<Queue>().eq("transactioncurrencyid",transactioncurrencyid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<Queue> searchDefault(QueueSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Queue> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Queue>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Queue> getQueueByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Queue> getQueueByEntities(List<Queue> entities) {
        List ids =new ArrayList();
        for(Queue entity : entities){
            Serializable id=entity.getQueueid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}



