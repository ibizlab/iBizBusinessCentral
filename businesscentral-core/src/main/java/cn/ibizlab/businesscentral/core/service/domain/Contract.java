package cn.ibizlab.businesscentral.core.service.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[合同]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "CONTRACT",resultMap = "ContractResultMap")
public class Contract extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 支持日历
     */
    @TableField(value = "effectivitycalendar")
    @JSONField(name = "effectivitycalendar")
    @JsonProperty("effectivitycalendar")
    private String effectivitycalendar;
    /**
     * 取消日期
     */
    @TableField(value = "cancelon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "cancelon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("cancelon")
    private Timestamp cancelon;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 客户
     */
    @TableField(value = "billingaccountname")
    @JSONField(name = "billingaccountname")
    @JsonProperty("billingaccountname")
    private String billingaccountname;
    /**
     * 客户
     */
    @TableField(value = "billingcustomername")
    @JSONField(name = "billingcustomername")
    @JsonProperty("billingcustomername")
    private String billingcustomername;
    /**
     * 联系人
     */
    @TableField(value = "billingcontactname")
    @JSONField(name = "billingcontactname")
    @JsonProperty("billingcontactname")
    private String billingcontactname;
    /**
     * ServiceAddressName
     */
    @TableField(value = "serviceaddressname")
    @JSONField(name = "serviceaddressname")
    @JsonProperty("serviceaddressname")
    private String serviceaddressname;
    /**
     * 付款人
     */
    @TableField(value = "billingcustomerid")
    @JSONField(name = "billingcustomerid")
    @JsonProperty("billingcustomerid")
    private String billingcustomerid;
    /**
     * BillToAddressName
     */
    @TableField(value = "billtoaddressname")
    @JSONField(name = "billtoaddressname")
    @JsonProperty("billtoaddressname")
    private String billtoaddressname;
    /**
     * Import Sequence Number
     */
    @TableField(value = "importsequencenumber")
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;
    /**
     * 实体图像
     */
    @TableField(value = "entityimage")
    @JSONField(name = "entityimage")
    @JsonProperty("entityimage")
    private String entityimage;
    /**
     * 合同编码
     */
    @TableField(value = "contractnumber")
    @JSONField(name = "contractnumber")
    @JsonProperty("contractnumber")
    private String contractnumber;
    /**
     * 记帐频率
     */
    @TableField(value = "billingfrequencycode")
    @JSONField(name = "billingfrequencycode")
    @JsonProperty("billingfrequencycode")
    private String billingfrequencycode;
    /**
     * 模板缩写
     */
    @TableField(value = "contracttemplateabbreviation")
    @JSONField(name = "contracttemplateabbreviation")
    @JsonProperty("contracttemplateabbreviation")
    private String contracttemplateabbreviation;
    /**
     * 客户
     */
    @TableField(value = "accountname")
    @JSONField(name = "accountname")
    @JsonProperty("accountname")
    private String accountname;
    /**
     * 总价 (Base)
     */
    @DEField(name = "totalprice_base")
    @TableField(value = "totalprice_base")
    @JSONField(name = "totalprice_base")
    @JsonProperty("totalprice_base")
    private BigDecimal totalpriceBase;
    /**
     * 负责人类型
     */
    @TableField(value = "ownertype")
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;
    /**
     * 折扣
     */
    @DEField(defaultValue = "0")
    @TableField(value = "usediscountaspercentage")
    @JSONField(name = "usediscountaspercentage")
    @JsonProperty("usediscountaspercentage")
    private Integer usediscountaspercentage;
    /**
     * 付款人类型
     */
    @TableField(value = "billingcustomertype")
    @JSONField(name = "billingcustomertype")
    @JsonProperty("billingcustomertype")
    private String billingcustomertype;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 总折扣 (Base)
     */
    @DEField(name = "totaldiscount_base")
    @TableField(value = "totaldiscount_base")
    @JSONField(name = "totaldiscount_base")
    @JsonProperty("totaldiscount_base")
    private BigDecimal totaldiscountBase;
    /**
     * 合同
     */
    @DEField(isKeyField=true)
    @TableId(value= "contractid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "contractid")
    @JsonProperty("contractid")
    private String contractid;
    /**
     * 总折扣
     */
    @TableField(value = "totaldiscount")
    @JSONField(name = "totaldiscount")
    @JsonProperty("totaldiscount")
    private BigDecimal totaldiscount;
    /**
     * EntityImage_Timestamp
     */
    @DEField(name = "entityimage_timestamp")
    @TableField(value = "entityimage_timestamp")
    @JSONField(name = "entityimage_timestamp")
    @JsonProperty("entityimage_timestamp")
    private BigInteger entityimageTimestamp;
    /**
     * 总价
     */
    @TableField(value = "totalprice")
    @JSONField(name = "totalprice")
    @JsonProperty("totalprice")
    private BigDecimal totalprice;
    /**
     * 联系人
     */
    @TableField(value = "contactname")
    @JSONField(name = "contactname")
    @JsonProperty("contactname")
    private String contactname;
    /**
     * 状态描述
     */
    @TableField(value = "statuscode")
    @JSONField(name = "statuscode")
    @JsonProperty("statuscode")
    private Integer statuscode;
    /**
     * UTC Conversion Time Zone Code
     */
    @TableField(value = "utcconversiontimezonecode")
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;
    /**
     * 服务级别
     */
    @TableField(value = "contractservicelevelcode")
    @JSONField(name = "contractservicelevelcode")
    @JsonProperty("contractservicelevelcode")
    private String contractservicelevelcode;
    /**
     * 合同名称
     */
    @TableField(value = "title")
    @JSONField(name = "title")
    @JsonProperty("title")
    private String title;
    /**
     * 说明
     */
    @TableField(value = "contractlanguage")
    @JSONField(name = "contractlanguage")
    @JsonProperty("contractlanguage")
    private String contractlanguage;
    /**
     * 记帐开始日期
     */
    @TableField(value = "billingstarton")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "billingstarton" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("billingstarton")
    private Timestamp billingstarton;
    /**
     * 记帐结束日期
     */
    @TableField(value = "billingendon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "billingendon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("billingendon")
    private Timestamp billingendon;
    /**
     * 客户
     */
    @TableField(value = "customername")
    @JSONField(name = "customername")
    @JsonProperty("customername")
    private String customername;
    /**
     * EntityImage_URL
     */
    @DEField(name = "entityimage_url")
    @TableField(value = "entityimage_url")
    @JSONField(name = "entityimage_url")
    @JsonProperty("entityimage_url")
    private String entityimageUrl;
    /**
     * Time Zone Rule Version Number
     */
    @TableField(value = "timezoneruleversionnumber")
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;
    /**
     * 净价
     */
    @TableField(value = "netprice")
    @JSONField(name = "netprice")
    @JsonProperty("netprice")
    private BigDecimal netprice;
    /**
     * 净价 (Base)
     */
    @DEField(name = "netprice_base")
    @TableField(value = "netprice_base")
    @JSONField(name = "netprice_base")
    @JsonProperty("netprice_base")
    private BigDecimal netpriceBase;
    /**
     * 持续时间
     */
    @TableField(value = "duration")
    @JSONField(name = "duration")
    @JsonProperty("duration")
    private Integer duration;
    /**
     * OriginatingContractName
     */
    @TableField(value = "originatingcontractname")
    @JSONField(name = "originatingcontractname")
    @JsonProperty("originatingcontractname")
    private String originatingcontractname;
    /**
     * 汇率
     */
    @TableField(value = "exchangerate")
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;
    /**
     * 合同开始日期
     */
    @TableField(value = "activeon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activeon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("activeon")
    private Timestamp activeon;
    /**
     * Version Number
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 状态
     */
    @TableField(value = "statecode")
    @JSONField(name = "statecode")
    @JsonProperty("statecode")
    private Integer statecode;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 负责人
     */
    @TableField(value = "ownername")
    @JSONField(name = "ownername")
    @JsonProperty("ownername")
    private String ownername;
    /**
     * 客户类型
     */
    @TableField(value = "customertype")
    @JSONField(name = "customertype")
    @JsonProperty("customertype")
    private String customertype;
    /**
     * 服务配额类型
     */
    @TableField(value = "allotmenttypecode")
    @JSONField(name = "allotmenttypecode")
    @JsonProperty("allotmenttypecode")
    private String allotmenttypecode;
    /**
     * 合同结束日期
     */
    @TableField(value = "expireson")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "expireson" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("expireson")
    private Timestamp expireson;
    /**
     * Record Created On
     */
    @TableField(value = "overriddencreatedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;
    /**
     * 负责人
     */
    @TableField(value = "ownerid")
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;
    /**
     * Email Address
     */
    @TableField(value = "emailaddress")
    @JSONField(name = "emailaddress")
    @JsonProperty("emailaddress")
    private String emailaddress;
    /**
     * 客户
     */
    @TableField(value = "customerid")
    @JSONField(name = "customerid")
    @JsonProperty("customerid")
    private String customerid;
    /**
     * EntityImageId
     */
    @TableField(value = "entityimageid")
    @JSONField(name = "entityimageid")
    @JsonProperty("entityimageid")
    private String entityimageid;
    /**
     * 帐单寄往地址
     */
    @TableField(value = "billtoaddress")
    @JSONField(name = "billtoaddress")
    @JsonProperty("billtoaddress")
    private String billtoaddress;
    /**
     * 合同模板
     */
    @TableField(value = "contracttemplateid")
    @JSONField(name = "contracttemplateid")
    @JsonProperty("contracttemplateid")
    private String contracttemplateid;
    /**
     * 合同地址
     */
    @TableField(value = "serviceaddress")
    @JSONField(name = "serviceaddress")
    @JsonProperty("serviceaddress")
    private String serviceaddress;
    /**
     * 原始合同
     */
    @TableField(value = "originatingcontract")
    @JSONField(name = "originatingcontract")
    @JsonProperty("originatingcontract")
    private String originatingcontract;
    /**
     * 货币
     */
    @TableField(value = "transactioncurrencyid")
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.service.domain.ContractTemplate contracttemplate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.service.domain.Contract originatingcontra;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.CustomerAddress billtoaddre;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.CustomerAddress serviceaddre;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency transactioncurrency;



    /**
     * 设置 [支持日历]
     */
    public void setEffectivitycalendar(String effectivitycalendar){
        this.effectivitycalendar = effectivitycalendar ;
        this.modify("effectivitycalendar",effectivitycalendar);
    }

    /**
     * 设置 [取消日期]
     */
    public void setCancelon(Timestamp cancelon){
        this.cancelon = cancelon ;
        this.modify("cancelon",cancelon);
    }

    /**
     * 格式化日期 [取消日期]
     */
    public String formatCancelon(){
        if (this.cancelon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(cancelon);
    }
    /**
     * 设置 [客户]
     */
    public void setBillingaccountname(String billingaccountname){
        this.billingaccountname = billingaccountname ;
        this.modify("billingaccountname",billingaccountname);
    }

    /**
     * 设置 [客户]
     */
    public void setBillingcustomername(String billingcustomername){
        this.billingcustomername = billingcustomername ;
        this.modify("billingcustomername",billingcustomername);
    }

    /**
     * 设置 [联系人]
     */
    public void setBillingcontactname(String billingcontactname){
        this.billingcontactname = billingcontactname ;
        this.modify("billingcontactname",billingcontactname);
    }

    /**
     * 设置 [ServiceAddressName]
     */
    public void setServiceaddressname(String serviceaddressname){
        this.serviceaddressname = serviceaddressname ;
        this.modify("serviceaddressname",serviceaddressname);
    }

    /**
     * 设置 [付款人]
     */
    public void setBillingcustomerid(String billingcustomerid){
        this.billingcustomerid = billingcustomerid ;
        this.modify("billingcustomerid",billingcustomerid);
    }

    /**
     * 设置 [BillToAddressName]
     */
    public void setBilltoaddressname(String billtoaddressname){
        this.billtoaddressname = billtoaddressname ;
        this.modify("billtoaddressname",billtoaddressname);
    }

    /**
     * 设置 [Import Sequence Number]
     */
    public void setImportsequencenumber(Integer importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [实体图像]
     */
    public void setEntityimage(String entityimage){
        this.entityimage = entityimage ;
        this.modify("entityimage",entityimage);
    }

    /**
     * 设置 [合同编码]
     */
    public void setContractnumber(String contractnumber){
        this.contractnumber = contractnumber ;
        this.modify("contractnumber",contractnumber);
    }

    /**
     * 设置 [记帐频率]
     */
    public void setBillingfrequencycode(String billingfrequencycode){
        this.billingfrequencycode = billingfrequencycode ;
        this.modify("billingfrequencycode",billingfrequencycode);
    }

    /**
     * 设置 [模板缩写]
     */
    public void setContracttemplateabbreviation(String contracttemplateabbreviation){
        this.contracttemplateabbreviation = contracttemplateabbreviation ;
        this.modify("contracttemplateabbreviation",contracttemplateabbreviation);
    }

    /**
     * 设置 [客户]
     */
    public void setAccountname(String accountname){
        this.accountname = accountname ;
        this.modify("accountname",accountname);
    }

    /**
     * 设置 [总价 (Base)]
     */
    public void setTotalpriceBase(BigDecimal totalpriceBase){
        this.totalpriceBase = totalpriceBase ;
        this.modify("totalprice_base",totalpriceBase);
    }

    /**
     * 设置 [负责人类型]
     */
    public void setOwnertype(String ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [折扣]
     */
    public void setUsediscountaspercentage(Integer usediscountaspercentage){
        this.usediscountaspercentage = usediscountaspercentage ;
        this.modify("usediscountaspercentage",usediscountaspercentage);
    }

    /**
     * 设置 [付款人类型]
     */
    public void setBillingcustomertype(String billingcustomertype){
        this.billingcustomertype = billingcustomertype ;
        this.modify("billingcustomertype",billingcustomertype);
    }

    /**
     * 设置 [总折扣 (Base)]
     */
    public void setTotaldiscountBase(BigDecimal totaldiscountBase){
        this.totaldiscountBase = totaldiscountBase ;
        this.modify("totaldiscount_base",totaldiscountBase);
    }

    /**
     * 设置 [总折扣]
     */
    public void setTotaldiscount(BigDecimal totaldiscount){
        this.totaldiscount = totaldiscount ;
        this.modify("totaldiscount",totaldiscount);
    }

    /**
     * 设置 [EntityImage_Timestamp]
     */
    public void setEntityimageTimestamp(BigInteger entityimageTimestamp){
        this.entityimageTimestamp = entityimageTimestamp ;
        this.modify("entityimage_timestamp",entityimageTimestamp);
    }

    /**
     * 设置 [总价]
     */
    public void setTotalprice(BigDecimal totalprice){
        this.totalprice = totalprice ;
        this.modify("totalprice",totalprice);
    }

    /**
     * 设置 [联系人]
     */
    public void setContactname(String contactname){
        this.contactname = contactname ;
        this.modify("contactname",contactname);
    }

    /**
     * 设置 [状态描述]
     */
    public void setStatuscode(Integer statuscode){
        this.statuscode = statuscode ;
        this.modify("statuscode",statuscode);
    }

    /**
     * 设置 [UTC Conversion Time Zone Code]
     */
    public void setUtcconversiontimezonecode(Integer utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [服务级别]
     */
    public void setContractservicelevelcode(String contractservicelevelcode){
        this.contractservicelevelcode = contractservicelevelcode ;
        this.modify("contractservicelevelcode",contractservicelevelcode);
    }

    /**
     * 设置 [合同名称]
     */
    public void setTitle(String title){
        this.title = title ;
        this.modify("title",title);
    }

    /**
     * 设置 [说明]
     */
    public void setContractlanguage(String contractlanguage){
        this.contractlanguage = contractlanguage ;
        this.modify("contractlanguage",contractlanguage);
    }

    /**
     * 设置 [记帐开始日期]
     */
    public void setBillingstarton(Timestamp billingstarton){
        this.billingstarton = billingstarton ;
        this.modify("billingstarton",billingstarton);
    }

    /**
     * 格式化日期 [记帐开始日期]
     */
    public String formatBillingstarton(){
        if (this.billingstarton == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(billingstarton);
    }
    /**
     * 设置 [记帐结束日期]
     */
    public void setBillingendon(Timestamp billingendon){
        this.billingendon = billingendon ;
        this.modify("billingendon",billingendon);
    }

    /**
     * 格式化日期 [记帐结束日期]
     */
    public String formatBillingendon(){
        if (this.billingendon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(billingendon);
    }
    /**
     * 设置 [客户]
     */
    public void setCustomername(String customername){
        this.customername = customername ;
        this.modify("customername",customername);
    }

    /**
     * 设置 [EntityImage_URL]
     */
    public void setEntityimageUrl(String entityimageUrl){
        this.entityimageUrl = entityimageUrl ;
        this.modify("entityimage_url",entityimageUrl);
    }

    /**
     * 设置 [Time Zone Rule Version Number]
     */
    public void setTimezoneruleversionnumber(Integer timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [净价]
     */
    public void setNetprice(BigDecimal netprice){
        this.netprice = netprice ;
        this.modify("netprice",netprice);
    }

    /**
     * 设置 [净价 (Base)]
     */
    public void setNetpriceBase(BigDecimal netpriceBase){
        this.netpriceBase = netpriceBase ;
        this.modify("netprice_base",netpriceBase);
    }

    /**
     * 设置 [持续时间]
     */
    public void setDuration(Integer duration){
        this.duration = duration ;
        this.modify("duration",duration);
    }

    /**
     * 设置 [OriginatingContractName]
     */
    public void setOriginatingcontractname(String originatingcontractname){
        this.originatingcontractname = originatingcontractname ;
        this.modify("originatingcontractname",originatingcontractname);
    }

    /**
     * 设置 [汇率]
     */
    public void setExchangerate(BigDecimal exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [合同开始日期]
     */
    public void setActiveon(Timestamp activeon){
        this.activeon = activeon ;
        this.modify("activeon",activeon);
    }

    /**
     * 格式化日期 [合同开始日期]
     */
    public String formatActiveon(){
        if (this.activeon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(activeon);
    }
    /**
     * 设置 [Version Number]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [状态]
     */
    public void setStatecode(Integer statecode){
        this.statecode = statecode ;
        this.modify("statecode",statecode);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnername(String ownername){
        this.ownername = ownername ;
        this.modify("ownername",ownername);
    }

    /**
     * 设置 [客户类型]
     */
    public void setCustomertype(String customertype){
        this.customertype = customertype ;
        this.modify("customertype",customertype);
    }

    /**
     * 设置 [服务配额类型]
     */
    public void setAllotmenttypecode(String allotmenttypecode){
        this.allotmenttypecode = allotmenttypecode ;
        this.modify("allotmenttypecode",allotmenttypecode);
    }

    /**
     * 设置 [合同结束日期]
     */
    public void setExpireson(Timestamp expireson){
        this.expireson = expireson ;
        this.modify("expireson",expireson);
    }

    /**
     * 格式化日期 [合同结束日期]
     */
    public String formatExpireson(){
        if (this.expireson == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(expireson);
    }
    /**
     * 设置 [Record Created On]
     */
    public void setOverriddencreatedon(Timestamp overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 格式化日期 [Record Created On]
     */
    public String formatOverriddencreatedon(){
        if (this.overriddencreatedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(overriddencreatedon);
    }
    /**
     * 设置 [负责人]
     */
    public void setOwnerid(String ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [Email Address]
     */
    public void setEmailaddress(String emailaddress){
        this.emailaddress = emailaddress ;
        this.modify("emailaddress",emailaddress);
    }

    /**
     * 设置 [客户]
     */
    public void setCustomerid(String customerid){
        this.customerid = customerid ;
        this.modify("customerid",customerid);
    }

    /**
     * 设置 [EntityImageId]
     */
    public void setEntityimageid(String entityimageid){
        this.entityimageid = entityimageid ;
        this.modify("entityimageid",entityimageid);
    }

    /**
     * 设置 [帐单寄往地址]
     */
    public void setBilltoaddress(String billtoaddress){
        this.billtoaddress = billtoaddress ;
        this.modify("billtoaddress",billtoaddress);
    }

    /**
     * 设置 [合同模板]
     */
    public void setContracttemplateid(String contracttemplateid){
        this.contracttemplateid = contracttemplateid ;
        this.modify("contracttemplateid",contracttemplateid);
    }

    /**
     * 设置 [合同地址]
     */
    public void setServiceaddress(String serviceaddress){
        this.serviceaddress = serviceaddress ;
        this.modify("serviceaddress",serviceaddress);
    }

    /**
     * 设置 [原始合同]
     */
    public void setOriginatingcontract(String originatingcontract){
        this.originatingcontract = originatingcontract ;
        this.modify("originatingcontract",originatingcontract);
    }

    /**
     * 设置 [货币]
     */
    public void setTransactioncurrencyid(String transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }


}


