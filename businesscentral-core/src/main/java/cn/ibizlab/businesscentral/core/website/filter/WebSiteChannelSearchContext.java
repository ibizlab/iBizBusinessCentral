package cn.ibizlab.businesscentral.core.website.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.website.domain.WebSiteChannel;
/**
 * 关系型数据实体[WebSiteChannel] 查询条件对象
 */
@Slf4j
@Data
public class WebSiteChannelSearchContext extends QueryWrapperContext<WebSiteChannel> {

	private String n_channeltype_eq;//[频道类型]
	public void setN_channeltype_eq(String n_channeltype_eq) {
        this.n_channeltype_eq = n_channeltype_eq;
        if(!ObjectUtils.isEmpty(this.n_channeltype_eq)){
            this.getSearchCond().eq("channeltype", n_channeltype_eq);
        }
    }
	private String n_websitechannelname_like;//[频道名称]
	public void setN_websitechannelname_like(String n_websitechannelname_like) {
        this.n_websitechannelname_like = n_websitechannelname_like;
        if(!ObjectUtils.isEmpty(this.n_websitechannelname_like)){
            this.getSearchCond().like("websitechannelname", n_websitechannelname_like);
        }
    }
	private String n_websitename_eq;//[站点]
	public void setN_websitename_eq(String n_websitename_eq) {
        this.n_websitename_eq = n_websitename_eq;
        if(!ObjectUtils.isEmpty(this.n_websitename_eq)){
            this.getSearchCond().eq("websitename", n_websitename_eq);
        }
    }
	private String n_websitename_like;//[站点]
	public void setN_websitename_like(String n_websitename_like) {
        this.n_websitename_like = n_websitename_like;
        if(!ObjectUtils.isEmpty(this.n_websitename_like)){
            this.getSearchCond().like("websitename", n_websitename_like);
        }
    }
	private String n_pwebsitechannelname_eq;//[上级频道]
	public void setN_pwebsitechannelname_eq(String n_pwebsitechannelname_eq) {
        this.n_pwebsitechannelname_eq = n_pwebsitechannelname_eq;
        if(!ObjectUtils.isEmpty(this.n_pwebsitechannelname_eq)){
            this.getSearchCond().eq("pwebsitechannelname", n_pwebsitechannelname_eq);
        }
    }
	private String n_pwebsitechannelname_like;//[上级频道]
	public void setN_pwebsitechannelname_like(String n_pwebsitechannelname_like) {
        this.n_pwebsitechannelname_like = n_pwebsitechannelname_like;
        if(!ObjectUtils.isEmpty(this.n_pwebsitechannelname_like)){
            this.getSearchCond().like("pwebsitechannelname", n_pwebsitechannelname_like);
        }
    }
	private String n_websiteid_eq;//[网站标识]
	public void setN_websiteid_eq(String n_websiteid_eq) {
        this.n_websiteid_eq = n_websiteid_eq;
        if(!ObjectUtils.isEmpty(this.n_websiteid_eq)){
            this.getSearchCond().eq("websiteid", n_websiteid_eq);
        }
    }
	private String n_pwebsitechannelid_eq;//[上级频道标识]
	public void setN_pwebsitechannelid_eq(String n_pwebsitechannelid_eq) {
        this.n_pwebsitechannelid_eq = n_pwebsitechannelid_eq;
        if(!ObjectUtils.isEmpty(this.n_pwebsitechannelid_eq)){
            this.getSearchCond().eq("pwebsitechannelid", n_pwebsitechannelid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("websitechannelname", query)   
            );
		 }
	}
}



