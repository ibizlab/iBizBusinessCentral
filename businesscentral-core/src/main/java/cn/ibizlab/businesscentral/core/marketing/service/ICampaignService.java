package cn.ibizlab.businesscentral.core.marketing.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.marketing.domain.Campaign;
import cn.ibizlab.businesscentral.core.marketing.filter.CampaignSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Campaign] 服务对象接口
 */
public interface ICampaignService extends IService<Campaign>{

    boolean create(Campaign et) ;
    void createBatch(List<Campaign> list) ;
    boolean update(Campaign et) ;
    void updateBatch(List<Campaign> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    Campaign get(String key) ;
    Campaign getDraft(Campaign et) ;
    Campaign active(Campaign et) ;
    boolean checkKey(Campaign et) ;
    boolean save(Campaign et) ;
    void saveBatch(List<Campaign> list) ;
    Campaign stop(Campaign et) ;
    Page<Campaign> searchDefault(CampaignSearchContext context) ;
    Page<Campaign> searchEffective(CampaignSearchContext context) ;
    Page<Campaign> searchStop(CampaignSearchContext context) ;
    List<Campaign> selectByPricelistid(String pricelevelid) ;
    void removeByPricelistid(String pricelevelid) ;
    List<Campaign> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Campaign> getCampaignByIds(List<String> ids) ;
    List<Campaign> getCampaignByEntities(List<Campaign> entities) ;
}


