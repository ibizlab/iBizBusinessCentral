package cn.ibizlab.businesscentral.core.service.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.service.domain.Contract;
import cn.ibizlab.businesscentral.core.service.filter.ContractSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Contract] 服务对象接口
 */
public interface IContractService extends IService<Contract>{

    boolean create(Contract et) ;
    void createBatch(List<Contract> list) ;
    boolean update(Contract et) ;
    void updateBatch(List<Contract> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    Contract get(String key) ;
    Contract getDraft(Contract et) ;
    boolean checkKey(Contract et) ;
    boolean save(Contract et) ;
    void saveBatch(List<Contract> list) ;
    Page<Contract> searchDefault(ContractSearchContext context) ;
    List<Contract> selectByContracttemplateid(String contracttemplateid) ;
    void removeByContracttemplateid(String contracttemplateid) ;
    List<Contract> selectByOriginatingcontract(String contractid) ;
    void removeByOriginatingcontract(String contractid) ;
    List<Contract> selectByBilltoaddress(String customeraddressid) ;
    void removeByBilltoaddress(String customeraddressid) ;
    List<Contract> selectByServiceaddress(String customeraddressid) ;
    void removeByServiceaddress(String customeraddressid) ;
    List<Contract> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Contract> getContractByIds(List<String> ids) ;
    List<Contract> getContractByEntities(List<Contract> entities) ;
}


