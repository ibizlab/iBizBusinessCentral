package cn.ibizlab.businesscentral.core.website.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.website.domain.WebSiteResource;
import cn.ibizlab.businesscentral.core.website.filter.WebSiteResourceSearchContext;
import cn.ibizlab.businesscentral.core.website.service.IWebSiteResourceService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.website.mapper.WebSiteResourceMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[站点资源] 服务对象接口实现
 */
@Slf4j
@Service("WebSiteResourceServiceImpl")
public class WebSiteResourceServiceImpl extends ServiceImpl<WebSiteResourceMapper, WebSiteResource> implements IWebSiteResourceService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.website.service.IWebSiteService websiteService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(WebSiteResource et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getWebsiteresourceid()),et);
        return true;
    }

    @Override
    public void createBatch(List<WebSiteResource> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(WebSiteResource et) {
        fillParentData(et);
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("websiteresourceid",et.getWebsiteresourceid())))
            return false;
        CachedBeanCopier.copy(get(et.getWebsiteresourceid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<WebSiteResource> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public WebSiteResource get(String key) {
        WebSiteResource et = getById(key);
        if(et==null){
            et=new WebSiteResource();
            et.setWebsiteresourceid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public WebSiteResource getDraft(WebSiteResource et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(WebSiteResource et) {
        return (!ObjectUtils.isEmpty(et.getWebsiteresourceid()))&&(!Objects.isNull(this.getById(et.getWebsiteresourceid())));
    }
    @Override
    @Transactional
    public boolean save(WebSiteResource et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(WebSiteResource et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<WebSiteResource> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<WebSiteResource> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<WebSiteResource> selectByWebsiteid(String websiteid) {
        return baseMapper.selectByWebsiteid(websiteid);
    }

    @Override
    public void removeByWebsiteid(String websiteid) {
        this.remove(new QueryWrapper<WebSiteResource>().eq("websiteid",websiteid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<WebSiteResource> searchDefault(WebSiteResourceSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<WebSiteResource> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<WebSiteResource>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(WebSiteResource et){
        //实体关系[DER1N_WEBSITERESOURCE_WEBSITE_WEBSITEID]
        if(!ObjectUtils.isEmpty(et.getWebsiteid())){
            cn.ibizlab.businesscentral.core.website.domain.WebSite website=et.getWebsite();
            if(ObjectUtils.isEmpty(website)){
                cn.ibizlab.businesscentral.core.website.domain.WebSite majorEntity=websiteService.get(et.getWebsiteid());
                et.setWebsite(majorEntity);
                website=majorEntity;
            }
            et.setWebsitename(website.getWebsitename());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<WebSiteResource> getWebsiteresourceByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<WebSiteResource> getWebsiteresourceByEntities(List<WebSiteResource> entities) {
        List ids =new ArrayList();
        for(WebSiteResource entity : entities){
            Serializable id=entity.getWebsiteresourceid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}



