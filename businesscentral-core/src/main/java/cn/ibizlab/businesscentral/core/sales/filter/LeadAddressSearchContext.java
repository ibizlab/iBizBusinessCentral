package cn.ibizlab.businesscentral.core.sales.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.sales.domain.LeadAddress;
/**
 * 关系型数据实体[LeadAddress] 查询条件对象
 */
@Slf4j
@Data
public class LeadAddressSearchContext extends QueryWrapperContext<LeadAddress> {

	private String n_addresstypecode_eq;//[地址类型]
	public void setN_addresstypecode_eq(String n_addresstypecode_eq) {
        this.n_addresstypecode_eq = n_addresstypecode_eq;
        if(!ObjectUtils.isEmpty(this.n_addresstypecode_eq)){
            this.getSearchCond().eq("addresstypecode", n_addresstypecode_eq);
        }
    }
	private String n_leadaddressname_like;//[潜在客户地址名称]
	public void setN_leadaddressname_like(String n_leadaddressname_like) {
        this.n_leadaddressname_like = n_leadaddressname_like;
        if(!ObjectUtils.isEmpty(this.n_leadaddressname_like)){
            this.getSearchCond().like("leadaddressname", n_leadaddressname_like);
        }
    }
	private String n_shippingmethodcode_eq;//[送货方式]
	public void setN_shippingmethodcode_eq(String n_shippingmethodcode_eq) {
        this.n_shippingmethodcode_eq = n_shippingmethodcode_eq;
        if(!ObjectUtils.isEmpty(this.n_shippingmethodcode_eq)){
            this.getSearchCond().eq("shippingmethodcode", n_shippingmethodcode_eq);
        }
    }
	private String n_parentid_eq;//[父级]
	public void setN_parentid_eq(String n_parentid_eq) {
        this.n_parentid_eq = n_parentid_eq;
        if(!ObjectUtils.isEmpty(this.n_parentid_eq)){
            this.getSearchCond().eq("parentid", n_parentid_eq);
        }
    }
	private String n_transactioncurrencyid_eq;//[货币]
	public void setN_transactioncurrencyid_eq(String n_transactioncurrencyid_eq) {
        this.n_transactioncurrencyid_eq = n_transactioncurrencyid_eq;
        if(!ObjectUtils.isEmpty(this.n_transactioncurrencyid_eq)){
            this.getSearchCond().eq("transactioncurrencyid", n_transactioncurrencyid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("leadaddressname", query)   
            );
		 }
	}
}



