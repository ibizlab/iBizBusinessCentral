package cn.ibizlab.businesscentral.core.finance.service.logic;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.businesscentral.core.finance.domain.Invoice;

/**
 * 关系型数据实体[Paid] 对象
 */
public interface IInvoicePaidLogic {

    void execute(Invoice et) ;

}
