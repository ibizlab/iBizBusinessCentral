package cn.ibizlab.businesscentral.core.sales.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.sales.domain.SalesLiteratureItem;
import cn.ibizlab.businesscentral.core.sales.filter.SalesLiteratureItemSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface SalesLiteratureItemMapper extends BaseMapper<SalesLiteratureItem>{

    Page<SalesLiteratureItem> searchDefault(IPage page, @Param("srf") SalesLiteratureItemSearchContext context, @Param("ew") Wrapper<SalesLiteratureItem> wrapper) ;
    @Override
    SalesLiteratureItem selectById(Serializable id);
    @Override
    int insert(SalesLiteratureItem entity);
    @Override
    int updateById(@Param(Constants.ENTITY) SalesLiteratureItem entity);
    @Override
    int update(@Param(Constants.ENTITY) SalesLiteratureItem entity, @Param("ew") Wrapper<SalesLiteratureItem> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<SalesLiteratureItem> selectBySalesliteratureid(@Param("salesliteratureid") Serializable salesliteratureid) ;

}
