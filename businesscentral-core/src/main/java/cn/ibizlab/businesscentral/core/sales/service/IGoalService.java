package cn.ibizlab.businesscentral.core.sales.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.sales.domain.Goal;
import cn.ibizlab.businesscentral.core.sales.filter.GoalSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Goal] 服务对象接口
 */
public interface IGoalService extends IService<Goal>{

    boolean create(Goal et) ;
    void createBatch(List<Goal> list) ;
    boolean update(Goal et) ;
    void updateBatch(List<Goal> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    Goal get(String key) ;
    Goal getDraft(Goal et) ;
    Goal active(Goal et) ;
    boolean checkKey(Goal et) ;
    Goal close(Goal et) ;
    boolean save(Goal et) ;
    void saveBatch(List<Goal> list) ;
    Page<Goal> searchDefault(GoalSearchContext context) ;
    Page<Goal> searchStop(GoalSearchContext context) ;
    Page<Goal> searchUsable(GoalSearchContext context) ;
    List<Goal> selectByRollupqueryactualdecimalid(String goalrollupqueryid) ;
    void removeByRollupqueryactualdecimalid(String goalrollupqueryid) ;
    List<Goal> selectByRollupqueryactualintegerid(String goalrollupqueryid) ;
    void removeByRollupqueryactualintegerid(String goalrollupqueryid) ;
    List<Goal> selectByRollupqueryactualmoneyid(String goalrollupqueryid) ;
    void removeByRollupqueryactualmoneyid(String goalrollupqueryid) ;
    List<Goal> selectByRollupquerycustomdecimalid(String goalrollupqueryid) ;
    void removeByRollupquerycustomdecimalid(String goalrollupqueryid) ;
    List<Goal> selectByRollupquerycustomintegerid(String goalrollupqueryid) ;
    void removeByRollupquerycustomintegerid(String goalrollupqueryid) ;
    List<Goal> selectByRollupquerycustommoneyid(String goalrollupqueryid) ;
    void removeByRollupquerycustommoneyid(String goalrollupqueryid) ;
    List<Goal> selectByRollupqueryinprogressdecimalid(String goalrollupqueryid) ;
    void removeByRollupqueryinprogressdecimalid(String goalrollupqueryid) ;
    List<Goal> selectByRollupqueryinprogressintegerid(String goalrollupqueryid) ;
    void removeByRollupqueryinprogressintegerid(String goalrollupqueryid) ;
    List<Goal> selectByRollupqueryinprogressmoneyid(String goalrollupqueryid) ;
    void removeByRollupqueryinprogressmoneyid(String goalrollupqueryid) ;
    List<Goal> selectByGoalwitherrorid(String goalid) ;
    void removeByGoalwitherrorid(String goalid) ;
    List<Goal> selectByParentgoalid(String goalid) ;
    void removeByParentgoalid(String goalid) ;
    List<Goal> selectByMetricid(String metricid) ;
    void removeByMetricid(String metricid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Goal> getGoalByIds(List<String> ids) ;
    List<Goal> getGoalByEntities(List<Goal> entities) ;
}


