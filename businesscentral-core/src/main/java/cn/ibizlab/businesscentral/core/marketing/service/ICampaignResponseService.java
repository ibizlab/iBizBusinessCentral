package cn.ibizlab.businesscentral.core.marketing.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.marketing.domain.CampaignResponse;
import cn.ibizlab.businesscentral.core.marketing.filter.CampaignResponseSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[CampaignResponse] 服务对象接口
 */
public interface ICampaignResponseService extends IService<CampaignResponse>{

    boolean create(CampaignResponse et) ;
    void createBatch(List<CampaignResponse> list) ;
    boolean update(CampaignResponse et) ;
    void updateBatch(List<CampaignResponse> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    CampaignResponse get(String key) ;
    CampaignResponse getDraft(CampaignResponse et) ;
    boolean checkKey(CampaignResponse et) ;
    boolean save(CampaignResponse et) ;
    void saveBatch(List<CampaignResponse> list) ;
    Page<CampaignResponse> searchByParentKey(CampaignResponseSearchContext context) ;
    Page<CampaignResponse> searchDefault(CampaignResponseSearchContext context) ;
    List<CampaignResponse> selectBySlaid(String slaid) ;
    void removeBySlaid(String slaid) ;
    List<CampaignResponse> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<CampaignResponse> getCampaignresponseByIds(List<String> ids) ;
    List<CampaignResponse> getCampaignresponseByEntities(List<CampaignResponse> entities) ;
}


