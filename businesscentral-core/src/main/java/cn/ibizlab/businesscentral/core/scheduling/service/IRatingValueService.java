package cn.ibizlab.businesscentral.core.scheduling.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.scheduling.domain.RatingValue;
import cn.ibizlab.businesscentral.core.scheduling.filter.RatingValueSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[RatingValue] 服务对象接口
 */
public interface IRatingValueService extends IService<RatingValue>{

    boolean create(RatingValue et) ;
    void createBatch(List<RatingValue> list) ;
    boolean update(RatingValue et) ;
    void updateBatch(List<RatingValue> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    RatingValue get(String key) ;
    RatingValue getDraft(RatingValue et) ;
    boolean checkKey(RatingValue et) ;
    boolean save(RatingValue et) ;
    void saveBatch(List<RatingValue> list) ;
    Page<RatingValue> searchDefault(RatingValueSearchContext context) ;
    List<RatingValue> selectByRatingmodel(String ratingmodelid) ;
    void removeByRatingmodel(String ratingmodelid) ;
    List<RatingValue> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<RatingValue> getRatingvalueByIds(List<String> ids) ;
    List<RatingValue> getRatingvalueByEntities(List<RatingValue> entities) ;
}


