package cn.ibizlab.businesscentral.core.base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.base.domain.SiteMap;
import cn.ibizlab.businesscentral.core.base.filter.SiteMapSearchContext;
import cn.ibizlab.businesscentral.core.base.service.ISiteMapService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.base.mapper.SiteMapMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[站点地图] 服务对象接口实现
 */
@Slf4j
@Service("SiteMapServiceImpl")
public class SiteMapServiceImpl extends ServiceImpl<SiteMapMapper, SiteMap> implements ISiteMapService {


    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(SiteMap et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getSitemapid()),et);
        return true;
    }

    @Override
    public void createBatch(List<SiteMap> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(SiteMap et) {
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("sitemapid",et.getSitemapid())))
            return false;
        CachedBeanCopier.copy(get(et.getSitemapid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<SiteMap> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public SiteMap get(String key) {
        SiteMap et = getById(key);
        if(et==null){
            et=new SiteMap();
            et.setSitemapid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public SiteMap getDraft(SiteMap et) {
        return et;
    }

    @Override
    public boolean checkKey(SiteMap et) {
        return (!ObjectUtils.isEmpty(et.getSitemapid()))&&(!Objects.isNull(this.getById(et.getSitemapid())));
    }
    @Override
    @Transactional
    public boolean save(SiteMap et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(SiteMap et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<SiteMap> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<SiteMap> list) {
        saveOrUpdateBatch(list,batchSize);
    }



    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<SiteMap> searchDefault(SiteMapSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<SiteMap> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<SiteMap>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<SiteMap> getSitemapByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<SiteMap> getSitemapByEntities(List<SiteMap> entities) {
        List ids =new ArrayList();
        for(SiteMap entity : entities){
            Serializable id=entity.getSitemapid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}



