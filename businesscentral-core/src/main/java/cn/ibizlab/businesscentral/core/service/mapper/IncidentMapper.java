package cn.ibizlab.businesscentral.core.service.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.service.domain.Incident;
import cn.ibizlab.businesscentral.core.service.filter.IncidentSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface IncidentMapper extends BaseMapper<Incident>{

    Page<Incident> searchByParentKey(IPage page, @Param("srf") IncidentSearchContext context, @Param("ew") Wrapper<Incident> wrapper) ;
    Page<Incident> searchCancel(IPage page, @Param("srf") IncidentSearchContext context, @Param("ew") Wrapper<Incident> wrapper) ;
    Page<Incident> searchDefault(IPage page, @Param("srf") IncidentSearchContext context, @Param("ew") Wrapper<Incident> wrapper) ;
    Page<Incident> searchEffective(IPage page, @Param("srf") IncidentSearchContext context, @Param("ew") Wrapper<Incident> wrapper) ;
    Page<Incident> searchResolved(IPage page, @Param("srf") IncidentSearchContext context, @Param("ew") Wrapper<Incident> wrapper) ;
    @Override
    Incident selectById(Serializable id);
    @Override
    int insert(Incident entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Incident entity);
    @Override
    int update(@Param(Constants.ENTITY) Incident entity, @Param("ew") Wrapper<Incident> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Incident> selectByCustomerid(@Param("customerid") Serializable customerid) ;

    List<Incident> selectByPrimarycontactid(@Param("contactid") Serializable contactid) ;

    List<Incident> selectByResponsiblecontactid(@Param("contactid") Serializable contactid) ;

    List<Incident> selectByContractdetailid(@Param("contractdetailid") Serializable contractdetailid) ;

    List<Incident> selectByContractid(@Param("contractid") Serializable contractid) ;

    List<Incident> selectByEntitlementid(@Param("entitlementid") Serializable entitlementid) ;

    List<Incident> selectByExistingcase(@Param("incidentid") Serializable incidentid) ;

    List<Incident> selectByMasterid(@Param("incidentid") Serializable incidentid) ;

    List<Incident> selectByParentcaseid(@Param("incidentid") Serializable incidentid) ;

    List<Incident> selectByProductid(@Param("productid") Serializable productid) ;

    List<Incident> selectByFirstresponsebykpiid(@Param("slakpiinstanceid") Serializable slakpiinstanceid) ;

    List<Incident> selectByResolvebykpiid(@Param("slakpiinstanceid") Serializable slakpiinstanceid) ;

    List<Incident> selectBySlaid(@Param("slaid") Serializable slaid) ;

    List<Incident> selectBySubjectid(@Param("subjectid") Serializable subjectid) ;

    List<Incident> selectByTransactioncurrencyid(@Param("transactioncurrencyid") Serializable transactioncurrencyid) ;

}
