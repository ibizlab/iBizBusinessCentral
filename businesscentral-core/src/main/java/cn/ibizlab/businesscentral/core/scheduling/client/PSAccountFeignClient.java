package cn.ibizlab.businesscentral.core.scheduling.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.scheduling.domain.PSAccount;
import cn.ibizlab.businesscentral.core.scheduling.filter.PSAccountSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[PSAccount] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.ibizprojectoperations-poejectoperationsapi:ibizprojectoperations-poejectoperationsapi}", contextId = "PSAccount", fallback = PSAccountFallback.class)
public interface PSAccountFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/psaccounts/select")
    Page<PSAccount> select();


    @RequestMapping(method = RequestMethod.POST, value = "/psaccounts")
    PSAccount create(@RequestBody PSAccount psaccount);

    @RequestMapping(method = RequestMethod.POST, value = "/psaccounts/batch")
    Boolean createBatch(@RequestBody List<PSAccount> psaccounts);


    @RequestMapping(method = RequestMethod.PUT, value = "/psaccounts/{accountid}")
    PSAccount update(@PathVariable("accountid") String accountid,@RequestBody PSAccount psaccount);

    @RequestMapping(method = RequestMethod.PUT, value = "/psaccounts/batch")
    Boolean updateBatch(@RequestBody List<PSAccount> psaccounts);


    @RequestMapping(method = RequestMethod.DELETE, value = "/psaccounts/{accountid}")
    Boolean remove(@PathVariable("accountid") String accountid);

    @RequestMapping(method = RequestMethod.DELETE, value = "/psaccounts/batch}")
    Boolean removeBatch(@RequestBody Collection<String> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/psaccounts/{accountid}")
    PSAccount get(@PathVariable("accountid") String accountid);


    @RequestMapping(method = RequestMethod.GET, value = "/psaccounts/getdraft")
    PSAccount getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/psaccounts/checkkey")
    Boolean checkKey(@RequestBody PSAccount psaccount);


    @RequestMapping(method = RequestMethod.POST, value = "/psaccounts/save")
    Boolean save(@RequestBody PSAccount psaccount);

    @RequestMapping(method = RequestMethod.POST, value = "/psaccounts/save")
    Boolean saveBatch(@RequestBody List<PSAccount> psaccounts);



    @RequestMapping(method = RequestMethod.POST, value = "/psaccounts/searchdefault")
    Page<PSAccount> searchDefault(@RequestBody PSAccountSearchContext context);


}
