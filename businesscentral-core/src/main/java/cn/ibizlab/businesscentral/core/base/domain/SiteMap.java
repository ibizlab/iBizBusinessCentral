package cn.ibizlab.businesscentral.core.base.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[站点地图]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SITEMAP",resultMap = "SiteMapResultMap")
public class SiteMap extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Managed
     */
    @DEField(defaultValue = "0")
    @TableField(value = "managed")
    @JSONField(name = "managed")
    @JsonProperty("managed")
    private Integer managed;
    /**
     * SolutionId
     */
    @TableField(value = "solutionid")
    @JSONField(name = "solutionid")
    @JsonProperty("solutionid")
    private String solutionid;
    /**
     * VersionNumber
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * 组件状态
     */
    @TableField(value = "componentstate")
    @JSONField(name = "componentstate")
    @JsonProperty("componentstate")
    private String componentstate;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 站点地图名称
     */
    @TableField(value = "sitemapname")
    @JSONField(name = "sitemapname")
    @JsonProperty("sitemapname")
    private String sitemapname;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * OverwriteTime
     */
    @TableField(value = "overwritetime")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overwritetime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overwritetime")
    private Timestamp overwritetime;
    /**
     * SiteMapIdUnique
     */
    @TableField(value = "sitemapidunique")
    @JSONField(name = "sitemapidunique")
    @JsonProperty("sitemapidunique")
    private String sitemapidunique;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * SiteMapXmlManaged
     */
    @TableField(value = "sitemapxmlmanaged")
    @JSONField(name = "sitemapxmlmanaged")
    @JsonProperty("sitemapxmlmanaged")
    private String sitemapxmlmanaged;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * SiteMapXml
     */
    @TableField(value = "sitemapxml")
    @JSONField(name = "sitemapxml")
    @JsonProperty("sitemapxml")
    private String sitemapxml;
    /**
     * SupportingSolutionId
     */
    @TableField(value = "supportingsolutionid")
    @JSONField(name = "supportingsolutionid")
    @JsonProperty("supportingsolutionid")
    private String supportingsolutionid;
    /**
     * SiteMapNameUnique
     */
    @TableField(value = "sitemapnameunique")
    @JSONField(name = "sitemapnameunique")
    @JsonProperty("sitemapnameunique")
    private String sitemapnameunique;
    /**
     * SiteMapId
     */
    @DEField(isKeyField=true)
    @TableId(value= "sitemapid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "sitemapid")
    @JsonProperty("sitemapid")
    private String sitemapid;
    /**
     * IsAppAware
     */
    @DEField(defaultValue = "0")
    @TableField(value = "appaware")
    @JSONField(name = "appaware")
    @JsonProperty("appaware")
    private Integer appaware;



    /**
     * 设置 [Managed]
     */
    public void setManaged(Integer managed){
        this.managed = managed ;
        this.modify("managed",managed);
    }

    /**
     * 设置 [SolutionId]
     */
    public void setSolutionid(String solutionid){
        this.solutionid = solutionid ;
        this.modify("solutionid",solutionid);
    }

    /**
     * 设置 [VersionNumber]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [组件状态]
     */
    public void setComponentstate(String componentstate){
        this.componentstate = componentstate ;
        this.modify("componentstate",componentstate);
    }

    /**
     * 设置 [站点地图名称]
     */
    public void setSitemapname(String sitemapname){
        this.sitemapname = sitemapname ;
        this.modify("sitemapname",sitemapname);
    }

    /**
     * 设置 [OverwriteTime]
     */
    public void setOverwritetime(Timestamp overwritetime){
        this.overwritetime = overwritetime ;
        this.modify("overwritetime",overwritetime);
    }

    /**
     * 格式化日期 [OverwriteTime]
     */
    public String formatOverwritetime(){
        if (this.overwritetime == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(overwritetime);
    }
    /**
     * 设置 [SiteMapIdUnique]
     */
    public void setSitemapidunique(String sitemapidunique){
        this.sitemapidunique = sitemapidunique ;
        this.modify("sitemapidunique",sitemapidunique);
    }

    /**
     * 设置 [SiteMapXmlManaged]
     */
    public void setSitemapxmlmanaged(String sitemapxmlmanaged){
        this.sitemapxmlmanaged = sitemapxmlmanaged ;
        this.modify("sitemapxmlmanaged",sitemapxmlmanaged);
    }

    /**
     * 设置 [SiteMapXml]
     */
    public void setSitemapxml(String sitemapxml){
        this.sitemapxml = sitemapxml ;
        this.modify("sitemapxml",sitemapxml);
    }

    /**
     * 设置 [SupportingSolutionId]
     */
    public void setSupportingsolutionid(String supportingsolutionid){
        this.supportingsolutionid = supportingsolutionid ;
        this.modify("supportingsolutionid",supportingsolutionid);
    }

    /**
     * 设置 [SiteMapNameUnique]
     */
    public void setSitemapnameunique(String sitemapnameunique){
        this.sitemapnameunique = sitemapnameunique ;
        this.modify("sitemapnameunique",sitemapnameunique);
    }

    /**
     * 设置 [IsAppAware]
     */
    public void setAppaware(Integer appaware){
        this.appaware = appaware ;
        this.modify("appaware",appaware);
    }


}


