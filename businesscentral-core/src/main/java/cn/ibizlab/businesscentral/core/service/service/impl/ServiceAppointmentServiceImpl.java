package cn.ibizlab.businesscentral.core.service.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.service.domain.ServiceAppointment;
import cn.ibizlab.businesscentral.core.service.filter.ServiceAppointmentSearchContext;
import cn.ibizlab.businesscentral.core.service.service.IServiceAppointmentService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.service.mapper.ServiceAppointmentMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[服务活动] 服务对象接口实现
 */
@Slf4j
@Service("ServiceAppointmentServiceImpl")
public class ServiceAppointmentServiceImpl extends ServiceImpl<ServiceAppointmentMapper, ServiceAppointment> implements IServiceAppointmentService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.service.service.IIBizServiceService ibizserviceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ISiteService siteService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ISlaService slaService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITransactionCurrencyService transactioncurrencyService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(ServiceAppointment et) {
        createIndexMajorEntityData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getActivityid()),et);
        return true;
    }

    @Override
    public void createBatch(List<ServiceAppointment> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(ServiceAppointment et) {
        activitypointerService.update(serviceappointmentInheritMapping.toActivitypointer(et));
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("activityid",et.getActivityid())))
            return false;
        CachedBeanCopier.copy(get(et.getActivityid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<ServiceAppointment> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        activitypointerService.remove(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public ServiceAppointment get(String key) {
        ServiceAppointment et = getById(key);
        if(et==null){
            et=new ServiceAppointment();
            et.setActivityid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public ServiceAppointment getDraft(ServiceAppointment et) {
        return et;
    }

    @Override
    public boolean checkKey(ServiceAppointment et) {
        return (!ObjectUtils.isEmpty(et.getActivityid()))&&(!Objects.isNull(this.getById(et.getActivityid())));
    }
    @Override
    @Transactional
    public boolean save(ServiceAppointment et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(ServiceAppointment et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<ServiceAppointment> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<ServiceAppointment> list) {
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<ServiceAppointment> selectByServiceid(String serviceid) {
        return baseMapper.selectByServiceid(serviceid);
    }

    @Override
    public void removeByServiceid(String serviceid) {
        this.remove(new QueryWrapper<ServiceAppointment>().eq("serviceid",serviceid));
    }

	@Override
    public List<ServiceAppointment> selectBySiteid(String siteid) {
        return baseMapper.selectBySiteid(siteid);
    }

    @Override
    public void removeBySiteid(String siteid) {
        this.remove(new QueryWrapper<ServiceAppointment>().eq("siteid",siteid));
    }

	@Override
    public List<ServiceAppointment> selectBySlaid(String slaid) {
        return baseMapper.selectBySlaid(slaid);
    }

    @Override
    public void removeBySlaid(String slaid) {
        this.remove(new QueryWrapper<ServiceAppointment>().eq("slaid",slaid));
    }

	@Override
    public List<ServiceAppointment> selectByTransactioncurrencyid(String transactioncurrencyid) {
        return baseMapper.selectByTransactioncurrencyid(transactioncurrencyid);
    }

    @Override
    public void removeByTransactioncurrencyid(String transactioncurrencyid) {
        this.remove(new QueryWrapper<ServiceAppointment>().eq("transactioncurrencyid",transactioncurrencyid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<ServiceAppointment> searchDefault(ServiceAppointmentSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<ServiceAppointment> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<ServiceAppointment>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }






    @Autowired
    cn.ibizlab.businesscentral.core.service.mapping.ServiceAppointmentInheritMapping serviceappointmentInheritMapping;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IActivityPointerService activitypointerService;

    /**
     * 创建索引主实体数据
     * @param et
     */
    private void createIndexMajorEntityData(ServiceAppointment et){
        if(ObjectUtils.isEmpty(et.getActivityid()))
            et.setActivityid((String)et.getDefaultKey(true));
        cn.ibizlab.businesscentral.core.base.domain.ActivityPointer activitypointer =serviceappointmentInheritMapping.toActivitypointer(et);
        activitypointer.set("activitytypecode","SERVICEAPPOINTMENT");
        activitypointerService.create(activitypointer);
    }

    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<ServiceAppointment> getServiceappointmentByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<ServiceAppointment> getServiceappointmentByEntities(List<ServiceAppointment> entities) {
        List ids =new ArrayList();
        for(ServiceAppointment entity : entities){
            Serializable id=entity.getActivityid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}



