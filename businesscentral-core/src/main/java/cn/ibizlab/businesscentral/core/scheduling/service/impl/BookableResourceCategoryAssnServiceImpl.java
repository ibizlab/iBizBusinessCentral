package cn.ibizlab.businesscentral.core.scheduling.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.scheduling.domain.BookableResourceCategoryAssn;
import cn.ibizlab.businesscentral.core.scheduling.filter.BookableResourceCategoryAssnSearchContext;
import cn.ibizlab.businesscentral.core.scheduling.service.IBookableResourceCategoryAssnService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.scheduling.mapper.BookableResourceCategoryAssnMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[可预订资源的类别关联] 服务对象接口实现
 */
@Slf4j
@Service("BookableResourceCategoryAssnServiceImpl")
public class BookableResourceCategoryAssnServiceImpl extends ServiceImpl<BookableResourceCategoryAssnMapper, BookableResourceCategoryAssn> implements IBookableResourceCategoryAssnService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.scheduling.service.IBookableResourceCategoryService bookableresourcecategoryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.scheduling.service.IBookableResourceService bookableresourceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITransactionCurrencyService transactioncurrencyService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(BookableResourceCategoryAssn et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getBookableresourcecategoryassnid()),et);
        return true;
    }

    @Override
    public void createBatch(List<BookableResourceCategoryAssn> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(BookableResourceCategoryAssn et) {
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("bookablerescategoryassnid",et.getBookableresourcecategoryassnid())))
            return false;
        CachedBeanCopier.copy(get(et.getBookableresourcecategoryassnid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<BookableResourceCategoryAssn> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public BookableResourceCategoryAssn get(String key) {
        BookableResourceCategoryAssn et = getById(key);
        if(et==null){
            et=new BookableResourceCategoryAssn();
            et.setBookableresourcecategoryassnid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public BookableResourceCategoryAssn getDraft(BookableResourceCategoryAssn et) {
        return et;
    }

    @Override
    public boolean checkKey(BookableResourceCategoryAssn et) {
        return (!ObjectUtils.isEmpty(et.getBookableresourcecategoryassnid()))&&(!Objects.isNull(this.getById(et.getBookableresourcecategoryassnid())));
    }
    @Override
    @Transactional
    public boolean save(BookableResourceCategoryAssn et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(BookableResourceCategoryAssn et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<BookableResourceCategoryAssn> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<BookableResourceCategoryAssn> list) {
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<BookableResourceCategoryAssn> selectByResourcecategory(String bookableresourcecategoryid) {
        return baseMapper.selectByResourcecategory(bookableresourcecategoryid);
    }

    @Override
    public void removeByResourcecategory(String bookableresourcecategoryid) {
        this.remove(new QueryWrapper<BookableResourceCategoryAssn>().eq("resourcecategory",bookableresourcecategoryid));
    }

	@Override
    public List<BookableResourceCategoryAssn> selectByResource(String bookableresourceid) {
        return baseMapper.selectByResource(bookableresourceid);
    }

    @Override
    public void removeByResource(String bookableresourceid) {
        this.remove(new QueryWrapper<BookableResourceCategoryAssn>().eq("resource",bookableresourceid));
    }

	@Override
    public List<BookableResourceCategoryAssn> selectByTransactioncurrencyid(String transactioncurrencyid) {
        return baseMapper.selectByTransactioncurrencyid(transactioncurrencyid);
    }

    @Override
    public void removeByTransactioncurrencyid(String transactioncurrencyid) {
        this.remove(new QueryWrapper<BookableResourceCategoryAssn>().eq("transactioncurrencyid",transactioncurrencyid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<BookableResourceCategoryAssn> searchDefault(BookableResourceCategoryAssnSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<BookableResourceCategoryAssn> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<BookableResourceCategoryAssn>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<BookableResourceCategoryAssn> getBookableresourcecategoryassnByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<BookableResourceCategoryAssn> getBookableresourcecategoryassnByEntities(List<BookableResourceCategoryAssn> entities) {
        List ids =new ArrayList();
        for(BookableResourceCategoryAssn entity : entities){
            Serializable id=entity.getBookableresourcecategoryassnid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}



