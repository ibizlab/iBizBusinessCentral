package cn.ibizlab.businesscentral.core.sales.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.sales.domain.Opportunity;
/**
 * 关系型数据实体[Opportunity] 查询条件对象
 */
@Slf4j
@Data
public class OpportunitySearchContext extends QueryWrapperContext<Opportunity> {

	private String n_pricingerrorcode_eq;//[定价错误]
	public void setN_pricingerrorcode_eq(String n_pricingerrorcode_eq) {
        this.n_pricingerrorcode_eq = n_pricingerrorcode_eq;
        if(!ObjectUtils.isEmpty(this.n_pricingerrorcode_eq)){
            this.getSearchCond().eq("pricingerrorcode", n_pricingerrorcode_eq);
        }
    }
	private String n_opportunityratingcode_eq;//[等级]
	public void setN_opportunityratingcode_eq(String n_opportunityratingcode_eq) {
        this.n_opportunityratingcode_eq = n_opportunityratingcode_eq;
        if(!ObjectUtils.isEmpty(this.n_opportunityratingcode_eq)){
            this.getSearchCond().eq("opportunityratingcode", n_opportunityratingcode_eq);
        }
    }
	private String n_purchaseprocess_eq;//[采购程序]
	public void setN_purchaseprocess_eq(String n_purchaseprocess_eq) {
        this.n_purchaseprocess_eq = n_purchaseprocess_eq;
        if(!ObjectUtils.isEmpty(this.n_purchaseprocess_eq)){
            this.getSearchCond().eq("purchaseprocess", n_purchaseprocess_eq);
        }
    }
	private Integer n_statuscode_eq;//[状态描述]
	public void setN_statuscode_eq(Integer n_statuscode_eq) {
        this.n_statuscode_eq = n_statuscode_eq;
        if(!ObjectUtils.isEmpty(this.n_statuscode_eq)){
            this.getSearchCond().eq("statuscode", n_statuscode_eq);
        }
    }
	private String n_salesstage_eq;//[销售阶段]
	public void setN_salesstage_eq(String n_salesstage_eq) {
        this.n_salesstage_eq = n_salesstage_eq;
        if(!ObjectUtils.isEmpty(this.n_salesstage_eq)){
            this.getSearchCond().eq("salesstage", n_salesstage_eq);
        }
    }
	private String n_salesstagecode_eq;//[处理代码]
	public void setN_salesstagecode_eq(String n_salesstagecode_eq) {
        this.n_salesstagecode_eq = n_salesstagecode_eq;
        if(!ObjectUtils.isEmpty(this.n_salesstagecode_eq)){
            this.getSearchCond().eq("salesstagecode", n_salesstagecode_eq);
        }
    }
	private String n_initialcommunication_eq;//[初始通信]
	public void setN_initialcommunication_eq(String n_initialcommunication_eq) {
        this.n_initialcommunication_eq = n_initialcommunication_eq;
        if(!ObjectUtils.isEmpty(this.n_initialcommunication_eq)){
            this.getSearchCond().eq("initialcommunication", n_initialcommunication_eq);
        }
    }
	private String n_budgetstatus_eq;//[预算]
	public void setN_budgetstatus_eq(String n_budgetstatus_eq) {
        this.n_budgetstatus_eq = n_budgetstatus_eq;
        if(!ObjectUtils.isEmpty(this.n_budgetstatus_eq)){
            this.getSearchCond().eq("budgetstatus", n_budgetstatus_eq);
        }
    }
	private String n_need_eq;//[需求]
	public void setN_need_eq(String n_need_eq) {
        this.n_need_eq = n_need_eq;
        if(!ObjectUtils.isEmpty(this.n_need_eq)){
            this.getSearchCond().eq("need", n_need_eq);
        }
    }
	private String n_prioritycode_eq;//[优先级]
	public void setN_prioritycode_eq(String n_prioritycode_eq) {
        this.n_prioritycode_eq = n_prioritycode_eq;
        if(!ObjectUtils.isEmpty(this.n_prioritycode_eq)){
            this.getSearchCond().eq("prioritycode", n_prioritycode_eq);
        }
    }
	private String n_opportunityname_like;//[商机名称]
	public void setN_opportunityname_like(String n_opportunityname_like) {
        this.n_opportunityname_like = n_opportunityname_like;
        if(!ObjectUtils.isEmpty(this.n_opportunityname_like)){
            this.getSearchCond().like("opportunityname", n_opportunityname_like);
        }
    }
	private String n_timeline_eq;//[日程表]
	public void setN_timeline_eq(String n_timeline_eq) {
        this.n_timeline_eq = n_timeline_eq;
        if(!ObjectUtils.isEmpty(this.n_timeline_eq)){
            this.getSearchCond().eq("timeline", n_timeline_eq);
        }
    }
	private Integer n_statecode_eq;//[状态]
	public void setN_statecode_eq(Integer n_statecode_eq) {
        this.n_statecode_eq = n_statecode_eq;
        if(!ObjectUtils.isEmpty(this.n_statecode_eq)){
            this.getSearchCond().eq("statecode", n_statecode_eq);
        }
    }
	private String n_purchasetimeframe_eq;//[购买时间范围]
	public void setN_purchasetimeframe_eq(String n_purchasetimeframe_eq) {
        this.n_purchasetimeframe_eq = n_purchasetimeframe_eq;
        if(!ObjectUtils.isEmpty(this.n_purchasetimeframe_eq)){
            this.getSearchCond().eq("purchasetimeframe", n_purchasetimeframe_eq);
        }
    }
	private String n_parentaccountname_eq;//[帐户]
	public void setN_parentaccountname_eq(String n_parentaccountname_eq) {
        this.n_parentaccountname_eq = n_parentaccountname_eq;
        if(!ObjectUtils.isEmpty(this.n_parentaccountname_eq)){
            this.getSearchCond().eq("parentaccountname", n_parentaccountname_eq);
        }
    }
	private String n_parentaccountname_like;//[帐户]
	public void setN_parentaccountname_like(String n_parentaccountname_like) {
        this.n_parentaccountname_like = n_parentaccountname_like;
        if(!ObjectUtils.isEmpty(this.n_parentaccountname_like)){
            this.getSearchCond().like("parentaccountname", n_parentaccountname_like);
        }
    }
	private String n_originatingleadname_eq;//[原始潜在顾客]
	public void setN_originatingleadname_eq(String n_originatingleadname_eq) {
        this.n_originatingleadname_eq = n_originatingleadname_eq;
        if(!ObjectUtils.isEmpty(this.n_originatingleadname_eq)){
            this.getSearchCond().eq("originatingleadname", n_originatingleadname_eq);
        }
    }
	private String n_originatingleadname_like;//[原始潜在顾客]
	public void setN_originatingleadname_like(String n_originatingleadname_like) {
        this.n_originatingleadname_like = n_originatingleadname_like;
        if(!ObjectUtils.isEmpty(this.n_originatingleadname_like)){
            this.getSearchCond().like("originatingleadname", n_originatingleadname_like);
        }
    }
	private String n_parentcontactname_eq;//[联系人]
	public void setN_parentcontactname_eq(String n_parentcontactname_eq) {
        this.n_parentcontactname_eq = n_parentcontactname_eq;
        if(!ObjectUtils.isEmpty(this.n_parentcontactname_eq)){
            this.getSearchCond().eq("parentcontactname", n_parentcontactname_eq);
        }
    }
	private String n_parentcontactname_like;//[联系人]
	public void setN_parentcontactname_like(String n_parentcontactname_like) {
        this.n_parentcontactname_like = n_parentcontactname_like;
        if(!ObjectUtils.isEmpty(this.n_parentcontactname_like)){
            this.getSearchCond().like("parentcontactname", n_parentcontactname_like);
        }
    }
	private String n_currencyname_eq;//[货币]
	public void setN_currencyname_eq(String n_currencyname_eq) {
        this.n_currencyname_eq = n_currencyname_eq;
        if(!ObjectUtils.isEmpty(this.n_currencyname_eq)){
            this.getSearchCond().eq("currencyname", n_currencyname_eq);
        }
    }
	private String n_currencyname_like;//[货币]
	public void setN_currencyname_like(String n_currencyname_like) {
        this.n_currencyname_like = n_currencyname_like;
        if(!ObjectUtils.isEmpty(this.n_currencyname_like)){
            this.getSearchCond().like("currencyname", n_currencyname_like);
        }
    }
	private String n_pricelevelname_eq;//[价目表]
	public void setN_pricelevelname_eq(String n_pricelevelname_eq) {
        this.n_pricelevelname_eq = n_pricelevelname_eq;
        if(!ObjectUtils.isEmpty(this.n_pricelevelname_eq)){
            this.getSearchCond().eq("pricelevelname", n_pricelevelname_eq);
        }
    }
	private String n_pricelevelname_like;//[价目表]
	public void setN_pricelevelname_like(String n_pricelevelname_like) {
        this.n_pricelevelname_like = n_pricelevelname_like;
        if(!ObjectUtils.isEmpty(this.n_pricelevelname_like)){
            this.getSearchCond().like("pricelevelname", n_pricelevelname_like);
        }
    }
	private String n_campaignname_eq;//[源市场活动]
	public void setN_campaignname_eq(String n_campaignname_eq) {
        this.n_campaignname_eq = n_campaignname_eq;
        if(!ObjectUtils.isEmpty(this.n_campaignname_eq)){
            this.getSearchCond().eq("campaignname", n_campaignname_eq);
        }
    }
	private String n_campaignname_like;//[源市场活动]
	public void setN_campaignname_like(String n_campaignname_like) {
        this.n_campaignname_like = n_campaignname_like;
        if(!ObjectUtils.isEmpty(this.n_campaignname_like)){
            this.getSearchCond().like("campaignname", n_campaignname_like);
        }
    }
	private String n_parentcontactid_eq;//[联系人]
	public void setN_parentcontactid_eq(String n_parentcontactid_eq) {
        this.n_parentcontactid_eq = n_parentcontactid_eq;
        if(!ObjectUtils.isEmpty(this.n_parentcontactid_eq)){
            this.getSearchCond().eq("parentcontactid", n_parentcontactid_eq);
        }
    }
	private String n_campaignid_eq;//[源市场活动]
	public void setN_campaignid_eq(String n_campaignid_eq) {
        this.n_campaignid_eq = n_campaignid_eq;
        if(!ObjectUtils.isEmpty(this.n_campaignid_eq)){
            this.getSearchCond().eq("campaignid", n_campaignid_eq);
        }
    }
	private String n_transactioncurrencyid_eq;//[货币]
	public void setN_transactioncurrencyid_eq(String n_transactioncurrencyid_eq) {
        this.n_transactioncurrencyid_eq = n_transactioncurrencyid_eq;
        if(!ObjectUtils.isEmpty(this.n_transactioncurrencyid_eq)){
            this.getSearchCond().eq("transactioncurrencyid", n_transactioncurrencyid_eq);
        }
    }
	private String n_parentaccountid_eq;//[帐户]
	public void setN_parentaccountid_eq(String n_parentaccountid_eq) {
        this.n_parentaccountid_eq = n_parentaccountid_eq;
        if(!ObjectUtils.isEmpty(this.n_parentaccountid_eq)){
            this.getSearchCond().eq("parentaccountid", n_parentaccountid_eq);
        }
    }
	private String n_pricelevelid_eq;//[价目表]
	public void setN_pricelevelid_eq(String n_pricelevelid_eq) {
        this.n_pricelevelid_eq = n_pricelevelid_eq;
        if(!ObjectUtils.isEmpty(this.n_pricelevelid_eq)){
            this.getSearchCond().eq("pricelevelid", n_pricelevelid_eq);
        }
    }
	private String n_originatingleadid_eq;//[原始潜在顾客]
	public void setN_originatingleadid_eq(String n_originatingleadid_eq) {
        this.n_originatingleadid_eq = n_originatingleadid_eq;
        if(!ObjectUtils.isEmpty(this.n_originatingleadid_eq)){
            this.getSearchCond().eq("originatingleadid", n_originatingleadid_eq);
        }
    }
	private String n_slaid_eq;//[SLA]
	public void setN_slaid_eq(String n_slaid_eq) {
        this.n_slaid_eq = n_slaid_eq;
        if(!ObjectUtils.isEmpty(this.n_slaid_eq)){
            this.getSearchCond().eq("slaid", n_slaid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("opportunityname", query)   
            );
		 }
	}
}



