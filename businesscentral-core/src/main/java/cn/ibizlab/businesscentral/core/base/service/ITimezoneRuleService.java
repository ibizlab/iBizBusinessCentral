package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.TimezoneRule;
import cn.ibizlab.businesscentral.core.base.filter.TimezoneRuleSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[TimezoneRule] 服务对象接口
 */
public interface ITimezoneRuleService extends IService<TimezoneRule>{

    boolean create(TimezoneRule et) ;
    void createBatch(List<TimezoneRule> list) ;
    boolean update(TimezoneRule et) ;
    void updateBatch(List<TimezoneRule> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    TimezoneRule get(String key) ;
    TimezoneRule getDraft(TimezoneRule et) ;
    boolean checkKey(TimezoneRule et) ;
    boolean save(TimezoneRule et) ;
    void saveBatch(List<TimezoneRule> list) ;
    Page<TimezoneRule> searchDefault(TimezoneRuleSearchContext context) ;
    List<TimezoneRule> selectByTimezonedefinitionid(String timezonedefinitionid) ;
    void removeByTimezonedefinitionid(String timezonedefinitionid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<TimezoneRule> getTimezoneruleByIds(List<String> ids) ;
    List<TimezoneRule> getTimezoneruleByEntities(List<TimezoneRule> entities) ;
}


