package cn.ibizlab.businesscentral.core.marketing.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[市场营销列表]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "LIST",resultMap = "IBizListResultMap")
public class IBizList extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 查询
     */
    @TableField(value = "query")
    @JSONField(name = "query")
    @JsonProperty("query")
    private String query;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 已锁定
     */
    @DEField(defaultValue = "0")
    @TableField(value = "lockstatus")
    @JSONField(name = "lockstatus")
    @JsonProperty("lockstatus")
    private Integer lockstatus;
    /**
     * 用途
     */
    @TableField(value = "purpose")
    @JSONField(name = "purpose")
    @JsonProperty("purpose")
    private String purpose;
    /**
     * 上次使用时间
     */
    @TableField(value = "lastusedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "lastusedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("lastusedon")
    private Timestamp lastusedon;
    /**
     * 负责人
     */
    @TableField(value = "ownerid")
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;
    /**
     * 负责人
     */
    @TableField(value = "ownername")
    @JSONField(name = "ownername")
    @JsonProperty("ownername")
    private String ownername;
    /**
     * 列表
     */
    @DEField(isKeyField=true)
    @TableId(value= "listid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "listid")
    @JsonProperty("listid")
    private String listid;
    /**
     * 目标对象
     */
    @TableField(value = "membertype")
    @JSONField(name = "membertype")
    @JsonProperty("membertype")
    private Integer membertype;
    /**
     * 排除退出的成员
     */
    @DEField(defaultValue = "1")
    @TableField(value = "donotsendonoptout")
    @JSONField(name = "donotsendonoptout")
    @JsonProperty("donotsendonoptout")
    private Integer donotsendonoptout;
    /**
     * Version Number
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * 成本
     */
    @TableField(value = "cost")
    @JSONField(name = "cost")
    @JsonProperty("cost")
    private BigDecimal cost;
    /**
     * 状态
     */
    @TableField(value = "statecode")
    @JSONField(name = "statecode")
    @JsonProperty("statecode")
    private Integer statecode;
    /**
     * 汇率
     */
    @TableField(value = "exchangerate")
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;
    /**
     * 负责人类型
     */
    @TableField(value = "ownertype")
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;
    /**
     * 名称
     */
    @TableField(value = "listname")
    @JSONField(name = "listname")
    @JsonProperty("listname")
    private String listname;
    /**
     * 状态描述
     */
    @TableField(value = "statuscode")
    @JSONField(name = "statuscode")
    @JsonProperty("statuscode")
    private Integer statuscode;
    /**
     * 说明
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 类型
     */
    @DEField(defaultValue = "0")
    @TableField(value = "type")
    @JSONField(name = "type")
    @JsonProperty("type")
    private Integer type;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * Import Sequence Number
     */
    @TableField(value = "importsequencenumber")
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * Process Id
     */
    @TableField(value = "processid")
    @JSONField(name = "processid")
    @JsonProperty("processid")
    private String processid;
    /**
     * 成本 (Base)
     */
    @DEField(name = "cost_base")
    @TableField(value = "cost_base")
    @JSONField(name = "cost_base")
    @JsonProperty("cost_base")
    private BigDecimal costBase;
    /**
     * 成员计数
     */
    @TableField(value = "membercount")
    @JSONField(name = "membercount")
    @JsonProperty("membercount")
    private Integer membercount;
    /**
     * 忽略停用列表成员
     */
    @DEField(defaultValue = "1")
    @TableField(value = "ignoreinactivelistmembers")
    @JSONField(name = "ignoreinactivelistmembers")
    @JsonProperty("ignoreinactivelistmembers")
    private Integer ignoreinactivelistmembers;
    /**
     * Stage Id
     */
    @TableField(value = "stageid")
    @JSONField(name = "stageid")
    @JsonProperty("stageid")
    private String stageid;
    /**
     * 来源
     */
    @TableField(value = "source")
    @JSONField(name = "source")
    @JsonProperty("source")
    private String source;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * UTC Conversion Time Zone Code
     */
    @TableField(value = "utcconversiontimezonecode")
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;
    /**
     * 成员类型
     */
    @TableField(value = "createdfromcode")
    @JSONField(name = "createdfromcode")
    @JsonProperty("createdfromcode")
    private String createdfromcode;
    /**
     * Record Created On
     */
    @TableField(value = "overriddencreatedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;
    /**
     * Time Zone Rule Version Number
     */
    @TableField(value = "timezoneruleversionnumber")
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;
    /**
     * Traversed Path
     */
    @TableField(value = "traversedpath")
    @JSONField(name = "traversedpath")
    @JsonProperty("traversedpath")
    private String traversedpath;
    /**
     * 货币
     */
    @TableField(exist = false)
    @JSONField(name = "currencyname")
    @JsonProperty("currencyname")
    private String currencyname;
    /**
     * 货币
     */
    @TableField(value = "transactioncurrencyid")
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency transactioncurrency;



    /**
     * 设置 [查询]
     */
    public void setQuery(String query){
        this.query = query ;
        this.modify("query",query);
    }

    /**
     * 设置 [已锁定]
     */
    public void setLockstatus(Integer lockstatus){
        this.lockstatus = lockstatus ;
        this.modify("lockstatus",lockstatus);
    }

    /**
     * 设置 [用途]
     */
    public void setPurpose(String purpose){
        this.purpose = purpose ;
        this.modify("purpose",purpose);
    }

    /**
     * 设置 [上次使用时间]
     */
    public void setLastusedon(Timestamp lastusedon){
        this.lastusedon = lastusedon ;
        this.modify("lastusedon",lastusedon);
    }

    /**
     * 格式化日期 [上次使用时间]
     */
    public String formatLastusedon(){
        if (this.lastusedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(lastusedon);
    }
    /**
     * 设置 [负责人]
     */
    public void setOwnerid(String ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnername(String ownername){
        this.ownername = ownername ;
        this.modify("ownername",ownername);
    }

    /**
     * 设置 [目标对象]
     */
    public void setMembertype(Integer membertype){
        this.membertype = membertype ;
        this.modify("membertype",membertype);
    }

    /**
     * 设置 [排除退出的成员]
     */
    public void setDonotsendonoptout(Integer donotsendonoptout){
        this.donotsendonoptout = donotsendonoptout ;
        this.modify("donotsendonoptout",donotsendonoptout);
    }

    /**
     * 设置 [Version Number]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [成本]
     */
    public void setCost(BigDecimal cost){
        this.cost = cost ;
        this.modify("cost",cost);
    }

    /**
     * 设置 [状态]
     */
    public void setStatecode(Integer statecode){
        this.statecode = statecode ;
        this.modify("statecode",statecode);
    }

    /**
     * 设置 [汇率]
     */
    public void setExchangerate(BigDecimal exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [负责人类型]
     */
    public void setOwnertype(String ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [名称]
     */
    public void setListname(String listname){
        this.listname = listname ;
        this.modify("listname",listname);
    }

    /**
     * 设置 [状态描述]
     */
    public void setStatuscode(Integer statuscode){
        this.statuscode = statuscode ;
        this.modify("statuscode",statuscode);
    }

    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [类型]
     */
    public void setType(Integer type){
        this.type = type ;
        this.modify("type",type);
    }

    /**
     * 设置 [Import Sequence Number]
     */
    public void setImportsequencenumber(Integer importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [Process Id]
     */
    public void setProcessid(String processid){
        this.processid = processid ;
        this.modify("processid",processid);
    }

    /**
     * 设置 [成本 (Base)]
     */
    public void setCostBase(BigDecimal costBase){
        this.costBase = costBase ;
        this.modify("cost_base",costBase);
    }

    /**
     * 设置 [成员计数]
     */
    public void setMembercount(Integer membercount){
        this.membercount = membercount ;
        this.modify("membercount",membercount);
    }

    /**
     * 设置 [忽略停用列表成员]
     */
    public void setIgnoreinactivelistmembers(Integer ignoreinactivelistmembers){
        this.ignoreinactivelistmembers = ignoreinactivelistmembers ;
        this.modify("ignoreinactivelistmembers",ignoreinactivelistmembers);
    }

    /**
     * 设置 [Stage Id]
     */
    public void setStageid(String stageid){
        this.stageid = stageid ;
        this.modify("stageid",stageid);
    }

    /**
     * 设置 [来源]
     */
    public void setSource(String source){
        this.source = source ;
        this.modify("source",source);
    }

    /**
     * 设置 [UTC Conversion Time Zone Code]
     */
    public void setUtcconversiontimezonecode(Integer utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [成员类型]
     */
    public void setCreatedfromcode(String createdfromcode){
        this.createdfromcode = createdfromcode ;
        this.modify("createdfromcode",createdfromcode);
    }

    /**
     * 设置 [Record Created On]
     */
    public void setOverriddencreatedon(Timestamp overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 格式化日期 [Record Created On]
     */
    public String formatOverriddencreatedon(){
        if (this.overriddencreatedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(overriddencreatedon);
    }
    /**
     * 设置 [Time Zone Rule Version Number]
     */
    public void setTimezoneruleversionnumber(Integer timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [Traversed Path]
     */
    public void setTraversedpath(String traversedpath){
        this.traversedpath = traversedpath ;
        this.modify("traversedpath",traversedpath);
    }

    /**
     * 设置 [货币]
     */
    public void setTransactioncurrencyid(String transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }


}


