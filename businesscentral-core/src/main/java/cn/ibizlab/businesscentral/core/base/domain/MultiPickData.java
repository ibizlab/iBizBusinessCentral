package cn.ibizlab.businesscentral.core.base.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[多类选择实体]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "MULTIPICKDATA",resultMap = "MultiPickDataResultMap")
public class MultiPickData extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 信息描述
     */
    @TableField(value = "pickdatainfo")
    @JSONField(name = "pickdatainfo")
    @JsonProperty("pickdatainfo")
    private String pickdatainfo;
    /**
     * 用户数据
     */
    @TableField(value = "userdata")
    @JSONField(name = "userdata")
    @JsonProperty("userdata")
    private String userdata;
    /**
     * 数据名称
     */
    @TableField(value = "pickdataname")
    @JSONField(name = "pickdataname")
    @JsonProperty("pickdataname")
    private String pickdataname;
    /**
     * 数据类型
     */
    @TableField(value = "pickdatatype")
    @JSONField(name = "pickdatatype")
    @JsonProperty("pickdatatype")
    private String pickdatatype;
    /**
     * 用户时间2
     */
    @TableField(value = "userdate2")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "userdate2" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("userdate2")
    private Timestamp userdate2;
    /**
     * 用户时间
     */
    @TableField(value = "userdate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "userdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("userdate")
    private Timestamp userdate;
    /**
     * 数据标识
     */
    @DEField(isKeyField=true)
    @TableId(value= "pickdataid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "pickdataid")
    @JsonProperty("pickdataid")
    private String pickdataid;
    /**
     * 用户数据4
     */
    @TableField(value = "userdata4")
    @JSONField(name = "userdata4")
    @JsonProperty("userdata4")
    private String userdata4;
    /**
     * 用户数据2
     */
    @TableField(value = "userdata2")
    @JSONField(name = "userdata2")
    @JsonProperty("userdata2")
    private String userdata2;
    /**
     * 用户数据3
     */
    @TableField(value = "userdata3")
    @JSONField(name = "userdata3")
    @JsonProperty("userdata3")
    private String userdata3;



    /**
     * 设置 [信息描述]
     */
    public void setPickdatainfo(String pickdatainfo){
        this.pickdatainfo = pickdatainfo ;
        this.modify("pickdatainfo",pickdatainfo);
    }

    /**
     * 设置 [用户数据]
     */
    public void setUserdata(String userdata){
        this.userdata = userdata ;
        this.modify("userdata",userdata);
    }

    /**
     * 设置 [数据名称]
     */
    public void setPickdataname(String pickdataname){
        this.pickdataname = pickdataname ;
        this.modify("pickdataname",pickdataname);
    }

    /**
     * 设置 [数据类型]
     */
    public void setPickdatatype(String pickdatatype){
        this.pickdatatype = pickdatatype ;
        this.modify("pickdatatype",pickdatatype);
    }

    /**
     * 设置 [用户时间2]
     */
    public void setUserdate2(Timestamp userdate2){
        this.userdate2 = userdate2 ;
        this.modify("userdate2",userdate2);
    }

    /**
     * 格式化日期 [用户时间2]
     */
    public String formatUserdate2(){
        if (this.userdate2 == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(userdate2);
    }
    /**
     * 设置 [用户时间]
     */
    public void setUserdate(Timestamp userdate){
        this.userdate = userdate ;
        this.modify("userdate",userdate);
    }

    /**
     * 格式化日期 [用户时间]
     */
    public String formatUserdate(){
        if (this.userdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(userdate);
    }
    /**
     * 设置 [用户数据4]
     */
    public void setUserdata4(String userdata4){
        this.userdata4 = userdata4 ;
        this.modify("userdata4",userdata4);
    }

    /**
     * 设置 [用户数据2]
     */
    public void setUserdata2(String userdata2){
        this.userdata2 = userdata2 ;
        this.modify("userdata2",userdata2);
    }

    /**
     * 设置 [用户数据3]
     */
    public void setUserdata3(String userdata3){
        this.userdata3 = userdata3 ;
        this.modify("userdata3",userdata3);
    }


}


