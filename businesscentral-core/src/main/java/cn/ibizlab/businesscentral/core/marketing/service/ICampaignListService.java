package cn.ibizlab.businesscentral.core.marketing.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.marketing.domain.CampaignList;
import cn.ibizlab.businesscentral.core.marketing.filter.CampaignListSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[CampaignList] 服务对象接口
 */
public interface ICampaignListService extends IService<CampaignList>{

    boolean create(CampaignList et) ;
    void createBatch(List<CampaignList> list) ;
    boolean update(CampaignList et) ;
    void updateBatch(List<CampaignList> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    CampaignList get(String key) ;
    CampaignList getDraft(CampaignList et) ;
    boolean checkKey(CampaignList et) ;
    boolean save(CampaignList et) ;
    void saveBatch(List<CampaignList> list) ;
    Page<CampaignList> searchDefault(CampaignListSearchContext context) ;
    List<CampaignList> selectByEntityid(String campaignid) ;
    void removeByEntityid(String campaignid) ;
    List<CampaignList> selectByEntity2id(String listid) ;
    void removeByEntity2id(String listid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<CampaignList> getCampaignlistByIds(List<String> ids) ;
    List<CampaignList> getCampaignlistByEntities(List<CampaignList> entities) ;
}


