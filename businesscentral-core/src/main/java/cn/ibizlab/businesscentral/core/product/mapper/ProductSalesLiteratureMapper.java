package cn.ibizlab.businesscentral.core.product.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.product.domain.ProductSalesLiterature;
import cn.ibizlab.businesscentral.core.product.filter.ProductSalesLiteratureSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface ProductSalesLiteratureMapper extends BaseMapper<ProductSalesLiterature>{

    Page<ProductSalesLiterature> searchDefault(IPage page, @Param("srf") ProductSalesLiteratureSearchContext context, @Param("ew") Wrapper<ProductSalesLiterature> wrapper) ;
    @Override
    ProductSalesLiterature selectById(Serializable id);
    @Override
    int insert(ProductSalesLiterature entity);
    @Override
    int updateById(@Param(Constants.ENTITY) ProductSalesLiterature entity);
    @Override
    int update(@Param(Constants.ENTITY) ProductSalesLiterature entity, @Param("ew") Wrapper<ProductSalesLiterature> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<ProductSalesLiterature> selectByEntityid(@Param("productid") Serializable productid) ;

    List<ProductSalesLiterature> selectByEntity2id(@Param("salesliteratureid") Serializable salesliteratureid) ;

}
