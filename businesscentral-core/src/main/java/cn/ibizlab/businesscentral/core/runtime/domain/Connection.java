package cn.ibizlab.businesscentral.core.runtime.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[连接]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "CONNECTION",resultMap = "ConnectionResultMap")
public class Connection extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 类型(目标)
     */
    @TableField(value = "record2objecttypecode")
    @JSONField(name = "record2objecttypecode")
    @JsonProperty("record2objecttypecode")
    private String record2objecttypecode;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * EntityImage_Timestamp
     */
    @DEField(name = "entityimage_timestamp")
    @TableField(value = "entityimage_timestamp")
    @JSONField(name = "entityimage_timestamp")
    @JsonProperty("entityimage_timestamp")
    private BigInteger entityimageTimestamp;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 类型(源)
     */
    @TableField(value = "record1objecttypecode")
    @JSONField(name = "record1objecttypecode")
    @JsonProperty("record1objecttypecode")
    private String record1objecttypecode;
    /**
     * 已连接到
     */
    @TableField(value = "record2id")
    @JSONField(name = "record2id")
    @JsonProperty("record2id")
    private String record2id;
    /**
     * 主记录
     */
    @DEField(defaultValue = "0")
    @TableField(value = "master")
    @JSONField(name = "master")
    @JsonProperty("master")
    private Integer master;
    /**
     * 连接自
     */
    @TableField(value = "record1idobjecttypecode")
    @JSONField(name = "record1idobjecttypecode")
    @JsonProperty("record1idobjecttypecode")
    private String record1idobjecttypecode;
    /**
     * 负责人
     */
    @TableField(value = "ownerid")
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 正在启动
     */
    @TableField(value = "effectivestart")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "effectivestart" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("effectivestart")
    private Timestamp effectivestart;
    /**
     * 连接自
     */
    @TableField(value = "record1id")
    @JSONField(name = "record1id")
    @JsonProperty("record1id")
    private String record1id;
    /**
     * 创建记录的时间
     */
    @TableField(value = "overriddencreatedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;
    /**
     * 导入序列号
     */
    @TableField(value = "importsequencenumber")
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;
    /**
     * 负责人
     */
    @TableField(value = "ownername")
    @JSONField(name = "ownername")
    @JsonProperty("ownername")
    private String ownername;
    /**
     * 负责人类型
     */
    @TableField(value = "ownertype")
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;
    /**
     * 已连接到
     */
    @TableField(value = "record2idobjecttypecode")
    @JSONField(name = "record2idobjecttypecode")
    @JsonProperty("record2idobjecttypecode")
    private String record2idobjecttypecode;
    /**
     * 汇率
     */
    @TableField(value = "exchangerate")
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;
    /**
     * 实体图像 ID
     */
    @TableField(value = "entityimageid")
    @JSONField(name = "entityimageid")
    @JsonProperty("entityimageid")
    private String entityimageid;
    /**
     * 版本号
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * 正在结束
     */
    @TableField(value = "effectiveend")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "effectiveend" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("effectiveend")
    private Timestamp effectiveend;
    /**
     * EntityImage_URL
     */
    @DEField(name = "entityimage_url")
    @TableField(value = "entityimage_url")
    @JSONField(name = "entityimage_url")
    @JsonProperty("entityimage_url")
    private String entityimageUrl;
    /**
     * 关联名称
     */
    @TableField(value = "connectionname")
    @JSONField(name = "connectionname")
    @JsonProperty("connectionname")
    private String connectionname;
    /**
     * 状态描述
     */
    @DEField(defaultValue = "1")
    @TableField(value = "statuscode")
    @JSONField(name = "statuscode")
    @JsonProperty("statuscode")
    private Integer statuscode;
    /**
     * 连接
     */
    @DEField(isKeyField=true)
    @TableId(value= "connectionid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "connectionid")
    @JsonProperty("connectionid")
    private String connectionid;
    /**
     * 状态
     */
    @DEField(defaultValue = "0")
    @TableField(value = "statecode")
    @JSONField(name = "statecode")
    @JsonProperty("statecode")
    private Integer statecode;
    /**
     * 说明
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 实体图像
     */
    @TableField(value = "entityimage")
    @JSONField(name = "entityimage")
    @JsonProperty("entityimage")
    private String entityimage;
    /**
     * 角色
     */
    @TableField(value = "record1rolename")
    @JSONField(name = "record1rolename")
    @JsonProperty("record1rolename")
    private String record1rolename;
    /**
     * 角色（目标）
     */
    @TableField(value = "record2rolename")
    @JSONField(name = "record2rolename")
    @JsonProperty("record2rolename")
    private String record2rolename;
    /**
     * 角色(源)
     */
    @TableField(value = "record1roleid")
    @JSONField(name = "record1roleid")
    @JsonProperty("record1roleid")
    private String record1roleid;
    /**
     * 货币
     */
    @TableField(value = "transactioncurrencyid")
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;
    /**
     * 角色(目标)
     */
    @TableField(value = "record2roleid")
    @JSONField(name = "record2roleid")
    @JsonProperty("record2roleid")
    private String record2roleid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.runtime.domain.ConnectionRole record1role;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.runtime.domain.ConnectionRole record2role;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency transactioncurrency;



    /**
     * 设置 [类型(目标)]
     */
    public void setRecord2objecttypecode(String record2objecttypecode){
        this.record2objecttypecode = record2objecttypecode ;
        this.modify("record2objecttypecode",record2objecttypecode);
    }

    /**
     * 设置 [EntityImage_Timestamp]
     */
    public void setEntityimageTimestamp(BigInteger entityimageTimestamp){
        this.entityimageTimestamp = entityimageTimestamp ;
        this.modify("entityimage_timestamp",entityimageTimestamp);
    }

    /**
     * 设置 [类型(源)]
     */
    public void setRecord1objecttypecode(String record1objecttypecode){
        this.record1objecttypecode = record1objecttypecode ;
        this.modify("record1objecttypecode",record1objecttypecode);
    }

    /**
     * 设置 [已连接到]
     */
    public void setRecord2id(String record2id){
        this.record2id = record2id ;
        this.modify("record2id",record2id);
    }

    /**
     * 设置 [主记录]
     */
    public void setMaster(Integer master){
        this.master = master ;
        this.modify("master",master);
    }

    /**
     * 设置 [连接自]
     */
    public void setRecord1idobjecttypecode(String record1idobjecttypecode){
        this.record1idobjecttypecode = record1idobjecttypecode ;
        this.modify("record1idobjecttypecode",record1idobjecttypecode);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnerid(String ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [正在启动]
     */
    public void setEffectivestart(Timestamp effectivestart){
        this.effectivestart = effectivestart ;
        this.modify("effectivestart",effectivestart);
    }

    /**
     * 格式化日期 [正在启动]
     */
    public String formatEffectivestart(){
        if (this.effectivestart == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(effectivestart);
    }
    /**
     * 设置 [连接自]
     */
    public void setRecord1id(String record1id){
        this.record1id = record1id ;
        this.modify("record1id",record1id);
    }

    /**
     * 设置 [创建记录的时间]
     */
    public void setOverriddencreatedon(Timestamp overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 格式化日期 [创建记录的时间]
     */
    public String formatOverriddencreatedon(){
        if (this.overriddencreatedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(overriddencreatedon);
    }
    /**
     * 设置 [导入序列号]
     */
    public void setImportsequencenumber(Integer importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnername(String ownername){
        this.ownername = ownername ;
        this.modify("ownername",ownername);
    }

    /**
     * 设置 [负责人类型]
     */
    public void setOwnertype(String ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [已连接到]
     */
    public void setRecord2idobjecttypecode(String record2idobjecttypecode){
        this.record2idobjecttypecode = record2idobjecttypecode ;
        this.modify("record2idobjecttypecode",record2idobjecttypecode);
    }

    /**
     * 设置 [汇率]
     */
    public void setExchangerate(BigDecimal exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [实体图像 ID]
     */
    public void setEntityimageid(String entityimageid){
        this.entityimageid = entityimageid ;
        this.modify("entityimageid",entityimageid);
    }

    /**
     * 设置 [版本号]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [正在结束]
     */
    public void setEffectiveend(Timestamp effectiveend){
        this.effectiveend = effectiveend ;
        this.modify("effectiveend",effectiveend);
    }

    /**
     * 格式化日期 [正在结束]
     */
    public String formatEffectiveend(){
        if (this.effectiveend == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(effectiveend);
    }
    /**
     * 设置 [EntityImage_URL]
     */
    public void setEntityimageUrl(String entityimageUrl){
        this.entityimageUrl = entityimageUrl ;
        this.modify("entityimage_url",entityimageUrl);
    }

    /**
     * 设置 [关联名称]
     */
    public void setConnectionname(String connectionname){
        this.connectionname = connectionname ;
        this.modify("connectionname",connectionname);
    }

    /**
     * 设置 [状态描述]
     */
    public void setStatuscode(Integer statuscode){
        this.statuscode = statuscode ;
        this.modify("statuscode",statuscode);
    }

    /**
     * 设置 [状态]
     */
    public void setStatecode(Integer statecode){
        this.statecode = statecode ;
        this.modify("statecode",statecode);
    }

    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [实体图像]
     */
    public void setEntityimage(String entityimage){
        this.entityimage = entityimage ;
        this.modify("entityimage",entityimage);
    }

    /**
     * 设置 [角色]
     */
    public void setRecord1rolename(String record1rolename){
        this.record1rolename = record1rolename ;
        this.modify("record1rolename",record1rolename);
    }

    /**
     * 设置 [角色（目标）]
     */
    public void setRecord2rolename(String record2rolename){
        this.record2rolename = record2rolename ;
        this.modify("record2rolename",record2rolename);
    }

    /**
     * 设置 [角色(源)]
     */
    public void setRecord1roleid(String record1roleid){
        this.record1roleid = record1roleid ;
        this.modify("record1roleid",record1roleid);
    }

    /**
     * 设置 [货币]
     */
    public void setTransactioncurrencyid(String transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }

    /**
     * 设置 [角色(目标)]
     */
    public void setRecord2roleid(String record2roleid){
        this.record2roleid = record2roleid ;
        this.modify("record2roleid",record2roleid);
    }


}


