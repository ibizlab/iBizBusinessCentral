package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.Fax;
import cn.ibizlab.businesscentral.core.base.filter.FaxSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Fax] 服务对象接口
 */
public interface IFaxService extends IService<Fax>{

    boolean create(Fax et) ;
    void createBatch(List<Fax> list) ;
    boolean update(Fax et) ;
    void updateBatch(List<Fax> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    Fax get(String key) ;
    Fax getDraft(Fax et) ;
    boolean checkKey(Fax et) ;
    boolean save(Fax et) ;
    void saveBatch(List<Fax> list) ;
    Page<Fax> searchDefault(FaxSearchContext context) ;
    List<Fax> selectByServiceid(String serviceid) ;
    void removeByServiceid(String serviceid) ;
    List<Fax> selectBySlaid(String slaid) ;
    void removeBySlaid(String slaid) ;
    List<Fax> selectByTransactioncurrencyid(String transactioncurrencyid) ;
    void removeByTransactioncurrencyid(String transactioncurrencyid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Fax> getFaxByIds(List<String> ids) ;
    List<Fax> getFaxByEntities(List<Fax> entities) ;
}


