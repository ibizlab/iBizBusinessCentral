package cn.ibizlab.businesscentral.core.scheduling.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.scheduling.domain.BookableResource;
import cn.ibizlab.businesscentral.core.scheduling.filter.BookableResourceSearchContext;
import cn.ibizlab.businesscentral.core.scheduling.service.IBookableResourceService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.scheduling.mapper.BookableResourceMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[可预订的资源] 服务对象接口实现
 */
@Slf4j
@Service("BookableResourceServiceImpl")
public class BookableResourceServiceImpl extends ServiceImpl<BookableResourceMapper, BookableResource> implements IBookableResourceService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.scheduling.service.IBookableResourceBookingService bookableresourcebookingService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.scheduling.service.IBookableResourceCategoryAssnService bookableresourcecategoryassnService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.scheduling.service.IBookableResourceCharacteristicService bookableresourcecharacteristicService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.scheduling.service.IBookableResourceGroupService bookableresourcegroupService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IAccountService accountService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ICalendarService calendarService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IContactService contactService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.ITransactionCurrencyService transactioncurrencyService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(BookableResource et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getBookableresourceid()),et);
        return true;
    }

    @Override
    public void createBatch(List<BookableResource> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(BookableResource et) {
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("bookableresid",et.getBookableresourceid())))
            return false;
        CachedBeanCopier.copy(get(et.getBookableresourceid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<BookableResource> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public BookableResource get(String key) {
        BookableResource et = getById(key);
        if(et==null){
            et=new BookableResource();
            et.setBookableresourceid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public BookableResource getDraft(BookableResource et) {
        return et;
    }

    @Override
    public boolean checkKey(BookableResource et) {
        return (!ObjectUtils.isEmpty(et.getBookableresourceid()))&&(!Objects.isNull(this.getById(et.getBookableresourceid())));
    }
    @Override
    @Transactional
    public boolean save(BookableResource et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(BookableResource et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<BookableResource> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<BookableResource> list) {
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<BookableResource> selectByAccountid(String accountid) {
        return baseMapper.selectByAccountid(accountid);
    }

    @Override
    public void removeByAccountid(String accountid) {
        this.remove(new QueryWrapper<BookableResource>().eq("accountid",accountid));
    }

	@Override
    public List<BookableResource> selectByCalendarid(String calendarid) {
        return baseMapper.selectByCalendarid(calendarid);
    }

    @Override
    public void removeByCalendarid(String calendarid) {
        this.remove(new QueryWrapper<BookableResource>().eq("calendarid",calendarid));
    }

	@Override
    public List<BookableResource> selectByContactid(String contactid) {
        return baseMapper.selectByContactid(contactid);
    }

    @Override
    public void removeByContactid(String contactid) {
        this.remove(new QueryWrapper<BookableResource>().eq("contactid",contactid));
    }

	@Override
    public List<BookableResource> selectByTransactioncurrencyid(String transactioncurrencyid) {
        return baseMapper.selectByTransactioncurrencyid(transactioncurrencyid);
    }

    @Override
    public void removeByTransactioncurrencyid(String transactioncurrencyid) {
        this.remove(new QueryWrapper<BookableResource>().eq("transactioncurrencyid",transactioncurrencyid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<BookableResource> searchDefault(BookableResourceSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<BookableResource> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<BookableResource>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<BookableResource> getBookableresourceByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<BookableResource> getBookableresourceByEntities(List<BookableResource> entities) {
        List ids =new ArrayList();
        for(BookableResource entity : entities){
            Serializable id=entity.getBookableresourceid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}



