package cn.ibizlab.businesscentral.core.service.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.service.domain.Equipment;
/**
 * 关系型数据实体[Equipment] 查询条件对象
 */
@Slf4j
@Data
public class EquipmentSearchContext extends QueryWrapperContext<Equipment> {

	private String n_equipmentname_like;//[设备名称]
	public void setN_equipmentname_like(String n_equipmentname_like) {
        this.n_equipmentname_like = n_equipmentname_like;
        if(!ObjectUtils.isEmpty(this.n_equipmentname_like)){
            this.getSearchCond().like("equipmentname", n_equipmentname_like);
        }
    }
	private String n_transactioncurrencyid_eq;//[货币]
	public void setN_transactioncurrencyid_eq(String n_transactioncurrencyid_eq) {
        this.n_transactioncurrencyid_eq = n_transactioncurrencyid_eq;
        if(!ObjectUtils.isEmpty(this.n_transactioncurrencyid_eq)){
            this.getSearchCond().eq("transactioncurrencyid", n_transactioncurrencyid_eq);
        }
    }
	private String n_businessunitid_eq;//[Business Unit Id]
	public void setN_businessunitid_eq(String n_businessunitid_eq) {
        this.n_businessunitid_eq = n_businessunitid_eq;
        if(!ObjectUtils.isEmpty(this.n_businessunitid_eq)){
            this.getSearchCond().eq("businessunitid", n_businessunitid_eq);
        }
    }
	private String n_calendarid_eq;//[日历]
	public void setN_calendarid_eq(String n_calendarid_eq) {
        this.n_calendarid_eq = n_calendarid_eq;
        if(!ObjectUtils.isEmpty(this.n_calendarid_eq)){
            this.getSearchCond().eq("calendarid", n_calendarid_eq);
        }
    }
	private String n_siteid_eq;//[场所]
	public void setN_siteid_eq(String n_siteid_eq) {
        this.n_siteid_eq = n_siteid_eq;
        if(!ObjectUtils.isEmpty(this.n_siteid_eq)){
            this.getSearchCond().eq("siteid", n_siteid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("equipmentname", query)   
            );
		 }
	}
}



