package cn.ibizlab.businesscentral.core.product.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.product.domain.ProductPriceLevel;
/**
 * 关系型数据实体[ProductPriceLevel] 查询条件对象
 */
@Slf4j
@Data
public class ProductPriceLevelSearchContext extends QueryWrapperContext<ProductPriceLevel> {

	private String n_pricingmethodcode_eq;//[定价方式]
	public void setN_pricingmethodcode_eq(String n_pricingmethodcode_eq) {
        this.n_pricingmethodcode_eq = n_pricingmethodcode_eq;
        if(!ObjectUtils.isEmpty(this.n_pricingmethodcode_eq)){
            this.getSearchCond().eq("pricingmethodcode", n_pricingmethodcode_eq);
        }
    }
	private String n_roundingoptioncode_eq;//[舍入选项]
	public void setN_roundingoptioncode_eq(String n_roundingoptioncode_eq) {
        this.n_roundingoptioncode_eq = n_roundingoptioncode_eq;
        if(!ObjectUtils.isEmpty(this.n_roundingoptioncode_eq)){
            this.getSearchCond().eq("roundingoptioncode", n_roundingoptioncode_eq);
        }
    }
	private String n_quantitysellingcode_eq;//[销售数量控制]
	public void setN_quantitysellingcode_eq(String n_quantitysellingcode_eq) {
        this.n_quantitysellingcode_eq = n_quantitysellingcode_eq;
        if(!ObjectUtils.isEmpty(this.n_quantitysellingcode_eq)){
            this.getSearchCond().eq("quantitysellingcode", n_quantitysellingcode_eq);
        }
    }
	private String n_roundingpolicycode_eq;//[舍入原则]
	public void setN_roundingpolicycode_eq(String n_roundingpolicycode_eq) {
        this.n_roundingpolicycode_eq = n_roundingpolicycode_eq;
        if(!ObjectUtils.isEmpty(this.n_roundingpolicycode_eq)){
            this.getSearchCond().eq("roundingpolicycode", n_roundingpolicycode_eq);
        }
    }
	private String n_uomschedulename_eq;//[计量单位组]
	public void setN_uomschedulename_eq(String n_uomschedulename_eq) {
        this.n_uomschedulename_eq = n_uomschedulename_eq;
        if(!ObjectUtils.isEmpty(this.n_uomschedulename_eq)){
            this.getSearchCond().eq("uomschedulename", n_uomschedulename_eq);
        }
    }
	private String n_uomschedulename_like;//[计量单位组]
	public void setN_uomschedulename_like(String n_uomschedulename_like) {
        this.n_uomschedulename_like = n_uomschedulename_like;
        if(!ObjectUtils.isEmpty(this.n_uomschedulename_like)){
            this.getSearchCond().like("uomschedulename", n_uomschedulename_like);
        }
    }
	private String n_productname_eq;//[产品名称]
	public void setN_productname_eq(String n_productname_eq) {
        this.n_productname_eq = n_productname_eq;
        if(!ObjectUtils.isEmpty(this.n_productname_eq)){
            this.getSearchCond().eq("productname", n_productname_eq);
        }
    }
	private String n_productname_like;//[产品名称]
	public void setN_productname_like(String n_productname_like) {
        this.n_productname_like = n_productname_like;
        if(!ObjectUtils.isEmpty(this.n_productname_like)){
            this.getSearchCond().like("productname", n_productname_like);
        }
    }
	private String n_currencyname_eq;//[货币]
	public void setN_currencyname_eq(String n_currencyname_eq) {
        this.n_currencyname_eq = n_currencyname_eq;
        if(!ObjectUtils.isEmpty(this.n_currencyname_eq)){
            this.getSearchCond().eq("currencyname", n_currencyname_eq);
        }
    }
	private String n_currencyname_like;//[货币]
	public void setN_currencyname_like(String n_currencyname_like) {
        this.n_currencyname_like = n_currencyname_like;
        if(!ObjectUtils.isEmpty(this.n_currencyname_like)){
            this.getSearchCond().like("currencyname", n_currencyname_like);
        }
    }
	private String n_uomname_eq;//[计量单位]
	public void setN_uomname_eq(String n_uomname_eq) {
        this.n_uomname_eq = n_uomname_eq;
        if(!ObjectUtils.isEmpty(this.n_uomname_eq)){
            this.getSearchCond().eq("uomname", n_uomname_eq);
        }
    }
	private String n_uomname_like;//[计量单位]
	public void setN_uomname_like(String n_uomname_like) {
        this.n_uomname_like = n_uomname_like;
        if(!ObjectUtils.isEmpty(this.n_uomname_like)){
            this.getSearchCond().like("uomname", n_uomname_like);
        }
    }
	private String n_discounttypename_eq;//[折扣表]
	public void setN_discounttypename_eq(String n_discounttypename_eq) {
        this.n_discounttypename_eq = n_discounttypename_eq;
        if(!ObjectUtils.isEmpty(this.n_discounttypename_eq)){
            this.getSearchCond().eq("discounttypename", n_discounttypename_eq);
        }
    }
	private String n_discounttypename_like;//[折扣表]
	public void setN_discounttypename_like(String n_discounttypename_like) {
        this.n_discounttypename_like = n_discounttypename_like;
        if(!ObjectUtils.isEmpty(this.n_discounttypename_like)){
            this.getSearchCond().like("discounttypename", n_discounttypename_like);
        }
    }
	private String n_pricelevelname_eq;//[价目表]
	public void setN_pricelevelname_eq(String n_pricelevelname_eq) {
        this.n_pricelevelname_eq = n_pricelevelname_eq;
        if(!ObjectUtils.isEmpty(this.n_pricelevelname_eq)){
            this.getSearchCond().eq("pricelevelname", n_pricelevelname_eq);
        }
    }
	private String n_pricelevelname_like;//[价目表]
	public void setN_pricelevelname_like(String n_pricelevelname_like) {
        this.n_pricelevelname_like = n_pricelevelname_like;
        if(!ObjectUtils.isEmpty(this.n_pricelevelname_like)){
            this.getSearchCond().like("pricelevelname", n_pricelevelname_like);
        }
    }
	private String n_productid_eq;//[产品]
	public void setN_productid_eq(String n_productid_eq) {
        this.n_productid_eq = n_productid_eq;
        if(!ObjectUtils.isEmpty(this.n_productid_eq)){
            this.getSearchCond().eq("productid", n_productid_eq);
        }
    }
	private String n_uomscheduleid_eq;//[单位计划 ID]
	public void setN_uomscheduleid_eq(String n_uomscheduleid_eq) {
        this.n_uomscheduleid_eq = n_uomscheduleid_eq;
        if(!ObjectUtils.isEmpty(this.n_uomscheduleid_eq)){
            this.getSearchCond().eq("uomscheduleid", n_uomscheduleid_eq);
        }
    }
	private String n_transactioncurrencyid_eq;//[货币]
	public void setN_transactioncurrencyid_eq(String n_transactioncurrencyid_eq) {
        this.n_transactioncurrencyid_eq = n_transactioncurrencyid_eq;
        if(!ObjectUtils.isEmpty(this.n_transactioncurrencyid_eq)){
            this.getSearchCond().eq("transactioncurrencyid", n_transactioncurrencyid_eq);
        }
    }
	private String n_uomid_eq;//[计价单位]
	public void setN_uomid_eq(String n_uomid_eq) {
        this.n_uomid_eq = n_uomid_eq;
        if(!ObjectUtils.isEmpty(this.n_uomid_eq)){
            this.getSearchCond().eq("uomid", n_uomid_eq);
        }
    }
	private String n_discounttypeid_eq;//[折扣表]
	public void setN_discounttypeid_eq(String n_discounttypeid_eq) {
        this.n_discounttypeid_eq = n_discounttypeid_eq;
        if(!ObjectUtils.isEmpty(this.n_discounttypeid_eq)){
            this.getSearchCond().eq("discounttypeid", n_discounttypeid_eq);
        }
    }
	private String n_pricelevelid_eq;//[价目表]
	public void setN_pricelevelid_eq(String n_pricelevelid_eq) {
        this.n_pricelevelid_eq = n_pricelevelid_eq;
        if(!ObjectUtils.isEmpty(this.n_pricelevelid_eq)){
            this.getSearchCond().eq("pricelevelid", n_pricelevelid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("productname", query)   
            );
		 }
	}
}



