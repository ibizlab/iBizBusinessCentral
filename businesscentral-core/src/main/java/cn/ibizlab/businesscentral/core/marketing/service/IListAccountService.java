package cn.ibizlab.businesscentral.core.marketing.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.marketing.domain.ListAccount;
import cn.ibizlab.businesscentral.core.marketing.filter.ListAccountSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[ListAccount] 服务对象接口
 */
public interface IListAccountService extends IService<ListAccount>{

    boolean create(ListAccount et) ;
    void createBatch(List<ListAccount> list) ;
    boolean update(ListAccount et) ;
    void updateBatch(List<ListAccount> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    ListAccount get(String key) ;
    ListAccount getDraft(ListAccount et) ;
    boolean checkKey(ListAccount et) ;
    boolean save(ListAccount et) ;
    void saveBatch(List<ListAccount> list) ;
    Page<ListAccount> searchDefault(ListAccountSearchContext context) ;
    List<ListAccount> selectByEntity2id(String accountid) ;
    void removeByEntity2id(String accountid) ;
    List<ListAccount> selectByEntityid(String listid) ;
    void removeByEntityid(String listid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<ListAccount> getListaccountByIds(List<String> ids) ;
    List<ListAccount> getListaccountByEntities(List<ListAccount> entities) ;
}


