package cn.ibizlab.businesscentral.core.base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.base.domain.OperationUnit;
import cn.ibizlab.businesscentral.core.base.filter.OperationUnitSearchContext;
import cn.ibizlab.businesscentral.core.base.service.IOperationUnitService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.base.mapper.OperationUnitMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[运营单位] 服务对象接口实现
 */
@Slf4j
@Service("OperationUnitServiceImpl")
public class OperationUnitServiceImpl extends ServiceImpl<OperationUnitMapper, OperationUnit> implements IOperationUnitService {


    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(OperationUnit et) {
        createIndexMajorEntityData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getOperationunitid()),et);
        return true;
    }

    @Override
    public void createBatch(List<OperationUnit> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(OperationUnit et) {
        organizationService.update(operationunitInheritMapping.toOrganization(et));
        if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("operationunitid",et.getOperationunitid())))
            return false;
        CachedBeanCopier.copy(get(et.getOperationunitid()),et);
        return true;
    }

    @Override
    public void updateBatch(List<OperationUnit> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        organizationService.remove(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public OperationUnit get(String key) {
        OperationUnit et = getById(key);
        if(et==null){
            et=new OperationUnit();
            et.setOperationunitid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public OperationUnit getDraft(OperationUnit et) {
        return et;
    }

    @Override
    public boolean checkKey(OperationUnit et) {
        return (!ObjectUtils.isEmpty(et.getOperationunitid()))&&(!Objects.isNull(this.getById(et.getOperationunitid())));
    }
    @Override
    @Transactional
    public boolean save(OperationUnit et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(OperationUnit et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    public boolean saveBatch(Collection<OperationUnit> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    public void saveBatch(List<OperationUnit> list) {
        saveOrUpdateBatch(list,batchSize);
    }



    /**
     * 查询集合 业务单位
     */
    @Override
    public Page<OperationUnit> searchBusinessUnit(OperationUnitSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<OperationUnit> pages=baseMapper.searchBusinessUnit(context.getPages(),context,context.getSelectCond());
        return new PageImpl<OperationUnit>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<OperationUnit> searchDefault(OperationUnitSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<OperationUnit> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<OperationUnit>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 部门
     */
    @Override
    public Page<OperationUnit> searchDepartment(OperationUnitSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<OperationUnit> pages=baseMapper.searchDepartment(context.getPages(),context,context.getSelectCond());
        return new PageImpl<OperationUnit>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }






    @Autowired
    cn.ibizlab.businesscentral.core.base.mapping.OperationUnitInheritMapping operationunitInheritMapping;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.base.service.IOrganizationService organizationService;

    /**
     * 创建索引主实体数据
     * @param et
     */
    private void createIndexMajorEntityData(OperationUnit et){
        if(ObjectUtils.isEmpty(et.getOperationunitid()))
            et.setOperationunitid((String)et.getDefaultKey(true));
        cn.ibizlab.businesscentral.core.base.domain.Organization organization =operationunitInheritMapping.toOrganization(et);
        organization.set("organizationtype","OPERATIONUNIT");
        organizationService.create(organization);
    }

    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<OperationUnit> getOperationunitByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<OperationUnit> getOperationunitByEntities(List<OperationUnit> entities) {
        List ids =new ArrayList();
        for(OperationUnit entity : entities){
            Serializable id=entity.getOperationunitid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }

}



