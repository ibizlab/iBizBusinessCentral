package cn.ibizlab.businesscentral.core.sales.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;

/**
 * 实体[报价单]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "QUOTE",resultMap = "QuoteResultMap")
public class Quote extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 帐单寄往国家/地区
     */
    @DEField(name = "billto_country")
    @TableField(value = "billto_country")
    @JSONField(name = "billto_country")
    @JsonProperty("billto_country")
    private String billtoCountry;
    /**
     * 暂候时间(分钟)
     */
    @TableField(value = "onholdtime")
    @JSONField(name = "onholdtime")
    @JsonProperty("onholdtime")
    private Integer onholdtime;
    /**
     * 送货地址
     */
    @DEField(defaultValue = "0")
    @TableField(value = "willcall")
    @JSONField(name = "willcall")
    @JsonProperty("willcall")
    private Integer willcall;
    /**
     * 送至街道 1
     */
    @DEField(name = "shipto_line1")
    @TableField(value = "shipto_line1")
    @JSONField(name = "shipto_line1")
    @JsonProperty("shipto_line1")
    private String shiptoLine1;
    /**
     * 货运条款
     */
    @TableField(value = "freighttermscode")
    @JSONField(name = "freighttermscode")
    @JsonProperty("freighttermscode")
    private String freighttermscode;
    /**
     * 状态
     */
    @TableField(value = "statecode")
    @JSONField(name = "statecode")
    @JsonProperty("statecode")
    private Integer statecode;
    /**
     * 开始生效日期
     */
    @TableField(value = "effectivefrom")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "effectivefrom" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("effectivefrom")
    private Timestamp effectivefrom;
    /**
     * 折后金额总计 (Base)
     */
    @DEField(name = "totalamountlessfreight_base")
    @TableField(value = "totalamountlessfreight_base")
    @JSONField(name = "totalamountlessfreight_base")
    @JsonProperty("totalamountlessfreight_base")
    private BigDecimal totalamountlessfreightBase;
    /**
     * 报价名称
     */
    @TableField(value = "quotename")
    @JSONField(name = "quotename")
    @JsonProperty("quotename")
    private String quotename;
    /**
     * 折扣金额总和 (Base)
     */
    @DEField(name = "totaldiscountamount_base")
    @TableField(value = "totaldiscountamount_base")
    @JSONField(name = "totaldiscountamount_base")
    @JsonProperty("totaldiscountamount_base")
    private BigDecimal totaldiscountamountBase;
    /**
     * 送货地的邮政编码
     */
    @DEField(name = "shipto_postalcode")
    @TableField(value = "shipto_postalcode")
    @JSONField(name = "shipto_postalcode")
    @JsonProperty("shipto_postalcode")
    private String shiptoPostalcode;
    /**
     * 总金额
     */
    @TableField(value = "totalamount")
    @JSONField(name = "totalamount")
    @JsonProperty("totalamount")
    private BigDecimal totalamount;
    /**
     * 帐单邮寄地址 ID
     */
    @DEField(name = "billto_addressid")
    @TableField(value = "billto_addressid")
    @JSONField(name = "billto_addressid")
    @JsonProperty("billto_addressid")
    private String billtoAddressid;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman" , fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 总金额 (Base)
     */
    @DEField(name = "totalamount_base")
    @TableField(value = "totalamount_base")
    @JSONField(name = "totalamount_base")
    @JsonProperty("totalamount_base")
    private BigDecimal totalamountBase;
    /**
     * 送至街道 2
     */
    @DEField(name = "shipto_line2")
    @TableField(value = "shipto_line2")
    @JSONField(name = "shipto_line2")
    @JsonProperty("shipto_line2")
    private String shiptoLine2;
    /**
     * 明细项目折扣金额总和
     */
    @TableField(value = "totallineitemdiscountamount")
    @JSONField(name = "totallineitemdiscountamount")
    @JsonProperty("totallineitemdiscountamount")
    private BigDecimal totallineitemdiscountamount;
    /**
     * Time Zone Rule Version Number
     */
    @TableField(value = "timezoneruleversionnumber")
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;
    /**
     * 明细金额总计 (Base)
     */
    @DEField(name = "totallineitemamount_base")
    @TableField(value = "totallineitemamount_base")
    @JSONField(name = "totallineitemamount_base")
    @JsonProperty("totallineitemamount_base")
    private BigDecimal totallineitemamountBase;
    /**
     * 送货地的联系人姓名
     */
    @DEField(name = "shipto_contactname")
    @TableField(value = "shipto_contactname")
    @JSONField(name = "shipto_contactname")
    @JsonProperty("shipto_contactname")
    private String shiptoContactname;
    /**
     * Record Created On
     */
    @TableField(value = "overriddencreatedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;
    /**
     * 送货地址 ID
     */
    @DEField(name = "shipto_addressid")
    @TableField(value = "shipto_addressid")
    @JSONField(name = "shipto_addressid")
    @JsonProperty("shipto_addressid")
    private String shiptoAddressid;
    /**
     * 送货地的名称
     */
    @DEField(name = "shipto_name")
    @TableField(value = "shipto_name")
    @JSONField(name = "shipto_name")
    @JsonProperty("shipto_name")
    private String shiptoName;
    /**
     * 状态描述
     */
    @TableField(value = "statuscode")
    @JSONField(name = "statuscode")
    @JsonProperty("statuscode")
    private Integer statuscode;
    /**
     * 报价单 ID
     */
    @TableField(value = "quotenumber")
    @JSONField(name = "quotenumber")
    @JsonProperty("quotenumber")
    private String quotenumber;
    /**
     * 帐单寄往地的电话号码
     */
    @DEField(name = "billto_telephone")
    @TableField(value = "billto_telephone")
    @JSONField(name = "billto_telephone")
    @JsonProperty("billto_telephone")
    private String billtoTelephone;
    /**
     * 结束日期
     */
    @TableField(value = "closedon")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "closedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("closedon")
    private Timestamp closedon;
    /**
     * 运费金额 (Base)
     */
    @DEField(name = "freightamount_base")
    @TableField(value = "freightamount_base")
    @JSONField(name = "freightamount_base")
    @JsonProperty("freightamount_base")
    private BigDecimal freightamountBase;
    /**
     * 送至街道 3
     */
    @DEField(name = "shipto_line3")
    @TableField(value = "shipto_line3")
    @JSONField(name = "shipto_line3")
    @JsonProperty("shipto_line3")
    private String shiptoLine3;
    /**
     * 送货方式
     */
    @TableField(value = "shippingmethodcode")
    @JSONField(name = "shippingmethodcode")
    @JsonProperty("shippingmethodcode")
    private String shippingmethodcode;
    /**
     * 潜在客户
     */
    @TableField(value = "customerid")
    @JSONField(name = "customerid")
    @JsonProperty("customerid")
    private String customerid;
    /**
     * 有效截止时间
     */
    @TableField(value = "effectiveto")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "effectiveto" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("effectiveto")
    private Timestamp effectiveto;
    /**
     * 货运条款
     */
    @DEField(name = "shipto_freighttermscode")
    @TableField(value = "shipto_freighttermscode")
    @JSONField(name = "shipto_freighttermscode")
    @JsonProperty("shipto_freighttermscode")
    private String shiptoFreighttermscode;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 明细金额总计
     */
    @TableField(value = "totallineitemamount")
    @JSONField(name = "totallineitemamount")
    @JsonProperty("totallineitemamount")
    private BigDecimal totallineitemamount;
    /**
     * 送货地的传真号码
     */
    @DEField(name = "shipto_fax")
    @TableField(value = "shipto_fax")
    @JSONField(name = "shipto_fax")
    @JsonProperty("shipto_fax")
    private String shiptoFax;
    /**
     * 负责人
     */
    @TableField(value = "ownerid")
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;
    /**
     * 客户
     */
    @TableField(value = "accountname")
    @JSONField(name = "accountname")
    @JsonProperty("accountname")
    private String accountname;
    /**
     * Email Address
     */
    @TableField(value = "emailaddress")
    @JSONField(name = "emailaddress")
    @JsonProperty("emailaddress")
    private String emailaddress;
    /**
     * Import Sequence Number
     */
    @TableField(value = "importsequencenumber")
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;
    /**
     * UTC Conversion Time Zone Code
     */
    @TableField(value = "utcconversiontimezonecode")
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;
    /**
     * 客户
     */
    @TableField(value = "customername")
    @JSONField(name = "customername")
    @JsonProperty("customername")
    private String customername;
    /**
     * 定价错误
     */
    @TableField(value = "pricingerrorcode")
    @JSONField(name = "pricingerrorcode")
    @JsonProperty("pricingerrorcode")
    private String pricingerrorcode;
    /**
     * 唯一说明 ID
     */
    @TableField(value = "uniquedscid")
    @JSONField(name = "uniquedscid")
    @JsonProperty("uniquedscid")
    private String uniquedscid;
    /**
     * 帐单寄往街道 3
     */
    @DEField(name = "billto_line3")
    @TableField(value = "billto_line3")
    @JSONField(name = "billto_line3")
    @JsonProperty("billto_line3")
    private String billtoLine3;
    /**
     * 说明
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 帐单寄往地址
     */
    @DEField(name = "billto_composite")
    @TableField(value = "billto_composite")
    @JSONField(name = "billto_composite")
    @JsonProperty("billto_composite")
    private String billtoComposite;
    /**
     * 要求交付日期
     */
    @TableField(value = "requestdeliveryby")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "requestdeliveryby" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("requestdeliveryby")
    private Timestamp requestdeliveryby;
    /**
     * 总税款
     */
    @TableField(value = "totaltax")
    @JSONField(name = "totaltax")
    @JsonProperty("totaltax")
    private BigDecimal totaltax;
    /**
     * 报价单
     */
    @DEField(isKeyField=true)
    @TableId(value= "quoteid",type=IdType.ASSIGN_UUID)
    @JSONField(name = "quoteid")
    @JsonProperty("quoteid")
    private String quoteid;
    /**
     * Process Id
     */
    @TableField(value = "processid")
    @JSONField(name = "processid")
    @JsonProperty("processid")
    private String processid;
    /**
     * Stage Id
     */
    @TableField(value = "stageid")
    @JSONField(name = "stageid")
    @JsonProperty("stageid")
    private String stageid;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 联系人
     */
    @TableField(value = "contactname")
    @JSONField(name = "contactname")
    @JsonProperty("contactname")
    private String contactname;
    /**
     * 汇率
     */
    @TableField(value = "exchangerate")
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;
    /**
     * Version Number
     */
    @TableField(value = "versionnumber")
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;
    /**
     * 送至国家/地区
     */
    @DEField(name = "shipto_country")
    @TableField(value = "shipto_country")
    @JSONField(name = "shipto_country")
    @JsonProperty("shipto_country")
    private String shiptoCountry;
    /**
     * 到期时间
     */
    @TableField(value = "expireson")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "expireson" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("expireson")
    private Timestamp expireson;
    /**
     * 负责人类型
     */
    @TableField(value = "ownertype")
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;
    /**
     * 帐单寄往地邮政编码
     */
    @DEField(name = "billto_postalcode")
    @TableField(value = "billto_postalcode")
    @JSONField(name = "billto_postalcode")
    @JsonProperty("billto_postalcode")
    private String billtoPostalcode;
    /**
     * 帐单寄往街道 2
     */
    @DEField(name = "billto_line2")
    @TableField(value = "billto_line2")
    @JSONField(name = "billto_line2")
    @JsonProperty("billto_line2")
    private String billtoLine2;
    /**
     * 帐单寄往地的名称
     */
    @DEField(name = "billto_name")
    @TableField(value = "billto_name")
    @JSONField(name = "billto_name")
    @JsonProperty("billto_name")
    private String billtoName;
    /**
     * 帐单寄往街道 1
     */
    @DEField(name = "billto_line1")
    @TableField(value = "billto_line1")
    @JSONField(name = "billto_line1")
    @JsonProperty("billto_line1")
    private String billtoLine1;
    /**
     * 修订 ID
     */
    @TableField(value = "revisionnumber")
    @JSONField(name = "revisionnumber")
    @JsonProperty("revisionnumber")
    private Integer revisionnumber;
    /**
     * 帐单寄往地联系人姓名
     */
    @DEField(name = "billto_contactname")
    @TableField(value = "billto_contactname")
    @JSONField(name = "billto_contactname")
    @JsonProperty("billto_contactname")
    private String billtoContactname;
    /**
     * 送至省/市/自治区
     */
    @DEField(name = "shipto_stateorprovince")
    @TableField(value = "shipto_stateorprovince")
    @JSONField(name = "shipto_stateorprovince")
    @JsonProperty("shipto_stateorprovince")
    private String shiptoStateorprovince;
    /**
     * 帐单寄往市/县
     */
    @DEField(name = "billto_city")
    @TableField(value = "billto_city")
    @JSONField(name = "billto_city")
    @JsonProperty("billto_city")
    private String billtoCity;
    /**
     * 上一暂候时间
     */
    @TableField(value = "lastonholdtime")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "lastonholdtime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("lastonholdtime")
    private Timestamp lastonholdtime;
    /**
     * 报价单折扣(%)
     */
    @TableField(value = "discountpercentage")
    @JSONField(name = "discountpercentage")
    @JsonProperty("discountpercentage")
    private BigDecimal discountpercentage;
    /**
     * 送至市/县
     */
    @DEField(name = "shipto_city")
    @TableField(value = "shipto_city")
    @JSONField(name = "shipto_city")
    @JsonProperty("shipto_city")
    private String shiptoCity;
    /**
     * 潜在客户类型
     */
    @TableField(value = "customertype")
    @JSONField(name = "customertype")
    @JsonProperty("customertype")
    private String customertype;
    /**
     * 报价单折扣金额
     */
    @TableField(value = "discountamount")
    @JSONField(name = "discountamount")
    @JsonProperty("discountamount")
    private BigDecimal discountamount;
    /**
     * 送货地址
     */
    @DEField(name = "shipto_composite")
    @TableField(value = "shipto_composite")
    @JSONField(name = "shipto_composite")
    @JsonProperty("shipto_composite")
    private String shiptoComposite;
    /**
     * 送货地的电话号码
     */
    @DEField(name = "shipto_telephone")
    @TableField(value = "shipto_telephone")
    @JSONField(name = "shipto_telephone")
    @JsonProperty("shipto_telephone")
    private String shiptoTelephone;
    /**
     * 帐单寄往省/市/自治区
     */
    @DEField(name = "billto_stateorprovince")
    @TableField(value = "billto_stateorprovince")
    @JSONField(name = "billto_stateorprovince")
    @JsonProperty("billto_stateorprovince")
    private String billtoStateorprovince;
    /**
     * 付款条件
     */
    @TableField(value = "paymenttermscode")
    @JSONField(name = "paymenttermscode")
    @JsonProperty("paymenttermscode")
    private String paymenttermscode;
    /**
     * 负责人
     */
    @TableField(value = "ownername")
    @JSONField(name = "ownername")
    @JsonProperty("ownername")
    private String ownername;
    /**
     * 折后金额总计
     */
    @TableField(value = "totalamountlessfreight")
    @JSONField(name = "totalamountlessfreight")
    @JsonProperty("totalamountlessfreight")
    private BigDecimal totalamountlessfreight;
    /**
     * Traversed Path
     */
    @TableField(value = "traversedpath")
    @JSONField(name = "traversedpath")
    @JsonProperty("traversedpath")
    private String traversedpath;
    /**
     * 帐单寄往地的传真号码
     */
    @DEField(name = "billto_fax")
    @TableField(value = "billto_fax")
    @JSONField(name = "billto_fax")
    @JsonProperty("billto_fax")
    private String billtoFax;
    /**
     * 折扣金额总和
     */
    @TableField(value = "totaldiscountamount")
    @JSONField(name = "totaldiscountamount")
    @JsonProperty("totaldiscountamount")
    private BigDecimal totaldiscountamount;
    /**
     * 总税款 (Base)
     */
    @DEField(name = "totaltax_base")
    @TableField(value = "totaltax_base")
    @JSONField(name = "totaltax_base")
    @JsonProperty("totaltax_base")
    private BigDecimal totaltaxBase;
    /**
     * 报价单折扣金额 (Base)
     */
    @DEField(name = "discountamount_base")
    @TableField(value = "discountamount_base")
    @JSONField(name = "discountamount_base")
    @JsonProperty("discountamount_base")
    private BigDecimal discountamountBase;
    /**
     * 运费金额
     */
    @TableField(value = "freightamount")
    @JSONField(name = "freightamount")
    @JsonProperty("freightamount")
    private BigDecimal freightamount;
    /**
     * 货币
     */
    @TableField(value = "currencyname")
    @JSONField(name = "currencyname")
    @JsonProperty("currencyname")
    private String currencyname;
    /**
     * 价目表
     */
    @TableField(value = "pricelevelname")
    @JSONField(name = "pricelevelname")
    @JsonProperty("pricelevelname")
    private String pricelevelname;
    /**
     * 商机
     */
    @TableField(value = "opportunityname")
    @JSONField(name = "opportunityname")
    @JsonProperty("opportunityname")
    private String opportunityname;
    /**
     * SLA
     */
    @TableField(value = "slaname")
    @JSONField(name = "slaname")
    @JsonProperty("slaname")
    private String slaname;
    /**
     * 源市场活动
     */
    @TableField(value = "campaignname")
    @JSONField(name = "campaignname")
    @JsonProperty("campaignname")
    private String campaignname;
    /**
     * 价目表
     */
    @TableField(value = "pricelevelid")
    @JSONField(name = "pricelevelid")
    @JsonProperty("pricelevelid")
    private String pricelevelid;
    /**
     * SLA
     */
    @TableField(value = "slaid")
    @JSONField(name = "slaid")
    @JsonProperty("slaid")
    private String slaid;
    /**
     * 货币
     */
    @TableField(value = "transactioncurrencyid")
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;
    /**
     * 商机
     */
    @TableField(value = "opportunityid")
    @JSONField(name = "opportunityid")
    @JsonProperty("opportunityid")
    private String opportunityid;
    /**
     * 源市场活动
     */
    @TableField(value = "campaignid")
    @JSONField(name = "campaignid")
    @JsonProperty("campaignid")
    private String campaignid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.marketing.domain.Campaign campaign;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.sales.domain.Opportunity opportunity;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.product.domain.PriceLevel pricelevel;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.Sla sla;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.base.domain.TransactionCurrency transactioncurrency;



    /**
     * 设置 [帐单寄往国家/地区]
     */
    public void setBilltoCountry(String billtoCountry){
        this.billtoCountry = billtoCountry ;
        this.modify("billto_country",billtoCountry);
    }

    /**
     * 设置 [暂候时间(分钟)]
     */
    public void setOnholdtime(Integer onholdtime){
        this.onholdtime = onholdtime ;
        this.modify("onholdtime",onholdtime);
    }

    /**
     * 设置 [送货地址]
     */
    public void setWillcall(Integer willcall){
        this.willcall = willcall ;
        this.modify("willcall",willcall);
    }

    /**
     * 设置 [送至街道 1]
     */
    public void setShiptoLine1(String shiptoLine1){
        this.shiptoLine1 = shiptoLine1 ;
        this.modify("shipto_line1",shiptoLine1);
    }

    /**
     * 设置 [货运条款]
     */
    public void setFreighttermscode(String freighttermscode){
        this.freighttermscode = freighttermscode ;
        this.modify("freighttermscode",freighttermscode);
    }

    /**
     * 设置 [状态]
     */
    public void setStatecode(Integer statecode){
        this.statecode = statecode ;
        this.modify("statecode",statecode);
    }

    /**
     * 设置 [开始生效日期]
     */
    public void setEffectivefrom(Timestamp effectivefrom){
        this.effectivefrom = effectivefrom ;
        this.modify("effectivefrom",effectivefrom);
    }

    /**
     * 格式化日期 [开始生效日期]
     */
    public String formatEffectivefrom(){
        if (this.effectivefrom == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(effectivefrom);
    }
    /**
     * 设置 [折后金额总计 (Base)]
     */
    public void setTotalamountlessfreightBase(BigDecimal totalamountlessfreightBase){
        this.totalamountlessfreightBase = totalamountlessfreightBase ;
        this.modify("totalamountlessfreight_base",totalamountlessfreightBase);
    }

    /**
     * 设置 [报价名称]
     */
    public void setQuotename(String quotename){
        this.quotename = quotename ;
        this.modify("quotename",quotename);
    }

    /**
     * 设置 [折扣金额总和 (Base)]
     */
    public void setTotaldiscountamountBase(BigDecimal totaldiscountamountBase){
        this.totaldiscountamountBase = totaldiscountamountBase ;
        this.modify("totaldiscountamount_base",totaldiscountamountBase);
    }

    /**
     * 设置 [送货地的邮政编码]
     */
    public void setShiptoPostalcode(String shiptoPostalcode){
        this.shiptoPostalcode = shiptoPostalcode ;
        this.modify("shipto_postalcode",shiptoPostalcode);
    }

    /**
     * 设置 [总金额]
     */
    public void setTotalamount(BigDecimal totalamount){
        this.totalamount = totalamount ;
        this.modify("totalamount",totalamount);
    }

    /**
     * 设置 [帐单邮寄地址 ID]
     */
    public void setBilltoAddressid(String billtoAddressid){
        this.billtoAddressid = billtoAddressid ;
        this.modify("billto_addressid",billtoAddressid);
    }

    /**
     * 设置 [总金额 (Base)]
     */
    public void setTotalamountBase(BigDecimal totalamountBase){
        this.totalamountBase = totalamountBase ;
        this.modify("totalamount_base",totalamountBase);
    }

    /**
     * 设置 [送至街道 2]
     */
    public void setShiptoLine2(String shiptoLine2){
        this.shiptoLine2 = shiptoLine2 ;
        this.modify("shipto_line2",shiptoLine2);
    }

    /**
     * 设置 [明细项目折扣金额总和]
     */
    public void setTotallineitemdiscountamount(BigDecimal totallineitemdiscountamount){
        this.totallineitemdiscountamount = totallineitemdiscountamount ;
        this.modify("totallineitemdiscountamount",totallineitemdiscountamount);
    }

    /**
     * 设置 [Time Zone Rule Version Number]
     */
    public void setTimezoneruleversionnumber(Integer timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [明细金额总计 (Base)]
     */
    public void setTotallineitemamountBase(BigDecimal totallineitemamountBase){
        this.totallineitemamountBase = totallineitemamountBase ;
        this.modify("totallineitemamount_base",totallineitemamountBase);
    }

    /**
     * 设置 [送货地的联系人姓名]
     */
    public void setShiptoContactname(String shiptoContactname){
        this.shiptoContactname = shiptoContactname ;
        this.modify("shipto_contactname",shiptoContactname);
    }

    /**
     * 设置 [Record Created On]
     */
    public void setOverriddencreatedon(Timestamp overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 格式化日期 [Record Created On]
     */
    public String formatOverriddencreatedon(){
        if (this.overriddencreatedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(overriddencreatedon);
    }
    /**
     * 设置 [送货地址 ID]
     */
    public void setShiptoAddressid(String shiptoAddressid){
        this.shiptoAddressid = shiptoAddressid ;
        this.modify("shipto_addressid",shiptoAddressid);
    }

    /**
     * 设置 [送货地的名称]
     */
    public void setShiptoName(String shiptoName){
        this.shiptoName = shiptoName ;
        this.modify("shipto_name",shiptoName);
    }

    /**
     * 设置 [状态描述]
     */
    public void setStatuscode(Integer statuscode){
        this.statuscode = statuscode ;
        this.modify("statuscode",statuscode);
    }

    /**
     * 设置 [报价单 ID]
     */
    public void setQuotenumber(String quotenumber){
        this.quotenumber = quotenumber ;
        this.modify("quotenumber",quotenumber);
    }

    /**
     * 设置 [帐单寄往地的电话号码]
     */
    public void setBilltoTelephone(String billtoTelephone){
        this.billtoTelephone = billtoTelephone ;
        this.modify("billto_telephone",billtoTelephone);
    }

    /**
     * 设置 [结束日期]
     */
    public void setClosedon(Timestamp closedon){
        this.closedon = closedon ;
        this.modify("closedon",closedon);
    }

    /**
     * 格式化日期 [结束日期]
     */
    public String formatClosedon(){
        if (this.closedon == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(closedon);
    }
    /**
     * 设置 [运费金额 (Base)]
     */
    public void setFreightamountBase(BigDecimal freightamountBase){
        this.freightamountBase = freightamountBase ;
        this.modify("freightamount_base",freightamountBase);
    }

    /**
     * 设置 [送至街道 3]
     */
    public void setShiptoLine3(String shiptoLine3){
        this.shiptoLine3 = shiptoLine3 ;
        this.modify("shipto_line3",shiptoLine3);
    }

    /**
     * 设置 [送货方式]
     */
    public void setShippingmethodcode(String shippingmethodcode){
        this.shippingmethodcode = shippingmethodcode ;
        this.modify("shippingmethodcode",shippingmethodcode);
    }

    /**
     * 设置 [潜在客户]
     */
    public void setCustomerid(String customerid){
        this.customerid = customerid ;
        this.modify("customerid",customerid);
    }

    /**
     * 设置 [有效截止时间]
     */
    public void setEffectiveto(Timestamp effectiveto){
        this.effectiveto = effectiveto ;
        this.modify("effectiveto",effectiveto);
    }

    /**
     * 格式化日期 [有效截止时间]
     */
    public String formatEffectiveto(){
        if (this.effectiveto == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(effectiveto);
    }
    /**
     * 设置 [货运条款]
     */
    public void setShiptoFreighttermscode(String shiptoFreighttermscode){
        this.shiptoFreighttermscode = shiptoFreighttermscode ;
        this.modify("shipto_freighttermscode",shiptoFreighttermscode);
    }

    /**
     * 设置 [明细金额总计]
     */
    public void setTotallineitemamount(BigDecimal totallineitemamount){
        this.totallineitemamount = totallineitemamount ;
        this.modify("totallineitemamount",totallineitemamount);
    }

    /**
     * 设置 [送货地的传真号码]
     */
    public void setShiptoFax(String shiptoFax){
        this.shiptoFax = shiptoFax ;
        this.modify("shipto_fax",shiptoFax);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnerid(String ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [客户]
     */
    public void setAccountname(String accountname){
        this.accountname = accountname ;
        this.modify("accountname",accountname);
    }

    /**
     * 设置 [Email Address]
     */
    public void setEmailaddress(String emailaddress){
        this.emailaddress = emailaddress ;
        this.modify("emailaddress",emailaddress);
    }

    /**
     * 设置 [Import Sequence Number]
     */
    public void setImportsequencenumber(Integer importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [UTC Conversion Time Zone Code]
     */
    public void setUtcconversiontimezonecode(Integer utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [客户]
     */
    public void setCustomername(String customername){
        this.customername = customername ;
        this.modify("customername",customername);
    }

    /**
     * 设置 [定价错误]
     */
    public void setPricingerrorcode(String pricingerrorcode){
        this.pricingerrorcode = pricingerrorcode ;
        this.modify("pricingerrorcode",pricingerrorcode);
    }

    /**
     * 设置 [唯一说明 ID]
     */
    public void setUniquedscid(String uniquedscid){
        this.uniquedscid = uniquedscid ;
        this.modify("uniquedscid",uniquedscid);
    }

    /**
     * 设置 [帐单寄往街道 3]
     */
    public void setBilltoLine3(String billtoLine3){
        this.billtoLine3 = billtoLine3 ;
        this.modify("billto_line3",billtoLine3);
    }

    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [帐单寄往地址]
     */
    public void setBilltoComposite(String billtoComposite){
        this.billtoComposite = billtoComposite ;
        this.modify("billto_composite",billtoComposite);
    }

    /**
     * 设置 [要求交付日期]
     */
    public void setRequestdeliveryby(Timestamp requestdeliveryby){
        this.requestdeliveryby = requestdeliveryby ;
        this.modify("requestdeliveryby",requestdeliveryby);
    }

    /**
     * 格式化日期 [要求交付日期]
     */
    public String formatRequestdeliveryby(){
        if (this.requestdeliveryby == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(requestdeliveryby);
    }
    /**
     * 设置 [总税款]
     */
    public void setTotaltax(BigDecimal totaltax){
        this.totaltax = totaltax ;
        this.modify("totaltax",totaltax);
    }

    /**
     * 设置 [Process Id]
     */
    public void setProcessid(String processid){
        this.processid = processid ;
        this.modify("processid",processid);
    }

    /**
     * 设置 [Stage Id]
     */
    public void setStageid(String stageid){
        this.stageid = stageid ;
        this.modify("stageid",stageid);
    }

    /**
     * 设置 [联系人]
     */
    public void setContactname(String contactname){
        this.contactname = contactname ;
        this.modify("contactname",contactname);
    }

    /**
     * 设置 [汇率]
     */
    public void setExchangerate(BigDecimal exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [Version Number]
     */
    public void setVersionnumber(BigInteger versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [送至国家/地区]
     */
    public void setShiptoCountry(String shiptoCountry){
        this.shiptoCountry = shiptoCountry ;
        this.modify("shipto_country",shiptoCountry);
    }

    /**
     * 设置 [到期时间]
     */
    public void setExpireson(Timestamp expireson){
        this.expireson = expireson ;
        this.modify("expireson",expireson);
    }

    /**
     * 格式化日期 [到期时间]
     */
    public String formatExpireson(){
        if (this.expireson == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(expireson);
    }
    /**
     * 设置 [负责人类型]
     */
    public void setOwnertype(String ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [帐单寄往地邮政编码]
     */
    public void setBilltoPostalcode(String billtoPostalcode){
        this.billtoPostalcode = billtoPostalcode ;
        this.modify("billto_postalcode",billtoPostalcode);
    }

    /**
     * 设置 [帐单寄往街道 2]
     */
    public void setBilltoLine2(String billtoLine2){
        this.billtoLine2 = billtoLine2 ;
        this.modify("billto_line2",billtoLine2);
    }

    /**
     * 设置 [帐单寄往地的名称]
     */
    public void setBilltoName(String billtoName){
        this.billtoName = billtoName ;
        this.modify("billto_name",billtoName);
    }

    /**
     * 设置 [帐单寄往街道 1]
     */
    public void setBilltoLine1(String billtoLine1){
        this.billtoLine1 = billtoLine1 ;
        this.modify("billto_line1",billtoLine1);
    }

    /**
     * 设置 [修订 ID]
     */
    public void setRevisionnumber(Integer revisionnumber){
        this.revisionnumber = revisionnumber ;
        this.modify("revisionnumber",revisionnumber);
    }

    /**
     * 设置 [帐单寄往地联系人姓名]
     */
    public void setBilltoContactname(String billtoContactname){
        this.billtoContactname = billtoContactname ;
        this.modify("billto_contactname",billtoContactname);
    }

    /**
     * 设置 [送至省/市/自治区]
     */
    public void setShiptoStateorprovince(String shiptoStateorprovince){
        this.shiptoStateorprovince = shiptoStateorprovince ;
        this.modify("shipto_stateorprovince",shiptoStateorprovince);
    }

    /**
     * 设置 [帐单寄往市/县]
     */
    public void setBilltoCity(String billtoCity){
        this.billtoCity = billtoCity ;
        this.modify("billto_city",billtoCity);
    }

    /**
     * 设置 [上一暂候时间]
     */
    public void setLastonholdtime(Timestamp lastonholdtime){
        this.lastonholdtime = lastonholdtime ;
        this.modify("lastonholdtime",lastonholdtime);
    }

    /**
     * 格式化日期 [上一暂候时间]
     */
    public String formatLastonholdtime(){
        if (this.lastonholdtime == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(lastonholdtime);
    }
    /**
     * 设置 [报价单折扣(%)]
     */
    public void setDiscountpercentage(BigDecimal discountpercentage){
        this.discountpercentage = discountpercentage ;
        this.modify("discountpercentage",discountpercentage);
    }

    /**
     * 设置 [送至市/县]
     */
    public void setShiptoCity(String shiptoCity){
        this.shiptoCity = shiptoCity ;
        this.modify("shipto_city",shiptoCity);
    }

    /**
     * 设置 [潜在客户类型]
     */
    public void setCustomertype(String customertype){
        this.customertype = customertype ;
        this.modify("customertype",customertype);
    }

    /**
     * 设置 [报价单折扣金额]
     */
    public void setDiscountamount(BigDecimal discountamount){
        this.discountamount = discountamount ;
        this.modify("discountamount",discountamount);
    }

    /**
     * 设置 [送货地址]
     */
    public void setShiptoComposite(String shiptoComposite){
        this.shiptoComposite = shiptoComposite ;
        this.modify("shipto_composite",shiptoComposite);
    }

    /**
     * 设置 [送货地的电话号码]
     */
    public void setShiptoTelephone(String shiptoTelephone){
        this.shiptoTelephone = shiptoTelephone ;
        this.modify("shipto_telephone",shiptoTelephone);
    }

    /**
     * 设置 [帐单寄往省/市/自治区]
     */
    public void setBilltoStateorprovince(String billtoStateorprovince){
        this.billtoStateorprovince = billtoStateorprovince ;
        this.modify("billto_stateorprovince",billtoStateorprovince);
    }

    /**
     * 设置 [付款条件]
     */
    public void setPaymenttermscode(String paymenttermscode){
        this.paymenttermscode = paymenttermscode ;
        this.modify("paymenttermscode",paymenttermscode);
    }

    /**
     * 设置 [负责人]
     */
    public void setOwnername(String ownername){
        this.ownername = ownername ;
        this.modify("ownername",ownername);
    }

    /**
     * 设置 [折后金额总计]
     */
    public void setTotalamountlessfreight(BigDecimal totalamountlessfreight){
        this.totalamountlessfreight = totalamountlessfreight ;
        this.modify("totalamountlessfreight",totalamountlessfreight);
    }

    /**
     * 设置 [Traversed Path]
     */
    public void setTraversedpath(String traversedpath){
        this.traversedpath = traversedpath ;
        this.modify("traversedpath",traversedpath);
    }

    /**
     * 设置 [帐单寄往地的传真号码]
     */
    public void setBilltoFax(String billtoFax){
        this.billtoFax = billtoFax ;
        this.modify("billto_fax",billtoFax);
    }

    /**
     * 设置 [折扣金额总和]
     */
    public void setTotaldiscountamount(BigDecimal totaldiscountamount){
        this.totaldiscountamount = totaldiscountamount ;
        this.modify("totaldiscountamount",totaldiscountamount);
    }

    /**
     * 设置 [总税款 (Base)]
     */
    public void setTotaltaxBase(BigDecimal totaltaxBase){
        this.totaltaxBase = totaltaxBase ;
        this.modify("totaltax_base",totaltaxBase);
    }

    /**
     * 设置 [报价单折扣金额 (Base)]
     */
    public void setDiscountamountBase(BigDecimal discountamountBase){
        this.discountamountBase = discountamountBase ;
        this.modify("discountamount_base",discountamountBase);
    }

    /**
     * 设置 [运费金额]
     */
    public void setFreightamount(BigDecimal freightamount){
        this.freightamount = freightamount ;
        this.modify("freightamount",freightamount);
    }

    /**
     * 设置 [货币]
     */
    public void setCurrencyname(String currencyname){
        this.currencyname = currencyname ;
        this.modify("currencyname",currencyname);
    }

    /**
     * 设置 [价目表]
     */
    public void setPricelevelname(String pricelevelname){
        this.pricelevelname = pricelevelname ;
        this.modify("pricelevelname",pricelevelname);
    }

    /**
     * 设置 [商机]
     */
    public void setOpportunityname(String opportunityname){
        this.opportunityname = opportunityname ;
        this.modify("opportunityname",opportunityname);
    }

    /**
     * 设置 [SLA]
     */
    public void setSlaname(String slaname){
        this.slaname = slaname ;
        this.modify("slaname",slaname);
    }

    /**
     * 设置 [源市场活动]
     */
    public void setCampaignname(String campaignname){
        this.campaignname = campaignname ;
        this.modify("campaignname",campaignname);
    }

    /**
     * 设置 [价目表]
     */
    public void setPricelevelid(String pricelevelid){
        this.pricelevelid = pricelevelid ;
        this.modify("pricelevelid",pricelevelid);
    }

    /**
     * 设置 [SLA]
     */
    public void setSlaid(String slaid){
        this.slaid = slaid ;
        this.modify("slaid",slaid);
    }

    /**
     * 设置 [货币]
     */
    public void setTransactioncurrencyid(String transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }

    /**
     * 设置 [商机]
     */
    public void setOpportunityid(String opportunityid){
        this.opportunityid = opportunityid ;
        this.modify("opportunityid",opportunityid);
    }

    /**
     * 设置 [源市场活动]
     */
    public void setCampaignid(String campaignid){
        this.campaignid = campaignid ;
        this.modify("campaignid",campaignid);
    }


}


