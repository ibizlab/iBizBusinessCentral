package cn.ibizlab.businesscentral.core.base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.base.domain.ApplicationRole;
import cn.ibizlab.businesscentral.core.base.filter.ApplicationRoleSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[ApplicationRole] 服务对象接口
 */
public interface IApplicationRoleService extends IService<ApplicationRole>{

    boolean create(ApplicationRole et) ;
    void createBatch(List<ApplicationRole> list) ;
    boolean update(ApplicationRole et) ;
    void updateBatch(List<ApplicationRole> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    ApplicationRole get(String key) ;
    ApplicationRole getDraft(ApplicationRole et) ;
    boolean checkKey(ApplicationRole et) ;
    boolean save(ApplicationRole et) ;
    void saveBatch(List<ApplicationRole> list) ;
    Page<ApplicationRole> searchDefault(ApplicationRoleSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<ApplicationRole> getApplicationroleByIds(List<String> ids) ;
    List<ApplicationRole> getApplicationroleByEntities(List<ApplicationRole> entities) ;
}


