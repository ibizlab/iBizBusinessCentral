package cn.ibizlab.businesscentral.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({com.alibaba.cloud.seata.feign.SeataFeignClientAutoConfiguration.class})
public class DevBootAutoConfiguration {

}
