import { MockAdapter } from '../mock-adapter';
const mock = MockAdapter.getInstance();

import Mock from 'mockjs'

// 获取studio链接数据
mock.onGet('./assets/json/view-config.json').reply((config: any) => {
    let status = MockAdapter.mockStatus(config);
    return [status,{
                "wfgrouppickupgridview": {
            "title": "角色/用户组选择表格视图",
            "caption": "角色/用户组",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "workflow",
            "viewname": "WFGroupPickupGridView",
            "viewtag": "00c544eb087eb999754c164cc4522767"
        },
        "ibzpostgridview": {
            "title": "岗位管理",
            "caption": "岗位管理",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "ou",
            "viewname": "IBZPostGridView",
            "viewtag": "024224b7592a26707a1842f7c13b9e3a"
        },
        "accountsummary": {
            "title": "客户概览",
            "caption": "客户概览",
            "viewtype": "DEPORTALVIEW",
            "viewmodule": "Base",
            "viewname": "AccountSummary",
            "viewtag": "03006e40067bad32bb63465fbf24239f"
        },
        "leadconleadgridview": {
            "title": "潜在顾客信息",
            "caption": "潜在顾客信息",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Sales",
            "viewname": "LeadConLeadGridView",
            "viewtag": "035209dd84240c81516eec3a0b59ed59"
        },
        "campaigneditview": {
            "title": "市场活动编辑视图",
            "caption": "市场活动",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Marketing",
            "viewname": "CampaignEditView",
            "viewtag": "04a3cac1e181e6e70b3c2c075cf8cced"
        },
        "ibzemployeegridview": {
            "title": "人员表格视图",
            "caption": "人员",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "ou",
            "viewname": "IBZEmployeeGridView",
            "viewtag": "04f380f1b1f25c0ca738d43a66ee8886"
        },
        "entitlementpickupgridview": {
            "title": "权利选择表格视图",
            "caption": "权利",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "Service",
            "viewname": "EntitlementPickupGridView",
            "viewtag": "05b6d10438027dfbf6ba017b3bd9ba4d"
        },
        "salesliteratureeditview": {
            "title": "销售宣传资料编辑视图",
            "caption": "销售宣传资料",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Sales",
            "viewname": "SalesLiteratureEditView",
            "viewtag": "067ac9604611757e7fb2802d011091cb"
        },
        "ibzemployeepickupview": {
            "title": "人员数据选择视图",
            "caption": "人员",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "ou",
            "viewname": "IBZEmployeePickupView",
            "viewtag": "067b7105c6d7b908acbb7c7738c7cbb9"
        },
        "sysrolepickupview": {
            "title": "角色数据选择视图",
            "caption": "系统角色",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "uaa",
            "viewname": "SysRolePickupView",
            "viewtag": "0713bc3f42db8b49e59593fa2d0bcef2"
        },
        "activitypointergridview": {
            "title": "活动",
            "caption": "活动信息",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Base",
            "viewname": "ActivityPointerGridView",
            "viewtag": "07c222a488dc6088cd6cafff113e4fd0"
        },
        "opportunitysummary": {
            "title": "商机概览",
            "caption": "商机概览",
            "viewtype": "DEPORTALVIEW",
            "viewmodule": "Sales",
            "viewname": "OpportunitySummary",
            "viewtag": "0845ca2722fffcce4896282c8f4eaa54"
        },
        "productsalesliteraturesallitproeditview": {
            "title": "宣传资料产品明细",
            "caption": "产品宣传资料",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Sales",
            "viewname": "ProductSalesLiteratureSalLitProEditView",
            "viewtag": "0886e5f482e4371e9845915787c68e0b"
        },
        "goalpickupview": {
            "title": "目标数据选择视图",
            "caption": "目标",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "Sales",
            "viewname": "GoalPickupView",
            "viewtag": "0ac803c2ada4edab5985c5c5a42ff4f3"
        },
        "contactinfo_person": {
            "title": "联系人编辑视图",
            "caption": "联系人",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Base",
            "viewname": "ContactInfo_Person",
            "viewtag": "0adb2b09360c3dc7b1707a466e5426a2"
        },
        "legalmastersummaryview": {
            "title": "主信息概览看板视图",
            "caption": "主信息概览",
            "viewtype": "DEPORTALVIEW",
            "viewmodule": "Base",
            "viewname": "LegalMasterSummaryView",
            "viewtag": "0af820aaea4d41e3c334e7c8bbcc9680"
        },
        "accountinfo_all": {
            "title": "客户编辑视图",
            "caption": "客户",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Base",
            "viewname": "AccountInfo_All",
            "viewtag": "0b19babbda9e6d6de6350ec800223446"
        },
        "productpickupgridview": {
            "title": "产品选择表格视图",
            "caption": "产品",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "Product",
            "viewname": "ProductPickupGridView",
            "viewtag": "0d27fd47dc558a0e38ac5ddfbf76b184"
        },
        "omhierarchygridview": {
            "title": "组织层次结构表格视图",
            "caption": "组织层次结构",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Base",
            "viewname": "OMHierarchyGridView",
            "viewtag": "0d30731deec5071077cc286f60ffe6a6"
        },
        "invoicepaidgridview": {
            "title": "发票信息",
            "caption": "发票",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Finance",
            "viewname": "InvoicePaidGridView",
            "viewtag": "0dc0318f68c6b9d051bdbf0249f67a7f"
        },
        "opportunityproducteditview": {
            "title": "商机产品编辑视图",
            "caption": "商机产品",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Sales",
            "viewname": "OpportunityProductEditView",
            "viewtag": "0dc8bc62a649eba55c7dc393ef605bcd"
        },
        "contacttabexpview": {
            "title": "联系人",
            "caption": "联系人",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "Base",
            "viewname": "ContactTabExpView",
            "viewtag": "0e7e8b4b97d616034977d3a8bd086ec1"
        },
        "ibzdepartmentgridview": {
            "title": "部门表格视图",
            "caption": "部门",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "ou",
            "viewname": "IBZDepartmentGridView",
            "viewtag": "0eaeff373019c706342f46fd5806a30a"
        },
        "knowledgearticleincidentgridview": {
            "title": "知识文章事件表格视图",
            "caption": "知识文章事件",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Service",
            "viewname": "KnowledgeArticleIncidentGridView",
            "viewtag": "0f71b535a4ea4c41d04fde25f3494994"
        },
        "leadongridview": {
            "title": "潜在顾客信息",
            "caption": "潜在顾客信息",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Sales",
            "viewname": "LeadOnGridView",
            "viewtag": "0f98226313015a091cf68e3b52a6dec8"
        },
        "priceleveleditview": {
            "title": "价目表",
            "caption": "价目表",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Product",
            "viewname": "PriceLevelEditView",
            "viewtag": "105856a272ba02c0edc3d740f6bf5ba2"
        },
        "ibzorganizationgridview": {
            "title": "单位管理",
            "caption": "单位管理",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "ou",
            "viewname": "IBZOrganizationGridView",
            "viewtag": "108c0efd0f9277a0b30cbc3416da023d"
        },
        "accountgradation": {
            "title": "客户关系导航",
            "caption": "客户关系导航",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "Base",
            "viewname": "AccountGradation",
            "viewtag": "11b8701c9f132c708f7bc1056670025f"
        },
        "setting": {
            "title": "设置",
            "caption": "设置",
            "viewtype": "APPPORTALVIEW",
            "viewmodule": "Ungroup",
            "viewname": "Setting",
            "viewtag": "127c15f76b66ca89e790d974f63abb89"
        },
        "opportunitycompetitoroppcompgridview": {
            "title": "商机对手表格视图",
            "caption": "商机对手",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Sales",
            "viewname": "OpportunityCompetitorOppCompGridView",
            "viewtag": "127c6fcfb5e1c721591eda4364367200"
        },
        "campaigninfo_manager": {
            "title": "管理信息",
            "caption": "管理信息",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Marketing",
            "viewname": "CampaignInfo_Manager",
            "viewtag": "12ac45a6d92da65d92a5dc25a7b37aac"
        },
        "knowledgearticleinfo_klartview": {
            "title": "知识文章信息视图",
            "caption": "知识文章",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Base",
            "viewname": "KnowledgeArticleInfo_KlArtView",
            "viewtag": "157ecd1a00739685ea7781f954188f07"
        },
        "opportunitypickupview": {
            "title": "商机数据选择视图",
            "caption": "商机",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "Sales",
            "viewname": "OpportunityPickupView",
            "viewtag": "15b10fa0deea0c5b96c1e498d79b69e5"
        },
        "jobsloggridview": {
            "title": "日志",
            "caption": "日志",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "task",
            "viewname": "JobsLogGridView",
            "viewtag": "15b8e4229d86ec0e20e36920321c7835"
        },
        "campaigneffectivegridview": {
            "title": "市场活动信息",
            "caption": "市场活动",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Marketing",
            "viewname": "CampaignEffectiveGridView",
            "viewtag": "165d222030bb4974999a8b69a7a80680"
        },
        "salesorderstatetabview": {
            "title": "订单信息",
            "caption": "订单信息",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "Sales",
            "viewname": "SalesOrderStateTabView",
            "viewtag": "166048749d603698637c9f8a207ddcb5"
        },
        "lettereditview": {
            "title": "信件编辑视图",
            "caption": "信件",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Base",
            "viewname": "LetterEditView",
            "viewtag": "16a2a9f33008222e13eba1058213ed9e"
        },
        "discounttypeeditview": {
            "title": "折扣表",
            "caption": "折扣表",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Sales",
            "viewname": "DiscountTypeEditView",
            "viewtag": "16d3f3e48a8009a37133993b0dbbe4aa"
        },
        "legalmasterquickview": {
            "title": "法人快速新建视图",
            "caption": "快速新建",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "Base",
            "viewname": "LegalMasterQuickView",
            "viewtag": "16f7707748a877dc5592669d2dec227e"
        },
        "contacteditmarket": {
            "title": "联系人选项操作视图",
            "caption": "市场营销信息",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "Base",
            "viewname": "ContactEditMarket",
            "viewtag": "17c110bf2c4968bf981e90f9ca81194c"
        },
        "sysuserpickupgridview": {
            "title": "用户表选择表格视图",
            "caption": "系统用户",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "uaa",
            "viewname": "SysUserPickupGridView",
            "viewtag": "1871252bcaed3c9ef49e9f4616c57d6c"
        },
        "incidentcustomerpickupgridview": {
            "title": "案例客户选择表格视图",
            "caption": "案例客户",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "Service",
            "viewname": "IncidentCustomerPickupGridView",
            "viewtag": "193cc211e5bf7fa2d238bafc7a4fbe93"
        },
        "competitorpickupview": {
            "title": "竞争对手数据选择视图",
            "caption": "竞争对手",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "Sales",
            "viewname": "CompetitorPickupView",
            "viewtag": "1962e36b33e32f2303be7f3fb02a043b"
        },
        "ibizlistinfo": {
            "title": "市场营销列表",
            "caption": "市场营销列表",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "Marketing",
            "viewname": "IBizListInfo",
            "viewtag": "1a3ced3f8b4a2e927a9e4ae11a1d92f5"
        },
        "ibzdeptmembereditview": {
            "title": "部门成员编辑视图",
            "caption": "部门成员",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "ou",
            "viewname": "IBZDeptMemberEditView",
            "viewtag": "1ba3a6683818e6058b6a5d0f35fce428"
        },
        "productsubstituteeditview": {
            "title": "产品关系编辑视图",
            "caption": "产品替换",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Product",
            "viewname": "ProductSubstituteEditView",
            "viewtag": "1bee7d502bfbd5a3949034cfd2fe1b13"
        },
        "dictcatalogeditview": {
            "title": "目录",
            "caption": "目录",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "dict",
            "viewname": "DictCatalogEditView",
            "viewtag": "1c4877e2de013f10cf185d5fbe5c4244"
        },
        "sysuserrolegridview": {
            "title": "用户角色关系表表格视图",
            "caption": "用户角色关系",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "uaa",
            "viewname": "SysUserRoleGridView",
            "viewtag": "1c9a59d576b1beb6a7062d8cd90d0ef0"
        },
        "salesorderinfo": {
            "title": "订单信息",
            "caption": "订单",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "Sales",
            "viewname": "SalesOrderInfo",
            "viewtag": "1cd4875218cbe15caced6d53a744335b"
        },
        "opportunityoppprodashboardview": {
            "title": "商机数据看板视图",
            "caption": "商机",
            "viewtype": "DEPORTALVIEW",
            "viewmodule": "Sales",
            "viewname": "OpportunityOppProDashboardView",
            "viewtag": "1d47f429281928a3e5a0b5a8e71aaa7a"
        },
        "entitlementpickupview": {
            "title": "权利数据选择视图",
            "caption": "权利",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "Service",
            "viewname": "EntitlementPickupView",
            "viewtag": "1ecb36d3d6a944555e7d2d1485844cbf"
        },
        "sysappeditview": {
            "title": "接入应用",
            "caption": "接入应用",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "uaa",
            "viewname": "SysAppEditView",
            "viewtag": "1fc215f3ccaaf46a79a6c1ef835baf92"
        },
        "appointmentquickcreate": {
            "title": "约会",
            "caption": "约会",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "Base",
            "viewname": "AppointmentQuickCreate",
            "viewtag": "20d17c459beb204dde3db44e56247bcf"
        },
        "accountstopgridview": {
            "title": "客户表格视图",
            "caption": "客户信息",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Base",
            "viewname": "AccountStopGridView",
            "viewtag": "2145d8dbe033f0797c43ee5ee66bbd09"
        },
        "leadstatustabview": {
            "title": "潜在顾客",
            "caption": "潜在顾客",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "Sales",
            "viewname": "LeadStatusTabView",
            "viewtag": "22c9503382246c4d49455f2b62fcd54b"
        },
        "goalstatetabview": {
            "title": "目标",
            "caption": "目标",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "Sales",
            "viewname": "GoalStateTabView",
            "viewtag": "22e8a76fa7fd741d5924fb98604b46f0"
        },
        "employeemastersummaryview": {
            "title": "主信息概览看板视图",
            "caption": "员工",
            "viewtype": "DEPORTALVIEW",
            "viewmodule": "HumanResource",
            "viewname": "EmployeeMasterSummaryView",
            "viewtag": "238378e8fcc75e3b771a92aef1d7b3d2"
        },
        "accounttreegridexview": {
            "title": "客户树表格视图",
            "caption": "客户",
            "viewtype": "DETREEGRIDEXVIEW",
            "viewmodule": "Base",
            "viewname": "AccountTreeGridExView",
            "viewtag": "23a3ce328957b360a2d477ac7aeba073"
        },
        "activitypointerbyparentkey": {
            "title": "活动",
            "caption": "活动",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Base",
            "viewname": "ActivityPointerByParentKey",
            "viewtag": "23c70cb5cf8a2dc519287dbb44b7c44d"
        },
        "connectionrolepickupgridview": {
            "title": "连接角色选择表格视图",
            "caption": "连接角色",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "Runtime",
            "viewname": "ConnectionRolePickupGridView",
            "viewtag": "243669c525bf93275af379de42863366"
        },
        "ibzemployeeeditview": {
            "title": "人员管理",
            "caption": "人员管理",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "ou",
            "viewname": "IBZEmployeeEditView",
            "viewtag": "243a593137a8e444bc39f8a5385d9900"
        },
        "ibzorganizationpickupview": {
            "title": "单位机构数据选择视图",
            "caption": "单位机构",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "ou",
            "viewname": "IBZOrganizationPickupView",
            "viewtag": "2644137e77038f3666fcea7258e6953d"
        },
        "campaignsummary_head": {
            "title": "市场活动概览",
            "caption": "市场活动概览",
            "viewtype": "DEPORTALVIEW",
            "viewmodule": "Marketing",
            "viewname": "CampaignSummary_Head",
            "viewtag": "265a697ca106356d6fddad60d59f89db"
        },
        "competitorgridview": {
            "title": "竞争对手",
            "caption": "竞争对手",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Sales",
            "viewname": "CompetitorGridView",
            "viewtag": "266689a977a810e46f94baba7255f96b"
        },
        "wfremodelgridview": {
            "title": "流程模型表格视图",
            "caption": "流程模型",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "workflow",
            "viewname": "WFREModelGridView",
            "viewtag": "2682fc518526a12d6802ab56bcef6650"
        },
        "campaignresponseeditview": {
            "title": "市场活动响应编辑视图",
            "caption": "市场活动响应",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Marketing",
            "viewname": "CampaignResponseEditView",
            "viewtag": "27d46affbc302df36338e48c2a268edb"
        },
        "contactpickupgridview": {
            "title": "联系人选择表格视图",
            "caption": "联系人",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "Base",
            "viewname": "ContactPickupGridView",
            "viewtag": "2821c085b75fe9f25be6b52841c934a0"
        },
        "opportunityconoppgridview": {
            "title": "商机表格视图",
            "caption": "商机信息",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Sales",
            "viewname": "OpportunityConOppGridView",
            "viewtag": "2999937490155e29862ca7e63d89bef5"
        },
        "ibizliststatetabview": {
            "title": "市场营销列表状态分页视图",
            "caption": "市场营销列表",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "Marketing",
            "viewname": "IBizListStateTabView",
            "viewtag": "29b0091518850a55f7a722a43e49c24f"
        },
        "leaddashboardview": {
            "title": "潜在顾客数据看板视图",
            "caption": "潜在顾客",
            "viewtype": "DEPORTALVIEW",
            "viewmodule": "Sales",
            "viewname": "LeadDashboardView",
            "viewtag": "29bfc8028805f4fa7c7aca9ce667b084"
        },
        "contacteditbook": {
            "title": "联系人选项操作视图",
            "caption": "记账信息",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "Base",
            "viewname": "ContactEditBook",
            "viewtag": "2aba05b699956fb36abfc76df8893eec"
        },
        "invoicedetaileditview": {
            "title": "发票产品编辑视图",
            "caption": "发票产品",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Finance",
            "viewname": "InvoiceDetailEditView",
            "viewtag": "2ad8344cc876a803a44b7bdb295336f8"
        },
        "quoteoppquagridview": {
            "title": "报价单表格视图",
            "caption": "报价单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Sales",
            "viewname": "QuoteOppQuaGridView",
            "viewtag": "2b1c941925543cdddcad6a7a19fdc0fe"
        },
        "campaignactivitygridview": {
            "title": "市场活动项目表格视图",
            "caption": "市场活动项目",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Marketing",
            "viewname": "CampaignActivityGridView",
            "viewtag": "2b6ee20210baf6e3974e3b6288be6ad7"
        },
        "competitorproductcompprogridview": {
            "title": "竞争对手产品信息",
            "caption": "竞争对手产品",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Sales",
            "viewname": "CompetitorProductCompProGridView",
            "viewtag": "2c800d6825295d71fde22ca3fdee8d2c"
        },
        "productassociationgridview": {
            "title": "产品关联表格视图",
            "caption": "产品关联",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Product",
            "viewname": "ProductAssociationGridView",
            "viewtag": "2c8c9b7299ec8d3f2ee5a5125c337086"
        },
        "operationunitmastertabinfoview": {
            "title": "主信息总览视图",
            "caption": "运营单位",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "Base",
            "viewname": "OperationUnitMasterTabInfoView",
            "viewtag": "2d4ec7cc0257c490f3536b96d36ec431"
        },
        "salesorderinfo_soview": {
            "title": "订单编辑视图",
            "caption": "订单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Sales",
            "viewname": "SalesOrderInfo_SOView",
            "viewtag": "2e83f094f56f133fc616da2318153c79"
        },
        "incidentbyparentkey": {
            "title": "服务案例信息",
            "caption": "案例",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Service",
            "viewname": "IncidentByParentKey",
            "viewtag": "30f706dfc8113f424d9fa00bd3000049"
        },
        "campaignquickcreateview": {
            "title": "快速新建",
            "caption": "市场活动",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "Marketing",
            "viewname": "CampaignQuickCreateView",
            "viewtag": "31a0f8ef2c78a7676dff231d130ce1f9"
        },
        "discounttypegridview": {
            "title": "折扣表",
            "caption": "折扣表",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Sales",
            "viewname": "DiscountTypeGridView",
            "viewtag": "3437f3d5d3dae01266f434e1d6905f59"
        },
        "uomschedulepickupview": {
            "title": "计价单位组数据选择视图",
            "caption": "计价单位组",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "Base",
            "viewname": "UomSchedulePickupView",
            "viewtag": "34b2636b4563ef6e297e6ff4cd5adaf8"
        },
        "incidentedit_chlidincident": {
            "title": "子案例信息",
            "caption": "案例",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Service",
            "viewname": "IncidentEdit_ChlidIncident",
            "viewtag": "352e10be4e9166655027a1703403c7a2"
        },
        "omhierarchycateditview": {
            "title": "实体编辑视图",
            "caption": "结构层次类别",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Base",
            "viewname": "OMHierarchyCatEditView",
            "viewtag": "36347bf25baca36cae03b7751ab5c40c"
        },
        "central": {
            "title": "企业中心",
            "caption": "企业中心",
            "viewtype": "APPINDEXVIEW",
            "viewmodule": "Base",
            "viewname": "Central",
            "viewtag": "36502c7cedb99f1a7d45fc5651242ac1"
        },
        "operationunitmastereditview": {
            "title": "主编辑视图",
            "caption": "运营单位编辑",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Base",
            "viewname": "OperationUnitMasterEditView",
            "viewtag": "379739756d12bced5a61e3ce3805de45"
        },
        "accountinfo_introduction": {
            "title": "客户编辑视图",
            "caption": "客户",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Base",
            "viewname": "AccountInfo_Introduction",
            "viewtag": "3892e7dc0755319f27b7faf83ac1bcdc"
        },
        "incidenteffectivegridview": {
            "title": "服务案例信息",
            "caption": "案例",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Service",
            "viewname": "IncidentEffectiveGridView",
            "viewtag": "38b5c0650071377cfb7641591ca47ef8"
        },
        "productsalesliteraturesallitprogridview": {
            "title": "宣传资料产品明细",
            "caption": "产品宣传资料",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Sales",
            "viewname": "ProductSalesLiteratureSalLitProGridView",
            "viewtag": "38f9d6dc0ccf05c54c833e8487536f7d"
        },
        "goalusablegridview": {
            "title": "目标信息",
            "caption": "目标",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Sales",
            "viewname": "GoalUsableGridView",
            "viewtag": "38fb8e1dbdbf211488968c14e15dfca0"
        },
        "ibzdepartmenteditview": {
            "title": "部门管理",
            "caption": "部门管理",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "ou",
            "viewname": "IBZDepartmentEditView",
            "viewtag": "3914cd8573fdbe9989a79cdfe6fb64fe"
        },
        "dictoptioneditview": {
            "title": "字典项",
            "caption": "字典项",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "dict",
            "viewname": "DictOptionEditView",
            "viewtag": "39f8bd0f6192e0700ccf6cd4e8c1a477"
        },
        "leadquickcreate": {
            "title": "快速新建",
            "caption": "快速新建",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "Sales",
            "viewname": "LeadQuickCreate",
            "viewtag": "3bc8d62c7f1f0f0767b3697f9daccd1f"
        },
        "competitoredit_datapanelview": {
            "title": "头部信息编辑",
            "caption": "竞争对手",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Sales",
            "viewname": "CompetitorEdit_DataPanelView",
            "viewtag": "3cbc0d3ebb7dde0a50ef1479c4b42721"
        },
        "sysrolegridview": {
            "title": "用户角色",
            "caption": "用户角色",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "uaa",
            "viewname": "SysRoleGridView",
            "viewtag": "3cea5e52f5820561614cf766aef191e4"
        },
        "salesorderdetaileditview": {
            "title": "订单产品编辑视图",
            "caption": "订单产品",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Sales",
            "viewname": "SalesOrderDetailEditView",
            "viewtag": "3d207e919fd272f7a1a392c6e894afe2"
        },
        "knowledgearticleedit_datapanelview": {
            "title": "头部信息编辑",
            "caption": "知识文章",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Base",
            "viewname": "KnowledgeArticleEdit_DataPanelView",
            "viewtag": "3d32edc6311f3f2439502df807425c39"
        },
        "listaccountinner": {
            "title": "营销列表-账户表格视图",
            "caption": "营销列表-账户",
            "viewtype": "DEGRIDVIEW9",
            "viewmodule": "Marketing",
            "viewname": "ListAccountInner",
            "viewtag": "3e4f6357076010351060324cc201be37"
        },
        "invoicedetailinvoiceprogridview": {
            "title": "发票产品表格视图",
            "caption": "发票产品",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Finance",
            "viewname": "InvoiceDetailInvoiceProGridView",
            "viewtag": "3ece01f022e2851161be9dcc3bb874df"
        },
        "contacteditperson": {
            "title": "联系人选项操作视图",
            "caption": "个人信息",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "Base",
            "viewname": "ContactEditPerson",
            "viewtag": "3f21ac18ebbb743ea8e7bf930012ed8a"
        },
        "incidentcancelgridview": {
            "title": "服务案例信息",
            "caption": "案例",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Service",
            "viewname": "IncidentCancelGridView",
            "viewtag": "40e7faa7fcb56490d33705bca1839020"
        },
        "productgridview": {
            "title": "产品信息",
            "caption": "产品",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Product",
            "viewname": "ProductGridView",
            "viewtag": "410f82d1851f2d6fb2cc4705f676293e"
        },
        "goalsummaryview": {
            "title": "目标数据看板视图",
            "caption": "目标",
            "viewtype": "DEPORTALVIEW",
            "viewmodule": "Sales",
            "viewname": "GoalSummaryView",
            "viewtag": "42587b9e3ef774294165ad5687e49442"
        },
        "accountinfo_contactsetting": {
            "title": "客户编辑视图",
            "caption": "客户",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Base",
            "viewname": "AccountInfo_ContactSetting",
            "viewtag": "43e265ac8bd70979bf820e9a802205f6"
        },
        "jobsinfogridview": {
            "title": "任务",
            "caption": "任务",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "task",
            "viewname": "JobsInfoGridView",
            "viewtag": "440edea13636042e729153ea705ffcd2"
        },
        "salesorderinvoicegridview": {
            "title": "订单信息",
            "caption": "订单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Sales",
            "viewname": "SalesOrderInvoiceGridView",
            "viewtag": "450f642a2a88478ed2d657124a7f37ff"
        },
        "employeegridview": {
            "title": "员工表格视图",
            "caption": "员工",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "HumanResource",
            "viewname": "EmployeeGridView",
            "viewtag": "45bfe897b1a5da60114badb98fe0676a"
        },
        "uomeditview": {
            "title": "计价单位",
            "caption": "计价单位",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Base",
            "viewname": "UomEditView",
            "viewtag": "45c661dc724cc625384b2d6c6d0b3c98"
        },
        "activitypointereditview": {
            "title": "活动编辑视图",
            "caption": "活动",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Base",
            "viewname": "ActivityPointerEditView",
            "viewtag": "45ed029fd1958ac54e154ea1ea1c2057"
        },
        "opportunityinfo": {
            "title": "商机信息",
            "caption": "商机信息",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "Sales",
            "viewname": "OpportunityInfo",
            "viewtag": "46a9ce2dc32847a6a56ef2fbc5a3d4a3"
        },
        "productinfo": {
            "title": "产品信息",
            "caption": "产品",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "Product",
            "viewname": "ProductInfo",
            "viewtag": "46b67a97c892710836659c28883b3963"
        },
        "leadmpickupview": {
            "title": "潜在顾客数据多项选择视图",
            "caption": "潜在顾客",
            "viewtype": "DEMPICKUPVIEW",
            "viewmodule": "Sales",
            "viewname": "LeadMPickupView",
            "viewtag": "472a562213edd8e96417a52a48bc0548"
        },
        "accountbyparentkey": {
            "title": "客户表格视图",
            "caption": "客户信息",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Base",
            "viewname": "AccountByParentKey",
            "viewtag": "4794d976902a3cbb623e23495f17787f"
        },
        "leadinfo_market": {
            "title": "潜在顾客编辑视图",
            "caption": "潜在顾客",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Sales",
            "viewname": "LeadInfo_Market",
            "viewtag": "47e870f1b926f68c4f9f0978b0fe67de"
        },
        "ibizlistedit_datapanelview": {
            "title": "头部信息编辑",
            "caption": "市场营销列表",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Marketing",
            "viewname": "IBizListEdit_DataPanelView",
            "viewtag": "484dc844319644a9483df32dfcf37644"
        },
        "productstatetabview": {
            "title": "产品信息",
            "caption": "产品信息",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "Product",
            "viewname": "ProductStateTabView",
            "viewtag": "486b2cf6464c79f2410ceec8c278d3bc"
        },
        "competitoreditview": {
            "title": "竞争对手编辑视图",
            "caption": "竞争对手",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Sales",
            "viewname": "CompetitorEditView",
            "viewtag": "4965d11740e43ea29a6d2cec1b10d1c8"
        },
        "goalinfo_goalview": {
            "title": "目标信息视图",
            "caption": "目标",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Sales",
            "viewname": "GoalInfo_GoalView",
            "viewtag": "4a0c7e2ae743d2d43c3a8522eb610d70"
        },
        "leadcompetitorleadgridview": {
            "title": "潜在客户",
            "caption": "潜在客户对手",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Sales",
            "viewname": "LeadCompetitorLeadGridView",
            "viewtag": "4a35c488f82e5950bbe19492e065655f"
        },
        "leadedit_datapanelview": {
            "title": "头部信息编辑",
            "caption": "潜在顾客",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Sales",
            "viewname": "LeadEdit_DataPanelView",
            "viewtag": "4a63f8787771d3c6f62487274c6cfa3b"
        },
        "campaignlisteditview": {
            "title": "市场活动-营销列表编辑视图",
            "caption": "市场活动-营销列表",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Marketing",
            "viewname": "CampaignListEditView",
            "viewtag": "4b2d33e743a8625d5a167e9feb58c3e0"
        },
        "connectionrolegridview": {
            "title": "连接角色",
            "caption": "连接角色",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Runtime",
            "viewname": "ConnectionRoleGridView",
            "viewtag": "4bb16ba8f2e41806855f2b10ecc5656c"
        },
        "invoicebyparentkey": {
            "title": "发票信息",
            "caption": "发票",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Finance",
            "viewname": "InvoiceByParentKey",
            "viewtag": "4c22963679dc3b1a48694e42e5310249"
        },
        "campaignresponsequickcreate": {
            "title": "快速新建",
            "caption": "快速新建",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "Marketing",
            "viewname": "CampaignResponseQuickCreate",
            "viewtag": "4c6495f1346f9062a924454886f5b3e1"
        },
        "opportunityinfo_product": {
            "title": "商机编辑视图",
            "caption": "商机",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Sales",
            "viewname": "OpportunityInfo_Product",
            "viewtag": "4ce0da8cee5e98bfec0e10630767d8ec"
        },
        "competitormpickupview": {
            "title": "竞争对手数据多项选择视图",
            "caption": "竞争对手",
            "viewtype": "DEMPICKUPVIEW",
            "viewmodule": "Sales",
            "viewname": "CompetitorMPickupView",
            "viewtag": "4d07a7a728748c14e057d047a6abe42c"
        },
        "goaleditview": {
            "title": "目标编辑视图",
            "caption": "目标",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Sales",
            "viewname": "GoalEditView",
            "viewtag": "4d3880dc1e763d533b96ec3a725660a3"
        },
        "listaccountquickcreatebylist": {
            "title": "查找客户",
            "caption": "查找客户",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "Marketing",
            "viewname": "ListAccountQuickCreateByList",
            "viewtag": "4ef2436b7ea04c193b715aa1f6327e7e"
        },
        "campaigninfo_campagin": {
            "title": "活动信息",
            "caption": "活动信息",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Marketing",
            "viewname": "CampaignInfo_Campagin",
            "viewtag": "5073ab2b1f8d4bc34669e325bcd8726c"
        },
        "leadgridview": {
            "title": "潜在顾客信息",
            "caption": "潜在顾客信息",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Sales",
            "viewname": "LeadGridView",
            "viewtag": "51191ff6d923ba71feea7b16f997540c"
        },
        "subjectpickupgridview": {
            "title": "主题选择表格视图",
            "caption": "主题",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "Base",
            "viewname": "SubjectPickupGridView",
            "viewtag": "55bbb00957affb96575a785c6c0b7a71"
        },
        "quoteedit_datapanelview": {
            "title": "头部信息编辑",
            "caption": "报价单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Sales",
            "viewname": "QuoteEdit_DataPanelView",
            "viewtag": "567e0b1363f5ebe1169cd805cc22ab73"
        },
        "contactusablegridview": {
            "title": "联系人信息",
            "caption": "联系人信息",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Base",
            "viewname": "ContactUsableGridView",
            "viewtag": "57205a63201b8438594d50af5343f060"
        },
        "goaledit_childgoalview": {
            "title": "子目标信息",
            "caption": "目标",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Sales",
            "viewname": "GoalEdit_ChildGoalView",
            "viewtag": "57dadcb9abbd039ba91889addeb63d73"
        },
        "invoiceeditview": {
            "title": "发票编辑视图",
            "caption": "发票",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Finance",
            "viewname": "InvoiceEditView",
            "viewtag": "57f91db28a14c2e04900566316281f32"
        },
        "centeralportal": {
            "title": "仪表盘",
            "caption": "仪表盘",
            "viewtype": "APPPORTALVIEW",
            "viewmodule": "Ungroup",
            "viewname": "CenteralPortal",
            "viewtag": "58199705a616a93d810b2730ae13bc8d"
        },
        "quoteinfo": {
            "title": "报价单信息",
            "caption": "报价单信息",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "Sales",
            "viewname": "QuoteInfo",
            "viewtag": "58baaa33f8632f27d99784c879d815af"
        },
        "languagelocalepickupgridview": {
            "title": "语言选择表格视图",
            "caption": "语言",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "Base",
            "viewname": "LanguageLocalePickupGridView",
            "viewtag": "598222a692f91930df2a71660504caf2"
        },
        "incidentinfo_incidentview": {
            "title": "案例信息",
            "caption": "案例",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Service",
            "viewname": "IncidentInfo_IncidentView",
            "viewtag": "59afbe7ce81dcef92047e59031769a52"
        },
        "ibzdepartmentpickupview": {
            "title": "部门选择视图",
            "caption": "部门",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "ou",
            "viewname": "IBZDepartmentPickupView",
            "viewtag": "59e18200dd83f3db1fb0ab1080594212"
        },
        "incidentchildincidentgridview": {
            "title": "子案例信息",
            "caption": "案例",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Service",
            "viewname": "IncidentChildIncidentGridView",
            "viewtag": "5ad52ebe309200b58137443d0452dade"
        },
        "leadexcludedgridview": {
            "title": "潜在顾客信息",
            "caption": "潜在顾客信息",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Sales",
            "viewname": "LeadExcludedGridView",
            "viewtag": "5bcb450be176267e4e53fd17daf80409"
        },
        "salesorderbyparentkey": {
            "title": "订单信息",
            "caption": "订单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Sales",
            "viewname": "SalesOrderByParentKey",
            "viewtag": "5bdbfcae37244eb9a426f66cd7de0e2d"
        },
        "sysusergridview": {
            "title": "用户表格视图",
            "caption": "系统用户",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "uaa",
            "viewname": "SysUserGridView",
            "viewtag": "5cbf0cef086d331b15afd5585b3c7e9e"
        },
        "activitypointereditview2": {
            "title": "活动编辑视图",
            "caption": "活动",
            "viewtype": "DEEDITVIEW2",
            "viewmodule": "Base",
            "viewname": "ActivityPointerEditView2",
            "viewtag": "5d1af1280f4470e0cd29bbf94e3fab28"
        },
        "salesorderpickupgridview": {
            "title": "订单选择表格视图",
            "caption": "订单",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "Sales",
            "viewname": "SalesOrderPickupGridView",
            "viewtag": "5dc8b0d0b7d02f854260d837787a81ec"
        },
        "ibzdepartmentpickupgridview": {
            "title": "部门选择表格视图",
            "caption": "部门",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "ou",
            "viewname": "IBZDepartmentPickupGridView",
            "viewtag": "5def5b47040f10f4e797df178afb972e"
        },
        "knowledgearticleinfoview": {
            "title": "知识文章信息",
            "caption": "知识文章",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "Base",
            "viewname": "KnowledgeArticleInfoView",
            "viewtag": "5e084a31ec05d982e37daa1e9fe918a8"
        },
        "websitecontentgridview": {
            "title": "站点内容",
            "caption": "站点内容",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "WebSite",
            "viewname": "WebSiteContentGridView",
            "viewtag": "5e118e8f33a4f0d63a19ca1dd63ee990"
        },
        "productpickupview": {
            "title": "产品数据选择视图",
            "caption": "产品",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "Product",
            "viewname": "ProductPickupView",
            "viewtag": "5ec696a90baa98180606bce6c6d803ad"
        },
        "incidentpickupview": {
            "title": "案例数据选择视图",
            "caption": "案例",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "Service",
            "viewname": "IncidentPickupView",
            "viewtag": "5f95ae3a8e8343e50037a03d1f7cc131"
        },
        "productedit_datapanelview": {
            "title": "头部信息编辑",
            "caption": "产品",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Product",
            "viewname": "ProductEdit_DataPanelView",
            "viewtag": "5fb6ebc7acda3197226c0dcd9216c7d1"
        },
        "quotepickupview": {
            "title": "报价单数据选择视图",
            "caption": "报价单",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "Sales",
            "viewname": "QuotePickupView",
            "viewtag": "60ced40e3d41f72fc7c0a9624dd4dc29"
        },
        "contacteditabstract": {
            "title": "联系人选项操作视图",
            "caption": "摘要信息",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "Base",
            "viewname": "ContactEditAbstract",
            "viewtag": "6170e58462d910e45b2901560383e927"
        },
        "ibzposteditview": {
            "title": "岗位管理",
            "caption": "岗位管理",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "ou",
            "viewname": "IBZPostEditView",
            "viewtag": "61b95b9a18630b03c8541add60f09e0e"
        },
        "knowledgearticlequickcreateview": {
            "title": "快速新建",
            "caption": "知识文章",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "Base",
            "viewname": "KnowledgeArticleQuickCreateView",
            "viewtag": "6218e1b557bb8d653fd979d0b2063062"
        },
        "salesorderpickupview": {
            "title": "订单数据选择视图",
            "caption": "订单",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "Sales",
            "viewname": "SalesOrderPickupView",
            "viewtag": "62c5a10bec3f9a6f8b3b766aaf8ee07a"
        },
        "faxeditview": {
            "title": "传真编辑视图",
            "caption": "传真",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Base",
            "viewname": "FaxEditView",
            "viewtag": "63d3061767ad534c1ddf0ee87d62e743"
        },
        "uomgridview": {
            "title": "计价单位",
            "caption": "计价单位",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Base",
            "viewname": "UomGridView",
            "viewtag": "63d4697f774a0e834db411456d55c2bb"
        },
        "wfgroupeditview": {
            "title": "流程角色",
            "caption": "流程角色",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "workflow",
            "viewname": "WFGroupEditView",
            "viewtag": "63ddb6ec1d0f1488f0f9514585d5d9a3"
        },
        "leadinfo_contact": {
            "title": "潜在顾客信息",
            "caption": "潜在顾客",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Sales",
            "viewname": "LeadInfo_Contact",
            "viewtag": "66b1b29633cd8c496c3938902fb4a2ec"
        },
        "goalpickupgridview": {
            "title": "目标选择表格视图",
            "caption": "目标",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "Sales",
            "viewname": "GoalPickupGridView",
            "viewtag": "67d9c74a71790e6c28411e5b673578c1"
        },
        "accountusablegridview": {
            "title": "客户表格视图",
            "caption": "客户信息",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Base",
            "viewname": "AccountUsableGridView",
            "viewtag": "6821428a5cabd1cace8f674de2ca9bcb"
        },
        "listleadquickcreatebylist": {
            "title": "快速新建（根据营销列表）",
            "caption": "快速新建",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "Marketing",
            "viewname": "ListLeadQuickCreateByList",
            "viewtag": "69f26cbe2194badbf0297d7ecca5d455"
        },
        "productstopgridview": {
            "title": "产品信息",
            "caption": "产品",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Product",
            "viewname": "ProductStopGridView",
            "viewtag": "6a110a93053b050d73ef73bea3cd0757"
        },
        "incidentpickupgridview": {
            "title": "案例选择表格视图",
            "caption": "案例",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "Service",
            "viewname": "IncidentPickupGridView",
            "viewtag": "6a72c9e97ea655d16ed1d9323b02973f"
        },
        "contacteditaddress": {
            "title": "联系人选项操作视图",
            "caption": "地址信息",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "Base",
            "viewname": "ContactEditAddress",
            "viewtag": "6b15651a990dc9900f2d839d79e8f866"
        },
        "contactinfo_book": {
            "title": "联系人编辑视图",
            "caption": "联系人",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Base",
            "viewname": "ContactInfo_Book",
            "viewtag": "6b1d4ab2949707c5cf48c8f4a13bd519"
        },
        "invoiceinfo": {
            "title": "发票信息",
            "caption": "发票",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "Finance",
            "viewname": "InvoiceInfo",
            "viewtag": "6b44326f26c5bd1384a7390bab14d377"
        },
        "listcontactinner": {
            "title": "营销列表-联系人表格视图",
            "caption": "联系人",
            "viewtype": "DEGRIDVIEW9",
            "viewmodule": "Marketing",
            "viewname": "ListContactInner",
            "viewtag": "6bb99f400a131d6eed9a587b0431bca7"
        },
        "leadcompetitoreditview": {
            "title": "潜在客户对手编辑视图",
            "caption": "潜在客户对手",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Sales",
            "viewname": "LeadCompetitorEditView",
            "viewtag": "6bc139fdd7da3523ea5fd1c8ed9c1d91"
        },
        "jobsinfoeditview": {
            "title": "任务",
            "caption": "任务",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "task",
            "viewname": "JobsInfoEditView",
            "viewtag": "6bca17d70561ebb545439b7384d3cfa2"
        },
        "competitorproductedit_compproview": {
            "title": "竞争对手产品信息",
            "caption": "竞争对手产品",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Sales",
            "viewname": "CompetitorProductEdit_CompProView",
            "viewtag": "6bdb8b32bc6287e43826d679e42b86da"
        },
        "employeemasterquickview": {
            "title": "快速新建视图",
            "caption": "快速新建",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "HumanResource",
            "viewname": "EmployeeMasterQuickView",
            "viewtag": "6cf74fb83871413d04f09231b1559fe0"
        },
        "contactinfo_abstract": {
            "title": "联系人编辑视图",
            "caption": "联系人",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Base",
            "viewname": "ContactInfo_Abstract",
            "viewtag": "6e994c3354fcbeb06f40febdc0fc4c36"
        },
        "ibzorganizationtreeexpview": {
            "title": "部门管理",
            "caption": "部门管理",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "ou",
            "viewname": "IBZOrganizationTreeExpView",
            "viewtag": "6ea2ff98846a9cefb635214c90968987"
        },
        "accountedit_accountinfo": {
            "title": "账户信息",
            "caption": "账户信息",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "Base",
            "viewname": "AccountEdit_AccountInfo",
            "viewtag": "6fee33bd537f424e1d88161ad1662f6c"
        },
        "uompickupview": {
            "title": "计价单位数据选择视图",
            "caption": "计价单位",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "Base",
            "viewname": "UomPickupView",
            "viewtag": "7035e5875b25e60d1fc11b06071aee1d"
        },
        "metricgridview": {
            "title": "目标度量信息",
            "caption": "目标度量",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Base",
            "viewname": "MetricGridView",
            "viewtag": "71df25d0d24b77a7e075f84ea321612b"
        },
        "accountinfo_distribution": {
            "title": "客户编辑视图",
            "caption": "客户",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Base",
            "viewname": "AccountInfo_Distribution",
            "viewtag": "723843f085a869bd0c539a0c3b4b8a06"
        },
        "uomscheduleeditview": {
            "title": "计价单位组",
            "caption": "计价单位组",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Base",
            "viewname": "UomScheduleEditView",
            "viewtag": "7243c7566af9dba07582df4e2e35ac07"
        },
        "wfprocessdefinitiongridview": {
            "title": "流程定义",
            "caption": "流程定义",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "workflow",
            "viewname": "WFProcessDefinitionGridView",
            "viewtag": "72fec87bf8339b30093576aa9097a75e"
        },
        "multipickdataacgrid": {
            "title": "负责人（客户、联系人）表格",
            "caption": "多类选择实体",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "Base",
            "viewname": "MultiPickDataACGrid",
            "viewtag": "731def77005293813123c3d71ff10f66"
        },
        "invoicestatetabview": {
            "title": "发票信息",
            "caption": "发票信息",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "Finance",
            "viewname": "InvoiceStateTabView",
            "viewtag": "73324c8d27828ead87dcca4fdbc8fb19"
        },
        "uomschedulepickupgridview": {
            "title": "计价单位组选择表格视图",
            "caption": "计价单位组",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "Base",
            "viewname": "UomSchedulePickupGridView",
            "viewtag": "73865d48892d5d3b980ceb30eeb0c69f"
        },
        "competitorpickupgridview": {
            "title": "竞争对手选择表格视图",
            "caption": "竞争对手",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "Sales",
            "viewname": "CompetitorPickupGridView",
            "viewtag": "73f0d5fb33479d51bdf3153b5f7a5b3f"
        },
        "invoicegridview": {
            "title": "发票信息",
            "caption": "发票",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Finance",
            "viewname": "InvoiceGridView",
            "viewtag": "73fb289c1d8a3e1834b495dda2188b8e"
        },
        "incidentcustomerindexpickupview": {
            "title": "案例客户数据选择视图",
            "caption": "案例客户",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "Service",
            "viewname": "IncidentCustomerIndexPickupView",
            "viewtag": "7448a9015461dcac4c232913f60d28d3"
        },
        "sysuserpickupview": {
            "title": "用户表数据选择视图",
            "caption": "系统用户",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "uaa",
            "viewname": "SysUserPickupView",
            "viewtag": "753b710282b0332c2d6418a1d6c7594e"
        },
        "quotesummary": {
            "title": "报价单数据看板视图",
            "caption": "报价单",
            "viewtype": "DEPORTALVIEW",
            "viewmodule": "Sales",
            "viewname": "QuoteSummary",
            "viewtag": "760898155bde111279981be640053efb"
        },
        "incidenteditview": {
            "title": "案例编辑视图",
            "caption": "案例",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Service",
            "viewname": "IncidentEditView",
            "viewtag": "785c7067ed5cfa73443e2ba6ece9f92d"
        },
        "incidentcustomerpickupview": {
            "title": "客户",
            "caption": "客户",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "Service",
            "viewname": "IncidentCustomerPickupView",
            "viewtag": "78831ef897ed4ce7d38788ded1b2c536"
        },
        "ibizlistsummary": {
            "title": "市场营销列表概览",
            "caption": "市场营销列表概览",
            "viewtype": "DEPORTALVIEW",
            "viewmodule": "Marketing",
            "viewname": "IBizListSummary",
            "viewtag": "78abe6f7e960e0e5d5df0b18b319c241"
        },
        "accountinnerpickupview": {
            "title": "客户数据选择视图",
            "caption": "客户",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "Base",
            "viewname": "AccountInnerPickupView",
            "viewtag": "7a0f54535d759023c89093ed23719dba"
        },
        "operationunitbumastergridview": {
            "title": "运营单位表格视图",
            "caption": "业务单位",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Base",
            "viewname": "OperationUnitBUMasterGridView",
            "viewtag": "7a9ed6cd5d637be649d2d7d57760d3bc"
        },
        "incidentedit_datapanelview": {
            "title": "头部信息编辑",
            "caption": "案例",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Service",
            "viewname": "IncidentEdit_DataPanelView",
            "viewtag": "7b74f6733aeff35c8bc965af197ea90d"
        },
        "sysappgridview": {
            "title": "接入应用",
            "caption": "接入应用",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "uaa",
            "viewname": "SysAppGridView",
            "viewtag": "7bc8357199426c5495de4695e0761b4d"
        },
        "competitorsummary": {
            "title": "竞争对手概览",
            "caption": "竞争对手概览",
            "viewtype": "DEPORTALVIEW",
            "viewmodule": "Sales",
            "viewname": "CompetitorSummary",
            "viewtag": "7be3b24d82f203e004d99ec9ed57e7bd"
        },
        "goalinfoview": {
            "title": "目标信息",
            "caption": "目标",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "Sales",
            "viewname": "GoalInfoView",
            "viewtag": "7cb9d17716b8adc5677efc79bf190953"
        },
        "pricelevelpickupgridview": {
            "title": "价目表选择表格视图",
            "caption": "价目表",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "Product",
            "viewname": "PriceLevelPickupGridView",
            "viewtag": "7d1ebe0e88a3c5564455651bd2ed8703"
        },
        "incidentquickcreatebyparentkey": {
            "title": "快速新建",
            "caption": "案例",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "Service",
            "viewname": "IncidentQuickCreateByParentKey",
            "viewtag": "7d8f0a9b8218be48ad238f993d9ecff5"
        },
        "connectionbyparentkey": {
            "title": "连接表格视图",
            "caption": "连接",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Runtime",
            "viewname": "ConnectionByParentKey",
            "viewtag": "7da18b8debf5bc6657088639a7b46503"
        },
        "operationunitdeptmastergridview": {
            "title": "运营单位表格视图",
            "caption": "部门",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Base",
            "viewname": "OperationUnitDeptMasterGridView",
            "viewtag": "7db776ba8dd9c3f0611ba4c6f2c6785b"
        },
        "producteffectivegridview": {
            "title": "产品信息",
            "caption": "产品",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Product",
            "viewname": "ProductEffectiveGridView",
            "viewtag": "7e25f832872dc1609b06ff59315384a7"
        },
        "languagelocalepickupview": {
            "title": "语言数据选择视图",
            "caption": "语言",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "Base",
            "viewname": "LanguageLocalePickupView",
            "viewtag": "7e7edefa5b3a142319f4c0fdc1056325"
        },
        "knowledgearticlegridview": {
            "title": "知识文章信息",
            "caption": "知识文章",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Base",
            "viewname": "KnowledgeArticleGridView",
            "viewtag": "7ead762dc581bea49e06845e17110984"
        },
        "metricpickupview": {
            "title": "目标度量数据选择视图",
            "caption": "目标度量",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "Base",
            "viewname": "MetricPickupView",
            "viewtag": "7ff410fd38e3eaac8b9b1ce504dac1a0"
        },
        "operationunitmastersummaryview": {
            "title": "主信息概览看板视图",
            "caption": "运营单位",
            "viewtype": "DEPORTALVIEW",
            "viewmodule": "Base",
            "viewname": "OperationUnitMasterSummaryView",
            "viewtag": "801dbfbf72c0f0c477887a845d0ff3f3"
        },
        "incidentgridview": {
            "title": "服务案例信息",
            "caption": "案例",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Service",
            "viewname": "IncidentGridView",
            "viewtag": "80cfcdaa17fe9f2b5b812ffd1237b9a8"
        },
        "transactioncurrencypickupgridview": {
            "title": "货币选择表格视图",
            "caption": "货币",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "Base",
            "viewname": "TransactionCurrencyPickupGridView",
            "viewtag": "80f6269047dcded88d0fb2022c268dd1"
        },
        "websitecontenteditview": {
            "title": "站点内容",
            "caption": "站点内容",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "WebSite",
            "viewname": "WebSiteContentEditView",
            "viewtag": "8166d726d899515777e53937e2cae391"
        },
        "wfuserpickupgridview": {
            "title": "用户选择表格视图",
            "caption": "用户",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "workflow",
            "viewname": "WFUserPickupGridView",
            "viewtag": "81e8217f619a6a2c51af343df20b1608"
        },
        "incidentinfoview": {
            "title": "服务案例信息",
            "caption": "案例",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "Service",
            "viewname": "IncidentInfoView",
            "viewtag": "8284ceb452b00047fe2b937e695a8f32"
        },
        "quotebyparentkey": {
            "title": "报价单信息",
            "caption": "报价单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Sales",
            "viewname": "QuoteByParentKey",
            "viewtag": "83d2f4f25fc3c0ea137b223b15f7aace"
        },
        "leadcompetitorcompetitorgridview": {
            "title": "竞争对手",
            "caption": "潜在客户对手",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Sales",
            "viewname": "LeadCompetitorCompetitorGridView",
            "viewtag": "84221901297fb7fc950d0f282b70961b"
        },
        "wfuserpickupview": {
            "title": "用户数据选择视图",
            "caption": "用户",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "workflow",
            "viewname": "WFUserPickupView",
            "viewtag": "8462425aecb08651249f9bfd31f96736"
        },
        "operationunitgridview": {
            "title": "运营单位表格视图",
            "caption": "运营单位",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Base",
            "viewname": "OperationUnitGridView",
            "viewtag": "859c62cd61b27425953a0a282480dc2c"
        },
        "knowledgearticleincidenteditview": {
            "title": "知识文章事件编辑视图",
            "caption": "知识文章事件",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Service",
            "viewname": "KnowledgeArticleIncidentEditView",
            "viewtag": "862f016be033da5efea0260dbf6ecc2b"
        },
        "legalgridview": {
            "title": "法人表格视图",
            "caption": "法人",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Base",
            "viewname": "LegalGridView",
            "viewtag": "86daa474c6a4cf4d6ba0eaefe05bbee9"
        },
        "sysusereditview": {
            "title": "用户表编辑视图",
            "caption": "系统用户",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "uaa",
            "viewname": "SysUserEditView",
            "viewtag": "87cbf85602c01523635225d20011a327"
        },
        "transactioncurrencyeditview": {
            "title": "货币",
            "caption": "货币",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Base",
            "viewname": "TransactionCurrencyEditView",
            "viewtag": "883ae72055f697273aa1687c715e1c6b"
        },
        "accountinfo_account": {
            "title": "客户编辑视图",
            "caption": "客户",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Base",
            "viewname": "AccountInfo_Account",
            "viewtag": "884d358ca46b33060593f85263bc74eb"
        },
        "opportunitywingridview": {
            "title": "商机信息",
            "caption": "商机信息",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Sales",
            "viewname": "OpportunityWinGridView",
            "viewtag": "896eea92ff5cd143ab140aa92e164d1c"
        },
        "contactinfo_address": {
            "title": "联系人编辑视图",
            "caption": "联系人",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Base",
            "viewname": "ContactInfo_Address",
            "viewtag": "899e159cc56eb4d2170f74e95d5c9c15"
        },
        "transactioncurrencygridview": {
            "title": "货币",
            "caption": "货币",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Base",
            "viewname": "TransactionCurrencyGridView",
            "viewtag": "8b2cf81338df8f3599dd11beaaa472f3"
        },
        "leadpickupgridview": {
            "title": "潜在顾客选择表格视图",
            "caption": "潜在顾客",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "Sales",
            "viewname": "LeadPickupGridView",
            "viewtag": "8b3cb324ab9773fd3d247af2a5a71d5b"
        },
        "connectionrolepickupview": {
            "title": "连接角色数据选择视图",
            "caption": "连接角色",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "Runtime",
            "viewname": "ConnectionRolePickupView",
            "viewtag": "8c76486988140b7c6a9f958785991366"
        },
        "sysrolepermissioncustomview": {
            "title": "角色权限关系自定义视图",
            "caption": "角色权限关系",
            "viewtype": "DECUSTOMVIEW",
            "viewmodule": "uaa",
            "viewname": "SysRolePermissionCustomView",
            "viewtag": "8cce851a326fc95dde55ad602a9240f1"
        },
        "leadinfo_detail": {
            "title": "潜在顾客编辑视图",
            "caption": "潜在顾客",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Sales",
            "viewname": "LeadInfo_Detail",
            "viewtag": "8dd4865f605e93ebf1b47e536d920235"
        },
        "salesliteraturesummaryview": {
            "title": "销售宣传资料数据看板视图",
            "caption": "销售宣传资料",
            "viewtype": "DEPORTALVIEW",
            "viewmodule": "Sales",
            "viewname": "SalesLiteratureSummaryView",
            "viewtag": "8e580d9d78c961242281b0a4b8ce5441"
        },
        "operationunitmasterquickview": {
            "title": "快速新建视图",
            "caption": "运营单位",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "Base",
            "viewname": "OperationUnitMasterQuickView",
            "viewtag": "8ec23e3aba218304418e8b694b402b13"
        },
        "campaignresponsebyparentkey": {
            "title": "市场活动响应表格视图",
            "caption": "市场活动响应",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Marketing",
            "viewname": "CampaignResponseByParentKey",
            "viewtag": "8f07b0f4ca840e455585b37452898414"
        },
        "opportunitycompetitoreditview": {
            "title": "商机对手编辑视图",
            "caption": "商机对手",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Sales",
            "viewname": "OpportunityCompetitorEditView",
            "viewtag": "8f248b9ff13e0c23b2efd9d5704d8441"
        },
        "productassociationeditview": {
            "title": "产品关联编辑视图",
            "caption": "产品关联",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Product",
            "viewname": "ProductAssociationEditView",
            "viewtag": "8fbbacd8f05f2542af977531b672e1b9"
        },
        "ibizlisteditview": {
            "title": "市场营销列表编辑视图",
            "caption": "市场营销列表",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Marketing",
            "viewname": "IBizListEditView",
            "viewtag": "90220aca38f56491553c7d90ce241bed"
        },
        "phonecalleditview": {
            "title": "电话联络编辑视图",
            "caption": "电话联络",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Base",
            "viewname": "PhoneCallEditView",
            "viewtag": "9141173eaa3d820d42f247a3d1bb0c25"
        },
        "transactioncurrencypickupview": {
            "title": "货币数据选择视图",
            "caption": "货币",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "Base",
            "viewname": "TransactionCurrencyPickupView",
            "viewtag": "914c752552fd5f1ff89ad69961b04c29"
        },
        "leadeditview": {
            "title": "潜在顾客编辑",
            "caption": "潜在顾客",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Sales",
            "viewname": "LeadEditView",
            "viewtag": "92385fcd4d29e1e9390b5c70dc7b71df"
        },
        "campaignpickupgridview": {
            "title": "市场活动选择表格视图",
            "caption": "市场活动",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "Marketing",
            "viewname": "CampaignPickupGridView",
            "viewtag": "93eb7626af96942bf7415e802b0cc1bd"
        },
        "legalmastertabinfoview": {
            "title": "主信息总览",
            "caption": "法人信息",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "Base",
            "viewname": "LegalMasterTabInfoView",
            "viewtag": "950a978df8865658d89e6c1b458757a9"
        },
        "ibzdepartmenttreeexpview": {
            "title": "人员管理",
            "caption": "人员管理",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "ou",
            "viewname": "IBZDepartmentTreeExpView",
            "viewtag": "9580f6a22ab4c3c0c4b9424833b79279"
        },
        "competitorinfo": {
            "title": "竞争对手信息",
            "caption": "竞争对手",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "Sales",
            "viewname": "CompetitorInfo",
            "viewtag": "95b1e26c826afb404f696e942e1dcebb"
        },
        "quotewingridview": {
            "title": "报价单信息",
            "caption": "报价单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Sales",
            "viewname": "QuoteWinGridView",
            "viewtag": "95d5f86c18aeb2f110ba7dd58ea89773"
        },
        "wfmembergridview": {
            "title": "流程角色",
            "caption": "流程角色",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "workflow",
            "viewname": "WFMemberGridView",
            "viewtag": "964145d76fd1f7806e8c06091892d5a1"
        },
        "taskeditview": {
            "title": "任务编辑视图",
            "caption": "任务",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Base",
            "viewname": "TaskEditView",
            "viewtag": "9744cf113899c865f5497c98855be97c"
        },
        "quotegridview": {
            "title": "报价单信息",
            "caption": "报价单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Sales",
            "viewname": "QuoteGridView",
            "viewtag": "97718d2d31e1706ad487e76b20885cbd"
        },
        "appointmenteditview": {
            "title": "约会编辑视图",
            "caption": "约会",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Base",
            "viewname": "AppointmentEditView",
            "viewtag": "98219aed6a6a72948a8184509e900c71"
        },
        "opportunitycompetitoredit_compoppview": {
            "title": "竞争对手商机信息",
            "caption": "商机对手",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Sales",
            "viewname": "OpportunityCompetitorEdit_CompOppView",
            "viewtag": "98795651be3cc1680f9f7583985850e3"
        },
        "campaignsummary": {
            "title": "市场活动概览",
            "caption": "市场活动概览",
            "viewtype": "DEPORTALVIEW",
            "viewmodule": "Marketing",
            "viewname": "CampaignSummary",
            "viewtag": "987a8b65a0290ad4c365bf0fd0244a26"
        },
        "incidentquickcreateview": {
            "title": "快速新建",
            "caption": "案例",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "Service",
            "viewname": "IncidentQuickCreateView",
            "viewtag": "98fd72a729c8001a55ace939f26f5ea3"
        },
        "sysusermpickupview": {
            "title": "用户表数据多项选择视图",
            "caption": "系统用户",
            "viewtype": "DEMPICKUPVIEW",
            "viewmodule": "uaa",
            "viewname": "SysUserMPickupView",
            "viewtag": "9a0975436d05e107a461c722c2332f60"
        },
        "incidentsummaryview": {
            "title": "案例数据看板视图",
            "caption": "案例",
            "viewtype": "DEPORTALVIEW",
            "viewmodule": "Service",
            "viewname": "IncidentSummaryView",
            "viewtag": "9a21ee1d10cdcd6c1b3823fd8fbcf669"
        },
        "campaignactivitybyparentkey": {
            "title": "市场活动项目表格视图",
            "caption": "市场活动项目",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Marketing",
            "viewname": "CampaignActivityByParentKey",
            "viewtag": "9a9189c4569b068b28c53032cc132e69"
        },
        "salesordersummaryview": {
            "title": "订单数据看板视图",
            "caption": "订单",
            "viewtype": "DEPORTALVIEW",
            "viewmodule": "Sales",
            "viewname": "SalesOrderSummaryView",
            "viewtag": "9a96c108dcc1998611eca28f1232990b"
        },
        "accountpickupgridview": {
            "title": "客户选择表格视图",
            "caption": "客户",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "Base",
            "viewname": "AccountPickupGridView",
            "viewtag": "9aabc81b70729c7b7ddb779020ef4331"
        },
        "salesordereditview": {
            "title": "订单编辑视图",
            "caption": "订单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Sales",
            "viewname": "SalesOrderEditView",
            "viewtag": "9ab28f546d5014097009f204959f4b87"
        },
        "pricelevelgridview": {
            "title": "价目表",
            "caption": "价目表",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Product",
            "viewname": "PriceLevelGridView",
            "viewtag": "9b07342706ae42f0d454302b30c620c6"
        },
        "contactbyaccount": {
            "title": "联系人列表",
            "caption": "联系人列表",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Base",
            "viewname": "ContactByAccount",
            "viewtag": "9bcb503839e8bd2afffeecbd069f40db"
        },
        "contactsummaryview": {
            "title": "联系人数据看板视图",
            "caption": "联系人",
            "viewtype": "DEPORTALVIEW",
            "viewmodule": "Base",
            "viewname": "ContactSummaryView",
            "viewtag": "9c1aae1a37869a46a0c88985d0352e4a"
        },
        "wfprocessdefinitioneditview": {
            "title": "流程定义",
            "caption": "流程定义",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "workflow",
            "viewname": "WFProcessDefinitionEditView",
            "viewtag": "9c550a85172ead86252f54b47b63d9fb"
        },
        "contactquickcreate": {
            "title": "快速新建",
            "caption": "联系人",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "Base",
            "viewname": "ContactQuickCreate",
            "viewtag": "9d13a2e1a31e69f346a4bf6fbb5935a2"
        },
        "quoteinfoabstract": {
            "title": "报价单编辑视图",
            "caption": "报价单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Sales",
            "viewname": "QuoteInfoAbstract",
            "viewtag": "9de45cde3d3f449f7772aa5370ebd01c"
        },
        "contactgridview": {
            "title": "联系人信息",
            "caption": "联系人信息",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Base",
            "viewname": "ContactGridView",
            "viewtag": "9dfbbb3bf0cb552b92fbb5391f29e430"
        },
        "quoteeditview": {
            "title": "报价单编辑视图",
            "caption": "报价单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Sales",
            "viewname": "QuoteEditView",
            "viewtag": "9eff78cc3008539f70799191cd2e0707"
        },
        "opportunityquickcreate": {
            "title": "快速新建",
            "caption": "快速新建",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "Sales",
            "viewname": "OpportunityQuickCreate",
            "viewtag": "9f31150bb0dcd2deef5be4cdf977dfc8"
        },
        "dictcatalogpickupgridview": {
            "title": "字典选择表格视图",
            "caption": "字典",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "dict",
            "viewname": "DictCatalogPickupGridView",
            "viewtag": "9f49c801cca2979e4e37231215137c48"
        },
        "omhierarchycatgridview": {
            "title": "实体表格视图",
            "caption": "层次类别",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Base",
            "viewname": "OMHierarchyCatGridView",
            "viewtag": "9fadb65fcfa1561655e2776135b75dc3"
        },
        "campaignactivityquickcreate": {
            "title": "快速新建",
            "caption": "快速新建",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "Marketing",
            "viewname": "CampaignActivityQuickCreate",
            "viewtag": "a12f1de520040f893da6aedd43eb4961"
        },
        "dictoptiongrideditview": {
            "title": "字典项",
            "caption": "字典项",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "dict",
            "viewname": "DictOptionGridEditView",
            "viewtag": "a1766a5da11cc8eb6f2502a0afb790de"
        },
        "operationunitmasterinfoview": {
            "title": "主信息概览视图",
            "caption": "运营单位",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Base",
            "viewname": "OperationUnitMasterInfoView",
            "viewtag": "a20eacba1e00028e490c46d7a4c67b18"
        },
        "incidentcustomergridview": {
            "title": "案例客户表格视图",
            "caption": "案例客户",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Service",
            "viewname": "IncidentCustomerGridView",
            "viewtag": "a2e7feb26f8f473c5ebbcb0b223d597f"
        },
        "knowledgearticlesummaryview": {
            "title": "知识文章数据看板视图",
            "caption": "知识文章",
            "viewtype": "DEPORTALVIEW",
            "viewmodule": "Base",
            "viewname": "KnowledgeArticleSummaryView",
            "viewtag": "a2f266aef39a0417b5565e9943689f46"
        },
        "accountinfo_majorcontact": {
            "title": "客户编辑视图",
            "caption": "客户",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Base",
            "viewname": "AccountInfo_MajorContact",
            "viewtag": "a30da9042a49e77902c43522adb89652"
        },
        "syspermissionmpickupview": {
            "title": "权限表数据多项选择视图",
            "caption": "权限/资源",
            "viewtype": "DEMPICKUPVIEW",
            "viewmodule": "uaa",
            "viewname": "SysPermissionMPickupView",
            "viewtag": "a35e5430216e8f5524548b2d79cb4c07"
        },
        "sysroleeditview": {
            "title": "用户角色",
            "caption": "用户角色",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "uaa",
            "viewname": "SysRoleEditView",
            "viewtag": "a3d201750c3215952b2704fc3ccd86bd"
        },
        "serviceappointmenteditview": {
            "title": "服务活动编辑视图",
            "caption": "服务活动",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Service",
            "viewname": "ServiceAppointmentEditView",
            "viewtag": "a3d6a5f99ba1285aeb473598cbfa2e05"
        },
        "dictcataloggridview": {
            "title": "目录",
            "caption": "目录",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "dict",
            "viewname": "DictCatalogGridView",
            "viewtag": "a424d59a6a6ce16993ba22d88adde4ea"
        },
        "salesordercancelgridview": {
            "title": "订单信息",
            "caption": "订单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Sales",
            "viewname": "SalesOrderCancelGridView",
            "viewtag": "a4378b862073216bf027011b98f5b81e"
        },
        "leadqualification": {
            "title": "授予资格",
            "caption": "授予资格",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "Sales",
            "viewname": "LeadQualification",
            "viewtag": "a51e70f47a9d85290602ad0c81e6b243"
        },
        "salesorderdetailsoproductgridview": {
            "title": "订单产品表格视图",
            "caption": "订单产品",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Sales",
            "viewname": "SalesOrderDetailSOProductGridView",
            "viewtag": "a5bc4a4fb1d8b41e534e96f5ea4c4f8f"
        },
        "accountedit_introduction": {
            "title": "简介信息",
            "caption": "简介信息",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "Base",
            "viewname": "AccountEdit_Introduction",
            "viewtag": "a6b4637e07666c90c8045ff9fb92f796"
        },
        "campaignpickupview": {
            "title": "市场活动数据选择视图",
            "caption": "市场活动",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "Marketing",
            "viewname": "CampaignPickupView",
            "viewtag": "a6ee1605291de8241062867eff2007dd"
        },
        "jobsregistrygridview": {
            "title": "注册",
            "caption": "注册",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "task",
            "viewname": "JobsRegistryGridView",
            "viewtag": "a7c2d930f5083821b7e19112a7a9252a"
        },
        "metricpickupgridview": {
            "title": "目标度量选择表格视图",
            "caption": "目标度量",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "Base",
            "viewname": "MetricPickupGridView",
            "viewtag": "a7e6b6721c505c18c29e33c83bcfbc4b"
        },
        "connectioneditview": {
            "title": "连接编辑视图",
            "caption": "连接",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Runtime",
            "viewname": "ConnectionEditView",
            "viewtag": "a7f57c8d7e0a35efebfcc4499e7072a7"
        },
        "ibzorganizationeditview": {
            "title": "单位管理",
            "caption": "单位管理",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "ou",
            "viewname": "IBZOrganizationEditView",
            "viewtag": "a961bd7f17e79c0d2dd6c180f768c5d8"
        },
        "invoiceinfo_invoiceview": {
            "title": "发票编辑视图",
            "caption": "发票",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Finance",
            "viewname": "InvoiceInfo_InvoiceView",
            "viewtag": "a9c176524018ca1aac5e64ef5a112255"
        },
        "contactedit_datapanelview": {
            "title": "头部信息编辑",
            "caption": "联系人",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Base",
            "viewname": "ContactEdit_DataPanelView",
            "viewtag": "ab2d0b70b61d6dd10b5336a536395d45"
        },
        "opportunitycompetitorcompoppgridview": {
            "title": "竞争对手商机信息",
            "caption": "商机对手",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Sales",
            "viewname": "OpportunityCompetitorCompOppGridView",
            "viewtag": "abb4dff8ba145165bb26c0ecb0dca7e8"
        },
        "connectionroleeditview": {
            "title": "连接角色",
            "caption": "连接角色",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Runtime",
            "viewname": "ConnectionRoleEditView",
            "viewtag": "acc291bb6f979f3429083c966bcd7d0e"
        },
        "quotedetailquodetailgridview": {
            "title": "报价单产品表格视图",
            "caption": "报价单产品",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Sales",
            "viewname": "QuoteDetailQuoDetailGridView",
            "viewtag": "ad2480f720b43666d5106e38707e2c68"
        },
        "incidentcustomereditview": {
            "title": "案例客户编辑视图",
            "caption": "案例客户",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Service",
            "viewname": "IncidentCustomerEditView",
            "viewtag": "ad8019022547911e1cea6866c52a5bb9"
        },
        "productpricelevelproprilvgridview": {
            "title": "产品价目表项",
            "caption": "价目表项",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Product",
            "viewname": "ProductPriceLevelProPrilvGridView",
            "viewtag": "ae52da5470617faa656daae330b9f0be"
        },
        "salesliteratureitemgridview": {
            "title": "销售附件表格视图",
            "caption": "销售附件",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Sales",
            "viewname": "SalesLiteratureItemGridView",
            "viewtag": "afa74002abbf85fbf5c693adcb913cdb"
        },
        "taskquickcreate": {
            "title": "快速新建：任务",
            "caption": "快速新建：任务",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "Base",
            "viewname": "TaskQuickCreate",
            "viewtag": "afe6f4861160d89b7fe7364834981597"
        },
        "omhierarchyeditview": {
            "title": "组织层次结构编辑视图",
            "caption": "组织层次结构",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Base",
            "viewname": "OMHierarchyEditView",
            "viewtag": "b0ffded44497dae0560de25cb28bfdd9"
        },
        "uompickupgridview": {
            "title": "计价单位选择表格视图",
            "caption": "计价单位",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "Base",
            "viewname": "UomPickupGridView",
            "viewtag": "b12ba9db37cce82d2d8f9e8149cc371a"
        },
        "contacteditview": {
            "title": "联系人编辑视图",
            "caption": "联系人",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Base",
            "viewname": "ContactEditView",
            "viewtag": "b13070d8fdb6fc7b2c46da8e610e429c"
        },
        "ibizlistinfo_abstract": {
            "title": "市场营销列表编辑视图",
            "caption": "市场营销列表",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Marketing",
            "viewname": "IBizListInfo_Abstract",
            "viewtag": "b16d4453e74dc9e530b8d46fcf0f7814"
        },
        "syspermissionpickupgridview": {
            "title": "权限表选择表格视图",
            "caption": "权限/资源",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "uaa",
            "viewname": "SysPermissionPickupGridView",
            "viewtag": "b16f20a6fcbcdf55678b2fae73069970"
        },
        "campaigninfo": {
            "title": "市场活动信息",
            "caption": "市场活动",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "Marketing",
            "viewname": "CampaignInfo",
            "viewtag": "b1a18fd12ba31ad35151d71738cb7668"
        },
        "contactinfo": {
            "title": "联系人信息",
            "caption": "联系人",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "Base",
            "viewname": "ContactInfo",
            "viewtag": "b25520cf5b51b60a1d66ec4190713529"
        },
        "leadinfo_company": {
            "title": "潜在顾客编辑视图",
            "caption": "潜在顾客",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Sales",
            "viewname": "LeadInfo_Company",
            "viewtag": "b30d19fb7a3b4cefb010d9fc4e3219f1"
        },
        "salesorderedit_datapanelview": {
            "title": "头部信息编辑",
            "caption": "订单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Sales",
            "viewname": "SalesOrderEdit_DataPanelView",
            "viewtag": "b32bb4d2c91988b508d51776c83feb0c"
        },
        "campaigngridview": {
            "title": "市场活动信息",
            "caption": "市场活动",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Marketing",
            "viewname": "CampaignGridView",
            "viewtag": "b3338bba1cf4e383e888b19b93d67224"
        },
        "jobsregistryeditview": {
            "title": "注册",
            "caption": "注册",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "task",
            "viewname": "JobsRegistryEditView",
            "viewtag": "b34039457f71c20564030bac3e06e42b"
        },
        "goalclosegridview": {
            "title": "目标信息",
            "caption": "目标",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Sales",
            "viewname": "GoalCloseGridView",
            "viewtag": "b3cb55aaa9b3a5bb5ed4969749fa7fa4"
        },
        "goalgridview": {
            "title": "目标信息",
            "caption": "目标",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Sales",
            "viewname": "GoalGridView",
            "viewtag": "b49556d9ccff6363129d172cee514c0b"
        },
        "omhierarchycatv_002": {
            "title": "V_002",
            "caption": "结构层次类别",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Base",
            "viewname": "OMHierarchyCatV_002",
            "viewtag": "b7b5d2474f0e51c5d378b5b8e7da9153"
        },
        "leadpickupview": {
            "title": "潜在顾客数据选择视图",
            "caption": "潜在顾客",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "Sales",
            "viewname": "LeadPickupView",
            "viewtag": "b8a91b5b96f5a36e92b29dfa71e55403"
        },
        "accountinnerpickupgridview": {
            "title": "客户选择表格视图",
            "caption": "客户",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "Base",
            "viewname": "AccountInnerPickupGridView",
            "viewtag": "b92125fdd043107fccf79ece271192db"
        },
        "wfremodeleditview": {
            "title": "发布新流程",
            "caption": "发布新流程",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "workflow",
            "viewname": "WFREModelEditView",
            "viewtag": "b9f0a7aa5e489abf5b64c3a6c56b6ec6"
        },
        "discounttypepickupgridview": {
            "title": "折扣表选择表格视图",
            "caption": "折扣表",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "Sales",
            "viewname": "DiscountTypePickupGridView",
            "viewtag": "b9f7052ccfa49f637ea5fa990e1e56f0"
        },
        "operationunitdeptmasterquickview": {
            "title": "部门快速新建视图",
            "caption": "快速新建",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "Base",
            "viewname": "OperationUnitDeptMasterQuickView",
            "viewtag": "ba058a45bb360b241a8015800951cb80"
        },
        "accountinfo_address": {
            "title": "客户编辑视图",
            "caption": "客户",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Base",
            "viewname": "AccountInfo_Address",
            "viewtag": "baccbae9cfa5ab976d4dae2f25084676"
        },
        "campaignedit_datapanelview": {
            "title": "头部信息编辑",
            "caption": "市场活动",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Marketing",
            "viewname": "CampaignEdit_DataPanelView",
            "viewtag": "bb7135b3c581176bd456a91db582b8ff"
        },
        "ibizlistoptionview": {
            "title": "市场营销列表选项操作视图",
            "caption": "摘要信息",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "Marketing",
            "viewname": "IBizListOptionView",
            "viewtag": "bc4a36fb63b9ac07a68274a059867ae2"
        },
        "ibizlistquickcreate": {
            "title": "快速新建",
            "caption": "快速新建",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "Marketing",
            "viewname": "IBizListQuickCreate",
            "viewtag": "bce31e180662bbaaaec7812607b2f4a3"
        },
        "dictcatalogpickupview": {
            "title": "字典数据选择视图",
            "caption": "字典",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "dict",
            "viewname": "DictCatalogPickupView",
            "viewtag": "bd6f149583c60a4f5f5f671334a71959"
        },
        "salesliteratureitemeditview": {
            "title": "销售附件编辑视图",
            "caption": "销售附件",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Sales",
            "viewname": "SalesLiteratureItemEditView",
            "viewtag": "bd70ebd41f9be24e19cf7f7f5fc72c4d"
        },
        "competitorinfo_abstract": {
            "title": "竞争对手编辑视图",
            "caption": "竞争对手",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Sales",
            "viewname": "CompetitorInfo_Abstract",
            "viewtag": "be78ca037813e48a849b10b91e72f406"
        },
        "productsubstitutegridview": {
            "title": "产品关系表格视图",
            "caption": "产品替换",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Product",
            "viewname": "ProductSubstituteGridView",
            "viewtag": "bf18bd589c8fd002b821ec3108473130"
        },
        "invoicequickcreateview": {
            "title": "快速新建",
            "caption": "发票",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "Finance",
            "viewname": "InvoiceQuickCreateView",
            "viewtag": "bfeeef2051040fe1fc981e5547d51245"
        },
        "competitorsalesliteraturesallitcompeditview": {
            "title": "宣传资料竞争对手明细",
            "caption": "竞争对手宣传资料",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Sales",
            "viewname": "CompetitorSalesLiteratureSalLitCompEditView",
            "viewtag": "c11790aaeac83fc19f33882f278f6064"
        },
        "pricelevelpickupview": {
            "title": "价目表数据选择视图",
            "caption": "价目表",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "Product",
            "viewname": "PriceLevelPickupView",
            "viewtag": "c2915614947c18118d7d66a67ec6a35d"
        },
        "jobslogeditview": {
            "title": "日志",
            "caption": "日志",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "task",
            "viewname": "JobsLogEditView",
            "viewtag": "c2a3242cd8f69982d759edcf776aabcc"
        },
        "multipickdataac": {
            "title": "负责人",
            "caption": "负责人",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "Base",
            "viewname": "MultiPickDataAC",
            "viewtag": "c4beabed09dcf58537c6a3394b50899f"
        },
        "salesorderfinishgridview": {
            "title": "订单信息",
            "caption": "订单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Sales",
            "viewname": "SalesOrderFinishGridView",
            "viewtag": "c54d850ea22c14b1896e9786be61f8e1"
        },
        "sysuserroleeditview": {
            "title": "用户角色",
            "caption": "用户角色",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "uaa",
            "viewname": "SysUserRoleEditView",
            "viewtag": "c579940b00707a82fe64440e59e76ed9"
        },
        "campaignpanelview": {
            "title": "市场活动面板视图",
            "caption": "市场活动",
            "viewtype": "DEPANELVIEW",
            "viewmodule": "Marketing",
            "viewname": "CampaignPanelView",
            "viewtag": "c8f0d07b5a47bae7c08dd1a137f5f782"
        },
        "accountpickupview": {
            "title": "客户数据选择视图",
            "caption": "客户",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "Base",
            "viewname": "AccountPickupView",
            "viewtag": "c91da7a0df16b7741be67ec9ef80c303"
        },
        "accountinfo": {
            "title": "客户信息",
            "caption": "客户信息",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "Base",
            "viewname": "AccountInfo",
            "viewtag": "c94372b520650b22489dc2e7d5850314"
        },
        "producteditview": {
            "title": "产品主信息编辑",
            "caption": "产品",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Product",
            "viewname": "ProductEditView",
            "viewtag": "c9adf7c8c2335b0ebac2cd5d00585633"
        },
        "listleadbylist": {
            "title": "营销列表-潜在客户（根据营销列表）",
            "caption": "潜在客户",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Marketing",
            "viewname": "ListLeadByList",
            "viewtag": "c9b0cc636f41e3e0c476857c2cd4cb5f"
        },
        "competitoroptionview": {
            "title": "竞争对手选项操作视图",
            "caption": "竞争对手",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "Sales",
            "viewname": "CompetitorOptionView",
            "viewtag": "c9bd931c59b8e88a5ad2f70f78f44257"
        },
        "listaccountbylist": {
            "title": "营销列表-账户表格视图",
            "caption": "营销列表-账户",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Marketing",
            "viewname": "ListAccountByList",
            "viewtag": "c9deb17577dcf6b6afc497b903385833"
        },
        "opportunityproductopp_oppprogridview": {
            "title": "商机产品表格视图",
            "caption": "商机产品",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Sales",
            "viewname": "OpportunityProductOpp_OppProGridView",
            "viewtag": "cace9709cd818418e3f8add5e8e49187"
        },
        "opportunityedit_datepanelview": {
            "title": "头部信息编辑",
            "caption": "商机",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Sales",
            "viewname": "OpportunityEdit_DatePanelView",
            "viewtag": "cb8322587a1abf703199bc34fefeeada"
        },
        "activitypointerredirectview": {
            "title": "活动数据重定向视图",
            "caption": "活动",
            "viewtype": "DEREDIRECTVIEW",
            "viewmodule": "Base",
            "viewname": "ActivityPointerRedirectView",
            "viewtag": "cbbd3c8d0c31669905963452e6e5e87c"
        },
        "opportunitylostgridview": {
            "title": "商机信息",
            "caption": "商机信息",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Sales",
            "viewname": "OpportunityLostGridView",
            "viewtag": "cc0ff88786a42026cb93179ec5e007cb"
        },
        "leadinfo": {
            "title": "潜在顾客",
            "caption": "潜在顾客",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "Sales",
            "viewname": "LeadInfo",
            "viewtag": "cc234b52507851c28839802b3845705a"
        },
        "contactpickupview": {
            "title": "联系人数据选择视图",
            "caption": "联系人",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "Base",
            "viewname": "ContactPickupView",
            "viewtag": "ccab6a9a19653a8367909c6357835eed"
        },
        "accountgridview": {
            "title": "客户表格视图",
            "caption": "客户信息",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Base",
            "viewname": "AccountGridView",
            "viewtag": "cd237e54b6ab1a1a44019300f7bacb00"
        },
        "listcontacteditview": {
            "title": "营销列表-联系人编辑视图",
            "caption": "营销列表-联系人",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Marketing",
            "viewname": "ListContactEditView",
            "viewtag": "cd36a4d5e19fd5523b22ee0568ed7bd8"
        },
        "ibizliststopgridview": {
            "title": "市场营销列表信息",
            "caption": "市场营销列表",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Marketing",
            "viewname": "IBizListStopGridView",
            "viewtag": "cd76f9cedb3e93d9512118f705c72865"
        },
        "invoicecancelgridview": {
            "title": "发票信息",
            "caption": "发票",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Finance",
            "viewname": "InvoiceCancelGridView",
            "viewtag": "cdc9ad2d3e1057a3a72769ca54dafcca"
        },
        "sysauthloggridview": {
            "title": "认证日志",
            "caption": "认证日志",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "uaa",
            "viewname": "SysAuthLogGridView",
            "viewtag": "ceab28aad766cec541f1467a42263524"
        },
        "quotepickupgridview": {
            "title": "报价单选择表格视图",
            "caption": "报价单",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "Sales",
            "viewname": "QuotePickupGridView",
            "viewtag": "ced3e5307eebe08d575277fdfd2457c5"
        },
        "ibzpostpickupview": {
            "title": "岗位数据选择视图",
            "caption": "岗位",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "ou",
            "viewname": "IBZPostPickupView",
            "viewtag": "cf7af9f3892de5fd8c05e9d23bdbb77a"
        },
        "territoryeditview": {
            "title": "区域",
            "caption": "区域",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Base",
            "viewname": "TerritoryEditView",
            "viewtag": "cfd7af4473c46050a49d30cd8fb9a427"
        },
        "incidentresolvedgridview": {
            "title": "服务案例信息",
            "caption": "案例",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Service",
            "viewname": "IncidentResolvedGridView",
            "viewtag": "d08cd02b4b413693460c69046ea96611"
        },
        "ibzpostpickupgridview": {
            "title": "岗位选择表格视图",
            "caption": "岗位",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "ou",
            "viewname": "IBZPostPickupGridView",
            "viewtag": "d099f56cc52d60591db78b1ed53f4305"
        },
        "accountedit_address": {
            "title": "地址信息",
            "caption": "地址信息",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "Base",
            "viewname": "AccountEdit_Address",
            "viewtag": "d1105a139b2a370346443c05d9a6e4a7"
        },
        "ibizlistgridview": {
            "title": "市场营销列表信息",
            "caption": "市场营销列表",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Marketing",
            "viewname": "IBizListGridView",
            "viewtag": "d2fd0e7805036429edec105eb6d57b30"
        },
        "omhierarchytreeexpview": {
            "title": "组织层次结构树导航视图",
            "caption": "组织层级结构",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "Base",
            "viewname": "OMHierarchyTreeExpView",
            "viewtag": "d38dc7b197b7c56ad6342d3d0d2f5b5a"
        },
        "campaignlistgridview": {
            "title": "市场活动-营销列表表格视图",
            "caption": "市场活动-营销列表",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Marketing",
            "viewname": "CampaignListGridView",
            "viewtag": "d3e3572adf0be4f8f811370353f9b22a"
        },
        "quoteclosedgridview": {
            "title": "报价单信息",
            "caption": "报价单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Sales",
            "viewname": "QuoteClosedGridView",
            "viewtag": "d62e3e827c60c0c811eb60e5fc770c93"
        },
        "salesliteratureinfo_sallitview": {
            "title": "销售宣传资料编辑视图",
            "caption": "销售宣传资料",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Sales",
            "viewname": "SalesLiteratureInfo_SalLitView",
            "viewtag": "d63a29daa331deb67fe9069650b61aac"
        },
        "campaignactivityeditview": {
            "title": "市场活动项目编辑视图",
            "caption": "市场活动项目",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Marketing",
            "viewname": "CampaignActivityEditView",
            "viewtag": "d7224ceddd683d057a87737e01b0a928"
        },
        "goalquickcreateview": {
            "title": "快速新建",
            "caption": "目标",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "Sales",
            "viewname": "GoalQuickCreateView",
            "viewtag": "d7e5cc68f308f44c8f2dd1a7f51d3bdc"
        },
        "syspermissionpickupview": {
            "title": "权限表数据选择视图",
            "caption": "权限/资源",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "uaa",
            "viewname": "SysPermissionPickupView",
            "viewtag": "d9006107ae68d070380f333abf2c9ea9"
        },
        "incidentcustomermpickupview": {
            "title": "案例客户数据多项选择视图",
            "caption": "案例客户",
            "viewtype": "DEMPICKUPVIEW",
            "viewmodule": "Service",
            "viewname": "IncidentCustomerMPickupView",
            "viewtag": "da70cb6f06104242301260e8ac00d713"
        },
        "knowledgearticleeditview": {
            "title": "知识文章编辑视图",
            "caption": "知识文章",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Base",
            "viewname": "KnowledgeArticleEditView",
            "viewtag": "daee9bf74a5745fa981837ccb288b39b"
        },
        "campaigninfo_head": {
            "title": "头信息",
            "caption": "头信息",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Marketing",
            "viewname": "CampaignInfo_Head",
            "viewtag": "db4b69a3d7d05112c492cbda8e0d33ca"
        },
        "incidentcustomerindexpickupdataview": {
            "title": "案例客户索引关系选择数据视图",
            "caption": "案例客户",
            "viewtype": "DEINDEXPICKUPDATAVIEW",
            "viewmodule": "Service",
            "viewname": "IncidentCustomerIndexPickupDataView",
            "viewtag": "db5f33ae2a6ff271f3ea6777cda8af1a"
        },
        "operationunitbumasterquickview": {
            "title": "业务单位快速新建视图",
            "caption": "运营单位",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "Base",
            "viewname": "OperationUnitBUMasterQuickView",
            "viewtag": "dcd459db2fc3a870c46b32e97239103b"
        },
        "productrevisegridview": {
            "title": "产品信息",
            "caption": "产品",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Product",
            "viewname": "ProductReviseGridView",
            "viewtag": "ddeeb9ce9314d8e504db363f4d4eafce"
        },
        "territorygridview": {
            "title": "区域",
            "caption": "区域",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Base",
            "viewname": "TerritoryGridView",
            "viewtag": "de000285652adbea25bbe70e4cbe123c"
        },
        "competitorsalesliteraturesallitcompgridview": {
            "title": "宣传资料竞争对手明细",
            "caption": "竞争对手宣传资料",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Sales",
            "viewname": "CompetitorSalesLiteratureSalLitCompGridView",
            "viewtag": "dea20b2e052cd3e77e1efe0f87f76872"
        },
        "salesliteratureedit_datapanelview": {
            "title": "头部信息编辑",
            "caption": "销售宣传资料",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Sales",
            "viewname": "SalesLiteratureEdit_DataPanelView",
            "viewtag": "deaff04cc5731f73a124da5a1ce84d06"
        },
        "salesliteraturequickcreateview": {
            "title": "快速新建",
            "caption": "销售宣传资料",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "Sales",
            "viewname": "SalesLiteratureQuickCreateView",
            "viewtag": "defd2767cd83fe2280d92fa6f611c579"
        },
        "accountedit_datapanel": {
            "title": "客户编辑视图",
            "caption": "客户",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Base",
            "viewname": "AccountEdit_DataPanel",
            "viewtag": "df3ee86db1eb3a21c665d85c35182291"
        },
        "metriceditview": {
            "title": "目标度量信息",
            "caption": "目标度量",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Base",
            "viewname": "MetricEditView",
            "viewtag": "df9a8d4022ff2b34b43c436c23d70d83"
        },
        "salesliteratureinfoview": {
            "title": "销售宣传资料",
            "caption": "销售宣传资料",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "Sales",
            "viewname": "SalesLiteratureInfoView",
            "viewtag": "e00dcbe1285dfeca0ac85b2d6750d0b2"
        },
        "campaignstatetabview": {
            "title": "市场活动状态分页视图",
            "caption": "市场活动",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "Marketing",
            "viewname": "CampaignStateTabView",
            "viewtag": "e0b474e2c960a07f02f5a0714a683634"
        },
        "salesliteraturegridview": {
            "title": "销售宣传资料",
            "caption": "销售宣传资料",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Sales",
            "viewname": "SalesLiteratureGridView",
            "viewtag": "e0f972baa76f41e353673dbdbfb7ff05"
        },
        "productinfo_proinfo": {
            "title": "产品编辑视图",
            "caption": "产品",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Product",
            "viewname": "ProductInfo_ProInfo",
            "viewtag": "e1247dd5f0fe6c0d76dcd58087b59595"
        },
        "listcontactgridview": {
            "title": "营销列表-联系人表格视图",
            "caption": "营销列表-联系人",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Marketing",
            "viewname": "ListContactGridView",
            "viewtag": "e1a19e522f997448a6860c25f9ed3b96"
        },
        "opportunitystatetabview": {
            "title": "商机信息",
            "caption": "商机信息",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "Sales",
            "viewname": "OpportunityStateTabView",
            "viewtag": "e20fd7e712b02e804ecee443eb428ad5"
        },
        "invoicesummaryview": {
            "title": "发票数据看板视图",
            "caption": "发票",
            "viewtype": "DEPORTALVIEW",
            "viewmodule": "Finance",
            "viewname": "InvoiceSummaryView",
            "viewtag": "e2404c0e98672ee6aef616dc57bf8088"
        },
        "uomschedulegridview": {
            "title": "计价单位组",
            "caption": "计价单位组",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Base",
            "viewname": "UomScheduleGridView",
            "viewtag": "e2953ab66b8f66ebac06cb553cb61902"
        },
        "accountquickcreate": {
            "title": "快速新建",
            "caption": "快速新建",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "Base",
            "viewname": "AccountQuickCreate",
            "viewtag": "e30a2916b3054875db8d057cf4116df7"
        },
        "ibizlisteffectivegridview": {
            "title": "市场营销列表信息",
            "caption": "市场营销列表",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Marketing",
            "viewname": "IBizListEffectiveGridView",
            "viewtag": "e31fa6e266f1f2fb7e751a5042c6b9e2"
        },
        "opportunityeditview": {
            "title": "商机编辑视图",
            "caption": "商机",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Sales",
            "viewname": "OpportunityEditView",
            "viewtag": "e35a879cacbb9cd8d2d361defe2f1573"
        },
        "wfmembereditview": {
            "title": "流程角色",
            "caption": "流程角色",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "workflow",
            "viewname": "WFMemberEditView",
            "viewtag": "e35db257782a07c39aab9787203c66b7"
        },
        "listaccounteditview": {
            "title": "营销列表-账户编辑视图",
            "caption": "营销列表-账户",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Marketing",
            "viewname": "ListAccountEditView",
            "viewtag": "e3b23002e56520b7990b8bba98362560"
        },
        "quotedetaileditview": {
            "title": "报价单产品编辑视图",
            "caption": "报价单产品",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Sales",
            "viewname": "QuoteDetailEditView",
            "viewtag": "e427a31bdfcc5358bee392e308879eed"
        },
        "discounttypepickupview": {
            "title": "折扣表数据选择视图",
            "caption": "折扣表",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "Sales",
            "viewname": "DiscountTypePickupView",
            "viewtag": "e4da1aeb86d3b3de7fbca392bbbae64f"
        },
        "wfgrouppickupview": {
            "title": "角色/用户组数据选择视图",
            "caption": "角色/用户组",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "workflow",
            "viewname": "WFGroupPickupView",
            "viewtag": "e5f7c32a3dfcaa46be1d1719743825ea"
        },
        "productsummaryview": {
            "title": "产品数据看板视图",
            "caption": "产品",
            "viewtype": "DEPORTALVIEW",
            "viewmodule": "Product",
            "viewname": "ProductSummaryView",
            "viewtag": "ea2d6d51c76f2d7870f58523cbad8fcd"
        },
        "productquickcreateview": {
            "title": "快速新建",
            "caption": "产品",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "Product",
            "viewname": "ProductQuickCreateView",
            "viewtag": "ea47461bad1a2c9d028006bcbff636c1"
        },
        "opportunitypickupgridview": {
            "title": "商机选择表格视图",
            "caption": "商机",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "Sales",
            "viewname": "OpportunityPickupGridView",
            "viewtag": "ebb92926ff11cab247bab465302c8de1"
        },
        "ibzorganizationpickupgridview": {
            "title": "单位机构选择表格视图",
            "caption": "单位机构",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "ou",
            "viewname": "IBZOrganizationPickupGridView",
            "viewtag": "ec5b5212220d0036f639bf637ed38119"
        },
        "goalchildgoalgridview": {
            "title": "子目标表格视图",
            "caption": "目标",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Sales",
            "viewname": "GoalChildGoalGridView",
            "viewtag": "eccc3a1bf47c5af95a6c1d3cbb215f3e"
        },
        "quoteeffectivegridview": {
            "title": "报价单信息",
            "caption": "报价单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Sales",
            "viewname": "QuoteEffectiveGridView",
            "viewtag": "ecd1e4798e86f414e60e598974042c2d"
        },
        "emaileditview": {
            "title": "电子邮件编辑视图",
            "caption": "电子邮件",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Base",
            "viewname": "EmailEditView",
            "viewtag": "ee667129f46043ab7f8c050ae6f58ea0"
        },
        "quotestatetabview": {
            "title": "报价单信息",
            "caption": "报价单信息",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "Sales",
            "viewname": "QuoteStateTabView",
            "viewtag": "eea64939b2c1c4ead99cc9cc8c9bedfa"
        },
        "salesorderquickcreateview": {
            "title": "快速新建",
            "caption": "订单",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "Sales",
            "viewname": "SalesOrderQuickCreateView",
            "viewtag": "ef569fa840bcb70189bd385d4c378934"
        },
        "opportunitygridview": {
            "title": "商机信息",
            "caption": "商机",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Sales",
            "viewname": "OpportunityGridView",
            "viewtag": "ef8147a81cfc2945df706a4c18747d07"
        },
        "productpricelevelproprilveditview": {
            "title": "产品价目表项",
            "caption": "价目表项",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Product",
            "viewname": "ProductPriceLevelProPrilvEditView",
            "viewtag": "efc2d3a0f824323cf1a60908f903321e"
        },
        "subjectpickupview": {
            "title": "主题数据选择视图",
            "caption": "主题",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "Base",
            "viewname": "SubjectPickupView",
            "viewtag": "f0a3750df4fb3bdef7bec686c4e68020"
        },
        "incidentstatetabview": {
            "title": "案例状态分页视图",
            "caption": "案例",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "Service",
            "viewname": "IncidentStateTabView",
            "viewtag": "f0edb8f10b90394cbbb91fa0deed5746"
        },
        "campaignstopgridview": {
            "title": "市场活动信息",
            "caption": "市场活动",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Marketing",
            "viewname": "CampaignStopGridView",
            "viewtag": "f20be3272045407288fd104101df2d4e"
        },
        "employeemasterinfoview": {
            "title": "主信息概览视图",
            "caption": "员工",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "HumanResource",
            "viewname": "EmployeeMasterInfoView",
            "viewtag": "f4272a1943d153ce9709d411adc7cf4a"
        },
        "ibzdeptmembergridview": {
            "title": "部门成员表格视图",
            "caption": "部门成员",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "ou",
            "viewname": "IBZDeptMemberGridView",
            "viewtag": "f4330e7aa1db39a31289f78e9a395d74"
        },
        "wfusermpickupview": {
            "title": "用户数据多项选择视图",
            "caption": "用户",
            "viewtype": "DEMPICKUPVIEW",
            "viewmodule": "workflow",
            "viewname": "WFUserMPickupView",
            "viewtag": "f4d9c187a031b8596d6a55d676f4955e"
        },
        "contactstopgridview": {
            "title": "联系人信息",
            "caption": "联系人信息",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Base",
            "viewname": "ContactStopGridView",
            "viewtag": "f5b40e8aa9d2c730dfb65597400a4a9e"
        },
        "salesordergridview": {
            "title": "订单信息",
            "caption": "订单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "Sales",
            "viewname": "SalesOrderGridView",
            "viewtag": "f65470d1fefb22ccc7aaf7b49b0d7a3c"
        },
        "sysrolepickupgridview": {
            "title": "角色选择表格视图",
            "caption": "系统角色",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "uaa",
            "viewname": "SysRolePickupGridView",
            "viewtag": "f92fa90f9731add4b4ab1d5e7d15bfb6"
        },
        "omhierarchycatdashboardview": {
            "title": "结构层次类别看板",
            "caption": "结构层次类别",
            "viewtype": "DEPORTALVIEW",
            "viewmodule": "Base",
            "viewname": "OMHierarchyCatDashboardView",
            "viewtag": "f9c65c1342ab966e5e6944e35d9d172c"
        },
        "ibzemployeepickupgridview": {
            "title": "人员选择表格视图",
            "caption": "人员",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "ou",
            "viewname": "IBZEmployeePickupGridView",
            "viewtag": "f9fea148573d603c7bf77e149b1eab47"
        },
        "invoiceedit_datapanelview": {
            "title": "头部信息编辑",
            "caption": "发票",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Finance",
            "viewname": "InvoiceEdit_DataPanelView",
            "viewtag": "fa64077b7fe46caf7b2943be73312f9f"
        },
        "opportunitylistexpview": {
            "title": "商机信息",
            "caption": "商机信息",
            "viewtype": "DELISTEXPVIEW",
            "viewmodule": "Sales",
            "viewname": "OpportunityListExpView",
            "viewtag": "fb32e277fd6da165e31ce791c74a3701"
        },
        "wfgroupgridview": {
            "title": "流程角色",
            "caption": "流程角色",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "workflow",
            "viewname": "WFGroupGridView",
            "viewtag": "fc35d414df0a1183f5f35eb6dabe7941"
        },
        "quoteoptionview": {
            "title": "报价单选项操作视图",
            "caption": "报价单",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "Sales",
            "viewname": "QuoteOptionView",
            "viewtag": "fd1e3a15b1d164bfe5c7ec4ef0002bdc"
        },
        "campaigninfo_schedule": {
            "title": "日程安排",
            "caption": "日程安排",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Marketing",
            "viewname": "CampaignInfo_Schedule",
            "viewtag": "fd501ea4fdfc6719d909e882ae5e5178"
        },
        "legalmasterinfoview": {
            "title": "主信息概览视图",
            "caption": "法人",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Base",
            "viewname": "LegalMasterInfoView",
            "viewtag": "fd51b5cc0295272326340415414865ee"
        },
        "accounteditview": {
            "title": "客户编辑视图",
            "caption": "客户",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Base",
            "viewname": "AccountEditView",
            "viewtag": "fd5aac6492deb3713e0bf11e9babcebd"
        },
        "contactinfo_market": {
            "title": "联系人编辑视图",
            "caption": "联系人",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Base",
            "viewname": "ContactInfo_Market",
            "viewtag": "fd949f676a83595910b0e6a13bf4ca79"
        },
        "leadcompetitoredit_competitorview": {
            "title": "竞争对手潜在顾客信息",
            "caption": "潜在客户对手",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Sales",
            "viewname": "LeadCompetitorEdit_CompetitorView",
            "viewtag": "fe01af89d38a5baa855934c41e454c3b"
        },
        "opportunityinfo_abstract": {
            "title": "商机编辑视图",
            "caption": "商机",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "Sales",
            "viewname": "OpportunityInfo_Abstract",
            "viewtag": "ff1553675274b115b167253dd1cf550c"
        },
        "accountstatustabview": {
            "title": "客户",
            "caption": "客户",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "Base",
            "viewname": "AccountStatusTabView",
            "viewtag": "ff391715ae77b5b4434d466a3fc51001"
        }
    }];
});