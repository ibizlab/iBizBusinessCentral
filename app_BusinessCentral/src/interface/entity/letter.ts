/**
 * 信件
 *
 * @export
 * @interface Letter
 */
export interface Letter {

    /**
     * 已记帐
     *
     * @returns {*}
     * @memberof Letter
     */
    billed?: any;

    /**
     * 创建记录的时间
     *
     * @returns {*}
     * @memberof Letter
     */
    overriddencreatedon?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof Letter
     */
    updateman?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof Letter
     */
    createdate?: any;

    /**
     * 上一暂候时间
     *
     * @returns {*}
     * @memberof Letter
     */
    lastonholdtime?: any;

    /**
     * 版本号
     *
     * @returns {*}
     * @memberof Letter
     */
    versionnumber?: any;

    /**
     * 抄送
     *
     * @returns {*}
     * @memberof Letter
     */
    cc?: any;

    /**
     * 时区规则版本号
     *
     * @returns {*}
     * @memberof Letter
     */
    timezoneruleversionnumber?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof Letter
     */
    updatedate?: any;

    /**
     * 关于
     *
     * @returns {*}
     * @memberof Letter
     */
    regardingobjectid?: any;

    /**
     * 负责人类型
     *
     * @returns {*}
     * @memberof Letter
     */
    ownertype?: any;

    /**
     * 预订
     *
     * @returns {*}
     * @memberof Letter
     */
    subscriptionid?: any;

    /**
     * 开始日期
     *
     * @returns {*}
     * @memberof Letter
     */
    scheduledstart?: any;

    /**
     * 持续时间
     *
     * @returns {*}
     * @memberof Letter
     */
    actualdurationminutes?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof Letter
     */
    createman?: any;

    /**
     * 是定期活动
     *
     * @returns {*}
     * @memberof Letter
     */
    regularactivity?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof Letter
     */
    ownername?: any;

    /**
     * 流程阶段
     *
     * @returns {*}
     * @memberof Letter
     */
    stageid?: any;

    /**
     * 优先级
     *
     * @returns {*}
     * @memberof Letter
     */
    prioritycode?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof Letter
     */
    description?: any;

    /**
     * 类别
     *
     * @returns {*}
     * @memberof Letter
     */
    category?: any;

    /**
     * 实际开始时间
     *
     * @returns {*}
     * @memberof Letter
     */
    actualstart?: any;

    /**
     * 遍历的路径
     *
     * @returns {*}
     * @memberof Letter
     */
    traversedpath?: any;

    /**
     * 暂候时间(分钟)
     *
     * @returns {*}
     * @memberof Letter
     */
    onholdtime?: any;

    /**
     * UTC 转换时区代码
     *
     * @returns {*}
     * @memberof Letter
     */
    utcconversiontimezonecode?: any;

    /**
     * 方向
     *
     * @returns {*}
     * @memberof Letter
     */
    directioncode?: any;

    /**
     * 排序日期
     *
     * @returns {*}
     * @memberof Letter
     */
    sortdate?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof Letter
     */
    ownerid?: any;

    /**
     * 导入序列号
     *
     * @returns {*}
     * @memberof Letter
     */
    importsequencenumber?: any;

    /**
     * 密件抄送
     *
     * @returns {*}
     * @memberof Letter
     */
    bcc?: any;

    /**
     * RegardingObjectTypeCode
     *
     * @returns {*}
     * @memberof Letter
     */
    regardingobjecttypecode?: any;

    /**
     * 活动类型
     *
     * @returns {*}
     * @memberof Letter
     */
    activitytypecode?: any;

    /**
     * 实际结束时间
     *
     * @returns {*}
     * @memberof Letter
     */
    actualend?: any;

    /**
     * 活动状态
     *
     * @returns {*}
     * @memberof Letter
     */
    statecode?: any;

    /**
     * 汇率
     *
     * @returns {*}
     * @memberof Letter
     */
    exchangerate?: any;

    /**
     * 子类别
     *
     * @returns {*}
     * @memberof Letter
     */
    subcategory?: any;

    /**
     * 发件人
     *
     * @returns {*}
     * @memberof Letter
     */
    from?: any;

    /**
     * 流程
     *
     * @returns {*}
     * @memberof Letter
     */
    processid?: any;

    /**
     * 关于
     *
     * @returns {*}
     * @memberof Letter
     */
    regardingobjectname?: any;

    /**
     * 地址
     *
     * @returns {*}
     * @memberof Letter
     */
    address?: any;

    /**
     * 收件人
     *
     * @returns {*}
     * @memberof Letter
     */
    to?: any;

    /**
     * 计划持续时间
     *
     * @returns {*}
     * @memberof Letter
     */
    scheduleddurationminutes?: any;

    /**
     * 状态描述
     *
     * @returns {*}
     * @memberof Letter
     */
    statuscode?: any;

    /**
     * 由工作流创建
     *
     * @returns {*}
     * @memberof Letter
     */
    workflowcreated?: any;

    /**
     * 截止日期
     *
     * @returns {*}
     * @memberof Letter
     */
    scheduledend?: any;

    /**
     * 信件
     *
     * @returns {*}
     * @memberof Letter
     */
    activityid?: any;

    /**
     * SLAName
     *
     * @returns {*}
     * @memberof Letter
     */
    slaname?: any;

    /**
     * 主题
     *
     * @returns {*}
     * @memberof Letter
     */
    subject?: any;

    /**
     * 服务
     *
     * @returns {*}
     * @memberof Letter
     */
    serviceid?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof Letter
     */
    transactioncurrencyid?: any;

    /**
     * SLA
     *
     * @returns {*}
     * @memberof Letter
     */
    slaid?: any;
}