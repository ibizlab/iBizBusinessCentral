/**
 * 价目表
 *
 * @export
 * @interface PriceLevel
 */
export interface PriceLevel {

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof PriceLevel
     */
    createman?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof PriceLevel
     */
    updateman?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof PriceLevel
     */
    description?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof PriceLevel
     */
    statecode?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof PriceLevel
     */
    updatedate?: any;

    /**
     * 付款方式
     *
     * @returns {*}
     * @memberof PriceLevel
     */
    paymentmethodcode?: any;

    /**
     * 价目表
     *
     * @returns {*}
     * @memberof PriceLevel
     */
    pricelevelid?: any;

    /**
     * 送货方式
     *
     * @returns {*}
     * @memberof PriceLevel
     */
    shippingmethodcode?: any;

    /**
     * Record Created On
     *
     * @returns {*}
     * @memberof PriceLevel
     */
    overriddencreatedon?: any;

    /**
     * UTC Conversion Time Zone Code
     *
     * @returns {*}
     * @memberof PriceLevel
     */
    utcconversiontimezonecode?: any;

    /**
     * Version Number
     *
     * @returns {*}
     * @memberof PriceLevel
     */
    versionnumber?: any;

    /**
     * 结束日期
     *
     * @returns {*}
     * @memberof PriceLevel
     */
    enddate?: any;

    /**
     * 汇率
     *
     * @returns {*}
     * @memberof PriceLevel
     */
    exchangerate?: any;

    /**
     * 开始日期
     *
     * @returns {*}
     * @memberof PriceLevel
     */
    begindate?: any;

    /**
     * Import Sequence Number
     *
     * @returns {*}
     * @memberof PriceLevel
     */
    importsequencenumber?: any;

    /**
     * 价目表名称
     *
     * @returns {*}
     * @memberof PriceLevel
     */
    pricelevelname?: any;

    /**
     * 状态描述
     *
     * @returns {*}
     * @memberof PriceLevel
     */
    statuscode?: any;

    /**
     * 货运条款
     *
     * @returns {*}
     * @memberof PriceLevel
     */
    freighttermscode?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof PriceLevel
     */
    createdate?: any;

    /**
     * Time Zone Rule Version Number
     *
     * @returns {*}
     * @memberof PriceLevel
     */
    timezoneruleversionnumber?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof PriceLevel
     */
    transactioncurrencyid?: any;
}