/**
 * 岗位
 *
 * @export
 * @interface IBZPost
 */
export interface IBZPost {

    /**
     * 岗位标识
     *
     * @returns {*}
     * @memberof IBZPost
     */
    postid?: any;

    /**
     * 岗位编码
     *
     * @returns {*}
     * @memberof IBZPost
     */
    postcode?: any;

    /**
     * 岗位名称
     *
     * @returns {*}
     * @memberof IBZPost
     */
    postname?: any;

    /**
     * 区属
     *
     * @returns {*}
     * @memberof IBZPost
     */
    domains?: any;

    /**
     * 备注
     *
     * @returns {*}
     * @memberof IBZPost
     */
    memo?: any;
}