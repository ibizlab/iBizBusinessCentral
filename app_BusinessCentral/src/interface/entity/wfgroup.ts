/**
 * 角色/用户组
 *
 * @export
 * @interface WFGroup
 */
export interface WFGroup {

    /**
     * 组标识
     *
     * @returns {*}
     * @memberof WFGroup
     */
    id?: any;

    /**
     * 组名称
     *
     * @returns {*}
     * @memberof WFGroup
     */
    name?: any;

    /**
     * 范围
     *
     * @returns {*}
     * @memberof WFGroup
     */
    groupscope?: any;
}