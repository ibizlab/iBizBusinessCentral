/**
 * product
 *
 * @export
 * @interface Product
 */
export interface Product {

    /**
     * 供应商 ID
     *
     * @returns {*}
     * @memberof Product
     */
    vendorid?: any;

    /**
     * 产品结构
     *
     * @returns {*}
     * @memberof Product
     */
    productstructure?: any;

    /**
     * Version Number
     *
     * @returns {*}
     * @memberof Product
     */
    versionnumber?: any;

    /**
     * Stage Id
     *
     * @returns {*}
     * @memberof Product
     */
    stageid?: any;

    /**
     * 状态描述
     *
     * @returns {*}
     * @memberof Product
     */
    statuscode?: any;

    /**
     * 实体图像
     *
     * @returns {*}
     * @memberof Product
     */
    entityimage?: any;

    /**
     * Record Created On
     *
     * @returns {*}
     * @memberof Product
     */
    overriddencreatedon?: any;

    /**
     * 库存项
     *
     * @returns {*}
     * @memberof Product
     */
    stockitem?: any;

    /**
     * Import Sequence Number
     *
     * @returns {*}
     * @memberof Product
     */
    importsequencenumber?: any;

    /**
     * 有效期的开始日期
     *
     * @returns {*}
     * @memberof Product
     */
    validfromdate?: any;

    /**
     * 提供商名称
     *
     * @returns {*}
     * @memberof Product
     */
    suppliername?: any;

    /**
     * 产品类型
     *
     * @returns {*}
     * @memberof Product
     */
    producttypecode?: any;

    /**
     * EntityImageId
     *
     * @returns {*}
     * @memberof Product
     */
    entityimageid?: any;

    /**
     * EntityImage_Timestamp
     *
     * @returns {*}
     * @memberof Product
     */
    entityimage_timestamp?: any;

    /**
     * URL
     *
     * @returns {*}
     * @memberof Product
     */
    producturl?: any;

    /**
     * 标准成本
     *
     * @returns {*}
     * @memberof Product
     */
    standardcost?: any;

    /**
     * 现存数量
     *
     * @returns {*}
     * @memberof Product
     */
    quantityonhand?: any;

    /**
     * 当前成本
     *
     * @returns {*}
     * @memberof Product
     */
    currentcost?: any;

    /**
     * 汇率
     *
     * @returns {*}
     * @memberof Product
     */
    exchangerate?: any;

    /**
     * 产品
     *
     * @returns {*}
     * @memberof Product
     */
    productid?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof Product
     */
    description?: any;

    /**
     * 供应商
     *
     * @returns {*}
     * @memberof Product
     */
    vendorname?: any;

    /**
     * 仅供内部使用
     *
     * @returns {*}
     * @memberof Product
     */
    dmtimportstate?: any;

    /**
     * EntityImage_URL
     *
     * @returns {*}
     * @memberof Product
     */
    entityimage_url?: any;

    /**
     * 库存容量
     *
     * @returns {*}
     * @memberof Product
     */
    stockvolume?: any;

    /**
     * 支持小数
     *
     * @returns {*}
     * @memberof Product
     */
    quantitydecimal?: any;

    /**
     * 供应商名称
     *
     * @returns {*}
     * @memberof Product
     */
    vendorpartnumber?: any;

    /**
     * 有效期的结束日期
     *
     * @returns {*}
     * @memberof Product
     */
    validtodate?: any;

    /**
     * Reparented
     *
     * @returns {*}
     * @memberof Product
     */
    reparented?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof Product
     */
    createman?: any;

    /**
     * Process Id
     *
     * @returns {*}
     * @memberof Product
     */
    processid?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof Product
     */
    createdate?: any;

    /**
     * 产品 ID
     *
     * @returns {*}
     * @memberof Product
     */
    productnumber?: any;

    /**
     * 为配套件
     *
     * @returns {*}
     * @memberof Product
     */
    kit?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof Product
     */
    updatedate?: any;

    /**
     * 当前成本 (Base)
     *
     * @returns {*}
     * @memberof Product
     */
    currentcost_base?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof Product
     */
    updateman?: any;

    /**
     * 库存重量
     *
     * @returns {*}
     * @memberof Product
     */
    stockweight?: any;

    /**
     * 大小
     *
     * @returns {*}
     * @memberof Product
     */
    size?: any;

    /**
     * Time Zone Rule Version Number
     *
     * @returns {*}
     * @memberof Product
     */
    timezoneruleversionnumber?: any;

    /**
     * UTC Conversion Time Zone Code
     *
     * @returns {*}
     * @memberof Product
     */
    utcconversiontimezonecode?: any;

    /**
     * 产品名称
     *
     * @returns {*}
     * @memberof Product
     */
    productname?: any;

    /**
     * 定价 (Base)
     *
     * @returns {*}
     * @memberof Product
     */
    price_base?: any;

    /**
     * 定价
     *
     * @returns {*}
     * @memberof Product
     */
    price?: any;

    /**
     * Traversed Path
     *
     * @returns {*}
     * @memberof Product
     */
    traversedpath?: any;

    /**
     * 层次结构路径
     *
     * @returns {*}
     * @memberof Product
     */
    hierarchypath?: any;

    /**
     * 标准成本 (Base)
     *
     * @returns {*}
     * @memberof Product
     */
    standardcost_base?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof Product
     */
    statecode?: any;

    /**
     * 计价单位组
     *
     * @returns {*}
     * @memberof Product
     */
    defaultuomschedulename?: any;

    /**
     * 主题
     *
     * @returns {*}
     * @memberof Product
     */
    subjectname?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof Product
     */
    currencyname?: any;

    /**
     * 默认价目表
     *
     * @returns {*}
     * @memberof Product
     */
    pricelevelname?: any;

    /**
     * 默认计价单位
     *
     * @returns {*}
     * @memberof Product
     */
    defaultuomname?: any;

    /**
     * 父级
     *
     * @returns {*}
     * @memberof Product
     */
    parentproductname?: any;

    /**
     * 父级
     *
     * @returns {*}
     * @memberof Product
     */
    parentproductid?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof Product
     */
    transactioncurrencyid?: any;

    /**
     * 默认价目表
     *
     * @returns {*}
     * @memberof Product
     */
    pricelevelid?: any;

    /**
     * 默认计价单位
     *
     * @returns {*}
     * @memberof Product
     */
    defaultuomid?: any;

    /**
     * 计价单位组
     *
     * @returns {*}
     * @memberof Product
     */
    defaultuomscheduleid?: any;

    /**
     * 主题
     *
     * @returns {*}
     * @memberof Product
     */
    subjectid?: any;
}