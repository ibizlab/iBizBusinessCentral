/**
 * 案例客户
 *
 * @export
 * @interface IncidentCustomer
 */
export interface IncidentCustomer {

    /**
     * 客户
     *
     * @returns {*}
     * @memberof IncidentCustomer
     */
    customerid?: any;

    /**
     * 商业类型
     *
     * @returns {*}
     * @memberof IncidentCustomer
     */
    businesstypecode?: any;

    /**
     * 客户类型
     *
     * @returns {*}
     * @memberof IncidentCustomer
     */
    customertype?: any;

    /**
     * 客户
     *
     * @returns {*}
     * @memberof IncidentCustomer
     */
    customername?: any;
}