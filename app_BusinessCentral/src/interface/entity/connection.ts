/**
 * 连接
 *
 * @export
 * @interface Connection
 */
export interface Connection {

    /**
     * 类型(目标)
     *
     * @returns {*}
     * @memberof Connection
     */
    record2objecttypecode?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof Connection
     */
    updatedate?: any;

    /**
     * EntityImage_Timestamp
     *
     * @returns {*}
     * @memberof Connection
     */
    entityimage_timestamp?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof Connection
     */
    createdate?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof Connection
     */
    createman?: any;

    /**
     * 类型(源)
     *
     * @returns {*}
     * @memberof Connection
     */
    record1objecttypecode?: any;

    /**
     * 已连接到
     *
     * @returns {*}
     * @memberof Connection
     */
    record2id?: any;

    /**
     * 主记录
     *
     * @returns {*}
     * @memberof Connection
     */
    master?: any;

    /**
     * 连接自
     *
     * @returns {*}
     * @memberof Connection
     */
    record1idobjecttypecode?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof Connection
     */
    ownerid?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof Connection
     */
    updateman?: any;

    /**
     * 正在启动
     *
     * @returns {*}
     * @memberof Connection
     */
    effectivestart?: any;

    /**
     * 连接自
     *
     * @returns {*}
     * @memberof Connection
     */
    record1id?: any;

    /**
     * 创建记录的时间
     *
     * @returns {*}
     * @memberof Connection
     */
    overriddencreatedon?: any;

    /**
     * 导入序列号
     *
     * @returns {*}
     * @memberof Connection
     */
    importsequencenumber?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof Connection
     */
    ownername?: any;

    /**
     * 负责人类型
     *
     * @returns {*}
     * @memberof Connection
     */
    ownertype?: any;

    /**
     * 已连接到
     *
     * @returns {*}
     * @memberof Connection
     */
    record2idobjecttypecode?: any;

    /**
     * 汇率
     *
     * @returns {*}
     * @memberof Connection
     */
    exchangerate?: any;

    /**
     * 实体图像 ID
     *
     * @returns {*}
     * @memberof Connection
     */
    entityimageid?: any;

    /**
     * 版本号
     *
     * @returns {*}
     * @memberof Connection
     */
    versionnumber?: any;

    /**
     * 正在结束
     *
     * @returns {*}
     * @memberof Connection
     */
    effectiveend?: any;

    /**
     * EntityImage_URL
     *
     * @returns {*}
     * @memberof Connection
     */
    entityimage_url?: any;

    /**
     * 关联名称
     *
     * @returns {*}
     * @memberof Connection
     */
    connectionname?: any;

    /**
     * 状态描述
     *
     * @returns {*}
     * @memberof Connection
     */
    statuscode?: any;

    /**
     * 连接
     *
     * @returns {*}
     * @memberof Connection
     */
    connectionid?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof Connection
     */
    statecode?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof Connection
     */
    description?: any;

    /**
     * 实体图像
     *
     * @returns {*}
     * @memberof Connection
     */
    entityimage?: any;

    /**
     * 角色
     *
     * @returns {*}
     * @memberof Connection
     */
    record1rolename?: any;

    /**
     * 角色（目标）
     *
     * @returns {*}
     * @memberof Connection
     */
    record2rolename?: any;

    /**
     * 角色(源)
     *
     * @returns {*}
     * @memberof Connection
     */
    record1roleid?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof Connection
     */
    transactioncurrencyid?: any;

    /**
     * 角色(目标)
     *
     * @returns {*}
     * @memberof Connection
     */
    record2roleid?: any;
}