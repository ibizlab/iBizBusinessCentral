/**
 * 应用
 *
 * @export
 * @interface SysApp
 */
export interface SysApp {

    /**
     * 应用标识
     *
     * @returns {*}
     * @memberof SysApp
     */
    id?: any;

    /**
     * 应用名
     *
     * @returns {*}
     * @memberof SysApp
     */
    label?: any;

    /**
     * 系统标识
     *
     * @returns {*}
     * @memberof SysApp
     */
    systemid?: any;

    /**
     * 全称
     *
     * @returns {*}
     * @memberof SysApp
     */
    fullname?: any;

    /**
     * 类型
     *
     * @returns {*}
     * @memberof SysApp
     */
    type?: any;

    /**
     * 分组
     *
     * @returns {*}
     * @memberof SysApp
     */
    group?: any;

    /**
     * 图标
     *
     * @returns {*}
     * @memberof SysApp
     */
    icon?: any;

    /**
     * 可见
     *
     * @returns {*}
     * @memberof SysApp
     */
    visabled?: any;

    /**
     * 地址
     *
     * @returns {*}
     * @memberof SysApp
     */
    addr?: any;
}