/**
 * 市场活动-市场活动
 *
 * @export
 * @interface CampaignCampaign
 */
export interface CampaignCampaign {

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof CampaignCampaign
     */
    createman?: any;

    /**
     * 关系类型
     *
     * @returns {*}
     * @memberof CampaignCampaign
     */
    relationshipstype?: any;

    /**
     * 关系名称
     *
     * @returns {*}
     * @memberof CampaignCampaign
     */
    relationshipsname?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof CampaignCampaign
     */
    updatedate?: any;

    /**
     * 关系标识
     *
     * @returns {*}
     * @memberof CampaignCampaign
     */
    relationshipsid?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof CampaignCampaign
     */
    createdate?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof CampaignCampaign
     */
    updateman?: any;

    /**
     * 活动名称
     *
     * @returns {*}
     * @memberof CampaignCampaign
     */
    entityname?: any;

    /**
     * 活动2名称
     *
     * @returns {*}
     * @memberof CampaignCampaign
     */
    entity2name?: any;

    /**
     * 市场活动
     *
     * @returns {*}
     * @memberof CampaignCampaign
     */
    entity2id?: any;

    /**
     * 市场活动
     *
     * @returns {*}
     * @memberof CampaignCampaign
     */
    entityid?: any;
}