/**
 * 价目表项
 *
 * @export
 * @interface ProductPriceLevel
 */
export interface ProductPriceLevel {

    /**
     * Import Sequence Number
     *
     * @returns {*}
     * @memberof ProductPriceLevel
     */
    importsequencenumber?: any;

    /**
     * 舍入金额
     *
     * @returns {*}
     * @memberof ProductPriceLevel
     */
    roundingoptionamount?: any;

    /**
     * Version Number
     *
     * @returns {*}
     * @memberof ProductPriceLevel
     */
    versionnumber?: any;

    /**
     * 金额 (Base)
     *
     * @returns {*}
     * @memberof ProductPriceLevel
     */
    amount_base?: any;

    /**
     * Process Id
     *
     * @returns {*}
     * @memberof ProductPriceLevel
     */
    processid?: any;

    /**
     * 金额
     *
     * @returns {*}
     * @memberof ProductPriceLevel
     */
    amount?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof ProductPriceLevel
     */
    createdate?: any;

    /**
     * 舍入金额 (Base)
     *
     * @returns {*}
     * @memberof ProductPriceLevel
     */
    roundingoptionamount_base?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof ProductPriceLevel
     */
    updateman?: any;

    /**
     * Record Created On
     *
     * @returns {*}
     * @memberof ProductPriceLevel
     */
    overriddencreatedon?: any;

    /**
     * 汇率
     *
     * @returns {*}
     * @memberof ProductPriceLevel
     */
    exchangerate?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof ProductPriceLevel
     */
    organizationid?: any;

    /**
     * 百分比
     *
     * @returns {*}
     * @memberof ProductPriceLevel
     */
    percentage?: any;

    /**
     * Traversed Path
     *
     * @returns {*}
     * @memberof ProductPriceLevel
     */
    traversedpath?: any;

    /**
     * 产品 ID
     *
     * @returns {*}
     * @memberof ProductPriceLevel
     */
    productnumber?: any;

    /**
     * 定价方式
     *
     * @returns {*}
     * @memberof ProductPriceLevel
     */
    pricingmethodcode?: any;

    /**
     * 舍入选项
     *
     * @returns {*}
     * @memberof ProductPriceLevel
     */
    roundingoptioncode?: any;

    /**
     * Time Zone Rule Version Number
     *
     * @returns {*}
     * @memberof ProductPriceLevel
     */
    timezoneruleversionnumber?: any;

    /**
     * UTC Conversion Time Zone Code
     *
     * @returns {*}
     * @memberof ProductPriceLevel
     */
    utcconversiontimezonecode?: any;

    /**
     * 产品价目表
     *
     * @returns {*}
     * @memberof ProductPriceLevel
     */
    productpricelevelid?: any;

    /**
     * 销售数量控制
     *
     * @returns {*}
     * @memberof ProductPriceLevel
     */
    quantitysellingcode?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof ProductPriceLevel
     */
    updatedate?: any;

    /**
     * 舍入原则
     *
     * @returns {*}
     * @memberof ProductPriceLevel
     */
    roundingpolicycode?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof ProductPriceLevel
     */
    createman?: any;

    /**
     * Stage Id
     *
     * @returns {*}
     * @memberof ProductPriceLevel
     */
    stageid?: any;

    /**
     * 计量单位组
     *
     * @returns {*}
     * @memberof ProductPriceLevel
     */
    uomschedulename?: any;

    /**
     * 产品名称
     *
     * @returns {*}
     * @memberof ProductPriceLevel
     */
    productname?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof ProductPriceLevel
     */
    currencyname?: any;

    /**
     * 计量单位
     *
     * @returns {*}
     * @memberof ProductPriceLevel
     */
    uomname?: any;

    /**
     * 折扣表
     *
     * @returns {*}
     * @memberof ProductPriceLevel
     */
    discounttypename?: any;

    /**
     * 价目表
     *
     * @returns {*}
     * @memberof ProductPriceLevel
     */
    pricelevelname?: any;

    /**
     * 产品
     *
     * @returns {*}
     * @memberof ProductPriceLevel
     */
    productid?: any;

    /**
     * 单位计划 ID
     *
     * @returns {*}
     * @memberof ProductPriceLevel
     */
    uomscheduleid?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof ProductPriceLevel
     */
    transactioncurrencyid?: any;

    /**
     * 计价单位
     *
     * @returns {*}
     * @memberof ProductPriceLevel
     */
    uomid?: any;

    /**
     * 折扣表
     *
     * @returns {*}
     * @memberof ProductPriceLevel
     */
    discounttypeid?: any;

    /**
     * 价目表
     *
     * @returns {*}
     * @memberof ProductPriceLevel
     */
    pricelevelid?: any;
}