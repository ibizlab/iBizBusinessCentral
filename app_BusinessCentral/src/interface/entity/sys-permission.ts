/**
 * 权限/资源
 *
 * @export
 * @interface SysPermission
 */
export interface SysPermission {

    /**
     * 资源标识
     *
     * @returns {*}
     * @memberof SysPermission
     */
    permissionid?: any;

    /**
     * 资源名称
     *
     * @returns {*}
     * @memberof SysPermission
     */
    permissionname?: any;

    /**
     * 资源类别
     *
     * @returns {*}
     * @memberof SysPermission
     */
    permissiontype?: any;

    /**
     * 系统
     *
     * @returns {*}
     * @memberof SysPermission
     */
    pssystemid?: any;

    /**
     * 逻辑有效
     *
     * @returns {*}
     * @memberof SysPermission
     */
    enable?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof SysPermission
     */
    createdate?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof SysPermission
     */
    updatedate?: any;
}