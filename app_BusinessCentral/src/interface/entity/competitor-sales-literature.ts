/**
 * 竞争对手宣传资料
 *
 * @export
 * @interface CompetitorSalesLiterature
 */
export interface CompetitorSalesLiterature {

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof CompetitorSalesLiterature
     */
    updatedate?: any;

    /**
     * 关系类型
     *
     * @returns {*}
     * @memberof CompetitorSalesLiterature
     */
    relationshipstype?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof CompetitorSalesLiterature
     */
    updateman?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof CompetitorSalesLiterature
     */
    createman?: any;

    /**
     * 关系名称
     *
     * @returns {*}
     * @memberof CompetitorSalesLiterature
     */
    relationshipsname?: any;

    /**
     * 关系标识
     *
     * @returns {*}
     * @memberof CompetitorSalesLiterature
     */
    relationshipsid?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof CompetitorSalesLiterature
     */
    createdate?: any;

    /**
     * 竞争对手
     *
     * @returns {*}
     * @memberof CompetitorSalesLiterature
     */
    entityname?: any;

    /**
     * 销售宣传资料
     *
     * @returns {*}
     * @memberof CompetitorSalesLiterature
     */
    entity2name?: any;

    /**
     * 销售宣传资料
     *
     * @returns {*}
     * @memberof CompetitorSalesLiterature
     */
    entity2id?: any;

    /**
     * 竞争对手
     *
     * @returns {*}
     * @memberof CompetitorSalesLiterature
     */
    entityid?: any;
}