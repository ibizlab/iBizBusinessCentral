/**
 * 发票
 *
 * @export
 * @interface Invoice
 */
export interface Invoice {

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof Invoice
     */
    ownername?: any;

    /**
     * 帐单寄往街道 1
     *
     * @returns {*}
     * @memberof Invoice
     */
    billto_line1?: any;

    /**
     * 帐单寄往地的电话号码
     *
     * @returns {*}
     * @memberof Invoice
     */
    billto_telephone?: any;

    /**
     * 上一暂候时间
     *
     * @returns {*}
     * @memberof Invoice
     */
    lastonholdtime?: any;

    /**
     * 帐单寄往省/市/自治区
     *
     * @returns {*}
     * @memberof Invoice
     */
    billto_stateorprovince?: any;

    /**
     * 截止日期
     *
     * @returns {*}
     * @memberof Invoice
     */
    duedate?: any;

    /**
     * 定价错误
     *
     * @returns {*}
     * @memberof Invoice
     */
    pricingerrorcode?: any;

    /**
     * Traversed Path
     *
     * @returns {*}
     * @memberof Invoice
     */
    traversedpath?: any;

    /**
     * 折扣金额总和 (Base)
     *
     * @returns {*}
     * @memberof Invoice
     */
    totaldiscountamount_base?: any;

    /**
     * 折后金额总计 (Base)
     *
     * @returns {*}
     * @memberof Invoice
     */
    totalamountlessfreight_base?: any;

    /**
     * 最近提交给 Back Office
     *
     * @returns {*}
     * @memberof Invoice
     */
    lastbackofficesubmit?: any;

    /**
     * 送货地的电话号码
     *
     * @returns {*}
     * @memberof Invoice
     */
    shipto_telephone?: any;

    /**
     * 负责人类型
     *
     * @returns {*}
     * @memberof Invoice
     */
    ownertype?: any;

    /**
     * 帐单寄往市/县
     *
     * @returns {*}
     * @memberof Invoice
     */
    billto_city?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof Invoice
     */
    createman?: any;

    /**
     * 送货地址
     *
     * @returns {*}
     * @memberof Invoice
     */
    shipto_composite?: any;

    /**
     * Record Created On
     *
     * @returns {*}
     * @memberof Invoice
     */
    overriddencreatedon?: any;

    /**
     * 帐单寄往地邮政编码
     *
     * @returns {*}
     * @memberof Invoice
     */
    billto_postalcode?: any;

    /**
     * 运费金额 (Base)
     *
     * @returns {*}
     * @memberof Invoice
     */
    freightamount_base?: any;

    /**
     * 发票名称
     *
     * @returns {*}
     * @memberof Invoice
     */
    invoicename?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof Invoice
     */
    updatedate?: any;

    /**
     * 送至市/县
     *
     * @returns {*}
     * @memberof Invoice
     */
    shipto_city?: any;

    /**
     * 总金额
     *
     * @returns {*}
     * @memberof Invoice
     */
    totalamount?: any;

    /**
     * 客户
     *
     * @returns {*}
     * @memberof Invoice
     */
    customerid?: any;

    /**
     * 总金额 (Base)
     *
     * @returns {*}
     * @memberof Invoice
     */
    totalamount_base?: any;

    /**
     * 帐单寄往地的名称
     *
     * @returns {*}
     * @memberof Invoice
     */
    billto_name?: any;

    /**
     * Time Zone Rule Version Number
     *
     * @returns {*}
     * @memberof Invoice
     */
    timezoneruleversionnumber?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof Invoice
     */
    description?: any;

    /**
     * UTC Conversion Time Zone Code
     *
     * @returns {*}
     * @memberof Invoice
     */
    utcconversiontimezonecode?: any;

    /**
     * 运费金额
     *
     * @returns {*}
     * @memberof Invoice
     */
    freightamount?: any;

    /**
     * 优先级
     *
     * @returns {*}
     * @memberof Invoice
     */
    prioritycode?: any;

    /**
     * 发票折扣金额 (Base)
     *
     * @returns {*}
     * @memberof Invoice
     */
    discountamount_base?: any;

    /**
     * 暂候时间(分钟)
     *
     * @returns {*}
     * @memberof Invoice
     */
    onholdtime?: any;

    /**
     * 送至国家/地区
     *
     * @returns {*}
     * @memberof Invoice
     */
    shipto_country?: any;

    /**
     * 付款条件
     *
     * @returns {*}
     * @memberof Invoice
     */
    paymenttermscode?: any;

    /**
     * 货运条款
     *
     * @returns {*}
     * @memberof Invoice
     */
    shipto_freighttermscode?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof Invoice
     */
    updateman?: any;

    /**
     * 送至街道 3
     *
     * @returns {*}
     * @memberof Invoice
     */
    shipto_line3?: any;

    /**
     * 总税款
     *
     * @returns {*}
     * @memberof Invoice
     */
    totaltax?: any;

    /**
     * 帐单寄往街道 2
     *
     * @returns {*}
     * @memberof Invoice
     */
    billto_line2?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof Invoice
     */
    createdate?: any;

    /**
     * 汇率
     *
     * @returns {*}
     * @memberof Invoice
     */
    exchangerate?: any;

    /**
     * 送至街道 1
     *
     * @returns {*}
     * @memberof Invoice
     */
    shipto_line1?: any;

    /**
     * 发票
     *
     * @returns {*}
     * @memberof Invoice
     */
    invoiceid?: any;

    /**
     * 客户
     *
     * @returns {*}
     * @memberof Invoice
     */
    customername?: any;

    /**
     * 发票折扣(%)
     *
     * @returns {*}
     * @memberof Invoice
     */
    discountpercentage?: any;

    /**
     * 明细金额总计 (Base)
     *
     * @returns {*}
     * @memberof Invoice
     */
    totallineitemamount_base?: any;

    /**
     * Stage Id
     *
     * @returns {*}
     * @memberof Invoice
     */
    stageid?: any;

    /**
     * EntityImage_URL
     *
     * @returns {*}
     * @memberof Invoice
     */
    entityimage_url?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof Invoice
     */
    statecode?: any;

    /**
     * 折后金额总计
     *
     * @returns {*}
     * @memberof Invoice
     */
    totalamountlessfreight?: any;

    /**
     * 总税款 (Base)
     *
     * @returns {*}
     * @memberof Invoice
     */
    totaltax_base?: any;

    /**
     * 送至街道 2
     *
     * @returns {*}
     * @memberof Invoice
     */
    shipto_line2?: any;

    /**
     * 帐单寄往国家/地区
     *
     * @returns {*}
     * @memberof Invoice
     */
    billto_country?: any;

    /**
     * 实体图像
     *
     * @returns {*}
     * @memberof Invoice
     */
    entityimage?: any;

    /**
     * Import Sequence Number
     *
     * @returns {*}
     * @memberof Invoice
     */
    importsequencenumber?: any;

    /**
     * 送货地的传真号码
     *
     * @returns {*}
     * @memberof Invoice
     */
    shipto_fax?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof Invoice
     */
    ownerid?: any;

    /**
     * 交付日期
     *
     * @returns {*}
     * @memberof Invoice
     */
    datedelivered?: any;

    /**
     * 联系人
     *
     * @returns {*}
     * @memberof Invoice
     */
    contactname?: any;

    /**
     * EntityImage_Timestamp
     *
     * @returns {*}
     * @memberof Invoice
     */
    entityimage_timestamp?: any;

    /**
     * Process Id
     *
     * @returns {*}
     * @memberof Invoice
     */
    processid?: any;

    /**
     * 折扣金额总和
     *
     * @returns {*}
     * @memberof Invoice
     */
    totaldiscountamount?: any;

    /**
     * 已锁定的价格
     *
     * @returns {*}
     * @memberof Invoice
     */
    pricelocked?: any;

    /**
     * 送货地址
     *
     * @returns {*}
     * @memberof Invoice
     */
    willcall?: any;

    /**
     * 帐单寄往街道 3
     *
     * @returns {*}
     * @memberof Invoice
     */
    billto_line3?: any;

    /**
     * 送货地的名称
     *
     * @returns {*}
     * @memberof Invoice
     */
    shipto_name?: any;

    /**
     * 状态描述
     *
     * @returns {*}
     * @memberof Invoice
     */
    statuscode?: any;

    /**
     * 明细金额总计
     *
     * @returns {*}
     * @memberof Invoice
     */
    totallineitemamount?: any;

    /**
     * 发票折扣金额
     *
     * @returns {*}
     * @memberof Invoice
     */
    discountamount?: any;

    /**
     * 送至省/市/自治区
     *
     * @returns {*}
     * @memberof Invoice
     */
    shipto_stateorprovince?: any;

    /**
     * EntityImageId
     *
     * @returns {*}
     * @memberof Invoice
     */
    entityimageid?: any;

    /**
     * Version Number
     *
     * @returns {*}
     * @memberof Invoice
     */
    versionnumber?: any;

    /**
     * 送货地的邮政编码
     *
     * @returns {*}
     * @memberof Invoice
     */
    shipto_postalcode?: any;

    /**
     * 发票编码
     *
     * @returns {*}
     * @memberof Invoice
     */
    invoicenumber?: any;

    /**
     * 帐单寄往地址
     *
     * @returns {*}
     * @memberof Invoice
     */
    billto_composite?: any;

    /**
     * Email Address
     *
     * @returns {*}
     * @memberof Invoice
     */
    emailaddress?: any;

    /**
     * 送货方式
     *
     * @returns {*}
     * @memberof Invoice
     */
    shippingmethodcode?: any;

    /**
     * 客户类型
     *
     * @returns {*}
     * @memberof Invoice
     */
    customertype?: any;

    /**
     * 明细项目折扣金额总和
     *
     * @returns {*}
     * @memberof Invoice
     */
    totallineitemdiscountamount?: any;

    /**
     * 客户
     *
     * @returns {*}
     * @memberof Invoice
     */
    accountname?: any;

    /**
     * 帐单寄往地的传真号码
     *
     * @returns {*}
     * @memberof Invoice
     */
    billto_fax?: any;

    /**
     * 商机
     *
     * @returns {*}
     * @memberof Invoice
     */
    opportunityname?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof Invoice
     */
    currencyname?: any;

    /**
     * 价目表
     *
     * @returns {*}
     * @memberof Invoice
     */
    pricelevelname?: any;

    /**
     * SLA
     *
     * @returns {*}
     * @memberof Invoice
     */
    slaname?: any;

    /**
     * 订单
     *
     * @returns {*}
     * @memberof Invoice
     */
    salesordername?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof Invoice
     */
    transactioncurrencyid?: any;

    /**
     * 价目表
     *
     * @returns {*}
     * @memberof Invoice
     */
    pricelevelid?: any;

    /**
     * 订单
     *
     * @returns {*}
     * @memberof Invoice
     */
    salesorderid?: any;

    /**
     * 商机
     *
     * @returns {*}
     * @memberof Invoice
     */
    opportunityid?: any;

    /**
     * SLA
     *
     * @returns {*}
     * @memberof Invoice
     */
    slaid?: any;
}