/**
 * 任务注册信息
 *
 * @export
 * @interface JobsRegistry
 */
export interface JobsRegistry {

    /**
     * 主键ID
     *
     * @returns {*}
     * @memberof JobsRegistry
     */
    id?: any;

    /**
     * 服务名
     *
     * @returns {*}
     * @memberof JobsRegistry
     */
    app?: any;

    /**
     * 执行地址
     *
     * @returns {*}
     * @memberof JobsRegistry
     */
    address?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof JobsRegistry
     */
    status?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof JobsRegistry
     */
    update_time?: any;
}