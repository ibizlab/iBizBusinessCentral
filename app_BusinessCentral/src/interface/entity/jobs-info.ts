/**
 * 任务信息
 *
 * @export
 * @interface JobsInfo
 */
export interface JobsInfo {

    /**
     * 主键ID
     *
     * @returns {*}
     * @memberof JobsInfo
     */
    id?: any;

    /**
     * 租户ID
     *
     * @returns {*}
     * @memberof JobsInfo
     */
    tenant_id?: any;

    /**
     * 服务名
     *
     * @returns {*}
     * @memberof JobsInfo
     */
    app?: any;

    /**
     * 任务执行CRON
     *
     * @returns {*}
     * @memberof JobsInfo
     */
    cron?: any;

    /**
     * 执行器任务HANDLER
     *
     * @returns {*}
     * @memberof JobsInfo
     */
    handler?: any;

    /**
     * 执行器任务参数
     *
     * @returns {*}
     * @memberof JobsInfo
     */
    param?: any;

    /**
     * 任务执行超时时间（秒）
     *
     * @returns {*}
     * @memberof JobsInfo
     */
    timeout?: any;

    /**
     * 失败重试次数
     *
     * @returns {*}
     * @memberof JobsInfo
     */
    fail_retry_count?: any;

    /**
     * 上次调度时间
     *
     * @returns {*}
     * @memberof JobsInfo
     */
    last_time?: any;

    /**
     * 下次调度时间
     *
     * @returns {*}
     * @memberof JobsInfo
     */
    next_time?: any;

    /**
     * 所有者
     *
     * @returns {*}
     * @memberof JobsInfo
     */
    author?: any;

    /**
     * 备注
     *
     * @returns {*}
     * @memberof JobsInfo
     */
    remark?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof JobsInfo
     */
    status?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof JobsInfo
     */
    update_time?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof JobsInfo
     */
    create_time?: any;
}