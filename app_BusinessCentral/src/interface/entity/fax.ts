/**
 * 传真
 *
 * @export
 * @interface Fax
 */
export interface Fax {

    /**
     * 状态描述
     *
     * @returns {*}
     * @memberof Fax
     */
    statuscode?: any;

    /**
     * 收件人
     *
     * @returns {*}
     * @memberof Fax
     */
    to?: any;

    /**
     * 优先级
     *
     * @returns {*}
     * @memberof Fax
     */
    prioritycode?: any;

    /**
     * 版本号
     *
     * @returns {*}
     * @memberof Fax
     */
    versionnumber?: any;

    /**
     * SLAName
     *
     * @returns {*}
     * @memberof Fax
     */
    slaname?: any;

    /**
     * 关于
     *
     * @returns {*}
     * @memberof Fax
     */
    regardingobjectname?: any;

    /**
     * 遍历的路径
     *
     * @returns {*}
     * @memberof Fax
     */
    traversedpath?: any;

    /**
     * 导入序列号
     *
     * @returns {*}
     * @memberof Fax
     */
    importsequencenumber?: any;

    /**
     * 发件人
     *
     * @returns {*}
     * @memberof Fax
     */
    from?: any;

    /**
     * 暂候时间(分钟)
     *
     * @returns {*}
     * @memberof Fax
     */
    onholdtime?: any;

    /**
     * 汇率
     *
     * @returns {*}
     * @memberof Fax
     */
    exchangerate?: any;

    /**
     * 类别
     *
     * @returns {*}
     * @memberof Fax
     */
    category?: any;

    /**
     * 主题
     *
     * @returns {*}
     * @memberof Fax
     */
    subject?: any;

    /**
     * 页数
     *
     * @returns {*}
     * @memberof Fax
     */
    numberofpages?: any;

    /**
     * 排序日期
     *
     * @returns {*}
     * @memberof Fax
     */
    sortdate?: any;

    /**
     * UTC 转换时区代码
     *
     * @returns {*}
     * @memberof Fax
     */
    utcconversiontimezonecode?: any;

    /**
     * 时区规则版本号
     *
     * @returns {*}
     * @memberof Fax
     */
    timezoneruleversionnumber?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof Fax
     */
    updateman?: any;

    /**
     * 由工作流创建
     *
     * @returns {*}
     * @memberof Fax
     */
    workflowcreated?: any;

    /**
     * 活动状态
     *
     * @returns {*}
     * @memberof Fax
     */
    statecode?: any;

    /**
     * 传真
     *
     * @returns {*}
     * @memberof Fax
     */
    activityid?: any;

    /**
     * 负责人类型
     *
     * @returns {*}
     * @memberof Fax
     */
    ownertype?: any;

    /**
     * 记帐代码
     *
     * @returns {*}
     * @memberof Fax
     */
    billingcode?: any;

    /**
     * 是定期活动
     *
     * @returns {*}
     * @memberof Fax
     */
    regularactivity?: any;

    /**
     * 封面名称
     *
     * @returns {*}
     * @memberof Fax
     */
    coverpagename?: any;

    /**
     * 传真号码
     *
     * @returns {*}
     * @memberof Fax
     */
    faxnumber?: any;

    /**
     * 流程阶段
     *
     * @returns {*}
     * @memberof Fax
     */
    stageid?: any;

    /**
     * 实际结束时间
     *
     * @returns {*}
     * @memberof Fax
     */
    actualend?: any;

    /**
     * 关于
     *
     * @returns {*}
     * @memberof Fax
     */
    regardingobjectid?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof Fax
     */
    description?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof Fax
     */
    createman?: any;

    /**
     * 方向
     *
     * @returns {*}
     * @memberof Fax
     */
    directioncode?: any;

    /**
     * 持续时间
     *
     * @returns {*}
     * @memberof Fax
     */
    actualdurationminutes?: any;

    /**
     * 实际开始时间
     *
     * @returns {*}
     * @memberof Fax
     */
    actualstart?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof Fax
     */
    ownerid?: any;

    /**
     * 活动类型
     *
     * @returns {*}
     * @memberof Fax
     */
    activitytypecode?: any;

    /**
     * 子类别
     *
     * @returns {*}
     * @memberof Fax
     */
    subcategory?: any;

    /**
     * RegardingObjectTypeCode
     *
     * @returns {*}
     * @memberof Fax
     */
    regardingobjecttypecode?: any;

    /**
     * 截止日期
     *
     * @returns {*}
     * @memberof Fax
     */
    scheduledend?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof Fax
     */
    updatedate?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof Fax
     */
    createdate?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof Fax
     */
    ownername?: any;

    /**
     * 已记帐
     *
     * @returns {*}
     * @memberof Fax
     */
    billed?: any;

    /**
     * 上一暂候时间
     *
     * @returns {*}
     * @memberof Fax
     */
    lastonholdtime?: any;

    /**
     * 创建记录的时间
     *
     * @returns {*}
     * @memberof Fax
     */
    overriddencreatedon?: any;

    /**
     * 计划持续时间
     *
     * @returns {*}
     * @memberof Fax
     */
    scheduleddurationminutes?: any;

    /**
     * 流程
     *
     * @returns {*}
     * @memberof Fax
     */
    processid?: any;

    /**
     * 预订
     *
     * @returns {*}
     * @memberof Fax
     */
    subscriptionid?: any;

    /**
     * 开始日期
     *
     * @returns {*}
     * @memberof Fax
     */
    scheduledstart?: any;

    /**
     * 传输站 ID
     *
     * @returns {*}
     * @memberof Fax
     */
    tsid?: any;

    /**
     * SLA
     *
     * @returns {*}
     * @memberof Fax
     */
    slaid?: any;

    /**
     * 服务
     *
     * @returns {*}
     * @memberof Fax
     */
    serviceid?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof Fax
     */
    transactioncurrencyid?: any;
}