/**
 * 营销列表-联系人
 *
 * @export
 * @interface ListContact
 */
export interface ListContact {

    /**
     * 关系标识
     *
     * @returns {*}
     * @memberof ListContact
     */
    relationshipsid?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof ListContact
     */
    updatedate?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof ListContact
     */
    createdate?: any;

    /**
     * 关系名称
     *
     * @returns {*}
     * @memberof ListContact
     */
    relationshipsname?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof ListContact
     */
    updateman?: any;

    /**
     * 关系类型
     *
     * @returns {*}
     * @memberof ListContact
     */
    relationshipstype?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof ListContact
     */
    createman?: any;

    /**
     * 电子邮件
     *
     * @returns {*}
     * @memberof ListContact
     */
    emailaddress1?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof ListContact
     */
    statecode?: any;

    /**
     * 联系人
     *
     * @returns {*}
     * @memberof ListContact
     */
    entity2name?: any;

    /**
     * 公司名称
     *
     * @returns {*}
     * @memberof ListContact
     */
    parentcustomerid?: any;

    /**
     * 营销列表
     *
     * @returns {*}
     * @memberof ListContact
     */
    entityname?: any;

    /**
     * 商务电话
     *
     * @returns {*}
     * @memberof ListContact
     */
    telephone1?: any;

    /**
     * 联系人
     *
     * @returns {*}
     * @memberof ListContact
     */
    entity2id?: any;

    /**
     * 列表
     *
     * @returns {*}
     * @memberof ListContact
     */
    entityid?: any;
}