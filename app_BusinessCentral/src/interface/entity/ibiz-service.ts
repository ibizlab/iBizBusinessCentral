/**
 * 服务
 *
 * @export
 * @interface IBizService
 */
export interface IBizService {

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof IBizService
     */
    createdate?: any;

    /**
     * Time Zone Rule Version Number
     *
     * @returns {*}
     * @memberof IBizService
     */
    timezoneruleversionnumber?: any;

    /**
     * Record Created On
     *
     * @returns {*}
     * @memberof IBizService
     */
    overriddencreatedon?: any;

    /**
     * 定位偏差
     *
     * @returns {*}
     * @memberof IBizService
     */
    anchoroffset?: any;

    /**
     * 初始状态描述
     *
     * @returns {*}
     * @memberof IBizService
     */
    initialstatuscode?: any;

    /**
     * 粒度
     *
     * @returns {*}
     * @memberof IBizService
     */
    granularity?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof IBizService
     */
    updateman?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof IBizService
     */
    description?: any;

    /**
     * 显示资源
     *
     * @returns {*}
     * @memberof IBizService
     */
    showresources?: any;

    /**
     * service名称
     *
     * @returns {*}
     * @memberof IBizService
     */
    servicename?: any;

    /**
     * Import Sequence Number
     *
     * @returns {*}
     * @memberof IBizService
     */
    importsequencenumber?: any;

    /**
     * 可公开
     *
     * @returns {*}
     * @memberof IBizService
     */
    visible?: any;

    /**
     * 日历
     *
     * @returns {*}
     * @memberof IBizService
     */
    calendarid?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof IBizService
     */
    createman?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof IBizService
     */
    schedulable?: any;

    /**
     * 策略
     *
     * @returns {*}
     * @memberof IBizService
     */
    strategyid?: any;

    /**
     * Version Number
     *
     * @returns {*}
     * @memberof IBizService
     */
    versionnumber?: any;

    /**
     * 服务
     *
     * @returns {*}
     * @memberof IBizService
     */
    serviceid?: any;

    /**
     * 持续时间
     *
     * @returns {*}
     * @memberof IBizService
     */
    duration?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof IBizService
     */
    updatedate?: any;

    /**
     * UTC Conversion Time Zone Code
     *
     * @returns {*}
     * @memberof IBizService
     */
    utcconversiontimezonecode?: any;

    /**
     * 所需资源
     *
     * @returns {*}
     * @memberof IBizService
     */
    resourcespecid?: any;
}