/**
 * 市场营销列表
 *
 * @export
 * @interface IBizList
 */
export interface IBizList {

    /**
     * 查询
     *
     * @returns {*}
     * @memberof IBizList
     */
    query?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof IBizList
     */
    updateman?: any;

    /**
     * 已锁定
     *
     * @returns {*}
     * @memberof IBizList
     */
    lockstatus?: any;

    /**
     * 用途
     *
     * @returns {*}
     * @memberof IBizList
     */
    purpose?: any;

    /**
     * 上次使用时间
     *
     * @returns {*}
     * @memberof IBizList
     */
    lastusedon?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof IBizList
     */
    ownerid?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof IBizList
     */
    ownername?: any;

    /**
     * 列表
     *
     * @returns {*}
     * @memberof IBizList
     */
    listid?: any;

    /**
     * 目标对象
     *
     * @returns {*}
     * @memberof IBizList
     */
    membertype?: any;

    /**
     * 排除退出的成员
     *
     * @returns {*}
     * @memberof IBizList
     */
    donotsendonoptout?: any;

    /**
     * Version Number
     *
     * @returns {*}
     * @memberof IBizList
     */
    versionnumber?: any;

    /**
     * 成本
     *
     * @returns {*}
     * @memberof IBizList
     */
    cost?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof IBizList
     */
    statecode?: any;

    /**
     * 汇率
     *
     * @returns {*}
     * @memberof IBizList
     */
    exchangerate?: any;

    /**
     * 负责人类型
     *
     * @returns {*}
     * @memberof IBizList
     */
    ownertype?: any;

    /**
     * 名称
     *
     * @returns {*}
     * @memberof IBizList
     */
    listname?: any;

    /**
     * 状态描述
     *
     * @returns {*}
     * @memberof IBizList
     */
    statuscode?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof IBizList
     */
    description?: any;

    /**
     * 类型
     *
     * @returns {*}
     * @memberof IBizList
     */
    type?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof IBizList
     */
    createdate?: any;

    /**
     * Import Sequence Number
     *
     * @returns {*}
     * @memberof IBizList
     */
    importsequencenumber?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof IBizList
     */
    createman?: any;

    /**
     * Process Id
     *
     * @returns {*}
     * @memberof IBizList
     */
    processid?: any;

    /**
     * 成本 (Base)
     *
     * @returns {*}
     * @memberof IBizList
     */
    cost_base?: any;

    /**
     * 成员计数
     *
     * @returns {*}
     * @memberof IBizList
     */
    membercount?: any;

    /**
     * 忽略停用列表成员
     *
     * @returns {*}
     * @memberof IBizList
     */
    ignoreinactivelistmembers?: any;

    /**
     * Stage Id
     *
     * @returns {*}
     * @memberof IBizList
     */
    stageid?: any;

    /**
     * 来源
     *
     * @returns {*}
     * @memberof IBizList
     */
    source?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof IBizList
     */
    updatedate?: any;

    /**
     * UTC Conversion Time Zone Code
     *
     * @returns {*}
     * @memberof IBizList
     */
    utcconversiontimezonecode?: any;

    /**
     * 成员类型
     *
     * @returns {*}
     * @memberof IBizList
     */
    createdfromcode?: any;

    /**
     * Record Created On
     *
     * @returns {*}
     * @memberof IBizList
     */
    overriddencreatedon?: any;

    /**
     * Time Zone Rule Version Number
     *
     * @returns {*}
     * @memberof IBizList
     */
    timezoneruleversionnumber?: any;

    /**
     * Traversed Path
     *
     * @returns {*}
     * @memberof IBizList
     */
    traversedpath?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof IBizList
     */
    currencyname?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof IBizList
     */
    transactioncurrencyid?: any;
}