/**
 * 角色权限关系
 *
 * @export
 * @interface SysRolePermission
 */
export interface SysRolePermission {

    /**
     * 角色权限关系表标识
     *
     * @returns {*}
     * @memberof SysRolePermission
     */
    rolepermissionid?: any;

    /**
     * 角色表标识
     *
     * @returns {*}
     * @memberof SysRolePermission
     */
    roleid?: any;

    /**
     * 角色名称
     *
     * @returns {*}
     * @memberof SysRolePermission
     */
    rolename?: any;

    /**
     * 权限表标识
     *
     * @returns {*}
     * @memberof SysRolePermission
     */
    permissionid?: any;

    /**
     * 权限名称
     *
     * @returns {*}
     * @memberof SysRolePermission
     */
    permissionname?: any;

    /**
     * 权限类型
     *
     * @returns {*}
     * @memberof SysRolePermission
     */
    permissiontype?: any;

    /**
     * 权限类型
     *
     * @returns {*}
     * @memberof SysRolePermission
     */
    permissionenable?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof SysRolePermission
     */
    createdate?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof SysRolePermission
     */
    updatedate?: any;
}