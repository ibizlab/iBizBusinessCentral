/**
 * 目标度量
 *
 * @export
 * @interface Metric
 */
export interface Metric {

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof Metric
     */
    createman?: any;

    /**
     * 状态描述
     *
     * @returns {*}
     * @memberof Metric
     */
    statuscode?: any;

    /**
     * 度量类型
     *
     * @returns {*}
     * @memberof Metric
     */
    amount?: any;

    /**
     * 时区规则版本号
     *
     * @returns {*}
     * @memberof Metric
     */
    timezoneruleversionnumber?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof Metric
     */
    updateman?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof Metric
     */
    description?: any;

    /**
     * UTC 转换时区代码
     *
     * @returns {*}
     * @memberof Metric
     */
    utcconversiontimezonecode?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof Metric
     */
    statecode?: any;

    /**
     * 创建记录的时间
     *
     * @returns {*}
     * @memberof Metric
     */
    overriddencreatedon?: any;

    /**
     * 公制名称
     *
     * @returns {*}
     * @memberof Metric
     */
    metricname?: any;

    /**
     * 导入序列号
     *
     * @returns {*}
     * @memberof Metric
     */
    importsequencenumber?: any;

    /**
     * 目标度量
     *
     * @returns {*}
     * @memberof Metric
     */
    metricid?: any;

    /**
     * 金额数据类型
     *
     * @returns {*}
     * @memberof Metric
     */
    amountdatatype?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof Metric
     */
    createdate?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof Metric
     */
    updatedate?: any;

    /**
     * 版本号
     *
     * @returns {*}
     * @memberof Metric
     */
    versionnumber?: any;

    /**
     * 跟踪扩展目标值
     *
     * @returns {*}
     * @memberof Metric
     */
    stretchtracked?: any;
}