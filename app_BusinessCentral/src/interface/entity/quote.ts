/**
 * 报价单
 *
 * @export
 * @interface Quote
 */
export interface Quote {

    /**
     * 帐单寄往国家/地区
     *
     * @returns {*}
     * @memberof Quote
     */
    billto_country?: any;

    /**
     * 暂候时间(分钟)
     *
     * @returns {*}
     * @memberof Quote
     */
    onholdtime?: any;

    /**
     * 送货地址
     *
     * @returns {*}
     * @memberof Quote
     */
    willcall?: any;

    /**
     * 送至街道 1
     *
     * @returns {*}
     * @memberof Quote
     */
    shipto_line1?: any;

    /**
     * 货运条款
     *
     * @returns {*}
     * @memberof Quote
     */
    freighttermscode?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof Quote
     */
    statecode?: any;

    /**
     * 开始生效日期
     *
     * @returns {*}
     * @memberof Quote
     */
    effectivefrom?: any;

    /**
     * 折后金额总计 (Base)
     *
     * @returns {*}
     * @memberof Quote
     */
    totalamountlessfreight_base?: any;

    /**
     * 报价名称
     *
     * @returns {*}
     * @memberof Quote
     */
    quotename?: any;

    /**
     * 折扣金额总和 (Base)
     *
     * @returns {*}
     * @memberof Quote
     */
    totaldiscountamount_base?: any;

    /**
     * 送货地的邮政编码
     *
     * @returns {*}
     * @memberof Quote
     */
    shipto_postalcode?: any;

    /**
     * 总金额
     *
     * @returns {*}
     * @memberof Quote
     */
    totalamount?: any;

    /**
     * 帐单邮寄地址 ID
     *
     * @returns {*}
     * @memberof Quote
     */
    billto_addressid?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof Quote
     */
    updateman?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof Quote
     */
    createman?: any;

    /**
     * 总金额 (Base)
     *
     * @returns {*}
     * @memberof Quote
     */
    totalamount_base?: any;

    /**
     * 送至街道 2
     *
     * @returns {*}
     * @memberof Quote
     */
    shipto_line2?: any;

    /**
     * 明细项目折扣金额总和
     *
     * @returns {*}
     * @memberof Quote
     */
    totallineitemdiscountamount?: any;

    /**
     * Time Zone Rule Version Number
     *
     * @returns {*}
     * @memberof Quote
     */
    timezoneruleversionnumber?: any;

    /**
     * 明细金额总计 (Base)
     *
     * @returns {*}
     * @memberof Quote
     */
    totallineitemamount_base?: any;

    /**
     * 送货地的联系人姓名
     *
     * @returns {*}
     * @memberof Quote
     */
    shipto_contactname?: any;

    /**
     * Record Created On
     *
     * @returns {*}
     * @memberof Quote
     */
    overriddencreatedon?: any;

    /**
     * 送货地址 ID
     *
     * @returns {*}
     * @memberof Quote
     */
    shipto_addressid?: any;

    /**
     * 送货地的名称
     *
     * @returns {*}
     * @memberof Quote
     */
    shipto_name?: any;

    /**
     * 状态描述
     *
     * @returns {*}
     * @memberof Quote
     */
    statuscode?: any;

    /**
     * 报价单 ID
     *
     * @returns {*}
     * @memberof Quote
     */
    quotenumber?: any;

    /**
     * 帐单寄往地的电话号码
     *
     * @returns {*}
     * @memberof Quote
     */
    billto_telephone?: any;

    /**
     * 结束日期
     *
     * @returns {*}
     * @memberof Quote
     */
    closedon?: any;

    /**
     * 运费金额 (Base)
     *
     * @returns {*}
     * @memberof Quote
     */
    freightamount_base?: any;

    /**
     * 送至街道 3
     *
     * @returns {*}
     * @memberof Quote
     */
    shipto_line3?: any;

    /**
     * 送货方式
     *
     * @returns {*}
     * @memberof Quote
     */
    shippingmethodcode?: any;

    /**
     * 潜在客户
     *
     * @returns {*}
     * @memberof Quote
     */
    customerid?: any;

    /**
     * 有效截止时间
     *
     * @returns {*}
     * @memberof Quote
     */
    effectiveto?: any;

    /**
     * 货运条款
     *
     * @returns {*}
     * @memberof Quote
     */
    shipto_freighttermscode?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof Quote
     */
    updatedate?: any;

    /**
     * 明细金额总计
     *
     * @returns {*}
     * @memberof Quote
     */
    totallineitemamount?: any;

    /**
     * 送货地的传真号码
     *
     * @returns {*}
     * @memberof Quote
     */
    shipto_fax?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof Quote
     */
    ownerid?: any;

    /**
     * 客户
     *
     * @returns {*}
     * @memberof Quote
     */
    accountname?: any;

    /**
     * Email Address
     *
     * @returns {*}
     * @memberof Quote
     */
    emailaddress?: any;

    /**
     * Import Sequence Number
     *
     * @returns {*}
     * @memberof Quote
     */
    importsequencenumber?: any;

    /**
     * UTC Conversion Time Zone Code
     *
     * @returns {*}
     * @memberof Quote
     */
    utcconversiontimezonecode?: any;

    /**
     * 客户
     *
     * @returns {*}
     * @memberof Quote
     */
    customername?: any;

    /**
     * 定价错误
     *
     * @returns {*}
     * @memberof Quote
     */
    pricingerrorcode?: any;

    /**
     * 唯一说明 ID
     *
     * @returns {*}
     * @memberof Quote
     */
    uniquedscid?: any;

    /**
     * 帐单寄往街道 3
     *
     * @returns {*}
     * @memberof Quote
     */
    billto_line3?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof Quote
     */
    description?: any;

    /**
     * 帐单寄往地址
     *
     * @returns {*}
     * @memberof Quote
     */
    billto_composite?: any;

    /**
     * 要求交付日期
     *
     * @returns {*}
     * @memberof Quote
     */
    requestdeliveryby?: any;

    /**
     * 总税款
     *
     * @returns {*}
     * @memberof Quote
     */
    totaltax?: any;

    /**
     * 报价单
     *
     * @returns {*}
     * @memberof Quote
     */
    quoteid?: any;

    /**
     * Process Id
     *
     * @returns {*}
     * @memberof Quote
     */
    processid?: any;

    /**
     * Stage Id
     *
     * @returns {*}
     * @memberof Quote
     */
    stageid?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof Quote
     */
    createdate?: any;

    /**
     * 联系人
     *
     * @returns {*}
     * @memberof Quote
     */
    contactname?: any;

    /**
     * 汇率
     *
     * @returns {*}
     * @memberof Quote
     */
    exchangerate?: any;

    /**
     * Version Number
     *
     * @returns {*}
     * @memberof Quote
     */
    versionnumber?: any;

    /**
     * 送至国家/地区
     *
     * @returns {*}
     * @memberof Quote
     */
    shipto_country?: any;

    /**
     * 到期时间
     *
     * @returns {*}
     * @memberof Quote
     */
    expireson?: any;

    /**
     * 负责人类型
     *
     * @returns {*}
     * @memberof Quote
     */
    ownertype?: any;

    /**
     * 帐单寄往地邮政编码
     *
     * @returns {*}
     * @memberof Quote
     */
    billto_postalcode?: any;

    /**
     * 帐单寄往街道 2
     *
     * @returns {*}
     * @memberof Quote
     */
    billto_line2?: any;

    /**
     * 帐单寄往地的名称
     *
     * @returns {*}
     * @memberof Quote
     */
    billto_name?: any;

    /**
     * 帐单寄往街道 1
     *
     * @returns {*}
     * @memberof Quote
     */
    billto_line1?: any;

    /**
     * 修订 ID
     *
     * @returns {*}
     * @memberof Quote
     */
    revisionnumber?: any;

    /**
     * 帐单寄往地联系人姓名
     *
     * @returns {*}
     * @memberof Quote
     */
    billto_contactname?: any;

    /**
     * 送至省/市/自治区
     *
     * @returns {*}
     * @memberof Quote
     */
    shipto_stateorprovince?: any;

    /**
     * 帐单寄往市/县
     *
     * @returns {*}
     * @memberof Quote
     */
    billto_city?: any;

    /**
     * 上一暂候时间
     *
     * @returns {*}
     * @memberof Quote
     */
    lastonholdtime?: any;

    /**
     * 报价单折扣(%)
     *
     * @returns {*}
     * @memberof Quote
     */
    discountpercentage?: any;

    /**
     * 送至市/县
     *
     * @returns {*}
     * @memberof Quote
     */
    shipto_city?: any;

    /**
     * 潜在客户类型
     *
     * @returns {*}
     * @memberof Quote
     */
    customertype?: any;

    /**
     * 报价单折扣金额
     *
     * @returns {*}
     * @memberof Quote
     */
    discountamount?: any;

    /**
     * 送货地址
     *
     * @returns {*}
     * @memberof Quote
     */
    shipto_composite?: any;

    /**
     * 送货地的电话号码
     *
     * @returns {*}
     * @memberof Quote
     */
    shipto_telephone?: any;

    /**
     * 帐单寄往省/市/自治区
     *
     * @returns {*}
     * @memberof Quote
     */
    billto_stateorprovince?: any;

    /**
     * 付款条件
     *
     * @returns {*}
     * @memberof Quote
     */
    paymenttermscode?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof Quote
     */
    ownername?: any;

    /**
     * 折后金额总计
     *
     * @returns {*}
     * @memberof Quote
     */
    totalamountlessfreight?: any;

    /**
     * Traversed Path
     *
     * @returns {*}
     * @memberof Quote
     */
    traversedpath?: any;

    /**
     * 帐单寄往地的传真号码
     *
     * @returns {*}
     * @memberof Quote
     */
    billto_fax?: any;

    /**
     * 折扣金额总和
     *
     * @returns {*}
     * @memberof Quote
     */
    totaldiscountamount?: any;

    /**
     * 总税款 (Base)
     *
     * @returns {*}
     * @memberof Quote
     */
    totaltax_base?: any;

    /**
     * 报价单折扣金额 (Base)
     *
     * @returns {*}
     * @memberof Quote
     */
    discountamount_base?: any;

    /**
     * 运费金额
     *
     * @returns {*}
     * @memberof Quote
     */
    freightamount?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof Quote
     */
    currencyname?: any;

    /**
     * 价目表
     *
     * @returns {*}
     * @memberof Quote
     */
    pricelevelname?: any;

    /**
     * 商机
     *
     * @returns {*}
     * @memberof Quote
     */
    opportunityname?: any;

    /**
     * SLA
     *
     * @returns {*}
     * @memberof Quote
     */
    slaname?: any;

    /**
     * 源市场活动
     *
     * @returns {*}
     * @memberof Quote
     */
    campaignname?: any;

    /**
     * 价目表
     *
     * @returns {*}
     * @memberof Quote
     */
    pricelevelid?: any;

    /**
     * SLA
     *
     * @returns {*}
     * @memberof Quote
     */
    slaid?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof Quote
     */
    transactioncurrencyid?: any;

    /**
     * 商机
     *
     * @returns {*}
     * @memberof Quote
     */
    opportunityid?: any;

    /**
     * 源市场活动
     *
     * @returns {*}
     * @memberof Quote
     */
    campaignid?: any;
}