/**
 * 产品替换
 *
 * @export
 * @interface ProductSubstitute
 */
export interface ProductSubstitute {

    /**
     * 产品子套件名称
     *
     * @returns {*}
     * @memberof ProductSubstitute
     */
    productsubstitutename?: any;

    /**
     * 状态描述
     *
     * @returns {*}
     * @memberof ProductSubstitute
     */
    statuscode?: any;

    /**
     * Version Number
     *
     * @returns {*}
     * @memberof ProductSubstitute
     */
    versionnumber?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof ProductSubstitute
     */
    createman?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof ProductSubstitute
     */
    updateman?: any;

    /**
     * Import Sequence Number
     *
     * @returns {*}
     * @memberof ProductSubstitute
     */
    importsequencenumber?: any;

    /**
     * 汇率
     *
     * @returns {*}
     * @memberof ProductSubstitute
     */
    exchangerate?: any;

    /**
     * UTC Conversion Time Zone Code
     *
     * @returns {*}
     * @memberof ProductSubstitute
     */
    utcconversiontimezonecode?: any;

    /**
     * Record Created On
     *
     * @returns {*}
     * @memberof ProductSubstitute
     */
    overriddencreatedon?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof ProductSubstitute
     */
    createdate?: any;

    /**
     * 方向
     *
     * @returns {*}
     * @memberof ProductSubstitute
     */
    direction?: any;

    /**
     * 销售关系类型
     *
     * @returns {*}
     * @memberof ProductSubstitute
     */
    salesrelationshiptype?: any;

    /**
     * Time Zone Rule Version Number
     *
     * @returns {*}
     * @memberof ProductSubstitute
     */
    timezoneruleversionnumber?: any;

    /**
     * 产品关系 ID
     *
     * @returns {*}
     * @memberof ProductSubstitute
     */
    productsubstituteid?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof ProductSubstitute
     */
    statecode?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof ProductSubstitute
     */
    updatedate?: any;

    /**
     * 产品
     *
     * @returns {*}
     * @memberof ProductSubstitute
     */
    productname?: any;

    /**
     * 相关产品
     *
     * @returns {*}
     * @memberof ProductSubstitute
     */
    substitutedproductname?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof ProductSubstitute
     */
    currencyname?: any;

    /**
     * 产品
     *
     * @returns {*}
     * @memberof ProductSubstitute
     */
    productid?: any;

    /**
     * 相关产品
     *
     * @returns {*}
     * @memberof ProductSubstitute
     */
    substitutedproductid?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof ProductSubstitute
     */
    transactioncurrencyid?: any;
}