/**
 * 组织层次结构应用
 *
 * @export
 * @interface OMHierarchyPurpose
 */
export interface OMHierarchyPurpose {

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof OMHierarchyPurpose
     */
    updatedate?: any;

    /**
     * 组织层次机构目的名称
     *
     * @returns {*}
     * @memberof OMHierarchyPurpose
     */
    omhierarchypurposename?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof OMHierarchyPurpose
     */
    createman?: any;

    /**
     * 组织层次机构目的标识
     *
     * @returns {*}
     * @memberof OMHierarchyPurpose
     */
    omhierarchypurposeid?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof OMHierarchyPurpose
     */
    createdate?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof OMHierarchyPurpose
     */
    updateman?: any;
}