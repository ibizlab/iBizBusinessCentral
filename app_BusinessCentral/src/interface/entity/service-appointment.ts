/**
 * 服务活动
 *
 * @export
 * @interface ServiceAppointment
 */
export interface ServiceAppointment {

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    updateman?: any;

    /**
     * 计划开始时间
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    scheduledstart?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    ownerid?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    statecode?: any;

    /**
     * 上次尝试传递的日期
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    deliverylastattemptedon?: any;

    /**
     * 负责人类型
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    ownertype?: any;

    /**
     * 计划结束时间
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    scheduledend?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    createman?: any;

    /**
     * 资源
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    resources?: any;

    /**
     * Import Sequence Number
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    importsequencenumber?: any;

    /**
     * 暂候时间(分钟)
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    onholdtime?: any;

    /**
     * UTC 转换时区代码
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    utcconversiontimezonecode?: any;

    /**
     * 实际结束时间
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    actualend?: any;

    /**
     * Required Attendees
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    requiredattendees?: any;

    /**
     * 实际开始时间
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    actualstart?: any;

    /**
     * 发送日期
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    senton?: any;

    /**
     * 实际持续时间
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    actualdurationminutes?: any;

    /**
     * 已记帐
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    billed?: any;

    /**
     * Record Created On
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    overriddencreatedon?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    updatedate?: any;

    /**
     * SLAName
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    slaname?: any;

    /**
     * 专用
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    mapiprivate?: any;

    /**
     * 类别
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    category?: any;

    /**
     * CC
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    cc?: any;

    /**
     * 优先级
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    prioritycode?: any;

    /**
     * BCC
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    bcc?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    createdate?: any;

    /**
     * To
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    to?: any;

    /**
     * 送达位置
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    location?: any;

    /**
     * 状态描述
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    statuscode?: any;

    /**
     * 预订
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    subscriptionid?: any;

    /**
     * Exchange WebLink
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    exchangeweblink?: any;

    /**
     * 附加参数
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    activityadditionalparams?: any;

    /**
     * 全天事件
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    alldayevent?: any;

    /**
     * 传递优先级
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    deliveryprioritycode?: any;

    /**
     * 定期实例类型
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    instancetypecode?: any;

    /**
     * 遍历的路径
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    traversedpath?: any;

    /**
     * 相关
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    regardingobjectid?: any;

    /**
     * 流程阶段
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    stageid?: any;

    /**
     * Organizer
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    organizer?: any;

    /**
     * 保留的语音邮件
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    leftvoicemail?: any;

    /**
     * 汇率
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    exchangerate?: any;

    /**
     * Outsource Vendors
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    partners?: any;

    /**
     * 系列 ID
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    seriesid?: any;

    /**
     * 版本号
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    versionnumber?: any;

    /**
     * 服务活动
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    activityid?: any;

    /**
     * 上一暂候时间
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    lastonholdtime?: any;

    /**
     * 排序日期
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    sortdate?: any;

    /**
     * 主题
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    subject?: any;

    /**
     * From
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    from?: any;

    /**
     * 子类别
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    subcategory?: any;

    /**
     * 时区规则版本号
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    timezoneruleversionnumber?: any;

    /**
     * 社交渠道
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    community?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    description?: any;

    /**
     * 关于
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    regardingobjectname?: any;

    /**
     * Optional Attendees
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    optionalattendees?: any;

    /**
     * 计划持续时间
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    scheduleddurationminutes?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    ownername?: any;

    /**
     * 流程
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    processid?: any;

    /**
     * 活动类型
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    activitytypecode?: any;

    /**
     * 是定期活动
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    regularactivity?: any;

    /**
     * Exchange 项目 ID
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    exchangeitemid?: any;

    /**
     * 由工作流创建
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    workflowcreated?: any;

    /**
     * 客户
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    customers?: any;

    /**
     * RegardingObjectTypeCode
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    regardingobjecttypecode?: any;

    /**
     * SLA
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    slaid?: any;

    /**
     * 服务
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    serviceid?: any;

    /**
     * 场所
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    siteid?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof ServiceAppointment
     */
    transactioncurrencyid?: any;
}