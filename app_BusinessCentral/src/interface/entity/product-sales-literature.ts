/**
 * 实体
 *
 * @export
 * @interface ProductSalesLiterature
 */
export interface ProductSalesLiterature {

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof ProductSalesLiterature
     */
    updateman?: any;

    /**
     * 关系名称
     *
     * @returns {*}
     * @memberof ProductSalesLiterature
     */
    relationshipsname?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof ProductSalesLiterature
     */
    createman?: any;

    /**
     * 关系标识
     *
     * @returns {*}
     * @memberof ProductSalesLiterature
     */
    relationshipsid?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof ProductSalesLiterature
     */
    createdate?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof ProductSalesLiterature
     */
    updatedate?: any;

    /**
     * 关系类型
     *
     * @returns {*}
     * @memberof ProductSalesLiterature
     */
    relationshipstype?: any;

    /**
     * 销售宣传资料
     *
     * @returns {*}
     * @memberof ProductSalesLiterature
     */
    entity2name?: any;

    /**
     * 产品
     *
     * @returns {*}
     * @memberof ProductSalesLiterature
     */
    entityname?: any;

    /**
     * 产品
     *
     * @returns {*}
     * @memberof ProductSalesLiterature
     */
    entityid?: any;

    /**
     * 销售宣传资料
     *
     * @returns {*}
     * @memberof ProductSalesLiterature
     */
    entity2id?: any;
}