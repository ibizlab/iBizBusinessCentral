/**
 * 员工
 *
 * @export
 * @interface Employee
 */
export interface Employee {

    /**
     * 员工名称
     *
     * @returns {*}
     * @memberof Employee
     */
    employeename?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof Employee
     */
    updateman?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof Employee
     */
    createman?: any;

    /**
     * 员工标识
     *
     * @returns {*}
     * @memberof Employee
     */
    employeeid?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof Employee
     */
    updatedate?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof Employee
     */
    createdate?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof Employee
     */
    organizationid?: any;

    /**
     * 组织编码
     *
     * @returns {*}
     * @memberof Employee
     */
    orgcode?: any;

    /**
     * 组织层级
     *
     * @returns {*}
     * @memberof Employee
     */
    orglevel?: any;

    /**
     * 排序号
     *
     * @returns {*}
     * @memberof Employee
     */
    showorder?: any;

    /**
     * 组织简称
     *
     * @returns {*}
     * @memberof Employee
     */
    shortname?: any;

    /**
     * 员工编号
     *
     * @returns {*}
     * @memberof Employee
     */
    employeecode?: any;

    /**
     * 出生日期
     *
     * @returns {*}
     * @memberof Employee
     */
    birthday?: any;

    /**
     * 证件号码
     *
     * @returns {*}
     * @memberof Employee
     */
    certnum?: any;

    /**
     * 年龄
     *
     * @returns {*}
     * @memberof Employee
     */
    age?: any;

    /**
     * 移动电话
     *
     * @returns {*}
     * @memberof Employee
     */
    mobile?: any;

    /**
     * 电子邮箱
     *
     * @returns {*}
     * @memberof Employee
     */
    email?: any;

    /**
     * 出生地
     *
     * @returns {*}
     * @memberof Employee
     */
    birthaddress?: any;

    /**
     * 通讯地址
     *
     * @returns {*}
     * @memberof Employee
     */
    postaladdress?: any;

    /**
     * 爱好特长
     *
     * @returns {*}
     * @memberof Employee
     */
    hobby?: any;

    /**
     * 户籍地址
     *
     * @returns {*}
     * @memberof Employee
     */
    nativeaddress?: any;

    /**
     * 到本单位时间
     *
     * @returns {*}
     * @memberof Employee
     */
    startorgtime?: any;

    /**
     * 健康状况
     *
     * @returns {*}
     * @memberof Employee
     */
    health?: any;

    /**
     * 参加工作时间
     *
     * @returns {*}
     * @memberof Employee
     */
    startworktime?: any;

    /**
     * 照片
     *
     * @returns {*}
     * @memberof Employee
     */
    photo?: any;

    /**
     * 技术职称
     *
     * @returns {*}
     * @memberof Employee
     */
    technicaltitle?: any;

    /**
     * 籍贯
     *
     * @returns {*}
     * @memberof Employee
     */
    nativeplace?: any;

    /**
     * 执业证书
     *
     * @returns {*}
     * @memberof Employee
     */
    certificates?: any;

    /**
     * 固定电话
     *
     * @returns {*}
     * @memberof Employee
     */
    telephone?: any;

    /**
     * 血型
     *
     * @returns {*}
     * @memberof Employee
     */
    bloodtype?: any;

    /**
     * 民族
     *
     * @returns {*}
     * @memberof Employee
     */
    nation?: any;

    /**
     * 证件类型
     *
     * @returns {*}
     * @memberof Employee
     */
    certtype?: any;

    /**
     * 性别
     *
     * @returns {*}
     * @memberof Employee
     */
    sex?: any;

    /**
     * 婚姻状况
     *
     * @returns {*}
     * @memberof Employee
     */
    marriage?: any;

    /**
     * 户口类型
     *
     * @returns {*}
     * @memberof Employee
     */
    nativetype?: any;

    /**
     * 政治面貌
     *
     * @returns {*}
     * @memberof Employee
     */
    political?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof Employee
     */
    organizationname?: any;
}