/**
 * 部门
 *
 * @export
 * @interface IBZDepartment
 */
export interface IBZDepartment {

    /**
     * 部门标识
     *
     * @returns {*}
     * @memberof IBZDepartment
     */
    deptid?: any;

    /**
     * 部门代码
     *
     * @returns {*}
     * @memberof IBZDepartment
     */
    deptcode?: any;

    /**
     * 部门名称
     *
     * @returns {*}
     * @memberof IBZDepartment
     */
    deptname?: any;

    /**
     * 单位
     *
     * @returns {*}
     * @memberof IBZDepartment
     */
    orgid?: any;

    /**
     * 上级部门
     *
     * @returns {*}
     * @memberof IBZDepartment
     */
    parentdeptid?: any;

    /**
     * 部门简称
     *
     * @returns {*}
     * @memberof IBZDepartment
     */
    shortname?: any;

    /**
     * 部门级别
     *
     * @returns {*}
     * @memberof IBZDepartment
     */
    deptlevel?: any;

    /**
     * 区属
     *
     * @returns {*}
     * @memberof IBZDepartment
     */
    domains?: any;

    /**
     * 排序
     *
     * @returns {*}
     * @memberof IBZDepartment
     */
    showorder?: any;

    /**
     * 业务编码
     *
     * @returns {*}
     * @memberof IBZDepartment
     */
    bcode?: any;

    /**
     * 分管领导标识
     *
     * @returns {*}
     * @memberof IBZDepartment
     */
    leaderid?: any;

    /**
     * 分管领导
     *
     * @returns {*}
     * @memberof IBZDepartment
     */
    leadername?: any;

    /**
     * 逻辑有效
     *
     * @returns {*}
     * @memberof IBZDepartment
     */
    enable?: any;

    /**
     * 单位
     *
     * @returns {*}
     * @memberof IBZDepartment
     */
    orgname?: any;

    /**
     * 上级部门
     *
     * @returns {*}
     * @memberof IBZDepartment
     */
    parentdeptname?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof IBZDepartment
     */
    createdate?: any;

    /**
     * 最后修改时间
     *
     * @returns {*}
     * @memberof IBZDepartment
     */
    updatedate?: any;
}