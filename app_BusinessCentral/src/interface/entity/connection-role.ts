/**
 * 连接角色
 *
 * @export
 * @interface ConnectionRole
 */
export interface ConnectionRole {

    /**
     * 替代时间
     *
     * @returns {*}
     * @memberof ConnectionRole
     */
    overwritetime?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof ConnectionRole
     */
    updatedate?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof ConnectionRole
     */
    managed?: any;

    /**
     * 解决方案
     *
     * @returns {*}
     * @memberof ConnectionRole
     */
    supportingsolutionid?: any;

    /**
     * 组件状态
     *
     * @returns {*}
     * @memberof ConnectionRole
     */
    componentstate?: any;

    /**
     * 连接角色类别
     *
     * @returns {*}
     * @memberof ConnectionRole
     */
    category?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof ConnectionRole
     */
    createdate?: any;

    /**
     * 导入序列号
     *
     * @returns {*}
     * @memberof ConnectionRole
     */
    importsequencenumber?: any;

    /**
     * 关联角色名称
     *
     * @returns {*}
     * @memberof ConnectionRole
     */
    connectionrolename?: any;

    /**
     * 状态描述
     *
     * @returns {*}
     * @memberof ConnectionRole
     */
    statuscode?: any;

    /**
     * 连接角色
     *
     * @returns {*}
     * @memberof ConnectionRole
     */
    connectionroleid?: any;

    /**
     * 唯一 ID
     *
     * @returns {*}
     * @memberof ConnectionRole
     */
    connectionroleidunique?: any;

    /**
     * 版本号
     *
     * @returns {*}
     * @memberof ConnectionRole
     */
    versionnumber?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof ConnectionRole
     */
    updateman?: any;

    /**
     * 解决方案
     *
     * @returns {*}
     * @memberof ConnectionRole
     */
    solutionid?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof ConnectionRole
     */
    description?: any;

    /**
     * 可自定义
     *
     * @returns {*}
     * @memberof ConnectionRole
     */
    customizable?: any;

    /**
     * 引入的版本
     *
     * @returns {*}
     * @memberof ConnectionRole
     */
    introducedversion?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof ConnectionRole
     */
    createman?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof ConnectionRole
     */
    statecode?: any;
}