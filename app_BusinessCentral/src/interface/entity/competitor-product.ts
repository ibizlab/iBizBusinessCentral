/**
 * 竞争对手产品
 *
 * @export
 * @interface CompetitorProduct
 */
export interface CompetitorProduct {

    /**
     * 关系标识
     *
     * @returns {*}
     * @memberof CompetitorProduct
     */
    relationshipsid?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof CompetitorProduct
     */
    updateman?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof CompetitorProduct
     */
    createman?: any;

    /**
     * 关系名称
     *
     * @returns {*}
     * @memberof CompetitorProduct
     */
    relationshipsname?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof CompetitorProduct
     */
    updatedate?: any;

    /**
     * 关系类型
     *
     * @returns {*}
     * @memberof CompetitorProduct
     */
    relationshipstype?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof CompetitorProduct
     */
    createdate?: any;

    /**
     * 竞争对手
     *
     * @returns {*}
     * @memberof CompetitorProduct
     */
    entityname?: any;

    /**
     * 产品ID
     *
     * @returns {*}
     * @memberof CompetitorProduct
     */
    productnumber?: any;

    /**
     * 产品
     *
     * @returns {*}
     * @memberof CompetitorProduct
     */
    entity2name?: any;

    /**
     * 竞争对手
     *
     * @returns {*}
     * @memberof CompetitorProduct
     */
    entityid?: any;

    /**
     * 产品
     *
     * @returns {*}
     * @memberof CompetitorProduct
     */
    entity2id?: any;
}