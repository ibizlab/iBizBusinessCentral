/**
 * 目标
 *
 * @export
 * @interface Goal
 */
export interface Goal {

    /**
     * 过程值(十进制)
     *
     * @returns {*}
     * @memberof Goal
     */
    inprogressdecimal?: any;

    /**
     * 目标
     *
     * @returns {*}
     * @memberof Goal
     */
    goalid?: any;

    /**
     * 负责人 ID 类型
     *
     * @returns {*}
     * @memberof Goal
     */
    ownertype?: any;

    /**
     * 扩展目标值(金额)
     *
     * @returns {*}
     * @memberof Goal
     */
    stretchtargetmoney?: any;

    /**
     * 实际值(十进制)
     *
     * @returns {*}
     * @memberof Goal
     */
    actualdecimal?: any;

    /**
     * 今天的目标值(整数)
     *
     * @returns {*}
     * @memberof Goal
     */
    computedtargetasoftodayinteger?: any;

    /**
     * 时区规则版本号
     *
     * @returns {*}
     * @memberof Goal
     */
    timezoneruleversionnumber?: any;

    /**
     * 汇率
     *
     * @returns {*}
     * @memberof Goal
     */
    exchangerate?: any;

    /**
     * 实体图像 ID
     *
     * @returns {*}
     * @memberof Goal
     */
    entityimageid?: any;

    /**
     * 目标值(整数)
     *
     * @returns {*}
     * @memberof Goal
     */
    targetinteger?: any;

    /**
     * 自定义汇总字段(金额) (基础)
     *
     * @returns {*}
     * @memberof Goal
     */
    customrollupfieldmoney_base?: any;

    /**
     * EntityImage_URL
     *
     * @returns {*}
     * @memberof Goal
     */
    entityimage_url?: any;

    /**
     * 实际值(金额) (基础)
     *
     * @returns {*}
     * @memberof Goal
     */
    actualmoney_base?: any;

    /**
     * UTC 转换时区代码
     *
     * @returns {*}
     * @memberof Goal
     */
    utcconversiontimezonecode?: any;

    /**
     * 过程值(整数)
     *
     * @returns {*}
     * @memberof Goal
     */
    inprogressinteger?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof Goal
     */
    statecode?: any;

    /**
     * 目标值(金额) (基础)
     *
     * @returns {*}
     * @memberof Goal
     */
    targetmoney_base?: any;

    /**
     * 过程值
     *
     * @returns {*}
     * @memberof Goal
     */
    inprogressstring?: any;

    /**
     * EntityImage_Timestamp
     *
     * @returns {*}
     * @memberof Goal
     */
    entityimage_timestamp?: any;

    /**
     * 从
     *
     * @returns {*}
     * @memberof Goal
     */
    goalstartdate?: any;

    /**
     * 已实现百分比
     *
     * @returns {*}
     * @memberof Goal
     */
    percentage?: any;

    /**
     * 经理
     *
     * @returns {*}
     * @memberof Goal
     */
    ownerid?: any;

    /**
     * 目标值(十进制)
     *
     * @returns {*}
     * @memberof Goal
     */
    targetdecimal?: any;

    /**
     * 度量类型
     *
     * @returns {*}
     * @memberof Goal
     */
    amount?: any;

    /**
     * 状态描述
     *
     * @returns {*}
     * @memberof Goal
     */
    statuscode?: any;

    /**
     * 金额数据类型
     *
     * @returns {*}
     * @memberof Goal
     */
    amountdatatype?: any;

    /**
     * 自定义汇总字段(整数)
     *
     * @returns {*}
     * @memberof Goal
     */
    customrollupfieldinteger?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof Goal
     */
    updatedate?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof Goal
     */
    goalownername?: any;

    /**
     * 过程值(金额)
     *
     * @returns {*}
     * @memberof Goal
     */
    inprogressmoney?: any;

    /**
     * 会计年度
     *
     * @returns {*}
     * @memberof Goal
     */
    fiscalyear?: any;

    /**
     * 会计期间
     *
     * @returns {*}
     * @memberof Goal
     */
    fiscalperiod?: any;

    /**
     * 名称
     *
     * @returns {*}
     * @memberof Goal
     */
    title?: any;

    /**
     * 扩展目标值(十进制)
     *
     * @returns {*}
     * @memberof Goal
     */
    stretchtargetdecimal?: any;

    /**
     * 自定义汇总字段(十进制)
     *
     * @returns {*}
     * @memberof Goal
     */
    customrollupfielddecimal?: any;

    /**
     * 供汇总的记录集
     *
     * @returns {*}
     * @memberof Goal
     */
    consideronlygoalownersrecords?: any;

    /**
     * 创建记录的时间
     *
     * @returns {*}
     * @memberof Goal
     */
    overriddencreatedon?: any;

    /**
     * 实际值(金额)
     *
     * @returns {*}
     * @memberof Goal
     */
    actualmoney?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof Goal
     */
    ownername?: any;

    /**
     * 仅从子目标汇总
     *
     * @returns {*}
     * @memberof Goal
     */
    rolluponlyfromchildgoals?: any;

    /**
     * 到
     *
     * @returns {*}
     * @memberof Goal
     */
    goalenddate?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof Goal
     */
    createdate?: any;

    /**
     * 自定义汇总字段(金额)
     *
     * @returns {*}
     * @memberof Goal
     */
    customrollupfieldmoney?: any;

    /**
     * 汇总错误代码
     *
     * @returns {*}
     * @memberof Goal
     */
    rolluperrorcode?: any;

    /**
     * 版本号
     *
     * @returns {*}
     * @memberof Goal
     */
    versionnumber?: any;

    /**
     * 扩展目标值(金额) (基础)
     *
     * @returns {*}
     * @memberof Goal
     */
    stretchtargetmoney_base?: any;

    /**
     * 树 ID
     *
     * @returns {*}
     * @memberof Goal
     */
    treeid?: any;

    /**
     * 目标负责人类型
     *
     * @returns {*}
     * @memberof Goal
     */
    goalownertype?: any;

    /**
     * 实体图像
     *
     * @returns {*}
     * @memberof Goal
     */
    entityimage?: any;

    /**
     * 已扩展目标值
     *
     * @returns {*}
     * @memberof Goal
     */
    stretchtargetstring?: any;

    /**
     * 扩展目标值(整数)
     *
     * @returns {*}
     * @memberof Goal
     */
    stretchtargetinteger?: any;

    /**
     * 目标期间类型
     *
     * @returns {*}
     * @memberof Goal
     */
    fiscalperiodgoal?: any;

    /**
     * 过程值(金额) (基础)
     *
     * @returns {*}
     * @memberof Goal
     */
    inprogressmoney_base?: any;

    /**
     * 已替代
     *
     * @returns {*}
     * @memberof Goal
     */
    overridden?: any;

    /**
     * 实际
     *
     * @returns {*}
     * @memberof Goal
     */
    actualstring?: any;

    /**
     * 实际值(整数)
     *
     * @returns {*}
     * @memberof Goal
     */
    actualinteger?: any;

    /**
     * 导入序列号
     *
     * @returns {*}
     * @memberof Goal
     */
    importsequencenumber?: any;

    /**
     * 目标负责人
     *
     * @returns {*}
     * @memberof Goal
     */
    goalownerid?: any;

    /**
     * 今天的目标值(十进制)
     *
     * @returns {*}
     * @memberof Goal
     */
    computedtargetasoftodaydecimal?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof Goal
     */
    createman?: any;

    /**
     * 目标
     *
     * @returns {*}
     * @memberof Goal
     */
    targetstring?: any;

    /**
     * 自定义汇总字段
     *
     * @returns {*}
     * @memberof Goal
     */
    customrollupfieldstring?: any;

    /**
     * 目标值(金额)
     *
     * @returns {*}
     * @memberof Goal
     */
    targetmoney?: any;

    /**
     * 上次汇总日期
     *
     * @returns {*}
     * @memberof Goal
     */
    lastrolledupdate?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof Goal
     */
    updateman?: any;

    /**
     * 替代
     *
     * @returns {*}
     * @memberof Goal
     */
    override?: any;

    /**
     * 今天的目标值(金额)
     *
     * @returns {*}
     * @memberof Goal
     */
    computedtargetasoftodaymoney?: any;

    /**
     * 目标度量
     *
     * @returns {*}
     * @memberof Goal
     */
    metricname?: any;

    /**
     * 上级目标
     *
     * @returns {*}
     * @memberof Goal
     */
    parentgoalname?: any;

    /**
     * 上级目标
     *
     * @returns {*}
     * @memberof Goal
     */
    parentgoalid?: any;

    /**
     * 汇总查询 - 过程值(金额)
     *
     * @returns {*}
     * @memberof Goal
     */
    rollupqueryinprogressmoneyid?: any;

    /**
     * 汇总查询 - 自定义汇总字段(金额)
     *
     * @returns {*}
     * @memberof Goal
     */
    rollupquerycustommoneyid?: any;

    /**
     * 汇总查询 - 过程值(整数)
     *
     * @returns {*}
     * @memberof Goal
     */
    rollupqueryinprogressintegerid?: any;

    /**
     * 汇总查询 - 自定义汇总字段(十进制)
     *
     * @returns {*}
     * @memberof Goal
     */
    rollupquerycustomdecimalid?: any;

    /**
     * 汇总查询 - 实际值(金额)
     *
     * @returns {*}
     * @memberof Goal
     */
    rollupqueryactualmoneyid?: any;

    /**
     * 汇总查询 - 自定义汇总字段(整数)
     *
     * @returns {*}
     * @memberof Goal
     */
    rollupquerycustomintegerid?: any;

    /**
     * 出现错误的目标
     *
     * @returns {*}
     * @memberof Goal
     */
    goalwitherrorid?: any;

    /**
     * 目标度量
     *
     * @returns {*}
     * @memberof Goal
     */
    metricid?: any;

    /**
     * 汇总查询 - 过程值(十进制)
     *
     * @returns {*}
     * @memberof Goal
     */
    rollupqueryinprogressdecimalid?: any;

    /**
     * 汇总查询 - 实际值(整数)
     *
     * @returns {*}
     * @memberof Goal
     */
    rollupqueryactualintegerid?: any;

    /**
     * 汇总查询 - 实际值(十进制)
     *
     * @returns {*}
     * @memberof Goal
     */
    rollupqueryactualdecimalid?: any;
}