/**
 * 折扣表
 *
 * @export
 * @interface DiscountType
 */
export interface DiscountType {

    /**
     * Time Zone Rule Version Number
     *
     * @returns {*}
     * @memberof DiscountType
     */
    timezoneruleversionnumber?: any;

    /**
     * 状态描述
     *
     * @returns {*}
     * @memberof DiscountType
     */
    statuscode?: any;

    /**
     * 折扣表
     *
     * @returns {*}
     * @memberof DiscountType
     */
    discounttypeid?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof DiscountType
     */
    createdate?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof DiscountType
     */
    updateman?: any;

    /**
     * 类型
     *
     * @returns {*}
     * @memberof DiscountType
     */
    amounttype?: any;

    /**
     * Version Number
     *
     * @returns {*}
     * @memberof DiscountType
     */
    versionnumber?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof DiscountType
     */
    createman?: any;

    /**
     * 折扣类型名称
     *
     * @returns {*}
     * @memberof DiscountType
     */
    discounttypename?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof DiscountType
     */
    updatedate?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof DiscountType
     */
    description?: any;

    /**
     * UTC Conversion Time Zone Code
     *
     * @returns {*}
     * @memberof DiscountType
     */
    utcconversiontimezonecode?: any;

    /**
     * Record Created On
     *
     * @returns {*}
     * @memberof DiscountType
     */
    overriddencreatedon?: any;

    /**
     * Import Sequence Number
     *
     * @returns {*}
     * @memberof DiscountType
     */
    importsequencenumber?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof DiscountType
     */
    statecode?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof DiscountType
     */
    transactioncurrencyid?: any;
}