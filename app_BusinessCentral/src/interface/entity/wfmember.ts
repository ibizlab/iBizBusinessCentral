/**
 * 成员
 *
 * @export
 * @interface WFMember
 */
export interface WFMember {

    /**
     * 组成员标识
     *
     * @returns {*}
     * @memberof WFMember
     */
    memberid?: any;

    /**
     * 组成员名称
     *
     * @returns {*}
     * @memberof WFMember
     */
    membername?: any;

    /**
     * 组标识
     *
     * @returns {*}
     * @memberof WFMember
     */
    groupid?: any;

    /**
     * 用户组
     *
     * @returns {*}
     * @memberof WFMember
     */
    groupname?: any;

    /**
     * 用户标识
     *
     * @returns {*}
     * @memberof WFMember
     */
    userid?: any;

    /**
     * 用户
     *
     * @returns {*}
     * @memberof WFMember
     */
    personname?: any;

    /**
     * 单位
     *
     * @returns {*}
     * @memberof WFMember
     */
    orgid?: any;

    /**
     * 单位
     *
     * @returns {*}
     * @memberof WFMember
     */
    orgname?: any;

    /**
     * 主部门
     *
     * @returns {*}
     * @memberof WFMember
     */
    mdeptid?: any;

    /**
     * 主部门
     *
     * @returns {*}
     * @memberof WFMember
     */
    mdeptname?: any;
}