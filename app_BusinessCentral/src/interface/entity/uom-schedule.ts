/**
 * 计价单位组
 *
 * @export
 * @interface UomSchedule
 */
export interface UomSchedule {

    /**
     * Import Sequence Number
     *
     * @returns {*}
     * @memberof UomSchedule
     */
    importsequencenumber?: any;

    /**
     * Time Zone Rule Version Number
     *
     * @returns {*}
     * @memberof UomSchedule
     */
    timezoneruleversionnumber?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof UomSchedule
     */
    createman?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof UomSchedule
     */
    description?: any;

    /**
     * 基础单位名称
     *
     * @returns {*}
     * @memberof UomSchedule
     */
    baseuomname?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof UomSchedule
     */
    updateman?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof UomSchedule
     */
    updatedate?: any;

    /**
     * Version Number
     *
     * @returns {*}
     * @memberof UomSchedule
     */
    versionnumber?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof UomSchedule
     */
    createdate?: any;

    /**
     * UTC Conversion Time Zone Code
     *
     * @returns {*}
     * @memberof UomSchedule
     */
    utcconversiontimezonecode?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof UomSchedule
     */
    statecode?: any;

    /**
     * 状态描述
     *
     * @returns {*}
     * @memberof UomSchedule
     */
    statuscode?: any;

    /**
     * Record Created On
     *
     * @returns {*}
     * @memberof UomSchedule
     */
    overriddencreatedon?: any;

    /**
     * 单位进度表名称
     *
     * @returns {*}
     * @memberof UomSchedule
     */
    uomschedulename?: any;

    /**
     * 计价单位组
     *
     * @returns {*}
     * @memberof UomSchedule
     */
    uomscheduleid?: any;
}