/**
 * 任务调度日志
 *
 * @export
 * @interface JobsLog
 */
export interface JobsLog {

    /**
     * 主键ID
     *
     * @returns {*}
     * @memberof JobsLog
     */
    id?: any;

    /**
     * 任务ID
     *
     * @returns {*}
     * @memberof JobsLog
     */
    job_id?: any;

    /**
     * 执行地址
     *
     * @returns {*}
     * @memberof JobsLog
     */
    address?: any;

    /**
     * 执行器任务HANDLER
     *
     * @returns {*}
     * @memberof JobsLog
     */
    handler?: any;

    /**
     * 执行器任务参数
     *
     * @returns {*}
     * @memberof JobsLog
     */
    param?: any;

    /**
     * 失败重试次数
     *
     * @returns {*}
     * @memberof JobsLog
     */
    fail_retry_count?: any;

    /**
     * 触发器调度返回码
     *
     * @returns {*}
     * @memberof JobsLog
     */
    trigger_code?: any;

    /**
     * 触发器调度类型
     *
     * @returns {*}
     * @memberof JobsLog
     */
    trigger_type?: any;

    /**
     * 触发器调度信息
     *
     * @returns {*}
     * @memberof JobsLog
     */
    trigger_msg?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof JobsLog
     */
    create_time?: any;
}