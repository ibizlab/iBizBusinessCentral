/**
 * 人员
 *
 * @export
 * @interface IBZEmployee
 */
export interface IBZEmployee {

    /**
     * 用户标识
     *
     * @returns {*}
     * @memberof IBZEmployee
     */
    userid?: any;

    /**
     * 用户全局名
     *
     * @returns {*}
     * @memberof IBZEmployee
     */
    username?: any;

    /**
     * 姓名
     *
     * @returns {*}
     * @memberof IBZEmployee
     */
    personname?: any;

    /**
     * 用户工号
     *
     * @returns {*}
     * @memberof IBZEmployee
     */
    usercode?: any;

    /**
     * 登录名
     *
     * @returns {*}
     * @memberof IBZEmployee
     */
    loginname?: any;

    /**
     * 密码
     *
     * @returns {*}
     * @memberof IBZEmployee
     */
    password?: any;

    /**
     * 区属
     *
     * @returns {*}
     * @memberof IBZEmployee
     */
    domains?: any;

    /**
     * 主部门
     *
     * @returns {*}
     * @memberof IBZEmployee
     */
    mdeptid?: any;

    /**
     * 主部门代码
     *
     * @returns {*}
     * @memberof IBZEmployee
     */
    mdeptcode?: any;

    /**
     * 主部门名称
     *
     * @returns {*}
     * @memberof IBZEmployee
     */
    mdeptname?: any;

    /**
     * 业务编码
     *
     * @returns {*}
     * @memberof IBZEmployee
     */
    bcode?: any;

    /**
     * 岗位标识
     *
     * @returns {*}
     * @memberof IBZEmployee
     */
    postid?: any;

    /**
     * 岗位代码
     *
     * @returns {*}
     * @memberof IBZEmployee
     */
    postcode?: any;

    /**
     * 岗位名称
     *
     * @returns {*}
     * @memberof IBZEmployee
     */
    postname?: any;

    /**
     * 单位
     *
     * @returns {*}
     * @memberof IBZEmployee
     */
    orgid?: any;

    /**
     * 单位代码
     *
     * @returns {*}
     * @memberof IBZEmployee
     */
    orgcode?: any;

    /**
     * 单位名称
     *
     * @returns {*}
     * @memberof IBZEmployee
     */
    orgname?: any;

    /**
     * 昵称别名
     *
     * @returns {*}
     * @memberof IBZEmployee
     */
    nickname?: any;

    /**
     * 性别
     *
     * @returns {*}
     * @memberof IBZEmployee
     */
    sex?: any;

    /**
     * 证件号码
     *
     * @returns {*}
     * @memberof IBZEmployee
     */
    certcode?: any;

    /**
     * 联系方式
     *
     * @returns {*}
     * @memberof IBZEmployee
     */
    phone?: any;

    /**
     * 出生日期
     *
     * @returns {*}
     * @memberof IBZEmployee
     */
    birthday?: any;

    /**
     * 邮件
     *
     * @returns {*}
     * @memberof IBZEmployee
     */
    email?: any;

    /**
     * 社交账号
     *
     * @returns {*}
     * @memberof IBZEmployee
     */
    avatar?: any;

    /**
     * 地址
     *
     * @returns {*}
     * @memberof IBZEmployee
     */
    addr?: any;

    /**
     * 照片
     *
     * @returns {*}
     * @memberof IBZEmployee
     */
    usericon?: any;

    /**
     * ip地址
     *
     * @returns {*}
     * @memberof IBZEmployee
     */
    ipaddr?: any;

    /**
     * 样式
     *
     * @returns {*}
     * @memberof IBZEmployee
     */
    theme?: any;

    /**
     * 语言
     *
     * @returns {*}
     * @memberof IBZEmployee
     */
    lang?: any;

    /**
     * 字号
     *
     * @returns {*}
     * @memberof IBZEmployee
     */
    fontsize?: any;

    /**
     * 备注
     *
     * @returns {*}
     * @memberof IBZEmployee
     */
    memo?: any;

    /**
     * 保留
     *
     * @returns {*}
     * @memberof IBZEmployee
     */
    reserver?: any;

    /**
     * 排序
     *
     * @returns {*}
     * @memberof IBZEmployee
     */
    showorder?: any;

    /**
     * 逻辑有效
     *
     * @returns {*}
     * @memberof IBZEmployee
     */
    enable?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof IBZEmployee
     */
    createdate?: any;

    /**
     * 最后修改时间
     *
     * @returns {*}
     * @memberof IBZEmployee
     */
    updatedate?: any;
}