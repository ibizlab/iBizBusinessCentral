/**
 * 活动
 *
 * @export
 * @interface ActivityPointer
 */
export interface ActivityPointer {

    /**
     * 系列 ID
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    seriesid?: any;

    /**
     * 遍历的路径
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    traversedpath?: any;

    /**
     * 传递优先级
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    deliveryprioritycode?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    ownername?: any;

    /**
     * 暂候时间(分钟)
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    onholdtime?: any;

    /**
     * 由工作流创建
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    workflowcreated?: any;

    /**
     * 发送日期
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    senton?: any;

    /**
     * 上一暂候时间
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    lastonholdtime?: any;

    /**
     * 实际持续时间
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    actualdurationminutes?: any;

    /**
     * 关于
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    regardingobjectid?: any;

    /**
     * 上次尝试传递的日期
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    deliverylastattemptedon?: any;

    /**
     * 隐藏
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    mapiprivate?: any;

    /**
     * 实际结束时间
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    actualend?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    description?: any;

    /**
     * 优先级
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    prioritycode?: any;

    /**
     * 已记帐
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    billed?: any;

    /**
     * 是定期活动
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    regularactivity?: any;

    /**
     * UTC 转换时区代码
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    utcconversiontimezonecode?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    createman?: any;

    /**
     * 计划持续时间
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    scheduleddurationminutes?: any;

    /**
     * 活动附加参数
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    activityadditionalparams?: any;

    /**
     * 活动
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    activityid?: any;

    /**
     * 关于
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    regardingobjectname?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    updatedate?: any;

    /**
     * 流程阶段
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    stageid?: any;

    /**
     * 保留的语音邮件
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    leftvoicemail?: any;

    /**
     * 截止日期
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    scheduledend?: any;

    /**
     * Exchange 项目 ID
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    exchangeitemid?: any;

    /**
     * 汇率
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    exchangerate?: any;

    /**
     * 开始日期
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    scheduledstart?: any;

    /**
     * 定期实例类型
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    instancetypecode?: any;

    /**
     * RegardingObjectTypeCode
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    regardingobjecttypecode?: any;

    /**
     * 排序日期
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    sortdate?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    createdate?: any;

    /**
     * 社交渠道
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    community?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    ownerid?: any;

    /**
     * 流程
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    processid?: any;

    /**
     * SLAName
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    slaname?: any;

    /**
     * 主题
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    subject?: any;

    /**
     * 版本号
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    versionnumber?: any;

    /**
     * 所有活动方
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    allparties?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    updateman?: any;

    /**
     * Exchange 链接
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    exchangeweblink?: any;

    /**
     * 状态描述
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    statuscode?: any;

    /**
     * 时区规则版本号
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    timezoneruleversionnumber?: any;

    /**
     * 实际开始时间
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    actualstart?: any;

    /**
     * 活动类型
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    activitytypecode?: any;

    /**
     * 关于
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    regardingobjectidname?: any;

    /**
     * 所有者类型
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    ownertype?: any;

    /**
     * 活动状态
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    statecode?: any;

    /**
     * 服务
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    serviceid?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    transactioncurrencyid?: any;

    /**
     * SLA
     *
     * @returns {*}
     * @memberof ActivityPointer
     */
    slaid?: any;
}