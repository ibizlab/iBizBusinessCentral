/**
 * 组织层次结构分配
 *
 * @export
 * @interface OMHierarchyPurposeRef
 */
export interface OMHierarchyPurposeRef {

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof OMHierarchyPurposeRef
     */
    updateman?: any;

    /**
     * 组织层次结构分配名称
     *
     * @returns {*}
     * @memberof OMHierarchyPurposeRef
     */
    omhierarchypurposerefname?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof OMHierarchyPurposeRef
     */
    createdate?: any;

    /**
     * 组织层次结构分配标识
     *
     * @returns {*}
     * @memberof OMHierarchyPurposeRef
     */
    omhierarchypurposerefid?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof OMHierarchyPurposeRef
     */
    updatedate?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof OMHierarchyPurposeRef
     */
    createman?: any;

    /**
     * 结构层次类别标识
     *
     * @returns {*}
     * @memberof OMHierarchyPurposeRef
     */
    omhierarchycatid?: any;

    /**
     * 组织层次机构目的标识
     *
     * @returns {*}
     * @memberof OMHierarchyPurposeRef
     */
    omhierarchypurposeid?: any;

    /**
     * 是否默认
     *
     * @returns {*}
     * @memberof OMHierarchyPurposeRef
     */
    isdefault?: any;

    /**
     * 结束日期
     *
     * @returns {*}
     * @memberof OMHierarchyPurposeRef
     */
    enddate?: any;

    /**
     * 开始日期
     *
     * @returns {*}
     * @memberof OMHierarchyPurposeRef
     */
    begindate?: any;
}