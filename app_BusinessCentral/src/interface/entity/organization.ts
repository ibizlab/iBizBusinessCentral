/**
 * 组织
 *
 * @export
 * @interface Organization
 */
export interface Organization {

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof Organization
     */
    updateman?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof Organization
     */
    updatedate?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof Organization
     */
    createman?: any;

    /**
     * 组织名称
     *
     * @returns {*}
     * @memberof Organization
     */
    organizationname?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof Organization
     */
    createdate?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof Organization
     */
    organizationid?: any;

    /**
     * 组织类型
     *
     * @returns {*}
     * @memberof Organization
     */
    organizationtype?: any;

    /**
     * 组织简称
     *
     * @returns {*}
     * @memberof Organization
     */
    shortname?: any;

    /**
     * 组织层级
     *
     * @returns {*}
     * @memberof Organization
     */
    orglevel?: any;

    /**
     * 组织编码
     *
     * @returns {*}
     * @memberof Organization
     */
    orgcode?: any;

    /**
     * 排序号
     *
     * @returns {*}
     * @memberof Organization
     */
    showorder?: any;
}