/**
 * 知识文章
 *
 * @export
 * @interface KnowledgeArticle
 */
export interface KnowledgeArticle {

    /**
     * 知识文章视图(上次更新时间)
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    knowledgearticleviews_date?: any;

    /**
     * 关键字
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    keywords?: any;

    /**
     * 状态描述
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    statuscode?: any;

    /**
     * 过期状态 ID
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    expirationstateid?: any;

    /**
     * 主要文章
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    isprimary?: any;

    /**
     * Rating(Count)
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    rating_count?: any;

    /**
     * 内容
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    content?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    ownerid?: any;

    /**
     * RootArticled
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    rootarticled?: any;

    /**
     * 知识文章视图(状态)
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    knowledgearticleviews_state?: any;

    /**
     * Rating(sum)
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    rating_sum?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    statecode?: any;

    /**
     * 遍历的路径
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    traversedpath?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    updateman?: any;

    /**
     * 已发布状态
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    publishstatusid?: any;

    /**
     * 负责人 ID 类型
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    ownertype?: any;

    /**
     * 文章公共编号
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    articlepublicnumber?: any;

    /**
     * primaryauthoridName
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    primaryauthoridname?: any;

    /**
     * 记录创建日期
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    overriddencreatedon?: any;

    /**
     * 主要作者 ID
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    primaryauthorid?: any;

    /**
     * 是最新版本
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    latestversion?: any;

    /**
     * 评分(状态)
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    rating_state?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    createdate?: any;

    /**
     * VersionNumber
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    versionnumber?: any;

    /**
     * UTC 转换时区代码
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    utcconversiontimezonecode?: any;

    /**
     * 已过期状态
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    expirationstatusid?: any;

    /**
     * 知识文章
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    knowledgearticleid?: any;

    /**
     * 设置类别关联
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    setcategoryassociations?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    description?: any;

    /**
     * 设置产品关联
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    setproductassociations?: any;

    /**
     * 次要版本号
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    minorversionnumber?: any;

    /**
     * 阶段 ID
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    stageid?: any;

    /**
     * 进程 ID
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    processid?: any;

    /**
     * 主要版本号
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    majorversionnumber?: any;

    /**
     * 评分
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    rating?: any;

    /**
     * 发布日期
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    publishon?: any;

    /**
     * 知识文章视图
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    knowledgearticleviews?: any;

    /**
     * 评分(上次更新时间)
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    rating_date?: any;

    /**
     * 汇率
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    exchangerate?: any;

    /**
     * 为审阅做准备
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    readyforreview?: any;

    /**
     * 导入序号
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    importsequencenumber?: any;

    /**
     * SubjectIdDsc
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    subjectiddsc?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    ownername?: any;

    /**
     * 审阅
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    review?: any;

    /**
     * LanguageLocaleIdLocaleId
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    languagelocaleidlocaleid?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    updatedate?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    createman?: any;

    /**
     * 已计划状态
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    scheduledstatusid?: any;

    /**
     * 时区规则版本号
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    timezoneruleversionnumber?: any;

    /**
     * 内部
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    internal?: any;

    /**
     * 标题
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    title?: any;

    /**
     * 更新内容
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    updatecontent?: any;

    /**
     * 到期日期
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    expirationdate?: any;

    /**
     * 已过期审阅选项
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    expiredreviewoptions?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    currencyname?: any;

    /**
     * 上级文章
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    parentarticlecontentname?: any;

    /**
     * 根文章
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    rootarticlename?: any;

    /**
     * 主题
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    subjectname?: any;

    /**
     * 语言
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    languagelocalename?: any;

    /**
     * 上篇文章
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    previousarticlecontentname?: any;

    /**
     * 上级文章
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    parentarticlecontentid?: any;

    /**
     * 上篇文章内容 ID
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    previousarticlecontentid?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    transactioncurrencyid?: any;

    /**
     * 根文章 ID
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    rootarticleid?: any;

    /**
     * 主题
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    subjectid?: any;

    /**
     * 语言
     *
     * @returns {*}
     * @memberof KnowledgeArticle
     */
    languagelocaleid?: any;
}