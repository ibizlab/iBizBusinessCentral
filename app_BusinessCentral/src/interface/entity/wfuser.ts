/**
 * 用户
 *
 * @export
 * @interface WFUser
 */
export interface WFUser {

    /**
     * 用户标识
     *
     * @returns {*}
     * @memberof WFUser
     */
    id?: any;

    /**
     * 用户全局名
     *
     * @returns {*}
     * @memberof WFUser
     */
    firstname?: any;

    /**
     * 用户名称
     *
     * @returns {*}
     * @memberof WFUser
     */
    displayname?: any;

    /**
     * 主部门
     *
     * @returns {*}
     * @memberof WFUser
     */
    mdeptid?: any;

    /**
     * 主部门代码
     *
     * @returns {*}
     * @memberof WFUser
     */
    mdeptcode?: any;

    /**
     * 主部门名称
     *
     * @returns {*}
     * @memberof WFUser
     */
    mdeptname?: any;

    /**
     * 业务编码
     *
     * @returns {*}
     * @memberof WFUser
     */
    bcode?: any;

    /**
     * 单位
     *
     * @returns {*}
     * @memberof WFUser
     */
    orgid?: any;

    /**
     * 单位代码
     *
     * @returns {*}
     * @memberof WFUser
     */
    orgcode?: any;

    /**
     * 单位名称
     *
     * @returns {*}
     * @memberof WFUser
     */
    orgname?: any;
}