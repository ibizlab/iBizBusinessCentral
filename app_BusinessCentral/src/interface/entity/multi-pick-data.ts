/**
 * 多类选择实体
 *
 * @export
 * @interface MultiPickData
 */
export interface MultiPickData {

    /**
     * 信息描述
     *
     * @returns {*}
     * @memberof MultiPickData
     */
    pickdatainfo?: any;

    /**
     * 用户数据
     *
     * @returns {*}
     * @memberof MultiPickData
     */
    userdata?: any;

    /**
     * 数据名称
     *
     * @returns {*}
     * @memberof MultiPickData
     */
    pickdataname?: any;

    /**
     * 数据类型
     *
     * @returns {*}
     * @memberof MultiPickData
     */
    pickdatatype?: any;

    /**
     * 用户时间2
     *
     * @returns {*}
     * @memberof MultiPickData
     */
    userdate2?: any;

    /**
     * 用户时间
     *
     * @returns {*}
     * @memberof MultiPickData
     */
    userdate?: any;

    /**
     * 数据标识
     *
     * @returns {*}
     * @memberof MultiPickData
     */
    pickdataid?: any;

    /**
     * 用户数据4
     *
     * @returns {*}
     * @memberof MultiPickData
     */
    userdata4?: any;

    /**
     * 用户数据2
     *
     * @returns {*}
     * @memberof MultiPickData
     */
    userdata2?: any;

    /**
     * 用户数据3
     *
     * @returns {*}
     * @memberof MultiPickData
     */
    userdata3?: any;
}