/**
 * 潜在顾客
 *
 * @export
 * @interface Lead
 */
export interface Lead {

    /**
     * 地址 1: 传真
     *
     * @returns {*}
     * @memberof Lead
     */
    address1_fax?: any;

    /**
     * 地址 2: UTC 时差
     *
     * @returns {*}
     * @memberof Lead
     */
    address2_utcoffset?: any;

    /**
     * 职务
     *
     * @returns {*}
     * @memberof Lead
     */
    jobtitle?: any;

    /**
     * 地址 2: 国家/地区
     *
     * @returns {*}
     * @memberof Lead
     */
    address2_country?: any;

    /**
     * 预算金额
     *
     * @returns {*}
     * @memberof Lead
     */
    budgetamount?: any;

    /**
     * 地址 2: 传真
     *
     * @returns {*}
     * @memberof Lead
     */
    address2_fax?: any;

    /**
     * 暂候时间(分钟)
     *
     * @returns {*}
     * @memberof Lead
     */
    onholdtime?: any;

    /**
     * 姓
     *
     * @returns {*}
     * @memberof Lead
     */
    lastname?: any;

    /**
     * 地址 1: 电话 2
     *
     * @returns {*}
     * @memberof Lead
     */
    address1_telephone2?: any;

    /**
     * 省/直辖市/自治区
     *
     * @returns {*}
     * @memberof Lead
     */
    address1_stateorprovince?: any;

    /**
     * 预计值(已弃用)
     *
     * @returns {*}
     * @memberof Lead
     */
    estimatedvalue?: any;

    /**
     * 潜在顾客
     *
     * @returns {*}
     * @memberof Lead
     */
    leadid?: any;

    /**
     * 地址 1: 经度
     *
     * @returns {*}
     * @memberof Lead
     */
    address1_longitude?: any;

    /**
     * 街道 1
     *
     * @returns {*}
     * @memberof Lead
     */
    address1_line1?: any;

    /**
     * 等级
     *
     * @returns {*}
     * @memberof Lead
     */
    leadqualitycode?: any;

    /**
     * 不允许电话联络
     *
     * @returns {*}
     * @memberof Lead
     */
    donotphone?: any;

    /**
     * 汇率
     *
     * @returns {*}
     * @memberof Lead
     */
    exchangerate?: any;

    /**
     * EntityImage_URL
     *
     * @returns {*}
     * @memberof Lead
     */
    entityimage_url?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof Lead
     */
    createman?: any;

    /**
     * 地址 2: 省/市/自治区
     *
     * @returns {*}
     * @memberof Lead
     */
    address2_stateorprovince?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof Lead
     */
    description?: any;

    /**
     * 员工数
     *
     * @returns {*}
     * @memberof Lead
     */
    numberofemployees?: any;

    /**
     * 市/县
     *
     * @returns {*}
     * @memberof Lead
     */
    address1_city?: any;

    /**
     * EntityImageId
     *
     * @returns {*}
     * @memberof Lead
     */
    entityimageid?: any;

    /**
     * 地址 2: 街道 3
     *
     * @returns {*}
     * @memberof Lead
     */
    address2_line3?: any;

    /**
     * 状态描述
     *
     * @returns {*}
     * @memberof Lead
     */
    statuscode?: any;

    /**
     * 地址 2: 街道 1
     *
     * @returns {*}
     * @memberof Lead
     */
    address2_line1?: any;

    /**
     * 客户
     *
     * @returns {*}
     * @memberof Lead
     */
    customerid?: any;

    /**
     * 公司名称
     *
     * @returns {*}
     * @memberof Lead
     */
    companyname?: any;

    /**
     * Version Number
     *
     * @returns {*}
     * @memberof Lead
     */
    versionnumber?: any;

    /**
     * 资格注释
     *
     * @returns {*}
     * @memberof Lead
     */
    qualificationcomments?: any;

    /**
     * 地址 2: 名称
     *
     * @returns {*}
     * @memberof Lead
     */
    address2_name?: any;

    /**
     * 电子邮件
     *
     * @returns {*}
     * @memberof Lead
     */
    emailaddress1?: any;

    /**
     * 跟踪电子邮件活动
     *
     * @returns {*}
     * @memberof Lead
     */
    followemail?: any;

    /**
     * 国家/地区
     *
     * @returns {*}
     * @memberof Lead
     */
    address1_country?: any;

    /**
     * 网站
     *
     * @returns {*}
     * @memberof Lead
     */
    websiteurl?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof Lead
     */
    updatedate?: any;

    /**
     * 街道 3
     *
     * @returns {*}
     * @memberof Lead
     */
    address1_line3?: any;

    /**
     * 地址 2: ID
     *
     * @returns {*}
     * @memberof Lead
     */
    address2_addressid?: any;

    /**
     * 地址 1
     *
     * @returns {*}
     * @memberof Lead
     */
    address1_composite?: any;

    /**
     * 主题
     *
     * @returns {*}
     * @memberof Lead
     */
    subject?: any;

    /**
     * 地址 1: UTC 时差
     *
     * @returns {*}
     * @memberof Lead
     */
    address1_utcoffset?: any;

    /**
     * 预算金额 (Base)
     *
     * @returns {*}
     * @memberof Lead
     */
    budgetamount_base?: any;

    /**
     * 地址 1: 地址类型
     *
     * @returns {*}
     * @memberof Lead
     */
    address1_addresstypecode?: any;

    /**
     * 地址 2: 电话 3
     *
     * @returns {*}
     * @memberof Lead
     */
    address2_telephone3?: any;

    /**
     * 客户
     *
     * @returns {*}
     * @memberof Lead
     */
    customername?: any;

    /**
     * 潜在客户
     *
     * @returns {*}
     * @memberof Lead
     */
    masterleadname?: any;

    /**
     * 评估相符程度
     *
     * @returns {*}
     * @memberof Lead
     */
    evaluatefit?: any;

    /**
     * 地址 1: 纬度
     *
     * @returns {*}
     * @memberof Lead
     */
    address1_latitude?: any;

    /**
     * 地址 1: 电话 3
     *
     * @returns {*}
     * @memberof Lead
     */
    address1_telephone3?: any;

    /**
     * 姓名
     *
     * @returns {*}
     * @memberof Lead
     */
    fullname?: any;

    /**
     * 预计值 (Base)
     *
     * @returns {*}
     * @memberof Lead
     */
    estimatedamount_base?: any;

    /**
     * 预算
     *
     * @returns {*}
     * @memberof Lead
     */
    budgetstatus?: any;

    /**
     * 行业
     *
     * @returns {*}
     * @memberof Lead
     */
    industrycode?: any;

    /**
     * 街道 2
     *
     * @returns {*}
     * @memberof Lead
     */
    address1_line2?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof Lead
     */
    ownername?: any;

    /**
     * 初始通信
     *
     * @returns {*}
     * @memberof Lead
     */
    initialcommunication?: any;

    /**
     * 地址 1: 邮政信箱
     *
     * @returns {*}
     * @memberof Lead
     */
    address1_postofficebox?: any;

    /**
     * 地址 1: 电话 1
     *
     * @returns {*}
     * @memberof Lead
     */
    address1_telephone1?: any;

    /**
     * 年收入
     *
     * @returns {*}
     * @memberof Lead
     */
    revenue?: any;

    /**
     * 地址 2: 县
     *
     * @returns {*}
     * @memberof Lead
     */
    address2_county?: any;

    /**
     * Stage Id
     *
     * @returns {*}
     * @memberof Lead
     */
    stageid?: any;

    /**
     * 地址 1: 送货方式
     *
     * @returns {*}
     * @memberof Lead
     */
    address1_shippingmethodcode?: any;

    /**
     * 预计值
     *
     * @returns {*}
     * @memberof Lead
     */
    estimatedamount?: any;

    /**
     * 地址 1: 县
     *
     * @returns {*}
     * @memberof Lead
     */
    address1_county?: any;

    /**
     * UTC Conversion Time Zone Code
     *
     * @returns {*}
     * @memberof Lead
     */
    utcconversiontimezonecode?: any;

    /**
     * 上一暂候时间
     *
     * @returns {*}
     * @memberof Lead
     */
    lastonholdtime?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof Lead
     */
    createdate?: any;

    /**
     * TeamsFollowed
     *
     * @returns {*}
     * @memberof Lead
     */
    teamsfollowed?: any;

    /**
     * 称呼
     *
     * @returns {*}
     * @memberof Lead
     */
    salutation?: any;

    /**
     * 地址 2: 送货方式
     *
     * @returns {*}
     * @memberof Lead
     */
    address2_shippingmethodcode?: any;

    /**
     * 地址 2: 纬度
     *
     * @returns {*}
     * @memberof Lead
     */
    address2_latitude?: any;

    /**
     * 参与工作流
     *
     * @returns {*}
     * @memberof Lead
     */
    participatesinworkflow?: any;

    /**
     * 地址 2
     *
     * @returns {*}
     * @memberof Lead
     */
    address2_composite?: any;

    /**
     * 销售阶段
     *
     * @returns {*}
     * @memberof Lead
     */
    salesstage?: any;

    /**
     * 不允许使用邮件
     *
     * @returns {*}
     * @memberof Lead
     */
    donotpostalmail?: any;

    /**
     * 年收入 (Base)
     *
     * @returns {*}
     * @memberof Lead
     */
    revenue_base?: any;

    /**
     * Record Created On
     *
     * @returns {*}
     * @memberof Lead
     */
    overriddencreatedon?: any;

    /**
     * 安排跟进(潜在客户)
     *
     * @returns {*}
     * @memberof Lead
     */
    schedulefollowup_prospect?: any;

    /**
     * 地址 2: 市/县
     *
     * @returns {*}
     * @memberof Lead
     */
    address2_city?: any;

    /**
     * 商务电话
     *
     * @returns {*}
     * @memberof Lead
     */
    telephone1?: any;

    /**
     * 是否私有
     *
     * @returns {*}
     * @memberof Lead
     */
    ibizprivate?: any;

    /**
     * 移动电话
     *
     * @returns {*}
     * @memberof Lead
     */
    mobilephone?: any;

    /**
     * 需求
     *
     * @returns {*}
     * @memberof Lead
     */
    need?: any;

    /**
     * 优先级
     *
     * @returns {*}
     * @memberof Lead
     */
    prioritycode?: any;

    /**
     * 地址 1: ID
     *
     * @returns {*}
     * @memberof Lead
     */
    address1_addressid?: any;

    /**
     * 销售阶段代码
     *
     * @returns {*}
     * @memberof Lead
     */
    salesstagecode?: any;

    /**
     * Import Sequence Number
     *
     * @returns {*}
     * @memberof Lead
     */
    importsequencenumber?: any;

    /**
     * 地址 2: 电话 1
     *
     * @returns {*}
     * @memberof Lead
     */
    address2_telephone1?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof Lead
     */
    ownerid?: any;

    /**
     * 中间名
     *
     * @returns {*}
     * @memberof Lead
     */
    middlename?: any;

    /**
     * 住宅电话
     *
     * @returns {*}
     * @memberof Lead
     */
    telephone2?: any;

    /**
     * 购买时间范围
     *
     * @returns {*}
     * @memberof Lead
     */
    purchasetimeframe?: any;

    /**
     * 潜在顾客来源
     *
     * @returns {*}
     * @memberof Lead
     */
    leadsourcecode?: any;

    /**
     * EntityImage_Timestamp
     *
     * @returns {*}
     * @memberof Lead
     */
    entityimage_timestamp?: any;

    /**
     * 首选联系方式
     *
     * @returns {*}
     * @memberof Lead
     */
    preferredcontactmethodcode?: any;

    /**
     * 实体图像
     *
     * @returns {*}
     * @memberof Lead
     */
    entityimage?: any;

    /**
     * 地址 2: 电话 2
     *
     * @returns {*}
     * @memberof Lead
     */
    address2_telephone2?: any;

    /**
     * 市场营销资料
     *
     * @returns {*}
     * @memberof Lead
     */
    donotsendmm?: any;

    /**
     * 采购程序
     *
     * @returns {*}
     * @memberof Lead
     */
    purchaseprocess?: any;

    /**
     * 不允许使用批量电子邮件
     *
     * @returns {*}
     * @memberof Lead
     */
    donotbulkemail?: any;

    /**
     * 行业编码
     *
     * @returns {*}
     * @memberof Lead
     */
    sic?: any;

    /**
     * 联系人
     *
     * @returns {*}
     * @memberof Lead
     */
    contactname?: any;

    /**
     * 不允许使用电子邮件
     *
     * @returns {*}
     * @memberof Lead
     */
    donotemail?: any;

    /**
     * 地址 2: 经度
     *
     * @returns {*}
     * @memberof Lead
     */
    address2_longitude?: any;

    /**
     * 确认有兴趣
     *
     * @returns {*}
     * @memberof Lead
     */
    confirminterest?: any;

    /**
     * 负责人类型
     *
     * @returns {*}
     * @memberof Lead
     */
    ownertype?: any;

    /**
     * 地址 2: 邮政信箱
     *
     * @returns {*}
     * @memberof Lead
     */
    address2_postofficebox?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof Lead
     */
    statecode?: any;

    /**
     * 自动创建
     *
     * @returns {*}
     * @memberof Lead
     */
    autocreate?: any;

    /**
     * 地址 1: 名称
     *
     * @returns {*}
     * @memberof Lead
     */
    address1_name?: any;

    /**
     * Time Zone Rule Version Number
     *
     * @returns {*}
     * @memberof Lead
     */
    timezoneruleversionnumber?: any;

    /**
     * 上次市场活动日期
     *
     * @returns {*}
     * @memberof Lead
     */
    lastusedincampaign?: any;

    /**
     * 预计结束日期
     *
     * @returns {*}
     * @memberof Lead
     */
    estimatedclosedate?: any;

    /**
     * 地址 2: 街道 2
     *
     * @returns {*}
     * @memberof Lead
     */
    address2_line2?: any;

    /**
     * 电子邮件地址 3
     *
     * @returns {*}
     * @memberof Lead
     */
    emailaddress3?: any;

    /**
     * 寻呼机
     *
     * @returns {*}
     * @memberof Lead
     */
    pager?: any;

    /**
     * 地址 2: UPS 区域
     *
     * @returns {*}
     * @memberof Lead
     */
    address2_upszone?: any;

    /**
     * Traversed Path
     *
     * @returns {*}
     * @memberof Lead
     */
    traversedpath?: any;

    /**
     * 传真
     *
     * @returns {*}
     * @memberof Lead
     */
    fax?: any;

    /**
     * 安排跟进(授予资格)
     *
     * @returns {*}
     * @memberof Lead
     */
    schedulefollowup_qualify?: any;

    /**
     * 其他电话
     *
     * @returns {*}
     * @memberof Lead
     */
    telephone3?: any;

    /**
     * Process Id
     *
     * @returns {*}
     * @memberof Lead
     */
    processid?: any;

    /**
     * 电子邮件地址 2
     *
     * @returns {*}
     * @memberof Lead
     */
    emailaddress2?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof Lead
     */
    updateman?: any;

    /**
     * 决策者?
     *
     * @returns {*}
     * @memberof Lead
     */
    decisionmaker?: any;

    /**
     * 邮政编码
     *
     * @returns {*}
     * @memberof Lead
     */
    address1_postalcode?: any;

    /**
     * 地址 2: 邮政编码
     *
     * @returns {*}
     * @memberof Lead
     */
    address2_postalcode?: any;

    /**
     * 不允许使用传真
     *
     * @returns {*}
     * @memberof Lead
     */
    donotfax?: any;

    /**
     * 客户类型
     *
     * @returns {*}
     * @memberof Lead
     */
    customertype?: any;

    /**
     * 名
     *
     * @returns {*}
     * @memberof Lead
     */
    firstname?: any;

    /**
     * 合并
     *
     * @returns {*}
     * @memberof Lead
     */
    merged?: any;

    /**
     * 地址 2: 地址类型
     *
     * @returns {*}
     * @memberof Lead
     */
    address2_addresstypecode?: any;

    /**
     * 地址 1: UPS 区域
     *
     * @returns {*}
     * @memberof Lead
     */
    address1_upszone?: any;

    /**
     * SLA
     *
     * @returns {*}
     * @memberof Lead
     */
    slaname?: any;

    /**
     * 相关市场活动响应
     *
     * @returns {*}
     * @memberof Lead
     */
    relatedobjectname?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof Lead
     */
    currencyname?: any;

    /**
     * 原始案例
     *
     * @returns {*}
     * @memberof Lead
     */
    originatingcasename?: any;

    /**
     * 源市场活动
     *
     * @returns {*}
     * @memberof Lead
     */
    campaignname?: any;

    /**
     * 识别客户
     *
     * @returns {*}
     * @memberof Lead
     */
    parentaccountname?: any;

    /**
     * 识别联系人
     *
     * @returns {*}
     * @memberof Lead
     */
    parentcontactname?: any;

    /**
     * 授予商机资格
     *
     * @returns {*}
     * @memberof Lead
     */
    qualifyingopportunityname?: any;

    /**
     * 授予商机资格
     *
     * @returns {*}
     * @memberof Lead
     */
    qualifyingopportunityid?: any;

    /**
     * SLA
     *
     * @returns {*}
     * @memberof Lead
     */
    slaid?: any;

    /**
     * 源市场活动
     *
     * @returns {*}
     * @memberof Lead
     */
    campaignid?: any;

    /**
     * 相关市场活动响应
     *
     * @returns {*}
     * @memberof Lead
     */
    relatedobjectid?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof Lead
     */
    transactioncurrencyid?: any;

    /**
     * 潜在顾客的上级单位
     *
     * @returns {*}
     * @memberof Lead
     */
    parentaccountid?: any;

    /**
     * 原始案例
     *
     * @returns {*}
     * @memberof Lead
     */
    originatingcaseid?: any;

    /**
     * 潜在顾客的上司
     *
     * @returns {*}
     * @memberof Lead
     */
    parentcontactid?: any;
}