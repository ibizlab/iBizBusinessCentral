/**
 * 区域
 *
 * @export
 * @interface Territory
 */
export interface Territory {

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof Territory
     */
    updateman?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof Territory
     */
    createdate?: any;

    /**
     * 汇率
     *
     * @returns {*}
     * @memberof Territory
     */
    exchangerate?: any;

    /**
     * 管理员
     *
     * @returns {*}
     * @memberof Territory
     */
    managername?: any;

    /**
     * 区域
     *
     * @returns {*}
     * @memberof Territory
     */
    territoryid?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof Territory
     */
    updatedate?: any;

    /**
     * EntityImageId
     *
     * @returns {*}
     * @memberof Territory
     */
    entityimageid?: any;

    /**
     * 经理
     *
     * @returns {*}
     * @memberof Territory
     */
    managerid?: any;

    /**
     * EntityImage_URL
     *
     * @returns {*}
     * @memberof Territory
     */
    entityimage_url?: any;

    /**
     * Import Sequence Number
     *
     * @returns {*}
     * @memberof Territory
     */
    importsequencenumber?: any;

    /**
     * 时区
     *
     * @returns {*}
     * @memberof Territory
     */
    utcconversiontimezonecode?: any;

    /**
     * 地区名称
     *
     * @returns {*}
     * @memberof Territory
     */
    territoryname?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof Territory
     */
    createman?: any;

    /**
     * Record Created On
     *
     * @returns {*}
     * @memberof Territory
     */
    overriddencreatedon?: any;

    /**
     * 实体图像
     *
     * @returns {*}
     * @memberof Territory
     */
    entityimage?: any;

    /**
     * Version Number
     *
     * @returns {*}
     * @memberof Territory
     */
    versionnumber?: any;

    /**
     * EntityImage_Timestamp
     *
     * @returns {*}
     * @memberof Territory
     */
    entityimage_timestamp?: any;

    /**
     * Time Zone Rule Version Number
     *
     * @returns {*}
     * @memberof Territory
     */
    timezoneruleversionnumber?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof Territory
     */
    description?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof Territory
     */
    currencyname?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof Territory
     */
    transactioncurrencyid?: any;
}