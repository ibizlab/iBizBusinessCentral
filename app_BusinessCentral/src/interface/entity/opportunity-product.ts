/**
 * 商机产品
 *
 * @export
 * @interface OpportunityProduct
 */
export interface OpportunityProduct {

    /**
     * 自定义的价格
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    priceoverridden?: any;

    /**
     * 税 (Base)
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    tax_base?: any;

    /**
     * SkipPriceCalculation
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    skippricecalculation?: any;

    /**
     * 批发折扣 (Base)
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    volumediscountamount_base?: any;

    /**
     * 零售折扣金额
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    manualdiscountamount?: any;

    /**
     * EntityImage_Timestamp
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    entityimage_timestamp?: any;

    /**
     * 商机状态
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    opportunitystatecode?: any;

    /**
     * 负责人类型
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    ownertype?: any;

    /**
     * 批发折扣
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    volumediscountamount?: any;

    /**
     * 应收净额 (Base)
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    extendedamount_base?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    ownername?: any;

    /**
     * 单价 (Base)
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    priceperunit_base?: any;

    /**
     * 商机产品
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    opportunityproductid?: any;

    /**
     * Version Number
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    versionnumber?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    createman?: any;

    /**
     * EntityImageId
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    entityimageid?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    description?: any;

    /**
     * 零售折扣金额 (Base)
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    manualdiscountamount_base?: any;

    /**
     * 序号
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    sequencenumber?: any;

    /**
     * 目录外产品
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    productdescription?: any;

    /**
     * 选择产品
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    productoverridden?: any;

    /**
     * Product Name
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    productname?: any;

    /**
     * 汇率
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    exchangerate?: any;

    /**
     * 名称
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    opportunityproductname?: any;

    /**
     * 属性配置
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    propertyconfigurationstatus?: any;

    /**
     * 税
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    tax?: any;

    /**
     * 单价
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    priceperunit?: any;

    /**
     * 父捆绑销售
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    parentbundleid?: any;

    /**
     * UTC Conversion Time Zone Code
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    utcconversiontimezonecode?: any;

    /**
     * 捆绑销售项关联
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    productassociationid?: any;

    /**
     * 金额
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    baseamount?: any;

    /**
     * 产品类型
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    producttypecode?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    updatedate?: any;

    /**
     * Time Zone Rule Version Number
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    timezoneruleversionnumber?: any;

    /**
     * 数量
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    quantity?: any;

    /**
     * 明细项目编号
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    lineitemnumber?: any;

    /**
     * Record Created On
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    overriddencreatedon?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    createdate?: any;

    /**
     * 应收净额
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    extendedamount?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    updateman?: any;

    /**
     * EntityImage_URL
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    entityimage_url?: any;

    /**
     * 定价错误
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    pricingerrorcode?: any;

    /**
     * 实体图像
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    entityimage?: any;

    /**
     * 金额 (Base)
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    baseamount_base?: any;

    /**
     * Import Sequence Number
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    importsequencenumber?: any;

    /**
     * 现有产品
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    productid?: any;

    /**
     * Parent bundle product
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    parentbundleidref?: any;

    /**
     * 计价单位
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    uomid?: any;

    /**
     * 货币
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    transactioncurrencyid?: any;

    /**
     * 商机
     *
     * @returns {*}
     * @memberof OpportunityProduct
     */
    opportunityid?: any;
}