import { Http,Util } from '@/utils';
import IBizServiceServiceBase from './ibiz-service-service-base';


/**
 * 服务服务对象
 *
 * @export
 * @class IBizServiceService
 * @extends {IBizServiceServiceBase}
 */
export default class IBizServiceService extends IBizServiceServiceBase {

    /**
     * Creates an instance of  IBizServiceService.
     * 
     * @param {*} [opts={}]
     * @memberof  IBizServiceService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}