import { Http,Util } from '@/utils';
import FinishLogicBase from './finish-logic-base';

/**
 * 完成订单
 *
 * @export
 * @class FinishLogic
 */
export default class FinishLogic extends FinishLogicBase{

    /**
     * Creates an instance of  FinishLogic
     * 
     * @param {*} [opts={}]
     * @memberof  FinishLogic
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}