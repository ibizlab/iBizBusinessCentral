import { Http,Util } from '@/utils';
import SalesOrderServiceBase from './sales-order-service-base';


/**
 * 订单服务对象
 *
 * @export
 * @class SalesOrderService
 * @extends {SalesOrderServiceBase}
 */
export default class SalesOrderService extends SalesOrderServiceBase {

    /**
     * Creates an instance of  SalesOrderService.
     * 
     * @param {*} [opts={}]
     * @memberof  SalesOrderService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}