import { Http,Util } from '@/utils';
import ConnectionRoleServiceBase from './connection-role-service-base';


/**
 * 连接角色服务对象
 *
 * @export
 * @class ConnectionRoleService
 * @extends {ConnectionRoleServiceBase}
 */
export default class ConnectionRoleService extends ConnectionRoleServiceBase {

    /**
     * Creates an instance of  ConnectionRoleService.
     * 
     * @param {*} [opts={}]
     * @memberof  ConnectionRoleService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}