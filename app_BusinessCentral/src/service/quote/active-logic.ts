import { Http,Util } from '@/utils';
import ActiveLogicBase from './active-logic-base';

/**
 * 激活报价单
 *
 * @export
 * @class ActiveLogic
 */
export default class ActiveLogic extends ActiveLogicBase{

    /**
     * Creates an instance of  ActiveLogic
     * 
     * @param {*} [opts={}]
     * @memberof  ActiveLogic
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}