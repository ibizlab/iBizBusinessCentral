import { Http,Util } from '@/utils';
import QuoteServiceBase from './quote-service-base';


/**
 * 报价单服务对象
 *
 * @export
 * @class QuoteService
 * @extends {QuoteServiceBase}
 */
export default class QuoteService extends QuoteServiceBase {

    /**
     * Creates an instance of  QuoteService.
     * 
     * @param {*} [opts={}]
     * @memberof  QuoteService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}