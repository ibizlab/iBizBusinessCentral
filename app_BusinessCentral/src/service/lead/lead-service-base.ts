import { Http,Util } from '@/utils';
import EntityService from '../entity-service';
import ActiveLogic from '@/service/lead/active-logic';
import CancelLogic from '@/service/lead/cancel-logic';
import LostOrderLogic from '@/service/lead/lost-order-logic';
import NoInterestedLogic from '@/service/lead/no-interested-logic';
import UnableLogic from '@/service/lead/unable-logic';



/**
 * 潜在顾客服务对象基类
 *
 * @export
 * @class LeadServiceBase
 * @extends {EntityServie}
 */
export default class LeadServiceBase extends EntityService {

    /**
     * Creates an instance of  LeadServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  LeadServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof LeadServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='lead';
        this.APPDEKEY = 'leadid';
        this.APPDENAME = 'leads';
        this.APPDETEXT = 'fullname';
        this.APPNAME = 'businesscentral';
        this.SYSTEMNAME = 'ibizbusinesscentral';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof LeadServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.lead){
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/leads/${context.lead}/select`,isloading);
            
            return res;
        }
        if(context.contact && context.lead){
            let res:any = Http.getInstance().get(`/contacts/${context.contact}/leads/${context.lead}/select`,isloading);
            
            return res;
        }
        if(context.campaign && context.lead){
            let res:any = Http.getInstance().get(`/campaigns/${context.campaign}/leads/${context.lead}/select`,isloading);
            
            return res;
        }
        if(context.account && context.lead){
            let res:any = Http.getInstance().get(`/accounts/${context.account}/leads/${context.lead}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/leads/${context.lead}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof LeadServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/leads`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_leadcompetitors',JSON.stringify(res.data.leadcompetitors?res.data.leadcompetitors:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_listleads',JSON.stringify(res.data.listleads?res.data.listleads:[]));
            
            return res;
        }
        if(context.contact && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/contacts/${context.contact}/leads`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_leadcompetitors',JSON.stringify(res.data.leadcompetitors?res.data.leadcompetitors:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_listleads',JSON.stringify(res.data.listleads?res.data.listleads:[]));
            
            return res;
        }
        if(context.campaign && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/campaigns/${context.campaign}/leads`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_leadcompetitors',JSON.stringify(res.data.leadcompetitors?res.data.leadcompetitors:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_listleads',JSON.stringify(res.data.listleads?res.data.listleads:[]));
            
            return res;
        }
        if(context.account && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/leads`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_leadcompetitors',JSON.stringify(res.data.leadcompetitors?res.data.leadcompetitors:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_listleads',JSON.stringify(res.data.listleads?res.data.listleads:[]));
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/leads`,data,isloading);
        this.tempStorage.setItem(tempContext.srfsessionkey+'_leadcompetitors',JSON.stringify(res.data.leadcompetitors?res.data.leadcompetitors:[]));
        this.tempStorage.setItem(tempContext.srfsessionkey+'_listleads',JSON.stringify(res.data.listleads?res.data.listleads:[]));
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof LeadServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.lead){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/accounts/${context.account}/contacts/${context.contact}/leads/${context.lead}`,data,isloading);
            
            return res;
        }
        if(context.contact && context.lead){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/contacts/${context.contact}/leads/${context.lead}`,data,isloading);
            
            return res;
        }
        if(context.campaign && context.lead){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/campaigns/${context.campaign}/leads/${context.lead}`,data,isloading);
            
            return res;
        }
        if(context.account && context.lead){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/accounts/${context.account}/leads/${context.lead}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/leads/${context.lead}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof LeadServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.lead){
            let res:any = Http.getInstance().delete(`/accounts/${context.account}/contacts/${context.contact}/leads/${context.lead}`,isloading);
            return res;
        }
        if(context.contact && context.lead){
            let res:any = Http.getInstance().delete(`/contacts/${context.contact}/leads/${context.lead}`,isloading);
            return res;
        }
        if(context.campaign && context.lead){
            let res:any = Http.getInstance().delete(`/campaigns/${context.campaign}/leads/${context.lead}`,isloading);
            return res;
        }
        if(context.account && context.lead){
            let res:any = Http.getInstance().delete(`/accounts/${context.account}/leads/${context.lead}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/leads/${context.lead}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof LeadServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.lead){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/leads/${context.lead}`,isloading);
            
            return res;
        }
        if(context.contact && context.lead){
            let res:any = await Http.getInstance().get(`/contacts/${context.contact}/leads/${context.lead}`,isloading);
            
            return res;
        }
        if(context.campaign && context.lead){
            let res:any = await Http.getInstance().get(`/campaigns/${context.campaign}/leads/${context.lead}`,isloading);
            
            return res;
        }
        if(context.account && context.lead){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/leads/${context.lead}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/leads/${context.lead}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof LeadServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && true){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/leads/getdraft`,isloading);
            res.data.lead = data.lead;
            
            return res;
        }
        if(context.contact && true){
            let res:any = await Http.getInstance().get(`/contacts/${context.contact}/leads/getdraft`,isloading);
            res.data.lead = data.lead;
            
            return res;
        }
        if(context.campaign && true){
            let res:any = await Http.getInstance().get(`/campaigns/${context.campaign}/leads/getdraft`,isloading);
            res.data.lead = data.lead;
            
            return res;
        }
        if(context.account && true){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/leads/getdraft`,isloading);
            res.data.lead = data.lead;
            
            return res;
        }
        let res:any = await  Http.getInstance().get(`/leads/getdraft`,isloading);
        res.data.lead = data.lead;
        
        return res;
    }

    /**
     * Active接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof LeadServiceBase
     */
    public async Active(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        let appLogic:ActiveLogic = new ActiveLogic({context:JSON.parse(JSON.stringify(context)),data:JSON.parse(JSON.stringify(data))});
        const res = await appLogic.onExecute(context,data,isloading?true:false);
        return {status:200,data:res};
    }

    /**
     * AddList接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof LeadServiceBase
     */
    public async AddList(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.lead){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/leads/${context.lead}/addlist`,data,isloading);
            
            return res;
        }
        if(context.contact && context.lead){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/contacts/${context.contact}/leads/${context.lead}/addlist`,data,isloading);
            
            return res;
        }
        if(context.campaign && context.lead){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/campaigns/${context.campaign}/leads/${context.lead}/addlist`,data,isloading);
            
            return res;
        }
        if(context.account && context.lead){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/leads/${context.lead}/addlist`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/leads/${context.lead}/addlist`,data,isloading);
            return res;
    }

    /**
     * Cancel接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof LeadServiceBase
     */
    public async Cancel(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        let appLogic:CancelLogic = new CancelLogic({context:JSON.parse(JSON.stringify(context)),data:JSON.parse(JSON.stringify(data))});
        const res = await appLogic.onExecute(context,data,isloading?true:false);
        return {status:200,data:res};
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof LeadServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.lead){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/leads/${context.lead}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.contact && context.lead){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/contacts/${context.contact}/leads/${context.lead}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.campaign && context.lead){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/campaigns/${context.campaign}/leads/${context.lead}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.account && context.lead){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/leads/${context.lead}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/leads/${context.lead}/checkkey`,data,isloading);
            return res;
    }

    /**
     * DisQualification接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof LeadServiceBase
     */
    public async DisQualification(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.lead){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/leads/${context.lead}/disqualification`,data,isloading);
            
            return res;
        }
        if(context.contact && context.lead){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/contacts/${context.contact}/leads/${context.lead}/disqualification`,data,isloading);
            
            return res;
        }
        if(context.campaign && context.lead){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/campaigns/${context.campaign}/leads/${context.lead}/disqualification`,data,isloading);
            
            return res;
        }
        if(context.account && context.lead){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/leads/${context.lead}/disqualification`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/leads/${context.lead}/disqualification`,data,isloading);
            return res;
    }

    /**
     * LostOrder接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof LeadServiceBase
     */
    public async LostOrder(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        let appLogic:LostOrderLogic = new LostOrderLogic({context:JSON.parse(JSON.stringify(context)),data:JSON.parse(JSON.stringify(data))});
        const res = await appLogic.onExecute(context,data,isloading?true:false);
        return {status:200,data:res};
    }

    /**
     * NoInterested接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof LeadServiceBase
     */
    public async NoInterested(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        let appLogic:NoInterestedLogic = new NoInterestedLogic({context:JSON.parse(JSON.stringify(context)),data:JSON.parse(JSON.stringify(data))});
        const res = await appLogic.onExecute(context,data,isloading?true:false);
        return {status:200,data:res};
    }

    /**
     * Qualification接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof LeadServiceBase
     */
    public async Qualification(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.lead){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/leads/${context.lead}/qualification`,data,isloading);
            
            return res;
        }
        if(context.contact && context.lead){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/contacts/${context.contact}/leads/${context.lead}/qualification`,data,isloading);
            
            return res;
        }
        if(context.campaign && context.lead){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/campaigns/${context.campaign}/leads/${context.lead}/qualification`,data,isloading);
            
            return res;
        }
        if(context.account && context.lead){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/leads/${context.lead}/qualification`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/leads/${context.lead}/qualification`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof LeadServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.lead){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/leads/${context.lead}/save`,data,isloading);
            
            return res;
        }
        if(context.contact && context.lead){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/contacts/${context.contact}/leads/${context.lead}/save`,data,isloading);
            
            return res;
        }
        if(context.campaign && context.lead){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/campaigns/${context.campaign}/leads/${context.lead}/save`,data,isloading);
            
            return res;
        }
        if(context.account && context.lead){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/leads/${context.lead}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/leads/${context.lead}/save`,data,isloading);
            
            return res;
    }

    /**
     * Unable接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof LeadServiceBase
     */
    public async Unable(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        let appLogic:UnableLogic = new UnableLogic({context:JSON.parse(JSON.stringify(context)),data:JSON.parse(JSON.stringify(data))});
        const res = await appLogic.onExecute(context,data,isloading?true:false);
        return {status:200,data:res};
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof LeadServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/leads/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.contact && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/contacts/${context.contact}/leads/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.campaign && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/campaigns/${context.campaign}/leads/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.account && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/leads/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/leads/fetchdefault`,tempData,isloading);
        return res;
    }

    /**
     * FetchExcluded接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof LeadServiceBase
     */
    public async FetchExcluded(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/leads/fetchexcluded`,tempData,isloading);
            return res;
        }
        if(context.contact && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/contacts/${context.contact}/leads/fetchexcluded`,tempData,isloading);
            return res;
        }
        if(context.campaign && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/campaigns/${context.campaign}/leads/fetchexcluded`,tempData,isloading);
            return res;
        }
        if(context.account && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/leads/fetchexcluded`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/leads/fetchexcluded`,tempData,isloading);
        return res;
    }

    /**
     * FetchOn接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof LeadServiceBase
     */
    public async FetchOn(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/leads/fetchon`,tempData,isloading);
            return res;
        }
        if(context.contact && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/contacts/${context.contact}/leads/fetchon`,tempData,isloading);
            return res;
        }
        if(context.campaign && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/campaigns/${context.campaign}/leads/fetchon`,tempData,isloading);
            return res;
        }
        if(context.account && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/leads/fetchon`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/leads/fetchon`,tempData,isloading);
        return res;
    }
}