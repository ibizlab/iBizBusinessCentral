import { Http,Util } from '@/utils';
import OrganizationServiceBase from './organization-service-base';


/**
 * 组织服务对象
 *
 * @export
 * @class OrganizationService
 * @extends {OrganizationServiceBase}
 */
export default class OrganizationService extends OrganizationServiceBase {

    /**
     * Creates an instance of  OrganizationService.
     * 
     * @param {*} [opts={}]
     * @memberof  OrganizationService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}