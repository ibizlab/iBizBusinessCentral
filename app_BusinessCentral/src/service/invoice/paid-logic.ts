import { Http,Util } from '@/utils';
import PaidLogicBase from './paid-logic-base';

/**
 * 发票已支付
 *
 * @export
 * @class PaidLogic
 */
export default class PaidLogic extends PaidLogicBase{

    /**
     * Creates an instance of  PaidLogic
     * 
     * @param {*} [opts={}]
     * @memberof  PaidLogic
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}