import { Http,Util } from '@/utils';
import InvoiceServiceBase from './invoice-service-base';


/**
 * 发票服务对象
 *
 * @export
 * @class InvoiceService
 * @extends {InvoiceServiceBase}
 */
export default class InvoiceService extends InvoiceServiceBase {

    /**
     * Creates an instance of  InvoiceService.
     * 
     * @param {*} [opts={}]
     * @memberof  InvoiceService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}