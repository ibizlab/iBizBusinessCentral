import { Http,Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 订单产品服务对象基类
 *
 * @export
 * @class SalesOrderDetailServiceBase
 * @extends {EntityServie}
 */
export default class SalesOrderDetailServiceBase extends EntityService {

    /**
     * Creates an instance of  SalesOrderDetailServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  SalesOrderDetailServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof SalesOrderDetailServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='salesorderdetail';
        this.APPDEKEY = 'salesorderdetailid';
        this.APPDENAME = 'salesorderdetails';
        this.APPDETEXT = 'salesorderdetailname';
        this.APPNAME = 'businesscentral';
        this.SYSTEMNAME = 'ibizbusinesscentral';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof SalesOrderDetailServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && context.salesorder && context.salesorderdetail){
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/salesorderdetails/${context.salesorderdetail}/select`,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && context.quote && context.salesorder && context.salesorderdetail){
            let res:any = Http.getInstance().get(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/salesorderdetails/${context.salesorderdetail}/select`,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && context.quote && context.salesorder && context.salesorderdetail){
            let res:any = Http.getInstance().get(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/salesorderdetails/${context.salesorderdetail}/select`,isloading);
            
            return res;
        }
        if(context.opportunity && context.quote && context.salesorder && context.salesorderdetail){
            let res:any = Http.getInstance().get(`/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/salesorderdetails/${context.salesorderdetail}/select`,isloading);
            
            return res;
        }
        if(context.quote && context.salesorder && context.salesorderdetail){
            let res:any = Http.getInstance().get(`/quotes/${context.quote}/salesorders/${context.salesorder}/salesorderdetails/${context.salesorderdetail}/select`,isloading);
            
            return res;
        }
        if(context.salesorder && context.salesorderdetail){
            let res:any = Http.getInstance().get(`/salesorders/${context.salesorder}/salesorderdetails/${context.salesorderdetail}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/salesorderdetails/${context.salesorderdetail}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof SalesOrderDetailServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && context.salesorder && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/salesorderdetails`,data,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && context.quote && context.salesorder && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/salesorderdetails`,data,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && context.quote && context.salesorder && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/salesorderdetails`,data,isloading);
            
            return res;
        }
        if(context.opportunity && context.quote && context.salesorder && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/salesorderdetails`,data,isloading);
            
            return res;
        }
        if(context.quote && context.salesorder && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/quotes/${context.quote}/salesorders/${context.salesorder}/salesorderdetails`,data,isloading);
            
            return res;
        }
        if(context.salesorder && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/salesorders/${context.salesorder}/salesorderdetails`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/salesorderdetails`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof SalesOrderDetailServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && context.salesorder && context.salesorderdetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/salesorderdetails/${context.salesorderdetail}`,data,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && context.quote && context.salesorder && context.salesorderdetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/salesorderdetails/${context.salesorderdetail}`,data,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && context.quote && context.salesorder && context.salesorderdetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/salesorderdetails/${context.salesorderdetail}`,data,isloading);
            
            return res;
        }
        if(context.opportunity && context.quote && context.salesorder && context.salesorderdetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/salesorderdetails/${context.salesorderdetail}`,data,isloading);
            
            return res;
        }
        if(context.quote && context.salesorder && context.salesorderdetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/quotes/${context.quote}/salesorders/${context.salesorder}/salesorderdetails/${context.salesorderdetail}`,data,isloading);
            
            return res;
        }
        if(context.salesorder && context.salesorderdetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/salesorders/${context.salesorder}/salesorderdetails/${context.salesorderdetail}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/salesorderdetails/${context.salesorderdetail}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof SalesOrderDetailServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && context.salesorder && context.salesorderdetail){
            let res:any = Http.getInstance().delete(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/salesorderdetails/${context.salesorderdetail}`,isloading);
            return res;
        }
        if(context.contact && context.opportunity && context.quote && context.salesorder && context.salesorderdetail){
            let res:any = Http.getInstance().delete(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/salesorderdetails/${context.salesorderdetail}`,isloading);
            return res;
        }
        if(context.account && context.opportunity && context.quote && context.salesorder && context.salesorderdetail){
            let res:any = Http.getInstance().delete(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/salesorderdetails/${context.salesorderdetail}`,isloading);
            return res;
        }
        if(context.opportunity && context.quote && context.salesorder && context.salesorderdetail){
            let res:any = Http.getInstance().delete(`/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/salesorderdetails/${context.salesorderdetail}`,isloading);
            return res;
        }
        if(context.quote && context.salesorder && context.salesorderdetail){
            let res:any = Http.getInstance().delete(`/quotes/${context.quote}/salesorders/${context.salesorder}/salesorderdetails/${context.salesorderdetail}`,isloading);
            return res;
        }
        if(context.salesorder && context.salesorderdetail){
            let res:any = Http.getInstance().delete(`/salesorders/${context.salesorder}/salesorderdetails/${context.salesorderdetail}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/salesorderdetails/${context.salesorderdetail}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof SalesOrderDetailServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && context.salesorder && context.salesorderdetail){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/salesorderdetails/${context.salesorderdetail}`,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && context.quote && context.salesorder && context.salesorderdetail){
            let res:any = await Http.getInstance().get(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/salesorderdetails/${context.salesorderdetail}`,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && context.quote && context.salesorder && context.salesorderdetail){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/salesorderdetails/${context.salesorderdetail}`,isloading);
            
            return res;
        }
        if(context.opportunity && context.quote && context.salesorder && context.salesorderdetail){
            let res:any = await Http.getInstance().get(`/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/salesorderdetails/${context.salesorderdetail}`,isloading);
            
            return res;
        }
        if(context.quote && context.salesorder && context.salesorderdetail){
            let res:any = await Http.getInstance().get(`/quotes/${context.quote}/salesorders/${context.salesorder}/salesorderdetails/${context.salesorderdetail}`,isloading);
            
            return res;
        }
        if(context.salesorder && context.salesorderdetail){
            let res:any = await Http.getInstance().get(`/salesorders/${context.salesorder}/salesorderdetails/${context.salesorderdetail}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/salesorderdetails/${context.salesorderdetail}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof SalesOrderDetailServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && context.salesorder && true){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/salesorderdetails/getdraft`,isloading);
            res.data.salesorderdetail = data.salesorderdetail;
            
            return res;
        }
        if(context.contact && context.opportunity && context.quote && context.salesorder && true){
            let res:any = await Http.getInstance().get(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/salesorderdetails/getdraft`,isloading);
            res.data.salesorderdetail = data.salesorderdetail;
            
            return res;
        }
        if(context.account && context.opportunity && context.quote && context.salesorder && true){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/salesorderdetails/getdraft`,isloading);
            res.data.salesorderdetail = data.salesorderdetail;
            
            return res;
        }
        if(context.opportunity && context.quote && context.salesorder && true){
            let res:any = await Http.getInstance().get(`/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/salesorderdetails/getdraft`,isloading);
            res.data.salesorderdetail = data.salesorderdetail;
            
            return res;
        }
        if(context.quote && context.salesorder && true){
            let res:any = await Http.getInstance().get(`/quotes/${context.quote}/salesorders/${context.salesorder}/salesorderdetails/getdraft`,isloading);
            res.data.salesorderdetail = data.salesorderdetail;
            
            return res;
        }
        if(context.salesorder && true){
            let res:any = await Http.getInstance().get(`/salesorders/${context.salesorder}/salesorderdetails/getdraft`,isloading);
            res.data.salesorderdetail = data.salesorderdetail;
            
            return res;
        }
        let res:any = await  Http.getInstance().get(`/salesorderdetails/getdraft`,isloading);
        res.data.salesorderdetail = data.salesorderdetail;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof SalesOrderDetailServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && context.salesorder && context.salesorderdetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/salesorderdetails/${context.salesorderdetail}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && context.quote && context.salesorder && context.salesorderdetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/salesorderdetails/${context.salesorderdetail}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && context.quote && context.salesorder && context.salesorderdetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/salesorderdetails/${context.salesorderdetail}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.opportunity && context.quote && context.salesorder && context.salesorderdetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/salesorderdetails/${context.salesorderdetail}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.quote && context.salesorder && context.salesorderdetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/quotes/${context.quote}/salesorders/${context.salesorder}/salesorderdetails/${context.salesorderdetail}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.salesorder && context.salesorderdetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/salesorders/${context.salesorder}/salesorderdetails/${context.salesorderdetail}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/salesorderdetails/${context.salesorderdetail}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof SalesOrderDetailServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && context.salesorder && context.salesorderdetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/salesorderdetails/${context.salesorderdetail}/save`,data,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && context.quote && context.salesorder && context.salesorderdetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/salesorderdetails/${context.salesorderdetail}/save`,data,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && context.quote && context.salesorder && context.salesorderdetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/salesorderdetails/${context.salesorderdetail}/save`,data,isloading);
            
            return res;
        }
        if(context.opportunity && context.quote && context.salesorder && context.salesorderdetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/salesorderdetails/${context.salesorderdetail}/save`,data,isloading);
            
            return res;
        }
        if(context.quote && context.salesorder && context.salesorderdetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/quotes/${context.quote}/salesorders/${context.salesorder}/salesorderdetails/${context.salesorderdetail}/save`,data,isloading);
            
            return res;
        }
        if(context.salesorder && context.salesorderdetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/salesorders/${context.salesorder}/salesorderdetails/${context.salesorderdetail}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/salesorderdetails/${context.salesorderdetail}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof SalesOrderDetailServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && context.salesorder && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/salesorderdetails/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.contact && context.opportunity && context.quote && context.salesorder && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/salesorderdetails/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.account && context.opportunity && context.quote && context.salesorder && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/salesorderdetails/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.opportunity && context.quote && context.salesorder && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/salesorderdetails/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.quote && context.salesorder && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/quotes/${context.quote}/salesorders/${context.salesorder}/salesorderdetails/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.salesorder && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/salesorders/${context.salesorder}/salesorderdetails/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/salesorderdetails/fetchdefault`,tempData,isloading);
        return res;
    }
}