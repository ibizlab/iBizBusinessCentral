import { Http,Util } from '@/utils';
import ProductPriceLevelServiceBase from './product-price-level-service-base';


/**
 * 价目表项服务对象
 *
 * @export
 * @class ProductPriceLevelService
 * @extends {ProductPriceLevelServiceBase}
 */
export default class ProductPriceLevelService extends ProductPriceLevelServiceBase {

    /**
     * Creates an instance of  ProductPriceLevelService.
     * 
     * @param {*} [opts={}]
     * @memberof  ProductPriceLevelService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}