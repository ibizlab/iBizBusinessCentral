import { Http,Util } from '@/utils';
import UomScheduleServiceBase from './uom-schedule-service-base';


/**
 * 计价单位组服务对象
 *
 * @export
 * @class UomScheduleService
 * @extends {UomScheduleServiceBase}
 */
export default class UomScheduleService extends UomScheduleServiceBase {

    /**
     * Creates an instance of  UomScheduleService.
     * 
     * @param {*} [opts={}]
     * @memberof  UomScheduleService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}