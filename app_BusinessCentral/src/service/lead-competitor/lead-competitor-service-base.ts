import { Http,Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 潜在客户对手服务对象基类
 *
 * @export
 * @class LeadCompetitorServiceBase
 * @extends {EntityServie}
 */
export default class LeadCompetitorServiceBase extends EntityService {

    /**
     * Creates an instance of  LeadCompetitorServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  LeadCompetitorServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof LeadCompetitorServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='leadcompetitor';
        this.APPDEKEY = 'relationshipsid';
        this.APPDENAME = 'leadcompetitors';
        this.APPDETEXT = 'relationshipsname';
        this.APPNAME = 'businesscentral';
        this.SYSTEMNAME = 'ibizbusinesscentral';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof LeadCompetitorServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.lead && context.leadcompetitor){
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/leads/${context.lead}/leadcompetitors/${context.leadcompetitor}/select`,isloading);
            
            return res;
        }
        if(context.contact && context.lead && context.leadcompetitor){
            let res:any = Http.getInstance().get(`/contacts/${context.contact}/leads/${context.lead}/leadcompetitors/${context.leadcompetitor}/select`,isloading);
            
            return res;
        }
        if(context.campaign && context.lead && context.leadcompetitor){
            let res:any = Http.getInstance().get(`/campaigns/${context.campaign}/leads/${context.lead}/leadcompetitors/${context.leadcompetitor}/select`,isloading);
            
            return res;
        }
        if(context.account && context.lead && context.leadcompetitor){
            let res:any = Http.getInstance().get(`/accounts/${context.account}/leads/${context.lead}/leadcompetitors/${context.leadcompetitor}/select`,isloading);
            
            return res;
        }
        if(context.lead && context.leadcompetitor){
            let res:any = Http.getInstance().get(`/leads/${context.lead}/leadcompetitors/${context.leadcompetitor}/select`,isloading);
            
            return res;
        }
        if(context.competitor && context.leadcompetitor){
            let res:any = Http.getInstance().get(`/competitors/${context.competitor}/leadcompetitors/${context.leadcompetitor}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/leadcompetitors/${context.leadcompetitor}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof LeadCompetitorServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.lead && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/leads/${context.lead}/leadcompetitors`,data,isloading);
            
            return res;
        }
        if(context.contact && context.lead && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/contacts/${context.contact}/leads/${context.lead}/leadcompetitors`,data,isloading);
            
            return res;
        }
        if(context.campaign && context.lead && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/campaigns/${context.campaign}/leads/${context.lead}/leadcompetitors`,data,isloading);
            
            return res;
        }
        if(context.account && context.lead && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/leads/${context.lead}/leadcompetitors`,data,isloading);
            
            return res;
        }
        if(context.lead && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/leads/${context.lead}/leadcompetitors`,data,isloading);
            
            return res;
        }
        if(context.competitor && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/competitors/${context.competitor}/leadcompetitors`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/leadcompetitors`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof LeadCompetitorServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.lead && context.leadcompetitor){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/accounts/${context.account}/contacts/${context.contact}/leads/${context.lead}/leadcompetitors/${context.leadcompetitor}`,data,isloading);
            
            return res;
        }
        if(context.contact && context.lead && context.leadcompetitor){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/contacts/${context.contact}/leads/${context.lead}/leadcompetitors/${context.leadcompetitor}`,data,isloading);
            
            return res;
        }
        if(context.campaign && context.lead && context.leadcompetitor){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/campaigns/${context.campaign}/leads/${context.lead}/leadcompetitors/${context.leadcompetitor}`,data,isloading);
            
            return res;
        }
        if(context.account && context.lead && context.leadcompetitor){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/accounts/${context.account}/leads/${context.lead}/leadcompetitors/${context.leadcompetitor}`,data,isloading);
            
            return res;
        }
        if(context.lead && context.leadcompetitor){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/leads/${context.lead}/leadcompetitors/${context.leadcompetitor}`,data,isloading);
            
            return res;
        }
        if(context.competitor && context.leadcompetitor){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/competitors/${context.competitor}/leadcompetitors/${context.leadcompetitor}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/leadcompetitors/${context.leadcompetitor}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof LeadCompetitorServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.lead && context.leadcompetitor){
            let res:any = Http.getInstance().delete(`/accounts/${context.account}/contacts/${context.contact}/leads/${context.lead}/leadcompetitors/${context.leadcompetitor}`,isloading);
            return res;
        }
        if(context.contact && context.lead && context.leadcompetitor){
            let res:any = Http.getInstance().delete(`/contacts/${context.contact}/leads/${context.lead}/leadcompetitors/${context.leadcompetitor}`,isloading);
            return res;
        }
        if(context.campaign && context.lead && context.leadcompetitor){
            let res:any = Http.getInstance().delete(`/campaigns/${context.campaign}/leads/${context.lead}/leadcompetitors/${context.leadcompetitor}`,isloading);
            return res;
        }
        if(context.account && context.lead && context.leadcompetitor){
            let res:any = Http.getInstance().delete(`/accounts/${context.account}/leads/${context.lead}/leadcompetitors/${context.leadcompetitor}`,isloading);
            return res;
        }
        if(context.lead && context.leadcompetitor){
            let res:any = Http.getInstance().delete(`/leads/${context.lead}/leadcompetitors/${context.leadcompetitor}`,isloading);
            return res;
        }
        if(context.competitor && context.leadcompetitor){
            let res:any = Http.getInstance().delete(`/competitors/${context.competitor}/leadcompetitors/${context.leadcompetitor}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/leadcompetitors/${context.leadcompetitor}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof LeadCompetitorServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.lead && context.leadcompetitor){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/leads/${context.lead}/leadcompetitors/${context.leadcompetitor}`,isloading);
            
            return res;
        }
        if(context.contact && context.lead && context.leadcompetitor){
            let res:any = await Http.getInstance().get(`/contacts/${context.contact}/leads/${context.lead}/leadcompetitors/${context.leadcompetitor}`,isloading);
            
            return res;
        }
        if(context.campaign && context.lead && context.leadcompetitor){
            let res:any = await Http.getInstance().get(`/campaigns/${context.campaign}/leads/${context.lead}/leadcompetitors/${context.leadcompetitor}`,isloading);
            
            return res;
        }
        if(context.account && context.lead && context.leadcompetitor){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/leads/${context.lead}/leadcompetitors/${context.leadcompetitor}`,isloading);
            
            return res;
        }
        if(context.lead && context.leadcompetitor){
            let res:any = await Http.getInstance().get(`/leads/${context.lead}/leadcompetitors/${context.leadcompetitor}`,isloading);
            
            return res;
        }
        if(context.competitor && context.leadcompetitor){
            let res:any = await Http.getInstance().get(`/competitors/${context.competitor}/leadcompetitors/${context.leadcompetitor}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/leadcompetitors/${context.leadcompetitor}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof LeadCompetitorServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.lead && true){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/leads/${context.lead}/leadcompetitors/getdraft`,isloading);
            res.data.leadcompetitor = data.leadcompetitor;
            
            return res;
        }
        if(context.contact && context.lead && true){
            let res:any = await Http.getInstance().get(`/contacts/${context.contact}/leads/${context.lead}/leadcompetitors/getdraft`,isloading);
            res.data.leadcompetitor = data.leadcompetitor;
            
            return res;
        }
        if(context.campaign && context.lead && true){
            let res:any = await Http.getInstance().get(`/campaigns/${context.campaign}/leads/${context.lead}/leadcompetitors/getdraft`,isloading);
            res.data.leadcompetitor = data.leadcompetitor;
            
            return res;
        }
        if(context.account && context.lead && true){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/leads/${context.lead}/leadcompetitors/getdraft`,isloading);
            res.data.leadcompetitor = data.leadcompetitor;
            
            return res;
        }
        if(context.lead && true){
            let res:any = await Http.getInstance().get(`/leads/${context.lead}/leadcompetitors/getdraft`,isloading);
            res.data.leadcompetitor = data.leadcompetitor;
            
            return res;
        }
        if(context.competitor && true){
            let res:any = await Http.getInstance().get(`/competitors/${context.competitor}/leadcompetitors/getdraft`,isloading);
            res.data.leadcompetitor = data.leadcompetitor;
            
            return res;
        }
        let res:any = await  Http.getInstance().get(`/leadcompetitors/getdraft`,isloading);
        res.data.leadcompetitor = data.leadcompetitor;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof LeadCompetitorServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.lead && context.leadcompetitor){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/leads/${context.lead}/leadcompetitors/${context.leadcompetitor}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.contact && context.lead && context.leadcompetitor){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/contacts/${context.contact}/leads/${context.lead}/leadcompetitors/${context.leadcompetitor}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.campaign && context.lead && context.leadcompetitor){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/campaigns/${context.campaign}/leads/${context.lead}/leadcompetitors/${context.leadcompetitor}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.account && context.lead && context.leadcompetitor){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/leads/${context.lead}/leadcompetitors/${context.leadcompetitor}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.lead && context.leadcompetitor){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/leads/${context.lead}/leadcompetitors/${context.leadcompetitor}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.competitor && context.leadcompetitor){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/competitors/${context.competitor}/leadcompetitors/${context.leadcompetitor}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/leadcompetitors/${context.leadcompetitor}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof LeadCompetitorServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.lead && context.leadcompetitor){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/leads/${context.lead}/leadcompetitors/${context.leadcompetitor}/save`,data,isloading);
            
            return res;
        }
        if(context.contact && context.lead && context.leadcompetitor){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/contacts/${context.contact}/leads/${context.lead}/leadcompetitors/${context.leadcompetitor}/save`,data,isloading);
            
            return res;
        }
        if(context.campaign && context.lead && context.leadcompetitor){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/campaigns/${context.campaign}/leads/${context.lead}/leadcompetitors/${context.leadcompetitor}/save`,data,isloading);
            
            return res;
        }
        if(context.account && context.lead && context.leadcompetitor){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/leads/${context.lead}/leadcompetitors/${context.leadcompetitor}/save`,data,isloading);
            
            return res;
        }
        if(context.lead && context.leadcompetitor){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/leads/${context.lead}/leadcompetitors/${context.leadcompetitor}/save`,data,isloading);
            
            return res;
        }
        if(context.competitor && context.leadcompetitor){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/competitors/${context.competitor}/leadcompetitors/${context.leadcompetitor}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/leadcompetitors/${context.leadcompetitor}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof LeadCompetitorServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.lead && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/leads/${context.lead}/leadcompetitors/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.contact && context.lead && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/contacts/${context.contact}/leads/${context.lead}/leadcompetitors/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.campaign && context.lead && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/campaigns/${context.campaign}/leads/${context.lead}/leadcompetitors/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.account && context.lead && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/leads/${context.lead}/leadcompetitors/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.lead && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/leads/${context.lead}/leadcompetitors/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.competitor && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/competitors/${context.competitor}/leadcompetitors/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/leadcompetitors/fetchdefault`,tempData,isloading);
        return res;
    }
}