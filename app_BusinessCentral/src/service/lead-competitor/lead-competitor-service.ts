import { Http,Util } from '@/utils';
import LeadCompetitorServiceBase from './lead-competitor-service-base';


/**
 * 潜在客户对手服务对象
 *
 * @export
 * @class LeadCompetitorService
 * @extends {LeadCompetitorServiceBase}
 */
export default class LeadCompetitorService extends LeadCompetitorServiceBase {

    /**
     * Creates an instance of  LeadCompetitorService.
     * 
     * @param {*} [opts={}]
     * @memberof  LeadCompetitorService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}