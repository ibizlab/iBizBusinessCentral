import { Http,Util } from '@/utils';
import MetricServiceBase from './metric-service-base';


/**
 * 目标度量服务对象
 *
 * @export
 * @class MetricService
 * @extends {MetricServiceBase}
 */
export default class MetricService extends MetricServiceBase {

    /**
     * Creates an instance of  MetricService.
     * 
     * @param {*} [opts={}]
     * @memberof  MetricService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}