import { Http,Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 市场活动-营销列表服务对象基类
 *
 * @export
 * @class CampaignListServiceBase
 * @extends {EntityServie}
 */
export default class CampaignListServiceBase extends EntityService {

    /**
     * Creates an instance of  CampaignListServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  CampaignListServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof CampaignListServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='campaignlist';
        this.APPDEKEY = 'relationshipsid';
        this.APPDENAME = 'campaignlists';
        this.APPDETEXT = 'relationshipsname';
        this.APPNAME = 'businesscentral';
        this.SYSTEMNAME = 'ibizbusinesscentral';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof CampaignListServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.ibizlist && context.campaignlist){
            let res:any = Http.getInstance().get(`/ibizlists/${context.ibizlist}/campaignlists/${context.campaignlist}/select`,isloading);
            
            return res;
        }
        if(context.campaign && context.campaignlist){
            let res:any = Http.getInstance().get(`/campaigns/${context.campaign}/campaignlists/${context.campaignlist}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/campaignlists/${context.campaignlist}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof CampaignListServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.ibizlist && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/ibizlists/${context.ibizlist}/campaignlists`,data,isloading);
            
            return res;
        }
        if(context.campaign && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/campaigns/${context.campaign}/campaignlists`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/campaignlists`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof CampaignListServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.ibizlist && context.campaignlist){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/ibizlists/${context.ibizlist}/campaignlists/${context.campaignlist}`,data,isloading);
            
            return res;
        }
        if(context.campaign && context.campaignlist){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/campaigns/${context.campaign}/campaignlists/${context.campaignlist}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/campaignlists/${context.campaignlist}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof CampaignListServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.ibizlist && context.campaignlist){
            let res:any = Http.getInstance().delete(`/ibizlists/${context.ibizlist}/campaignlists/${context.campaignlist}`,isloading);
            return res;
        }
        if(context.campaign && context.campaignlist){
            let res:any = Http.getInstance().delete(`/campaigns/${context.campaign}/campaignlists/${context.campaignlist}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/campaignlists/${context.campaignlist}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof CampaignListServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.ibizlist && context.campaignlist){
            let res:any = await Http.getInstance().get(`/ibizlists/${context.ibizlist}/campaignlists/${context.campaignlist}`,isloading);
            
            return res;
        }
        if(context.campaign && context.campaignlist){
            let res:any = await Http.getInstance().get(`/campaigns/${context.campaign}/campaignlists/${context.campaignlist}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/campaignlists/${context.campaignlist}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof CampaignListServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.ibizlist && true){
            let res:any = await Http.getInstance().get(`/ibizlists/${context.ibizlist}/campaignlists/getdraft`,isloading);
            res.data.campaignlist = data.campaignlist;
            
            return res;
        }
        if(context.campaign && true){
            let res:any = await Http.getInstance().get(`/campaigns/${context.campaign}/campaignlists/getdraft`,isloading);
            res.data.campaignlist = data.campaignlist;
            
            return res;
        }
        let res:any = await  Http.getInstance().get(`/campaignlists/getdraft`,isloading);
        res.data.campaignlist = data.campaignlist;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof CampaignListServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.ibizlist && context.campaignlist){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/ibizlists/${context.ibizlist}/campaignlists/${context.campaignlist}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.campaign && context.campaignlist){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/campaigns/${context.campaign}/campaignlists/${context.campaignlist}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/campaignlists/${context.campaignlist}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof CampaignListServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.ibizlist && context.campaignlist){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/ibizlists/${context.ibizlist}/campaignlists/${context.campaignlist}/save`,data,isloading);
            
            return res;
        }
        if(context.campaign && context.campaignlist){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/campaigns/${context.campaign}/campaignlists/${context.campaignlist}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/campaignlists/${context.campaignlist}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof CampaignListServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.ibizlist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/ibizlists/${context.ibizlist}/campaignlists/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.campaign && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/campaigns/${context.campaign}/campaignlists/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/campaignlists/fetchdefault`,tempData,isloading);
        return res;
    }
}