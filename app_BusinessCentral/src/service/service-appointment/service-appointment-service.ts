import { Http,Util } from '@/utils';
import ServiceAppointmentServiceBase from './service-appointment-service-base';


/**
 * 服务活动服务对象
 *
 * @export
 * @class ServiceAppointmentService
 * @extends {ServiceAppointmentServiceBase}
 */
export default class ServiceAppointmentService extends ServiceAppointmentServiceBase {

    /**
     * Creates an instance of  ServiceAppointmentService.
     * 
     * @param {*} [opts={}]
     * @memberof  ServiceAppointmentService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}