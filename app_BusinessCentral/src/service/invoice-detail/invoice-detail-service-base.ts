import { Http,Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 发票产品服务对象基类
 *
 * @export
 * @class InvoiceDetailServiceBase
 * @extends {EntityServie}
 */
export default class InvoiceDetailServiceBase extends EntityService {

    /**
     * Creates an instance of  InvoiceDetailServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  InvoiceDetailServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof InvoiceDetailServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='invoicedetail';
        this.APPDEKEY = 'invoicedetailid';
        this.APPDENAME = 'invoicedetails';
        this.APPDETEXT = 'invoicedetailname';
        this.APPNAME = 'businesscentral';
        this.SYSTEMNAME = 'ibizbusinesscentral';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof InvoiceDetailServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && context.salesorder && context.invoice && context.invoicedetail){
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/${context.invoicedetail}/select`,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && context.quote && context.salesorder && context.invoice && context.invoicedetail){
            let res:any = Http.getInstance().get(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/${context.invoicedetail}/select`,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && context.quote && context.salesorder && context.invoice && context.invoicedetail){
            let res:any = Http.getInstance().get(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/${context.invoicedetail}/select`,isloading);
            
            return res;
        }
        if(context.opportunity && context.quote && context.salesorder && context.invoice && context.invoicedetail){
            let res:any = Http.getInstance().get(`/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/${context.invoicedetail}/select`,isloading);
            
            return res;
        }
        if(context.quote && context.salesorder && context.invoice && context.invoicedetail){
            let res:any = Http.getInstance().get(`/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/${context.invoicedetail}/select`,isloading);
            
            return res;
        }
        if(context.salesorder && context.invoice && context.invoicedetail){
            let res:any = Http.getInstance().get(`/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/${context.invoicedetail}/select`,isloading);
            
            return res;
        }
        if(context.invoice && context.invoicedetail){
            let res:any = Http.getInstance().get(`/invoices/${context.invoice}/invoicedetails/${context.invoicedetail}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/invoicedetails/${context.invoicedetail}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof InvoiceDetailServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && context.salesorder && context.invoice && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails`,data,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && context.quote && context.salesorder && context.invoice && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails`,data,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && context.quote && context.salesorder && context.invoice && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails`,data,isloading);
            
            return res;
        }
        if(context.opportunity && context.quote && context.salesorder && context.invoice && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails`,data,isloading);
            
            return res;
        }
        if(context.quote && context.salesorder && context.invoice && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails`,data,isloading);
            
            return res;
        }
        if(context.salesorder && context.invoice && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails`,data,isloading);
            
            return res;
        }
        if(context.invoice && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/invoices/${context.invoice}/invoicedetails`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/invoicedetails`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof InvoiceDetailServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && context.salesorder && context.invoice && context.invoicedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/${context.invoicedetail}`,data,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && context.quote && context.salesorder && context.invoice && context.invoicedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/${context.invoicedetail}`,data,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && context.quote && context.salesorder && context.invoice && context.invoicedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/${context.invoicedetail}`,data,isloading);
            
            return res;
        }
        if(context.opportunity && context.quote && context.salesorder && context.invoice && context.invoicedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/${context.invoicedetail}`,data,isloading);
            
            return res;
        }
        if(context.quote && context.salesorder && context.invoice && context.invoicedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/${context.invoicedetail}`,data,isloading);
            
            return res;
        }
        if(context.salesorder && context.invoice && context.invoicedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/${context.invoicedetail}`,data,isloading);
            
            return res;
        }
        if(context.invoice && context.invoicedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/invoices/${context.invoice}/invoicedetails/${context.invoicedetail}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/invoicedetails/${context.invoicedetail}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof InvoiceDetailServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && context.salesorder && context.invoice && context.invoicedetail){
            let res:any = Http.getInstance().delete(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/${context.invoicedetail}`,isloading);
            return res;
        }
        if(context.contact && context.opportunity && context.quote && context.salesorder && context.invoice && context.invoicedetail){
            let res:any = Http.getInstance().delete(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/${context.invoicedetail}`,isloading);
            return res;
        }
        if(context.account && context.opportunity && context.quote && context.salesorder && context.invoice && context.invoicedetail){
            let res:any = Http.getInstance().delete(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/${context.invoicedetail}`,isloading);
            return res;
        }
        if(context.opportunity && context.quote && context.salesorder && context.invoice && context.invoicedetail){
            let res:any = Http.getInstance().delete(`/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/${context.invoicedetail}`,isloading);
            return res;
        }
        if(context.quote && context.salesorder && context.invoice && context.invoicedetail){
            let res:any = Http.getInstance().delete(`/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/${context.invoicedetail}`,isloading);
            return res;
        }
        if(context.salesorder && context.invoice && context.invoicedetail){
            let res:any = Http.getInstance().delete(`/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/${context.invoicedetail}`,isloading);
            return res;
        }
        if(context.invoice && context.invoicedetail){
            let res:any = Http.getInstance().delete(`/invoices/${context.invoice}/invoicedetails/${context.invoicedetail}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/invoicedetails/${context.invoicedetail}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof InvoiceDetailServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && context.salesorder && context.invoice && context.invoicedetail){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/${context.invoicedetail}`,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && context.quote && context.salesorder && context.invoice && context.invoicedetail){
            let res:any = await Http.getInstance().get(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/${context.invoicedetail}`,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && context.quote && context.salesorder && context.invoice && context.invoicedetail){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/${context.invoicedetail}`,isloading);
            
            return res;
        }
        if(context.opportunity && context.quote && context.salesorder && context.invoice && context.invoicedetail){
            let res:any = await Http.getInstance().get(`/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/${context.invoicedetail}`,isloading);
            
            return res;
        }
        if(context.quote && context.salesorder && context.invoice && context.invoicedetail){
            let res:any = await Http.getInstance().get(`/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/${context.invoicedetail}`,isloading);
            
            return res;
        }
        if(context.salesorder && context.invoice && context.invoicedetail){
            let res:any = await Http.getInstance().get(`/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/${context.invoicedetail}`,isloading);
            
            return res;
        }
        if(context.invoice && context.invoicedetail){
            let res:any = await Http.getInstance().get(`/invoices/${context.invoice}/invoicedetails/${context.invoicedetail}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/invoicedetails/${context.invoicedetail}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof InvoiceDetailServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && context.salesorder && context.invoice && true){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/getdraft`,isloading);
            res.data.invoicedetail = data.invoicedetail;
            
            return res;
        }
        if(context.contact && context.opportunity && context.quote && context.salesorder && context.invoice && true){
            let res:any = await Http.getInstance().get(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/getdraft`,isloading);
            res.data.invoicedetail = data.invoicedetail;
            
            return res;
        }
        if(context.account && context.opportunity && context.quote && context.salesorder && context.invoice && true){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/getdraft`,isloading);
            res.data.invoicedetail = data.invoicedetail;
            
            return res;
        }
        if(context.opportunity && context.quote && context.salesorder && context.invoice && true){
            let res:any = await Http.getInstance().get(`/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/getdraft`,isloading);
            res.data.invoicedetail = data.invoicedetail;
            
            return res;
        }
        if(context.quote && context.salesorder && context.invoice && true){
            let res:any = await Http.getInstance().get(`/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/getdraft`,isloading);
            res.data.invoicedetail = data.invoicedetail;
            
            return res;
        }
        if(context.salesorder && context.invoice && true){
            let res:any = await Http.getInstance().get(`/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/getdraft`,isloading);
            res.data.invoicedetail = data.invoicedetail;
            
            return res;
        }
        if(context.invoice && true){
            let res:any = await Http.getInstance().get(`/invoices/${context.invoice}/invoicedetails/getdraft`,isloading);
            res.data.invoicedetail = data.invoicedetail;
            
            return res;
        }
        let res:any = await  Http.getInstance().get(`/invoicedetails/getdraft`,isloading);
        res.data.invoicedetail = data.invoicedetail;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof InvoiceDetailServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && context.salesorder && context.invoice && context.invoicedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/${context.invoicedetail}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && context.quote && context.salesorder && context.invoice && context.invoicedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/${context.invoicedetail}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && context.quote && context.salesorder && context.invoice && context.invoicedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/${context.invoicedetail}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.opportunity && context.quote && context.salesorder && context.invoice && context.invoicedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/${context.invoicedetail}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.quote && context.salesorder && context.invoice && context.invoicedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/${context.invoicedetail}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.salesorder && context.invoice && context.invoicedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/${context.invoicedetail}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.invoice && context.invoicedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/invoices/${context.invoice}/invoicedetails/${context.invoicedetail}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/invoicedetails/${context.invoicedetail}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof InvoiceDetailServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && context.salesorder && context.invoice && context.invoicedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/${context.invoicedetail}/save`,data,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && context.quote && context.salesorder && context.invoice && context.invoicedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/${context.invoicedetail}/save`,data,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && context.quote && context.salesorder && context.invoice && context.invoicedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/${context.invoicedetail}/save`,data,isloading);
            
            return res;
        }
        if(context.opportunity && context.quote && context.salesorder && context.invoice && context.invoicedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/${context.invoicedetail}/save`,data,isloading);
            
            return res;
        }
        if(context.quote && context.salesorder && context.invoice && context.invoicedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/${context.invoicedetail}/save`,data,isloading);
            
            return res;
        }
        if(context.salesorder && context.invoice && context.invoicedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/${context.invoicedetail}/save`,data,isloading);
            
            return res;
        }
        if(context.invoice && context.invoicedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/invoices/${context.invoice}/invoicedetails/${context.invoicedetail}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/invoicedetails/${context.invoicedetail}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof InvoiceDetailServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.quote && context.salesorder && context.invoice && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.contact && context.opportunity && context.quote && context.salesorder && context.invoice && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/contacts/${context.contact}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.account && context.opportunity && context.quote && context.salesorder && context.invoice && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.opportunity && context.quote && context.salesorder && context.invoice && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/opportunities/${context.opportunity}/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.quote && context.salesorder && context.invoice && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/quotes/${context.quote}/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.salesorder && context.invoice && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/salesorders/${context.salesorder}/invoices/${context.invoice}/invoicedetails/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.invoice && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/invoices/${context.invoice}/invoicedetails/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/invoicedetails/fetchdefault`,tempData,isloading);
        return res;
    }
}