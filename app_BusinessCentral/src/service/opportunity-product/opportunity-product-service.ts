import { Http,Util } from '@/utils';
import OpportunityProductServiceBase from './opportunity-product-service-base';


/**
 * 商机产品服务对象
 *
 * @export
 * @class OpportunityProductService
 * @extends {OpportunityProductServiceBase}
 */
export default class OpportunityProductService extends OpportunityProductServiceBase {

    /**
     * Creates an instance of  OpportunityProductService.
     * 
     * @param {*} [opts={}]
     * @memberof  OpportunityProductService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}