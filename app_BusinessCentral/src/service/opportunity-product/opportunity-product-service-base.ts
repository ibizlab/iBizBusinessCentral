import { Http,Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 商机产品服务对象基类
 *
 * @export
 * @class OpportunityProductServiceBase
 * @extends {EntityServie}
 */
export default class OpportunityProductServiceBase extends EntityService {

    /**
     * Creates an instance of  OpportunityProductServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  OpportunityProductServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof OpportunityProductServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='opportunityproduct';
        this.APPDEKEY = 'opportunityproductid';
        this.APPDENAME = 'opportunityproducts';
        this.APPDETEXT = 'opportunityproductname';
        this.APPNAME = 'businesscentral';
        this.SYSTEMNAME = 'ibizbusinesscentral';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof OpportunityProductServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.opportunityproduct){
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/opportunityproducts/${context.opportunityproduct}/select`,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && context.opportunityproduct){
            let res:any = Http.getInstance().get(`/contacts/${context.contact}/opportunities/${context.opportunity}/opportunityproducts/${context.opportunityproduct}/select`,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && context.opportunityproduct){
            let res:any = Http.getInstance().get(`/accounts/${context.account}/opportunities/${context.opportunity}/opportunityproducts/${context.opportunityproduct}/select`,isloading);
            
            return res;
        }
        if(context.opportunity && context.opportunityproduct){
            let res:any = Http.getInstance().get(`/opportunities/${context.opportunity}/opportunityproducts/${context.opportunityproduct}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/opportunityproducts/${context.opportunityproduct}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof OpportunityProductServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/opportunityproducts`,data,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/contacts/${context.contact}/opportunities/${context.opportunity}/opportunityproducts`,data,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/opportunities/${context.opportunity}/opportunityproducts`,data,isloading);
            
            return res;
        }
        if(context.opportunity && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/opportunities/${context.opportunity}/opportunityproducts`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/opportunityproducts`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof OpportunityProductServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.opportunityproduct){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/opportunityproducts/${context.opportunityproduct}`,data,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && context.opportunityproduct){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/contacts/${context.contact}/opportunities/${context.opportunity}/opportunityproducts/${context.opportunityproduct}`,data,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && context.opportunityproduct){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/accounts/${context.account}/opportunities/${context.opportunity}/opportunityproducts/${context.opportunityproduct}`,data,isloading);
            
            return res;
        }
        if(context.opportunity && context.opportunityproduct){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/opportunities/${context.opportunity}/opportunityproducts/${context.opportunityproduct}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/opportunityproducts/${context.opportunityproduct}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof OpportunityProductServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.opportunityproduct){
            let res:any = Http.getInstance().delete(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/opportunityproducts/${context.opportunityproduct}`,isloading);
            return res;
        }
        if(context.contact && context.opportunity && context.opportunityproduct){
            let res:any = Http.getInstance().delete(`/contacts/${context.contact}/opportunities/${context.opportunity}/opportunityproducts/${context.opportunityproduct}`,isloading);
            return res;
        }
        if(context.account && context.opportunity && context.opportunityproduct){
            let res:any = Http.getInstance().delete(`/accounts/${context.account}/opportunities/${context.opportunity}/opportunityproducts/${context.opportunityproduct}`,isloading);
            return res;
        }
        if(context.opportunity && context.opportunityproduct){
            let res:any = Http.getInstance().delete(`/opportunities/${context.opportunity}/opportunityproducts/${context.opportunityproduct}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/opportunityproducts/${context.opportunityproduct}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof OpportunityProductServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.opportunityproduct){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/opportunityproducts/${context.opportunityproduct}`,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && context.opportunityproduct){
            let res:any = await Http.getInstance().get(`/contacts/${context.contact}/opportunities/${context.opportunity}/opportunityproducts/${context.opportunityproduct}`,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && context.opportunityproduct){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/opportunities/${context.opportunity}/opportunityproducts/${context.opportunityproduct}`,isloading);
            
            return res;
        }
        if(context.opportunity && context.opportunityproduct){
            let res:any = await Http.getInstance().get(`/opportunities/${context.opportunity}/opportunityproducts/${context.opportunityproduct}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/opportunityproducts/${context.opportunityproduct}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof OpportunityProductServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && true){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/opportunityproducts/getdraft`,isloading);
            res.data.opportunityproduct = data.opportunityproduct;
            
            return res;
        }
        if(context.contact && context.opportunity && true){
            let res:any = await Http.getInstance().get(`/contacts/${context.contact}/opportunities/${context.opportunity}/opportunityproducts/getdraft`,isloading);
            res.data.opportunityproduct = data.opportunityproduct;
            
            return res;
        }
        if(context.account && context.opportunity && true){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/opportunities/${context.opportunity}/opportunityproducts/getdraft`,isloading);
            res.data.opportunityproduct = data.opportunityproduct;
            
            return res;
        }
        if(context.opportunity && true){
            let res:any = await Http.getInstance().get(`/opportunities/${context.opportunity}/opportunityproducts/getdraft`,isloading);
            res.data.opportunityproduct = data.opportunityproduct;
            
            return res;
        }
        let res:any = await  Http.getInstance().get(`/opportunityproducts/getdraft`,isloading);
        res.data.opportunityproduct = data.opportunityproduct;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof OpportunityProductServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.opportunityproduct){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/opportunityproducts/${context.opportunityproduct}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && context.opportunityproduct){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/contacts/${context.contact}/opportunities/${context.opportunity}/opportunityproducts/${context.opportunityproduct}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && context.opportunityproduct){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/opportunities/${context.opportunity}/opportunityproducts/${context.opportunityproduct}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.opportunity && context.opportunityproduct){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/opportunities/${context.opportunity}/opportunityproducts/${context.opportunityproduct}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/opportunityproducts/${context.opportunityproduct}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof OpportunityProductServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.opportunityproduct){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/opportunityproducts/${context.opportunityproduct}/save`,data,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && context.opportunityproduct){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/contacts/${context.contact}/opportunities/${context.opportunity}/opportunityproducts/${context.opportunityproduct}/save`,data,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && context.opportunityproduct){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/opportunities/${context.opportunity}/opportunityproducts/${context.opportunityproduct}/save`,data,isloading);
            
            return res;
        }
        if(context.opportunity && context.opportunityproduct){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/opportunities/${context.opportunity}/opportunityproducts/${context.opportunityproduct}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/opportunityproducts/${context.opportunityproduct}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof OpportunityProductServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/opportunityproducts/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.contact && context.opportunity && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/contacts/${context.contact}/opportunities/${context.opportunity}/opportunityproducts/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.account && context.opportunity && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/opportunities/${context.opportunity}/opportunityproducts/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.opportunity && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/opportunities/${context.opportunity}/opportunityproducts/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/opportunityproducts/fetchdefault`,tempData,isloading);
        return res;
    }
}