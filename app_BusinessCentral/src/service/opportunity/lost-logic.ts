import { Http,Util } from '@/utils';
import LostLogicBase from './lost-logic-base';

/**
 * 作为丢单结束
 *
 * @export
 * @class LostLogic
 */
export default class LostLogic extends LostLogicBase{

    /**
     * Creates an instance of  LostLogic
     * 
     * @param {*} [opts={}]
     * @memberof  LostLogic
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}