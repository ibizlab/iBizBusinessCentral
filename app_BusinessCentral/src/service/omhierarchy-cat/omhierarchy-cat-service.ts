import { Http,Util } from '@/utils';
import OMHierarchyCatServiceBase from './omhierarchy-cat-service-base';


/**
 * 结构层次类别服务对象
 *
 * @export
 * @class OMHierarchyCatService
 * @extends {OMHierarchyCatServiceBase}
 */
export default class OMHierarchyCatService extends OMHierarchyCatServiceBase {

    /**
     * Creates an instance of  OMHierarchyCatService.
     * 
     * @param {*} [opts={}]
     * @memberof  OMHierarchyCatService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}