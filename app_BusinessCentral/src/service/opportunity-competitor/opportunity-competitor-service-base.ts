import { Http,Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 商机对手服务对象基类
 *
 * @export
 * @class OpportunityCompetitorServiceBase
 * @extends {EntityServie}
 */
export default class OpportunityCompetitorServiceBase extends EntityService {

    /**
     * Creates an instance of  OpportunityCompetitorServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  OpportunityCompetitorServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof OpportunityCompetitorServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='opportunitycompetitor';
        this.APPDEKEY = 'relationshipsid';
        this.APPDENAME = 'opportunitycompetitors';
        this.APPDETEXT = 'relationshipsname';
        this.APPNAME = 'businesscentral';
        this.SYSTEMNAME = 'ibizbusinesscentral';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof OpportunityCompetitorServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.opportunitycompetitor){
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/opportunitycompetitors/${context.opportunitycompetitor}/select`,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && context.opportunitycompetitor){
            let res:any = Http.getInstance().get(`/contacts/${context.contact}/opportunities/${context.opportunity}/opportunitycompetitors/${context.opportunitycompetitor}/select`,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && context.opportunitycompetitor){
            let res:any = Http.getInstance().get(`/accounts/${context.account}/opportunities/${context.opportunity}/opportunitycompetitors/${context.opportunitycompetitor}/select`,isloading);
            
            return res;
        }
        if(context.opportunity && context.opportunitycompetitor){
            let res:any = Http.getInstance().get(`/opportunities/${context.opportunity}/opportunitycompetitors/${context.opportunitycompetitor}/select`,isloading);
            
            return res;
        }
        if(context.competitor && context.opportunitycompetitor){
            let res:any = Http.getInstance().get(`/competitors/${context.competitor}/opportunitycompetitors/${context.opportunitycompetitor}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/opportunitycompetitors/${context.opportunitycompetitor}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof OpportunityCompetitorServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/opportunitycompetitors`,data,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/contacts/${context.contact}/opportunities/${context.opportunity}/opportunitycompetitors`,data,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/opportunities/${context.opportunity}/opportunitycompetitors`,data,isloading);
            
            return res;
        }
        if(context.opportunity && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/opportunities/${context.opportunity}/opportunitycompetitors`,data,isloading);
            
            return res;
        }
        if(context.competitor && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/competitors/${context.competitor}/opportunitycompetitors`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/opportunitycompetitors`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof OpportunityCompetitorServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.opportunitycompetitor){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/opportunitycompetitors/${context.opportunitycompetitor}`,data,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && context.opportunitycompetitor){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/contacts/${context.contact}/opportunities/${context.opportunity}/opportunitycompetitors/${context.opportunitycompetitor}`,data,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && context.opportunitycompetitor){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/accounts/${context.account}/opportunities/${context.opportunity}/opportunitycompetitors/${context.opportunitycompetitor}`,data,isloading);
            
            return res;
        }
        if(context.opportunity && context.opportunitycompetitor){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/opportunities/${context.opportunity}/opportunitycompetitors/${context.opportunitycompetitor}`,data,isloading);
            
            return res;
        }
        if(context.competitor && context.opportunitycompetitor){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/competitors/${context.competitor}/opportunitycompetitors/${context.opportunitycompetitor}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/opportunitycompetitors/${context.opportunitycompetitor}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof OpportunityCompetitorServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.opportunitycompetitor){
            let res:any = Http.getInstance().delete(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/opportunitycompetitors/${context.opportunitycompetitor}`,isloading);
            return res;
        }
        if(context.contact && context.opportunity && context.opportunitycompetitor){
            let res:any = Http.getInstance().delete(`/contacts/${context.contact}/opportunities/${context.opportunity}/opportunitycompetitors/${context.opportunitycompetitor}`,isloading);
            return res;
        }
        if(context.account && context.opportunity && context.opportunitycompetitor){
            let res:any = Http.getInstance().delete(`/accounts/${context.account}/opportunities/${context.opportunity}/opportunitycompetitors/${context.opportunitycompetitor}`,isloading);
            return res;
        }
        if(context.opportunity && context.opportunitycompetitor){
            let res:any = Http.getInstance().delete(`/opportunities/${context.opportunity}/opportunitycompetitors/${context.opportunitycompetitor}`,isloading);
            return res;
        }
        if(context.competitor && context.opportunitycompetitor){
            let res:any = Http.getInstance().delete(`/competitors/${context.competitor}/opportunitycompetitors/${context.opportunitycompetitor}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/opportunitycompetitors/${context.opportunitycompetitor}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof OpportunityCompetitorServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.opportunitycompetitor){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/opportunitycompetitors/${context.opportunitycompetitor}`,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && context.opportunitycompetitor){
            let res:any = await Http.getInstance().get(`/contacts/${context.contact}/opportunities/${context.opportunity}/opportunitycompetitors/${context.opportunitycompetitor}`,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && context.opportunitycompetitor){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/opportunities/${context.opportunity}/opportunitycompetitors/${context.opportunitycompetitor}`,isloading);
            
            return res;
        }
        if(context.opportunity && context.opportunitycompetitor){
            let res:any = await Http.getInstance().get(`/opportunities/${context.opportunity}/opportunitycompetitors/${context.opportunitycompetitor}`,isloading);
            
            return res;
        }
        if(context.competitor && context.opportunitycompetitor){
            let res:any = await Http.getInstance().get(`/competitors/${context.competitor}/opportunitycompetitors/${context.opportunitycompetitor}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/opportunitycompetitors/${context.opportunitycompetitor}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof OpportunityCompetitorServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && true){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/opportunitycompetitors/getdraft`,isloading);
            res.data.opportunitycompetitor = data.opportunitycompetitor;
            
            return res;
        }
        if(context.contact && context.opportunity && true){
            let res:any = await Http.getInstance().get(`/contacts/${context.contact}/opportunities/${context.opportunity}/opportunitycompetitors/getdraft`,isloading);
            res.data.opportunitycompetitor = data.opportunitycompetitor;
            
            return res;
        }
        if(context.account && context.opportunity && true){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/opportunities/${context.opportunity}/opportunitycompetitors/getdraft`,isloading);
            res.data.opportunitycompetitor = data.opportunitycompetitor;
            
            return res;
        }
        if(context.opportunity && true){
            let res:any = await Http.getInstance().get(`/opportunities/${context.opportunity}/opportunitycompetitors/getdraft`,isloading);
            res.data.opportunitycompetitor = data.opportunitycompetitor;
            
            return res;
        }
        if(context.competitor && true){
            let res:any = await Http.getInstance().get(`/competitors/${context.competitor}/opportunitycompetitors/getdraft`,isloading);
            res.data.opportunitycompetitor = data.opportunitycompetitor;
            
            return res;
        }
        let res:any = await  Http.getInstance().get(`/opportunitycompetitors/getdraft`,isloading);
        res.data.opportunitycompetitor = data.opportunitycompetitor;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof OpportunityCompetitorServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.opportunitycompetitor){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/opportunitycompetitors/${context.opportunitycompetitor}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && context.opportunitycompetitor){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/contacts/${context.contact}/opportunities/${context.opportunity}/opportunitycompetitors/${context.opportunitycompetitor}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && context.opportunitycompetitor){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/opportunities/${context.opportunity}/opportunitycompetitors/${context.opportunitycompetitor}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.opportunity && context.opportunitycompetitor){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/opportunities/${context.opportunity}/opportunitycompetitors/${context.opportunitycompetitor}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.competitor && context.opportunitycompetitor){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/competitors/${context.competitor}/opportunitycompetitors/${context.opportunitycompetitor}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/opportunitycompetitors/${context.opportunitycompetitor}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof OpportunityCompetitorServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && context.opportunitycompetitor){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/opportunitycompetitors/${context.opportunitycompetitor}/save`,data,isloading);
            
            return res;
        }
        if(context.contact && context.opportunity && context.opportunitycompetitor){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/contacts/${context.contact}/opportunities/${context.opportunity}/opportunitycompetitors/${context.opportunitycompetitor}/save`,data,isloading);
            
            return res;
        }
        if(context.account && context.opportunity && context.opportunitycompetitor){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/opportunities/${context.opportunity}/opportunitycompetitors/${context.opportunitycompetitor}/save`,data,isloading);
            
            return res;
        }
        if(context.opportunity && context.opportunitycompetitor){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/opportunities/${context.opportunity}/opportunitycompetitors/${context.opportunitycompetitor}/save`,data,isloading);
            
            return res;
        }
        if(context.competitor && context.opportunitycompetitor){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/competitors/${context.competitor}/opportunitycompetitors/${context.opportunitycompetitor}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/opportunitycompetitors/${context.opportunitycompetitor}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof OpportunityCompetitorServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.opportunity && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/opportunities/${context.opportunity}/opportunitycompetitors/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.contact && context.opportunity && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/contacts/${context.contact}/opportunities/${context.opportunity}/opportunitycompetitors/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.account && context.opportunity && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/opportunities/${context.opportunity}/opportunitycompetitors/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.opportunity && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/opportunities/${context.opportunity}/opportunitycompetitors/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.competitor && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/competitors/${context.competitor}/opportunitycompetitors/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/opportunitycompetitors/fetchdefault`,tempData,isloading);
        return res;
    }
}