import { Http,Util } from '@/utils';
import TerritoryServiceBase from './territory-service-base';


/**
 * 区域服务对象
 *
 * @export
 * @class TerritoryService
 * @extends {TerritoryServiceBase}
 */
export default class TerritoryService extends TerritoryServiceBase {

    /**
     * Creates an instance of  TerritoryService.
     * 
     * @param {*} [opts={}]
     * @memberof  TerritoryService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}