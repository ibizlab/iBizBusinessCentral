import { Http,Util } from '@/utils';
import WebSiteContentServiceBase from './web-site-content-service-base';


/**
 * 站点内容服务对象
 *
 * @export
 * @class WebSiteContentService
 * @extends {WebSiteContentServiceBase}
 */
export default class WebSiteContentService extends WebSiteContentServiceBase {

    /**
     * Creates an instance of  WebSiteContentService.
     * 
     * @param {*} [opts={}]
     * @memberof  WebSiteContentService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}