import { Http,Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 竞争对手产品服务对象基类
 *
 * @export
 * @class CompetitorProductServiceBase
 * @extends {EntityServie}
 */
export default class CompetitorProductServiceBase extends EntityService {

    /**
     * Creates an instance of  CompetitorProductServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  CompetitorProductServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof CompetitorProductServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='competitorproduct';
        this.APPDEKEY = 'relationshipsid';
        this.APPDENAME = 'competitorproducts';
        this.APPDETEXT = 'relationshipsname';
        this.APPNAME = 'businesscentral';
        this.SYSTEMNAME = 'ibizbusinesscentral';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof CompetitorProductServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.competitor && context.competitorproduct){
            let res:any = Http.getInstance().get(`/competitors/${context.competitor}/competitorproducts/${context.competitorproduct}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/competitorproducts/${context.competitorproduct}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof CompetitorProductServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.competitor && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/competitors/${context.competitor}/competitorproducts`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/competitorproducts`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof CompetitorProductServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.competitor && context.competitorproduct){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/competitors/${context.competitor}/competitorproducts/${context.competitorproduct}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/competitorproducts/${context.competitorproduct}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof CompetitorProductServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.competitor && context.competitorproduct){
            let res:any = Http.getInstance().delete(`/competitors/${context.competitor}/competitorproducts/${context.competitorproduct}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/competitorproducts/${context.competitorproduct}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof CompetitorProductServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.competitor && context.competitorproduct){
            let res:any = await Http.getInstance().get(`/competitors/${context.competitor}/competitorproducts/${context.competitorproduct}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/competitorproducts/${context.competitorproduct}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof CompetitorProductServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.competitor && true){
            let res:any = await Http.getInstance().get(`/competitors/${context.competitor}/competitorproducts/getdraft`,isloading);
            res.data.competitorproduct = data.competitorproduct;
            
            return res;
        }
        let res:any = await  Http.getInstance().get(`/competitorproducts/getdraft`,isloading);
        res.data.competitorproduct = data.competitorproduct;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof CompetitorProductServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.competitor && context.competitorproduct){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/competitors/${context.competitor}/competitorproducts/${context.competitorproduct}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/competitorproducts/${context.competitorproduct}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof CompetitorProductServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.competitor && context.competitorproduct){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/competitors/${context.competitor}/competitorproducts/${context.competitorproduct}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/competitorproducts/${context.competitorproduct}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof CompetitorProductServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.competitor && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/competitors/${context.competitor}/competitorproducts/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/competitorproducts/fetchdefault`,tempData,isloading);
        return res;
    }
}