import { Http,Util } from '@/utils';
import EmailServiceBase from './email-service-base';


/**
 * 电子邮件服务对象
 *
 * @export
 * @class EmailService
 * @extends {EmailServiceBase}
 */
export default class EmailService extends EmailServiceBase {

    /**
     * Creates an instance of  EmailService.
     * 
     * @param {*} [opts={}]
     * @memberof  EmailService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}