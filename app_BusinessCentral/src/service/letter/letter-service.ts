import { Http,Util } from '@/utils';
import LetterServiceBase from './letter-service-base';


/**
 * 信件服务对象
 *
 * @export
 * @class LetterService
 * @extends {LetterServiceBase}
 */
export default class LetterService extends LetterServiceBase {

    /**
     * Creates an instance of  LetterService.
     * 
     * @param {*} [opts={}]
     * @memberof  LetterService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}