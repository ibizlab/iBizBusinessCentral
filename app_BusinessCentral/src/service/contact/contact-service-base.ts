import { Http,Util } from '@/utils';
import EntityService from '../entity-service';
import ActiveLogic from '@/service/contact/active-logic';
import InactiveLogic from '@/service/contact/inactive-logic';



/**
 * 联系人服务对象基类
 *
 * @export
 * @class ContactServiceBase
 * @extends {EntityServie}
 */
export default class ContactServiceBase extends EntityService {

    /**
     * Creates an instance of  ContactServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  ContactServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof ContactServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='contact';
        this.APPDEKEY = 'contactid';
        this.APPDENAME = 'contacts';
        this.APPDETEXT = 'fullname';
        this.APPNAME = 'businesscentral';
        this.SYSTEMNAME = 'ibizbusinesscentral';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ContactServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact){
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/contacts/${context.contact}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ContactServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_leads',JSON.stringify(res.data.leads?res.data.leads:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_listcontacts',JSON.stringify(res.data.listcontacts?res.data.listcontacts:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_opportunities',JSON.stringify(res.data.opportunities?res.data.opportunities:[]));
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/contacts`,data,isloading);
        this.tempStorage.setItem(tempContext.srfsessionkey+'_leads',JSON.stringify(res.data.leads?res.data.leads:[]));
        this.tempStorage.setItem(tempContext.srfsessionkey+'_listcontacts',JSON.stringify(res.data.listcontacts?res.data.listcontacts:[]));
        this.tempStorage.setItem(tempContext.srfsessionkey+'_opportunities',JSON.stringify(res.data.opportunities?res.data.opportunities:[]));
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ContactServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/accounts/${context.account}/contacts/${context.contact}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/contacts/${context.contact}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ContactServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact){
            let res:any = Http.getInstance().delete(`/accounts/${context.account}/contacts/${context.contact}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/contacts/${context.contact}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ContactServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/contacts/${context.contact}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ContactServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && true){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/contacts/getdraft`,isloading);
            res.data.contact = data.contact;
            
            return res;
        }
        let res:any = await  Http.getInstance().get(`/contacts/getdraft`,isloading);
        res.data.contact = data.contact;
        
        return res;
    }

    /**
     * Active接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ContactServiceBase
     */
    public async Active(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        let appLogic:ActiveLogic = new ActiveLogic({context:JSON.parse(JSON.stringify(context)),data:JSON.parse(JSON.stringify(data))});
        const res = await appLogic.onExecute(context,data,isloading?true:false);
        return {status:200,data:res};
    }

    /**
     * AddList接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ContactServiceBase
     */
    public async AddList(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/addlist`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/contacts/${context.contact}/addlist`,data,isloading);
            return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ContactServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/contacts/${context.contact}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Inactive接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ContactServiceBase
     */
    public async Inactive(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        let appLogic:InactiveLogic = new InactiveLogic({context:JSON.parse(JSON.stringify(context)),data:JSON.parse(JSON.stringify(data))});
        const res = await appLogic.onExecute(context,data,isloading?true:false);
        return {status:200,data:res};
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ContactServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/contacts/${context.contact}/save`,data,isloading);
            
            return res;
    }

    /**
     * SetPrimary接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ContactServiceBase
     */
    public async SetPrimary(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/setprimary`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/contacts/${context.contact}/setprimary`,data,isloading);
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ContactServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/contacts/fetchdefault`,tempData,isloading);
        return res;
    }

    /**
     * FetchStop接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ContactServiceBase
     */
    public async FetchStop(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/fetchstop`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/contacts/fetchstop`,tempData,isloading);
        return res;
    }

    /**
     * FetchUsable接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ContactServiceBase
     */
    public async FetchUsable(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/fetchusable`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/contacts/fetchusable`,tempData,isloading);
        return res;
    }
}