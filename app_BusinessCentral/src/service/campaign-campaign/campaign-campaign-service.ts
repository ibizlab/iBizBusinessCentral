import { Http,Util } from '@/utils';
import CampaignCampaignServiceBase from './campaign-campaign-service-base';


/**
 * 市场活动-市场活动服务对象
 *
 * @export
 * @class CampaignCampaignService
 * @extends {CampaignCampaignServiceBase}
 */
export default class CampaignCampaignService extends CampaignCampaignServiceBase {

    /**
     * Creates an instance of  CampaignCampaignService.
     * 
     * @param {*} [opts={}]
     * @memberof  CampaignCampaignService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}