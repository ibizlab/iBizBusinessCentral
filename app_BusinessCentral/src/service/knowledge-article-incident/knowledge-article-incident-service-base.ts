import { Http,Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 知识文章事件服务对象基类
 *
 * @export
 * @class KnowledgeArticleIncidentServiceBase
 * @extends {EntityServie}
 */
export default class KnowledgeArticleIncidentServiceBase extends EntityService {

    /**
     * Creates an instance of  KnowledgeArticleIncidentServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  KnowledgeArticleIncidentServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof KnowledgeArticleIncidentServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='knowledgearticleincident';
        this.APPDEKEY = 'knowledgearticleincidentid';
        this.APPDENAME = 'knowledgearticleincidents';
        this.APPDETEXT = 'statecode';
        this.APPNAME = 'businesscentral';
        this.SYSTEMNAME = 'ibizbusinesscentral';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof KnowledgeArticleIncidentServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.knowledgearticle && context.knowledgearticleincident){
            let res:any = Http.getInstance().get(`/knowledgearticles/${context.knowledgearticle}/knowledgearticleincidents/${context.knowledgearticleincident}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/knowledgearticleincidents/${context.knowledgearticleincident}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof KnowledgeArticleIncidentServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.knowledgearticle && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/knowledgearticles/${context.knowledgearticle}/knowledgearticleincidents`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/knowledgearticleincidents`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof KnowledgeArticleIncidentServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.knowledgearticle && context.knowledgearticleincident){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/knowledgearticles/${context.knowledgearticle}/knowledgearticleincidents/${context.knowledgearticleincident}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/knowledgearticleincidents/${context.knowledgearticleincident}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof KnowledgeArticleIncidentServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.knowledgearticle && context.knowledgearticleincident){
            let res:any = Http.getInstance().delete(`/knowledgearticles/${context.knowledgearticle}/knowledgearticleincidents/${context.knowledgearticleincident}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/knowledgearticleincidents/${context.knowledgearticleincident}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof KnowledgeArticleIncidentServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.knowledgearticle && context.knowledgearticleincident){
            let res:any = await Http.getInstance().get(`/knowledgearticles/${context.knowledgearticle}/knowledgearticleincidents/${context.knowledgearticleincident}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/knowledgearticleincidents/${context.knowledgearticleincident}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof KnowledgeArticleIncidentServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.knowledgearticle && true){
            let res:any = await Http.getInstance().get(`/knowledgearticles/${context.knowledgearticle}/knowledgearticleincidents/getdraft`,isloading);
            res.data.knowledgearticleincident = data.knowledgearticleincident;
            
            return res;
        }
        let res:any = await  Http.getInstance().get(`/knowledgearticleincidents/getdraft`,isloading);
        res.data.knowledgearticleincident = data.knowledgearticleincident;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof KnowledgeArticleIncidentServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.knowledgearticle && context.knowledgearticleincident){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/knowledgearticles/${context.knowledgearticle}/knowledgearticleincidents/${context.knowledgearticleincident}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/knowledgearticleincidents/${context.knowledgearticleincident}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof KnowledgeArticleIncidentServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.knowledgearticle && context.knowledgearticleincident){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/knowledgearticles/${context.knowledgearticle}/knowledgearticleincidents/${context.knowledgearticleincident}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/knowledgearticleincidents/${context.knowledgearticleincident}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof KnowledgeArticleIncidentServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.knowledgearticle && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/knowledgearticles/${context.knowledgearticle}/knowledgearticleincidents/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/knowledgearticleincidents/fetchdefault`,tempData,isloading);
        return res;
    }
}