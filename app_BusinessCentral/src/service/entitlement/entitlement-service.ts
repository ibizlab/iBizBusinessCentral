import { Http,Util } from '@/utils';
import EntitlementServiceBase from './entitlement-service-base';


/**
 * 权利服务对象
 *
 * @export
 * @class EntitlementService
 * @extends {EntitlementServiceBase}
 */
export default class EntitlementService extends EntitlementServiceBase {

    /**
     * Creates an instance of  EntitlementService.
     * 
     * @param {*} [opts={}]
     * @memberof  EntitlementService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}