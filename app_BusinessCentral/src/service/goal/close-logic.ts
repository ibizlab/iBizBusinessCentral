import { Http,Util } from '@/utils';
import CloseLogicBase from './close-logic-base';

/**
 * 关闭
 *
 * @export
 * @class CloseLogic
 */
export default class CloseLogic extends CloseLogicBase{

    /**
     * Creates an instance of  CloseLogic
     * 
     * @param {*} [opts={}]
     * @memberof  CloseLogic
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}