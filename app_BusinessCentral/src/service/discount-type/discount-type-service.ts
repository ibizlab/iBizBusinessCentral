import { Http,Util } from '@/utils';
import DiscountTypeServiceBase from './discount-type-service-base';


/**
 * 折扣表服务对象
 *
 * @export
 * @class DiscountTypeService
 * @extends {DiscountTypeServiceBase}
 */
export default class DiscountTypeService extends DiscountTypeServiceBase {

    /**
     * Creates an instance of  DiscountTypeService.
     * 
     * @param {*} [opts={}]
     * @memberof  DiscountTypeService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}