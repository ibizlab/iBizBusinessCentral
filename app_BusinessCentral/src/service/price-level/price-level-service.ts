import { Http,Util } from '@/utils';
import PriceLevelServiceBase from './price-level-service-base';


/**
 * 价目表服务对象
 *
 * @export
 * @class PriceLevelService
 * @extends {PriceLevelServiceBase}
 */
export default class PriceLevelService extends PriceLevelServiceBase {

    /**
     * Creates an instance of  PriceLevelService.
     * 
     * @param {*} [opts={}]
     * @memberof  PriceLevelService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}