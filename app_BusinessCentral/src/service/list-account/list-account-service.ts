import { Http,Util } from '@/utils';
import ListAccountServiceBase from './list-account-service-base';


/**
 * 营销列表-账户服务对象
 *
 * @export
 * @class ListAccountService
 * @extends {ListAccountServiceBase}
 */
export default class ListAccountService extends ListAccountServiceBase {

    /**
     * Creates an instance of  ListAccountService.
     * 
     * @param {*} [opts={}]
     * @memberof  ListAccountService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}