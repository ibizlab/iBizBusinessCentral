import { Http,Util } from '@/utils';
import IncidentCustomerServiceBase from './incident-customer-service-base';


/**
 * 案例客户服务对象
 *
 * @export
 * @class IncidentCustomerService
 * @extends {IncidentCustomerServiceBase}
 */
export default class IncidentCustomerService extends IncidentCustomerServiceBase {

    /**
     * Creates an instance of  IncidentCustomerService.
     * 
     * @param {*} [opts={}]
     * @memberof  IncidentCustomerService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}