import { Http,Util } from '@/utils';
import QuoteDetailServiceBase from './quote-detail-service-base';


/**
 * 报价单产品服务对象
 *
 * @export
 * @class QuoteDetailService
 * @extends {QuoteDetailServiceBase}
 */
export default class QuoteDetailService extends QuoteDetailServiceBase {

    /**
     * Creates an instance of  QuoteDetailService.
     * 
     * @param {*} [opts={}]
     * @memberof  QuoteDetailService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}