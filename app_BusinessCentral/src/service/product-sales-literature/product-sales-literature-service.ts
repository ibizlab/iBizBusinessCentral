import { Http,Util } from '@/utils';
import ProductSalesLiteratureServiceBase from './product-sales-literature-service-base';


/**
 * 产品宣传资料服务对象
 *
 * @export
 * @class ProductSalesLiteratureService
 * @extends {ProductSalesLiteratureServiceBase}
 */
export default class ProductSalesLiteratureService extends ProductSalesLiteratureServiceBase {

    /**
     * Creates an instance of  ProductSalesLiteratureService.
     * 
     * @param {*} [opts={}]
     * @memberof  ProductSalesLiteratureService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}