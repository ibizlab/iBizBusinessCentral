import { Http,Util } from '@/utils';
import PublishLogicBase from './publish-logic-base';

/**
 * 发布
 *
 * @export
 * @class PublishLogic
 */
export default class PublishLogic extends PublishLogicBase{

    /**
     * Creates an instance of  PublishLogic
     * 
     * @param {*} [opts={}]
     * @memberof  PublishLogic
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}