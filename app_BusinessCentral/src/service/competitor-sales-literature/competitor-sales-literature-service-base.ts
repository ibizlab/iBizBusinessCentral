import { Http,Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 竞争对手宣传资料服务对象基类
 *
 * @export
 * @class CompetitorSalesLiteratureServiceBase
 * @extends {EntityServie}
 */
export default class CompetitorSalesLiteratureServiceBase extends EntityService {

    /**
     * Creates an instance of  CompetitorSalesLiteratureServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  CompetitorSalesLiteratureServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof CompetitorSalesLiteratureServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='competitorsalesliterature';
        this.APPDEKEY = 'relationshipsid';
        this.APPDENAME = 'competitorsalesliteratures';
        this.APPDETEXT = 'relationshipsname';
        this.APPNAME = 'businesscentral';
        this.SYSTEMNAME = 'ibizbusinesscentral';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof CompetitorSalesLiteratureServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.salesliterature && context.competitorsalesliterature){
            let res:any = Http.getInstance().get(`/salesliteratures/${context.salesliterature}/competitorsalesliteratures/${context.competitorsalesliterature}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/competitorsalesliteratures/${context.competitorsalesliterature}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof CompetitorSalesLiteratureServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.salesliterature && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/salesliteratures/${context.salesliterature}/competitorsalesliteratures`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/competitorsalesliteratures`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof CompetitorSalesLiteratureServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.salesliterature && context.competitorsalesliterature){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/salesliteratures/${context.salesliterature}/competitorsalesliteratures/${context.competitorsalesliterature}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/competitorsalesliteratures/${context.competitorsalesliterature}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof CompetitorSalesLiteratureServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.salesliterature && context.competitorsalesliterature){
            let res:any = Http.getInstance().delete(`/salesliteratures/${context.salesliterature}/competitorsalesliteratures/${context.competitorsalesliterature}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/competitorsalesliteratures/${context.competitorsalesliterature}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof CompetitorSalesLiteratureServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.salesliterature && context.competitorsalesliterature){
            let res:any = await Http.getInstance().get(`/salesliteratures/${context.salesliterature}/competitorsalesliteratures/${context.competitorsalesliterature}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/competitorsalesliteratures/${context.competitorsalesliterature}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof CompetitorSalesLiteratureServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.salesliterature && true){
            let res:any = await Http.getInstance().get(`/salesliteratures/${context.salesliterature}/competitorsalesliteratures/getdraft`,isloading);
            res.data.competitorsalesliterature = data.competitorsalesliterature;
            
            return res;
        }
        let res:any = await  Http.getInstance().get(`/competitorsalesliteratures/getdraft`,isloading);
        res.data.competitorsalesliterature = data.competitorsalesliterature;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof CompetitorSalesLiteratureServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.salesliterature && context.competitorsalesliterature){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/salesliteratures/${context.salesliterature}/competitorsalesliteratures/${context.competitorsalesliterature}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/competitorsalesliteratures/${context.competitorsalesliterature}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof CompetitorSalesLiteratureServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.salesliterature && context.competitorsalesliterature){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/salesliteratures/${context.salesliterature}/competitorsalesliteratures/${context.competitorsalesliterature}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/competitorsalesliteratures/${context.competitorsalesliterature}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof CompetitorSalesLiteratureServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.salesliterature && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/salesliteratures/${context.salesliterature}/competitorsalesliteratures/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/competitorsalesliteratures/fetchdefault`,tempData,isloading);
        return res;
    }
}