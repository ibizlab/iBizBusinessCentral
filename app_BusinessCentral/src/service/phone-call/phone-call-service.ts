import { Http,Util } from '@/utils';
import PhoneCallServiceBase from './phone-call-service-base';


/**
 * 电话联络服务对象
 *
 * @export
 * @class PhoneCallService
 * @extends {PhoneCallServiceBase}
 */
export default class PhoneCallService extends PhoneCallServiceBase {

    /**
     * Creates an instance of  PhoneCallService.
     * 
     * @param {*} [opts={}]
     * @memberof  PhoneCallService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}