import { Http,Util } from '@/utils';
import ProductSubstituteServiceBase from './product-substitute-service-base';


/**
 * 产品替换服务对象
 *
 * @export
 * @class ProductSubstituteService
 * @extends {ProductSubstituteServiceBase}
 */
export default class ProductSubstituteService extends ProductSubstituteServiceBase {

    /**
     * Creates an instance of  ProductSubstituteService.
     * 
     * @param {*} [opts={}]
     * @memberof  ProductSubstituteService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}