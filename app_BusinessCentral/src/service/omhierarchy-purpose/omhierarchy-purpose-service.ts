import { Http,Util } from '@/utils';
import OMHierarchyPurposeServiceBase from './omhierarchy-purpose-service-base';


/**
 * 组织层次结构应用服务对象
 *
 * @export
 * @class OMHierarchyPurposeService
 * @extends {OMHierarchyPurposeServiceBase}
 */
export default class OMHierarchyPurposeService extends OMHierarchyPurposeServiceBase {

    /**
     * Creates an instance of  OMHierarchyPurposeService.
     * 
     * @param {*} [opts={}]
     * @memberof  OMHierarchyPurposeService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}