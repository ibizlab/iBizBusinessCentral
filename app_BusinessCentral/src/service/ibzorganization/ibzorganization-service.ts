import { Http,Util } from '@/utils';
import IBZOrganizationServiceBase from './ibzorganization-service-base';


/**
 * 单位机构服务对象
 *
 * @export
 * @class IBZOrganizationService
 * @extends {IBZOrganizationServiceBase}
 */
export default class IBZOrganizationService extends IBZOrganizationServiceBase {

    /**
     * Creates an instance of  IBZOrganizationService.
     * 
     * @param {*} [opts={}]
     * @memberof  IBZOrganizationService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}