import { Http,Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 营销列表-联系人服务对象基类
 *
 * @export
 * @class ListContactServiceBase
 * @extends {EntityServie}
 */
export default class ListContactServiceBase extends EntityService {

    /**
     * Creates an instance of  ListContactServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  ListContactServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof ListContactServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='listcontact';
        this.APPDEKEY = 'relationshipsid';
        this.APPDENAME = 'listcontacts';
        this.APPDETEXT = 'relationshipsname';
        this.APPNAME = 'businesscentral';
        this.SYSTEMNAME = 'ibizbusinesscentral';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ListContactServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.listcontact){
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/listcontacts/${context.listcontact}/select`,isloading);
            
            return res;
        }
        if(context.ibizlist && context.listcontact){
            let res:any = Http.getInstance().get(`/ibizlists/${context.ibizlist}/listcontacts/${context.listcontact}/select`,isloading);
            
            return res;
        }
        if(context.contact && context.listcontact){
            let res:any = Http.getInstance().get(`/contacts/${context.contact}/listcontacts/${context.listcontact}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/listcontacts/${context.listcontact}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ListContactServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/listcontacts`,data,isloading);
            
            return res;
        }
        if(context.ibizlist && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/ibizlists/${context.ibizlist}/listcontacts`,data,isloading);
            
            return res;
        }
        if(context.contact && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/contacts/${context.contact}/listcontacts`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/listcontacts`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ListContactServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.listcontact){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/accounts/${context.account}/contacts/${context.contact}/listcontacts/${context.listcontact}`,data,isloading);
            
            return res;
        }
        if(context.ibizlist && context.listcontact){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/ibizlists/${context.ibizlist}/listcontacts/${context.listcontact}`,data,isloading);
            
            return res;
        }
        if(context.contact && context.listcontact){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/contacts/${context.contact}/listcontacts/${context.listcontact}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/listcontacts/${context.listcontact}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ListContactServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.listcontact){
            let res:any = Http.getInstance().delete(`/accounts/${context.account}/contacts/${context.contact}/listcontacts/${context.listcontact}`,isloading);
            return res;
        }
        if(context.ibizlist && context.listcontact){
            let res:any = Http.getInstance().delete(`/ibizlists/${context.ibizlist}/listcontacts/${context.listcontact}`,isloading);
            return res;
        }
        if(context.contact && context.listcontact){
            let res:any = Http.getInstance().delete(`/contacts/${context.contact}/listcontacts/${context.listcontact}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/listcontacts/${context.listcontact}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ListContactServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.listcontact){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/listcontacts/${context.listcontact}`,isloading);
            
            return res;
        }
        if(context.ibizlist && context.listcontact){
            let res:any = await Http.getInstance().get(`/ibizlists/${context.ibizlist}/listcontacts/${context.listcontact}`,isloading);
            
            return res;
        }
        if(context.contact && context.listcontact){
            let res:any = await Http.getInstance().get(`/contacts/${context.contact}/listcontacts/${context.listcontact}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/listcontacts/${context.listcontact}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ListContactServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && true){
            let res:any = await Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/listcontacts/getdraft`,isloading);
            res.data.listcontact = data.listcontact;
            
            return res;
        }
        if(context.ibizlist && true){
            let res:any = await Http.getInstance().get(`/ibizlists/${context.ibizlist}/listcontacts/getdraft`,isloading);
            res.data.listcontact = data.listcontact;
            
            return res;
        }
        if(context.contact && true){
            let res:any = await Http.getInstance().get(`/contacts/${context.contact}/listcontacts/getdraft`,isloading);
            res.data.listcontact = data.listcontact;
            
            return res;
        }
        let res:any = await  Http.getInstance().get(`/listcontacts/getdraft`,isloading);
        res.data.listcontact = data.listcontact;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ListContactServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.listcontact){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/listcontacts/${context.listcontact}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.ibizlist && context.listcontact){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/ibizlists/${context.ibizlist}/listcontacts/${context.listcontact}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.contact && context.listcontact){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/contacts/${context.contact}/listcontacts/${context.listcontact}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/listcontacts/${context.listcontact}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ListContactServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && context.listcontact){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/accounts/${context.account}/contacts/${context.contact}/listcontacts/${context.listcontact}/save`,data,isloading);
            
            return res;
        }
        if(context.ibizlist && context.listcontact){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/ibizlists/${context.ibizlist}/listcontacts/${context.listcontact}/save`,data,isloading);
            
            return res;
        }
        if(context.contact && context.listcontact){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/contacts/${context.contact}/listcontacts/${context.listcontact}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/listcontacts/${context.listcontact}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ListContactServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.account && context.contact && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/accounts/${context.account}/contacts/${context.contact}/listcontacts/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.ibizlist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/ibizlists/${context.ibizlist}/listcontacts/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.contact && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/contacts/${context.contact}/listcontacts/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/listcontacts/fetchdefault`,tempData,isloading);
        return res;
    }
}