import { Http,Util } from '@/utils';
import ListContactServiceBase from './list-contact-service-base';


/**
 * 营销列表-联系人服务对象
 *
 * @export
 * @class ListContactService
 * @extends {ListContactServiceBase}
 */
export default class ListContactService extends ListContactServiceBase {

    /**
     * Creates an instance of  ListContactService.
     * 
     * @param {*} [opts={}]
     * @memberof  ListContactService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}