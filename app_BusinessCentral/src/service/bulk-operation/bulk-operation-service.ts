import { Http,Util } from '@/utils';
import BulkOperationServiceBase from './bulk-operation-service-base';


/**
 * 快速市场活动服务对象
 *
 * @export
 * @class BulkOperationService
 * @extends {BulkOperationServiceBase}
 */
export default class BulkOperationService extends BulkOperationServiceBase {

    /**
     * Creates an instance of  BulkOperationService.
     * 
     * @param {*} [opts={}]
     * @memberof  BulkOperationService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}