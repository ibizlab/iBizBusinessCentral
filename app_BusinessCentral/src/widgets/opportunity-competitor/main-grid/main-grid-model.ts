/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'entity2id',
          prop: 'entity2id',
          dataType: 'PICKUP',
        },
        {
          name: 'estimatedvalue',
          prop: 'estimatedvalue',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'entityid',
          prop: 'entityid',
          dataType: 'PICKUP',
        },
        {
          name: 'actualvalue',
          prop: 'actualvalue',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'estimatedclosedate',
          prop: 'estimatedclosedate',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'entityname',
          prop: 'entityname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'originatingleadname',
          prop: 'originatingleadname',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'srfmajortext',
          prop: 'relationshipsname',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'relationshipsid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'relationshipsid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'entity2name',
          prop: 'entity2name',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'opportunitycompetitor',
          prop: 'relationshipsid',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}