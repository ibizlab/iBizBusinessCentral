import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, SearchFormControlBase } from '@/studio-core';
import JobsInfoService from '@/service/jobs-info/jobs-info-service';
import DefaultService from './default-searchform-service';
import JobsInfoUIService from '@/uiservice/jobs-info/jobs-info-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';


/**
 * searchform部件基类
 *
 * @export
 * @class SearchFormControlBase
 * @extends {DefaultSearchFormBase}
 */
export class DefaultSearchFormBase extends SearchFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected controlType: string = 'SEARCHFORM';

    /**
     * 建构部件服务对象
     *
     * @type {DefaultService}
     * @memberof DefaultSearchFormBase
     */
    public service: DefaultService = new DefaultService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {JobsInfoService}
     * @memberof DefaultSearchFormBase
     */
    public appEntityService: JobsInfoService = new JobsInfoService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected appDeName: string = 'jobsinfo';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected appDeLogicName: string = '任务信息';

    /**
     * 界面UI服务对象
     *
     * @type {JobsInfoUIService}
     * @memberof DefaultBase
     */  
    public appUIService:JobsInfoUIService = new JobsInfoUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof DefaultSearchFormBase
     */
    public data: any = {
        n_app_like: null,
        n_handler_like: null,
        n_status_eq: null,
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof DefaultSearchFormBase
     */
    public detailsModel: any = {
        formpage1: new FormPageModel({ caption: '常规条件', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this })
, 
        n_app_like: new FormItemModel({ caption: '服务名(文本包含(%))', detailType: 'FORMITEM', name: 'n_app_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_handler_like: new FormItemModel({ caption: '执行器任务HANDLER(文本包含(%))', detailType: 'FORMITEM', name: 'n_handler_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_status_eq: new FormItemModel({ caption: '状态(等于(=))', detailType: 'FORMITEM', name: 'n_status_eq', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
    };
}