import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * SOInfo 部件服务对象
 *
 * @export
 * @class SOInfoService
 */
export default class SOInfoService extends ControlService {
}