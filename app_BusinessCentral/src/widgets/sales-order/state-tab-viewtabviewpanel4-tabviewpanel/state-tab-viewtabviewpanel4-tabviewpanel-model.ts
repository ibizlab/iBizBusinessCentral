/**
 * StateTabViewtabviewpanel4 部件模型
 *
 * @export
 * @class StateTabViewtabviewpanel4Model
 */
export default class StateTabViewtabviewpanel4Model {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof StateTabViewtabviewpanel4Model
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'entityimage',
      },
      {
        name: 'pricingerrorcode',
      },
      {
        name: 'shipto_country',
      },
      {
        name: 'shipto_fax',
      },
      {
        name: 'totallineitemamount_base',
      },
      {
        name: 'billto_country',
      },
      {
        name: 'totalamountlessfreight',
      },
      {
        name: 'willcall',
      },
      {
        name: 'accountname',
      },
      {
        name: 'discountamount_base',
      },
      {
        name: 'datefulfilled',
      },
      {
        name: 'customertype',
      },
      {
        name: 'billto_line3',
      },
      {
        name: 'billto_line1',
      },
      {
        name: 'utcconversiontimezonecode',
      },
      {
        name: 'ownerid',
      },
      {
        name: 'exchangerate',
      },
      {
        name: 'entityimageid',
      },
      {
        name: 'shipto_line3',
      },
      {
        name: 'createman',
      },
      {
        name: 'shipto_telephone',
      },
      {
        name: 'stageid',
      },
      {
        name: 'customername',
      },
      {
        name: 'traversedpath',
      },
      {
        name: 'updateman',
      },
      {
        name: 'ordernumber',
      },
      {
        name: 'timezoneruleversionnumber',
      },
      {
        name: 'totaldiscountamount_base',
      },
      {
        name: 'shipto_contactname',
      },
      {
        name: 'requestdeliveryby',
      },
      {
        name: 'ownername',
      },
      {
        name: 'statecode',
      },
      {
        name: 'totallineitemdiscountamount',
      },
      {
        name: 'shipto_freighttermscode',
      },
      {
        name: 'totaltax',
      },
      {
        name: 'shipto_line1',
      },
      {
        name: 'discountpercentage',
      },
      {
        name: 'prioritycode',
      },
      {
        name: 'billto_composite',
      },
      {
        name: 'statuscode',
      },
      {
        name: 'shipto_addressid',
      },
      {
        name: 'paymenttermscode',
      },
      {
        name: 'billto_postalcode',
      },
      {
        name: 'billto_addressid',
      },
      {
        name: 'billto_telephone',
      },
      {
        name: 'shipto_composite',
      },
      {
        name: 'submitstatus',
      },
      {
        name: 'submitstatusdescription',
      },
      {
        name: 'emailaddress',
      },
      {
        name: 'billto_city',
      },
      {
        name: 'freightamount_base',
      },
      {
        name: 'createdate',
      },
      {
        name: 'salesorder',
        prop: 'salesorderid',
      },
      {
        name: 'contactname',
      },
      {
        name: 'freightamount',
      },
      {
        name: 'shipto_stateorprovince',
      },
      {
        name: 'discountamount',
      },
      {
        name: 'totalamount',
      },
      {
        name: 'totaldiscountamount',
      },
      {
        name: 'totalamount_base',
      },
      {
        name: 'importsequencenumber',
      },
      {
        name: 'totalamountlessfreight_base',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'versionnumber',
      },
      {
        name: 'shipto_city',
      },
      {
        name: 'billto_line2',
      },
      {
        name: 'shipto_line2',
      },
      {
        name: 'processid',
      },
      {
        name: 'billto_name',
      },
      {
        name: 'totallineitemamount',
      },
      {
        name: 'lastonholdtime',
      },
      {
        name: 'lastbackofficesubmit',
      },
      {
        name: 'submitdate',
      },
      {
        name: 'billto_fax',
      },
      {
        name: 'shipto_postalcode',
      },
      {
        name: 'entityimage_url',
      },
      {
        name: 'overriddencreatedon',
      },
      {
        name: 'customerid',
      },
      {
        name: 'billto_contactname',
      },
      {
        name: 'onholdtime',
      },
      {
        name: 'totaltax_base',
      },
      {
        name: 'ownertype',
      },
      {
        name: 'entityimage_timestamp',
      },
      {
        name: 'freighttermscode',
      },
      {
        name: 'billto_stateorprovince',
      },
      {
        name: 'shipto_name',
      },
      {
        name: 'salesordername',
      },
      {
        name: 'shippingmethodcode',
      },
      {
        name: 'pricelocked',
      },
      {
        name: 'description',
      },
      {
        name: 'campaignname',
      },
      {
        name: 'opportunityname',
      },
      {
        name: 'slaname',
      },
      {
        name: 'quotename',
      },
      {
        name: 'pricelevelname',
      },
      {
        name: 'currencyname',
      },
      {
        name: 'quoteid',
      },
      {
        name: 'slaid',
      },
      {
        name: 'transactioncurrencyid',
      },
      {
        name: 'opportunityid',
      },
      {
        name: 'pricelevelid',
      },
      {
        name: 'campaignid',
      },
    ]
  }


}