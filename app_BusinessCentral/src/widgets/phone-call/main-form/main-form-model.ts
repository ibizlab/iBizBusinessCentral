/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof MainModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'activityid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'subject',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'ownername',
        prop: 'ownername',
        dataType: 'TEXT',
      },
      {
        name: 'subject',
        prop: 'subject',
        dataType: 'TEXT',
      },
      {
        name: 'from',
        prop: 'from',
        dataType: 'LONGTEXT',
      },
      {
        name: 'to',
        prop: 'to',
        dataType: 'LONGTEXT',
      },
      {
        name: 'directioncode',
        prop: 'directioncode',
        dataType: 'YESNO',
      },
      {
        name: 'phonenumber',
        prop: 'phonenumber',
        dataType: 'TEXT',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'TEXT',
      },
      {
        name: 'actualdurationminutes',
        prop: 'actualdurationminutes',
        dataType: 'INT',
      },
      {
        name: 'scheduledend',
        prop: 'scheduledend',
        dataType: 'DATETIME',
      },
      {
        name: 'prioritycode',
        prop: 'prioritycode',
        dataType: 'SSCODELIST',
      },
      {
        name: 'activityid',
        prop: 'activityid',
        dataType: 'GUID',
      },
      {
        name: 'phonecall',
        prop: 'activityid',
        dataType: 'FONTKEY',
      },
    ]
  }

}