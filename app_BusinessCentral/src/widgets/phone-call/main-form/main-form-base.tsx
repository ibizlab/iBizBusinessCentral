import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, EditFormControlBase } from '@/studio-core';
import PhoneCallService from '@/service/phone-call/phone-call-service';
import MainService from './main-form-service';
import PhoneCallUIService from '@/uiservice/phone-call/phone-call-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';


/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {MainEditFormBase}
 */
export class MainEditFormBase extends EditFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof MainEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {MainService}
     * @memberof MainEditFormBase
     */
    public service: MainService = new MainService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {PhoneCallService}
     * @memberof MainEditFormBase
     */
    public appEntityService: PhoneCallService = new PhoneCallService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MainEditFormBase
     */
    protected appDeName: string = 'phonecall';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof MainEditFormBase
     */
    protected appDeLogicName: string = '电话联络';

    /**
     * 界面UI服务对象
     *
     * @type {PhoneCallUIService}
     * @memberof MainBase
     */  
    public appUIService:PhoneCallUIService = new PhoneCallUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof MainEditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        ownername: null,
        subject: null,
        from: null,
        to: null,
        directioncode: null,
        phonenumber: null,
        description: null,
        actualdurationminutes: null,
        scheduledend: null,
        prioritycode: null,
        activityid: null,
        phonecall:null,
    };

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof MainEditFormBase
     */
    public rules: any = {
        ownername: [
            { required: true, type: 'string', message: '负责人 值不能为空', trigger: 'change' },
            { required: true, type: 'string', message: '负责人 值不能为空', trigger: 'blur' },
        ],
        subject: [
            { required: true, type: 'string', message: '主题 值不能为空', trigger: 'change' },
            { required: true, type: 'string', message: '主题 值不能为空', trigger: 'blur' },
        ],
        from: [
            { required: true, type: 'string', message: '呼叫方 值不能为空', trigger: 'change' },
            { required: true, type: 'string', message: '呼叫方 值不能为空', trigger: 'blur' },
        ],
        to: [
            { required: true, type: 'string', message: '被呼叫方 值不能为空', trigger: 'change' },
            { required: true, type: 'string', message: '被呼叫方 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof MainBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof MainEditFormBase
     */
    public detailsModel: any = {
        group1: new FormGroupPanelModel({ caption: 'phonecall基本信息', detailType: 'GROUPPANEL', name: 'group1', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.phonecall.main_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({ caption: '更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 0 }),

        srforikey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfkey: new FormItemModel({ caption: '电话联络', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfmajortext: new FormItemModel({ caption: '主题', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srftempmode: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfuf: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfdeid: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfsourcekey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        ownername: new FormItemModel({ caption: '负责人', detailType: 'FORMITEM', name: 'ownername', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        subject: new FormItemModel({ caption: '主题', detailType: 'FORMITEM', name: 'subject', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        from: new FormItemModel({ caption: '呼叫方', detailType: 'FORMITEM', name: 'from', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        to: new FormItemModel({ caption: '被呼叫方', detailType: 'FORMITEM', name: 'to', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        directioncode: new FormItemModel({ caption: '方向', detailType: 'FORMITEM', name: 'directioncode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        phonenumber: new FormItemModel({ caption: '电话号码', detailType: 'FORMITEM', name: 'phonenumber', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        description: new FormItemModel({ caption: '说明', detailType: 'FORMITEM', name: 'description', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        actualdurationminutes: new FormItemModel({ caption: '持续时间', detailType: 'FORMITEM', name: 'actualdurationminutes', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        scheduledend: new FormItemModel({ caption: '截止日期', detailType: 'FORMITEM', name: 'scheduledend', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        prioritycode: new FormItemModel({ caption: '优先级', detailType: 'FORMITEM', name: 'prioritycode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        activityid: new FormItemModel({ caption: '电话联络', detailType: 'FORMITEM', name: 'activityid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

    };
}