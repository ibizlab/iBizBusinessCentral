/**
 * SOProductEdit 部件模型
 *
 * @export
 * @class SOProductEditModel
 */
export default class SOProductEditModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof SOProductEditModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'salesorderdetailid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'salesorderdetailname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'productname',
        prop: 'productname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'priceperunit',
        prop: 'priceperunit',
        dataType: 'BIGDECIMAL',
      },
      {
        name: 'quantity',
        prop: 'quantity',
        dataType: 'BIGDECIMAL',
      },
      {
        name: 'uomname',
        prop: 'uomname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'manualdiscountamount',
        prop: 'manualdiscountamount',
        dataType: 'BIGDECIMAL',
      },
      {
        name: 'tax',
        prop: 'tax',
        dataType: 'BIGDECIMAL',
      },
      {
        name: 'productid',
        prop: 'productid',
        dataType: 'PICKUP',
      },
      {
        name: 'uomid',
        prop: 'uomid',
        dataType: 'PICKUP',
      },
      {
        name: 'salesorderdetailid',
        prop: 'salesorderdetailid',
        dataType: 'GUID',
      },
      {
        name: 'salesorderdetail',
        prop: 'salesorderdetailid',
        dataType: 'FONTKEY',
      },
    ]
  }

}