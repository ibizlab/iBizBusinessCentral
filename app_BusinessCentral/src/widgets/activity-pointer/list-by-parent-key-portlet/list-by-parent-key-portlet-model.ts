/**
 * List_ByParentKey 部件模型
 *
 * @export
 * @class List_ByParentKeyModel
 */
export default class List_ByParentKeyModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof List_ByParentKeyModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'seriesid',
      },
      {
        name: 'traversedpath',
      },
      {
        name: 'deliveryprioritycode',
      },
      {
        name: 'ownername',
      },
      {
        name: 'onholdtime',
      },
      {
        name: 'workflowcreated',
      },
      {
        name: 'senton',
      },
      {
        name: 'lastonholdtime',
      },
      {
        name: 'actualdurationminutes',
      },
      {
        name: 'regardingobjectid',
      },
      {
        name: 'deliverylastattemptedon',
      },
      {
        name: 'mapiprivate',
      },
      {
        name: 'actualend',
      },
      {
        name: 'description',
      },
      {
        name: 'prioritycode',
      },
      {
        name: 'billed',
      },
      {
        name: 'regularactivity',
      },
      {
        name: 'utcconversiontimezonecode',
      },
      {
        name: 'createman',
      },
      {
        name: 'scheduleddurationminutes',
      },
      {
        name: 'activityadditionalparams',
      },
      {
        name: 'activitypointer',
        prop: 'activityid',
      },
      {
        name: 'regardingobjectname',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'stageid',
      },
      {
        name: 'leftvoicemail',
      },
      {
        name: 'scheduledend',
      },
      {
        name: 'exchangeitemid',
      },
      {
        name: 'exchangerate',
      },
      {
        name: 'scheduledstart',
      },
      {
        name: 'instancetypecode',
      },
      {
        name: 'regardingobjecttypecode',
      },
      {
        name: 'sortdate',
      },
      {
        name: 'createdate',
      },
      {
        name: 'community',
      },
      {
        name: 'ownerid',
      },
      {
        name: 'processid',
      },
      {
        name: 'slaname',
      },
      {
        name: 'subject',
      },
      {
        name: 'versionnumber',
      },
      {
        name: 'allparties',
      },
      {
        name: 'updateman',
      },
      {
        name: 'exchangeweblink',
      },
      {
        name: 'statuscode',
      },
      {
        name: 'timezoneruleversionnumber',
      },
      {
        name: 'actualstart',
      },
      {
        name: 'activitytypecode',
      },
      {
        name: 'regardingobjectidname',
      },
      {
        name: 'ownertype',
      },
      {
        name: 'statecode',
      },
      {
        name: 'serviceid',
      },
      {
        name: 'transactioncurrencyid',
      },
      {
        name: 'slaid',
      },
    ]
  }


}
