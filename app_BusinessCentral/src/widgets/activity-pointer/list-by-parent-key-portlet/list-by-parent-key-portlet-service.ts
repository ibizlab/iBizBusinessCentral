import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * List_ByParentKey 部件服务对象
 *
 * @export
 * @class List_ByParentKeyService
 */
export default class List_ByParentKeyService extends ControlService {
}
