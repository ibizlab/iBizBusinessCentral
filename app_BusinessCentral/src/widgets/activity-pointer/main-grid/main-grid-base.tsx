import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, GridControlBase } from '@/studio-core';
import ActivityPointerService from '@/service/activity-pointer/activity-pointer-service';
import MainService from './main-grid-service';
import ActivityPointerUIService from '@/uiservice/activity-pointer/activity-pointer-ui-service';
import { FormItemModel } from '@/model/form-detail';


/**
 * grid部件基类
 *
 * @export
 * @class GridControlBase
 * @extends {MainGridBase}
 */
export class MainGridBase extends GridControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected controlType: string = 'GRID';

    /**
     * 建构部件服务对象
     *
     * @type {MainService}
     * @memberof MainGridBase
     */
    public service: MainService = new MainService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {ActivityPointerService}
     * @memberof MainGridBase
     */
    public appEntityService: ActivityPointerService = new ActivityPointerService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected appDeName: string = 'activitypointer';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected appDeLogicName: string = '活动';

    /**
     * 界面UI服务对象
     *
     * @type {ActivityPointerUIService}
     * @memberof MainBase
     */  
    public appUIService:ActivityPointerUIService = new ActivityPointerUIService(this.$store);

    /**
     * 界面行为模型
     *
     * @type {*}
     * @memberof MainBase
     */  
    public ActionModel: any = {
    };

    /**
     * 本地缓存标识
     *
     * @protected
     * @type {string}
     * @memberof MainBase
     */
    protected localStorageTag: string = 'activitypointer_main_grid';

    /**
     * 所有列成员
     *
     * @type {any[]}
     * @memberof MainGridBase
     */
    public allColumns: any[] = [
        {
            name: 'subject',
            label: '主题',
            langtag: 'entities.activitypointer.main_grid.columns.subject',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'regardingobjectidname',
            label: '关于',
            langtag: 'entities.activitypointer.main_grid.columns.regardingobjectidname',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'activitytypecode',
            label: '活动类型',
            langtag: 'entities.activitypointer.main_grid.columns.activitytypecode',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'statecode',
            label: '活动状态',
            langtag: 'entities.activitypointer.main_grid.columns.statecode',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'prioritycode',
            label: '优先级',
            langtag: 'entities.activitypointer.main_grid.columns.prioritycode',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'scheduledstart',
            label: '开始日期',
            langtag: 'entities.activitypointer.main_grid.columns.scheduledstart',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'scheduledend',
            label: '截止日期',
            langtag: 'entities.activitypointer.main_grid.columns.scheduledend',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
    ]

    /**
     * 获取表格行模型
     *
     * @type {*}
     * @memberof MainGridBase
     */
    public getGridRowModel(){
        return {
          srfkey: new FormItemModel(),
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof MainGridBase
     */
    public rules: any = {
        srfkey: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '活动 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '活动 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 获取对应列class
     *
     * @type {*}
     * @memberof MainBase
     */
    public hasRowEdit: any = {
        'subject':false,
        'regardingobjectidname':false,
        'activitytypecode':false,
        'statecode':false,
        'prioritycode':false,
        'scheduledstart':false,
        'scheduledend':false,
    };

    /**
     * 获取对应列class
     *
     * @param {*} $args row 行数据，column 列数据，rowIndex 行索引，列索引
     * @returns {void}
     * @memberof MainBase
     */
    public getCellClassName(args: {row: any, column: any, rowIndex: number, columnIndex: number}): any {
        return ( this.hasRowEdit[args.column.property] && this.actualIsOpenEdit ) ? "edit-cell" : "info-cell";
    }


    /**
     * 导出数据格式化
     *
     * @param {*} filterVal
     * @param {*} jsonData
     * @param {any[]} [codelistColumns=[]]
     * @returns {Promise<any>}
     * @memberof MainGridBase
     */
    public async formatExcelData(filterVal: any, jsonData: any, codelistColumns?: any[]): Promise<any> {
        return super.formatExcelData(filterVal, jsonData, [
            {
                name: 'activitytypecode',
                srfkey: 'ActivityTypeCode',
                codelistType : 'STATIC',
                renderMode: 'other',
                textSeparator: '、',
                valueSeparator: ',',
            },
            {
                name: 'statecode',
                srfkey: 'Activitypointer__StateCode',
                codelistType : 'STATIC',
                renderMode: 'other',
                textSeparator: '、',
                valueSeparator: ',',
            },
            {
                name: 'prioritycode',
                srfkey: 'Activitypointer__PriorityCode',
                codelistType : 'STATIC',
                renderMode: 'other',
                textSeparator: '、',
                valueSeparator: ',',
            },
        ]);
    }

}