/**
 * Default 部件模型
 *
 * @export
 * @class DefaultModel
 */
export default class DefaultModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof DefaultModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'n_subject_like',
        prop: 'subject',
        dataType: 'TEXT',
      },
      {
        name: 'n_activitytypecode_eq',
        prop: 'activitytypecode',
        dataType: 'SSCODELIST',
      },
      {
        name: 'n_prioritycode_eq',
        prop: 'prioritycode',
        dataType: 'SSCODELIST',
      },
      {
        name: 'n_statuscode_eq',
        prop: 'statuscode',
        dataType: 'NSCODELIST',
      },
      {
        name: 'n_statecode_eq',
        prop: 'statecode',
        dataType: 'NSCODELIST',
      },
    ]
  }

}