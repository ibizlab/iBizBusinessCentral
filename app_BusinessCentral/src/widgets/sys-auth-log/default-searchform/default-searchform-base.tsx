import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, SearchFormControlBase } from '@/studio-core';
import SysAuthLogService from '@/service/sys-auth-log/sys-auth-log-service';
import DefaultService from './default-searchform-service';
import SysAuthLogUIService from '@/uiservice/sys-auth-log/sys-auth-log-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';


/**
 * searchform部件基类
 *
 * @export
 * @class SearchFormControlBase
 * @extends {DefaultSearchFormBase}
 */
export class DefaultSearchFormBase extends SearchFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected controlType: string = 'SEARCHFORM';

    /**
     * 建构部件服务对象
     *
     * @type {DefaultService}
     * @memberof DefaultSearchFormBase
     */
    public service: DefaultService = new DefaultService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {SysAuthLogService}
     * @memberof DefaultSearchFormBase
     */
    public appEntityService: SysAuthLogService = new SysAuthLogService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected appDeName: string = 'sysauthlog';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected appDeLogicName: string = '认证日志';

    /**
     * 界面UI服务对象
     *
     * @type {SysAuthLogUIService}
     * @memberof DefaultBase
     */  
    public appUIService:SysAuthLogUIService = new SysAuthLogUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof DefaultSearchFormBase
     */
    public data: any = {
        n_username_like: null,
        n_personname_like: null,
        n_authcode_eq: null,
        n_authtime_gtandeq: null,
        n_authtime_ltandeq: null,
        n_domain_like: null,
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof DefaultSearchFormBase
     */
    public detailsModel: any = {
        formpage1: new FormPageModel({ caption: '常规条件', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this })
, 
        n_username_like: new FormItemModel({ caption: '用户全局名(文本包含(%))', detailType: 'FORMITEM', name: 'n_username_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_personname_like: new FormItemModel({ caption: '用户名称(文本包含(%))', detailType: 'FORMITEM', name: 'n_personname_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_authcode_eq: new FormItemModel({ caption: '认证结果(等于(=))', detailType: 'FORMITEM', name: 'n_authcode_eq', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_authtime_gtandeq: new FormItemModel({ caption: '认证时间(大于等于(>=))', detailType: 'FORMITEM', name: 'n_authtime_gtandeq', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_authtime_ltandeq: new FormItemModel({ caption: '认证时间(小于等于(<=))', detailType: 'FORMITEM', name: 'n_authtime_ltandeq', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_domain_like: new FormItemModel({ caption: '域(文本包含(%))', detailType: 'FORMITEM', name: 'n_domain_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
    };
}