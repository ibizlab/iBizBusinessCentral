import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, EditFormControlBase } from '@/studio-core';
import ProductSalesLiteratureService from '@/service/product-sales-literature/product-sales-literature-service';
import SalLitProService from './sal-lit-pro-form-service';
import ProductSalesLiteratureUIService from '@/uiservice/product-sales-literature/product-sales-literature-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';


/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {SalLitProEditFormBase}
 */
export class SalLitProEditFormBase extends EditFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof SalLitProEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {SalLitProService}
     * @memberof SalLitProEditFormBase
     */
    public service: SalLitProService = new SalLitProService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {ProductSalesLiteratureService}
     * @memberof SalLitProEditFormBase
     */
    public appEntityService: ProductSalesLiteratureService = new ProductSalesLiteratureService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof SalLitProEditFormBase
     */
    protected appDeName: string = 'productsalesliterature';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof SalLitProEditFormBase
     */
    protected appDeLogicName: string = '实体';

    /**
     * 界面UI服务对象
     *
     * @type {ProductSalesLiteratureUIService}
     * @memberof SalLitProBase
     */  
    public appUIService:ProductSalesLiteratureUIService = new ProductSalesLiteratureUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof SalLitProEditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        entity2id: null,
        entityname: null,
        relationshipsid: null,
        entityid: null,
        productsalesliterature:null,
    };

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof SalLitProEditFormBase
     */
    public rules: any = {
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof SalLitProBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof SalLitProEditFormBase
     */
    public detailsModel: any = {
        group1: new FormGroupPanelModel({ caption: '产品宣传资料基本信息', detailType: 'GROUPPANEL', name: 'group1', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.productsalesliterature.sallitpro_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({ caption: '更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 0 }),

        srforikey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfkey: new FormItemModel({ caption: '关系标识', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfmajortext: new FormItemModel({ caption: '关系名称', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srftempmode: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfuf: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfdeid: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfsourcekey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        entity2id: new FormItemModel({ caption: '销售宣传资料', detailType: 'FORMITEM', name: 'entity2id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        entityname: new FormItemModel({ caption: '产品', detailType: 'FORMITEM', name: 'entityname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        relationshipsid: new FormItemModel({ caption: '关系标识', detailType: 'FORMITEM', name: 'relationshipsid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        entityid: new FormItemModel({ caption: '产品', detailType: 'FORMITEM', name: 'entityid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

    };
}