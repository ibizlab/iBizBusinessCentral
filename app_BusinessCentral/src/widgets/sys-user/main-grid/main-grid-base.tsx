import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, GridControlBase } from '@/studio-core';
import SysUserService from '@/service/sys-user/sys-user-service';
import MainService from './main-grid-service';
import SysUserUIService from '@/uiservice/sys-user/sys-user-ui-service';
import { FormItemModel } from '@/model/form-detail';


/**
 * grid部件基类
 *
 * @export
 * @class GridControlBase
 * @extends {MainGridBase}
 */
export class MainGridBase extends GridControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected controlType: string = 'GRID';

    /**
     * 建构部件服务对象
     *
     * @type {MainService}
     * @memberof MainGridBase
     */
    public service: MainService = new MainService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {SysUserService}
     * @memberof MainGridBase
     */
    public appEntityService: SysUserService = new SysUserService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected appDeName: string = 'sysuser';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected appDeLogicName: string = '系统用户';

    /**
     * 界面UI服务对象
     *
     * @type {SysUserUIService}
     * @memberof MainBase
     */  
    public appUIService:SysUserUIService = new SysUserUIService(this.$store);

    /**
     * 界面行为模型
     *
     * @type {*}
     * @memberof MainBase
     */  
    public ActionModel: any = {
    };

    /**
     * 本地缓存标识
     *
     * @protected
     * @type {string}
     * @memberof MainBase
     */
    protected localStorageTag: string = 'sys_user_main_grid';

    /**
     * 所有列成员
     *
     * @type {any[]}
     * @memberof MainGridBase
     */
    public allColumns: any[] = [
        {
            name: 'userid',
            label: '用户标识',
            langtag: 'entities.sysuser.main_grid.columns.userid',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'username',
            label: '用户全局名',
            langtag: 'entities.sysuser.main_grid.columns.username',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'personname',
            label: '用户姓名',
            langtag: 'entities.sysuser.main_grid.columns.personname',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'loginname',
            label: '登录名',
            langtag: 'entities.sysuser.main_grid.columns.loginname',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'orgname',
            label: '单位名称',
            langtag: 'entities.sysuser.main_grid.columns.orgname',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'mdeptname',
            label: '主部门名称',
            langtag: 'entities.sysuser.main_grid.columns.mdeptname',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
    ]

    /**
     * 获取表格行模型
     *
     * @type {*}
     * @memberof MainGridBase
     */
    public getGridRowModel(){
        return {
          srfkey: new FormItemModel(),
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof MainGridBase
     */
    public rules: any = {
        srfkey: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '用户标识 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '用户标识 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 获取对应列class
     *
     * @type {*}
     * @memberof MainBase
     */
    public hasRowEdit: any = {
        'userid':false,
        'username':false,
        'personname':false,
        'loginname':false,
        'orgname':false,
        'mdeptname':false,
    };

    /**
     * 获取对应列class
     *
     * @param {*} $args row 行数据，column 列数据，rowIndex 行索引，列索引
     * @returns {void}
     * @memberof MainBase
     */
    public getCellClassName(args: {row: any, column: any, rowIndex: number, columnIndex: number}): any {
        return ( this.hasRowEdit[args.column.property] && this.actualIsOpenEdit ) ? "edit-cell" : "info-cell";
    }


    /**
     * 导出数据格式化
     *
     * @param {*} filterVal
     * @param {*} jsonData
     * @param {any[]} [codelistColumns=[]]
     * @returns {Promise<any>}
     * @memberof MainGridBase
     */
    public async formatExcelData(filterVal: any, jsonData: any, codelistColumns?: any[]): Promise<any> {
        return super.formatExcelData(filterVal, jsonData, [
        ]);
    }

}