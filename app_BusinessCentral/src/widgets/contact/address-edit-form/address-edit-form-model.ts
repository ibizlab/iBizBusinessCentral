/**
 * AddressEdit 部件模型
 *
 * @export
 * @class AddressEditModel
 */
export default class AddressEditModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof AddressEditModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'contactid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'fullname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'address1_country',
        prop: 'address1_country',
        dataType: 'TEXT',
      },
      {
        name: 'address1_stateorprovince',
        prop: 'address1_stateorprovince',
        dataType: 'TEXT',
      },
      {
        name: 'address1_city',
        prop: 'address1_city',
        dataType: 'TEXT',
      },
      {
        name: 'address1_line1',
        prop: 'address1_line1',
        dataType: 'TEXT',
      },
      {
        name: 'address1_postalcode',
        prop: 'address1_postalcode',
        dataType: 'TEXT',
      },
      {
        name: 'contactid',
        prop: 'contactid',
        dataType: 'GUID',
      },
      {
        name: 'contact',
        prop: 'contactid',
        dataType: 'FONTKEY',
      },
    ]
  }

}