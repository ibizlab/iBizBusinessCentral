import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, GridControlBase } from '@/studio-core';
import ContactService from '@/service/contact/contact-service';
import ByAccountService from './by-account-grid-service';
import ContactUIService from '@/uiservice/contact/contact-ui-service';
import { FormItemModel } from '@/model/form-detail';


/**
 * grid部件基类
 *
 * @export
 * @class GridControlBase
 * @extends {ByAccountGridBase}
 */
export class ByAccountGridBase extends GridControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof ByAccountGridBase
     */
    protected controlType: string = 'GRID';

    /**
     * 建构部件服务对象
     *
     * @type {ByAccountService}
     * @memberof ByAccountGridBase
     */
    public service: ByAccountService = new ByAccountService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {ContactService}
     * @memberof ByAccountGridBase
     */
    public appEntityService: ContactService = new ContactService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof ByAccountGridBase
     */
    protected appDeName: string = 'contact';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof ByAccountGridBase
     */
    protected appDeLogicName: string = '联系人';

    /**
     * 界面UI服务对象
     *
     * @type {ContactUIService}
     * @memberof ByAccountBase
     */  
    public appUIService:ContactUIService = new ContactUIService(this.$store);

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public grid_uagridcolumn1_u771bf36_click(params: any = {}, tag?: any, $event?: any) {
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        xData = this;
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:ContactUIService  = new ContactUIService();
        curUIService.Contact_SetPrimary(datas,contextJO, paramJO,  $event, xData,this,"Contact");
    }

    /**
     * 界面行为模型
     *
     * @type {*}
     * @memberof ByAccountBase
     */  
    public ActionModel: any = {
        SetPrimary: { name: 'SetPrimary',disabled: false, visabled: true,noprivdisplaymode:2,dataaccaction: '', actiontarget: 'SINGLEKEY'}
    };

    /**
     * 本地缓存标识
     *
     * @protected
     * @type {string}
     * @memberof ByAccountBase
     */
    protected localStorageTag: string = 'contact_byaccount_grid';

    /**
     * 是否支持分页
     *
     * @type {boolean}
     * @memberof ByAccountGridBase
     */
    public isEnablePagingBar: boolean = false;

    /**
     * 分页条数
     *
     * @type {number}
     * @memberof ByAccountGridBase
     */
    public limit: number = 50;

    /**
     * 所有列成员
     *
     * @type {any[]}
     * @memberof ByAccountGridBase
     */
    public allColumns: any[] = [
        {
            name: 'fullname',
            label: '全名',
            langtag: 'entities.contact.byaccount_grid.columns.fullname',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'emailaddress1',
            label: '电子邮件',
            langtag: 'entities.contact.byaccount_grid.columns.emailaddress1',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'parentcustomerid',
            label: '公司名称',
            langtag: 'entities.contact.byaccount_grid.columns.parentcustomerid',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'telephone1',
            label: '商务电话',
            langtag: 'entities.contact.byaccount_grid.columns.telephone1',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'uagridcolumn1',
            label: '设置主联系人',
            langtag: 'entities.contact.byaccount_grid.columns.uagridcolumn1',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
    ]

    /**
     * 获取表格行模型
     *
     * @type {*}
     * @memberof ByAccountGridBase
     */
    public getGridRowModel(){
        return {
          srfkey: new FormItemModel(),
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof ByAccountGridBase
     */
    public rules: any = {
        srfkey: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '联系人 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '联系人 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 获取对应列class
     *
     * @type {*}
     * @memberof ByAccountBase
     */
    public hasRowEdit: any = {
        'fullname':false,
        'emailaddress1':false,
        'parentcustomerid':false,
        'telephone1':false,
        'uagridcolumn1':false,
    };

    /**
     * 获取对应列class
     *
     * @param {*} $args row 行数据，column 列数据，rowIndex 行索引，列索引
     * @returns {void}
     * @memberof ByAccountBase
     */
    public getCellClassName(args: {row: any, column: any, rowIndex: number, columnIndex: number}): any {
        return ( this.hasRowEdit[args.column.property] && this.actualIsOpenEdit ) ? "edit-cell" : "info-cell";
    }


    /**
     * 导出数据格式化
     *
     * @param {*} filterVal
     * @param {*} jsonData
     * @param {any[]} [codelistColumns=[]]
     * @returns {Promise<any>}
     * @memberof ByAccountGridBase
     */
    public async formatExcelData(filterVal: any, jsonData: any, codelistColumns?: any[]): Promise<any> {
        return super.formatExcelData(filterVal, jsonData, [
        ]);
    }


    /**
     * 界面行为
     *
     * @param {*} row
     * @param {*} tag
     * @param {*} $event
     * @memberof ByAccountGridBase
     */
	public uiAction(row: any, tag: any, $event: any): void {
        $event.stopPropagation();
        if(Object.is('SetPrimary', tag)) {
            this.grid_uagridcolumn1_u771bf36_click(row, tag, $event);
        }
    }
}