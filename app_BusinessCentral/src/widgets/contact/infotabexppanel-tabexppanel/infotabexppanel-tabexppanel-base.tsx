import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, TabExpPanelControlBase } from '@/studio-core';
import ContactService from '@/service/contact/contact-service';
import InfotabexppanelService from './infotabexppanel-tabexppanel-service';
import ContactUIService from '@/uiservice/contact/contact-ui-service';


/**
 * tabexppanel部件基类
 *
 * @export
 * @class TabExpPanelControlBase
 * @extends {InfotabexppanelTabexppanelBase}
 */
export class InfotabexppanelTabexppanelBase extends TabExpPanelControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof InfotabexppanelTabexppanelBase
     */
    protected controlType: string = 'TABEXPPANEL';

    /**
     * 建构部件服务对象
     *
     * @type {InfotabexppanelService}
     * @memberof InfotabexppanelTabexppanelBase
     */
    public service: InfotabexppanelService = new InfotabexppanelService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {ContactService}
     * @memberof InfotabexppanelTabexppanelBase
     */
    public appEntityService: ContactService = new ContactService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof InfotabexppanelTabexppanelBase
     */
    protected appDeName: string = 'contact';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof InfotabexppanelTabexppanelBase
     */
    protected appDeLogicName: string = '联系人';

    /**
     * 界面UI服务对象
     *
     * @type {ContactUIService}
     * @memberof InfotabexppanelBase
     */  
    public appUIService:ContactUIService = new ContactUIService(this.$store);
    /**
     * 是否初始化
     *
     * @protected
     * @returns {any}
     * @memberof Infotabexppanel
     */
    protected isInit: any = {
        tabviewpanel:  true ,
        tabviewpanel4:  false ,
        tabviewpanel2:  false ,
        tabviewpanel3:  false ,
        tabviewpanel5:  false ,
    }

    /**
     * 被激活的分页面板
     *
     * @protected
     * @type {string}
     * @memberof Infotabexppanel
     */
    protected activatedTabViewPanel: string = 'tabviewpanel';

    /**
     * 组件创建完毕
     *
     * @protected
     * @memberof Infotabexppanel
     */
    protected ctrlCreated(): void {
        //设置分页导航srfparentdename和srfparentkey
        if (this.context.contact) {
            Object.assign(this.context, { srfparentdename: 'Contact', srfparentkey: this.context.contact });
        }
        super.ctrlCreated();
    }
}