/**
 * Infotabexppanel 部件模型
 *
 * @export
 * @class InfotabexppanelModel
 */
export default class InfotabexppanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof InfotabexppanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'address1_freighttermscode',
      },
      {
        name: 'address3_city',
      },
      {
        name: 'department',
      },
      {
        name: 'address1_stateorprovince',
      },
      {
        name: 'aging90_base',
      },
      {
        name: 'donotbulkpostalmail',
      },
      {
        name: 'managername',
      },
      {
        name: 'donotpostalmail',
      },
      {
        name: 'spousesname',
      },
      {
        name: 'familystatuscode',
      },
      {
        name: 'address3_composite',
      },
      {
        name: 'address3_shippingmethodcode',
      },
      {
        name: 'lastname',
      },
      {
        name: 'lastonholdtime',
      },
      {
        name: 'educationcode',
      },
      {
        name: 'address2_line1',
      },
      {
        name: 'birthdate',
      },
      {
        name: 'haschildrencode',
      },
      {
        name: 'company',
      },
      {
        name: 'address2_fax',
      },
      {
        name: 'assistantphone',
      },
      {
        name: 'callback',
      },
      {
        name: 'paymenttermscode',
      },
      {
        name: 'address2_telephone1',
      },
      {
        name: 'marketingonly',
      },
      {
        name: 'address2_utcoffset',
      },
      {
        name: 'address2_shippingmethodcode',
      },
      {
        name: 'traversedpath',
      },
      {
        name: 'employeeid',
      },
      {
        name: 'creditlimit_base',
      },
      {
        name: 'followemail',
      },
      {
        name: 'address3_postalcode',
      },
      {
        name: 'merged',
      },
      {
        name: 'jobtitle',
      },
      {
        name: 'address1_telephone1',
      },
      {
        name: 'customersizecode',
      },
      {
        name: 'address3_addresstypecode',
      },
      {
        name: 'pager',
      },
      {
        name: 'assistantname',
      },
      {
        name: 'address1_composite',
      },
      {
        name: 'address1_line1',
      },
      {
        name: 'address1_telephone3',
      },
      {
        name: 'telephone2',
      },
      {
        name: 'address2_addressid',
      },
      {
        name: 'leadsourcecode',
      },
      {
        name: 'statecode',
      },
      {
        name: 'address2_freighttermscode',
      },
      {
        name: 'emailaddress1',
      },
      {
        name: 'entityimage_timestamp',
      },
      {
        name: 'address3_line1',
      },
      {
        name: 'salutation',
      },
      {
        name: 'address1_line3',
      },
      {
        name: 'address3_primarycontactname',
      },
      {
        name: 'ibizprivate',
      },
      {
        name: 'donotfax',
      },
      {
        name: 'createdate',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'address3_stateorprovince',
      },
      {
        name: 'address3_line3',
      },
      {
        name: 'creditlimit',
      },
      {
        name: 'timezoneruleversionnumber',
      },
      {
        name: 'parentcustomerid',
      },
      {
        name: 'childrensnames',
      },
      {
        name: 'address1_addresstypecode',
      },
      {
        name: 'accountrolecode',
      },
      {
        name: 'donotphone',
      },
      {
        name: 'managerphone',
      },
      {
        name: 'creditonhold',
      },
      {
        name: 'accountname',
      },
      {
        name: 'updateman',
      },
      {
        name: 'address2_postalcode',
      },
      {
        name: 'address1_line2',
      },
      {
        name: 'parentcustomertype',
      },
      {
        name: 'nickname',
      },
      {
        name: 'versionnumber',
      },
      {
        name: 'entityimage',
      },
      {
        name: 'shippingmethodcode',
      },
      {
        name: 'customertypecode',
      },
      {
        name: 'address2_county',
      },
      {
        name: 'aging90',
      },
      {
        name: 'address2_stateorprovince',
      },
      {
        name: 'address3_utcoffset',
      },
      {
        name: 'fullname',
      },
      {
        name: 'participatesinworkflow',
      },
      {
        name: 'websiteurl',
      },
      {
        name: 'description',
      },
      {
        name: 'address3_telephone1',
      },
      {
        name: 'address2_upszone',
      },
      {
        name: 'address3_county',
      },
      {
        name: 'entityimage_url',
      },
      {
        name: 'address3_upszone',
      },
      {
        name: 'address1_name',
      },
      {
        name: 'autocreate',
      },
      {
        name: 'backofficecustomer',
      },
      {
        name: 'address2_longitude',
      },
      {
        name: 'ownertype',
      },
      {
        name: 'address1_fax',
      },
      {
        name: 'exchangerate',
      },
      {
        name: 'address1_city',
      },
      {
        name: 'entityimageid',
      },
      {
        name: 'address1_telephone2',
      },
      {
        name: 'address2_composite',
      },
      {
        name: 'importsequencenumber',
      },
      {
        name: 'gendercode',
      },
      {
        name: 'annualincome',
      },
      {
        name: 'subscriptionid',
      },
      {
        name: 'territorycode',
      },
      {
        name: 'overriddencreatedon',
      },
      {
        name: 'address3_country',
      },
      {
        name: 'donotbulkemail',
      },
      {
        name: 'address3_telephone2',
      },
      {
        name: 'ownerid',
      },
      {
        name: 'externaluseridentifier',
      },
      {
        name: 'teamsfollowed',
      },
      {
        name: 'donotemail',
      },
      {
        name: 'anniversary',
      },
      {
        name: 'preferredappointmentdaycode',
      },
      {
        name: 'middlename',
      },
      {
        name: 'emailaddress3',
      },
      {
        name: 'address2_telephone2',
      },
      {
        name: 'fax',
      },
      {
        name: 'mobilephone',
      },
      {
        name: 'home2',
      },
      {
        name: 'createman',
      },
      {
        name: 'onholdtime',
      },
      {
        name: 'preferredappointmenttimecode',
      },
      {
        name: 'statuscode',
      },
      {
        name: 'governmentid',
      },
      {
        name: 'address3_telephone3',
      },
      {
        name: 'business2',
      },
      {
        name: 'preferredsystemuserid',
      },
      {
        name: 'address1_upszone',
      },
      {
        name: 'aging60',
      },
      {
        name: 'address3_postofficebox',
      },
      {
        name: 'parentcontactname',
      },
      {
        name: 'ownername',
      },
      {
        name: 'address2_city',
      },
      {
        name: 'processid',
      },
      {
        name: 'annualincome_base',
      },
      {
        name: 'address3_addressid',
      },
      {
        name: 'contact',
        prop: 'contactid',
      },
      {
        name: 'aging60_base',
      },
      {
        name: 'address3_latitude',
      },
      {
        name: 'telephone3',
      },
      {
        name: 'address1_primarycontactname',
      },
      {
        name: 'address3_fax',
      },
      {
        name: 'preferredcontactmethodcode',
      },
      {
        name: 'address1_utcoffset',
      },
      {
        name: 'donotsendmm',
      },
      {
        name: 'address2_telephone3',
      },
      {
        name: 'address2_country',
      },
      {
        name: 'aging30',
      },
      {
        name: 'address2_postofficebox',
      },
      {
        name: 'preferredsystemusername',
      },
      {
        name: 'telephone1',
      },
      {
        name: 'address3_longitude',
      },
      {
        name: 'parentcustomername',
      },
      {
        name: 'lastusedincampaign',
      },
      {
        name: 'ftpsiteurl',
      },
      {
        name: 'aging30_base',
      },
      {
        name: 'address2_name',
      },
      {
        name: 'suffix',
      },
      {
        name: 'address1_county',
      },
      {
        name: 'address2_addresstypecode',
      },
      {
        name: 'address1_longitude',
      },
      {
        name: 'address3_line2',
      },
      {
        name: 'address1_addressid',
      },
      {
        name: 'address2_line3',
      },
      {
        name: 'address2_latitude',
      },
      {
        name: 'address2_line2',
      },
      {
        name: 'address1_shippingmethodcode',
      },
      {
        name: 'address3_freighttermscode',
      },
      {
        name: 'mastercontactname',
      },
      {
        name: 'address1_postofficebox',
      },
      {
        name: 'utcconversiontimezonecode',
      },
      {
        name: 'address3_name',
      },
      {
        name: 'address1_latitude',
      },
      {
        name: 'numberofchildren',
      },
      {
        name: 'address2_primarycontactname',
      },
      {
        name: 'address1_postalcode',
      },
      {
        name: 'stageid',
      },
      {
        name: 'address1_country',
      },
      {
        name: 'emailaddress2',
      },
      {
        name: 'currencyname',
      },
      {
        name: 'preferredservicename',
      },
      {
        name: 'slaname',
      },
      {
        name: 'originatingleadname',
      },
      {
        name: 'preferredequipmentname',
      },
      {
        name: 'customername',
      },
      {
        name: 'defaultpricelevelname',
      },
      {
        name: 'customerid',
      },
      {
        name: 'defaultpricelevelid',
      },
      {
        name: 'preferredequipmentid',
      },
      {
        name: 'transactioncurrencyid',
      },
      {
        name: 'slaid',
      },
      {
        name: 'originatingleadid',
      },
      {
        name: 'preferredserviceid',
      },
    ]
  }


}