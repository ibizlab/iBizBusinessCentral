import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, EditFormControlBase } from '@/studio-core';
import ContactService from '@/service/contact/contact-service';
import Edit_DataPanelService from './edit-data-panel-form-service';
import ContactUIService from '@/uiservice/contact/contact-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';


/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {Edit_DataPanelEditFormBase}
 */
export class Edit_DataPanelEditFormBase extends EditFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof Edit_DataPanelEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {Edit_DataPanelService}
     * @memberof Edit_DataPanelEditFormBase
     */
    public service: Edit_DataPanelService = new Edit_DataPanelService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {ContactService}
     * @memberof Edit_DataPanelEditFormBase
     */
    public appEntityService: ContactService = new ContactService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Edit_DataPanelEditFormBase
     */
    protected appDeName: string = 'contact';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof Edit_DataPanelEditFormBase
     */
    protected appDeLogicName: string = '联系人';

    /**
     * 界面UI服务对象
     *
     * @type {ContactUIService}
     * @memberof Edit_DataPanelBase
     */  
    public appUIService:ContactUIService = new ContactUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof Edit_DataPanelEditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        customersizecode: null,
        statuscode: null,
        ownername: null,
        contactid: null,
        contact:null,
    };

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Edit_DataPanelEditFormBase
     */
    public rules: any = {
        customersizecode: [
            { required: true, type: 'string', message: '客户规模 值不能为空', trigger: 'change' },
            { required: true, type: 'string', message: '客户规模 值不能为空', trigger: 'blur' },
        ],
        statuscode: [
            { required: true, type: 'number', message: '状态 值不能为空', trigger: 'change' },
            { required: true, type: 'number', message: '状态 值不能为空', trigger: 'blur' },
        ],
        ownername: [
            { required: true, type: 'string', message: '负责人 值不能为空', trigger: 'change' },
            { required: true, type: 'string', message: '负责人 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Edit_DataPanelBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof Edit_DataPanelEditFormBase
     */
    public detailsModel: any = {
        group1: new FormGroupPanelModel({ caption: '联系人基本信息', detailType: 'GROUPPANEL', name: 'group1', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.contact.edit_datapanel_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({ caption: '更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 0 }),

        srforikey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfkey: new FormItemModel({ caption: '联系人', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfmajortext: new FormItemModel({ caption: '全名', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srftempmode: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfuf: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfdeid: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfsourcekey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        customersizecode: new FormItemModel({ caption: '客户规模', detailType: 'FORMITEM', name: 'customersizecode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        statuscode: new FormItemModel({ caption: '状态', detailType: 'FORMITEM', name: 'statuscode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        ownername: new FormItemModel({ caption: '负责人', detailType: 'FORMITEM', name: 'ownername', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        contactid: new FormItemModel({ caption: '联系人', detailType: 'FORMITEM', name: 'contactid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

    };
}