import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, EditFormControlBase } from '@/studio-core';
import ContactService from '@/service/contact/contact-service';
import PersonEditService from './person-edit-form-service';
import ContactUIService from '@/uiservice/contact/contact-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';


/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {PersonEditEditFormBase}
 */
export class PersonEditEditFormBase extends EditFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof PersonEditEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {PersonEditService}
     * @memberof PersonEditEditFormBase
     */
    public service: PersonEditService = new PersonEditService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {ContactService}
     * @memberof PersonEditEditFormBase
     */
    public appEntityService: ContactService = new ContactService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof PersonEditEditFormBase
     */
    protected appDeName: string = 'contact';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof PersonEditEditFormBase
     */
    protected appDeLogicName: string = '联系人';

    /**
     * 界面UI服务对象
     *
     * @type {ContactUIService}
     * @memberof PersonEditBase
     */  
    public appUIService:ContactUIService = new ContactUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof PersonEditEditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        gendercode: null,
        familystatuscode: null,
        spousesname: null,
        birthdate: null,
        anniversary: null,
        description: null,
        contactid: null,
        contact:null,
    };

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof PersonEditEditFormBase
     */
    public rules: any = {
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof PersonEditBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof PersonEditEditFormBase
     */
    public detailsModel: any = {
        group1: new FormGroupPanelModel({ caption: '联系人基本信息', detailType: 'GROUPPANEL', name: 'group1', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.contact.personedit_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({ caption: '更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 0 }),

        srforikey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfkey: new FormItemModel({ caption: '联系人', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfmajortext: new FormItemModel({ caption: '全名', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srftempmode: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfuf: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfdeid: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfsourcekey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        gendercode: new FormItemModel({ caption: '性别', detailType: 'FORMITEM', name: 'gendercode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        familystatuscode: new FormItemModel({ caption: '婚姻状况', detailType: 'FORMITEM', name: 'familystatuscode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        spousesname: new FormItemModel({ caption: '配偶/伴侣姓名', detailType: 'FORMITEM', name: 'spousesname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        birthdate: new FormItemModel({ caption: '生日', detailType: 'FORMITEM', name: 'birthdate', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        anniversary: new FormItemModel({ caption: '纪念日', detailType: 'FORMITEM', name: 'anniversary', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        description: new FormItemModel({ caption: '说明', detailType: 'FORMITEM', name: 'description', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        contactid: new FormItemModel({ caption: '联系人', detailType: 'FORMITEM', name: 'contactid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

    };
}