import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, EditFormControlBase } from '@/studio-core';
import ContactService from '@/service/contact/contact-service';
import Edit_AbstractService from './edit-abstract-form-service';
import ContactUIService from '@/uiservice/contact/contact-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';


/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {Edit_AbstractEditFormBase}
 */
export class Edit_AbstractEditFormBase extends EditFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof Edit_AbstractEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {Edit_AbstractService}
     * @memberof Edit_AbstractEditFormBase
     */
    public service: Edit_AbstractService = new Edit_AbstractService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {ContactService}
     * @memberof Edit_AbstractEditFormBase
     */
    public appEntityService: ContactService = new ContactService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Edit_AbstractEditFormBase
     */
    protected appDeName: string = 'contact';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof Edit_AbstractEditFormBase
     */
    protected appDeLogicName: string = '联系人';

    /**
     * 界面UI服务对象
     *
     * @type {ContactUIService}
     * @memberof Edit_AbstractBase
     */  
    public appUIService:ContactUIService = new ContactUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof Edit_AbstractEditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        fullname: null,
        jobtitle: null,
        customername: null,
        emailaddress1: null,
        telephone1: null,
        mobilephone: null,
        fax: null,
        address1_country: null,
        address1_stateorprovince: null,
        address1_city: null,
        address1_line1: null,
        address1_postalcode: null,
        gendercode: null,
        familystatuscode: null,
        spousesname: null,
        birthdate: null,
        anniversary: null,
        description: null,
        originatingleadname: null,
        lastusedincampaign: null,
        donotsendmm: null,
        shippingmethodcode: null,
        transactioncurrencyname: null,
        creditlimit: null,
        creditonhold: null,
        paymenttermscode: null,
        customerid: null,
        originatingleadid: null,
        transactioncurrencyid: null,
        contactid: null,
        contact:null,
    };

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Edit_AbstractEditFormBase
     */
    public rules: any = {
        fullname: [
            { required: true, type: 'string', message: '全名 值不能为空', trigger: 'change' },
            { required: true, type: 'string', message: '全名 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Edit_AbstractBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof Edit_AbstractEditFormBase
     */
    public detailsModel: any = {
        group1: new FormGroupPanelModel({ caption: '联系人基本信息', detailType: 'GROUPPANEL', name: 'group1', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.contact.edit_abstract_form', extractMode: 'ITEM', details: [] } }),

        grouppanel1: new FormGroupPanelModel({ caption: '地址信息', detailType: 'GROUPPANEL', name: 'grouppanel1', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.contact.edit_abstract_form', extractMode: 'ITEM', details: [] } }),

        grouppanel2: new FormGroupPanelModel({ caption: '个人信息', detailType: 'GROUPPANEL', name: 'grouppanel2', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.contact.edit_abstract_form', extractMode: 'ITEM', details: [] } }),

        grouppanel3: new FormGroupPanelModel({ caption: '市场营销信息', detailType: 'GROUPPANEL', name: 'grouppanel3', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.contact.edit_abstract_form', extractMode: 'ITEM', details: [] } }),

        grouppanel4: new FormGroupPanelModel({ caption: '记账信息', detailType: 'GROUPPANEL', name: 'grouppanel4', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.contact.edit_abstract_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({ caption: '更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 0 }),

        srforikey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfkey: new FormItemModel({ caption: '联系人', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfmajortext: new FormItemModel({ caption: '全名', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srftempmode: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfuf: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfdeid: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfsourcekey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        fullname: new FormItemModel({ caption: '全名', detailType: 'FORMITEM', name: 'fullname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        jobtitle: new FormItemModel({ caption: '职务', detailType: 'FORMITEM', name: 'jobtitle', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        customername: new FormItemModel({ caption: '客户', detailType: 'FORMITEM', name: 'customername', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        emailaddress1: new FormItemModel({ caption: '电子邮件', detailType: 'FORMITEM', name: 'emailaddress1', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        telephone1: new FormItemModel({ caption: '商务电话', detailType: 'FORMITEM', name: 'telephone1', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        mobilephone: new FormItemModel({ caption: '移动电话', detailType: 'FORMITEM', name: 'mobilephone', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        fax: new FormItemModel({ caption: '传真', detailType: 'FORMITEM', name: 'fax', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        address1_country: new FormItemModel({ caption: '国家/地区', detailType: 'FORMITEM', name: 'address1_country', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        address1_stateorprovince: new FormItemModel({ caption: '省/市/自治区', detailType: 'FORMITEM', name: 'address1_stateorprovince', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        address1_city: new FormItemModel({ caption: '市/县', detailType: 'FORMITEM', name: 'address1_city', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        address1_line1: new FormItemModel({ caption: '街道', detailType: 'FORMITEM', name: 'address1_line1', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        address1_postalcode: new FormItemModel({ caption: '邮政编码', detailType: 'FORMITEM', name: 'address1_postalcode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        gendercode: new FormItemModel({ caption: '性别', detailType: 'FORMITEM', name: 'gendercode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        familystatuscode: new FormItemModel({ caption: '婚姻状况', detailType: 'FORMITEM', name: 'familystatuscode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        spousesname: new FormItemModel({ caption: '配偶/伴侣姓名', detailType: 'FORMITEM', name: 'spousesname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        birthdate: new FormItemModel({ caption: '生日', detailType: 'FORMITEM', name: 'birthdate', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        anniversary: new FormItemModel({ caption: '纪念日', detailType: 'FORMITEM', name: 'anniversary', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        description: new FormItemModel({ caption: '说明', detailType: 'FORMITEM', name: 'description', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        originatingleadname: new FormItemModel({ caption: '原始潜在顾客', detailType: 'FORMITEM', name: 'originatingleadname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        lastusedincampaign: new FormItemModel({ caption: '上次参与市场活动的日期', detailType: 'FORMITEM', name: 'lastusedincampaign', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        donotsendmm: new FormItemModel({ caption: '发送市场营销资料', detailType: 'FORMITEM', name: 'donotsendmm', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        shippingmethodcode: new FormItemModel({ caption: '送货方式', detailType: 'FORMITEM', name: 'shippingmethodcode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        transactioncurrencyname: new FormItemModel({ caption: '货币', detailType: 'FORMITEM', name: 'transactioncurrencyname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        creditlimit: new FormItemModel({ caption: '信用额度', detailType: 'FORMITEM', name: 'creditlimit', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        creditonhold: new FormItemModel({ caption: '信用冻结', detailType: 'FORMITEM', name: 'creditonhold', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        paymenttermscode: new FormItemModel({ caption: '付款方式', detailType: 'FORMITEM', name: 'paymenttermscode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        customerid: new FormItemModel({ caption: '客户', detailType: 'FORMITEM', name: 'customerid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        originatingleadid: new FormItemModel({ caption: '原始潜在顾客', detailType: 'FORMITEM', name: 'originatingleadid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        transactioncurrencyid: new FormItemModel({ caption: '货币', detailType: 'FORMITEM', name: 'transactioncurrencyid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        contactid: new FormItemModel({ caption: '联系人', detailType: 'FORMITEM', name: 'contactid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

    };
}