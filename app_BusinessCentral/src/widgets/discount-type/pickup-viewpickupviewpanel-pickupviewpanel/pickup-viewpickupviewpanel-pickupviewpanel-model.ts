/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'timezoneruleversionnumber',
      },
      {
        name: 'statuscode',
      },
      {
        name: 'discounttype',
        prop: 'discounttypeid',
      },
      {
        name: 'createdate',
      },
      {
        name: 'updateman',
      },
      {
        name: 'amounttype',
      },
      {
        name: 'versionnumber',
      },
      {
        name: 'createman',
      },
      {
        name: 'discounttypename',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'description',
      },
      {
        name: 'utcconversiontimezonecode',
      },
      {
        name: 'overriddencreatedon',
      },
      {
        name: 'importsequencenumber',
      },
      {
        name: 'statecode',
      },
      {
        name: 'transactioncurrencyid',
      },
    ]
  }


}