import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, GridControlBase } from '@/studio-core';
import ConnectionRoleService from '@/service/connection-role/connection-role-service';
import MainService from './main-grid-service';
import ConnectionRoleUIService from '@/uiservice/connection-role/connection-role-ui-service';
import { FormItemModel } from '@/model/form-detail';


/**
 * grid部件基类
 *
 * @export
 * @class GridControlBase
 * @extends {MainGridBase}
 */
export class MainGridBase extends GridControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected controlType: string = 'GRID';

    /**
     * 建构部件服务对象
     *
     * @type {MainService}
     * @memberof MainGridBase
     */
    public service: MainService = new MainService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {ConnectionRoleService}
     * @memberof MainGridBase
     */
    public appEntityService: ConnectionRoleService = new ConnectionRoleService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected appDeName: string = 'connectionrole';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected appDeLogicName: string = '连接角色';

    /**
     * 界面UI服务对象
     *
     * @type {ConnectionRoleUIService}
     * @memberof MainBase
     */  
    public appUIService:ConnectionRoleUIService = new ConnectionRoleUIService(this.$store);

    /**
     * 界面行为模型
     *
     * @type {*}
     * @memberof MainBase
     */  
    public ActionModel: any = {
    };

    /**
     * 本地缓存标识
     *
     * @protected
     * @type {string}
     * @memberof MainBase
     */
    protected localStorageTag: string = 'connectionrole_main_grid';

    /**
     * 所有列成员
     *
     * @type {any[]}
     * @memberof MainGridBase
     */
    public allColumns: any[] = [
        {
            name: 'connectionrolename',
            label: '关联角色名称',
            langtag: 'entities.connectionrole.main_grid.columns.connectionrolename',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'category',
            label: '连接角色类别',
            langtag: 'entities.connectionrole.main_grid.columns.category',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'componentstate',
            label: '组件状态',
            langtag: 'entities.connectionrole.main_grid.columns.componentstate',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'managed',
            label: '状态',
            langtag: 'entities.connectionrole.main_grid.columns.managed',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'statecode',
            label: '状态',
            langtag: 'entities.connectionrole.main_grid.columns.statecode',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'statuscode',
            label: '状态描述',
            langtag: 'entities.connectionrole.main_grid.columns.statuscode',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'description',
            label: '说明',
            langtag: 'entities.connectionrole.main_grid.columns.description',
            show: true,
            unit: 'STAR',
            isEnableRowEdit: false,
        },
    ]

    /**
     * 获取表格行模型
     *
     * @type {*}
     * @memberof MainGridBase
     */
    public getGridRowModel(){
        return {
          srfkey: new FormItemModel(),
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof MainGridBase
     */
    public rules: any = {
        srfkey: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '连接角色 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '连接角色 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 获取对应列class
     *
     * @type {*}
     * @memberof MainBase
     */
    public hasRowEdit: any = {
        'connectionrolename':false,
        'category':false,
        'componentstate':false,
        'managed':false,
        'statecode':false,
        'statuscode':false,
        'description':false,
    };

    /**
     * 获取对应列class
     *
     * @param {*} $args row 行数据，column 列数据，rowIndex 行索引，列索引
     * @returns {void}
     * @memberof MainBase
     */
    public getCellClassName(args: {row: any, column: any, rowIndex: number, columnIndex: number}): any {
        return ( this.hasRowEdit[args.column.property] && this.actualIsOpenEdit ) ? "edit-cell" : "info-cell";
    }


    /**
     * 导出数据格式化
     *
     * @param {*} filterVal
     * @param {*} jsonData
     * @param {any[]} [codelistColumns=[]]
     * @returns {Promise<any>}
     * @memberof MainGridBase
     */
    public async formatExcelData(filterVal: any, jsonData: any, codelistColumns?: any[]): Promise<any> {
        return super.formatExcelData(filterVal, jsonData, [
            {
                name: 'category',
                srfkey: 'Connectionrole__Category',
                codelistType : 'STATIC',
                renderMode: 'other',
                textSeparator: '、',
                valueSeparator: ',',
            },
            {
                name: 'componentstate',
                srfkey: 'Connectionrole__ComponentState',
                codelistType : 'STATIC',
                renderMode: 'other',
                textSeparator: '、',
                valueSeparator: ',',
            },
            {
                name: 'managed',
                srfkey: 'YesNo',
                codelistType : 'STATIC',
                renderMode: 'other',
                textSeparator: '、',
                valueSeparator: ',',
            },
            {
                name: 'statecode',
                srfkey: 'Connectionrole__StateCode',
                codelistType : 'STATIC',
                renderMode: 'other',
                textSeparator: '、',
                valueSeparator: ',',
            },
            {
                name: 'statuscode',
                srfkey: 'Connectionrole__StatusCode',
                codelistType : 'STATIC',
                renderMode: 'other',
                textSeparator: '、',
                valueSeparator: ',',
            },
        ]);
    }

}