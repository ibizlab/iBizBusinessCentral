/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'statecode',
          prop: 'statecode',
          dataType: 'NSCODELIST',
        },
        {
          name: 'managed',
          prop: 'managed',
          dataType: 'YESNO',
        },
        {
          name: 'category',
          prop: 'category',
          dataType: 'SSCODELIST',
        },
        {
          name: 'description',
          prop: 'description',
          dataType: 'TEXT',
        },
        {
          name: 'componentstate',
          prop: 'componentstate',
          dataType: 'SSCODELIST',
        },
        {
          name: 'statuscode',
          prop: 'statuscode',
          dataType: 'NSCODELIST',
        },
        {
          name: 'srfmajortext',
          prop: 'connectionrolename',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'connectionroleid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'connectionroleid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'connectionrolename',
          prop: 'connectionrolename',
          dataType: 'TEXT',
        },
        {
          name: 'connectionrole',
          prop: 'connectionroleid',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}