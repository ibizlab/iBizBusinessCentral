/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'overwritetime',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'managed',
      },
      {
        name: 'supportingsolutionid',
      },
      {
        name: 'componentstate',
      },
      {
        name: 'category',
      },
      {
        name: 'createdate',
      },
      {
        name: 'importsequencenumber',
      },
      {
        name: 'connectionrolename',
      },
      {
        name: 'statuscode',
      },
      {
        name: 'connectionrole',
        prop: 'connectionroleid',
      },
      {
        name: 'connectionroleidunique',
      },
      {
        name: 'versionnumber',
      },
      {
        name: 'updateman',
      },
      {
        name: 'solutionid',
      },
      {
        name: 'description',
      },
      {
        name: 'customizable',
      },
      {
        name: 'introducedversion',
      },
      {
        name: 'createman',
      },
      {
        name: 'statecode',
      },
    ]
  }


}