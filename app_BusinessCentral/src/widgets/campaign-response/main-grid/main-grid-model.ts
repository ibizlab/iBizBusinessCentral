/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'actualend',
          prop: 'actualend',
          dataType: 'DATETIME',
        },
        {
          name: 'activitytypecode',
          prop: 'activitytypecode',
          dataType: 'TEXT',
        },
        {
          name: 'actualstart',
          prop: 'actualstart',
          dataType: 'DATETIME',
        },
        {
          name: 'subject',
          prop: 'subject',
          dataType: 'TEXT',
        },
        {
          name: 'transactioncurrencyid',
          prop: 'transactioncurrencyid',
          dataType: 'PICKUP',
        },
        {
          name: 'slaid',
          prop: 'slaid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'subject',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'activityid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'activityid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'campaignresponse',
          prop: 'activityid',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}