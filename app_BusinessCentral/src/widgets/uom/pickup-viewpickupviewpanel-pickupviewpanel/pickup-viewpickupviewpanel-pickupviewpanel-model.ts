/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'overriddencreatedon',
      },
      {
        name: 'baseuomname',
      },
      {
        name: 'updateman',
      },
      {
        name: 'createman',
      },
      {
        name: 'versionnumber',
      },
      {
        name: 'quantity',
      },
      {
        name: 'uom',
        prop: 'uomid',
      },
      {
        name: 'schedulebaseuom',
      },
      {
        name: 'organizationid',
      },
      {
        name: 'createdate',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'uomname',
      },
      {
        name: 'utcconversiontimezonecode',
      },
      {
        name: 'timezoneruleversionnumber',
      },
      {
        name: 'importsequencenumber',
      },
      {
        name: 'uomscheduleid',
      },
      {
        name: 'baseuom',
      },
    ]
  }


}