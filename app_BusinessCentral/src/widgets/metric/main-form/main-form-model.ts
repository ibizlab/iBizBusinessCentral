/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof MainModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'metricid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'metricname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'metricname',
        prop: 'metricname',
        dataType: 'TEXT',
      },
      {
        name: 'amount',
        prop: 'amount',
        dataType: 'YESNO',
      },
      {
        name: 'amountdatatype',
        prop: 'amountdatatype',
        dataType: 'SSCODELIST',
      },
      {
        name: 'stretchtracked',
        prop: 'stretchtracked',
        dataType: 'YESNO',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'TEXT',
      },
      {
        name: 'metricid',
        prop: 'metricid',
        dataType: 'GUID',
      },
      {
        name: 'metric',
        prop: 'metricid',
        dataType: 'FONTKEY',
      },
    ]
  }

}