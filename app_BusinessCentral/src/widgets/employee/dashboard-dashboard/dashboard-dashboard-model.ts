/**
 * Dashboard 部件模型
 *
 * @export
 * @class DashboardModel
 */
export default class DashboardModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof DashboardModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'employeename',
      },
      {
        name: 'updateman',
      },
      {
        name: 'createman',
      },
      {
        name: 'employee',
        prop: 'employeeid',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'createdate',
      },
      {
        name: 'organizationid',
      },
      {
        name: 'orgcode',
      },
      {
        name: 'orglevel',
      },
      {
        name: 'showorder',
      },
      {
        name: 'shortname',
      },
      {
        name: 'employeecode',
      },
      {
        name: 'birthday',
      },
      {
        name: 'certnum',
      },
      {
        name: 'age',
      },
      {
        name: 'mobile',
      },
      {
        name: 'email',
      },
      {
        name: 'birthaddress',
      },
      {
        name: 'postaladdress',
      },
      {
        name: 'hobby',
      },
      {
        name: 'nativeaddress',
      },
      {
        name: 'startorgtime',
      },
      {
        name: 'health',
      },
      {
        name: 'startworktime',
      },
      {
        name: 'photo',
      },
      {
        name: 'technicaltitle',
      },
      {
        name: 'nativeplace',
      },
      {
        name: 'certificates',
      },
      {
        name: 'telephone',
      },
      {
        name: 'bloodtype',
      },
      {
        name: 'nation',
      },
      {
        name: 'certtype',
      },
      {
        name: 'sex',
      },
      {
        name: 'marriage',
      },
      {
        name: 'nativetype',
      },
      {
        name: 'political',
      },
      {
        name: 'organizationname',
      },
    ]
  }


}