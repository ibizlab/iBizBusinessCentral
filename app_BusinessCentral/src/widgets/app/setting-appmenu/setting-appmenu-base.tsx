import { Vue } from 'vue-property-decorator';

/**
 * 应用菜单基类
 */
export class SettingBase extends Vue {

    /**
     * 菜单点击
     *
     * @param {*} item 菜单数据
     * @memberof Setting
     */
    public click(item: any) {
        if (item) {
            let judge = true;
            switch (item.appfunctag) {
                case 'Auto27': 
                    this.clickAuto27(item); break;
                default:
                    judge = false;
                    console.warn('未指定应用功能');
            }
            if (judge && this.$uiState.isStyle2()) {
                this.$appService.navHistory.reset();
            }
        }
    }
    
    /**
     * 链接角色
     *
     * @param {*} [item={}]
     * @memberof Setting
     */
    public clickAuto27(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'connectionroles', parameterName: 'connectionrole' },
            { pathName: 'gridview', parameterName: 'gridview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }

    /**
     * 绘制内容
     *
     * @private
     * @memberof Setting
     */
    public render(): any {
        return <span style="display: none;"/>
    }

}