import { Http,Util,Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import SettingModel from './setting-appmenu-model';


/**
 * Setting 部件服务对象
 *
 * @export
 * @class SettingService
 */
export default class SettingService extends ControlService {

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof SettingService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of SettingService.
     * 
     * @param {*} [opts={}]
     * @memberof SettingService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new SettingModel();
    }

    /**
     * 获取数据
     *
     * @returns {Promise<any>}
     * @memberof Setting
     */
    @Errorlog
    public get(params: any = {}): Promise<any> {
        return Http.getInstance().get('v7/settingappmenu', params);
    }

}