import { Http,Util,Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import CentralModel from './central-appmenu-model';


/**
 * Central 部件服务对象
 *
 * @export
 * @class CentralService
 */
export default class CentralService extends ControlService {

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof CentralService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of CentralService.
     * 
     * @param {*} [opts={}]
     * @memberof CentralService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new CentralModel();
    }

    /**
     * 获取数据
     *
     * @returns {Promise<any>}
     * @memberof Central
     */
    @Errorlog
    public get(params: any = {}): Promise<any> {
        return Http.getInstance().get('v7/centralappmenu', params);
    }

}