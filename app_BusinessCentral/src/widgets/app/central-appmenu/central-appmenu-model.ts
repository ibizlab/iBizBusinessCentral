import { ViewTool } from '@/utils';

/**
 * Central 部件模型
 *
 * @export
 * @class CentralModel
 */
export default class CentralModel {

    /**
     * 菜单项集合
     *
     * @private
     * @type {any[]}
     * @memberof CentralModel
     */
    private items: any[] = [
                {
        	id: '73df7da258a8fd0f68afb4b52dc31e38',
        	name: 'user_menus',
        	text: '用户菜单',
        	type: 'MENUITEM',
        	counterid: '',
        	tooltip: '用户菜单',
        	expanded: false,
        	separator: false,
        	hidden: false,
        	hidesidebar: false,
        	opendefault: false,
        	iconcls: '',
        	icon: '',
        	textcls: '',
        	appfunctag: '',
        	resourcetag: '',
        }
        ,
                {
        	id: 'ee69440287e1034e18fb0be6c422b770',
        	name: 'top_menus',
        	text: '顶部菜单',
        	type: 'MENUITEM',
        	counterid: '',
        	tooltip: '顶部菜单',
        	expanded: false,
        	separator: false,
        	hidden: false,
        	hidesidebar: false,
        	opendefault: false,
        	iconcls: '',
        	icon: '',
        	textcls: '',
        	appfunctag: '',
        	resourcetag: '',
        	items: [
                		        {
                	id: '53209e4357b4a907f89ac7fe3d5bc287',
                	name: 'menuitem3',
                	text: '系统设置',
                	type: 'MENUITEM',
                	counterid: '',
                	tooltip: '系统设置',
                	expanded: false,
                	separator: false,
                	hidden: false,
                	hidesidebar: false,
                	opendefault: false,
                	iconcls: 'fa fa-cog',
                	icon: '',
                	textcls: '',
                	appfunctag: 'Auto26',
                	appfuncyype: 'APPVIEW',
                	viewname: 'setting',
                	resourcetag: '',
                	items: [
                        		        {
                        	id: 'ba861bc02473e14cfbb38e595752be79',
                        	name: 'menuitem39',
                        	text: '区域',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: '区域',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-cog',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: 'Auto24',
                        	appfuncyype: 'APPVIEW',
                        	viewname: 'territory-grid-view',
                        	resourcetag: '',
                        }
                        ,
                        		        {
                        	id: '8d6e526f7b5d2d14fe1d433926c5c2bb',
                        	name: 'menuitem26',
                        	text: '货币',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: '货币',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-rmb',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: 'Auto29',
                        	appfuncyype: 'APPVIEW',
                        	viewname: 'transaction-currency-grid-view',
                        	resourcetag: '',
                        }
                        ,
                        		        {
                        	id: 'fda4e1030d9603fcffa274a0b01d85b6',
                        	name: 'menuitem4',
                        	text: '链接角色',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: '链接角色',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-link',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: 'Auto27',
                        	appfuncyype: 'APPVIEW',
                        	viewname: 'connection-role-grid-view',
                        	resourcetag: '',
                        }
                        ,
                        		        {
                        	id: 'dde7f6157aa18ed1ce409f54266bc18f',
                        	name: 'menuitem33',
                        	text: '计价单位',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: '计价单位',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-cog',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: 'Auto21',
                        	appfuncyype: 'APPVIEW',
                        	viewname: 'uom-grid-view',
                        	resourcetag: '',
                        }
                        ,
                        		        {
                        	id: '4ff857ac470f65ef4953a3569b1e6fd3',
                        	name: 'menuitem36',
                        	text: '计价单位组',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: '计价单位组',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-cog',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: 'Auto18',
                        	appfuncyype: 'APPVIEW',
                        	viewname: 'uom-schedule-grid-view',
                        	resourcetag: '',
                        }
                        ,
                        		        {
                        	id: '2bd0cafcb4f47efe8b90fb64b6c88594',
                        	name: 'menuitem37',
                        	text: '价目表',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: '价目表',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-list-alt',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: 'Auto31',
                        	appfuncyype: 'APPVIEW',
                        	viewname: 'price-level-grid-view',
                        	resourcetag: '',
                        }
                        ,
                        		        {
                        	id: '53892de22c07aa648a0c4eaf3f028e82',
                        	name: 'menuitem38',
                        	text: '折扣表',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: '折扣表',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-th-list',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: 'Auto23',
                        	appfuncyype: 'APPVIEW',
                        	viewname: 'discount-type-grid-view',
                        	resourcetag: '',
                        }
                        ,
                        		        {
                        	id: 'e740ecc27ff2747961413f2b305718ff',
                        	name: 'menuitem31',
                        	text: '目标度量',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: '目标度量',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-line-chart',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: 'Auto28',
                        	appfuncyype: 'APPVIEW',
                        	viewname: 'metric-grid-view',
                        	resourcetag: '',
                        }
                        ,
                        		        {
                        	id: '40AFB32F-FDCB-4D2B-A8B6-2F1065D79841',
                        	name: 'menuitem44',
                        	text: '字典管理',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: '字典管理',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-cog',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: 'Auto39',
                        	appfuncyype: 'APPVIEW',
                        	viewname: 'dict-catalog-grid-view',
                        	resourcetag: '',
                        }
                        ,
                	],
                }
                ,
                		        {
                	id: '8282C485-171B-4F74-A35B-83925308E493',
                	name: 'menuitem53',
                	text: '组织管理',
                	type: 'MENUITEM',
                	counterid: '',
                	tooltip: '组织管理',
                	expanded: false,
                	separator: false,
                	hidden: false,
                	hidesidebar: false,
                	opendefault: false,
                	iconcls: 'fa fa-cog',
                	icon: '',
                	textcls: '',
                	appfunctag: '',
                	resourcetag: '',
                	items: [
                        		        {
                        	id: 'E4345614-CBA1-4A68-BFD3-55850064E654',
                        	name: 'menuitem54',
                        	text: '单位管理',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: '单位管理',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-cog',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: 'Auto43',
                        	appfuncyype: 'APPVIEW',
                        	viewname: 'ibzorganization-grid-view',
                        	resourcetag: '',
                        }
                        ,
                        		        {
                        	id: '1962972A-8ACA-43D0-AF0F-96C44B2B0E28',
                        	name: 'menuitem55',
                        	text: '部门管理',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: '部门管理',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-cog',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: 'Auto38',
                        	appfuncyype: 'APPVIEW',
                        	viewname: 'ibzorganization-tree-exp-view',
                        	resourcetag: '',
                        }
                        ,
                        		        {
                        	id: '7A0D6DD1-1846-4E3F-BB3C-2C522B25CB7E',
                        	name: 'menuitem56',
                        	text: '人员管理',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: '人员管理',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-cog',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: 'Auto33',
                        	appfuncyype: 'APPVIEW',
                        	viewname: 'ibzdepartment-tree-exp-view',
                        	resourcetag: '',
                        }
                        ,
                        		        {
                        	id: '25CCB3C2-2535-4A84-83C1-0E43ED9CA535',
                        	name: 'menuitem57',
                        	text: '岗位管理',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: '岗位管理',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-cog',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: 'Auto25',
                        	appfuncyype: 'APPVIEW',
                        	viewname: 'ibzpost-grid-view',
                        	resourcetag: '',
                        }
                        ,
                	],
                }
                ,
                		        {
                	id: '188D3EC6-9B18-4EAD-9F9E-7AA2E55ACD3B',
                	name: 'menuitem17',
                	text: '系统权限',
                	type: 'MENUITEM',
                	counterid: '',
                	tooltip: '系统权限',
                	expanded: false,
                	separator: false,
                	hidden: false,
                	hidesidebar: false,
                	opendefault: false,
                	iconcls: 'fa fa-cog',
                	icon: '',
                	textcls: '',
                	appfunctag: '',
                	resourcetag: '',
                	items: [
                        		        {
                        	id: '4B79877B-065B-4BC8-809E-6066E2F0F993',
                        	name: 'menuitem40',
                        	text: '系统用户',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: '系统用户',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-cog',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: 'Auto41',
                        	appfuncyype: 'APPVIEW',
                        	viewname: 'sys-user-grid-view',
                        	resourcetag: '',
                        }
                        ,
                        		        {
                        	id: '20CA88A4-AD2C-4F35-9CC8-0E96EA7372E2',
                        	name: 'menuitem41',
                        	text: ' 用户角色',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: ' 用户角色',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-cog',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: 'Auto35',
                        	appfuncyype: 'APPVIEW',
                        	viewname: 'sys-role-grid-view',
                        	resourcetag: '',
                        }
                        ,
                        		        {
                        	id: '8A12BF76-EA25-4F50-89DD-4EDDAB14CF43',
                        	name: 'menuitem42',
                        	text: '认证日志',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: '认证日志',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-cog',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: 'Auto37',
                        	appfuncyype: 'APPVIEW',
                        	viewname: 'sys-auth-log-grid-view',
                        	resourcetag: '',
                        }
                        ,
                        		        {
                        	id: 'EA69BCE7-1CE7-4E83-8C97-0A4C7CDC5189',
                        	name: 'menuitem43',
                        	text: '接入应用',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: '接入应用',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-cog',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: 'Auto40',
                        	appfuncyype: 'APPVIEW',
                        	viewname: 'sys-app-grid-view',
                        	resourcetag: '',
                        }
                        ,
                	],
                }
                ,
                		        {
                	id: '945C4497-846D-49FA-A428-AA0A6BABBCB2',
                	name: 'menuitem45',
                	text: '任务管理',
                	type: 'MENUITEM',
                	counterid: '',
                	tooltip: '任务管理',
                	expanded: false,
                	separator: false,
                	hidden: false,
                	hidesidebar: false,
                	opendefault: false,
                	iconcls: 'fa fa-cog',
                	icon: '',
                	textcls: '',
                	appfunctag: '',
                	resourcetag: '',
                	items: [
                        		        {
                        	id: '2190FD2E-BC6F-43A7-A72D-36DCA94FDE22',
                        	name: 'menuitem46',
                        	text: ' 任务注册',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: ' 任务注册',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-cog',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: 'Auto36',
                        	appfuncyype: 'APPVIEW',
                        	viewname: 'jobs-registry-grid-view',
                        	resourcetag: '',
                        }
                        ,
                        		        {
                        	id: 'E83CAA87-8659-4EF2-8D47-D4881A074986',
                        	name: 'menuitem47',
                        	text: ' 任务管理',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: ' 任务管理',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-cog',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: 'Auto20',
                        	appfuncyype: 'APPVIEW',
                        	viewname: 'jobs-info-grid-view',
                        	resourcetag: '',
                        }
                        ,
                        		        {
                        	id: '9E7F0D04-1BF4-4972-8F66-C10E4ED1DB51',
                        	name: 'menuitem48',
                        	text: '任务日志',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: '任务日志',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-cog',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: 'Auto34',
                        	appfuncyype: 'APPVIEW',
                        	viewname: 'jobs-log-grid-view',
                        	resourcetag: '',
                        }
                        ,
                	],
                }
                ,
                		        {
                	id: '183DE402-3DD7-4D05-B33E-77AA9D2EC763',
                	name: 'menuitem49',
                	text: '流程管理',
                	type: 'MENUITEM',
                	counterid: '',
                	tooltip: '流程管理',
                	expanded: false,
                	separator: false,
                	hidden: false,
                	hidesidebar: false,
                	opendefault: false,
                	iconcls: 'fa fa-cog',
                	icon: '',
                	textcls: '',
                	appfunctag: '',
                	resourcetag: '',
                	items: [
                        		        {
                        	id: 'C794FD4E-67D2-41A0-A7DD-81288E2BF731',
                        	name: 'menuitem50',
                        	text: '流程定义',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: '流程定义',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-cog',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: 'Auto32',
                        	appfuncyype: 'APPVIEW',
                        	viewname: 'wfprocess-definition-grid-view',
                        	resourcetag: '',
                        }
                        ,
                        		        {
                        	id: 'C838A850-7FA6-455C-BF51-3385F8C89318',
                        	name: 'menuitem51',
                        	text: '流程角色',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: '流程角色',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-cog',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: 'Auto44',
                        	appfuncyype: 'APPVIEW',
                        	viewname: 'wfgroup-grid-view',
                        	resourcetag: '',
                        }
                        ,
                        		        {
                        	id: '8FF530DF-004C-4810-B845-A50B2F3B8301',
                        	name: 'menuitem52',
                        	text: '发布新流程',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: '发布新流程',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-cog',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: 'Auto42',
                        	appfuncyype: 'APPVIEW',
                        	viewname: 'wfremodel-grid-view',
                        	resourcetag: '',
                        }
                        ,
                	],
                }
                ,
                		        {
                	id: '99ec07537e66a1b9bf48c217f2f98f5b',
                	name: 'menuitem34',
                	text: '消息通知',
                	type: 'MENUITEM',
                	counterid: '',
                	tooltip: '消息通知',
                	expanded: false,
                	separator: false,
                	hidden: false,
                	hidesidebar: false,
                	opendefault: false,
                	iconcls: 'fa fa-bell-o',
                	icon: '',
                	textcls: '',
                	appfunctag: '',
                	resourcetag: '',
                }
                ,
                		        {
                	id: 'cbb7e5badd9fc06ed968b3131c0cad4a',
                	name: 'menuitem35',
                	text: '帮助',
                	type: 'MENUITEM',
                	counterid: '',
                	tooltip: '帮助',
                	expanded: false,
                	separator: false,
                	hidden: false,
                	hidesidebar: false,
                	opendefault: false,
                	iconcls: 'fa fa-question',
                	icon: '',
                	textcls: '',
                	appfunctag: '',
                	resourcetag: '',
                }
                ,
        	],
        }
        ,
                {
        	id: 'd9c980333990344f55261e46d543f682',
        	name: 'left_exp',
        	text: '左侧菜单',
        	type: 'MENUITEM',
        	counterid: '',
        	tooltip: '左侧菜单',
        	expanded: false,
        	separator: false,
        	hidden: false,
        	hidesidebar: false,
        	opendefault: false,
        	iconcls: '',
        	icon: '',
        	textcls: '',
        	appfunctag: '',
        	resourcetag: '',
        	items: [
                		        {
                	id: 'c290b058dccd59dc492487c1e6f13097',
                	name: 'menuitem18',
                	text: '最近',
                	type: 'MENUITEM',
                	counterid: '',
                	tooltip: '最近',
                	expanded: false,
                	separator: false,
                	hidden: false,
                	hidesidebar: false,
                	opendefault: false,
                	iconcls: 'fa fa-clock-o',
                	icon: '',
                	textcls: '',
                	appfunctag: '',
                	resourcetag: '',
                }
                ,
                		        {
                	id: 'f3d4f2e9b88c956ca15e3230636d4ce1',
                	name: 'menuitem19',
                	text: '固定',
                	type: 'MENUITEM',
                	counterid: '',
                	tooltip: '固定',
                	expanded: false,
                	separator: false,
                	hidden: false,
                	hidesidebar: false,
                	opendefault: false,
                	iconcls: 'fa fa-thumb-tack',
                	icon: '',
                	textcls: '',
                	appfunctag: '',
                	resourcetag: '',
                }
                ,
                		        {
                	id: 'BCDCEAC8-4A33-4583-9125-31EF87B78B11',
                	name: 'menuitem24',
                	text: '组织权限管理',
                	type: 'MENUITEM',
                	counterid: '',
                	tooltip: '组织权限管理',
                	expanded: false,
                	separator: false,
                	hidden: false,
                	hidesidebar: false,
                	opendefault: false,
                	iconcls: 'fa fa-lock',
                	icon: '',
                	textcls: '',
                	appfunctag: '',
                	resourcetag: '',
                	items: [
                        		        {
                        	id: '248A43AF-C9B1-4EA8-AF7B-4477509691AD',
                        	name: 'menuitem60',
                        	text: '法人',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: '法人',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-legal',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: '_6',
                        	appfuncyype: 'APPVIEW',
                        	viewname: 'legal-grid-view',
                        	resourcetag: '',
                        }
                        ,
                        		        {
                        	id: 'E2512582-1D1F-4CA0-A6DE-A5ACE4B83520',
                        	name: 'menuitem58',
                        	text: '业务单位',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: '业务单位',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-shirtsinbulk',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: '_5',
                        	appfuncyype: 'APPVIEW',
                        	viewname: 'operation-unit-bumaster-grid-view',
                        	resourcetag: '',
                        }
                        ,
                        		        {
                        	id: '67A3F06D-8C83-4AC5-A7A5-2BD9F34F41FD',
                        	name: 'menuitem59',
                        	text: '部门',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: '部门',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-folder',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: '_4',
                        	appfuncyype: 'APPVIEW',
                        	viewname: 'operation-unit-dept-master-grid-view',
                        	resourcetag: '',
                        }
                        ,
                        		        {
                        	id: '6C2284E3-FAB5-40AF-8A35-00233503FE09',
                        	name: 'menuitem29',
                        	text: '组织层次',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: '组织层次',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-tree',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: '_2',
                        	appfuncyype: 'APPVIEW',
                        	viewname: 'omhierarchy-cat-grid-view',
                        	resourcetag: '',
                        }
                        ,
                        		        {
                        	id: 'D4304080-B955-4CEC-ADB6-BF8036AE1EEA',
                        	name: 'menuitem61',
                        	text: '员工',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: '员工',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-user',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: '_3',
                        	appfuncyype: 'APPVIEW',
                        	viewname: 'employee-grid-view',
                        	resourcetag: '',
                        }
                        ,
                	],
                }
                ,
        	],
        }
        ,
                {
        	id: '1f3aa7ed08c771c43ef211f48f4e0fbf',
        	name: 'bottom_exp',
        	text: '底部内容',
        	type: 'MENUITEM',
        	counterid: '',
        	tooltip: '底部内容',
        	expanded: false,
        	separator: false,
        	hidden: false,
        	hidesidebar: false,
        	opendefault: false,
        	iconcls: '',
        	icon: '',
        	textcls: '',
        	appfunctag: '',
        	resourcetag: '',
        }
        ,
                {
        	id: '5a8c3fff88b801c4e47d5cab5019db8f',
        	name: 'footer_left',
        	text: '底部左侧',
        	type: 'MENUITEM',
        	counterid: '',
        	tooltip: '底部左侧',
        	expanded: false,
        	separator: false,
        	hidden: false,
        	hidesidebar: false,
        	opendefault: false,
        	iconcls: '',
        	icon: '',
        	textcls: '',
        	appfunctag: '',
        	resourcetag: '',
        }
        ,
                {
        	id: '54c31b90a4fa98f651176aa17f1f9b6f',
        	name: 'footer_center',
        	text: '底部中间',
        	type: 'MENUITEM',
        	counterid: '',
        	tooltip: '底部中间',
        	expanded: false,
        	separator: false,
        	hidden: false,
        	hidesidebar: false,
        	opendefault: false,
        	iconcls: '',
        	icon: '',
        	textcls: '',
        	appfunctag: '',
        	resourcetag: '',
        }
        ,
                {
        	id: 'c04d0da2c1b571e222b6b2ba725d0ac8',
        	name: 'footer_right',
        	text: '底部右侧',
        	type: 'MENUITEM',
        	counterid: '',
        	tooltip: '底部右侧',
        	expanded: false,
        	separator: false,
        	hidden: false,
        	hidesidebar: false,
        	opendefault: false,
        	iconcls: '',
        	icon: '',
        	textcls: '',
        	appfunctag: '',
        	resourcetag: '',
        }
        ,
    ];

	/**
	 * 应用功能集合
	 *
	 * @private
	 * @type {any[]}
	 * @memberof CentralModel
	 */
	private funcs: any[] = [
        {
            appfunctag: '_3',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'employeegridview',
            deResParameters: [],
            routepath: '/central/:central?/employees/:employee?/gridview/:gridview?',
            parameters: [
                { pathName: 'employees', parameterName: 'employee' },
                { pathName: 'gridview', parameterName: 'gridview' },
            ],
        },
        {
            appfunctag: 'Auto38',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'ibzorganizationtreeexpview',
            deResParameters: [],
            routepath: '/central/:central?/ibzorganizations/:ibzorganization?/treeexpview/:treeexpview?',
            parameters: [
                { pathName: 'ibzorganizations', parameterName: 'ibzorganization' },
                { pathName: 'treeexpview', parameterName: 'treeexpview' },
            ],
        },
        {
            appfunctag: 'Auto24',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'territorygridview',
            deResParameters: [],
            routepath: '/central/:central?/territories/:territory?/gridview/:gridview?',
            parameters: [
                { pathName: 'territories', parameterName: 'territory' },
                { pathName: 'gridview', parameterName: 'gridview' },
            ],
        },
        {
            appfunctag: '_4',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'operationunitdeptmastergridview',
            deResParameters: [],
            routepath: '/central/:central?/operationunits/:operationunit?/deptmastergridview/:deptmastergridview?',
            parameters: [
                { pathName: 'operationunits', parameterName: 'operationunit' },
                { pathName: 'deptmastergridview', parameterName: 'deptmastergridview' },
            ],
        },
        {
            appfunctag: 'Auto37',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'sysauthloggridview',
            deResParameters: [],
            routepath: '/central/:central?/sysauthlogs/:sysauthlog?/gridview/:gridview?',
            parameters: [
                { pathName: 'sysauthlogs', parameterName: 'sysauthlog' },
                { pathName: 'gridview', parameterName: 'gridview' },
            ],
        },
        {
            appfunctag: 'Auto34',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'jobsloggridview',
            deResParameters: [],
            routepath: '/central/:central?/jobslogs/:jobslog?/gridview/:gridview?',
            parameters: [
                { pathName: 'jobslogs', parameterName: 'jobslog' },
                { pathName: 'gridview', parameterName: 'gridview' },
            ],
        },
        {
            appfunctag: 'Auto32',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'wfprocessdefinitiongridview',
            deResParameters: [],
            routepath: '/central/:central?/wfprocessdefinitions/:wfprocessdefinition?/gridview/:gridview?',
            parameters: [
                { pathName: 'wfprocessdefinitions', parameterName: 'wfprocessdefinition' },
                { pathName: 'gridview', parameterName: 'gridview' },
            ],
        },
        {
            appfunctag: '_2',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'omhierarchycatgridview',
            deResParameters: [],
            routepath: '/central/:central?/omhierarchycats/:omhierarchycat?/gridview/:gridview?',
            parameters: [
                { pathName: 'omhierarchycats', parameterName: 'omhierarchycat' },
                { pathName: 'gridview', parameterName: 'gridview' },
            ],
        },
        {
            appfunctag: 'Auto29',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'transactioncurrencygridview',
            deResParameters: [],
            routepath: '/central/:central?/transactioncurrencies/:transactioncurrency?/gridview/:gridview?',
            parameters: [
                { pathName: 'transactioncurrencies', parameterName: 'transactioncurrency' },
                { pathName: 'gridview', parameterName: 'gridview' },
            ],
        },
        {
            appfunctag: 'Auto23',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'discounttypegridview',
            deResParameters: [],
            routepath: '/central/:central?/discounttypes/:discounttype?/gridview/:gridview?',
            parameters: [
                { pathName: 'discounttypes', parameterName: 'discounttype' },
                { pathName: 'gridview', parameterName: 'gridview' },
            ],
        },
        {
            appfunctag: 'Auto25',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'ibzpostgridview',
            deResParameters: [],
            routepath: '/central/:central?/ibzposts/:ibzpost?/gridview/:gridview?',
            parameters: [
                { pathName: 'ibzposts', parameterName: 'ibzpost' },
                { pathName: 'gridview', parameterName: 'gridview' },
            ],
        },
        {
            appfunctag: '_5',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'operationunitbumastergridview',
            deResParameters: [],
            routepath: '/central/:central?/operationunits/:operationunit?/bumastergridview/:bumastergridview?',
            parameters: [
                { pathName: 'operationunits', parameterName: 'operationunit' },
                { pathName: 'bumastergridview', parameterName: 'bumastergridview' },
            ],
        },
        {
            appfunctag: 'Auto28',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'metricgridview',
            deResParameters: [],
            routepath: '/central/:central?/metrics/:metric?/gridview/:gridview?',
            parameters: [
                { pathName: 'metrics', parameterName: 'metric' },
                { pathName: 'gridview', parameterName: 'gridview' },
            ],
        },
        {
            appfunctag: 'Auto43',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'ibzorganizationgridview',
            deResParameters: [],
            routepath: '/central/:central?/ibzorganizations/:ibzorganization?/gridview/:gridview?',
            parameters: [
                { pathName: 'ibzorganizations', parameterName: 'ibzorganization' },
                { pathName: 'gridview', parameterName: 'gridview' },
            ],
        },
        {
            appfunctag: 'Auto27',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'connectionrolegridview',
            deResParameters: [],
            routepath: '/central/:central?/connectionroles/:connectionrole?/gridview/:gridview?',
            parameters: [
                { pathName: 'connectionroles', parameterName: 'connectionrole' },
                { pathName: 'gridview', parameterName: 'gridview' },
            ],
        },
        {
            appfunctag: 'Auto33',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'ibzdepartmenttreeexpview',
            deResParameters: [],
            routepath: '/central/:central?/ibzdepartments/:ibzdepartment?/treeexpview/:treeexpview?',
            parameters: [
                { pathName: 'ibzdepartments', parameterName: 'ibzdepartment' },
                { pathName: 'treeexpview', parameterName: 'treeexpview' },
            ],
        },
        {
            appfunctag: 'Auto40',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'sysappgridview',
            deResParameters: [],
            routepath: '/central/:central?/sysapps/:sysapp?/gridview/:gridview?',
            parameters: [
                { pathName: 'sysapps', parameterName: 'sysapp' },
                { pathName: 'gridview', parameterName: 'gridview' },
            ],
        },
        {
            appfunctag: 'Auto26',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'setting',
            deResParameters: [],
            routepath: '/central/:central?/setting/:setting?',
            parameters: [
                { pathName: 'setting', parameterName: 'setting' },
            ],
        },
        {
            appfunctag: 'Auto39',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'dictcataloggridview',
            deResParameters: [],
            routepath: '/central/:central?/dictcatalogs/:dictcatalog?/gridview/:gridview?',
            parameters: [
                { pathName: 'dictcatalogs', parameterName: 'dictcatalog' },
                { pathName: 'gridview', parameterName: 'gridview' },
            ],
        },
        {
            appfunctag: 'Auto41',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'sysusergridview',
            deResParameters: [],
            routepath: '/central/:central?/sysusers/:sysuser?/gridview/:gridview?',
            parameters: [
                { pathName: 'sysusers', parameterName: 'sysuser' },
                { pathName: 'gridview', parameterName: 'gridview' },
            ],
        },
        {
            appfunctag: '_6',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'legalgridview',
            deResParameters: [],
            routepath: '/central/:central?/legals/:legal?/gridview/:gridview?',
            parameters: [
                { pathName: 'legals', parameterName: 'legal' },
                { pathName: 'gridview', parameterName: 'gridview' },
            ],
        },
        {
            appfunctag: 'Auto31',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'pricelevelgridview',
            deResParameters: [],
            routepath: '/central/:central?/pricelevels/:pricelevel?/gridview/:gridview?',
            parameters: [
                { pathName: 'pricelevels', parameterName: 'pricelevel' },
                { pathName: 'gridview', parameterName: 'gridview' },
            ],
        },
        {
            appfunctag: 'Auto44',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'wfgroupgridview',
            deResParameters: [],
            routepath: '/central/:central?/wfgroups/:wfgroup?/gridview/:gridview?',
            parameters: [
                { pathName: 'wfgroups', parameterName: 'wfgroup' },
                { pathName: 'gridview', parameterName: 'gridview' },
            ],
        },
        {
            appfunctag: 'Auto42',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'wfremodelgridview',
            deResParameters: [],
            routepath: '/central/:central?/wfremodels/:wfremodel?/gridview/:gridview?',
            parameters: [
                { pathName: 'wfremodels', parameterName: 'wfremodel' },
                { pathName: 'gridview', parameterName: 'gridview' },
            ],
        },
        {
            appfunctag: 'Auto18',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'uomschedulegridview',
            deResParameters: [],
            routepath: '/central/:central?/uomschedules/:uomschedule?/gridview/:gridview?',
            parameters: [
                { pathName: 'uomschedules', parameterName: 'uomschedule' },
                { pathName: 'gridview', parameterName: 'gridview' },
            ],
        },
        {
            appfunctag: 'Auto35',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'sysrolegridview',
            deResParameters: [],
            routepath: '/central/:central?/sysroles/:sysrole?/gridview/:gridview?',
            parameters: [
                { pathName: 'sysroles', parameterName: 'sysrole' },
                { pathName: 'gridview', parameterName: 'gridview' },
            ],
        },
        {
            appfunctag: 'Auto36',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'jobsregistrygridview',
            deResParameters: [],
            routepath: '/central/:central?/jobsregistries/:jobsregistry?/gridview/:gridview?',
            parameters: [
                { pathName: 'jobsregistries', parameterName: 'jobsregistry' },
                { pathName: 'gridview', parameterName: 'gridview' },
            ],
        },
        {
            appfunctag: 'Auto21',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'uomgridview',
            deResParameters: [],
            routepath: '/central/:central?/uoms/:uom?/gridview/:gridview?',
            parameters: [
                { pathName: 'uoms', parameterName: 'uom' },
                { pathName: 'gridview', parameterName: 'gridview' },
            ],
        },
        {
            appfunctag: 'Auto20',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'jobsinfogridview',
            deResParameters: [],
            routepath: '/central/:central?/jobsinfos/:jobsinfo?/gridview/:gridview?',
            parameters: [
                { pathName: 'jobsinfos', parameterName: 'jobsinfo' },
                { pathName: 'gridview', parameterName: 'gridview' },
            ],
        },
	];

	/**
	 * 根据当前路由查找激活菜单
	 *
	 * @param {*} route
	 * @returns {*}
	 * @memberof CentralModel
	 */
	public findActiveMenuByRoute(route: any): any {
		if (route) {
			const func = this.funcs.find((item: any) => {
				if (item.openmode === '') {
					const url: string = ViewTool.buildUpRoutePath(route, route.params, [], item.parameters, [], {});
					return url === route.fullPath;
				}
			});
            if (func) {
			    return this.findMenuByFuncTag(func.appfunctag);
            }
		}
	}

	/**
	 * 根据应用功能id查找菜单项
	 *
	 * @param {string} tag
	 * @param {any[]} [menus=this.items]
	 * @returns {*}
	 * @memberof CentralModel
	 */
	public findMenuByFuncTag(tag: string, menus: any[] = this.items): any {
		let menu: any;
		menus.every((item: any) => {
			if (item.appfunctag === tag) {
				menu = item;
				return false;
			}
			if (item.items) {
				menu = this.findMenuByFuncTag(tag, item.items);
				if (menu) {
					return false;
				}
			}
			return true;
		});
		return menu;
	}

	/**
	 * 查找默认打开菜单
	 *
	 * @param {any[]} [menus=this.items]
	 * @returns {*}
	 * @memberof CentralModel
	 */
	public findDefaultOpenMenu(menus: any[] = this.items): any {
		let menu: any;
		menus.every((item: any) => {
			if (item.opendefault === true) {
				menu = item;
				return false;
			}
			if (item.items) {
				menu = this.findMenuByFuncTag(item.items);
				if (menu) {
					return false;
				}
			}
			return true;
		});
		return menu;
	}

    /**
     * 获取所有菜单项集合
     *
     * @returns {any[]}
     * @memberof CentralModel
     */
    public getAppMenuItems(): any[] {
        return this.items;
    }

	/**
	 * 根据名称获取菜单组
	 *
	 * @param {string} name
	 * @returns {*}
	 * @memberof CentralModel
	 */
	public getMenuGroup(name: string): any {
		return this.items.find((item: any) => Object.is(item.name, name));
	}

    /**
     * 获取所有应用功能集合
     *
     * @returns {any[]}
     * @memberof CentralModel
     */
    public getAppFuncs(): any[] {
        return this.funcs;
    }
}