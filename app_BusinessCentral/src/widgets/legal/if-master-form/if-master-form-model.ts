/**
 * IF_Master 部件模型
 *
 * @export
 * @class IF_MasterModel
 */
export default class IF_MasterModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof IF_MasterModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'legalid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'legalname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'orgcode',
        prop: 'orgcode',
        dataType: 'INHERIT',
      },
      {
        name: 'legalname',
        prop: 'legalname',
        dataType: 'TEXT',
      },
      {
        name: 'shortname',
        prop: 'shortname',
        dataType: 'INHERIT',
      },
      {
        name: 'legalid',
        prop: 'legalid',
        dataType: 'GUID',
      },
      {
        name: 'legal',
        prop: 'legalid',
        dataType: 'FONTKEY',
      },
    ]
  }

}