import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * MasterTabInfoViewtabviewpanel 部件服务对象
 *
 * @export
 * @class MasterTabInfoViewtabviewpanelService
 */
export default class MasterTabInfoViewtabviewpanelService extends ControlService {
}