/**
 * MasterTabInfoViewtabexppanel 部件模型
 *
 * @export
 * @class MasterTabInfoViewtabexppanelModel
 */
export default class MasterTabInfoViewtabexppanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof MasterTabInfoViewtabexppanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'legalname',
      },
      {
        name: 'organizationtype',
      },
      {
        name: 'createman',
      },
      {
        name: 'updateman',
      },
      {
        name: 'legal',
        prop: 'legalid',
      },
      {
        name: 'createdate',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'orgcode',
      },
      {
        name: 'orglevel',
      },
      {
        name: 'showorder',
      },
      {
        name: 'shortname',
      },
    ]
  }


}