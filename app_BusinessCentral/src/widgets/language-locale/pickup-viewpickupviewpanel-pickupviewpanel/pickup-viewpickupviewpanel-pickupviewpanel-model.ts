/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'createdate',
      },
      {
        name: 'languagelocale',
        prop: 'languagelocaleid',
      },
      {
        name: 'languagelocalename',
      },
      {
        name: 'localeid',
      },
      {
        name: 'language',
      },
      {
        name: 'versionnumber',
      },
      {
        name: 'updateman',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'createman',
      },
      {
        name: 'code',
      },
      {
        name: 'statuscode',
      },
      {
        name: 'statecode',
      },
      {
        name: 'region',
      },
    ]
  }


}