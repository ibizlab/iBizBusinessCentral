import { Http,Util,Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import OperationUnitService from '@/service/operation-unit/operation-unit-service';
import MasterTabInfoViewtabexppanelModel from './master-tab-info-viewtabexppanel-tabexppanel-model';


/**
 * MasterTabInfoViewtabexppanel 部件服务对象
 *
 * @export
 * @class MasterTabInfoViewtabexppanelService
 */
export default class MasterTabInfoViewtabexppanelService extends ControlService {

    /**
     * 运营单位服务对象
     *
     * @type {OperationUnitService}
     * @memberof MasterTabInfoViewtabexppanelService
     */
    public appEntityService: OperationUnitService = new OperationUnitService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof MasterTabInfoViewtabexppanelService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of MasterTabInfoViewtabexppanelService.
     * 
     * @param {*} [opts={}]
     * @memberof MasterTabInfoViewtabexppanelService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new MasterTabInfoViewtabexppanelModel();
    }

    
}