/**
 * EF_MasterQuick 部件模型
 *
 * @export
 * @class EF_MasterQuickModel
 */
export default class EF_MasterQuickModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof EF_MasterQuickModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'operationunitid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'operationunitname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'orgcode',
        prop: 'orgcode',
        dataType: 'INHERIT',
      },
      {
        name: 'operationunitname',
        prop: 'operationunitname',
        dataType: 'TEXT',
      },
      {
        name: 'operationunittype',
        prop: 'operationunittype',
        dataType: 'SSCODELIST',
      },
      {
        name: 'operationunitid',
        prop: 'operationunitid',
        dataType: 'GUID',
      },
      {
        name: 'operationunit',
        prop: 'operationunitid',
        dataType: 'FONTKEY',
      },
    ]
  }

}