import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, EditFormControlBase } from '@/studio-core';
import OperationUnitService from '@/service/operation-unit/operation-unit-service';
import EF_DeptMasterQuickService from './ef-dept-master-quick-form-service';
import OperationUnitUIService from '@/uiservice/operation-unit/operation-unit-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';


/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {EF_DeptMasterQuickEditFormBase}
 */
export class EF_DeptMasterQuickEditFormBase extends EditFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof EF_DeptMasterQuickEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {EF_DeptMasterQuickService}
     * @memberof EF_DeptMasterQuickEditFormBase
     */
    public service: EF_DeptMasterQuickService = new EF_DeptMasterQuickService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {OperationUnitService}
     * @memberof EF_DeptMasterQuickEditFormBase
     */
    public appEntityService: OperationUnitService = new OperationUnitService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EF_DeptMasterQuickEditFormBase
     */
    protected appDeName: string = 'operationunit';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof EF_DeptMasterQuickEditFormBase
     */
    protected appDeLogicName: string = '运营单位';

    /**
     * 界面UI服务对象
     *
     * @type {OperationUnitUIService}
     * @memberof EF_DeptMasterQuickBase
     */  
    public appUIService:OperationUnitUIService = new OperationUnitUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof EF_DeptMasterQuickEditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        orgcode: null,
        operationunitname: null,
        operationunittype: null,
        operationunitid: null,
        operationunit:null,
    };

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof EF_DeptMasterQuickEditFormBase
     */
    public rules: any = {
        orgcode: [
            { required: true, type: 'string', message: '组织编码 值不能为空', trigger: 'change' },
            { required: true, type: 'string', message: '组织编码 值不能为空', trigger: 'blur' },
        ],
        operationunitname: [
            { required: true, type: 'string', message: '运营单位名称 值不能为空', trigger: 'change' },
            { required: true, type: 'string', message: '运营单位名称 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof EF_DeptMasterQuickBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof EF_DeptMasterQuickEditFormBase
     */
    public detailsModel: any = {
        group1: new FormGroupPanelModel({ caption: '运营单位基本信息', detailType: 'GROUPPANEL', name: 'group1', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.operationunit.ef_deptmasterquick_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({ caption: '更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srforikey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfkey: new FormItemModel({ caption: '运营单位标识', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfmajortext: new FormItemModel({ caption: '运营单位名称', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srftempmode: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfuf: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfdeid: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfsourcekey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        orgcode: new FormItemModel({ caption: '组织编码', detailType: 'FORMITEM', name: 'orgcode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        operationunitname: new FormItemModel({ caption: '运营单位名称', detailType: 'FORMITEM', name: 'operationunitname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        operationunittype: new FormItemModel({ caption: '运营单位类型', detailType: 'FORMITEM', name: 'operationunittype', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 0 }),

        operationunitid: new FormItemModel({ caption: '运营单位标识', detailType: 'FORMITEM', name: 'operationunitid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

    };

    /**
     * 新建默认值
     * @memberof EF_DeptMasterQuickEditFormBase
     */
    public createDefault(){                    
        if (this.data.hasOwnProperty('operationunittype')) {
            this.data['operationunittype'] = 'DEPARTMENT';
        }
    }
}