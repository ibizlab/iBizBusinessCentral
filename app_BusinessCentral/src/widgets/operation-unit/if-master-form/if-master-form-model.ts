/**
 * IF_Master 部件模型
 *
 * @export
 * @class IF_MasterModel
 */
export default class IF_MasterModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof IF_MasterModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'operationunitid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'operationunitname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'orgcode',
        prop: 'orgcode',
        dataType: 'INHERIT',
      },
      {
        name: 'operationunitname',
        prop: 'operationunitname',
        dataType: 'TEXT',
      },
      {
        name: 'shortname',
        prop: 'shortname',
        dataType: 'INHERIT',
      },
      {
        name: 'operationunittype',
        prop: 'operationunittype',
        dataType: 'SSCODELIST',
      },
      {
        name: 'operationunitid',
        prop: 'operationunitid',
        dataType: 'GUID',
      },
      {
        name: 'operationunit',
        prop: 'operationunitid',
        dataType: 'FONTKEY',
      },
    ]
  }

}