/**
 * InvoiceProductGrid 部件模型
 *
 * @export
 * @class InvoiceProductGridModel
 */
export default class InvoiceProductGridModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof InvoiceProductGridGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof InvoiceProductGridGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'extendedamount',
          prop: 'extendedamount',
          dataType: 'BIGDECIMAL',
        },
        {
          name: 'parentbundleidref',
          prop: 'parentbundleidref',
          dataType: 'PICKUP',
        },
        {
          name: 'transactioncurrencyid',
          prop: 'transactioncurrencyid',
          dataType: 'PICKUP',
        },
        {
          name: 'manualdiscountamount',
          prop: 'manualdiscountamount',
          dataType: 'BIGDECIMAL',
        },
        {
          name: 'salesorderdetailid',
          prop: 'salesorderdetailid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'invoicedetailname',
          dataType: 'TEXT',
        },
        {
          name: 'srfkey',
          prop: 'invoicedetailid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'srfdataaccaction',
          prop: 'invoicedetailid',
          dataType: 'GUID',
        },
        {
          name: 'productid',
          prop: 'productid',
          dataType: 'PICKUP',
        },
        {
          name: 'priceperunit',
          prop: 'priceperunit',
          dataType: 'BIGDECIMAL',
        },
        {
          name: 'quantity',
          prop: 'quantity',
          dataType: 'BIGDECIMAL',
        },
        {
          name: 'uomid',
          prop: 'uomid',
          dataType: 'PICKUP',
        },
        {
          name: 'productname',
          prop: 'productname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'invoiceid',
          prop: 'invoiceid',
          dataType: 'PICKUP',
        },
        {
          name: 'invoicedetail',
          prop: 'invoicedetailid',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}