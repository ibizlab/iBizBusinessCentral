/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'userdata',
          prop: 'userdata',
          dataType: 'TEXT',
        },
        {
          name: 'pickdatainfo',
          prop: 'pickdatainfo',
          dataType: 'LONGTEXT_1000',
        },
        {
          name: 'userdata2',
          prop: 'userdata2',
          dataType: 'TEXT',
        },
        {
          name: 'pickdataname',
          prop: 'pickdataname',
          dataType: 'TEXT',
        },
        {
          name: 'srfmajortext',
          prop: 'pickdataname',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'pickdataid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'pickdataid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'pickdatatype',
          prop: 'pickdatatype',
          dataType: 'SSCODELIST',
        },
        {
          name: 'multipickdata',
          prop: 'pickdataid',
        },
      {
        name: 'n_pickdataname_like',
        prop: 'n_pickdataname_like',
        dataType: 'TEXT',
      },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}