import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, GridControlBase } from '@/studio-core';
import MultiPickDataService from '@/service/multi-pick-data/multi-pick-data-service';
import MainService from './main-grid-service';
import MultiPickDataUIService from '@/uiservice/multi-pick-data/multi-pick-data-ui-service';
import { FormItemModel } from '@/model/form-detail';


/**
 * grid部件基类
 *
 * @export
 * @class GridControlBase
 * @extends {MainGridBase}
 */
export class MainGridBase extends GridControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected controlType: string = 'GRID';

    /**
     * 建构部件服务对象
     *
     * @type {MainService}
     * @memberof MainGridBase
     */
    public service: MainService = new MainService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {MultiPickDataService}
     * @memberof MainGridBase
     */
    public appEntityService: MultiPickDataService = new MultiPickDataService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected appDeName: string = 'multipickdata';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected appDeLogicName: string = '多类选择实体';

    /**
     * 界面UI服务对象
     *
     * @type {MultiPickDataUIService}
     * @memberof MainBase
     */  
    public appUIService:MultiPickDataUIService = new MultiPickDataUIService(this.$store);

    /**
     * 界面行为模型
     *
     * @type {*}
     * @memberof MainBase
     */  
    public ActionModel: any = {
    };

    /**
     * 本地缓存标识
     *
     * @protected
     * @type {string}
     * @memberof MainBase
     */
    protected localStorageTag: string = 'multipickdata_main_grid';

    /**
     * 所有列成员
     *
     * @type {any[]}
     * @memberof MainGridBase
     */
    public allColumns: any[] = [
        {
            name: 'pickdataname',
            label: '数据名称',
            langtag: 'entities.multipickdata.main_grid.columns.pickdataname',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'pickdatatype',
            label: '数据类型',
            langtag: 'entities.multipickdata.main_grid.columns.pickdatatype',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'userdata',
            label: '用户数据',
            langtag: 'entities.multipickdata.main_grid.columns.userdata',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'userdata2',
            label: '用户数据2',
            langtag: 'entities.multipickdata.main_grid.columns.userdata2',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'pickdatainfo',
            label: '信息描述',
            langtag: 'entities.multipickdata.main_grid.columns.pickdatainfo',
            show: true,
            unit: 'STAR',
            isEnableRowEdit: false,
        },
    ]

    /**
     * 获取表格行模型
     *
     * @type {*}
     * @memberof MainGridBase
     */
    public getGridRowModel(){
        return {
          srfkey: new FormItemModel(),
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof MainGridBase
     */
    public rules: any = {
        srfkey: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '数据标识 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '数据标识 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 获取对应列class
     *
     * @type {*}
     * @memberof MainBase
     */
    public hasRowEdit: any = {
        'pickdataname':false,
        'pickdatatype':false,
        'userdata':false,
        'userdata2':false,
        'pickdatainfo':false,
    };

    /**
     * 获取对应列class
     *
     * @param {*} $args row 行数据，column 列数据，rowIndex 行索引，列索引
     * @returns {void}
     * @memberof MainBase
     */
    public getCellClassName(args: {row: any, column: any, rowIndex: number, columnIndex: number}): any {
        return ( this.hasRowEdit[args.column.property] && this.actualIsOpenEdit ) ? "edit-cell" : "info-cell";
    }


    /**
     * 导出数据格式化
     *
     * @param {*} filterVal
     * @param {*} jsonData
     * @param {any[]} [codelistColumns=[]]
     * @returns {Promise<any>}
     * @memberof MainGridBase
     */
    public async formatExcelData(filterVal: any, jsonData: any, codelistColumns?: any[]): Promise<any> {
        return super.formatExcelData(filterVal, jsonData, [
            {
                name: 'pickdatatype',
                srfkey: 'PickDataType',
                codelistType : 'STATIC',
                renderMode: 'other',
                textSeparator: '、',
                valueSeparator: ',',
            },
        ]);
    }

}