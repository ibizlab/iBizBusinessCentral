import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, SearchFormControlBase } from '@/studio-core';
import MultiPickDataService from '@/service/multi-pick-data/multi-pick-data-service';
import ACService from './ac-searchform-service';
import MultiPickDataUIService from '@/uiservice/multi-pick-data/multi-pick-data-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';


/**
 * searchform部件基类
 *
 * @export
 * @class SearchFormControlBase
 * @extends {ACSearchFormBase}
 */
export class ACSearchFormBase extends SearchFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof ACSearchFormBase
     */
    protected controlType: string = 'SEARCHFORM';

    /**
     * 建构部件服务对象
     *
     * @type {ACService}
     * @memberof ACSearchFormBase
     */
    public service: ACService = new ACService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {MultiPickDataService}
     * @memberof ACSearchFormBase
     */
    public appEntityService: MultiPickDataService = new MultiPickDataService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof ACSearchFormBase
     */
    protected appDeName: string = 'multipickdata';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof ACSearchFormBase
     */
    protected appDeLogicName: string = '多类选择实体';

    /**
     * 界面UI服务对象
     *
     * @type {MultiPickDataUIService}
     * @memberof ACBase
     */  
    public appUIService:MultiPickDataUIService = new MultiPickDataUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof ACSearchFormBase
     */
    public data: any = {
        n_pickdataname_like: null,
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof ACSearchFormBase
     */
    public detailsModel: any = {
        formpage1: new FormPageModel({ caption: '常规条件', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this })
, 
        n_pickdataname_like: new FormItemModel({ caption: '负责人', detailType: 'FORMITEM', name: 'n_pickdataname_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
    };
}