/**
 * ACpickupviewpanel 部件模型
 *
 * @export
 * @class ACpickupviewpanelModel
 */
export default class ACpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof ACpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'pickdatainfo',
      },
      {
        name: 'userdata',
      },
      {
        name: 'pickdataname',
      },
      {
        name: 'pickdatatype',
      },
      {
        name: 'userdate2',
      },
      {
        name: 'userdate',
      },
      {
        name: 'multipickdata',
        prop: 'pickdataid',
      },
      {
        name: 'userdata4',
      },
      {
        name: 'userdata2',
      },
      {
        name: 'userdata3',
      },
    ]
  }


}