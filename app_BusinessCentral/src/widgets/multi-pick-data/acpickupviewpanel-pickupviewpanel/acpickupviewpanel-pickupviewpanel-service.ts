import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * ACpickupviewpanel 部件服务对象
 *
 * @export
 * @class ACpickupviewpanelService
 */
export default class ACpickupviewpanelService extends ControlService {
}