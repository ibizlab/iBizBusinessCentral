import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, GridControlBase } from '@/studio-core';
import WFProcessDefinitionService from '@/service/wfprocess-definition/wfprocess-definition-service';
import MainService from './main-grid-service';
import WFProcessDefinitionUIService from '@/uiservice/wfprocess-definition/wfprocess-definition-ui-service';
import { FormItemModel } from '@/model/form-detail';


/**
 * grid部件基类
 *
 * @export
 * @class GridControlBase
 * @extends {MainGridBase}
 */
export class MainGridBase extends GridControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected controlType: string = 'GRID';

    /**
     * 建构部件服务对象
     *
     * @type {MainService}
     * @memberof MainGridBase
     */
    public service: MainService = new MainService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {WFProcessDefinitionService}
     * @memberof MainGridBase
     */
    public appEntityService: WFProcessDefinitionService = new WFProcessDefinitionService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected appDeName: string = 'wfprocessdefinition';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected appDeLogicName: string = '流程定义';

    /**
     * 界面UI服务对象
     *
     * @type {WFProcessDefinitionUIService}
     * @memberof MainBase
     */  
    public appUIService:WFProcessDefinitionUIService = new WFProcessDefinitionUIService(this.$store);

    /**
     * 界面行为模型
     *
     * @type {*}
     * @memberof MainBase
     */  
    public ActionModel: any = {
    };

    /**
     * 本地缓存标识
     *
     * @protected
     * @type {string}
     * @memberof MainBase
     */
    protected localStorageTag: string = 'wf_definition_main_grid';

    /**
     * 所有列成员
     *
     * @type {any[]}
     * @memberof MainGridBase
     */
    public allColumns: any[] = [
        {
            name: 'definitionkey',
            label: 'DefinitionKey',
            langtag: 'entities.wfprocessdefinition.main_grid.columns.definitionkey',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'definitionname',
            label: '流程定义名称',
            langtag: 'entities.wfprocessdefinition.main_grid.columns.definitionname',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'pssystemid',
            label: '系统标识',
            langtag: 'entities.wfprocessdefinition.main_grid.columns.pssystemid',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'modelversion',
            label: '模型版本',
            langtag: 'entities.wfprocessdefinition.main_grid.columns.modelversion',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'modelenable',
            label: '模型是否启用',
            langtag: 'entities.wfprocessdefinition.main_grid.columns.modelenable',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
    ]

    /**
     * 获取表格行模型
     *
     * @type {*}
     * @memberof MainGridBase
     */
    public getGridRowModel(){
        return {
          srfkey: new FormItemModel(),
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof MainGridBase
     */
    public rules: any = {
        srfkey: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: 'DefinitionKey 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: 'DefinitionKey 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 获取对应列class
     *
     * @type {*}
     * @memberof MainBase
     */
    public hasRowEdit: any = {
        'definitionkey':false,
        'definitionname':false,
        'pssystemid':false,
        'modelversion':false,
        'modelenable':false,
    };

    /**
     * 获取对应列class
     *
     * @param {*} $args row 行数据，column 列数据，rowIndex 行索引，列索引
     * @returns {void}
     * @memberof MainBase
     */
    public getCellClassName(args: {row: any, column: any, rowIndex: number, columnIndex: number}): any {
        return ( this.hasRowEdit[args.column.property] && this.actualIsOpenEdit ) ? "edit-cell" : "info-cell";
    }


    /**
     * 导出数据格式化
     *
     * @param {*} filterVal
     * @param {*} jsonData
     * @param {any[]} [codelistColumns=[]]
     * @returns {Promise<any>}
     * @memberof MainGridBase
     */
    public async formatExcelData(filterVal: any, jsonData: any, codelistColumns?: any[]): Promise<any> {
        return super.formatExcelData(filterVal, jsonData, [
            {
                name: 'modelenable',
                srfkey: 'YesNo',
                codelistType : 'STATIC',
                renderMode: 'other',
                textSeparator: '、',
                valueSeparator: ',',
            },
        ]);
    }

}