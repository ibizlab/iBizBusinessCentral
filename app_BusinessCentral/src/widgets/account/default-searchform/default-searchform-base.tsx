import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, SearchFormControlBase } from '@/studio-core';
import AccountService from '@/service/account/account-service';
import DefaultService from './default-searchform-service';
import AccountUIService from '@/uiservice/account/account-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';


/**
 * searchform部件基类
 *
 * @export
 * @class SearchFormControlBase
 * @extends {DefaultSearchFormBase}
 */
export class DefaultSearchFormBase extends SearchFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected controlType: string = 'SEARCHFORM';

    /**
     * 建构部件服务对象
     *
     * @type {DefaultService}
     * @memberof DefaultSearchFormBase
     */
    public service: DefaultService = new DefaultService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {AccountService}
     * @memberof DefaultSearchFormBase
     */
    public appEntityService: AccountService = new AccountService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected appDeName: string = 'account';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected appDeLogicName: string = '客户';

    /**
     * 界面UI服务对象
     *
     * @type {AccountUIService}
     * @memberof DefaultBase
     */  
    public appUIService:AccountUIService = new AccountUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof DefaultSearchFormBase
     */
    public data: any = {
        n_accountname_like: null,
        n_statecode_eq: null,
        n_accountcategorycode_eq: null,
        n_accountclassificationcode_eq: null,
        n_accountratingcode_eq: null,
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof DefaultSearchFormBase
     */
    public detailsModel: any = {
        formpage1: new FormPageModel({ caption: '常规条件', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this })
, 
        n_accountname_like: new FormItemModel({ caption: '客户名称(文本包含(%))', detailType: 'FORMITEM', name: 'n_accountname_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_statecode_eq: new FormItemModel({ caption: '状态(等于(=))', detailType: 'FORMITEM', name: 'n_statecode_eq', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_accountcategorycode_eq: new FormItemModel({ caption: '类别(等于(=))', detailType: 'FORMITEM', name: 'n_accountcategorycode_eq', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_accountclassificationcode_eq: new FormItemModel({ caption: '分类(等于(=))', detailType: 'FORMITEM', name: 'n_accountclassificationcode_eq', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_accountratingcode_eq: new FormItemModel({ caption: '客户等级(等于(=))', detailType: 'FORMITEM', name: 'n_accountratingcode_eq', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
    };
}