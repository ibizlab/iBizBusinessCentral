/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof MainModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'accountid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'accountname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'accountname',
        prop: 'accountname',
        dataType: 'TEXT',
      },
      {
        name: 'telephone1',
        prop: 'telephone1',
        dataType: 'TEXT',
      },
      {
        name: 'fax',
        prop: 'fax',
        dataType: 'TEXT',
      },
      {
        name: 'websiteurl',
        prop: 'websiteurl',
        dataType: 'TEXT',
      },
      {
        name: 'parentaccountname',
        prop: 'parentaccountname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'tickersymbol',
        prop: 'tickersymbol',
        dataType: 'TEXT',
      },
      {
        name: 'customertypecode',
        prop: 'customertypecode',
        dataType: 'SSCODELIST',
      },
      {
        name: 'defaultpricelevelname',
        prop: 'defaultpricelevelname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'industrycode',
        prop: 'industrycode',
        dataType: 'SSCODELIST',
      },
      {
        name: 'sic',
        prop: 'sic',
        dataType: 'TEXT',
      },
      {
        name: 'ownershipcode',
        prop: 'ownershipcode',
        dataType: 'SSCODELIST',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'TEXT',
      },
      {
        name: 'address1_addressid',
        prop: 'address1_addressid',
        dataType: 'TEXT',
      },
      {
        name: 'address1_name',
        prop: 'address1_name',
        dataType: 'TEXT',
      },
      {
        name: 'address1_addresstypecode',
        prop: 'address1_addresstypecode',
        dataType: 'SSCODELIST',
      },
      {
        name: 'address1_country',
        prop: 'address1_country',
        dataType: 'TEXT',
      },
      {
        name: 'address1_stateorprovince',
        prop: 'address1_stateorprovince',
        dataType: 'TEXT',
      },
      {
        name: 'address1_city',
        prop: 'address1_city',
        dataType: 'TEXT',
      },
      {
        name: 'address1_county',
        prop: 'address1_county',
        dataType: 'TEXT',
      },
      {
        name: 'address1_line1',
        prop: 'address1_line1',
        dataType: 'TEXT',
      },
      {
        name: 'address1_fax',
        prop: 'address1_fax',
        dataType: 'TEXT',
      },
      {
        name: 'address1_freighttermscode',
        prop: 'address1_freighttermscode',
        dataType: 'SSCODELIST',
      },
      {
        name: 'address1_postalcode',
        prop: 'address1_postalcode',
        dataType: 'TEXT',
      },
      {
        name: 'preferredcontactmethodcode',
        prop: 'preferredcontactmethodcode',
        dataType: 'SSCODELIST',
      },
      {
        name: 'donotemail',
        prop: 'donotemail',
        dataType: 'YESNO',
      },
      {
        name: 'donotbulkemail',
        prop: 'donotbulkemail',
        dataType: 'YESNO',
      },
      {
        name: 'donotphone',
        prop: 'donotphone',
        dataType: 'YESNO',
      },
      {
        name: 'donotfax',
        prop: 'donotfax',
        dataType: 'YESNO',
      },
      {
        name: 'donotpostalmail',
        prop: 'donotpostalmail',
        dataType: 'YESNO',
      },
      {
        name: 'defaultpricelevelid',
        prop: 'defaultpricelevelid',
        dataType: 'PICKUP',
      },
      {
        name: 'parentaccountid',
        prop: 'parentaccountid',
        dataType: 'PICKUP',
      },
      {
        name: 'accountid',
        prop: 'accountid',
        dataType: 'GUID',
      },
      {
        name: 'account',
        prop: 'accountid',
        dataType: 'FONTKEY',
      },
    ]
  }

}