import { Http,Util,Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import AccountService from '@/service/account/account-service';
import StatusTabViewtabexppanelModel from './status-tab-viewtabexppanel-tabexppanel-model';


/**
 * StatusTabViewtabexppanel 部件服务对象
 *
 * @export
 * @class StatusTabViewtabexppanelService
 */
export default class StatusTabViewtabexppanelService extends ControlService {

    /**
     * 客户服务对象
     *
     * @type {AccountService}
     * @memberof StatusTabViewtabexppanelService
     */
    public appEntityService: AccountService = new AccountService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof StatusTabViewtabexppanelService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of StatusTabViewtabexppanelService.
     * 
     * @param {*} [opts={}]
     * @memberof StatusTabViewtabexppanelService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new StatusTabViewtabexppanelModel();
    }

    
}