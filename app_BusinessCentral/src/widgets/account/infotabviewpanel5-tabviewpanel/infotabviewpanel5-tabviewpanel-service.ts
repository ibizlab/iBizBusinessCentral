import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * Infotabviewpanel5 部件服务对象
 *
 * @export
 * @class Infotabviewpanel5Service
 */
export default class Infotabviewpanel5Service extends ControlService {
}