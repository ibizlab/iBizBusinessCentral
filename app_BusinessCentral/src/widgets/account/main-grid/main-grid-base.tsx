import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, GridControlBase } from '@/studio-core';
import AccountService from '@/service/account/account-service';
import MainService from './main-grid-service';
import AccountUIService from '@/uiservice/account/account-ui-service';
import { FormItemModel } from '@/model/form-detail';


/**
 * grid部件基类
 *
 * @export
 * @class GridControlBase
 * @extends {MainGridBase}
 */
export class MainGridBase extends GridControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected controlType: string = 'GRID';

    /**
     * 建构部件服务对象
     *
     * @type {MainService}
     * @memberof MainGridBase
     */
    public service: MainService = new MainService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {AccountService}
     * @memberof MainGridBase
     */
    public appEntityService: AccountService = new AccountService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected appDeName: string = 'account';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected appDeLogicName: string = '客户';

    /**
     * 界面UI服务对象
     *
     * @type {AccountUIService}
     * @memberof MainBase
     */  
    public appUIService:AccountUIService = new AccountUIService(this.$store);

    /**
     * 界面行为模型
     *
     * @type {*}
     * @memberof MainBase
     */  
    public ActionModel: any = {
    };

    /**
     * 本地缓存标识
     *
     * @protected
     * @type {string}
     * @memberof MainBase
     */
    protected localStorageTag: string = 'account_main_grid';

    /**
     * 所有列成员
     *
     * @type {any[]}
     * @memberof MainGridBase
     */
    public allColumns: any[] = [
        {
            name: 'accountname',
            label: '客户名称',
            langtag: 'entities.account.main_grid.columns.accountname',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'telephone1',
            label: '主要电话',
            langtag: 'entities.account.main_grid.columns.telephone1',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'address1_city',
            label: '地址 1: 市/县',
            langtag: 'entities.account.main_grid.columns.address1_city',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'primarycontactname',
            label: '主要联系人',
            langtag: 'entities.account.main_grid.columns.primarycontactname',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'emailaddress1',
            label: '电子邮件',
            langtag: 'entities.account.main_grid.columns.emailaddress1',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'revenue',
            label: '年收入',
            langtag: 'entities.account.main_grid.columns.revenue',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'parentaccountname',
            label: '上级客户',
            langtag: 'entities.account.main_grid.columns.parentaccountname',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
    ]

    /**
     * 获取表格行模型
     *
     * @type {*}
     * @memberof MainGridBase
     */
    public getGridRowModel(){
        return {
          srfkey: new FormItemModel(),
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof MainGridBase
     */
    public rules: any = {
        srfkey: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '客户 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '客户 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 获取对应列class
     *
     * @type {*}
     * @memberof MainBase
     */
    public hasRowEdit: any = {
        'accountname':false,
        'telephone1':false,
        'address1_city':false,
        'primarycontactname':false,
        'emailaddress1':false,
        'revenue':false,
        'parentaccountname':false,
    };

    /**
     * 获取对应列class
     *
     * @param {*} $args row 行数据，column 列数据，rowIndex 行索引，列索引
     * @returns {void}
     * @memberof MainBase
     */
    public getCellClassName(args: {row: any, column: any, rowIndex: number, columnIndex: number}): any {
        return ( this.hasRowEdit[args.column.property] && this.actualIsOpenEdit ) ? "edit-cell" : "info-cell";
    }


    /**
     * 导出数据格式化
     *
     * @param {*} filterVal
     * @param {*} jsonData
     * @param {any[]} [codelistColumns=[]]
     * @returns {Promise<any>}
     * @memberof MainGridBase
     */
    public async formatExcelData(filterVal: any, jsonData: any, codelistColumns?: any[]): Promise<any> {
        return super.formatExcelData(filterVal, jsonData, [
        ]);
    }

}