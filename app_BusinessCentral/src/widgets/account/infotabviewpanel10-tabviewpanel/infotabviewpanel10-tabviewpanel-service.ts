import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * Infotabviewpanel10 部件服务对象
 *
 * @export
 * @class Infotabviewpanel10Service
 */
export default class Infotabviewpanel10Service extends ControlService {
}