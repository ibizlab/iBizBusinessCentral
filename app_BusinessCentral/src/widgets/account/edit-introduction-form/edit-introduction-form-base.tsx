import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, EditFormControlBase } from '@/studio-core';
import AccountService from '@/service/account/account-service';
import Edit_IntroductionService from './edit-introduction-form-service';
import AccountUIService from '@/uiservice/account/account-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';


/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {Edit_IntroductionEditFormBase}
 */
export class Edit_IntroductionEditFormBase extends EditFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof Edit_IntroductionEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {Edit_IntroductionService}
     * @memberof Edit_IntroductionEditFormBase
     */
    public service: Edit_IntroductionService = new Edit_IntroductionService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {AccountService}
     * @memberof Edit_IntroductionEditFormBase
     */
    public appEntityService: AccountService = new AccountService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Edit_IntroductionEditFormBase
     */
    protected appDeName: string = 'account';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof Edit_IntroductionEditFormBase
     */
    protected appDeLogicName: string = '客户';

    /**
     * 界面UI服务对象
     *
     * @type {AccountUIService}
     * @memberof Edit_IntroductionBase
     */  
    public appUIService:AccountUIService = new AccountUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof Edit_IntroductionEditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        industrycode: null,
        sic: null,
        ownershipcode: null,
        description: null,
        accountid: null,
        account:null,
    };

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Edit_IntroductionEditFormBase
     */
    public rules: any = {
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Edit_IntroductionBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof Edit_IntroductionEditFormBase
     */
    public detailsModel: any = {
        group1: new FormGroupPanelModel({ caption: '客户基本信息', detailType: 'GROUPPANEL', name: 'group1', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.account.edit_introduction_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({ caption: '更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 0 }),

        srforikey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfkey: new FormItemModel({ caption: '客户', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfmajortext: new FormItemModel({ caption: '客户名称', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srftempmode: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfuf: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfdeid: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfsourcekey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        industrycode: new FormItemModel({ caption: '行业', detailType: 'FORMITEM', name: 'industrycode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        sic: new FormItemModel({ caption: '行业编码', detailType: 'FORMITEM', name: 'sic', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        ownershipcode: new FormItemModel({ caption: '所有权', detailType: 'FORMITEM', name: 'ownershipcode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        description: new FormItemModel({ caption: '说明', detailType: 'FORMITEM', name: 'description', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        accountid: new FormItemModel({ caption: '客户', detailType: 'FORMITEM', name: 'accountid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

    };
}