/**
 * Edit_Introduction 部件模型
 *
 * @export
 * @class Edit_IntroductionModel
 */
export default class Edit_IntroductionModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Edit_IntroductionModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'accountid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'accountname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'industrycode',
        prop: 'industrycode',
        dataType: 'SSCODELIST',
      },
      {
        name: 'sic',
        prop: 'sic',
        dataType: 'TEXT',
      },
      {
        name: 'ownershipcode',
        prop: 'ownershipcode',
        dataType: 'SSCODELIST',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'TEXT',
      },
      {
        name: 'accountid',
        prop: 'accountid',
        dataType: 'GUID',
      },
      {
        name: 'account',
        prop: 'accountid',
        dataType: 'FONTKEY',
      },
    ]
  }

}