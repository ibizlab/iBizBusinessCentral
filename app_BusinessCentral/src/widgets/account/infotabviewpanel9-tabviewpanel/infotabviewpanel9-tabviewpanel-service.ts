import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * Infotabviewpanel9 部件服务对象
 *
 * @export
 * @class Infotabviewpanel9Service
 */
export default class Infotabviewpanel9Service extends ControlService {
}