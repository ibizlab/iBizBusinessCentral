import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * Infotabviewpanel6 部件服务对象
 *
 * @export
 * @class Infotabviewpanel6Service
 */
export default class Infotabviewpanel6Service extends ControlService {
}