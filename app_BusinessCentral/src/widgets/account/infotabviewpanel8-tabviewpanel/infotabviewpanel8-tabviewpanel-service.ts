import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * Infotabviewpanel8 部件服务对象
 *
 * @export
 * @class Infotabviewpanel8Service
 */
export default class Infotabviewpanel8Service extends ControlService {
}