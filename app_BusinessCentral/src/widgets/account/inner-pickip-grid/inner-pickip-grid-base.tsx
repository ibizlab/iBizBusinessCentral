import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, GridControlBase } from '@/studio-core';
import AccountService from '@/service/account/account-service';
import InnerPickipService from './inner-pickip-grid-service';
import AccountUIService from '@/uiservice/account/account-ui-service';
import { FormItemModel } from '@/model/form-detail';


/**
 * grid部件基类
 *
 * @export
 * @class GridControlBase
 * @extends {InnerPickipGridBase}
 */
export class InnerPickipGridBase extends GridControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof InnerPickipGridBase
     */
    protected controlType: string = 'GRID';

    /**
     * 建构部件服务对象
     *
     * @type {InnerPickipService}
     * @memberof InnerPickipGridBase
     */
    public service: InnerPickipService = new InnerPickipService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {AccountService}
     * @memberof InnerPickipGridBase
     */
    public appEntityService: AccountService = new AccountService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof InnerPickipGridBase
     */
    protected appDeName: string = 'account';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof InnerPickipGridBase
     */
    protected appDeLogicName: string = '客户';

    /**
     * 界面UI服务对象
     *
     * @type {AccountUIService}
     * @memberof InnerPickipBase
     */  
    public appUIService:AccountUIService = new AccountUIService(this.$store);

    /**
     * 界面行为模型
     *
     * @type {*}
     * @memberof InnerPickipBase
     */  
    public ActionModel: any = {
    };

    /**
     * 本地缓存标识
     *
     * @protected
     * @type {string}
     * @memberof InnerPickipBase
     */
    protected localStorageTag: string = 'account_innerpickip_grid';

    /**
     * 所有列成员
     *
     * @type {any[]}
     * @memberof InnerPickipGridBase
     */
    public allColumns: any[] = [
        {
            name: 'accountname',
            label: '客户名称',
            langtag: 'entities.account.innerpickip_grid.columns.accountname',
            show: true,
            unit: 'STAR',
            isEnableRowEdit: false,
        },
        {
            name: 'emailaddress1',
            label: '电子邮件',
            langtag: 'entities.account.innerpickip_grid.columns.emailaddress1',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
    ]

    /**
     * 获取表格行模型
     *
     * @type {*}
     * @memberof InnerPickipGridBase
     */
    public getGridRowModel(){
        return {
          srfkey: new FormItemModel(),
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof InnerPickipGridBase
     */
    public rules: any = {
        srfkey: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '客户 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '客户 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 获取对应列class
     *
     * @type {*}
     * @memberof InnerPickipBase
     */
    public hasRowEdit: any = {
        'accountname':false,
        'emailaddress1':false,
    };

    /**
     * 获取对应列class
     *
     * @param {*} $args row 行数据，column 列数据，rowIndex 行索引，列索引
     * @returns {void}
     * @memberof InnerPickipBase
     */
    public getCellClassName(args: {row: any, column: any, rowIndex: number, columnIndex: number}): any {
        return ( this.hasRowEdit[args.column.property] && this.actualIsOpenEdit ) ? "edit-cell" : "info-cell";
    }


    /**
     * 导出数据格式化
     *
     * @param {*} filterVal
     * @param {*} jsonData
     * @param {any[]} [codelistColumns=[]]
     * @returns {Promise<any>}
     * @memberof InnerPickipGridBase
     */
    public async formatExcelData(filterVal: any, jsonData: any, codelistColumns?: any[]): Promise<any> {
        return super.formatExcelData(filterVal, jsonData, [
        ]);
    }

}