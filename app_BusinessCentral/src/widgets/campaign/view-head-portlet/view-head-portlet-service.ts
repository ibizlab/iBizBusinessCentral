import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * View_Head 部件服务对象
 *
 * @export
 * @class View_HeadService
 */
export default class View_HeadService extends ControlService {
}
