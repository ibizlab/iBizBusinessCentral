/**
 * StateTabViewtabviewpanel3 部件模型
 *
 * @export
 * @class StateTabViewtabviewpanel3Model
 */
export default class StateTabViewtabviewpanel3Model {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof StateTabViewtabviewpanel3Model
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'expectedresponse',
      },
      {
        name: 'typecode',
      },
      {
        name: 'budgetedcost',
      },
      {
        name: 'createdate',
      },
      {
        name: 'ownername',
      },
      {
        name: 'totalactualcost',
      },
      {
        name: 'utcconversiontimezonecode',
      },
      {
        name: 'importsequencenumber',
      },
      {
        name: 'othercost',
      },
      {
        name: 'timezoneruleversionnumber',
      },
      {
        name: 'campaignname',
      },
      {
        name: 'entityimage_url',
      },
      {
        name: 'statecode',
      },
      {
        name: 'template',
      },
      {
        name: 'ownertype',
      },
      {
        name: 'exchangerate',
      },
      {
        name: 'campaign',
        prop: 'campaignid',
      },
      {
        name: 'createman',
      },
      {
        name: 'objective',
      },
      {
        name: 'budgetedcost_base',
      },
      {
        name: 'updateman',
      },
      {
        name: 'expectedrevenue_base',
      },
      {
        name: 'entityimage_timestamp',
      },
      {
        name: 'traversedpath',
      },
      {
        name: 'othercost_base',
      },
      {
        name: 'proposedend',
      },
      {
        name: 'versionnumber',
      },
      {
        name: 'entityimage',
      },
      {
        name: 'codename',
      },
      {
        name: 'description',
      },
      {
        name: 'actualend',
      },
      {
        name: 'expectedrevenue',
      },
      {
        name: 'pricelistname',
      },
      {
        name: 'entityimageid',
      },
      {
        name: 'processid',
      },
      {
        name: 'actualstart',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'ownerid',
      },
      {
        name: 'message',
      },
      {
        name: 'stageid',
      },
      {
        name: 'statuscode',
      },
      {
        name: 'overriddencreatedon',
      },
      {
        name: 'proposedstart',
      },
      {
        name: 'totalactualcost_base',
      },
      {
        name: 'emailaddress',
      },
      {
        name: 'promotioncodename',
      },
      {
        name: 'currencyname',
      },
      {
        name: 'pricelistid',
      },
      {
        name: 'transactioncurrencyid',
      },
    ]
  }


}