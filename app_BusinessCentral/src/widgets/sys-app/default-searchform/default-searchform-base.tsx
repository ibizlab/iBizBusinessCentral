import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, SearchFormControlBase } from '@/studio-core';
import SysAppService from '@/service/sys-app/sys-app-service';
import DefaultService from './default-searchform-service';
import SysAppUIService from '@/uiservice/sys-app/sys-app-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';


/**
 * searchform部件基类
 *
 * @export
 * @class SearchFormControlBase
 * @extends {DefaultSearchFormBase}
 */
export class DefaultSearchFormBase extends SearchFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected controlType: string = 'SEARCHFORM';

    /**
     * 建构部件服务对象
     *
     * @type {DefaultService}
     * @memberof DefaultSearchFormBase
     */
    public service: DefaultService = new DefaultService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {SysAppService}
     * @memberof DefaultSearchFormBase
     */
    public appEntityService: SysAppService = new SysAppService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected appDeName: string = 'sysapp';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected appDeLogicName: string = '应用';

    /**
     * 界面UI服务对象
     *
     * @type {SysAppUIService}
     * @memberof DefaultBase
     */  
    public appUIService:SysAppUIService = new SysAppUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof DefaultSearchFormBase
     */
    public data: any = {
        n_pssystemid_eq: null,
        n_appname_like: null,
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof DefaultSearchFormBase
     */
    public detailsModel: any = {
        formpage1: new FormPageModel({ caption: '常规条件', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this })
, 
        n_pssystemid_eq: new FormItemModel({ caption: '系统标识(等于(=))', detailType: 'FORMITEM', name: 'n_pssystemid_eq', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_appname_like: new FormItemModel({ caption: '应用名(文本包含(%))', detailType: 'FORMITEM', name: 'n_appname_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
    };
}