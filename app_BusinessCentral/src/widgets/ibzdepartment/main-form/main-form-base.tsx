import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, EditFormControlBase } from '@/studio-core';
import IBZDepartmentService from '@/service/ibzdepartment/ibzdepartment-service';
import MainService from './main-form-service';
import IBZDepartmentUIService from '@/uiservice/ibzdepartment/ibzdepartment-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';


/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {MainEditFormBase}
 */
export class MainEditFormBase extends EditFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof MainEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {MainService}
     * @memberof MainEditFormBase
     */
    public service: MainService = new MainService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {IBZDepartmentService}
     * @memberof MainEditFormBase
     */
    public appEntityService: IBZDepartmentService = new IBZDepartmentService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MainEditFormBase
     */
    protected appDeName: string = 'ibzdepartment';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof MainEditFormBase
     */
    protected appDeLogicName: string = '部门';

    /**
     * 界面UI服务对象
     *
     * @type {IBZDepartmentUIService}
     * @memberof MainBase
     */  
    public appUIService:IBZDepartmentUIService = new IBZDepartmentUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof MainEditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        deptid: null,
        deptcode: null,
        deptname: null,
        orgname: null,
        pdeptname: null,
        orgid: null,
        pdeptid: null,
        shortname: null,
        bcode: null,
        deptlevel: null,
        leaderid: null,
        leadername: null,
        showorder: null,
        createdate: null,
        updatedate: null,
        ibzdepartment:null,
    };

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof MainEditFormBase
     */
    public rules: any = {
        deptcode: [
            { required: true, type: 'string', message: '部门代码 值不能为空', trigger: 'change' },
            { required: true, type: 'string', message: '部门代码 值不能为空', trigger: 'blur' },
        ],
        deptname: [
            { required: true, type: 'string', message: '部门名称 值不能为空', trigger: 'change' },
            { required: true, type: 'string', message: '部门名称 值不能为空', trigger: 'blur' },
        ],
        orgname: [
            { required: true, type: 'string', message: '单位 值不能为空', trigger: 'change' },
            { required: true, type: 'string', message: '单位 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof MainBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof MainEditFormBase
     */
    public detailsModel: any = {
        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({ caption: '最后修改时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srforikey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfkey: new FormItemModel({ caption: '部门标识', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfmajortext: new FormItemModel({ caption: '部门名称', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srftempmode: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfuf: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfdeid: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfsourcekey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        deptid: new FormItemModel({ caption: '部门标识', detailType: 'FORMITEM', name: 'deptid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        deptcode: new FormItemModel({ caption: '部门代码', detailType: 'FORMITEM', name: 'deptcode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 1 }),

        deptname: new FormItemModel({ caption: '部门名称', detailType: 'FORMITEM', name: 'deptname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        orgname: new FormItemModel({ caption: '单位', detailType: 'FORMITEM', name: 'orgname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 1 }),

        pdeptname: new FormItemModel({ caption: '上级部门', detailType: 'FORMITEM', name: 'pdeptname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        orgid: new FormItemModel({ caption: '单位', detailType: 'FORMITEM', name: 'orgid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        pdeptid: new FormItemModel({ caption: '上级部门', detailType: 'FORMITEM', name: 'pdeptid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        shortname: new FormItemModel({ caption: '部门简称', detailType: 'FORMITEM', name: 'shortname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        bcode: new FormItemModel({ caption: '业务编码', detailType: 'FORMITEM', name: 'bcode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        deptlevel: new FormItemModel({ caption: '部门级别', detailType: 'FORMITEM', name: 'deptlevel', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        leaderid: new FormItemModel({ caption: '分管领导标识', detailType: 'FORMITEM', name: 'leaderid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        leadername: new FormItemModel({ caption: '分管领导', detailType: 'FORMITEM', name: 'leadername', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        showorder: new FormItemModel({ caption: '排序', detailType: 'FORMITEM', name: 'showorder', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        createdate: new FormItemModel({ caption: '创建时间', detailType: 'FORMITEM', name: 'createdate', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        updatedate: new FormItemModel({ caption: '最后修改时间', detailType: 'FORMITEM', name: 'updatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

    };

    /**
     * 重置表单项值
     *
     * @param {{ name: string, newVal: any, oldVal: any }} { name, newVal, oldVal }
     * @memberof MainEditFormBase
     */
    public resetFormData({ name, newVal, oldVal }: { name: string, newVal: any, oldVal: any }): void {
        if (Object.is(name, 'orgid')) {
            this.onFormItemValueChange({ name: 'pdeptname', value: null });
            this.onFormItemValueChange({ name: 'pdeptid', value: null });
        }
        if (Object.is(name, 'orgid')) {
            this.onFormItemValueChange({ name: 'leadername', value: null });
            this.onFormItemValueChange({ name: 'leaderid', value: null });
        }
    }

    /**
     * 新建默认值
     * @memberof MainEditFormBase
     */
    public createDefault(){                    
        if (this.data.hasOwnProperty('showorder')) {
            this.data['showorder'] = 1;
        }
    }
}