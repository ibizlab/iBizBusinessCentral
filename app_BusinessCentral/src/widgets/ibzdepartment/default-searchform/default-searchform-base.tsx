import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, SearchFormControlBase } from '@/studio-core';
import IBZDepartmentService from '@/service/ibzdepartment/ibzdepartment-service';
import DefaultService from './default-searchform-service';
import IBZDepartmentUIService from '@/uiservice/ibzdepartment/ibzdepartment-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';


/**
 * searchform部件基类
 *
 * @export
 * @class SearchFormControlBase
 * @extends {DefaultSearchFormBase}
 */
export class DefaultSearchFormBase extends SearchFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected controlType: string = 'SEARCHFORM';

    /**
     * 建构部件服务对象
     *
     * @type {DefaultService}
     * @memberof DefaultSearchFormBase
     */
    public service: DefaultService = new DefaultService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {IBZDepartmentService}
     * @memberof DefaultSearchFormBase
     */
    public appEntityService: IBZDepartmentService = new IBZDepartmentService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected appDeName: string = 'ibzdepartment';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected appDeLogicName: string = '部门';

    /**
     * 界面UI服务对象
     *
     * @type {IBZDepartmentUIService}
     * @memberof DefaultBase
     */  
    public appUIService:IBZDepartmentUIService = new IBZDepartmentUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof DefaultSearchFormBase
     */
    public data: any = {
        n_deptcode_like: null,
        n_deptname_like: null,
        n_orgid_eq: null,
        n_pdeptid_eq: null,
        n_bcode_like: null,
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof DefaultSearchFormBase
     */
    public detailsModel: any = {
        formpage1: new FormPageModel({ caption: '常规条件', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this })
, 
        n_deptcode_like: new FormItemModel({ caption: '部门代码(%)', detailType: 'FORMITEM', name: 'n_deptcode_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_deptname_like: new FormItemModel({ caption: '部门名称(%)', detailType: 'FORMITEM', name: 'n_deptname_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_orgid_eq: new FormItemModel({ caption: '单位(=)', detailType: 'FORMITEM', name: 'n_orgid_eq', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_pdeptid_eq: new FormItemModel({ caption: '上级部门(=)', detailType: 'FORMITEM', name: 'n_pdeptid_eq', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_bcode_like: new FormItemModel({ caption: '业务编码(%)', detailType: 'FORMITEM', name: 'n_bcode_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
    };
}