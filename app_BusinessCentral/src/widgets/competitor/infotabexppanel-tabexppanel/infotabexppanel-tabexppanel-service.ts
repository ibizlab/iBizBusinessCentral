import { Http,Util,Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import CompetitorService from '@/service/competitor/competitor-service';
import InfotabexppanelModel from './infotabexppanel-tabexppanel-model';


/**
 * Infotabexppanel 部件服务对象
 *
 * @export
 * @class InfotabexppanelService
 */
export default class InfotabexppanelService extends ControlService {

    /**
     * 竞争对手服务对象
     *
     * @type {CompetitorService}
     * @memberof InfotabexppanelService
     */
    public appEntityService: CompetitorService = new CompetitorService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof InfotabexppanelService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of InfotabexppanelService.
     * 
     * @param {*} [opts={}]
     * @memberof InfotabexppanelService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new InfotabexppanelModel();
    }

    
}