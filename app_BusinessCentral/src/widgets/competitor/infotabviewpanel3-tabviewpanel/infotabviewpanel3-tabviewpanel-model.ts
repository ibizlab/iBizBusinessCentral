/**
 * Infotabviewpanel3 部件模型
 *
 * @export
 * @class Infotabviewpanel3Model
 */
export default class Infotabviewpanel3Model {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof Infotabviewpanel3Model
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'entityimage',
      },
      {
        name: 'createdate',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'address2_shippingmethodcode',
      },
      {
        name: 'address2_country',
      },
      {
        name: 'address2_telephone2',
      },
      {
        name: 'winpercentage',
      },
      {
        name: 'address1_shippingmethodcode',
      },
      {
        name: 'referenceinfourl',
      },
      {
        name: 'address2_latitude',
      },
      {
        name: 'versionnumber',
      },
      {
        name: 'address2_county',
      },
      {
        name: 'importsequencenumber',
      },
      {
        name: 'address2_telephone3',
      },
      {
        name: 'traversedpath',
      },
      {
        name: 'address1_city',
      },
      {
        name: 'reportedrevenue_base',
      },
      {
        name: 'competitor',
        prop: 'competitorid',
      },
      {
        name: 'address1_fax',
      },
      {
        name: 'updateman',
      },
      {
        name: 'overview',
      },
      {
        name: 'address2_stateorprovince',
      },
      {
        name: 'entityimageid',
      },
      {
        name: 'timezoneruleversionnumber',
      },
      {
        name: 'address2_upszone',
      },
      {
        name: 'address1_line3',
      },
      {
        name: 'stockexchange',
      },
      {
        name: 'stageid',
      },
      {
        name: 'reportingyear',
      },
      {
        name: 'reportingquarter',
      },
      {
        name: 'address1_stateorprovince',
      },
      {
        name: 'address1_utcoffset',
      },
      {
        name: 'address1_latitude',
      },
      {
        name: 'address1_composite',
      },
      {
        name: 'address1_county',
      },
      {
        name: 'address1_name',
      },
      {
        name: 'address1_postalcode',
      },
      {
        name: 'opportunities',
      },
      {
        name: 'name',
      },
      {
        name: 'address1_addressid',
      },
      {
        name: 'address2_line3',
      },
      {
        name: 'address2_fax',
      },
      {
        name: 'processid',
      },
      {
        name: 'address2_postofficebox',
      },
      {
        name: 'createman',
      },
      {
        name: 'exchangerate',
      },
      {
        name: 'overriddencreatedon',
      },
      {
        name: 'address1_telephone2',
      },
      {
        name: 'strengths',
      },
      {
        name: 'address2_addresstypecode',
      },
      {
        name: 'address1_postofficebox',
      },
      {
        name: 'weaknesses',
      },
      {
        name: 'address1_line2',
      },
      {
        name: 'address2_longitude',
      },
      {
        name: 'threats',
      },
      {
        name: 'address2_composite',
      },
      {
        name: 'entityimage_timestamp',
      },
      {
        name: 'address2_utcoffset',
      },
      {
        name: 'address1_country',
      },
      {
        name: 'utcconversiontimezonecode',
      },
      {
        name: 'reportedrevenue',
      },
      {
        name: 'address1_longitude',
      },
      {
        name: 'tickersymbol',
      },
      {
        name: 'address2_telephone1',
      },
      {
        name: 'keyproduct',
      },
      {
        name: 'entityimage_url',
      },
      {
        name: 'address2_city',
      },
      {
        name: 'websiteurl',
      },
      {
        name: 'address2_addressid',
      },
      {
        name: 'address1_telephone1',
      },
      {
        name: 'address2_postalcode',
      },
      {
        name: 'address1_line1',
      },
      {
        name: 'address1_telephone3',
      },
      {
        name: 'address2_line1',
      },
      {
        name: 'address1_addresstypecode',
      },
      {
        name: 'address2_name',
      },
      {
        name: 'competitorname',
      },
      {
        name: 'address1_upszone',
      },
      {
        name: 'address2_line2',
      },
      {
        name: 'currencyname',
      },
      {
        name: 'transactioncurrencyid',
      },
    ]
  }


}