import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, EditFormControlBase } from '@/studio-core';
import CompetitorService from '@/service/competitor/competitor-service';
import AbstractInfoService from './abstract-info-form-service';
import CompetitorUIService from '@/uiservice/competitor/competitor-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';


/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {AbstractInfoEditFormBase}
 */
export class AbstractInfoEditFormBase extends EditFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof AbstractInfoEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {AbstractInfoService}
     * @memberof AbstractInfoEditFormBase
     */
    public service: AbstractInfoService = new AbstractInfoService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {CompetitorService}
     * @memberof AbstractInfoEditFormBase
     */
    public appEntityService: CompetitorService = new CompetitorService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof AbstractInfoEditFormBase
     */
    protected appDeName: string = 'competitor';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof AbstractInfoEditFormBase
     */
    protected appDeLogicName: string = '竞争对手';

    /**
     * 界面UI服务对象
     *
     * @type {CompetitorUIService}
     * @memberof AbstractInfoBase
     */  
    public appUIService:CompetitorUIService = new CompetitorUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof AbstractInfoEditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        competitorname: null,
        websiteurl: null,
        transactioncurrencyname: null,
        address1_postalcode: null,
        address1_country: null,
        address1_stateorprovince: null,
        address1_city: null,
        address1_line1: null,
        strengths: null,
        weaknesses: null,
        competitorid: null,
        competitor:null,
    };

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof AbstractInfoEditFormBase
     */
    public rules: any = {
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof AbstractInfoBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof AbstractInfoEditFormBase
     */
    public detailsModel: any = {
        group1: new FormGroupPanelModel({ caption: '竞争对手基本信息', detailType: 'GROUPPANEL', name: 'group1', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.competitor.abstractinfo_form', extractMode: 'ITEM', details: [] } }),

        grouppanel1: new FormGroupPanelModel({ caption: '详细信息', detailType: 'GROUPPANEL', name: 'grouppanel1', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.competitor.abstractinfo_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({ caption: '更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 0 }),

        srforikey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfkey: new FormItemModel({ caption: '竞争对手', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfmajortext: new FormItemModel({ caption: '竞争对手名称', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srftempmode: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfuf: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfdeid: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfsourcekey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        competitorname: new FormItemModel({ caption: '竞争对手名称', detailType: 'FORMITEM', name: 'competitorname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        websiteurl: new FormItemModel({ caption: '网站', detailType: 'FORMITEM', name: 'websiteurl', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        transactioncurrencyname: new FormItemModel({ caption: '货币', detailType: 'FORMITEM', name: 'transactioncurrencyname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        address1_postalcode: new FormItemModel({ caption: '邮政编码', detailType: 'FORMITEM', name: 'address1_postalcode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        address1_country: new FormItemModel({ caption: '国家/地区', detailType: 'FORMITEM', name: 'address1_country', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        address1_stateorprovince: new FormItemModel({ caption: '省/直辖市/自治区', detailType: 'FORMITEM', name: 'address1_stateorprovince', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        address1_city: new FormItemModel({ caption: '市/县', detailType: 'FORMITEM', name: 'address1_city', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        address1_line1: new FormItemModel({ caption: '街道', detailType: 'FORMITEM', name: 'address1_line1', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        strengths: new FormItemModel({ caption: '优势', detailType: 'FORMITEM', name: 'strengths', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        weaknesses: new FormItemModel({ caption: '劣势', detailType: 'FORMITEM', name: 'weaknesses', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        competitorid: new FormItemModel({ caption: '竞争对手', detailType: 'FORMITEM', name: 'competitorid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

    };
}