/**
 * AbstractInfo 部件模型
 *
 * @export
 * @class AbstractInfoModel
 */
export default class AbstractInfoModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof AbstractInfoModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'competitorid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'competitorname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'competitorname',
        prop: 'competitorname',
        dataType: 'TEXT',
      },
      {
        name: 'websiteurl',
        prop: 'websiteurl',
        dataType: 'TEXT',
      },
      {
        name: 'transactioncurrencyname',
        prop: 'currencyname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'address1_postalcode',
        prop: 'address1_postalcode',
        dataType: 'TEXT',
      },
      {
        name: 'address1_country',
        prop: 'address1_country',
        dataType: 'TEXT',
      },
      {
        name: 'address1_stateorprovince',
        prop: 'address1_stateorprovince',
        dataType: 'TEXT',
      },
      {
        name: 'address1_city',
        prop: 'address1_city',
        dataType: 'TEXT',
      },
      {
        name: 'address1_line1',
        prop: 'address1_line1',
        dataType: 'TEXT',
      },
      {
        name: 'strengths',
        prop: 'strengths',
        dataType: 'TEXT',
      },
      {
        name: 'weaknesses',
        prop: 'weaknesses',
        dataType: 'TEXT',
      },
      {
        name: 'competitorid',
        prop: 'competitorid',
        dataType: 'GUID',
      },
      {
        name: 'competitor',
        prop: 'competitorid',
        dataType: 'FONTKEY',
      },
    ]
  }

}