/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'overriddencreatedon',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'versionnumber',
      },
      {
        name: 'description',
      },
      {
        name: 'importsequencenumber',
      },
      {
        name: 'updateman',
      },
      {
        name: 'createdate',
      },
      {
        name: 'parentsubjectname',
      },
      {
        name: 'featuremask',
      },
      {
        name: 'createman',
      },
      {
        name: 'title',
      },
      {
        name: 'subject',
        prop: 'subjectid',
      },
      {
        name: 'parentsubject',
      },
    ]
  }


}