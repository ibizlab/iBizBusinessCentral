/**
 * StateTabViewtabviewpanel2 部件模型
 *
 * @export
 * @class StateTabViewtabviewpanel2Model
 */
export default class StateTabViewtabviewpanel2Model {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof StateTabViewtabviewpanel2Model
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'lastonholdtime',
      },
      {
        name: 'versionnumber',
      },
      {
        name: 'firstresponseslastatus',
      },
      {
        name: 'incidentstagecode',
      },
      {
        name: 'influencescore',
      },
      {
        name: 'socialprofileid',
      },
      {
        name: 'ticketnumber',
      },
      {
        name: 'merged',
      },
      {
        name: 'productserialnumber',
      },
      {
        name: 'accountname',
      },
      {
        name: 'description',
      },
      {
        name: 'escalated',
      },
      {
        name: 'stageid',
      },
      {
        name: 'casetypecode',
      },
      {
        name: 'emailaddress',
      },
      {
        name: 'checkemail',
      },
      {
        name: 'severitycode',
      },
      {
        name: 'escalatedon',
      },
      {
        name: 'utcconversiontimezonecode',
      },
      {
        name: 'ownerid',
      },
      {
        name: 'billedserviceunits',
      },
      {
        name: 'prioritycode',
      },
      {
        name: 'entityimage',
      },
      {
        name: 'onholdtime',
      },
      {
        name: 'createman',
      },
      {
        name: 'kbarticleid',
      },
      {
        name: 'contractservicelevelcode',
      },
      {
        name: 'ownertype',
      },
      {
        name: 'ownername',
      },
      {
        name: 'entityimage_url',
      },
      {
        name: 'customercontacted',
      },
      {
        name: 'processid',
      },
      {
        name: 'timezoneruleversionnumber',
      },
      {
        name: 'createdate',
      },
      {
        name: 'decrementing',
      },
      {
        name: 'updateman',
      },
      {
        name: 'statuscode',
      },
      {
        name: 'resolveby',
      },
      {
        name: 'responseby',
      },
      {
        name: 'entityimageid',
      },
      {
        name: 'followuptaskcreated',
      },
      {
        name: 'firstresponsesent',
      },
      {
        name: 'followupby',
      },
      {
        name: 'incident',
        prop: 'incidentid',
      },
      {
        name: 'servicestage',
      },
      {
        name: 'traversedpath',
      },
      {
        name: 'numberofchildincidents',
      },
      {
        name: 'blockedprofile',
      },
      {
        name: 'overriddencreatedon',
      },
      {
        name: 'actualserviceunits',
      },
      {
        name: 'messagetypecode',
      },
      {
        name: 'contactname',
      },
      {
        name: 'routecase',
      },
      {
        name: 'statecode',
      },
      {
        name: 'importsequencenumber',
      },
      {
        name: 'resolvebyslastatus',
      },
      {
        name: 'caseorigincode',
      },
      {
        name: 'activitiescomplete',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'decremententitlementterm',
      },
      {
        name: 'customersatisfactioncode',
      },
      {
        name: 'sentimentvalue',
      },
      {
        name: 'entityimage_timestamp',
      },
      {
        name: 'title',
      },
      {
        name: 'exchangerate',
      },
      {
        name: 'existingcasename',
      },
      {
        name: 'subjectname',
      },
      {
        name: 'contractdetailname',
      },
      {
        name: 'parentcasename',
      },
      {
        name: 'entitlementname',
      },
      {
        name: 'mastername',
      },
      {
        name: 'contractname',
      },
      {
        name: 'customername',
      },
      {
        name: 'customertype',
      },
      {
        name: 'currencyname',
      },
      {
        name: 'productname',
      },
      {
        name: 'slaname',
      },
      {
        name: 'firstresponsebykpiname',
      },
      {
        name: 'resolvebykpiname',
      },
      {
        name: 'primarycontactname',
      },
      {
        name: 'responsiblecontactname',
      },
      {
        name: 'entitlementid',
      },
      {
        name: 'customerid',
      },
      {
        name: 'firstresponsebykpiid',
      },
      {
        name: 'slaid',
      },
      {
        name: 'existingcase',
      },
      {
        name: 'productid',
      },
      {
        name: 'contractid',
      },
      {
        name: 'responsiblecontactid',
      },
      {
        name: 'resolvebykpiid',
      },
      {
        name: 'contractdetailid',
      },
      {
        name: 'subjectid',
      },
      {
        name: 'masterid',
      },
      {
        name: 'parentcaseid',
      },
      {
        name: 'primarycontactid',
      },
      {
        name: 'transactioncurrencyid',
      },
    ]
  }


}