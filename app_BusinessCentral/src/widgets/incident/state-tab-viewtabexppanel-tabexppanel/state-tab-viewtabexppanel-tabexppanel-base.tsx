import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, TabExpPanelControlBase } from '@/studio-core';
import IncidentService from '@/service/incident/incident-service';
import StateTabViewtabexppanelService from './state-tab-viewtabexppanel-tabexppanel-service';
import IncidentUIService from '@/uiservice/incident/incident-ui-service';


/**
 * tabexppanel部件基类
 *
 * @export
 * @class TabExpPanelControlBase
 * @extends {StateTabViewtabexppanelTabexppanelBase}
 */
export class StateTabViewtabexppanelTabexppanelBase extends TabExpPanelControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof StateTabViewtabexppanelTabexppanelBase
     */
    protected controlType: string = 'TABEXPPANEL';

    /**
     * 建构部件服务对象
     *
     * @type {StateTabViewtabexppanelService}
     * @memberof StateTabViewtabexppanelTabexppanelBase
     */
    public service: StateTabViewtabexppanelService = new StateTabViewtabexppanelService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {IncidentService}
     * @memberof StateTabViewtabexppanelTabexppanelBase
     */
    public appEntityService: IncidentService = new IncidentService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof StateTabViewtabexppanelTabexppanelBase
     */
    protected appDeName: string = 'incident';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof StateTabViewtabexppanelTabexppanelBase
     */
    protected appDeLogicName: string = '案例';

    /**
     * 界面UI服务对象
     *
     * @type {IncidentUIService}
     * @memberof StateTabViewtabexppanelBase
     */  
    public appUIService:IncidentUIService = new IncidentUIService(this.$store);
    /**
     * 是否初始化
     *
     * @protected
     * @returns {any}
     * @memberof StateTabViewtabexppanel
     */
    protected isInit: any = {
        tabviewpanel:  true ,
        tabviewpanel2:  false ,
        tabviewpanel3:  false ,
        tabviewpanel4:  false ,
    }

    /**
     * 被激活的分页面板
     *
     * @protected
     * @type {string}
     * @memberof StateTabViewtabexppanel
     */
    protected activatedTabViewPanel: string = 'tabviewpanel';

    /**
     * 组件创建完毕
     *
     * @protected
     * @memberof StateTabViewtabexppanel
     */
    protected ctrlCreated(): void {
        //设置分页导航srfparentdename和srfparentkey
        if (this.context.incident) {
            Object.assign(this.context, { srfparentdename: 'Incident', srfparentkey: this.context.incident });
        }
        super.ctrlCreated();
    }
}