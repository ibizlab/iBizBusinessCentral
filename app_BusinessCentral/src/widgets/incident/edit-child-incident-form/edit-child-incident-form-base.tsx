import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, EditFormControlBase } from '@/studio-core';
import IncidentService from '@/service/incident/incident-service';
import Edit_ChildIncidentService from './edit-child-incident-form-service';
import IncidentUIService from '@/uiservice/incident/incident-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';


/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {Edit_ChildIncidentEditFormBase}
 */
export class Edit_ChildIncidentEditFormBase extends EditFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof Edit_ChildIncidentEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {Edit_ChildIncidentService}
     * @memberof Edit_ChildIncidentEditFormBase
     */
    public service: Edit_ChildIncidentService = new Edit_ChildIncidentService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {IncidentService}
     * @memberof Edit_ChildIncidentEditFormBase
     */
    public appEntityService: IncidentService = new IncidentService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Edit_ChildIncidentEditFormBase
     */
    protected appDeName: string = 'incident';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof Edit_ChildIncidentEditFormBase
     */
    protected appDeLogicName: string = '案例';

    /**
     * 界面UI服务对象
     *
     * @type {IncidentUIService}
     * @memberof Edit_ChildIncidentBase
     */  
    public appUIService:IncidentUIService = new IncidentUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof Edit_ChildIncidentEditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        title: null,
        subjectname: null,
        casetypecode: null,
        primarycontactname: null,
        caseorigincode: null,
        productname: null,
        entitlementname: null,
        productid: null,
        incidentid: null,
        primarycontactid: null,
        subjectid: null,
        entitlementid: null,
        incident:null,
    };

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Edit_ChildIncidentEditFormBase
     */
    public rules: any = {
        title: [
            { required: true, type: 'string', message: '案例标题 值不能为空', trigger: 'change' },
            { required: true, type: 'string', message: '案例标题 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Edit_ChildIncidentBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof Edit_ChildIncidentEditFormBase
     */
    public detailsModel: any = {
        group1: new FormGroupPanelModel({ caption: '案例信息', detailType: 'GROUPPANEL', name: 'group1', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.incident.edit_childincident_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({ caption: '更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 0 }),

        srforikey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfkey: new FormItemModel({ caption: '案例', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfmajortext: new FormItemModel({ caption: '案例标题', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srftempmode: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfuf: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfdeid: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfsourcekey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        title: new FormItemModel({ caption: '案例标题', detailType: 'FORMITEM', name: 'title', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        subjectname: new FormItemModel({ caption: '主题', detailType: 'FORMITEM', name: 'subjectname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        casetypecode: new FormItemModel({ caption: '案例类型', detailType: 'FORMITEM', name: 'casetypecode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        primarycontactname: new FormItemModel({ caption: '联系人', detailType: 'FORMITEM', name: 'primarycontactname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        caseorigincode: new FormItemModel({ caption: '起源', detailType: 'FORMITEM', name: 'caseorigincode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        productname: new FormItemModel({ caption: '产品', detailType: 'FORMITEM', name: 'productname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        entitlementname: new FormItemModel({ caption: '权利', detailType: 'FORMITEM', name: 'entitlementname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        productid: new FormItemModel({ caption: '产品', detailType: 'FORMITEM', name: 'productid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        incidentid: new FormItemModel({ caption: '案例', detailType: 'FORMITEM', name: 'incidentid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        primarycontactid: new FormItemModel({ caption: '联系人', detailType: 'FORMITEM', name: 'primarycontactid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        subjectid: new FormItemModel({ caption: '主题', detailType: 'FORMITEM', name: 'subjectid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        entitlementid: new FormItemModel({ caption: '权利', detailType: 'FORMITEM', name: 'entitlementid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

    };
}