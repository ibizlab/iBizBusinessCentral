/**
 * Edit_ChildIncident 部件模型
 *
 * @export
 * @class Edit_ChildIncidentModel
 */
export default class Edit_ChildIncidentModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Edit_ChildIncidentModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'incidentid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'title',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'title',
        prop: 'title',
        dataType: 'TEXT',
      },
      {
        name: 'subjectname',
        prop: 'subjectname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'casetypecode',
        prop: 'casetypecode',
        dataType: 'SSCODELIST',
      },
      {
        name: 'primarycontactname',
        prop: 'primarycontactname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'caseorigincode',
        prop: 'caseorigincode',
        dataType: 'SSCODELIST',
      },
      {
        name: 'productname',
        prop: 'productname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'entitlementname',
        prop: 'entitlementname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'productid',
        prop: 'productid',
        dataType: 'PICKUP',
      },
      {
        name: 'incidentid',
        prop: 'incidentid',
        dataType: 'GUID',
      },
      {
        name: 'primarycontactid',
        prop: 'primarycontactid',
        dataType: 'PICKUP',
      },
      {
        name: 'subjectid',
        prop: 'subjectid',
        dataType: 'PICKUP',
      },
      {
        name: 'entitlementid',
        prop: 'entitlementid',
        dataType: 'PICKUP',
      },
      {
        name: 'incident',
        prop: 'incidentid',
        dataType: 'FONTKEY',
      },
    ]
  }

}