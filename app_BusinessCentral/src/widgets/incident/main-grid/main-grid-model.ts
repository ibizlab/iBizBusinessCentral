/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'prioritycode',
          prop: 'prioritycode',
          dataType: 'SSCODELIST',
        },
        {
          name: 'statecode',
          prop: 'statecode',
          dataType: 'NSCODELIST',
        },
        {
          name: 'resolvebykpiid',
          prop: 'resolvebykpiid',
          dataType: 'PICKUP',
        },
        {
          name: 'caseorigincode',
          prop: 'caseorigincode',
          dataType: 'SSCODELIST',
        },
        {
          name: 'contractid',
          prop: 'contractid',
          dataType: 'PICKUP',
        },
        {
          name: 'primarycontactid',
          prop: 'primarycontactid',
          dataType: 'PICKUP',
        },
        {
          name: 'transactioncurrencyid',
          prop: 'transactioncurrencyid',
          dataType: 'PICKUP',
        },
        {
          name: 'slaid',
          prop: 'slaid',
          dataType: 'PICKUP',
        },
        {
          name: 'parentcaseid',
          prop: 'parentcaseid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'title',
          dataType: 'TEXT',
        },
        {
          name: 'contractdetailid',
          prop: 'contractdetailid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfkey',
          prop: 'incidentid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'srfdataaccaction',
          prop: 'incidentid',
          dataType: 'GUID',
        },
        {
          name: 'ticketnumber',
          prop: 'ticketnumber',
          dataType: 'TEXT',
        },
        {
          name: 'masterid',
          prop: 'masterid',
          dataType: 'PICKUP',
        },
        {
          name: 'title',
          prop: 'title',
          dataType: 'TEXT',
        },
        {
          name: 'customerid',
          prop: 'customerid',
          dataType: 'PICKUP',
        },
        {
          name: 'productid',
          prop: 'productid',
          dataType: 'PICKUP',
        },
        {
          name: 'firstresponsebykpiid',
          prop: 'firstresponsebykpiid',
          dataType: 'PICKUP',
        },
        {
          name: 'existingcase',
          prop: 'existingcase',
          dataType: 'PICKUP',
        },
        {
          name: 'subjectid',
          prop: 'subjectid',
          dataType: 'PICKUP',
        },
        {
          name: 'responsiblecontactid',
          prop: 'responsiblecontactid',
          dataType: 'PICKUP',
        },
        {
          name: 'entitlementid',
          prop: 'entitlementid',
          dataType: 'PICKUP',
        },
        {
          name: 'incident',
          prop: 'incidentid',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}