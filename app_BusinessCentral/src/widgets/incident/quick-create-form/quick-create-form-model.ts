/**
 * QuickCreate 部件模型
 *
 * @export
 * @class QuickCreateModel
 */
export default class QuickCreateModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof QuickCreateModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'incidentid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'title',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'title',
        prop: 'title',
        dataType: 'TEXT',
      },
      {
        name: 'subjectname',
        prop: 'subjectname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'caseorigincode',
        prop: 'caseorigincode',
        dataType: 'SSCODELIST',
      },
      {
        name: 'productname',
        prop: 'productname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'entitlementname',
        prop: 'entitlementname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'TEXT',
      },
      {
        name: 'customerid',
        prop: 'customerid',
        dataType: 'PICKUP',
      },
      {
        name: 'customername',
        prop: 'customername',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'customertype',
        prop: 'customertype',
        dataType: 'PICKUPDATA',
      },
      {
        name: 'productid',
        prop: 'productid',
        dataType: 'PICKUP',
      },
      {
        name: 'incidentid',
        prop: 'incidentid',
        dataType: 'GUID',
      },
      {
        name: 'subjectid',
        prop: 'subjectid',
        dataType: 'PICKUP',
      },
      {
        name: 'entitlementid',
        prop: 'entitlementid',
        dataType: 'PICKUP',
      },
      {
        name: 'incident',
        prop: 'incidentid',
        dataType: 'FONTKEY',
      },
    ]
  }

}