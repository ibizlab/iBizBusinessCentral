import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * SalLitInfo 部件服务对象
 *
 * @export
 * @class SalLitInfoService
 */
export default class SalLitInfoService extends ControlService {
}