import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, MainControlBase } from '@/studio-core';
import SalesLiteratureService from '@/service/sales-literature/sales-literature-service';
import View_SalLitInfoService from './view-sal-lit-info-portlet-service';
import SalesLiteratureUIService from '@/uiservice/sales-literature/sales-literature-ui-service';
import { Environment } from '@/environments/environment';


/**
 * dashboard_sysportlet1部件基类
 *
 * @export
 * @class MainControlBase
 * @extends {View_SalLitInfoPortletBase}
 */
export class View_SalLitInfoPortletBase extends MainControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof View_SalLitInfoPortletBase
     */
    protected controlType: string = 'PORTLET';

    /**
     * 建构部件服务对象
     *
     * @type {View_SalLitInfoService}
     * @memberof View_SalLitInfoPortletBase
     */
    public service: View_SalLitInfoService = new View_SalLitInfoService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {SalesLiteratureService}
     * @memberof View_SalLitInfoPortletBase
     */
    public appEntityService: SalesLiteratureService = new SalesLiteratureService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof View_SalLitInfoPortletBase
     */
    protected appDeName: string = 'salesliterature';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof View_SalLitInfoPortletBase
     */
    protected appDeLogicName: string = '销售宣传资料';

    /**
     * 界面UI服务对象
     *
     * @type {SalesLiteratureUIService}
     * @memberof View_SalLitInfoBase
     */  
    public appUIService:SalesLiteratureUIService = new SalesLiteratureUIService(this.$store);

    /**
     * 长度
     *
     * @type {number}
     * @memberof View_SalLitInfo
     */
    @Prop() public height?: number;

    /**
     * 宽度
     *
     * @type {number}
     * @memberof View_SalLitInfo
     */
    @Prop() public width?: number;



    /**
     * 是否自适应大小
     *
     * @returns {boolean}
     * @memberof View_SalLitInfoBase
     */
    @Prop({default: false})public isAdaptiveSize!: boolean;

    /**
     * 获取多项数据
     *
     * @returns {any[]}
     * @memberof View_SalLitInfoBase
     */
    public getDatas(): any[] {
        return [];
    }

    /**
     * 获取单项树
     *
     * @returns {*}
     * @memberof View_SalLitInfoBase
     */
    public getData(): any {
        return {};
    }

    /**
     * 获取高度
     *
     * @returns {any[]}
     * @memberof View_SalLitInfoBase
     */
    get getHeight(): any{
        if(!this.$util.isEmpty(this.height) && !this.$util.isNumberNaN(this.height)){
            if(this.height == 0){
                return 'auto';
            } else {
                return this.height+'px';
            }
        } else {
            return 'auto';
        }
    }

    /**
     * vue 生命周期
     *
     * @memberof View_SalLitInfoBase
     */
    public created() {
        this.afterCreated();
    }

    /**
     * 执行created后的逻辑
     *
     *  @memberof View_SalLitInfoBase
     */    
    public afterCreated(){
        if (this.viewState) {
            this.viewStateEvent = this.viewState.subscribe(({ tag, action, data }) => {
                if (!Object.is(tag, this.name)) {
                    return;
                }
                const refs: any = this.$refs;
                Object.keys(refs).forEach((_name: string) => {
                    this.viewState.next({ tag: _name, action: action, data: data });
                });
            });
        }
    }

    /**
     * vue 生命周期
     *
     * @memberof View_SalLitInfoBase
     */
    public destroyed() {
        this.afterDestroy();
    }

    /**
     * 执行destroyed后的逻辑
     *
     * @memberof View_SalLitInfoBase
     */
    public afterDestroy() {
        if (this.viewStateEvent) {
            this.viewStateEvent.unsubscribe();
        }
    }


}
