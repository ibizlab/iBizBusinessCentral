/**
 * View_SalLitInfo 部件模型
 *
 * @export
 * @class View_SalLitInfoModel
 */
export default class View_SalLitInfoModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof View_SalLitInfoModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'importsequencenumber',
      },
      {
        name: 'createdate',
      },
      {
        name: 'createman',
      },
      {
        name: 'timezoneruleversionnumber',
      },
      {
        name: 'customerviewable',
      },
      {
        name: 'expirationdate',
      },
      {
        name: 'exchangerate',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'description',
      },
      {
        name: 'salesliteraturename',
      },
      {
        name: 'processid',
      },
      {
        name: 'stageid',
      },
      {
        name: 'updateman',
      },
      {
        name: 'entityimage',
      },
      {
        name: 'entityimage_url',
      },
      {
        name: 'hasattachments',
      },
      {
        name: 'overriddencreatedon',
      },
      {
        name: 'entityimage_timestamp',
      },
      {
        name: 'employeecontactid',
      },
      {
        name: 'keywords',
      },
      {
        name: 'versionnumber',
      },
      {
        name: 'utcconversiontimezonecode',
      },
      {
        name: 'literaturetypecode',
      },
      {
        name: 'salesliterature',
        prop: 'salesliteratureid',
      },
      {
        name: 'entityimageid',
      },
      {
        name: 'employeecontactname',
      },
      {
        name: 'traversedpath',
      },
      {
        name: 'currencyname',
      },
      {
        name: 'subjectname',
      },
      {
        name: 'transactioncurrencyid',
      },
      {
        name: 'subjectid',
      },
    ]
  }


}
