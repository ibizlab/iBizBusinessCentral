import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * View_SalLitInfo 部件服务对象
 *
 * @export
 * @class View_SalLitInfoService
 */
export default class View_SalLitInfoService extends ControlService {
}
