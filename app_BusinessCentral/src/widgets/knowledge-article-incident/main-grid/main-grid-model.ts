/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'knowledgearticlename',
          prop: 'knowledgearticlename',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'knowledgearticleid',
          prop: 'knowledgearticleid',
          dataType: 'PICKUP',
        },
        {
          name: 'incidentname',
          prop: 'incidentname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'incidentid',
          prop: 'incidentid',
          dataType: 'PICKUP',
        },
        {
          name: 'statuscode',
          prop: 'statuscode',
          dataType: 'NSCODELIST',
        },
        {
          name: 'srfmajortext',
          prop: 'statecode',
          dataType: 'NSCODELIST',
        },
        {
          name: 'srfdataaccaction',
          prop: 'knowledgearticleincidentid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'knowledgearticleincidentid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'knowledgearticleincident',
          prop: 'knowledgearticleincidentid',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}