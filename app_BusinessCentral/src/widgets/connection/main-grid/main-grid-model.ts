/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'statecode',
          prop: 'statecode',
          dataType: 'NSCODELIST',
        },
        {
          name: 'record1roleid',
          prop: 'record1roleid',
          dataType: 'PICKUP',
        },
        {
          name: 'record1rolename',
          prop: 'record1rolename',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'record2roleid',
          prop: 'record2roleid',
          dataType: 'PICKUP',
        },
        {
          name: 'record2idobjecttypecode',
          prop: 'record2idobjecttypecode',
          dataType: 'TEXT',
        },
        {
          name: 'transactioncurrencyid',
          prop: 'transactioncurrencyid',
          dataType: 'PICKUP',
        },
        {
          name: 'statuscode',
          prop: 'statuscode',
          dataType: 'NSCODELIST',
        },
        {
          name: 'srfmajortext',
          prop: 'connectionname',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'connectionid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'connectionid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'connection',
          prop: 'connectionid',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}