import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, SearchFormControlBase } from '@/studio-core';
import JobsLogService from '@/service/jobs-log/jobs-log-service';
import DefaultService from './default-searchform-service';
import JobsLogUIService from '@/uiservice/jobs-log/jobs-log-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';


/**
 * searchform部件基类
 *
 * @export
 * @class SearchFormControlBase
 * @extends {DefaultSearchFormBase}
 */
export class DefaultSearchFormBase extends SearchFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected controlType: string = 'SEARCHFORM';

    /**
     * 建构部件服务对象
     *
     * @type {DefaultService}
     * @memberof DefaultSearchFormBase
     */
    public service: DefaultService = new DefaultService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {JobsLogService}
     * @memberof DefaultSearchFormBase
     */
    public appEntityService: JobsLogService = new JobsLogService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected appDeName: string = 'jobslog';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected appDeLogicName: string = '任务调度日志';

    /**
     * 界面UI服务对象
     *
     * @type {JobsLogUIService}
     * @memberof DefaultBase
     */  
    public appUIService:JobsLogUIService = new JobsLogUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof DefaultSearchFormBase
     */
    public data: any = {
        n_job_id_eq: null,
        n_handler_like: null,
        n_trigger_code_eq: null,
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof DefaultSearchFormBase
     */
    public detailsModel: any = {
        formpage1: new FormPageModel({ caption: '常规条件', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this })
, 
        n_job_id_eq: new FormItemModel({ caption: '任务ID(等于(=))', detailType: 'FORMITEM', name: 'n_job_id_eq', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_handler_like: new FormItemModel({ caption: '执行器任务HANDLER(文本包含(%))', detailType: 'FORMITEM', name: 'n_handler_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_trigger_code_eq: new FormItemModel({ caption: '触发器调度返回码(等于(=))', detailType: 'FORMITEM', name: 'n_trigger_code_eq', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
    };
}