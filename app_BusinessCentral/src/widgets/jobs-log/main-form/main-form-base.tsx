import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, EditFormControlBase } from '@/studio-core';
import JobsLogService from '@/service/jobs-log/jobs-log-service';
import MainService from './main-form-service';
import JobsLogUIService from '@/uiservice/jobs-log/jobs-log-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';


/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {MainEditFormBase}
 */
export class MainEditFormBase extends EditFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof MainEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {MainService}
     * @memberof MainEditFormBase
     */
    public service: MainService = new MainService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {JobsLogService}
     * @memberof MainEditFormBase
     */
    public appEntityService: JobsLogService = new JobsLogService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MainEditFormBase
     */
    protected appDeName: string = 'jobslog';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof MainEditFormBase
     */
    protected appDeLogicName: string = '任务调度日志';

    /**
     * 界面UI服务对象
     *
     * @type {JobsLogUIService}
     * @memberof MainBase
     */  
    public appUIService:JobsLogUIService = new JobsLogUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof MainEditFormBase
     */
    public data: any = {
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        job_id: null,
        handler: null,
        param: null,
        fail_retry_count: null,
        trigger_code: null,
        trigger_type: null,
        trigger_msg: null,
        address: null,
        create_time: null,
        id: null,
        jobslog:null,
    };

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof MainEditFormBase
     */
    public rules: any = {
        job_id: [
            { required: true, type: 'string', message: '任务ID 值不能为空', trigger: 'change' },
            { required: true, type: 'string', message: '任务ID 值不能为空', trigger: 'blur' },
        ],
        fail_retry_count: [
            { required: true, type: 'number', message: '失败重试次数 值不能为空', trigger: 'change' },
            { required: true, type: 'number', message: '失败重试次数 值不能为空', trigger: 'blur' },
        ],
        trigger_code: [
            { required: true, type: 'number', message: '触发器调度返回码 值不能为空', trigger: 'change' },
            { required: true, type: 'number', message: '触发器调度返回码 值不能为空', trigger: 'blur' },
        ],
        trigger_type: [
            { required: true, type: 'string', message: '触发器调度类型 值不能为空', trigger: 'change' },
            { required: true, type: 'string', message: '触发器调度类型 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof MainBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof MainEditFormBase
     */
    public detailsModel: any = {
        group1: new FormGroupPanelModel({ caption: '任务调度日志基本信息', detailType: 'GROUPPANEL', name: 'group1', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.jobslog.main_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srforikey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfkey: new FormItemModel({ caption: '主键ID', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfmajortext: new FormItemModel({ caption: '执行器任务HANDLER', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srftempmode: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfuf: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfdeid: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfsourcekey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        job_id: new FormItemModel({ caption: '任务ID', detailType: 'FORMITEM', name: 'job_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        handler: new FormItemModel({ caption: '执行器任务HANDLER', detailType: 'FORMITEM', name: 'handler', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        param: new FormItemModel({ caption: '执行器任务参数', detailType: 'FORMITEM', name: 'param', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        fail_retry_count: new FormItemModel({ caption: '失败重试次数', detailType: 'FORMITEM', name: 'fail_retry_count', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        trigger_code: new FormItemModel({ caption: '触发器调度返回码', detailType: 'FORMITEM', name: 'trigger_code', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        trigger_type: new FormItemModel({ caption: '触发器调度类型', detailType: 'FORMITEM', name: 'trigger_type', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        trigger_msg: new FormItemModel({ caption: '触发器调度信息', detailType: 'FORMITEM', name: 'trigger_msg', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        address: new FormItemModel({ caption: '执行地址', detailType: 'FORMITEM', name: 'address', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        create_time: new FormItemModel({ caption: '创建时间', detailType: 'FORMITEM', name: 'create_time', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        id: new FormItemModel({ caption: '主键ID', detailType: 'FORMITEM', name: 'id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

    };
}