/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'associatedproduct',
          prop: 'associatedproduct',
          dataType: 'PICKUP',
        },
        {
          name: 'productid',
          prop: 'productid',
          dataType: 'PICKUP',
        },
        {
          name: 'associatedproductname',
          prop: 'associatedproductname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'transactioncurrencyid',
          prop: 'transactioncurrencyid',
          dataType: 'PICKUP',
        },
        {
          name: 'quantity',
          prop: 'quantity',
          dataType: 'BIGDECIMAL',
        },
        {
          name: 'uomid',
          prop: 'uomid',
          dataType: 'PICKUP',
        },
        {
          name: 'uomname',
          prop: 'uomname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'srfmajortext',
          prop: 'productname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'productassociationid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'productassociationid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'productassociation',
          prop: 'productassociationid',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}