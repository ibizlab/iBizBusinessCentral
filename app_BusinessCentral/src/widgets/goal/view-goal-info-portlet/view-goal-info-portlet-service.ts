import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * View_GoalInfo 部件服务对象
 *
 * @export
 * @class View_GoalInfoService
 */
export default class View_GoalInfoService extends ControlService {
}
