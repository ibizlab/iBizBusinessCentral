import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * GoalInfo 部件服务对象
 *
 * @export
 * @class GoalInfoService
 */
export default class GoalInfoService extends ControlService {
}