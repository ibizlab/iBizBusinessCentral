import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, TabExpPanelControlBase } from '@/studio-core';
import GoalService from '@/service/goal/goal-service';
import InfoViewtabexppanelService from './info-viewtabexppanel-tabexppanel-service';
import GoalUIService from '@/uiservice/goal/goal-ui-service';


/**
 * tabexppanel部件基类
 *
 * @export
 * @class TabExpPanelControlBase
 * @extends {InfoViewtabexppanelTabexppanelBase}
 */
export class InfoViewtabexppanelTabexppanelBase extends TabExpPanelControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof InfoViewtabexppanelTabexppanelBase
     */
    protected controlType: string = 'TABEXPPANEL';

    /**
     * 建构部件服务对象
     *
     * @type {InfoViewtabexppanelService}
     * @memberof InfoViewtabexppanelTabexppanelBase
     */
    public service: InfoViewtabexppanelService = new InfoViewtabexppanelService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {GoalService}
     * @memberof InfoViewtabexppanelTabexppanelBase
     */
    public appEntityService: GoalService = new GoalService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof InfoViewtabexppanelTabexppanelBase
     */
    protected appDeName: string = 'goal';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof InfoViewtabexppanelTabexppanelBase
     */
    protected appDeLogicName: string = '目标';

    /**
     * 界面UI服务对象
     *
     * @type {GoalUIService}
     * @memberof InfoViewtabexppanelBase
     */  
    public appUIService:GoalUIService = new GoalUIService(this.$store);
    /**
     * 是否初始化
     *
     * @protected
     * @returns {any}
     * @memberof InfoViewtabexppanel
     */
    protected isInit: any = {
        tabviewpanel:  true ,
        tabviewpanel2:  false ,
    }

    /**
     * 被激活的分页面板
     *
     * @protected
     * @type {string}
     * @memberof InfoViewtabexppanel
     */
    protected activatedTabViewPanel: string = 'tabviewpanel';

    /**
     * 组件创建完毕
     *
     * @protected
     * @memberof InfoViewtabexppanel
     */
    protected ctrlCreated(): void {
        //设置分页导航srfparentdename和srfparentkey
        if (this.context.goal) {
            Object.assign(this.context, { srfparentdename: 'Goal', srfparentkey: this.context.goal });
        }
        super.ctrlCreated();
    }
}