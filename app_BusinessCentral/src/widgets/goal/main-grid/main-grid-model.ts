/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'rollupqueryactualdecimalid',
          prop: 'rollupqueryactualdecimalid',
          dataType: 'PICKUP',
        },
        {
          name: 'parentgoalid',
          prop: 'parentgoalid',
          dataType: 'PICKUP',
        },
        {
          name: 'rollupquerycustomintegerid',
          prop: 'rollupquerycustomintegerid',
          dataType: 'PICKUP',
        },
        {
          name: 'rollupqueryinprogressmoneyid',
          prop: 'rollupqueryinprogressmoneyid',
          dataType: 'PICKUP',
        },
        {
          name: 'rollupquerycustomdecimalid',
          prop: 'rollupquerycustomdecimalid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'title',
          dataType: 'TEXT',
        },
        {
          name: 'srfkey',
          prop: 'goalid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'srfdataaccaction',
          prop: 'goalid',
          dataType: 'GUID',
        },
        {
          name: 'rollupqueryactualintegerid',
          prop: 'rollupqueryactualintegerid',
          dataType: 'PICKUP',
        },
        {
          name: 'rollupqueryactualmoneyid',
          prop: 'rollupqueryactualmoneyid',
          dataType: 'PICKUP',
        },
        {
          name: 'metricname',
          prop: 'metricname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'parentgoalname',
          prop: 'parentgoalname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'title',
          prop: 'title',
          dataType: 'TEXT',
        },
        {
          name: 'goalwitherrorid',
          prop: 'goalwitherrorid',
          dataType: 'PICKUP',
        },
        {
          name: 'rollupqueryinprogressdecimalid',
          prop: 'rollupqueryinprogressdecimalid',
          dataType: 'PICKUP',
        },
        {
          name: 'metricid',
          prop: 'metricid',
          dataType: 'PICKUP',
        },
        {
          name: 'rollupquerycustommoneyid',
          prop: 'rollupquerycustommoneyid',
          dataType: 'PICKUP',
        },
        {
          name: 'rollupqueryinprogressintegerid',
          prop: 'rollupqueryinprogressintegerid',
          dataType: 'PICKUP',
        },
        {
          name: 'goal',
          prop: 'goalid',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}