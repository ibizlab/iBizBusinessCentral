/**
 * QuickCreate 部件模型
 *
 * @export
 * @class QuickCreateModel
 */
export default class QuickCreateModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof QuickCreateModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'goalid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'title',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'title',
        prop: 'title',
        dataType: 'TEXT',
      },
      {
        name: 'parentgoalname',
        prop: 'parentgoalname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'goalownerid',
        prop: 'goalownerid',
        dataType: 'TEXT',
      },
      {
        name: 'metricname',
        prop: 'metricname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'ownerid',
        prop: 'ownerid',
        dataType: 'TEXT',
      },
      {
        name: 'parentgoalid',
        prop: 'parentgoalid',
        dataType: 'PICKUP',
      },
      {
        name: 'goalid',
        prop: 'goalid',
        dataType: 'GUID',
      },
      {
        name: 'metricid',
        prop: 'metricid',
        dataType: 'PICKUP',
      },
      {
        name: 'goal',
        prop: 'goalid',
        dataType: 'FONTKEY',
      },
    ]
  }

}