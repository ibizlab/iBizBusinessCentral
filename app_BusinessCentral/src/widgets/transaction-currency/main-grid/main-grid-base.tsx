import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, GridControlBase } from '@/studio-core';
import TransactionCurrencyService from '@/service/transaction-currency/transaction-currency-service';
import MainService from './main-grid-service';
import TransactionCurrencyUIService from '@/uiservice/transaction-currency/transaction-currency-ui-service';
import { FormItemModel } from '@/model/form-detail';


/**
 * grid部件基类
 *
 * @export
 * @class GridControlBase
 * @extends {MainGridBase}
 */
export class MainGridBase extends GridControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected controlType: string = 'GRID';

    /**
     * 建构部件服务对象
     *
     * @type {MainService}
     * @memberof MainGridBase
     */
    public service: MainService = new MainService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {TransactionCurrencyService}
     * @memberof MainGridBase
     */
    public appEntityService: TransactionCurrencyService = new TransactionCurrencyService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected appDeName: string = 'transactioncurrency';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected appDeLogicName: string = '货币';

    /**
     * 界面UI服务对象
     *
     * @type {TransactionCurrencyUIService}
     * @memberof MainBase
     */  
    public appUIService:TransactionCurrencyUIService = new TransactionCurrencyUIService(this.$store);

    /**
     * 界面行为模型
     *
     * @type {*}
     * @memberof MainBase
     */  
    public ActionModel: any = {
    };

    /**
     * 本地缓存标识
     *
     * @protected
     * @type {string}
     * @memberof MainBase
     */
    protected localStorageTag: string = 'transactioncurrency_main_grid';

    /**
     * 所有列成员
     *
     * @type {any[]}
     * @memberof MainGridBase
     */
    public allColumns: any[] = [
        {
            name: 'currencyname',
            label: '货币名称',
            langtag: 'entities.transactioncurrency.main_grid.columns.currencyname',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'currencyprecision',
            label: '货币精度',
            langtag: 'entities.transactioncurrency.main_grid.columns.currencyprecision',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'currencysymbol',
            label: '货币符号',
            langtag: 'entities.transactioncurrency.main_grid.columns.currencysymbol',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'exchangerate',
            label: '汇率',
            langtag: 'entities.transactioncurrency.main_grid.columns.exchangerate',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'isocurrencycode',
            label: '货币代码',
            langtag: 'entities.transactioncurrency.main_grid.columns.isocurrencycode',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'statecode',
            label: '状态',
            langtag: 'entities.transactioncurrency.main_grid.columns.statecode',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'statuscode',
            label: '状态描述',
            langtag: 'entities.transactioncurrency.main_grid.columns.statuscode',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'updatedate',
            label: '更新时间',
            langtag: 'entities.transactioncurrency.main_grid.columns.updatedate',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
    ]

    /**
     * 获取表格行模型
     *
     * @type {*}
     * @memberof MainGridBase
     */
    public getGridRowModel(){
        return {
          srfkey: new FormItemModel(),
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof MainGridBase
     */
    public rules: any = {
        srfkey: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '交易币种 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '交易币种 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 获取对应列class
     *
     * @type {*}
     * @memberof MainBase
     */
    public hasRowEdit: any = {
        'currencyname':false,
        'currencyprecision':false,
        'currencysymbol':false,
        'exchangerate':false,
        'isocurrencycode':false,
        'statecode':false,
        'statuscode':false,
        'updatedate':false,
    };

    /**
     * 获取对应列class
     *
     * @param {*} $args row 行数据，column 列数据，rowIndex 行索引，列索引
     * @returns {void}
     * @memberof MainBase
     */
    public getCellClassName(args: {row: any, column: any, rowIndex: number, columnIndex: number}): any {
        return ( this.hasRowEdit[args.column.property] && this.actualIsOpenEdit ) ? "edit-cell" : "info-cell";
    }


    /**
     * 导出数据格式化
     *
     * @param {*} filterVal
     * @param {*} jsonData
     * @param {any[]} [codelistColumns=[]]
     * @returns {Promise<any>}
     * @memberof MainGridBase
     */
    public async formatExcelData(filterVal: any, jsonData: any, codelistColumns?: any[]): Promise<any> {
        return super.formatExcelData(filterVal, jsonData, [
            {
                name: 'statecode',
                srfkey: 'Transactioncurrency__StateCode',
                codelistType : 'STATIC',
                renderMode: 'other',
                textSeparator: '、',
                valueSeparator: ',',
            },
            {
                name: 'statuscode',
                srfkey: 'Transactioncurrency__StatusCode',
                codelistType : 'STATIC',
                renderMode: 'other',
                textSeparator: '、',
                valueSeparator: ',',
            },
        ]);
    }

}