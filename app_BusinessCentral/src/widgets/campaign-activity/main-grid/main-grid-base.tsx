import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, GridControlBase } from '@/studio-core';
import CampaignActivityService from '@/service/campaign-activity/campaign-activity-service';
import MainService from './main-grid-service';
import CampaignActivityUIService from '@/uiservice/campaign-activity/campaign-activity-ui-service';
import { FormItemModel } from '@/model/form-detail';


/**
 * grid部件基类
 *
 * @export
 * @class GridControlBase
 * @extends {MainGridBase}
 */
export class MainGridBase extends GridControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected controlType: string = 'GRID';

    /**
     * 建构部件服务对象
     *
     * @type {MainService}
     * @memberof MainGridBase
     */
    public service: MainService = new MainService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {CampaignActivityService}
     * @memberof MainGridBase
     */
    public appEntityService: CampaignActivityService = new CampaignActivityService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected appDeName: string = 'campaignactivity';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected appDeLogicName: string = '市场活动项目';

    /**
     * 界面UI服务对象
     *
     * @type {CampaignActivityUIService}
     * @memberof MainBase
     */  
    public appUIService:CampaignActivityUIService = new CampaignActivityUIService(this.$store);

    /**
     * 界面行为模型
     *
     * @type {*}
     * @memberof MainBase
     */  
    public ActionModel: any = {
    };

    /**
     * 本地缓存标识
     *
     * @protected
     * @type {string}
     * @memberof MainBase
     */
    protected localStorageTag: string = 'campaignactivity_main_grid';

    /**
     * 所有列成员
     *
     * @type {any[]}
     * @memberof MainGridBase
     */
    public allColumns: any[] = [
        {
            name: 'subject',
            label: '主题',
            langtag: 'entities.campaignactivity.main_grid.columns.subject',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'activitytypecode',
            label: '活动类型',
            langtag: 'entities.campaignactivity.main_grid.columns.activitytypecode',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'actualcost',
            label: '实际成本',
            langtag: 'entities.campaignactivity.main_grid.columns.actualcost',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'actualstart',
            label: '实际开始时间',
            langtag: 'entities.campaignactivity.main_grid.columns.actualstart',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'actualend',
            label: '实际结束时间',
            langtag: 'entities.campaignactivity.main_grid.columns.actualend',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
    ]

    /**
     * 获取表格行模型
     *
     * @type {*}
     * @memberof MainGridBase
     */
    public getGridRowModel(){
        return {
          srfkey: new FormItemModel(),
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof MainGridBase
     */
    public rules: any = {
        srfkey: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '市场活动项目 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '市场活动项目 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 获取对应列class
     *
     * @type {*}
     * @memberof MainBase
     */
    public hasRowEdit: any = {
        'subject':false,
        'activitytypecode':false,
        'actualcost':false,
        'actualstart':false,
        'actualend':false,
    };

    /**
     * 获取对应列class
     *
     * @param {*} $args row 行数据，column 列数据，rowIndex 行索引，列索引
     * @returns {void}
     * @memberof MainBase
     */
    public getCellClassName(args: {row: any, column: any, rowIndex: number, columnIndex: number}): any {
        return ( this.hasRowEdit[args.column.property] && this.actualIsOpenEdit ) ? "edit-cell" : "info-cell";
    }


    /**
     * 导出数据格式化
     *
     * @param {*} filterVal
     * @param {*} jsonData
     * @param {any[]} [codelistColumns=[]]
     * @returns {Promise<any>}
     * @memberof MainGridBase
     */
    public async formatExcelData(filterVal: any, jsonData: any, codelistColumns?: any[]): Promise<any> {
        return super.formatExcelData(filterVal, jsonData, [
        ]);
    }

}