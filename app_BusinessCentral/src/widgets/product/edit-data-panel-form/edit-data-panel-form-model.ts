/**
 * Edit_DataPanel 部件模型
 *
 * @export
 * @class Edit_DataPanelModel
 */
export default class Edit_DataPanelModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Edit_DataPanelModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'productid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'productname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'statecode',
        prop: 'statecode',
        dataType: 'NSCODELIST',
      },
      {
        name: 'productid',
        prop: 'productid',
        dataType: 'GUID',
      },
      {
        name: 'product',
        prop: 'productid',
        dataType: 'FONTKEY',
      },
    ]
  }

}