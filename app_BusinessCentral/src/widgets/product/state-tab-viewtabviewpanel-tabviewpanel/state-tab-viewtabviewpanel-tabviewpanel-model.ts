/**
 * StateTabViewtabviewpanel 部件模型
 *
 * @export
 * @class StateTabViewtabviewpanelModel
 */
export default class StateTabViewtabviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof StateTabViewtabviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'vendorid',
      },
      {
        name: 'productstructure',
      },
      {
        name: 'versionnumber',
      },
      {
        name: 'stageid',
      },
      {
        name: 'statuscode',
      },
      {
        name: 'entityimage',
      },
      {
        name: 'overriddencreatedon',
      },
      {
        name: 'stockitem',
      },
      {
        name: 'importsequencenumber',
      },
      {
        name: 'validfromdate',
      },
      {
        name: 'suppliername',
      },
      {
        name: 'producttypecode',
      },
      {
        name: 'entityimageid',
      },
      {
        name: 'entityimage_timestamp',
      },
      {
        name: 'producturl',
      },
      {
        name: 'standardcost',
      },
      {
        name: 'quantityonhand',
      },
      {
        name: 'currentcost',
      },
      {
        name: 'exchangerate',
      },
      {
        name: 'product',
        prop: 'productid',
      },
      {
        name: 'description',
      },
      {
        name: 'vendorname',
      },
      {
        name: 'dmtimportstate',
      },
      {
        name: 'entityimage_url',
      },
      {
        name: 'stockvolume',
      },
      {
        name: 'quantitydecimal',
      },
      {
        name: 'vendorpartnumber',
      },
      {
        name: 'validtodate',
      },
      {
        name: 'reparented',
      },
      {
        name: 'createman',
      },
      {
        name: 'processid',
      },
      {
        name: 'createdate',
      },
      {
        name: 'productnumber',
      },
      {
        name: 'kit',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'currentcost_base',
      },
      {
        name: 'updateman',
      },
      {
        name: 'stockweight',
      },
      {
        name: 'size',
      },
      {
        name: 'timezoneruleversionnumber',
      },
      {
        name: 'utcconversiontimezonecode',
      },
      {
        name: 'productname',
      },
      {
        name: 'price_base',
      },
      {
        name: 'price',
      },
      {
        name: 'traversedpath',
      },
      {
        name: 'hierarchypath',
      },
      {
        name: 'standardcost_base',
      },
      {
        name: 'statecode',
      },
      {
        name: 'defaultuomschedulename',
      },
      {
        name: 'subjectname',
      },
      {
        name: 'currencyname',
      },
      {
        name: 'pricelevelname',
      },
      {
        name: 'defaultuomname',
      },
      {
        name: 'parentproductname',
      },
      {
        name: 'parentproductid',
      },
      {
        name: 'transactioncurrencyid',
      },
      {
        name: 'pricelevelid',
      },
      {
        name: 'defaultuomid',
      },
      {
        name: 'defaultuomscheduleid',
      },
      {
        name: 'subjectid',
      },
    ]
  }


}