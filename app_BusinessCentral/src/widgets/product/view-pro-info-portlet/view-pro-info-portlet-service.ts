import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * View_ProInfo 部件服务对象
 *
 * @export
 * @class View_ProInfoService
 */
export default class View_ProInfoService extends ControlService {
}
