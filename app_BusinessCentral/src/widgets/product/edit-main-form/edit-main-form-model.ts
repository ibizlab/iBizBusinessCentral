/**
 * Edit_Main 部件模型
 *
 * @export
 * @class Edit_MainModel
 */
export default class Edit_MainModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Edit_MainModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'productid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'productname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'productname',
        prop: 'productname',
        dataType: 'TEXT',
      },
      {
        name: 'productnumber',
        prop: 'productnumber',
        dataType: 'TEXT',
      },
      {
        name: 'validfromdate',
        prop: 'validfromdate',
        dataType: 'DATETIME',
      },
      {
        name: 'validtodate',
        prop: 'validtodate',
        dataType: 'DATETIME',
      },
      {
        name: 'defaultuomschedulename',
        prop: 'defaultuomschedulename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'defaultuomname',
        prop: 'defaultuomname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'pricelevelname',
        prop: 'pricelevelname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'quantitydecimal',
        prop: 'quantitydecimal',
        dataType: 'INT',
      },
      {
        name: 'subjectname',
        prop: 'subjectname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'LONGTEXT_1000',
      },
      {
        name: 'defaultuomid',
        prop: 'defaultuomid',
        dataType: 'PICKUP',
      },
      {
        name: 'productid',
        prop: 'productid',
        dataType: 'GUID',
      },
      {
        name: 'defaultuomscheduleid',
        prop: 'defaultuomscheduleid',
        dataType: 'PICKUP',
      },
      {
        name: 'subjectid',
        prop: 'subjectid',
        dataType: 'PICKUP',
      },
      {
        name: 'pricelevelid',
        prop: 'pricelevelid',
        dataType: 'PICKUP',
      },
      {
        name: 'product',
        prop: 'productid',
        dataType: 'FONTKEY',
      },
    ]
  }

}