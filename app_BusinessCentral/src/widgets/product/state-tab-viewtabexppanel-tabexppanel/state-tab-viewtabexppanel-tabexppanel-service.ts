import { Http,Util,Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import ProductService from '@/service/product/product-service';
import StateTabViewtabexppanelModel from './state-tab-viewtabexppanel-tabexppanel-model';


/**
 * StateTabViewtabexppanel 部件服务对象
 *
 * @export
 * @class StateTabViewtabexppanelService
 */
export default class StateTabViewtabexppanelService extends ControlService {

    /**
     * 产品服务对象
     *
     * @type {ProductService}
     * @memberof StateTabViewtabexppanelService
     */
    public appEntityService: ProductService = new ProductService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof StateTabViewtabexppanelService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of StateTabViewtabexppanelService.
     * 
     * @param {*} [opts={}]
     * @memberof StateTabViewtabexppanelService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new StateTabViewtabexppanelModel();
    }

    
}