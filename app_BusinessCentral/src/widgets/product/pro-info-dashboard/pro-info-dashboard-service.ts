import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * ProInfo 部件服务对象
 *
 * @export
 * @class ProInfoService
 */
export default class ProInfoService extends ControlService {
}