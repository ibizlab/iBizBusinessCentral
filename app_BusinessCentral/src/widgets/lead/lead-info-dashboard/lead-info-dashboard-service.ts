import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * LeadInfo 部件服务对象
 *
 * @export
 * @class LeadInfoService
 */
export default class LeadInfoService extends ControlService {
}