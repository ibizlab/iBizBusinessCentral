/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'parentcontactid',
          prop: 'parentcontactid',
          dataType: 'PICKUP',
        },
        {
          name: 'originatingcaseid',
          prop: 'originatingcaseid',
          dataType: 'PICKUP',
        },
        {
          name: 'subject',
          prop: 'subject',
          dataType: 'TEXT',
        },
        {
          name: 'transactioncurrencyid',
          prop: 'transactioncurrencyid',
          dataType: 'PICKUP',
        },
        {
          name: 'slaid',
          prop: 'slaid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'fullname',
          dataType: 'TEXT',
        },
        {
          name: 'srfkey',
          prop: 'leadid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'srfdataaccaction',
          prop: 'leadid',
          dataType: 'GUID',
        },
        {
          name: 'relatedobjectid',
          prop: 'relatedobjectid',
          dataType: 'PICKUP',
        },
        {
          name: 'parentaccountid',
          prop: 'parentaccountid',
          dataType: 'PICKUP',
        },
        {
          name: 'campaignid',
          prop: 'campaignid',
          dataType: 'PICKUP',
        },
        {
          name: 'statuscode',
          prop: 'statuscode',
          dataType: 'NSCODELIST',
        },
        {
          name: 'fullname',
          prop: 'fullname',
          dataType: 'TEXT',
        },
        {
          name: 'qualifyingopportunityid',
          prop: 'qualifyingopportunityid',
          dataType: 'PICKUP',
        },
        {
          name: 'lead',
          prop: 'leadid',
        },
      {
        name: 'n_fullname_like',
        prop: 'n_fullname_like',
        dataType: 'TEXT',
      },
      {
        name: 'n_leadsourcecode_eq',
        prop: 'n_leadsourcecode_eq',
        dataType: 'SSCODELIST',
      },
      {
        name: 'n_statecode_eq',
        prop: 'n_statecode_eq',
        dataType: 'NSCODELIST',
      },
      {
        name: 'n_statuscode_eq',
        prop: 'n_statuscode_eq',
        dataType: 'NSCODELIST',
      },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}