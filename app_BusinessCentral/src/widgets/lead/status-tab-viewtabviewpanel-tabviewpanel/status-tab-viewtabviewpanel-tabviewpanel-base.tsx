import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, MainControlBase } from '@/studio-core';
import LeadService from '@/service/lead/lead-service';
import StatusTabViewtabviewpanelService from './status-tab-viewtabviewpanel-tabviewpanel-service';
import LeadUIService from '@/uiservice/lead/lead-ui-service';


/**
 * tabviewpanel部件基类
 *
 * @export
 * @class MainControlBase
 * @extends {StatusTabViewtabviewpanelTabviewpanelBase}
 */
export class StatusTabViewtabviewpanelTabviewpanelBase extends MainControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof StatusTabViewtabviewpanelTabviewpanelBase
     */
    protected controlType: string = 'TABVIEWPANEL';

    /**
     * 建构部件服务对象
     *
     * @type {StatusTabViewtabviewpanelService}
     * @memberof StatusTabViewtabviewpanelTabviewpanelBase
     */
    public service: StatusTabViewtabviewpanelService = new StatusTabViewtabviewpanelService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {LeadService}
     * @memberof StatusTabViewtabviewpanelTabviewpanelBase
     */
    public appEntityService: LeadService = new LeadService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof StatusTabViewtabviewpanelTabviewpanelBase
     */
    protected appDeName: string = 'lead';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof StatusTabViewtabviewpanelTabviewpanelBase
     */
    protected appDeLogicName: string = '潜在顾客';

    /**
     * 界面UI服务对象
     *
     * @type {LeadUIService}
     * @memberof StatusTabViewtabviewpanelBase
     */  
    public appUIService:LeadUIService = new LeadUIService(this.$store);

    /**
     * 导航模式下项是否激活
     *
     * @type {*}
     * @memberof StatusTabViewtabviewpanel
     */
    @Prop()
    public expActive!: any;

    /**
     * 获取多项数据
     *
     * @returns {any[]}
     * @memberof StatusTabViewtabviewpanel
     */
    public getDatas(): any[] {
        return [];
    }

    /**
     * 获取单项树
     *
     * @returns {*}
     * @memberof StatusTabViewtabviewpanel
     */
    public getData(): any {
        return null;
    }

    /**
     * 是否被激活
     *
     * @type {boolean}
     * @memberof StatusTabViewtabviewpanel
     */
    public isActivied: boolean = true;

    /**
     * 局部上下文
     *
     * @type {*}
     * @memberof StatusTabViewtabviewpanel
     */
    public localContext: any = null;

    /**
     * 局部视图参数
     *
     * @type {*}
     * @memberof StatusTabViewtabviewpanel
     */
    public localViewParam: any = null;

    /**
     * 传入上下文
     *
     * @type {string}
     * @memberof TabExpViewtabviewpanel
     */
    public viewdata: string = JSON.stringify(this.context);

    /**
     * 传入视图参数
     *
     * @type {string}
     * @memberof PickupViewpickupviewpanel
     */
    public viewparam: string = JSON.stringify(this.viewparams);

    /**
     * 视图面板过滤项
     *
     * @type {string}
     * @memberof StatusTabViewtabviewpanel
     */
    public navfilter: string = "";
             
    /**
     * vue 生命周期
     *
     * @returns
     * @memberof StatusTabViewtabviewpanel
     */
    public created() {
        this.afterCreated();
    }

    /**
     * 执行created后的逻辑
     *
     *  @memberof StatusTabViewtabviewpanel
     */    
    public afterCreated(){
        this.initNavParam();
        if (this.viewState) {
            this.viewStateEvent = this.viewState.subscribe(({ tag, action, data }) => {
                if (!Object.is(tag, this.name)) {
                    return;
                }
                this.$forceUpdate();
                this.initNavParam();
            });
        }
    }

    /**
     * 初始化导航参数
     *
     * @memberof StatusTabViewtabviewpanel
     */
    public initNavParam(){
        if(!Object.is(this.navfilter,"")){
            Object.assign(this.viewparams,{[this.navfilter]:this.context['majorentity']})
        }
        if(this.localContext && Object.keys(this.localContext).length >0){
            let _context:any = this.$util.computedNavData({},this.context,this.viewparams,this.localContext);
            Object.assign(this.context,_context);
        }
        if(this.localViewParam && Object.keys(this.localViewParam).length >0){
            let _param:any = this.$util.computedNavData({},this.context,this.viewparams,this.localViewParam);
            Object.assign(this.viewparams,_param);
        }
        this.viewdata =JSON.stringify(this.context);
        this.viewparam = JSON.stringify(this.viewparams);
    }

    /**
     * 视图数据变化
     *
     * @memberof  StatusTabViewtabviewpanel
     */
    public viewDatasChange($event:any){
        this.$emit('viewpanelDatasChange',$event);
    }

    /**
     * vue 生命周期
     *
     * @memberof StatusTabViewtabviewpanel
     */
    public destroyed() {
        this.afterDestroy();
    }

    /**
     * 执行destroyed后的逻辑
     *
     * @memberof StatusTabViewtabviewpanel
     */
    public afterDestroy() {
        if (this.viewStateEvent) {
            this.viewStateEvent.unsubscribe();
        }
    }
}