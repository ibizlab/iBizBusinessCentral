/**
 * Qualification 部件模型
 *
 * @export
 * @class QualificationModel
 */
export default class QualificationModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof QualificationModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'leadid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'fullname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'parentcontactname',
        prop: 'parentcontactname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'parentaccountname',
        prop: 'parentaccountname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'purchasetimeframe',
        prop: 'purchasetimeframe',
        dataType: 'SSCODELIST',
      },
      {
        name: 'budgetamount',
        prop: 'budgetamount',
        dataType: 'BIGDECIMAL',
      },
      {
        name: 'purchaseprocess',
        prop: 'purchaseprocess',
        dataType: 'SSCODELIST',
      },
      {
        name: 'decisionmaker',
        prop: 'decisionmaker',
        dataType: 'YESNO',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'TEXT',
      },
      {
        name: 'fullname',
        prop: 'fullname',
        dataType: 'TEXT',
      },
      {
        name: 'companyname',
        prop: 'companyname',
        dataType: 'TEXT',
      },
      {
        name: 'parentcontactid',
        prop: 'parentcontactid',
        dataType: 'PICKUP',
      },
      {
        name: 'parentaccountid',
        prop: 'parentaccountid',
        dataType: 'PICKUP',
      },
      {
        name: 'leadid',
        prop: 'leadid',
        dataType: 'GUID',
      },
      {
        name: 'lead',
        prop: 'leadid',
        dataType: 'FONTKEY',
      },
    ]
  }

}