import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, EditFormControlBase } from '@/studio-core';
import LeadService from '@/service/lead/lead-service';
import Edit_MainService from './edit-main-form-service';
import LeadUIService from '@/uiservice/lead/lead-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';


/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {Edit_MainEditFormBase}
 */
export class Edit_MainEditFormBase extends EditFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof Edit_MainEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {Edit_MainService}
     * @memberof Edit_MainEditFormBase
     */
    public service: Edit_MainService = new Edit_MainService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {LeadService}
     * @memberof Edit_MainEditFormBase
     */
    public appEntityService: LeadService = new LeadService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Edit_MainEditFormBase
     */
    protected appDeName: string = 'lead';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof Edit_MainEditFormBase
     */
    protected appDeLogicName: string = '潜在顾客';

    /**
     * 界面UI服务对象
     *
     * @type {LeadUIService}
     * @memberof Edit_MainBase
     */  
    public appUIService:LeadUIService = new LeadUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof Edit_MainEditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        subject: null,
        fullname: null,
        jobtitle: null,
        telephone1: null,
        mobilephone: null,
        emailaddress1: null,
        companyname: null,
        websiteurl: null,
        address1_postalcode: null,
        address1_country: null,
        address1_stateorprovince: null,
        address1_city: null,
        address1_line1: null,
        industrycode: null,
        revenue: null,
        numberofemployees: null,
        transactioncurrencyname: null,
        campaignname: null,
        donotsendmm: null,
        lastusedincampaign: null,
        transactioncurrencyid: null,
        campaignid: null,
        leadid: null,
        lead:null,
    };

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Edit_MainEditFormBase
     */
    public rules: any = {
        subject: [
            { required: true, type: 'string', message: '主题 值不能为空', trigger: 'change' },
            { required: true, type: 'string', message: '主题 值不能为空', trigger: 'blur' },
        ],
        fullname: [
            { required: true, type: 'string', message: '姓名 值不能为空', trigger: 'change' },
            { required: true, type: 'string', message: '姓名 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Edit_MainBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof Edit_MainEditFormBase
     */
    public detailsModel: any = {
        group1: new FormGroupPanelModel({ caption: '潜在顾客基本信息', detailType: 'GROUPPANEL', name: 'group1', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.lead.edit_main_form', extractMode: 'ITEM', details: [] } }),

        grouppanel1: new FormGroupPanelModel({ caption: '公司信息', detailType: 'GROUPPANEL', name: 'grouppanel1', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.lead.edit_main_form', extractMode: 'ITEM', details: [] } }),

        grouppanel2: new FormGroupPanelModel({ caption: '详细信息', detailType: 'GROUPPANEL', name: 'grouppanel2', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.lead.edit_main_form', extractMode: 'ITEM', details: [] } }),

        grouppanel3: new FormGroupPanelModel({ caption: '市场营销信息', detailType: 'GROUPPANEL', name: 'grouppanel3', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.lead.edit_main_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({ caption: '更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 0 }),

        srforikey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfkey: new FormItemModel({ caption: '潜在顾客', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfmajortext: new FormItemModel({ caption: '姓名', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srftempmode: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfuf: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfdeid: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfsourcekey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        subject: new FormItemModel({ caption: '主题', detailType: 'FORMITEM', name: 'subject', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        fullname: new FormItemModel({ caption: '姓名', detailType: 'FORMITEM', name: 'fullname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        jobtitle: new FormItemModel({ caption: '职务', detailType: 'FORMITEM', name: 'jobtitle', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        telephone1: new FormItemModel({ caption: '商务电话', detailType: 'FORMITEM', name: 'telephone1', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        mobilephone: new FormItemModel({ caption: '移动电话', detailType: 'FORMITEM', name: 'mobilephone', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        emailaddress1: new FormItemModel({ caption: '电子邮件', detailType: 'FORMITEM', name: 'emailaddress1', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        companyname: new FormItemModel({ caption: '公司名称', detailType: 'FORMITEM', name: 'companyname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        websiteurl: new FormItemModel({ caption: '网站', detailType: 'FORMITEM', name: 'websiteurl', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        address1_postalcode: new FormItemModel({ caption: '邮政编码', detailType: 'FORMITEM', name: 'address1_postalcode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        address1_country: new FormItemModel({ caption: '国家/地区', detailType: 'FORMITEM', name: 'address1_country', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        address1_stateorprovince: new FormItemModel({ caption: '省/直辖市/自治区', detailType: 'FORMITEM', name: 'address1_stateorprovince', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        address1_city: new FormItemModel({ caption: '市/县', detailType: 'FORMITEM', name: 'address1_city', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        address1_line1: new FormItemModel({ caption: '街道', detailType: 'FORMITEM', name: 'address1_line1', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        industrycode: new FormItemModel({ caption: '行业', detailType: 'FORMITEM', name: 'industrycode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        revenue: new FormItemModel({ caption: '年收入', detailType: 'FORMITEM', name: 'revenue', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        numberofemployees: new FormItemModel({ caption: '员工数', detailType: 'FORMITEM', name: 'numberofemployees', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        transactioncurrencyname: new FormItemModel({ caption: '货币', detailType: 'FORMITEM', name: 'transactioncurrencyname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        campaignname: new FormItemModel({ caption: '源市场活动', detailType: 'FORMITEM', name: 'campaignname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        donotsendmm: new FormItemModel({ caption: '市场营销资料', detailType: 'FORMITEM', name: 'donotsendmm', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        lastusedincampaign: new FormItemModel({ caption: '上次市场活动日期', detailType: 'FORMITEM', name: 'lastusedincampaign', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        transactioncurrencyid: new FormItemModel({ caption: '货币', detailType: 'FORMITEM', name: 'transactioncurrencyid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        campaignid: new FormItemModel({ caption: '源市场活动', detailType: 'FORMITEM', name: 'campaignid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        leadid: new FormItemModel({ caption: '潜在顾客', detailType: 'FORMITEM', name: 'leadid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

    };
}