/**
 * Edit_DatePanel 部件模型
 *
 * @export
 * @class Edit_DatePanelModel
 */
export default class Edit_DatePanelModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Edit_DatePanelModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'opportunityid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'opportunityname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'estimatedclosedate',
        prop: 'estimatedclosedate',
        dataType: 'DATETIME',
      },
      {
        name: 'estimatedvalue',
        prop: 'estimatedvalue',
        dataType: 'BIGDECIMAL',
      },
      {
        name: 'statecode',
        prop: 'statecode',
        dataType: 'NSCODELIST',
      },
      {
        name: 'ownername',
        prop: 'ownername',
        dataType: 'TEXT',
      },
      {
        name: 'opportunityid',
        prop: 'opportunityid',
        dataType: 'GUID',
      },
      {
        name: 'opportunity',
        prop: 'opportunityid',
        dataType: 'FONTKEY',
      },
    ]
  }

}