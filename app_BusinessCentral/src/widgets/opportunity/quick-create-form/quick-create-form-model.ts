/**
 * QuickCreate 部件模型
 *
 * @export
 * @class QuickCreateModel
 */
export default class QuickCreateModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof QuickCreateModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'opportunityid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'opportunityname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'opportunityname',
        prop: 'opportunityname',
        dataType: 'TEXT',
      },
      {
        name: 'parentcontactname',
        prop: 'parentcontactname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'parentaccountname',
        prop: 'parentaccountname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'purchasetimeframe',
        prop: 'purchasetimeframe',
        dataType: 'SSCODELIST',
      },
      {
        name: 'transactioncurrencyname',
        prop: 'currencyname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'budgetamount',
        prop: 'budgetamount',
        dataType: 'BIGDECIMAL',
      },
      {
        name: 'purchaseprocess',
        prop: 'purchaseprocess',
        dataType: 'SSCODELIST',
      },
      {
        name: 'parentcontactid',
        prop: 'parentcontactid',
        dataType: 'PICKUP',
      },
      {
        name: 'parentaccountid',
        prop: 'parentaccountid',
        dataType: 'PICKUP',
      },
      {
        name: 'transactioncurrencyid',
        prop: 'transactioncurrencyid',
        dataType: 'PICKUP',
      },
      {
        name: 'opportunityid',
        prop: 'opportunityid',
        dataType: 'GUID',
      },
      {
        name: 'opportunity',
        prop: 'opportunityid',
        dataType: 'FONTKEY',
      },
    ]
  }

}