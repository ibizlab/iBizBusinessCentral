import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, EditFormControlBase } from '@/studio-core';
import OpportunityService from '@/service/opportunity/opportunity-service';
import AbstractInfoService from './abstract-info-form-service';
import OpportunityUIService from '@/uiservice/opportunity/opportunity-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';


/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {AbstractInfoEditFormBase}
 */
export class AbstractInfoEditFormBase extends EditFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof AbstractInfoEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {AbstractInfoService}
     * @memberof AbstractInfoEditFormBase
     */
    public service: AbstractInfoService = new AbstractInfoService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {OpportunityService}
     * @memberof AbstractInfoEditFormBase
     */
    public appEntityService: OpportunityService = new OpportunityService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof AbstractInfoEditFormBase
     */
    protected appDeName: string = 'opportunity';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof AbstractInfoEditFormBase
     */
    protected appDeLogicName: string = '商机';

    /**
     * 界面UI服务对象
     *
     * @type {OpportunityUIService}
     * @memberof AbstractInfoBase
     */  
    public appUIService:OpportunityUIService = new OpportunityUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof AbstractInfoEditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        opportunityname: null,
        parentcontactname: null,
        parentaccountname: null,
        purchasetimeframe: null,
        transactioncurrencyname: null,
        budgetamount: null,
        purchaseprocess: null,
        description: null,
        pricelevelname: null,
        currentsituation: null,
        customerneed: null,
        proposedsolution: null,
        parentcontactid: null,
        parentaccountid: null,
        opportunityid: null,
        opportunity:null,
    };

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof AbstractInfoEditFormBase
     */
    public rules: any = {
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof AbstractInfoBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof AbstractInfoEditFormBase
     */
    public detailsModel: any = {
        group1: new FormGroupPanelModel({ caption: '商机基本信息', detailType: 'GROUPPANEL', name: 'group1', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.opportunity.abstractinfo_form', extractMode: 'ITEM', details: [] } }),

        grouppanel1: new FormGroupPanelModel({ caption: '详细信息', detailType: 'GROUPPANEL', name: 'grouppanel1', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.opportunity.abstractinfo_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({ caption: '更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 0 }),

        srforikey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfkey: new FormItemModel({ caption: '商机', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfmajortext: new FormItemModel({ caption: '商机名称', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srftempmode: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfuf: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfdeid: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfsourcekey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        opportunityname: new FormItemModel({ caption: '商机名称', detailType: 'FORMITEM', name: 'opportunityname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        parentcontactname: new FormItemModel({ caption: '联系人', detailType: 'FORMITEM', name: 'parentcontactname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        parentaccountname: new FormItemModel({ caption: '帐户', detailType: 'FORMITEM', name: 'parentaccountname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        purchasetimeframe: new FormItemModel({ caption: '购买时间范围', detailType: 'FORMITEM', name: 'purchasetimeframe', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        transactioncurrencyname: new FormItemModel({ caption: '货币', detailType: 'FORMITEM', name: 'transactioncurrencyname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        budgetamount: new FormItemModel({ caption: '预算金额', detailType: 'FORMITEM', name: 'budgetamount', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        purchaseprocess: new FormItemModel({ caption: '采购程序', detailType: 'FORMITEM', name: 'purchaseprocess', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        description: new FormItemModel({ caption: '说明', detailType: 'FORMITEM', name: 'description', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        pricelevelname: new FormItemModel({ caption: '价目表', detailType: 'FORMITEM', name: 'pricelevelname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        currentsituation: new FormItemModel({ caption: '当前状况', detailType: 'FORMITEM', name: 'currentsituation', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        customerneed: new FormItemModel({ caption: '客户需求', detailType: 'FORMITEM', name: 'customerneed', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        proposedsolution: new FormItemModel({ caption: '已拟定解决方案', detailType: 'FORMITEM', name: 'proposedsolution', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        parentcontactid: new FormItemModel({ caption: '联系人', detailType: 'FORMITEM', name: 'parentcontactid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        parentaccountid: new FormItemModel({ caption: '帐户', detailType: 'FORMITEM', name: 'parentaccountid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        opportunityid: new FormItemModel({ caption: '商机', detailType: 'FORMITEM', name: 'opportunityid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

    };
}