import { Http,Util,Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import OpportunityService from '@/service/opportunity/opportunity-service';
import InfotabexppanelModel from './infotabexppanel-tabexppanel-model';


/**
 * Infotabexppanel 部件服务对象
 *
 * @export
 * @class InfotabexppanelService
 */
export default class InfotabexppanelService extends ControlService {

    /**
     * 商机服务对象
     *
     * @type {OpportunityService}
     * @memberof InfotabexppanelService
     */
    public appEntityService: OpportunityService = new OpportunityService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof InfotabexppanelService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of InfotabexppanelService.
     * 
     * @param {*} [opts={}]
     * @memberof InfotabexppanelService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new InfotabexppanelModel();
    }

    
}