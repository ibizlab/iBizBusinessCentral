import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * PART_CHT_001 部件服务对象
 *
 * @export
 * @class PART_CHT_001Service
 */
export default class PART_CHT_001Service extends ControlService {
}
