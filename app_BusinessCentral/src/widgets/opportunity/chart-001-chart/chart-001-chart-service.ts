import { Http,Util,Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import OpportunityService from '@/service/opportunity/opportunity-service';
import CHART_001Model from './chart-001-chart-model';


/**
 * CHART_001 部件服务对象
 *
 * @export
 * @class CHART_001Service
 */
export default class CHART_001Service extends ControlService {

    /**
     * 商机服务对象
     *
     * @type {OpportunityService}
     * @memberof CHART_001Service
     */
    public appEntityService: OpportunityService = new OpportunityService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof CHART_001Service
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of CHART_001Service.
     * 
     * @param {*} [opts={}]
     * @memberof CHART_001Service
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new CHART_001Model();
    }

    /**
     * 查询数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof CHART_001Service
     */
    @Errorlog
    public search(action: string,context: any = {}, data: any = {}, isloading?: boolean): Promise<any> {
        const {data:Data,context:Context} = this.handleRequestData(action,context,data,true);
        return new Promise((resolve: any, reject: any) => {
            const _appEntityService: any = this.appEntityService;
            let result: Promise<any>;
            if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
                result = _appEntityService[action](Context,Data, isloading);
            }else{
                result =_appEntityService.FetchDefault(Context,Data, isloading);
            }
            result.then((response) => {
                resolve(response);
            }).catch(response => {
                reject(response);
            });      
        });
    }
}