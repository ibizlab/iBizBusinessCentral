import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, SearchFormControlBase } from '@/studio-core';
import OpportunityService from '@/service/opportunity/opportunity-service';
import DefaultService from './default-searchform-service';
import OpportunityUIService from '@/uiservice/opportunity/opportunity-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';


/**
 * searchform部件基类
 *
 * @export
 * @class SearchFormControlBase
 * @extends {DefaultSearchFormBase}
 */
export class DefaultSearchFormBase extends SearchFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected controlType: string = 'SEARCHFORM';

    /**
     * 建构部件服务对象
     *
     * @type {DefaultService}
     * @memberof DefaultSearchFormBase
     */
    public service: DefaultService = new DefaultService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {OpportunityService}
     * @memberof DefaultSearchFormBase
     */
    public appEntityService: OpportunityService = new OpportunityService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected appDeName: string = 'opportunity';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected appDeLogicName: string = '商机';

    /**
     * 界面UI服务对象
     *
     * @type {OpportunityUIService}
     * @memberof DefaultBase
     */  
    public appUIService:OpportunityUIService = new OpportunityUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof DefaultSearchFormBase
     */
    public data: any = {
        n_opportunityname_like: null,
        n_parentaccountname_like: null,
        n_parentcontactname_like: null,
        n_prioritycode_eq: null,
        n_statecode_eq: null,
        n_statuscode_eq: null,
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof DefaultSearchFormBase
     */
    public detailsModel: any = {
        formpage1: new FormPageModel({ caption: '常规条件', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this })
, 
        n_opportunityname_like: new FormItemModel({ caption: '商机名称(文本包含(%))', detailType: 'FORMITEM', name: 'n_opportunityname_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_parentaccountname_like: new FormItemModel({ caption: '帐户(文本包含(%))', detailType: 'FORMITEM', name: 'n_parentaccountname_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_parentcontactname_like: new FormItemModel({ caption: '联系人(文本包含(%))', detailType: 'FORMITEM', name: 'n_parentcontactname_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_prioritycode_eq: new FormItemModel({ caption: '优先级(等于(=))', detailType: 'FORMITEM', name: 'n_prioritycode_eq', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_statecode_eq: new FormItemModel({ caption: '状态(等于(=))', detailType: 'FORMITEM', name: 'n_statecode_eq', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_statuscode_eq: new FormItemModel({ caption: '状态描述(等于(=))', detailType: 'FORMITEM', name: 'n_statuscode_eq', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
    };
}