/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'parentcontactid',
          prop: 'parentcontactid',
          dataType: 'PICKUP',
        },
        {
          name: 'closeprobability',
          prop: 'closeprobability',
          dataType: 'INT',
        },
        {
          name: 'transactioncurrencyid',
          prop: 'transactioncurrencyid',
          dataType: 'PICKUP',
        },
        {
          name: 'parentaccountname',
          prop: 'parentaccountname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'slaid',
          prop: 'slaid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'opportunityname',
          dataType: 'TEXT',
        },
        {
          name: 'srfkey',
          prop: 'opportunityid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'srfdataaccaction',
          prop: 'opportunityid',
          dataType: 'GUID',
        },
        {
          name: 'parentcontactname',
          prop: 'parentcontactname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'estimatedvalue',
          prop: 'estimatedvalue',
          dataType: 'BIGDECIMAL',
        },
        {
          name: 'originatingleadid',
          prop: 'originatingleadid',
          dataType: 'PICKUP',
        },
        {
          name: 'parentaccountid',
          prop: 'parentaccountid',
          dataType: 'PICKUP',
        },
        {
          name: 'estimatedclosedate',
          prop: 'estimatedclosedate',
          dataType: 'DATETIME',
        },
        {
          name: 'opportunityid',
          prop: 'opportunityid',
          dataType: 'GUID',
        },
        {
          name: 'campaignid',
          prop: 'campaignid',
          dataType: 'PICKUP',
        },
        {
          name: 'pricelevelid',
          prop: 'pricelevelid',
          dataType: 'PICKUP',
        },
        {
          name: 'opportunityname',
          prop: 'opportunityname',
          dataType: 'TEXT',
        },
        {
          name: 'opportunity',
          prop: 'opportunityid',
        },
      {
        name: 'n_opportunityname_like',
        prop: 'n_opportunityname_like',
        dataType: 'TEXT',
      },
      {
        name: 'n_parentaccountname_like',
        prop: 'n_parentaccountname_like',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'n_parentcontactname_like',
        prop: 'n_parentcontactname_like',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'n_prioritycode_eq',
        prop: 'n_prioritycode_eq',
        dataType: 'SSCODELIST',
      },
      {
        name: 'n_statecode_eq',
        prop: 'n_statecode_eq',
        dataType: 'NSCODELIST',
      },
      {
        name: 'n_statuscode_eq',
        prop: 'n_statuscode_eq',
        dataType: 'NSCODELIST',
      },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}