import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, GridControlBase } from '@/studio-core';
import OpportunityService from '@/service/opportunity/opportunity-service';
import MainService from './main-grid-service';
import OpportunityUIService from '@/uiservice/opportunity/opportunity-ui-service';
import { FormItemModel } from '@/model/form-detail';


/**
 * grid部件基类
 *
 * @export
 * @class GridControlBase
 * @extends {MainGridBase}
 */
export class MainGridBase extends GridControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected controlType: string = 'GRID';

    /**
     * 建构部件服务对象
     *
     * @type {MainService}
     * @memberof MainGridBase
     */
    public service: MainService = new MainService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {OpportunityService}
     * @memberof MainGridBase
     */
    public appEntityService: OpportunityService = new OpportunityService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected appDeName: string = 'opportunity';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof MainGridBase
     */
    protected appDeLogicName: string = '商机';

    /**
     * 界面UI服务对象
     *
     * @type {OpportunityUIService}
     * @memberof MainBase
     */  
    public appUIService:OpportunityUIService = new OpportunityUIService(this.$store);

    /**
     * 界面行为模型
     *
     * @type {*}
     * @memberof MainBase
     */  
    public ActionModel: any = {
    };

    /**
     * 本地缓存标识
     *
     * @protected
     * @type {string}
     * @memberof MainBase
     */
    protected localStorageTag: string = 'opportunity_main_grid';

    /**
     * 所有列成员
     *
     * @type {any[]}
     * @memberof MainGridBase
     */
    public allColumns: any[] = [
        {
            name: 'opportunityname',
            label: '商机名称',
            langtag: 'entities.opportunity.main_grid.columns.opportunityname',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'parentaccountname',
            label: '帐户',
            langtag: 'entities.opportunity.main_grid.columns.parentaccountname',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'parentcontactname',
            label: '联系人',
            langtag: 'entities.opportunity.main_grid.columns.parentcontactname',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'estimatedclosedate',
            label: '预计结束日期',
            langtag: 'entities.opportunity.main_grid.columns.estimatedclosedate',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'closeprobability',
            label: '可能性',
            langtag: 'entities.opportunity.main_grid.columns.closeprobability',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'estimatedvalue',
            label: '预计收入',
            langtag: 'entities.opportunity.main_grid.columns.estimatedvalue',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
    ]

    /**
     * 获取表格行模型
     *
     * @type {*}
     * @memberof MainGridBase
     */
    public getGridRowModel(){
        return {
          srfkey: new FormItemModel(),
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof MainGridBase
     */
    public rules: any = {
        srfkey: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '商机 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '商机 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 获取对应列class
     *
     * @type {*}
     * @memberof MainBase
     */
    public hasRowEdit: any = {
        'opportunityname':false,
        'parentaccountname':false,
        'parentcontactname':false,
        'estimatedclosedate':false,
        'closeprobability':false,
        'estimatedvalue':false,
    };

    /**
     * 获取对应列class
     *
     * @param {*} $args row 行数据，column 列数据，rowIndex 行索引，列索引
     * @returns {void}
     * @memberof MainBase
     */
    public getCellClassName(args: {row: any, column: any, rowIndex: number, columnIndex: number}): any {
        return ( this.hasRowEdit[args.column.property] && this.actualIsOpenEdit ) ? "edit-cell" : "info-cell";
    }


    /**
     * 导出数据格式化
     *
     * @param {*} filterVal
     * @param {*} jsonData
     * @param {any[]} [codelistColumns=[]]
     * @returns {Promise<any>}
     * @memberof MainGridBase
     */
    public async formatExcelData(filterVal: any, jsonData: any, codelistColumns?: any[]): Promise<any> {
        return super.formatExcelData(filterVal, jsonData, [
        ]);
    }

}