import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, GridControlBase } from '@/studio-core';
import DictOptionService from '@/service/dict-option/dict-option-service';
import OptionsService from './options-grid-service';
import DictOptionUIService from '@/uiservice/dict-option/dict-option-ui-service';
import { FormItemModel } from '@/model/form-detail';


/**
 * grid部件基类
 *
 * @export
 * @class GridControlBase
 * @extends {OptionsGridBase}
 */
export class OptionsGridBase extends GridControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof OptionsGridBase
     */
    protected controlType: string = 'GRID';

    /**
     * 建构部件服务对象
     *
     * @type {OptionsService}
     * @memberof OptionsGridBase
     */
    public service: OptionsService = new OptionsService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {DictOptionService}
     * @memberof OptionsGridBase
     */
    public appEntityService: DictOptionService = new DictOptionService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof OptionsGridBase
     */
    protected appDeName: string = 'dictoption';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof OptionsGridBase
     */
    protected appDeLogicName: string = '字典项';

    /**
     * 界面UI服务对象
     *
     * @type {DictOptionUIService}
     * @memberof OptionsBase
     */  
    public appUIService:DictOptionUIService = new DictOptionUIService(this.$store);

    /**
     * 界面行为模型
     *
     * @type {*}
     * @memberof OptionsBase
     */  
    public ActionModel: any = {
    };

    /**
     * 本地缓存标识
     *
     * @protected
     * @type {string}
     * @memberof OptionsBase
     */
    protected localStorageTag: string = 'dict_option_options_grid';

    /**
     * 排序方向
     *
     * @type {string}
     * @memberof OptionsGridBase
     */
    public minorSortDir: string = 'ASC';

    /**
     * 排序字段
     *
     * @type {string}
     * @memberof OptionsGridBase
     */
    public minorSortPSDEF: string = 'showorder';

    /**
     * 分页条数
     *
     * @type {number}
     * @memberof OptionsGridBase
     */
    public limit: number = 50;

    /**
     * 所有列成员
     *
     * @type {any[]}
     * @memberof OptionsGridBase
     */
    public allColumns: any[] = [
        {
            name: 'cid',
            label: '目录代码',
            langtag: 'entities.dictoption.options_grid.columns.cid',
            show: false,
            unit: 'PX',
            isEnableRowEdit: true,
        },
        {
            name: 'val',
            label: '代码值',
            langtag: 'entities.dictoption.options_grid.columns.val',
            show: true,
            unit: 'PX',
            isEnableRowEdit: true,
        },
        {
            name: 'label',
            label: '名称',
            langtag: 'entities.dictoption.options_grid.columns.label',
            show: true,
            unit: 'PX',
            isEnableRowEdit: true,
        },
        {
            name: 'pval',
            label: '父代码值',
            langtag: 'entities.dictoption.options_grid.columns.pval',
            show: true,
            unit: 'PX',
            isEnableRowEdit: true,
        },
        {
            name: 'showorder',
            label: '排序',
            langtag: 'entities.dictoption.options_grid.columns.showorder',
            show: true,
            unit: 'PX',
            isEnableRowEdit: true,
        },
        {
            name: 'cname',
            label: '目录',
            langtag: 'entities.dictoption.options_grid.columns.cname',
            show: false,
            unit: 'PX',
            isEnableRowEdit: true,
        },
        {
            name: 'cls',
            label: '栏目样式',
            langtag: 'entities.dictoption.options_grid.columns.cls',
            show: true,
            unit: 'PX',
            isEnableRowEdit: true,
        },
        {
            name: 'iconcls',
            label: '图标',
            langtag: 'entities.dictoption.options_grid.columns.iconcls',
            show: true,
            unit: 'PX',
            isEnableRowEdit: true,
        },
        {
            name: 'vfilter',
            label: '过滤项',
            langtag: 'entities.dictoption.options_grid.columns.vfilter',
            show: true,
            unit: 'PX',
            isEnableRowEdit: true,
        },
        {
            name: 'disabled',
            label: '是否禁用',
            langtag: 'entities.dictoption.options_grid.columns.disabled',
            show: true,
            unit: 'PX',
            isEnableRowEdit: true,
        },
        {
            name: 'expired',
            label: '过期/失效',
            langtag: 'entities.dictoption.options_grid.columns.expired',
            show: true,
            unit: 'PX',
            isEnableRowEdit: true,
        },
        {
            name: 'extension',
            label: '扩展',
            langtag: 'entities.dictoption.options_grid.columns.extension',
            show: true,
            unit: 'PX',
            isEnableRowEdit: true,
        },
    ]

    /**
     * 获取表格行模型
     *
     * @type {*}
     * @memberof OptionsGridBase
     */
    public getGridRowModel(){
        return {
          val: new FormItemModel(),
          pval: new FormItemModel(),
          cls: new FormItemModel(),
          expired: new FormItemModel(),
          label: new FormItemModel(),
          cid: new FormItemModel(),
          srfkey: new FormItemModel(),
          iconcls: new FormItemModel(),
          extension: new FormItemModel(),
          vfilter: new FormItemModel(),
          showorder: new FormItemModel(),
          cname: new FormItemModel(),
          disabled: new FormItemModel(),
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof OptionsGridBase
     */
    public rules: any = {
        val: [
            { required: true, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '代码值 值不能为空', trigger: 'change' },
            { required: true, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '代码值 值不能为空', trigger: 'blur' },
        ],
        pval: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '父代码值 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '父代码值 值不能为空', trigger: 'blur' },
        ],
        cls: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '栏目样式 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '栏目样式 值不能为空', trigger: 'blur' },
        ],
        expired: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '过期/失效 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '过期/失效 值不能为空', trigger: 'blur' },
        ],
        label: [
            { required: true, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '名称 值不能为空', trigger: 'change' },
            { required: true, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '名称 值不能为空', trigger: 'blur' },
        ],
        cid: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '目录代码 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '目录代码 值不能为空', trigger: 'blur' },
        ],
        srfkey: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '标识 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '标识 值不能为空', trigger: 'blur' },
        ],
        iconcls: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '图标 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '图标 值不能为空', trigger: 'blur' },
        ],
        extension: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '扩展 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '扩展 值不能为空', trigger: 'blur' },
        ],
        vfilter: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '过滤项 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '过滤项 值不能为空', trigger: 'blur' },
        ],
        showorder: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '排序 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '排序 值不能为空', trigger: 'blur' },
        ],
        cname: [
            { required: true, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '目录 值不能为空', trigger: 'change' },
            { required: true, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '目录 值不能为空', trigger: 'blur' },
        ],
        disabled: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '是否禁用 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '是否禁用 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 获取对应列class
     *
     * @type {*}
     * @memberof OptionsBase
     */
    public hasRowEdit: any = {
        'cid':true,
        'val':true,
        'label':true,
        'pval':true,
        'showorder':true,
        'cname':true,
        'cls':true,
        'iconcls':true,
        'vfilter':true,
        'disabled':true,
        'expired':true,
        'extension':true,
    };

    /**
     * 获取对应列class
     *
     * @param {*} $args row 行数据，column 列数据，rowIndex 行索引，列索引
     * @returns {void}
     * @memberof OptionsBase
     */
    public getCellClassName(args: {row: any, column: any, rowIndex: number, columnIndex: number}): any {
        return ( this.hasRowEdit[args.column.property] && this.actualIsOpenEdit ) ? "edit-cell" : "info-cell";
    }


    /**
     * 导出数据格式化
     *
     * @param {*} filterVal
     * @param {*} jsonData
     * @param {any[]} [codelistColumns=[]]
     * @returns {Promise<any>}
     * @memberof OptionsGridBase
     */
    public async formatExcelData(filterVal: any, jsonData: any, codelistColumns?: any[]): Promise<any> {
        return super.formatExcelData(filterVal, jsonData, [
            {
                name: 'disabled',
                srfkey: 'YesNo',
                codelistType : 'STATIC',
                renderMode: 'other',
                textSeparator: '、',
                valueSeparator: ',',
            },
            {
                name: 'expired',
                srfkey: 'YesNo',
                codelistType : 'STATIC',
                renderMode: 'other',
                textSeparator: '、',
                valueSeparator: ',',
            },
        ]);
    }

}