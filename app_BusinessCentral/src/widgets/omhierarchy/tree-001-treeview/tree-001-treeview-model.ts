/**
 * TREE_001 部件模型
 *
 * @export
 * @class TREE_001Model
 */
export default class TREE_001Model {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof TREE_001Model
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'createdate',
      },
      {
        name: 'omhierarchy',
        prop: 'omhierarchyid',
      },
      {
        name: 'omhierarchyname',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'updateman',
      },
      {
        name: 'createman',
      },
      {
        name: 'organizationid',
      },
      {
        name: 'pomhierarchyid',
      },
      {
        name: 'omhierarchycatid',
      },
      {
        name: 'valid',
      },
      {
        name: 'displayname',
      },
      {
        name: 'sn',
      },
      {
        name: 'codevalue',
      },
      {
        name: 'showorder',
      },
      {
        name: 'shortname',
      },
      {
        name: 'organizationname',
      },
      {
        name: 'omhierarchycatname',
      },
      {
        name: 'pomhierarchyname',
      },
    ]
  }


}