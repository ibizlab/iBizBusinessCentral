/**
 * TreeExpViewtreeexpbar 部件模型
 *
 * @export
 * @class TreeExpViewtreeexpbarModel
 */
export default class TreeExpViewtreeexpbarModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof TreeExpViewtreeexpbarModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'createdate',
      },
      {
        name: 'omhierarchy',
        prop: 'omhierarchyid',
      },
      {
        name: 'omhierarchyname',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'updateman',
      },
      {
        name: 'createman',
      },
      {
        name: 'organizationid',
      },
      {
        name: 'pomhierarchyid',
      },
      {
        name: 'omhierarchycatid',
      },
      {
        name: 'valid',
      },
      {
        name: 'displayname',
      },
      {
        name: 'sn',
      },
      {
        name: 'codevalue',
      },
      {
        name: 'showorder',
      },
      {
        name: 'shortname',
      },
      {
        name: 'organizationname',
      },
      {
        name: 'omhierarchycatname',
      },
      {
        name: 'pomhierarchyname',
      },
    ]
  }


}