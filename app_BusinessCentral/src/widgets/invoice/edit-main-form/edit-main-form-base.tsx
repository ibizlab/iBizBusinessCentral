import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, EditFormControlBase } from '@/studio-core';
import InvoiceService from '@/service/invoice/invoice-service';
import Edit_MainService from './edit-main-form-service';
import InvoiceUIService from '@/uiservice/invoice/invoice-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';


/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {Edit_MainEditFormBase}
 */
export class Edit_MainEditFormBase extends EditFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof Edit_MainEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {Edit_MainService}
     * @memberof Edit_MainEditFormBase
     */
    public service: Edit_MainService = new Edit_MainService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {InvoiceService}
     * @memberof Edit_MainEditFormBase
     */
    public appEntityService: InvoiceService = new InvoiceService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Edit_MainEditFormBase
     */
    protected appDeName: string = 'invoice';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof Edit_MainEditFormBase
     */
    protected appDeLogicName: string = '发票';

    /**
     * 界面UI服务对象
     *
     * @type {InvoiceUIService}
     * @memberof Edit_MainBase
     */  
    public appUIService:InvoiceUIService = new InvoiceUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof Edit_MainEditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        invoicenumber: null,
        invoicename: null,
        transactioncurrencyname: null,
        pricelevelname: null,
        datedelivered: null,
        duedate: null,
        shippingmethodcode: null,
        paymenttermscode: null,
        description: null,
        totallineitemamount: null,
        discountpercentage: null,
        discountamount: null,
        totalamountlessfreight: null,
        freightamount: null,
        totalamount: null,
        opportunityname: null,
        salesordername: null,
        customerid: null,
        billto_country: null,
        billto_stateorprovince: null,
        billto_city: null,
        billto_line1: null,
        salesorderid: null,
        transactioncurrencyid: null,
        opportunityid: null,
        pricelevelid: null,
        invoiceid: null,
        invoice:null,
    };

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Edit_MainEditFormBase
     */
    public rules: any = {
        invoicenumber: [
            { required: true, type: 'string', message: '发票编码 值不能为空', trigger: 'change' },
            { required: true, type: 'string', message: '发票编码 值不能为空', trigger: 'blur' },
        ],
        invoicename: [
            { required: true, type: 'string', message: '发票名称 值不能为空', trigger: 'change' },
            { required: true, type: 'string', message: '发票名称 值不能为空', trigger: 'blur' },
        ],
        transactioncurrencyname: [
            { required: true, type: 'string', message: '货币 值不能为空', trigger: 'change' },
            { required: true, type: 'string', message: '货币 值不能为空', trigger: 'blur' },
        ],
        pricelevelname: [
            { required: true, type: 'string', message: '价目表 值不能为空', trigger: 'change' },
            { required: true, type: 'string', message: '价目表 值不能为空', trigger: 'blur' },
        ],
        customerid: [
            { required: true, type: 'string', message: '客户 值不能为空', trigger: 'change' },
            { required: true, type: 'string', message: '客户 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Edit_MainBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof Edit_MainEditFormBase
     */
    public detailsModel: any = {
        group1: new FormGroupPanelModel({ caption: '发票基本信息', detailType: 'GROUPPANEL', name: 'group1', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.invoice.edit_main_form', extractMode: 'ITEM', details: [] } }),

        grouppanel1: new FormGroupPanelModel({ caption: '详细信息', detailType: 'GROUPPANEL', name: 'grouppanel1', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.invoice.edit_main_form', extractMode: 'ITEM', details: [] } }),

        grouppanel3: new FormGroupPanelModel({ caption: '发票金额', detailType: 'GROUPPANEL', name: 'grouppanel3', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.invoice.edit_main_form', extractMode: 'ITEM', details: [] } }),

        grouppanel2: new FormGroupPanelModel({ caption: '销售信息', detailType: 'GROUPPANEL', name: 'grouppanel2', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.invoice.edit_main_form', extractMode: 'ITEM', details: [] } }),

        grouppanel4: new FormGroupPanelModel({ caption: '帐单地址', detailType: 'GROUPPANEL', name: 'grouppanel4', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.invoice.edit_main_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({ caption: '更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 0 }),

        srforikey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfkey: new FormItemModel({ caption: '发票', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfmajortext: new FormItemModel({ caption: '发票名称', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srftempmode: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfuf: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfdeid: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfsourcekey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        invoicenumber: new FormItemModel({ caption: '发票编码', detailType: 'FORMITEM', name: 'invoicenumber', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        invoicename: new FormItemModel({ caption: '发票名称', detailType: 'FORMITEM', name: 'invoicename', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        transactioncurrencyname: new FormItemModel({ caption: '货币', detailType: 'FORMITEM', name: 'transactioncurrencyname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        pricelevelname: new FormItemModel({ caption: '价目表', detailType: 'FORMITEM', name: 'pricelevelname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        datedelivered: new FormItemModel({ caption: '交付日期', detailType: 'FORMITEM', name: 'datedelivered', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        duedate: new FormItemModel({ caption: '截止日期', detailType: 'FORMITEM', name: 'duedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        shippingmethodcode: new FormItemModel({ caption: '送货方式', detailType: 'FORMITEM', name: 'shippingmethodcode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        paymenttermscode: new FormItemModel({ caption: '付款条件', detailType: 'FORMITEM', name: 'paymenttermscode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        description: new FormItemModel({ caption: '说明', detailType: 'FORMITEM', name: 'description', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        totallineitemamount: new FormItemModel({ caption: '明细金额总计', detailType: 'FORMITEM', name: 'totallineitemamount', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        discountpercentage: new FormItemModel({ caption: '发票折扣(%)', detailType: 'FORMITEM', name: 'discountpercentage', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        discountamount: new FormItemModel({ caption: '发票折扣金额', detailType: 'FORMITEM', name: 'discountamount', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        totalamountlessfreight: new FormItemModel({ caption: '折后金额总计', detailType: 'FORMITEM', name: 'totalamountlessfreight', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        freightamount: new FormItemModel({ caption: '运费金额', detailType: 'FORMITEM', name: 'freightamount', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        totalamount: new FormItemModel({ caption: '总金额', detailType: 'FORMITEM', name: 'totalamount', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        opportunityname: new FormItemModel({ caption: '商机', detailType: 'FORMITEM', name: 'opportunityname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        salesordername: new FormItemModel({ caption: '订单', detailType: 'FORMITEM', name: 'salesordername', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        customerid: new FormItemModel({ caption: '客户', detailType: 'FORMITEM', name: 'customerid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        billto_country: new FormItemModel({ caption: '国家/地区', detailType: 'FORMITEM', name: 'billto_country', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        billto_stateorprovince: new FormItemModel({ caption: '省/市/自治区', detailType: 'FORMITEM', name: 'billto_stateorprovince', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        billto_city: new FormItemModel({ caption: '市/县', detailType: 'FORMITEM', name: 'billto_city', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        billto_line1: new FormItemModel({ caption: '街道', detailType: 'FORMITEM', name: 'billto_line1', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        salesorderid: new FormItemModel({ caption: '订单', detailType: 'FORMITEM', name: 'salesorderid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        transactioncurrencyid: new FormItemModel({ caption: '货币', detailType: 'FORMITEM', name: 'transactioncurrencyid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        opportunityid: new FormItemModel({ caption: '商机', detailType: 'FORMITEM', name: 'opportunityid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        pricelevelid: new FormItemModel({ caption: '价目表', detailType: 'FORMITEM', name: 'pricelevelid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        invoiceid: new FormItemModel({ caption: '发票', detailType: 'FORMITEM', name: 'invoiceid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

    };
}