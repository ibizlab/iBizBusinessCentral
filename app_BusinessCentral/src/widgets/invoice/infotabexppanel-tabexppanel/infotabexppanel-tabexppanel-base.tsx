import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, TabExpPanelControlBase } from '@/studio-core';
import InvoiceService from '@/service/invoice/invoice-service';
import InfotabexppanelService from './infotabexppanel-tabexppanel-service';
import InvoiceUIService from '@/uiservice/invoice/invoice-ui-service';


/**
 * tabexppanel部件基类
 *
 * @export
 * @class TabExpPanelControlBase
 * @extends {InfotabexppanelTabexppanelBase}
 */
export class InfotabexppanelTabexppanelBase extends TabExpPanelControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof InfotabexppanelTabexppanelBase
     */
    protected controlType: string = 'TABEXPPANEL';

    /**
     * 建构部件服务对象
     *
     * @type {InfotabexppanelService}
     * @memberof InfotabexppanelTabexppanelBase
     */
    public service: InfotabexppanelService = new InfotabexppanelService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {InvoiceService}
     * @memberof InfotabexppanelTabexppanelBase
     */
    public appEntityService: InvoiceService = new InvoiceService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof InfotabexppanelTabexppanelBase
     */
    protected appDeName: string = 'invoice';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof InfotabexppanelTabexppanelBase
     */
    protected appDeLogicName: string = '发票';

    /**
     * 界面UI服务对象
     *
     * @type {InvoiceUIService}
     * @memberof InfotabexppanelBase
     */  
    public appUIService:InvoiceUIService = new InvoiceUIService(this.$store);
    /**
     * 是否初始化
     *
     * @protected
     * @returns {any}
     * @memberof Infotabexppanel
     */
    protected isInit: any = {
        tabviewpanel:  true ,
        tabviewpanel2:  false ,
    }

    /**
     * 被激活的分页面板
     *
     * @protected
     * @type {string}
     * @memberof Infotabexppanel
     */
    protected activatedTabViewPanel: string = 'tabviewpanel';

    /**
     * 组件创建完毕
     *
     * @protected
     * @memberof Infotabexppanel
     */
    protected ctrlCreated(): void {
        //设置分页导航srfparentdename和srfparentkey
        if (this.context.invoice) {
            Object.assign(this.context, { srfparentdename: 'Invoice', srfparentkey: this.context.invoice });
        }
        super.ctrlCreated();
    }
}