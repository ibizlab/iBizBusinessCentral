import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, MainControlBase } from '@/studio-core';
import InvoiceService from '@/service/invoice/invoice-service';
import View_InvInfoService from './view-inv-info-portlet-service';
import InvoiceUIService from '@/uiservice/invoice/invoice-ui-service';
import { Environment } from '@/environments/environment';


/**
 * dashboard_sysportlet1部件基类
 *
 * @export
 * @class MainControlBase
 * @extends {View_InvInfoPortletBase}
 */
export class View_InvInfoPortletBase extends MainControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof View_InvInfoPortletBase
     */
    protected controlType: string = 'PORTLET';

    /**
     * 建构部件服务对象
     *
     * @type {View_InvInfoService}
     * @memberof View_InvInfoPortletBase
     */
    public service: View_InvInfoService = new View_InvInfoService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {InvoiceService}
     * @memberof View_InvInfoPortletBase
     */
    public appEntityService: InvoiceService = new InvoiceService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof View_InvInfoPortletBase
     */
    protected appDeName: string = 'invoice';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof View_InvInfoPortletBase
     */
    protected appDeLogicName: string = '发票';

    /**
     * 界面UI服务对象
     *
     * @type {InvoiceUIService}
     * @memberof View_InvInfoBase
     */  
    public appUIService:InvoiceUIService = new InvoiceUIService(this.$store);

    /**
     * 长度
     *
     * @type {number}
     * @memberof View_InvInfo
     */
    @Prop() public height?: number;

    /**
     * 宽度
     *
     * @type {number}
     * @memberof View_InvInfo
     */
    @Prop() public width?: number;



    /**
     * 是否自适应大小
     *
     * @returns {boolean}
     * @memberof View_InvInfoBase
     */
    @Prop({default: false})public isAdaptiveSize!: boolean;

    /**
     * 获取多项数据
     *
     * @returns {any[]}
     * @memberof View_InvInfoBase
     */
    public getDatas(): any[] {
        return [];
    }

    /**
     * 获取单项树
     *
     * @returns {*}
     * @memberof View_InvInfoBase
     */
    public getData(): any {
        return {};
    }

    /**
     * 获取高度
     *
     * @returns {any[]}
     * @memberof View_InvInfoBase
     */
    get getHeight(): any{
        if(!this.$util.isEmpty(this.height) && !this.$util.isNumberNaN(this.height)){
            if(this.height == 0){
                return 'auto';
            } else {
                return this.height+'px';
            }
        } else {
            return 'auto';
        }
    }

    /**
     * vue 生命周期
     *
     * @memberof View_InvInfoBase
     */
    public created() {
        this.afterCreated();
    }

    /**
     * 执行created后的逻辑
     *
     *  @memberof View_InvInfoBase
     */    
    public afterCreated(){
        if (this.viewState) {
            this.viewStateEvent = this.viewState.subscribe(({ tag, action, data }) => {
                if (!Object.is(tag, this.name)) {
                    return;
                }
                const refs: any = this.$refs;
                Object.keys(refs).forEach((_name: string) => {
                    this.viewState.next({ tag: _name, action: action, data: data });
                });
            });
        }
    }

    /**
     * vue 生命周期
     *
     * @memberof View_InvInfoBase
     */
    public destroyed() {
        this.afterDestroy();
    }

    /**
     * 执行destroyed后的逻辑
     *
     * @memberof View_InvInfoBase
     */
    public afterDestroy() {
        if (this.viewStateEvent) {
            this.viewStateEvent.unsubscribe();
        }
    }


}
