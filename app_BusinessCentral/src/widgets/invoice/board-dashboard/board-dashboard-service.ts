import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * BOARD 部件服务对象
 *
 * @export
 * @class BOARDService
 */
export default class BOARDService extends ControlService {
}