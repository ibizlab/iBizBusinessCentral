import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, GridControlBase } from '@/studio-core';
import CompetitorSalesLiteratureService from '@/service/competitor-sales-literature/competitor-sales-literature-service';
import SalLitCompGridService from './sal-lit-comp-grid-grid-service';
import CompetitorSalesLiteratureUIService from '@/uiservice/competitor-sales-literature/competitor-sales-literature-ui-service';
import { FormItemModel } from '@/model/form-detail';


/**
 * grid部件基类
 *
 * @export
 * @class GridControlBase
 * @extends {SalLitCompGridGridBase}
 */
export class SalLitCompGridGridBase extends GridControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof SalLitCompGridGridBase
     */
    protected controlType: string = 'GRID';

    /**
     * 建构部件服务对象
     *
     * @type {SalLitCompGridService}
     * @memberof SalLitCompGridGridBase
     */
    public service: SalLitCompGridService = new SalLitCompGridService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {CompetitorSalesLiteratureService}
     * @memberof SalLitCompGridGridBase
     */
    public appEntityService: CompetitorSalesLiteratureService = new CompetitorSalesLiteratureService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof SalLitCompGridGridBase
     */
    protected appDeName: string = 'competitorsalesliterature';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof SalLitCompGridGridBase
     */
    protected appDeLogicName: string = '竞争对手宣传资料';

    /**
     * 界面UI服务对象
     *
     * @type {CompetitorSalesLiteratureUIService}
     * @memberof SalLitCompGridBase
     */  
    public appUIService:CompetitorSalesLiteratureUIService = new CompetitorSalesLiteratureUIService(this.$store);

    /**
     * 界面行为模型
     *
     * @type {*}
     * @memberof SalLitCompGridBase
     */  
    public ActionModel: any = {
    };

    /**
     * 本地缓存标识
     *
     * @protected
     * @type {string}
     * @memberof SalLitCompGridBase
     */
    protected localStorageTag: string = 'competitorsalesliterature_sallitcompgrid_grid';

    /**
     * 所有列成员
     *
     * @type {any[]}
     * @memberof SalLitCompGridGridBase
     */
    public allColumns: any[] = [
        {
            name: 'entity2name',
            label: '销售宣传资料',
            langtag: 'entities.competitorsalesliterature.sallitcompgrid_grid.columns.entity2name',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
        {
            name: 'entityname',
            label: '竞争对手',
            langtag: 'entities.competitorsalesliterature.sallitcompgrid_grid.columns.entityname',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
        },
    ]

    /**
     * 获取表格行模型
     *
     * @type {*}
     * @memberof SalLitCompGridGridBase
     */
    public getGridRowModel(){
        return {
          srfkey: new FormItemModel(),
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof SalLitCompGridGridBase
     */
    public rules: any = {
        srfkey: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '关系标识 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '关系标识 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 获取对应列class
     *
     * @type {*}
     * @memberof SalLitCompGridBase
     */
    public hasRowEdit: any = {
        'entity2name':false,
        'entityname':false,
    };

    /**
     * 获取对应列class
     *
     * @param {*} $args row 行数据，column 列数据，rowIndex 行索引，列索引
     * @returns {void}
     * @memberof SalLitCompGridBase
     */
    public getCellClassName(args: {row: any, column: any, rowIndex: number, columnIndex: number}): any {
        return ( this.hasRowEdit[args.column.property] && this.actualIsOpenEdit ) ? "edit-cell" : "info-cell";
    }


    /**
     * 导出数据格式化
     *
     * @param {*} filterVal
     * @param {*} jsonData
     * @param {any[]} [codelistColumns=[]]
     * @returns {Promise<any>}
     * @memberof SalLitCompGridGridBase
     */
    public async formatExcelData(filterVal: any, jsonData: any, codelistColumns?: any[]): Promise<any> {
        return super.formatExcelData(filterVal, jsonData, [
        ]);
    }

}