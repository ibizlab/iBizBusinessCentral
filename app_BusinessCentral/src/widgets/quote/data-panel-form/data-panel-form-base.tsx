import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, EditFormControlBase } from '@/studio-core';
import QuoteService from '@/service/quote/quote-service';
import DataPanelService from './data-panel-form-service';
import QuoteUIService from '@/uiservice/quote/quote-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';


/**
 * datapanel部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {DataPanelEditFormBase}
 */
export class DataPanelEditFormBase extends EditFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof DataPanelEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {DataPanelService}
     * @memberof DataPanelEditFormBase
     */
    public service: DataPanelService = new DataPanelService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {QuoteService}
     * @memberof DataPanelEditFormBase
     */
    public appEntityService: QuoteService = new QuoteService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof DataPanelEditFormBase
     */
    protected appDeName: string = 'quote';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof DataPanelEditFormBase
     */
    protected appDeLogicName: string = '报价单';

    /**
     * 界面UI服务对象
     *
     * @type {QuoteUIService}
     * @memberof DataPanelBase
     */  
    public appUIService:QuoteUIService = new QuoteUIService(this.$store);

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public datapanel_button1_click(params: any = {}, tag?: any, $event?: any) {
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        xData = this;
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:QuoteUIService  = new QuoteUIService();
        curUIService.Quote_Edit_DataPanel(datas,contextJO, paramJO,  $event, xData,this,"Quote");
    }

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof DataPanelEditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        totalamount: null,
        effectivefrom: null,
        effectiveto: null,
        ownername: null,
        quoteid: null,
        quote:null,
    };

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof DataPanelEditFormBase
     */
    public rules: any = {
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof DataPanelBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof DataPanelEditFormBase
     */
    public detailsModel: any = {
        button1: new FormButtonModel({ caption: '编辑头部信息', detailType: 'BUTTON', name: 'button1', visible: true, isShowCaption: false, form: this, showMoreMode: 0,disabled: false, uiaction: { type: 'DEUIACTION', 
 tag: 'Edit_DataPanel',actiontarget: 'SINGLEKEY',noprivdisplaymode:2,visabled: true,disabled: false} }),

        grouppanel1: new FormGroupPanelModel({ caption: '分组面板', detailType: 'GROUPPANEL', name: 'grouppanel1', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.quote.datapanel_form', extractMode: 'ITEM', details: [] } }),

        group1: new FormGroupPanelModel({ caption: '报价单基本信息', detailType: 'GROUPPANEL', name: 'group1', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.quote.datapanel_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({ caption: '更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 0 }),

        srforikey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfkey: new FormItemModel({ caption: '报价单', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfmajortext: new FormItemModel({ caption: '报价名称', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srftempmode: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfuf: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfdeid: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfsourcekey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        totalamount: new FormItemModel({ caption: '总金额', detailType: 'FORMITEM', name: 'totalamount', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        effectivefrom: new FormItemModel({ caption: '开始生效日期', detailType: 'FORMITEM', name: 'effectivefrom', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        effectiveto: new FormItemModel({ caption: '有效截止时间', detailType: 'FORMITEM', name: 'effectiveto', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        ownername: new FormItemModel({ caption: '负责人', detailType: 'FORMITEM', name: 'ownername', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        quoteid: new FormItemModel({ caption: '报价单', detailType: 'FORMITEM', name: 'quoteid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

    };

	/**
	 * 表单 编辑头部信息 事件
	 *
	 * @memberof @memberof DataPanelEditFormBase
	 */
    public button1_click($event: any): void {
        this.datapanel_button1_click(null, null, $event);

    }
}