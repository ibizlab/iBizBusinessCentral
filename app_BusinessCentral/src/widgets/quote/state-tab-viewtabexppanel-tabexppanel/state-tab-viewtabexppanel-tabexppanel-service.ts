import { Http,Util,Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import QuoteService from '@/service/quote/quote-service';
import StateTabViewtabexppanelModel from './state-tab-viewtabexppanel-tabexppanel-model';


/**
 * StateTabViewtabexppanel 部件服务对象
 *
 * @export
 * @class StateTabViewtabexppanelService
 */
export default class StateTabViewtabexppanelService extends ControlService {

    /**
     * 报价单服务对象
     *
     * @type {QuoteService}
     * @memberof StateTabViewtabexppanelService
     */
    public appEntityService: QuoteService = new QuoteService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof StateTabViewtabexppanelService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of StateTabViewtabexppanelService.
     * 
     * @param {*} [opts={}]
     * @memberof StateTabViewtabexppanelService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new StateTabViewtabexppanelModel();
    }

    
}