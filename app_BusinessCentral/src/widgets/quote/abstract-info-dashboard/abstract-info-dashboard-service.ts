import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * AbstractInfo 部件服务对象
 *
 * @export
 * @class AbstractInfoService
 */
export default class AbstractInfoService extends ControlService {
}