import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * View_QuoAbs 部件服务对象
 *
 * @export
 * @class View_QuoAbsService
 */
export default class View_QuoAbsService extends ControlService {
}
