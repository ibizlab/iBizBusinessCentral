import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, EditFormControlBase } from '@/studio-core';
import QuoteService from '@/service/quote/quote-service';
import Edit_MainService from './edit-main-form-service';
import QuoteUIService from '@/uiservice/quote/quote-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';


/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {Edit_MainEditFormBase}
 */
export class Edit_MainEditFormBase extends EditFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof Edit_MainEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {Edit_MainService}
     * @memberof Edit_MainEditFormBase
     */
    public service: Edit_MainService = new Edit_MainService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {QuoteService}
     * @memberof Edit_MainEditFormBase
     */
    public appEntityService: QuoteService = new QuoteService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Edit_MainEditFormBase
     */
    protected appDeName: string = 'quote';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof Edit_MainEditFormBase
     */
    protected appDeLogicName: string = '报价单';

    /**
     * 界面UI服务对象
     *
     * @type {QuoteUIService}
     * @memberof Edit_MainBase
     */  
    public appUIService:QuoteUIService = new QuoteUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof Edit_MainEditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        quotenumber: null,
        quotename: null,
        transactioncurrencyname: null,
        opportunityname: null,
        customerid: null,
        pricelevelname: null,
        statecode: null,
        description: null,
        paymenttermscode: null,
        freighttermscode: null,
        billto_postalcode: null,
        formitem: null,
        billto_country: null,
        billto_stateorprovince: null,
        billto_city: null,
        billto_line1: null,
        shippingmethodcode: null,
        shipto_postalcode: null,
        willcall: null,
        transactioncurrencyid: null,
        opportunityid: null,
        pricelevelid: null,
        quoteid: null,
        quote:null,
    };

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Edit_MainEditFormBase
     */
    public rules: any = {
        quotenumber: [
            { required: true, type: 'string', message: '报价单 ID 值不能为空', trigger: 'change' },
            { required: true, type: 'string', message: '报价单 ID 值不能为空', trigger: 'blur' },
        ],
        quotename: [
            { required: true, type: 'string', message: '报价名称 值不能为空', trigger: 'change' },
            { required: true, type: 'string', message: '报价名称 值不能为空', trigger: 'blur' },
        ],
        customerid: [
            { required: true, type: 'string', message: '潜在客户 值不能为空', trigger: 'change' },
            { required: true, type: 'string', message: '潜在客户 值不能为空', trigger: 'blur' },
        ],
        statecode: [
            { required: true, type: 'number', message: '状态 值不能为空', trigger: 'change' },
            { required: true, type: 'number', message: '状态 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Edit_MainBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof Edit_MainEditFormBase
     */
    public detailsModel: any = {
        group1: new FormGroupPanelModel({ caption: '概要信息', detailType: 'GROUPPANEL', name: 'group1', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.quote.edit_main_form', extractMode: 'ITEM', details: [] } }),

        grouppanel1: new FormGroupPanelModel({ caption: '详细信息', detailType: 'GROUPPANEL', name: 'grouppanel1', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.quote.edit_main_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({ caption: '更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 0 }),

        srforikey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfkey: new FormItemModel({ caption: '报价单', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfmajortext: new FormItemModel({ caption: '报价名称', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srftempmode: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfuf: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfdeid: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfsourcekey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        quotenumber: new FormItemModel({ caption: '报价单 ID', detailType: 'FORMITEM', name: 'quotenumber', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        quotename: new FormItemModel({ caption: '报价名称', detailType: 'FORMITEM', name: 'quotename', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        transactioncurrencyname: new FormItemModel({ caption: '货币', detailType: 'FORMITEM', name: 'transactioncurrencyname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        opportunityname: new FormItemModel({ caption: '商机', detailType: 'FORMITEM', name: 'opportunityname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        customerid: new FormItemModel({ caption: '潜在客户', detailType: 'FORMITEM', name: 'customerid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        pricelevelname: new FormItemModel({ caption: '价目表', detailType: 'FORMITEM', name: 'pricelevelname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        statecode: new FormItemModel({ caption: '状态', detailType: 'FORMITEM', name: 'statecode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        description: new FormItemModel({ caption: '说明', detailType: 'FORMITEM', name: 'description', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        paymenttermscode: new FormItemModel({ caption: '付款条件', detailType: 'FORMITEM', name: 'paymenttermscode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        freighttermscode: new FormItemModel({ caption: '货运条款', detailType: 'FORMITEM', name: 'freighttermscode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        billto_postalcode: new FormItemModel({ caption: '帐单寄往地邮政编码', detailType: 'FORMITEM', name: 'billto_postalcode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        formitem: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'formitem', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        billto_country: new FormItemModel({ caption: '帐单寄往国家/地区', detailType: 'FORMITEM', name: 'billto_country', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        billto_stateorprovince: new FormItemModel({ caption: '帐单寄往省/市/自治区', detailType: 'FORMITEM', name: 'billto_stateorprovince', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        billto_city: new FormItemModel({ caption: '帐单寄往市/县', detailType: 'FORMITEM', name: 'billto_city', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        billto_line1: new FormItemModel({ caption: '帐单寄往街道', detailType: 'FORMITEM', name: 'billto_line1', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        shippingmethodcode: new FormItemModel({ caption: '送货方式', detailType: 'FORMITEM', name: 'shippingmethodcode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        shipto_postalcode: new FormItemModel({ caption: '送货地的邮政编码', detailType: 'FORMITEM', name: 'shipto_postalcode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        willcall: new FormItemModel({ caption: '送货地址', detailType: 'FORMITEM', name: 'willcall', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        transactioncurrencyid: new FormItemModel({ caption: '货币', detailType: 'FORMITEM', name: 'transactioncurrencyid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        opportunityid: new FormItemModel({ caption: '商机', detailType: 'FORMITEM', name: 'opportunityid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        pricelevelid: new FormItemModel({ caption: '价目表', detailType: 'FORMITEM', name: 'pricelevelid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        quoteid: new FormItemModel({ caption: '报价单', detailType: 'FORMITEM', name: 'quoteid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

    };
}