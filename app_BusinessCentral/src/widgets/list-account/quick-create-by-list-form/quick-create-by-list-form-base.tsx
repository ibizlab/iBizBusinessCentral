import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, EditFormControlBase } from '@/studio-core';
import ListAccountService from '@/service/list-account/list-account-service';
import QuickCreateByListService from './quick-create-by-list-form-service';
import ListAccountUIService from '@/uiservice/list-account/list-account-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';


/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {QuickCreateByListEditFormBase}
 */
export class QuickCreateByListEditFormBase extends EditFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof QuickCreateByListEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {QuickCreateByListService}
     * @memberof QuickCreateByListEditFormBase
     */
    public service: QuickCreateByListService = new QuickCreateByListService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {ListAccountService}
     * @memberof QuickCreateByListEditFormBase
     */
    public appEntityService: ListAccountService = new ListAccountService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof QuickCreateByListEditFormBase
     */
    protected appDeName: string = 'listaccount';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof QuickCreateByListEditFormBase
     */
    protected appDeLogicName: string = '营销列表-账户';

    /**
     * 界面UI服务对象
     *
     * @type {ListAccountUIService}
     * @memberof QuickCreateByListBase
     */  
    public appUIService:ListAccountUIService = new ListAccountUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof QuickCreateByListEditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        entity2name: null,
        entity2id: null,
        relationshipsid: null,
        listaccount:null,
    };

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof QuickCreateByListEditFormBase
     */
    public rules: any = {
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof QuickCreateByListBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof QuickCreateByListEditFormBase
     */
    public detailsModel: any = {
        group1: new FormGroupPanelModel({ caption: '营销列表-账户基本信息', detailType: 'GROUPPANEL', name: 'group1', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.listaccount.quickcreatebylist_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({ caption: '更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 0 }),

        srforikey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfkey: new FormItemModel({ caption: '关系标识', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfmajortext: new FormItemModel({ caption: '关系名称', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srftempmode: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfuf: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfdeid: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfsourcekey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        entity2name: new FormItemModel({ caption: '选择客户：', detailType: 'FORMITEM', name: 'entity2name', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        entity2id: new FormItemModel({ caption: '客户', detailType: 'FORMITEM', name: 'entity2id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        relationshipsid: new FormItemModel({ caption: '关系标识', detailType: 'FORMITEM', name: 'relationshipsid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

    };
}