import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, SearchFormControlBase } from '@/studio-core';
import IBZEmployeeService from '@/service/ibzemployee/ibzemployee-service';
import DefaultService from './default-searchform-service';
import IBZEmployeeUIService from '@/uiservice/ibzemployee/ibzemployee-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';


/**
 * searchform部件基类
 *
 * @export
 * @class SearchFormControlBase
 * @extends {DefaultSearchFormBase}
 */
export class DefaultSearchFormBase extends SearchFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected controlType: string = 'SEARCHFORM';

    /**
     * 建构部件服务对象
     *
     * @type {DefaultService}
     * @memberof DefaultSearchFormBase
     */
    public service: DefaultService = new DefaultService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {IBZEmployeeService}
     * @memberof DefaultSearchFormBase
     */
    public appEntityService: IBZEmployeeService = new IBZEmployeeService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected appDeName: string = 'ibzemployee';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected appDeLogicName: string = '人员';

    /**
     * 界面UI服务对象
     *
     * @type {IBZEmployeeUIService}
     * @memberof DefaultBase
     */  
    public appUIService:IBZEmployeeUIService = new IBZEmployeeUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof DefaultSearchFormBase
     */
    public data: any = {
        n_usercode_like: null,
        n_personname_like: null,
        n_mdeptname_like: null,
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof DefaultSearchFormBase
     */
    public detailsModel: any = {
        formpage1: new FormPageModel({ caption: '常规条件', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this })
, 
        n_usercode_like: new FormItemModel({ caption: '工号', detailType: 'FORMITEM', name: 'n_usercode_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_personname_like: new FormItemModel({ caption: '姓名', detailType: 'FORMITEM', name: 'n_personname_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_mdeptname_like: new FormItemModel({ caption: '部门', detailType: 'FORMITEM', name: 'n_mdeptname_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
    };
}