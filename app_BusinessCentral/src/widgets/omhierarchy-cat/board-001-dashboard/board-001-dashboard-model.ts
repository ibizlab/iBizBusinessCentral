/**
 * BOARD_001 部件模型
 *
 * @export
 * @class BOARD_001Model
 */
export default class BOARD_001Model {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof BOARD_001Model
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'updatedate',
      },
      {
        name: 'updateman',
      },
      {
        name: 'omhierarchycatname',
      },
      {
        name: 'createman',
      },
      {
        name: 'omhierarchycat',
        prop: 'omhierarchycatid',
      },
      {
        name: 'createdate',
      },
      {
        name: 'memo',
      },
    ]
  }


}