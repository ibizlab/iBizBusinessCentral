/**
 * Edit_DataPanel 部件模型
 *
 * @export
 * @class Edit_DataPanelModel
 */
export default class Edit_DataPanelModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Edit_DataPanelModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'knowledgearticleid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'title',
        dataType: 'LONGTEXT_1000',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'languagelocalename',
        prop: 'languagelocalename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'statecode',
        prop: 'statecode',
        dataType: 'NSCODELIST',
      },
      {
        name: 'knowledgearticleid',
        prop: 'knowledgearticleid',
        dataType: 'GUID',
      },
      {
        name: 'languagelocaleid',
        prop: 'languagelocaleid',
        dataType: 'PICKUP',
      },
      {
        name: 'knowledgearticle',
        prop: 'knowledgearticleid',
        dataType: 'FONTKEY',
      },
    ]
  }

}