/**
 * ViewKlArtInfo 部件模型
 *
 * @export
 * @class ViewKlArtInfoModel
 */
export default class ViewKlArtInfoModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof ViewKlArtInfoModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'knowledgearticleviews_date',
      },
      {
        name: 'keywords',
      },
      {
        name: 'statuscode',
      },
      {
        name: 'expirationstateid',
      },
      {
        name: 'isprimary',
      },
      {
        name: 'rating_count',
      },
      {
        name: 'content',
      },
      {
        name: 'ownerid',
      },
      {
        name: 'rootarticled',
      },
      {
        name: 'knowledgearticleviews_state',
      },
      {
        name: 'rating_sum',
      },
      {
        name: 'statecode',
      },
      {
        name: 'traversedpath',
      },
      {
        name: 'updateman',
      },
      {
        name: 'publishstatusid',
      },
      {
        name: 'ownertype',
      },
      {
        name: 'articlepublicnumber',
      },
      {
        name: 'primaryauthoridname',
      },
      {
        name: 'overriddencreatedon',
      },
      {
        name: 'primaryauthorid',
      },
      {
        name: 'latestversion',
      },
      {
        name: 'rating_state',
      },
      {
        name: 'createdate',
      },
      {
        name: 'versionnumber',
      },
      {
        name: 'utcconversiontimezonecode',
      },
      {
        name: 'expirationstatusid',
      },
      {
        name: 'knowledgearticle',
        prop: 'knowledgearticleid',
      },
      {
        name: 'setcategoryassociations',
      },
      {
        name: 'description',
      },
      {
        name: 'setproductassociations',
      },
      {
        name: 'minorversionnumber',
      },
      {
        name: 'stageid',
      },
      {
        name: 'processid',
      },
      {
        name: 'majorversionnumber',
      },
      {
        name: 'rating',
      },
      {
        name: 'publishon',
      },
      {
        name: 'knowledgearticleviews',
      },
      {
        name: 'rating_date',
      },
      {
        name: 'exchangerate',
      },
      {
        name: 'readyforreview',
      },
      {
        name: 'importsequencenumber',
      },
      {
        name: 'subjectiddsc',
      },
      {
        name: 'ownername',
      },
      {
        name: 'review',
      },
      {
        name: 'languagelocaleidlocaleid',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'createman',
      },
      {
        name: 'scheduledstatusid',
      },
      {
        name: 'timezoneruleversionnumber',
      },
      {
        name: 'internal',
      },
      {
        name: 'title',
      },
      {
        name: 'updatecontent',
      },
      {
        name: 'expirationdate',
      },
      {
        name: 'expiredreviewoptions',
      },
      {
        name: 'currencyname',
      },
      {
        name: 'parentarticlecontentname',
      },
      {
        name: 'rootarticlename',
      },
      {
        name: 'subjectname',
      },
      {
        name: 'languagelocalename',
      },
      {
        name: 'previousarticlecontentname',
      },
      {
        name: 'parentarticlecontentid',
      },
      {
        name: 'previousarticlecontentid',
      },
      {
        name: 'transactioncurrencyid',
      },
      {
        name: 'rootarticleid',
      },
      {
        name: 'subjectid',
      },
      {
        name: 'languagelocaleid',
      },
    ]
  }


}
