import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * ViewKlArtInfo 部件服务对象
 *
 * @export
 * @class ViewKlArtInfoService
 */
export default class ViewKlArtInfoService extends ControlService {
}
