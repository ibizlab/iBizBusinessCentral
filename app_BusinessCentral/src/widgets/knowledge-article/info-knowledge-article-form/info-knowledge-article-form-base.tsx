import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, EditFormControlBase } from '@/studio-core';
import KnowledgeArticleService from '@/service/knowledge-article/knowledge-article-service';
import Info_KnowledgeArticleService from './info-knowledge-article-form-service';
import KnowledgeArticleUIService from '@/uiservice/knowledge-article/knowledge-article-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';


/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {Info_KnowledgeArticleEditFormBase}
 */
export class Info_KnowledgeArticleEditFormBase extends EditFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof Info_KnowledgeArticleEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {Info_KnowledgeArticleService}
     * @memberof Info_KnowledgeArticleEditFormBase
     */
    public service: Info_KnowledgeArticleService = new Info_KnowledgeArticleService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {KnowledgeArticleService}
     * @memberof Info_KnowledgeArticleEditFormBase
     */
    public appEntityService: KnowledgeArticleService = new KnowledgeArticleService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Info_KnowledgeArticleEditFormBase
     */
    protected appDeName: string = 'knowledgearticle';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof Info_KnowledgeArticleEditFormBase
     */
    protected appDeLogicName: string = '知识文章';

    /**
     * 界面UI服务对象
     *
     * @type {KnowledgeArticleUIService}
     * @memberof Info_KnowledgeArticleBase
     */  
    public appUIService:KnowledgeArticleUIService = new KnowledgeArticleUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof Info_KnowledgeArticleEditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        title: null,
        keywords: null,
        internal: null,
        statuscode: null,
        ownerid: null,
        primaryauthorid: null,
        subjectname: null,
        publishon: null,
        expirationdate: null,
        content: null,
        description: null,
        knowledgearticleid: null,
        knowledgearticle:null,
    };

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Info_KnowledgeArticleEditFormBase
     */
    public rules: any = {
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Info_KnowledgeArticleBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof Info_KnowledgeArticleEditFormBase
     */
    public detailsModel: any = {
        group1: new FormGroupPanelModel({ caption: '知识文章基本信息', detailType: 'GROUPPANEL', name: 'group1', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.knowledgearticle.info_knowledgearticle_form', extractMode: 'ITEM', details: [] } }),

        grouppanel2: new FormGroupPanelModel({ caption: '发布信息', detailType: 'GROUPPANEL', name: 'grouppanel2', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.knowledgearticle.info_knowledgearticle_form', extractMode: 'ITEM', details: [] } }),

        grouppanel1: new FormGroupPanelModel({ caption: '详细信息', detailType: 'GROUPPANEL', name: 'grouppanel1', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.knowledgearticle.info_knowledgearticle_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({ caption: '更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 0 }),

        srforikey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfkey: new FormItemModel({ caption: '知识文章', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfmajortext: new FormItemModel({ caption: '标题', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srftempmode: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfuf: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfdeid: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        srfsourcekey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        title: new FormItemModel({ caption: '标题', detailType: 'FORMITEM', name: 'title', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        keywords: new FormItemModel({ caption: '关键字', detailType: 'FORMITEM', name: 'keywords', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        internal: new FormItemModel({ caption: '内部', detailType: 'FORMITEM', name: 'internal', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        statuscode: new FormItemModel({ caption: '状态描述', detailType: 'FORMITEM', name: 'statuscode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        ownerid: new FormItemModel({ caption: '负责人', detailType: 'FORMITEM', name: 'ownerid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        primaryauthorid: new FormItemModel({ caption: '主要作者 ID', detailType: 'FORMITEM', name: 'primaryauthorid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        subjectname: new FormItemModel({ caption: '主题', detailType: 'FORMITEM', name: 'subjectname', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        publishon: new FormItemModel({ caption: '发布日期', detailType: 'FORMITEM', name: 'publishon', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        expirationdate: new FormItemModel({ caption: '到期日期', detailType: 'FORMITEM', name: 'expirationdate', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        content: new FormItemModel({ caption: '内容', detailType: 'FORMITEM', name: 'content', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        description: new FormItemModel({ caption: '说明', detailType: 'FORMITEM', name: 'description', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

        knowledgearticleid: new FormItemModel({ caption: '知识文章', detailType: 'FORMITEM', name: 'knowledgearticleid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, disabled: false, enableCond: 3 }),

    };
}