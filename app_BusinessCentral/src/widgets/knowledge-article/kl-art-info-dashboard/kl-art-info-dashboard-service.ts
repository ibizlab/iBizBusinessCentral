import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * KlArtInfo 部件服务对象
 *
 * @export
 * @class KlArtInfoService
 */
export default class KlArtInfoService extends ControlService {
}