import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * Infotabviewpanel3 部件服务对象
 *
 * @export
 * @class Infotabviewpanel3Service
 */
export default class Infotabviewpanel3Service extends ControlService {
}