import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { Watch, MainControlBase } from '@/studio-core';
import IBizListService from '@/service/ibiz-list/ibiz-list-service';
import View_ListAbsService from './view-list-abs-portlet-service';
import IBizListUIService from '@/uiservice/ibiz-list/ibiz-list-ui-service';
import { Environment } from '@/environments/environment';


/**
 * dashboard_sysportlet1部件基类
 *
 * @export
 * @class MainControlBase
 * @extends {View_ListAbsPortletBase}
 */
export class View_ListAbsPortletBase extends MainControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof View_ListAbsPortletBase
     */
    protected controlType: string = 'PORTLET';

    /**
     * 建构部件服务对象
     *
     * @type {View_ListAbsService}
     * @memberof View_ListAbsPortletBase
     */
    public service: View_ListAbsService = new View_ListAbsService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {IBizListService}
     * @memberof View_ListAbsPortletBase
     */
    public appEntityService: IBizListService = new IBizListService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof View_ListAbsPortletBase
     */
    protected appDeName: string = 'ibizlist';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof View_ListAbsPortletBase
     */
    protected appDeLogicName: string = '市场营销列表';

    /**
     * 界面UI服务对象
     *
     * @type {IBizListUIService}
     * @memberof View_ListAbsBase
     */  
    public appUIService:IBizListUIService = new IBizListUIService(this.$store);

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public dashboard_sysportlet1_ub4e394f_click(params: any = {}, tag?: any, $event?: any) {
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        xData = this;
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:IBizListUIService  = new IBizListUIService();
        curUIService.IBizList_Edit_Abstract(datas,contextJO, paramJO,  $event, xData,this,"IBizList");
    }

    /**
     * 长度
     *
     * @type {number}
     * @memberof View_ListAbs
     */
    @Prop() public height?: number;

    /**
     * 宽度
     *
     * @type {number}
     * @memberof View_ListAbs
     */
    @Prop() public width?: number;



    /**
     * 是否自适应大小
     *
     * @returns {boolean}
     * @memberof View_ListAbsBase
     */
    @Prop({default: false})public isAdaptiveSize!: boolean;

    /**
     * 获取多项数据
     *
     * @returns {any[]}
     * @memberof View_ListAbsBase
     */
    public getDatas(): any[] {
        return [];
    }

    /**
     * 获取单项树
     *
     * @returns {*}
     * @memberof View_ListAbsBase
     */
    public getData(): any {
        return {};
    }

    /**
     * 获取高度
     *
     * @returns {any[]}
     * @memberof View_ListAbsBase
     */
    get getHeight(): any{
        if(!this.$util.isEmpty(this.height) && !this.$util.isNumberNaN(this.height)){
            if(this.height == 0){
                return 'auto';
            } else {
                return this.height+'px';
            }
        } else {
            return '300px';
        }
    }

    /**
     * vue 生命周期
     *
     * @memberof View_ListAbsBase
     */
    public created() {
        this.afterCreated();
    }

    /**
     * 执行created后的逻辑
     *
     *  @memberof View_ListAbsBase
     */    
    public afterCreated(){
        if (this.viewState) {
            this.viewStateEvent = this.viewState.subscribe(({ tag, action, data }) => {
                if (!Object.is(tag, this.name)) {
                    return;
                }
                const refs: any = this.$refs;
                Object.keys(refs).forEach((_name: string) => {
                    this.viewState.next({ tag: _name, action: action, data: data });
                });
            });
        }
    }

    /**
     * vue 生命周期
     *
     * @memberof View_ListAbsBase
     */
    public destroyed() {
        this.afterDestroy();
    }

    /**
     * 执行destroyed后的逻辑
     *
     * @memberof View_ListAbsBase
     */
    public afterDestroy() {
        if (this.viewStateEvent) {
            this.viewStateEvent.unsubscribe();
        }
    }

    /**
     * 执行界面行为
     *
     * @memberof View_ListAbsBase
     */
    public uiAction(tag:string,event:any){
        if(Object.is(tag,'ub4e394f')){
            this.dashboard_sysportlet1_ub4e394f_click(null,tag,event);
        }
    }

}
