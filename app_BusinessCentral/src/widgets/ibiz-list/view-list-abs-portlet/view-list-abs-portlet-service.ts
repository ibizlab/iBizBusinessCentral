import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * View_ListAbs 部件服务对象
 *
 * @export
 * @class View_ListAbsService
 */
export default class View_ListAbsService extends ControlService {
}
