import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import ConnectionRoleService from '@/service/connection-role/connection-role-service';
import ConnectionRoleAuthService from '@/authservice/connection-role/connection-role-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import ConnectionRoleUIService from '@/uiservice/connection-role/connection-role-ui-service';

/**
 * 连接角色选择表格视图视图基类
 *
 * @export
 * @class ConnectionRolePickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class ConnectionRolePickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof ConnectionRolePickupGridViewBase
     */
    protected appDeName: string = 'connectionrole';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof ConnectionRolePickupGridViewBase
     */
    protected appDeKey: string = 'connectionroleid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof ConnectionRolePickupGridViewBase
     */
    protected appDeMajor: string = 'connectionrolename';

    /**
     * 实体服务对象
     *
     * @type {ConnectionRoleService}
     * @memberof ConnectionRolePickupGridViewBase
     */
    protected appEntityService: ConnectionRoleService = new ConnectionRoleService;

    /**
     * 实体权限服务对象
     *
     * @type ConnectionRoleUIService
     * @memberof ConnectionRolePickupGridViewBase
     */
    public appUIService: ConnectionRoleUIService = new ConnectionRoleUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof ConnectionRolePickupGridViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof ConnectionRolePickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.connectionrole.views.pickupgridview.caption',
        srfTitle: 'entities.connectionrole.views.pickupgridview.title',
        srfSubTitle: 'entities.connectionrole.views.pickupgridview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof ConnectionRolePickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: { name: 'grid', type: 'GRID' },
        view_searchform: { name: 'searchform', type: 'SEARCHFORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '243669c525bf93275af379de42863366';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof ConnectionRolePickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof ConnectionRolePickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'connectionrole',
            majorPSDEField: 'connectionrolename',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof ConnectionRolePickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof ConnectionRolePickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof ConnectionRolePickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof ConnectionRolePickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof ConnectionRolePickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof ConnectionRolePickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof ConnectionRolePickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof ConnectionRolePickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}