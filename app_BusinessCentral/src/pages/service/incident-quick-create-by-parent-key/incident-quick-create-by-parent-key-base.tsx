import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { OptionViewBase } from '@/studio-core';
import IncidentService from '@/service/incident/incident-service';
import IncidentAuthService from '@/authservice/incident/incident-auth-service';
import OptionViewEngine from '@engine/view/option-view-engine';
import IncidentUIService from '@/uiservice/incident/incident-ui-service';

/**
 * 快速新建视图基类
 *
 * @export
 * @class IncidentQuickCreateByParentKeyBase
 * @extends {OptionViewBase}
 */
export class IncidentQuickCreateByParentKeyBase extends OptionViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof IncidentQuickCreateByParentKeyBase
     */
    protected appDeName: string = 'incident';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof IncidentQuickCreateByParentKeyBase
     */
    protected appDeKey: string = 'incidentid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof IncidentQuickCreateByParentKeyBase
     */
    protected appDeMajor: string = 'title';

    /**
     * 实体服务对象
     *
     * @type {IncidentService}
     * @memberof IncidentQuickCreateByParentKeyBase
     */
    protected appEntityService: IncidentService = new IncidentService;

    /**
     * 实体权限服务对象
     *
     * @type IncidentUIService
     * @memberof IncidentQuickCreateByParentKeyBase
     */
    public appUIService: IncidentUIService = new IncidentUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof IncidentQuickCreateByParentKeyBase
     */    
    protected counterServiceArray: Array<any> = [];

	/**
	 * 自定义视图导航参数集合
	 *
     * @protected
	 * @type {*}
	 * @memberof IncidentQuickCreateByParentKeyBase
	 */
    protected customViewParams: any = {
        'customertype': { isRawValue: false, value: 'customertype' },
        'customerid': { isRawValue: false, value: 'customerid' },
        'customername': { isRawValue: false, value: 'customername' }
    };

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof IncidentQuickCreateByParentKeyBase
     */
    protected model: any = {
        srfCaption: 'entities.incident.views.quickcreatebyparentkey.caption',
        srfTitle: 'entities.incident.views.quickcreatebyparentkey.title',
        srfSubTitle: 'entities.incident.views.quickcreatebyparentkey.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof IncidentQuickCreateByParentKeyBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '7d8f0a9b8218be48ad238f993d9ecff5';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof IncidentQuickCreateByParentKeyBase
     */
    public engine: OptionViewEngine = new OptionViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof IncidentQuickCreateByParentKeyBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'incident',
            majorPSDEField: 'title',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IncidentQuickCreateByParentKeyBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IncidentQuickCreateByParentKeyBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IncidentQuickCreateByParentKeyBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}