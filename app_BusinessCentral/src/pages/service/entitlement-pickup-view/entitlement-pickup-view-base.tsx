import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import EntitlementService from '@/service/entitlement/entitlement-service';
import EntitlementAuthService from '@/authservice/entitlement/entitlement-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import EntitlementUIService from '@/uiservice/entitlement/entitlement-ui-service';

/**
 * 权利数据选择视图视图基类
 *
 * @export
 * @class EntitlementPickupViewBase
 * @extends {PickupViewBase}
 */
export class EntitlementPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EntitlementPickupViewBase
     */
    protected appDeName: string = 'entitlement';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EntitlementPickupViewBase
     */
    protected appDeKey: string = 'entitlementid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EntitlementPickupViewBase
     */
    protected appDeMajor: string = 'entitlementname';

    /**
     * 实体服务对象
     *
     * @type {EntitlementService}
     * @memberof EntitlementPickupViewBase
     */
    protected appEntityService: EntitlementService = new EntitlementService;

    /**
     * 实体权限服务对象
     *
     * @type EntitlementUIService
     * @memberof EntitlementPickupViewBase
     */
    public appUIService: EntitlementUIService = new EntitlementUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof EntitlementPickupViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EntitlementPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.entitlement.views.pickupview.caption',
        srfTitle: 'entities.entitlement.views.pickupview.title',
        srfSubTitle: 'entities.entitlement.views.pickupview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EntitlementPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: { name: 'pickupviewpanel', type: 'PICKUPVIEWPANEL' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '1ecb36d3d6a944555e7d2d1485844cbf';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EntitlementPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EntitlementPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'entitlement',
            majorPSDEField: 'entitlementname',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EntitlementPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EntitlementPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EntitlementPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}