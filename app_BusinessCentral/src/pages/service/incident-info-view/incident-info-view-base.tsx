import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { TabExpViewBase } from '@/studio-core';
import IncidentService from '@/service/incident/incident-service';
import IncidentAuthService from '@/authservice/incident/incident-auth-service';
import TabExpViewEngine from '@engine/view/tab-exp-view-engine';
import DataPanelEngine from '@engine/ctrl/data-panel-engine';
import IncidentUIService from '@/uiservice/incident/incident-ui-service';

/**
 * 服务案例信息视图基类
 *
 * @export
 * @class IncidentInfoViewBase
 * @extends {TabExpViewBase}
 */
export class IncidentInfoViewBase extends TabExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof IncidentInfoViewBase
     */
    protected appDeName: string = 'incident';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof IncidentInfoViewBase
     */
    protected appDeKey: string = 'incidentid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof IncidentInfoViewBase
     */
    protected appDeMajor: string = 'title';

    /**
     * 实体服务对象
     *
     * @type {IncidentService}
     * @memberof IncidentInfoViewBase
     */
    protected appEntityService: IncidentService = new IncidentService;

    /**
     * 实体权限服务对象
     *
     * @type IncidentUIService
     * @memberof IncidentInfoViewBase
     */
    public appUIService: IncidentUIService = new IncidentUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof IncidentInfoViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof IncidentInfoViewBase
     */
    protected model: any = {
        srfCaption: 'entities.incident.views.infoview.caption',
        srfTitle: 'entities.incident.views.infoview.title',
        srfSubTitle: 'entities.incident.views.infoview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof IncidentInfoViewBase
     */
    protected containerModel: any = {
        view_datapanel: { name: 'datapanel', type: 'FORM' },
        view_toolbar: { name: 'toolbar', type: 'TOOLBAR' },
        view_tabexppanel: { name: 'tabexppanel', type: 'TABEXPPANEL' },
    };

    /**
     * 工具栏模型
     *
     * @type {*}
     * @memberof IncidentInfoView
     */
    public toolBarModels: any = {
        tbitem1_openmaineditview: { name: 'tbitem1_openmaineditview', caption: '编辑', 'isShowCaption': true, 'isShowIcon': true, tooltip: '编辑', iconcls: 'fa fa-edit', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'OpenMainEditView', target: 'SINGLEKEY', class: '' } },

        tbitem1_remove_sep: {  name: 'tbitem1_remove_sep', type: 'SEPERATOR', visabled: true, dataaccaction: '', uiaction: { } },
        tbitem1_remove: { name: 'tbitem1_remove', caption: '删除并关闭', 'isShowCaption': true, 'isShowIcon': true, tooltip: '删除并关闭', iconcls: 'fa fa-remove', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Remove', target: 'SINGLEKEY', class: '' } },

        seperator4: {  name: 'seperator4', type: 'SEPERATOR', visabled: true, dataaccaction: '', uiaction: { } },
        tbitem17_resolve: { name: 'tbitem17_resolve', caption: '解决案例', 'isShowCaption': true, 'isShowIcon': true, tooltip: '解决案例', iconcls: 'fa fa-coffee', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Resolve', target: 'SINGLEKEY', class: '' } },

        tbitem17_cancel_sep: {  name: 'tbitem17_cancel_sep', type: 'SEPERATOR', visabled: true, dataaccaction: '', uiaction: { } },
        tbitem17_cancel: { name: 'tbitem17_cancel', caption: '取消案例', 'isShowCaption': true, 'isShowIcon': true, tooltip: '取消案例', iconcls: 'fa fa-close', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Cancel', target: 'SINGLEKEY', class: '' } },

        tbitem17_active_sep: {  name: 'tbitem17_active_sep', type: 'SEPERATOR', visabled: true, dataaccaction: '', uiaction: { } },
        tbitem17_active: { name: 'tbitem17_active', caption: '重新激活', 'isShowCaption': true, 'isShowIcon': true, tooltip: '重新激活', iconcls: 'fa fa-play-circle-o', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Active', target: 'SINGLEKEY', class: '' } },

        tbitem2: {  name: 'tbitem2', type: 'SEPERATOR', visabled: true, dataaccaction: '', uiaction: { } },
        tbitem12: { name: 'tbitem12', caption: '关闭', 'isShowCaption': true, 'isShowIcon': true, tooltip: '关闭', iconcls: 'fa fa-sign-out', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Exit', target: '', class: '' } },

    };



	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '8284ceb452b00047fe2b937e695a8f32';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof IncidentInfoViewBase
     */
    public engine: TabExpViewEngine = new TabExpViewEngine();

    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof IncidentInfoViewBase
     */
    public datapanel: DataPanelEngine = new DataPanelEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof IncidentInfoViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            keyPSDEField: 'incident',
            majorPSDEField: 'title',
            isLoadDefault: true,
        });
        this.datapanel.init({
            view: this,
            datapanel: this.$refs.datapanel,
            keyPSDEField: 'incident',
            majorPSDEField: 'title',
            isLoadDefault: true,
        });
    }

    /**
     * toolbar 部件 click 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IncidentInfoViewBase
     */
    public toolbar_click($event: any, $event2?: any): void {
        if (Object.is($event.tag, 'tbitem1_openmaineditview')) {
            this.toolbar_tbitem1_openmaineditview_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem1_remove')) {
            this.toolbar_tbitem1_remove_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem17_resolve')) {
            this.toolbar_tbitem17_resolve_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem17_cancel')) {
            this.toolbar_tbitem17_cancel_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem17_active')) {
            this.toolbar_tbitem17_active_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem12')) {
            this.toolbar_tbitem12_click(null, '', $event2);
        }
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem1_openmaineditview_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:IncidentUIService  = new IncidentUIService();
        curUIService.Incident_OpenMainEditView(datas,contextJO, paramJO,  $event, xData,this,"Incident");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem1_remove_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:IncidentUIService  = new IncidentUIService();
        curUIService.Incident_Remove(datas,contextJO, paramJO,  $event, xData,this,"Incident");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem17_resolve_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:IncidentUIService  = new IncidentUIService();
        curUIService.Incident_Resolve(datas,contextJO, paramJO,  $event, xData,this,"Incident");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem17_cancel_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:IncidentUIService  = new IncidentUIService();
        curUIService.Incident_Cancel(datas,contextJO, paramJO,  $event, xData,this,"Incident");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem17_active_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:IncidentUIService  = new IncidentUIService();
        curUIService.Incident_Active(datas,contextJO, paramJO,  $event, xData,this,"Incident");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem12_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        this.Exit(datas, contextJO,paramJO,  $event, xData,this,"Incident");
    }

    /**
     * 关闭
     *
     * @param {any[]} args 当前数据
     * @param {any} contextJO 行为附加上下文
     * @param {*} [params] 附加参数
     * @param {*} [$event] 事件源
     * @param {*} [xData]  执行行为所需当前部件
     * @param {*} [actionContext]  执行行为上下文
     * @memberof IncidentInfoViewBase
     */
    public Exit(args: any[],contextJO?:any, params?: any, $event?: any, xData?: any,actionContext?:any,srfParentDeName?:string) {
        this.closeView(args);
        if(window.parent){
            window.parent.postMessage([{ ...args }],'*');
        }
    }


}