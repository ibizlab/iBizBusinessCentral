import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { TabExpViewBase } from '@/studio-core';
import InvoiceService from '@/service/invoice/invoice-service';
import InvoiceAuthService from '@/authservice/invoice/invoice-auth-service';
import TabExpViewEngine from '@engine/view/tab-exp-view-engine';
import DataPanelEngine from '@engine/ctrl/data-panel-engine';
import InvoiceUIService from '@/uiservice/invoice/invoice-ui-service';

/**
 * 发票信息视图基类
 *
 * @export
 * @class InvoiceInfoBase
 * @extends {TabExpViewBase}
 */
export class InvoiceInfoBase extends TabExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof InvoiceInfoBase
     */
    protected appDeName: string = 'invoice';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof InvoiceInfoBase
     */
    protected appDeKey: string = 'invoiceid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof InvoiceInfoBase
     */
    protected appDeMajor: string = 'invoicename';

    /**
     * 实体服务对象
     *
     * @type {InvoiceService}
     * @memberof InvoiceInfoBase
     */
    protected appEntityService: InvoiceService = new InvoiceService;

    /**
     * 实体权限服务对象
     *
     * @type InvoiceUIService
     * @memberof InvoiceInfoBase
     */
    public appUIService: InvoiceUIService = new InvoiceUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof InvoiceInfoBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof InvoiceInfoBase
     */
    protected model: any = {
        srfCaption: 'entities.invoice.views.info.caption',
        srfTitle: 'entities.invoice.views.info.title',
        srfSubTitle: 'entities.invoice.views.info.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof InvoiceInfoBase
     */
    protected containerModel: any = {
        view_toolbar: { name: 'toolbar', type: 'TOOLBAR' },
        view_datapanel: { name: 'datapanel', type: 'FORM' },
        view_tabexppanel: { name: 'tabexppanel', type: 'TABEXPPANEL' },
    };

    /**
     * 工具栏模型
     *
     * @type {*}
     * @memberof InvoiceInfo
     */
    public toolBarModels: any = {
        tbitem1_openmaineditview: { name: 'tbitem1_openmaineditview', caption: '编辑', 'isShowCaption': true, 'isShowIcon': true, tooltip: '编辑', iconcls: 'fa fa-edit', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'OpenMainEditView', target: 'SINGLEKEY', class: '' } },

        tbitem1_remove_sep: {  name: 'tbitem1_remove_sep', type: 'SEPERATOR', visabled: true, dataaccaction: '', uiaction: { } },
        tbitem1_remove: { name: 'tbitem1_remove', caption: '删除并关闭', 'isShowCaption': true, 'isShowIcon': true, tooltip: '删除并关闭', iconcls: 'fa fa-remove', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Remove', target: 'SINGLEKEY', class: '' } },

        seperator4: {  name: 'seperator4', type: 'SEPERATOR', visabled: true, dataaccaction: '', uiaction: { } },
        tbitem17_finish: { name: 'tbitem17_finish', caption: '确认发票', 'isShowCaption': true, 'isShowIcon': true, tooltip: '确认发票', iconcls: 'fa fa-check', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Finish', target: 'SINGLEKEY', class: '' } },

        tbitem17_cancel_sep: {  name: 'tbitem17_cancel_sep', type: 'SEPERATOR', visabled: true, dataaccaction: '', uiaction: { } },
        tbitem17_cancel: { name: 'tbitem17_cancel', caption: '取消发票', 'isShowCaption': true, 'isShowIcon': true, tooltip: '取消发票', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Cancel', target: 'SINGLEKEY', class: '' } },

        tbitem17_paid_sep: {  name: 'tbitem17_paid_sep', type: 'SEPERATOR', visabled: true, dataaccaction: '', uiaction: { } },
        tbitem17_paid: { name: 'tbitem17_paid', caption: '发票已支付', 'isShowCaption': true, 'isShowIcon': true, tooltip: '发票已支付', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Paid', target: 'SINGLEKEY', class: '' } },

        tbitem2: {  name: 'tbitem2', type: 'SEPERATOR', visabled: true, dataaccaction: '', uiaction: { } },
        tbitem12: { name: 'tbitem12', caption: '关闭', 'isShowCaption': true, 'isShowIcon': true, tooltip: '关闭', iconcls: 'fa fa-sign-out', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Exit', target: '', class: '' } },

    };



	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '6b44326f26c5bd1384a7390bab14d377';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof InvoiceInfoBase
     */
    public engine: TabExpViewEngine = new TabExpViewEngine();

    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof InvoiceInfoBase
     */
    public datapanel: DataPanelEngine = new DataPanelEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof InvoiceInfoBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            keyPSDEField: 'invoice',
            majorPSDEField: 'invoicename',
            isLoadDefault: true,
        });
        this.datapanel.init({
            view: this,
            datapanel: this.$refs.datapanel,
            keyPSDEField: 'invoice',
            majorPSDEField: 'invoicename',
            isLoadDefault: true,
        });
    }

    /**
     * toolbar 部件 click 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof InvoiceInfoBase
     */
    public toolbar_click($event: any, $event2?: any): void {
        if (Object.is($event.tag, 'tbitem1_openmaineditview')) {
            this.toolbar_tbitem1_openmaineditview_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem1_remove')) {
            this.toolbar_tbitem1_remove_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem17_finish')) {
            this.toolbar_tbitem17_finish_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem17_cancel')) {
            this.toolbar_tbitem17_cancel_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem17_paid')) {
            this.toolbar_tbitem17_paid_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem12')) {
            this.toolbar_tbitem12_click(null, '', $event2);
        }
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem1_openmaineditview_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:InvoiceUIService  = new InvoiceUIService();
        curUIService.Invoice_OpenMainEditView(datas,contextJO, paramJO,  $event, xData,this,"Invoice");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem1_remove_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:InvoiceUIService  = new InvoiceUIService();
        curUIService.Invoice_Remove(datas,contextJO, paramJO,  $event, xData,this,"Invoice");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem17_finish_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:InvoiceUIService  = new InvoiceUIService();
        curUIService.Invoice_Finish(datas,contextJO, paramJO,  $event, xData,this,"Invoice");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem17_cancel_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:InvoiceUIService  = new InvoiceUIService();
        curUIService.Invoice_Cancel(datas,contextJO, paramJO,  $event, xData,this,"Invoice");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem17_paid_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:InvoiceUIService  = new InvoiceUIService();
        curUIService.Invoice_Paid(datas,contextJO, paramJO,  $event, xData,this,"Invoice");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem12_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        this.Exit(datas, contextJO,paramJO,  $event, xData,this,"Invoice");
    }

    /**
     * 关闭
     *
     * @param {any[]} args 当前数据
     * @param {any} contextJO 行为附加上下文
     * @param {*} [params] 附加参数
     * @param {*} [$event] 事件源
     * @param {*} [xData]  执行行为所需当前部件
     * @param {*} [actionContext]  执行行为上下文
     * @memberof InvoiceInfoBase
     */
    public Exit(args: any[],contextJO?:any, params?: any, $event?: any, xData?: any,actionContext?:any,srfParentDeName?:string) {
        this.closeView(args);
        if(window.parent){
            window.parent.postMessage([{ ...args }],'*');
        }
    }


}