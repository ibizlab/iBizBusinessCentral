import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { DashboardViewBase } from '@/studio-core';
import InvoiceService from '@/service/invoice/invoice-service';
import InvoiceAuthService from '@/authservice/invoice/invoice-auth-service';
import PortalViewEngine from '@engine/view/portal-view-engine';
import InvoiceUIService from '@/uiservice/invoice/invoice-ui-service';

/**
 * 发票数据看板视图视图基类
 *
 * @export
 * @class InvoiceSummaryViewBase
 * @extends {DashboardViewBase}
 */
export class InvoiceSummaryViewBase extends DashboardViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof InvoiceSummaryViewBase
     */
    protected appDeName: string = 'invoice';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof InvoiceSummaryViewBase
     */
    protected appDeKey: string = 'invoiceid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof InvoiceSummaryViewBase
     */
    protected appDeMajor: string = 'invoicename';

    /**
     * 实体服务对象
     *
     * @type {InvoiceService}
     * @memberof InvoiceSummaryViewBase
     */
    protected appEntityService: InvoiceService = new InvoiceService;

    /**
     * 实体权限服务对象
     *
     * @type InvoiceUIService
     * @memberof InvoiceSummaryViewBase
     */
    public appUIService: InvoiceUIService = new InvoiceUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof InvoiceSummaryViewBase
     */    
    protected counterServiceArray: Array<any> = [];

	/**
	 * 自定义视图导航上下文集合
	 *
     * @protected
	 * @type {*}
	 * @memberof InvoiceSummaryViewBase
	 */
    protected customViewNavContexts: any = {
        'REGARDINGOBJECTID': { isRawValue: false, value: 'invoice' },
        'REGARDINGOBJECTTYPECODE': { isRawValue: true, value: 'INVOICE' }
    };

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof InvoiceSummaryViewBase
     */
    protected model: any = {
        srfCaption: 'entities.invoice.views.summaryview.caption',
        srfTitle: 'entities.invoice.views.summaryview.title',
        srfSubTitle: 'entities.invoice.views.summaryview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof InvoiceSummaryViewBase
     */
    protected containerModel: any = {
        view_dashboard: { name: 'dashboard', type: 'DASHBOARD' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'e2404c0e98672ee6aef616dc57bf8088';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof InvoiceSummaryViewBase
     */
    public engine: PortalViewEngine = new PortalViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof InvoiceSummaryViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            dashboard: this.$refs.dashboard,
            keyPSDEField: 'invoice',
            majorPSDEField: 'invoicename',
            isLoadDefault: true,
        });
    }

    /**
     * dashboard 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof InvoiceSummaryViewBase
     */
    public dashboard_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('dashboard', 'load', $event);
    }


}