import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { OptionViewBase } from '@/studio-core';
import InvoiceService from '@/service/invoice/invoice-service';
import InvoiceAuthService from '@/authservice/invoice/invoice-auth-service';
import OptionViewEngine from '@engine/view/option-view-engine';
import InvoiceUIService from '@/uiservice/invoice/invoice-ui-service';

/**
 * 快速新建视图基类
 *
 * @export
 * @class InvoiceQuickCreateViewBase
 * @extends {OptionViewBase}
 */
export class InvoiceQuickCreateViewBase extends OptionViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof InvoiceQuickCreateViewBase
     */
    protected appDeName: string = 'invoice';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof InvoiceQuickCreateViewBase
     */
    protected appDeKey: string = 'invoiceid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof InvoiceQuickCreateViewBase
     */
    protected appDeMajor: string = 'invoicename';

    /**
     * 实体服务对象
     *
     * @type {InvoiceService}
     * @memberof InvoiceQuickCreateViewBase
     */
    protected appEntityService: InvoiceService = new InvoiceService;

    /**
     * 实体权限服务对象
     *
     * @type InvoiceUIService
     * @memberof InvoiceQuickCreateViewBase
     */
    public appUIService: InvoiceUIService = new InvoiceUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof InvoiceQuickCreateViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof InvoiceQuickCreateViewBase
     */
    protected model: any = {
        srfCaption: 'entities.invoice.views.quickcreateview.caption',
        srfTitle: 'entities.invoice.views.quickcreateview.title',
        srfSubTitle: 'entities.invoice.views.quickcreateview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof InvoiceQuickCreateViewBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'bfeeef2051040fe1fc981e5547d51245';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof InvoiceQuickCreateViewBase
     */
    public engine: OptionViewEngine = new OptionViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof InvoiceQuickCreateViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'invoice',
            majorPSDEField: 'invoicename',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof InvoiceQuickCreateViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof InvoiceQuickCreateViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof InvoiceQuickCreateViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}