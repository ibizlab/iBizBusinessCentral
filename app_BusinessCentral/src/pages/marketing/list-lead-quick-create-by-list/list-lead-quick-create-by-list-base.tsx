import { Subject } from 'rxjs';
import { OptionViewBase } from '@/studio-core';
import ListLeadService from '@/service/list-lead/list-lead-service';
import ListLeadAuthService from '@/authservice/list-lead/list-lead-auth-service';
import OptionViewEngine from '@engine/view/option-view-engine';
import ListLeadUIService from '@/uiservice/list-lead/list-lead-ui-service';

/**
 * 快速新建（根据营销列表）视图基类
 *
 * @export
 * @class ListLeadQuickCreateByListBase
 * @extends {OptionViewBase}
 */
export class ListLeadQuickCreateByListBase extends OptionViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof ListLeadQuickCreateByListBase
     */
    protected appDeName: string = 'listlead';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof ListLeadQuickCreateByListBase
     */
    protected appDeKey: string = 'relationshipsid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof ListLeadQuickCreateByListBase
     */
    protected appDeMajor: string = 'relationshipsname';

    /**
     * 实体服务对象
     *
     * @type {ListLeadService}
     * @memberof ListLeadQuickCreateByListBase
     */
    protected appEntityService: ListLeadService = new ListLeadService;

    /**
     * 实体权限服务对象
     *
     * @type ListLeadUIService
     * @memberof ListLeadQuickCreateByListBase
     */
    public appUIService: ListLeadUIService = new ListLeadUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof ListLeadQuickCreateByListBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof ListLeadQuickCreateByListBase
     */
    protected model: any = {
        srfCaption: 'entities.listlead.views.quickcreatebylist.caption',
        srfTitle: 'entities.listlead.views.quickcreatebylist.title',
        srfSubTitle: 'entities.listlead.views.quickcreatebylist.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof ListLeadQuickCreateByListBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '69f26cbe2194badbf0297d7ecca5d455';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof ListLeadQuickCreateByListBase
     */
    public engine: OptionViewEngine = new OptionViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof ListLeadQuickCreateByListBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'listlead',
            majorPSDEField: 'relationshipsname',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof ListLeadQuickCreateByListBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof ListLeadQuickCreateByListBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof ListLeadQuickCreateByListBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}