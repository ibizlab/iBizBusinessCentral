import { Subject } from 'rxjs';
import { OptionViewBase } from '@/studio-core';
import ListAccountService from '@/service/list-account/list-account-service';
import ListAccountAuthService from '@/authservice/list-account/list-account-auth-service';
import OptionViewEngine from '@engine/view/option-view-engine';
import ListAccountUIService from '@/uiservice/list-account/list-account-ui-service';

/**
 * 查找客户视图基类
 *
 * @export
 * @class ListAccountQuickCreateByListBase
 * @extends {OptionViewBase}
 */
export class ListAccountQuickCreateByListBase extends OptionViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof ListAccountQuickCreateByListBase
     */
    protected appDeName: string = 'listaccount';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof ListAccountQuickCreateByListBase
     */
    protected appDeKey: string = 'relationshipsid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof ListAccountQuickCreateByListBase
     */
    protected appDeMajor: string = 'relationshipsname';

    /**
     * 实体服务对象
     *
     * @type {ListAccountService}
     * @memberof ListAccountQuickCreateByListBase
     */
    protected appEntityService: ListAccountService = new ListAccountService;

    /**
     * 实体权限服务对象
     *
     * @type ListAccountUIService
     * @memberof ListAccountQuickCreateByListBase
     */
    public appUIService: ListAccountUIService = new ListAccountUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof ListAccountQuickCreateByListBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof ListAccountQuickCreateByListBase
     */
    protected model: any = {
        srfCaption: 'entities.listaccount.views.quickcreatebylist.caption',
        srfTitle: 'entities.listaccount.views.quickcreatebylist.title',
        srfSubTitle: 'entities.listaccount.views.quickcreatebylist.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof ListAccountQuickCreateByListBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '4ef2436b7ea04c193b715aa1f6327e7e';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof ListAccountQuickCreateByListBase
     */
    public engine: OptionViewEngine = new OptionViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof ListAccountQuickCreateByListBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'listaccount',
            majorPSDEField: 'relationshipsname',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof ListAccountQuickCreateByListBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof ListAccountQuickCreateByListBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof ListAccountQuickCreateByListBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}