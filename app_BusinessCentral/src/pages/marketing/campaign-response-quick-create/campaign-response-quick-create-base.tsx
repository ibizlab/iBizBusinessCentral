import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { OptionViewBase } from '@/studio-core';
import CampaignResponseService from '@/service/campaign-response/campaign-response-service';
import CampaignResponseAuthService from '@/authservice/campaign-response/campaign-response-auth-service';
import OptionViewEngine from '@engine/view/option-view-engine';
import CampaignResponseUIService from '@/uiservice/campaign-response/campaign-response-ui-service';

/**
 * 快速新建视图基类
 *
 * @export
 * @class CampaignResponseQuickCreateBase
 * @extends {OptionViewBase}
 */
export class CampaignResponseQuickCreateBase extends OptionViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof CampaignResponseQuickCreateBase
     */
    protected appDeName: string = 'campaignresponse';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof CampaignResponseQuickCreateBase
     */
    protected appDeKey: string = 'activityid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof CampaignResponseQuickCreateBase
     */
    protected appDeMajor: string = 'subject';

    /**
     * 实体服务对象
     *
     * @type {CampaignResponseService}
     * @memberof CampaignResponseQuickCreateBase
     */
    protected appEntityService: CampaignResponseService = new CampaignResponseService;

    /**
     * 实体权限服务对象
     *
     * @type CampaignResponseUIService
     * @memberof CampaignResponseQuickCreateBase
     */
    public appUIService: CampaignResponseUIService = new CampaignResponseUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof CampaignResponseQuickCreateBase
     */    
    protected counterServiceArray: Array<any> = [];

	/**
	 * 自定义视图导航参数集合
	 *
     * @protected
	 * @type {*}
	 * @memberof CampaignResponseQuickCreateBase
	 */
    protected customViewParams: any = {
        'regardingobjectid': { isRawValue: false, value: 'campaign' }
    };

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof CampaignResponseQuickCreateBase
     */
    protected model: any = {
        srfCaption: 'entities.campaignresponse.views.quickcreate.caption',
        srfTitle: 'entities.campaignresponse.views.quickcreate.title',
        srfSubTitle: 'entities.campaignresponse.views.quickcreate.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof CampaignResponseQuickCreateBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '4c6495f1346f9062a924454886f5b3e1';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof CampaignResponseQuickCreateBase
     */
    public engine: OptionViewEngine = new OptionViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof CampaignResponseQuickCreateBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'campaignresponse',
            majorPSDEField: 'subject',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof CampaignResponseQuickCreateBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof CampaignResponseQuickCreateBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof CampaignResponseQuickCreateBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}