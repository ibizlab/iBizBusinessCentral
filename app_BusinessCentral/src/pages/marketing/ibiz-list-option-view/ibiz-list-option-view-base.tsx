import { Subject } from 'rxjs';
import { OptionViewBase } from '@/studio-core';
import IBizListService from '@/service/ibiz-list/ibiz-list-service';
import IBizListAuthService from '@/authservice/ibiz-list/ibiz-list-auth-service';
import OptionViewEngine from '@engine/view/option-view-engine';
import IBizListUIService from '@/uiservice/ibiz-list/ibiz-list-ui-service';

/**
 * 市场营销列表选项操作视图视图基类
 *
 * @export
 * @class IBizListOptionViewBase
 * @extends {OptionViewBase}
 */
export class IBizListOptionViewBase extends OptionViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof IBizListOptionViewBase
     */
    protected appDeName: string = 'ibizlist';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof IBizListOptionViewBase
     */
    protected appDeKey: string = 'listid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof IBizListOptionViewBase
     */
    protected appDeMajor: string = 'listname';

    /**
     * 实体服务对象
     *
     * @type {IBizListService}
     * @memberof IBizListOptionViewBase
     */
    protected appEntityService: IBizListService = new IBizListService;

    /**
     * 实体权限服务对象
     *
     * @type IBizListUIService
     * @memberof IBizListOptionViewBase
     */
    public appUIService: IBizListUIService = new IBizListUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof IBizListOptionViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof IBizListOptionViewBase
     */
    protected model: any = {
        srfCaption: 'entities.ibizlist.views.optionview.caption',
        srfTitle: 'entities.ibizlist.views.optionview.title',
        srfSubTitle: 'entities.ibizlist.views.optionview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof IBizListOptionViewBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'bc4a36fb63b9ac07a68274a059867ae2';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof IBizListOptionViewBase
     */
    public engine: OptionViewEngine = new OptionViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof IBizListOptionViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'ibizlist',
            majorPSDEField: 'listname',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBizListOptionViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBizListOptionViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBizListOptionViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}