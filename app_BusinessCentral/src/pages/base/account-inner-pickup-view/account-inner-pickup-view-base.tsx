import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import AccountService from '@/service/account/account-service';
import AccountAuthService from '@/authservice/account/account-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import AccountUIService from '@/uiservice/account/account-ui-service';

/**
 * 客户数据选择视图视图基类
 *
 * @export
 * @class AccountInnerPickupViewBase
 * @extends {PickupViewBase}
 */
export class AccountInnerPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof AccountInnerPickupViewBase
     */
    protected appDeName: string = 'account';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof AccountInnerPickupViewBase
     */
    protected appDeKey: string = 'accountid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof AccountInnerPickupViewBase
     */
    protected appDeMajor: string = 'accountname';

    /**
     * 实体服务对象
     *
     * @type {AccountService}
     * @memberof AccountInnerPickupViewBase
     */
    protected appEntityService: AccountService = new AccountService;

    /**
     * 实体权限服务对象
     *
     * @type AccountUIService
     * @memberof AccountInnerPickupViewBase
     */
    public appUIService: AccountUIService = new AccountUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof AccountInnerPickupViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof AccountInnerPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.account.views.innerpickupview.caption',
        srfTitle: 'entities.account.views.innerpickupview.title',
        srfSubTitle: 'entities.account.views.innerpickupview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof AccountInnerPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: { name: 'pickupviewpanel', type: 'PICKUPVIEWPANEL' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '7a0f54535d759023c89093ed23719dba';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof AccountInnerPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof AccountInnerPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'account',
            majorPSDEField: 'accountname',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof AccountInnerPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof AccountInnerPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof AccountInnerPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}