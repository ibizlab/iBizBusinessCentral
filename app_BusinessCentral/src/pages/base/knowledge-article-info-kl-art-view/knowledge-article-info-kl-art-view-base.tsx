import { Subject } from 'rxjs';
import { EditViewBase } from '@/studio-core';
import KnowledgeArticleService from '@/service/knowledge-article/knowledge-article-service';
import KnowledgeArticleAuthService from '@/authservice/knowledge-article/knowledge-article-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import KnowledgeArticleUIService from '@/uiservice/knowledge-article/knowledge-article-ui-service';

/**
 * 知识文章信息视图视图基类
 *
 * @export
 * @class KnowledgeArticleInfo_KlArtViewBase
 * @extends {EditViewBase}
 */
export class KnowledgeArticleInfo_KlArtViewBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof KnowledgeArticleInfo_KlArtViewBase
     */
    protected appDeName: string = 'knowledgearticle';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof KnowledgeArticleInfo_KlArtViewBase
     */
    protected appDeKey: string = 'knowledgearticleid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof KnowledgeArticleInfo_KlArtViewBase
     */
    protected appDeMajor: string = 'title';

    /**
     * 实体服务对象
     *
     * @type {KnowledgeArticleService}
     * @memberof KnowledgeArticleInfo_KlArtViewBase
     */
    protected appEntityService: KnowledgeArticleService = new KnowledgeArticleService;

    /**
     * 实体权限服务对象
     *
     * @type KnowledgeArticleUIService
     * @memberof KnowledgeArticleInfo_KlArtViewBase
     */
    public appUIService: KnowledgeArticleUIService = new KnowledgeArticleUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof KnowledgeArticleInfo_KlArtViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof KnowledgeArticleInfo_KlArtViewBase
     */
    protected model: any = {
        srfCaption: 'entities.knowledgearticle.views.info_klartview.caption',
        srfTitle: 'entities.knowledgearticle.views.info_klartview.title',
        srfSubTitle: 'entities.knowledgearticle.views.info_klartview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof KnowledgeArticleInfo_KlArtViewBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '157ecd1a00739685ea7781f954188f07';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof KnowledgeArticleInfo_KlArtViewBase
     */
    public engine: EditViewEngine = new EditViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof KnowledgeArticleInfo_KlArtViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'knowledgearticle',
            majorPSDEField: 'title',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof KnowledgeArticleInfo_KlArtViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof KnowledgeArticleInfo_KlArtViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof KnowledgeArticleInfo_KlArtViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}