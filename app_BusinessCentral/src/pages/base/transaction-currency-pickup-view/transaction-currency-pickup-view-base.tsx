import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import TransactionCurrencyService from '@/service/transaction-currency/transaction-currency-service';
import TransactionCurrencyAuthService from '@/authservice/transaction-currency/transaction-currency-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import TransactionCurrencyUIService from '@/uiservice/transaction-currency/transaction-currency-ui-service';

/**
 * 货币数据选择视图视图基类
 *
 * @export
 * @class TransactionCurrencyPickupViewBase
 * @extends {PickupViewBase}
 */
export class TransactionCurrencyPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof TransactionCurrencyPickupViewBase
     */
    protected appDeName: string = 'transactioncurrency';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof TransactionCurrencyPickupViewBase
     */
    protected appDeKey: string = 'transactioncurrencyid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof TransactionCurrencyPickupViewBase
     */
    protected appDeMajor: string = 'currencyname';

    /**
     * 实体服务对象
     *
     * @type {TransactionCurrencyService}
     * @memberof TransactionCurrencyPickupViewBase
     */
    protected appEntityService: TransactionCurrencyService = new TransactionCurrencyService;

    /**
     * 实体权限服务对象
     *
     * @type TransactionCurrencyUIService
     * @memberof TransactionCurrencyPickupViewBase
     */
    public appUIService: TransactionCurrencyUIService = new TransactionCurrencyUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof TransactionCurrencyPickupViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof TransactionCurrencyPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.transactioncurrency.views.pickupview.caption',
        srfTitle: 'entities.transactioncurrency.views.pickupview.title',
        srfSubTitle: 'entities.transactioncurrency.views.pickupview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof TransactionCurrencyPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: { name: 'pickupviewpanel', type: 'PICKUPVIEWPANEL' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '914c752552fd5f1ff89ad69961b04c29';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof TransactionCurrencyPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof TransactionCurrencyPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'transactioncurrency',
            majorPSDEField: 'currencyname',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof TransactionCurrencyPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof TransactionCurrencyPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof TransactionCurrencyPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}