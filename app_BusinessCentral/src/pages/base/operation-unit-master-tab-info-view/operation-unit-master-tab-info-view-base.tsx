import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { TabExpViewBase } from '@/studio-core';
import OperationUnitService from '@/service/operation-unit/operation-unit-service';
import OperationUnitAuthService from '@/authservice/operation-unit/operation-unit-auth-service';
import TabExpViewEngine from '@engine/view/tab-exp-view-engine';
import OperationUnitUIService from '@/uiservice/operation-unit/operation-unit-ui-service';

/**
 * 主信息总览视图视图基类
 *
 * @export
 * @class OperationUnitMasterTabInfoViewBase
 * @extends {TabExpViewBase}
 */
export class OperationUnitMasterTabInfoViewBase extends TabExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof OperationUnitMasterTabInfoViewBase
     */
    protected appDeName: string = 'operationunit';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof OperationUnitMasterTabInfoViewBase
     */
    protected appDeKey: string = 'operationunitid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof OperationUnitMasterTabInfoViewBase
     */
    protected appDeMajor: string = 'operationunitname';

    /**
     * 实体服务对象
     *
     * @type {OperationUnitService}
     * @memberof OperationUnitMasterTabInfoViewBase
     */
    protected appEntityService: OperationUnitService = new OperationUnitService;

    /**
     * 实体权限服务对象
     *
     * @type OperationUnitUIService
     * @memberof OperationUnitMasterTabInfoViewBase
     */
    public appUIService: OperationUnitUIService = new OperationUnitUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof OperationUnitMasterTabInfoViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof OperationUnitMasterTabInfoViewBase
     */
    protected model: any = {
        srfCaption: 'entities.operationunit.views.mastertabinfoview.caption',
        srfTitle: 'entities.operationunit.views.mastertabinfoview.title',
        srfSubTitle: 'entities.operationunit.views.mastertabinfoview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof OperationUnitMasterTabInfoViewBase
     */
    protected containerModel: any = {
        view_tabexppanel: { name: 'tabexppanel', type: 'TABEXPPANEL' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '2d4ec7cc0257c490f3536b96d36ec431';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof OperationUnitMasterTabInfoViewBase
     */
    public engine: TabExpViewEngine = new TabExpViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof OperationUnitMasterTabInfoViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            keyPSDEField: 'operationunit',
            majorPSDEField: 'operationunitname',
            isLoadDefault: true,
        });
    }


}