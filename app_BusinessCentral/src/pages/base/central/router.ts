import Vue from 'vue';
import Router from 'vue-router';
import { AuthGuard } from '@/utils';
import qs from 'qs';
import { globalRoutes, indexRoutes} from '@/router'
import { AppService } from '@/studio-core/service/app-service/AppService';

Vue.use(Router);

const appService = new AppService();

const router = new Router({
    routes: [
        {
            path: '/central/:central?',
            beforeEnter: async (to: any, from: any, next: any) => {
                const routerParamsName = 'central';
                const params: any = {};
                if (to.params && to.params[routerParamsName]) {
                    params[routerParamsName] = to.params[routerParamsName];
                }
                const url: string = '/appdata';
                await AuthGuard.getInstance().authGuard(url, params, router);
                appService.navHistory.indexMeta = {
                    caption: 'app.views.central.title',
                    info:'',
                    viewType: 'APPINDEX',
                    parameters: [
                        { pathName: 'central', parameterName: 'central' },
                    ],
                    requireAuth: true,
                };
                next();
            },
            meta: {  
                caption: 'app.views.central.title',
                info:'',
                viewType: 'APPINDEX',
                parameters: [
                    { pathName: 'central', parameterName: 'central' },
                ],
                requireAuth: true,
            },
            component: () => import('@pages/base/central/central.vue'),
            children: [
                {
                    path: 'employees/:employee?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.employee.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'employees', parameterName: 'employee' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/human-resource/employee-grid-view/employee-grid-view.vue'),
                },
                {
                    path: 'ibzorganizations/:ibzorganization?/pickupview/:pickupview?',
                    meta: {
                        caption: 'entities.ibzorganization.views.pickupview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzorganizations', parameterName: 'ibzorganization' },
                            { pathName: 'pickupview', parameterName: 'pickupview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzorganization-pickup-view/ibzorganization-pickup-view.vue'),
                },
                {
                    path: 'employees/:employee?/masterquickview/:masterquickview?',
                    meta: {
                        caption: 'entities.employee.views.masterquickview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'employees', parameterName: 'employee' },
                            { pathName: 'masterquickview', parameterName: 'masterquickview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/human-resource/employee-master-quick-view/employee-master-quick-view.vue'),
                },
                {
                    path: 'legals/:legal?/masterquickview/:masterquickview?',
                    meta: {
                        caption: 'entities.legal.views.masterquickview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'legals', parameterName: 'legal' },
                            { pathName: 'masterquickview', parameterName: 'masterquickview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/base/legal-master-quick-view/legal-master-quick-view.vue'),
                },
                {
                    path: 'territories/:territory?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.territory.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'territories', parameterName: 'territory' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/base/territory-grid-view/territory-grid-view.vue'),
                },
                {
                    path: 'ibzorganizations/:ibzorganization?/treeexpview/:treeexpview?',
                    meta: {
                        caption: 'entities.ibzorganization.views.treeexpview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzorganizations', parameterName: 'ibzorganization' },
                            { pathName: 'treeexpview', parameterName: 'treeexpview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzorganization-tree-exp-view/ibzorganization-tree-exp-view.vue'),
                },
                {
                    path: 'wfgroups/:wfgroup?/pickupgridview/:pickupgridview?',
                    meta: {
                        caption: 'entities.wfgroup.views.pickupgridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'wfgroups', parameterName: 'wfgroup' },
                            { pathName: 'pickupgridview', parameterName: 'pickupgridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/workflow/wfgroup-pickup-grid-view/wfgroup-pickup-grid-view.vue'),
                },
                {
                    path: 'metrics/:metric?/editview/:editview?',
                    meta: {
                        caption: 'entities.metric.views.editview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'metrics', parameterName: 'metric' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/base/metric-edit-view/metric-edit-view.vue'),
                },
                {
                    path: 'omhierarchycats/:omhierarchycat?/editview/:editview?',
                    meta: {
                        caption: 'entities.omhierarchycat.views.editview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'omhierarchycats', parameterName: 'omhierarchycat' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/base/omhierarchy-cat-edit-view/omhierarchy-cat-edit-view.vue'),
                },
                {
                    path: 'omhierarchycats/:omhierarchycat?/dashboardview/:dashboardview?',
                    meta: {
                        caption: 'entities.omhierarchycat.views.dashboardview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'omhierarchycats', parameterName: 'omhierarchycat' },
                            { pathName: 'dashboardview', parameterName: 'dashboardview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/base/omhierarchy-cat-dashboard-view/omhierarchy-cat-dashboard-view.vue'),
                },
                {
                    path: 'wfgroups/:wfgroup?/editview/:editview?',
                    meta: {
                        caption: 'entities.wfgroup.views.editview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'wfgroups', parameterName: 'wfgroup' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/workflow/wfgroup-edit-view/wfgroup-edit-view.vue'),
                },
                {
                    path: 'ibzorganizations/:ibzorganization?/ibzdepartments/:ibzdepartment?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.ibzdepartment.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzorganizations', parameterName: 'ibzorganization' },
                            { pathName: 'ibzdepartments', parameterName: 'ibzdepartment' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzdepartment-grid-view/ibzdepartment-grid-view.vue'),
                },
                {
                    path: 'ibzdepartments/:ibzdepartment?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.ibzdepartment.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzdepartments', parameterName: 'ibzdepartment' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzdepartment-grid-view/ibzdepartment-grid-view.vue'),
                },
                {
                    path: 'sysapps/:sysapp?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.sysapp.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'sysapps', parameterName: 'sysapp' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/uaa/sys-app-grid-view/sys-app-grid-view.vue'),
                },
                {
                    path: 'uoms/:uom?/editview/:editview?',
                    meta: {
                        caption: 'entities.uom.views.editview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'uoms', parameterName: 'uom' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/base/uom-edit-view/uom-edit-view.vue'),
                },
                {
                    path: 'wfusers/:wfuser?/wfmembers/:wfmember?/editview/:editview?',
                    meta: {
                        caption: 'entities.wfmember.views.editview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'wfusers', parameterName: 'wfuser' },
                            { pathName: 'wfmembers', parameterName: 'wfmember' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/workflow/wfmember-edit-view/wfmember-edit-view.vue'),
                },
                {
                    path: 'wfgroups/:wfgroup?/wfmembers/:wfmember?/editview/:editview?',
                    meta: {
                        caption: 'entities.wfmember.views.editview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'wfgroups', parameterName: 'wfgroup' },
                            { pathName: 'wfmembers', parameterName: 'wfmember' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/workflow/wfmember-edit-view/wfmember-edit-view.vue'),
                },
                {
                    path: 'wfmembers/:wfmember?/editview/:editview?',
                    meta: {
                        caption: 'entities.wfmember.views.editview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'wfmembers', parameterName: 'wfmember' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/workflow/wfmember-edit-view/wfmember-edit-view.vue'),
                },
                {
                    path: 'ibzorganizations/:ibzorganization?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.ibzorganization.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzorganizations', parameterName: 'ibzorganization' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzorganization-grid-view/ibzorganization-grid-view.vue'),
                },
                {
                    path: 'sysusers/:sysuser?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.sysuser.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'sysusers', parameterName: 'sysuser' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/uaa/sys-user-grid-view/sys-user-grid-view.vue'),
                },
                {
                    path: 'sysusers/:sysuser?/sysuserroles/:sysuserrole?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.sysuserrole.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'sysusers', parameterName: 'sysuser' },
                            { pathName: 'sysuserroles', parameterName: 'sysuserrole' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/uaa/sys-user-role-grid-view/sys-user-role-grid-view.vue'),
                },
                {
                    path: 'sysroles/:sysrole?/sysuserroles/:sysuserrole?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.sysuserrole.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'sysroles', parameterName: 'sysrole' },
                            { pathName: 'sysuserroles', parameterName: 'sysuserrole' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/uaa/sys-user-role-grid-view/sys-user-role-grid-view.vue'),
                },
                {
                    path: 'sysuserroles/:sysuserrole?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.sysuserrole.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'sysuserroles', parameterName: 'sysuserrole' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/uaa/sys-user-role-grid-view/sys-user-role-grid-view.vue'),
                },
                {
                    path: 'operationunits/:operationunit?/mastertabinfoview/:mastertabinfoview?',
                    meta: {
                        caption: 'entities.operationunit.views.mastertabinfoview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'operationunits', parameterName: 'operationunit' },
                            { pathName: 'mastertabinfoview', parameterName: 'mastertabinfoview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/base/operation-unit-master-tab-info-view/operation-unit-master-tab-info-view.vue'),
                },
                {
                    path: 'ibzorganizations/:ibzorganization?/ibzdepartments/:ibzdepartment?/ibzemployees/:ibzemployee?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.ibzemployee.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzorganizations', parameterName: 'ibzorganization' },
                            { pathName: 'ibzdepartments', parameterName: 'ibzdepartment' },
                            { pathName: 'ibzemployees', parameterName: 'ibzemployee' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzemployee-grid-view/ibzemployee-grid-view.vue'),
                },
                {
                    path: 'ibzorganizations/:ibzorganization?/ibzemployees/:ibzemployee?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.ibzemployee.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzorganizations', parameterName: 'ibzorganization' },
                            { pathName: 'ibzemployees', parameterName: 'ibzemployee' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzemployee-grid-view/ibzemployee-grid-view.vue'),
                },
                {
                    path: 'ibzdepartments/:ibzdepartment?/ibzemployees/:ibzemployee?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.ibzemployee.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzdepartments', parameterName: 'ibzdepartment' },
                            { pathName: 'ibzemployees', parameterName: 'ibzemployee' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzemployee-grid-view/ibzemployee-grid-view.vue'),
                },
                {
                    path: 'ibzemployees/:ibzemployee?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.ibzemployee.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzemployees', parameterName: 'ibzemployee' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzemployee-grid-view/ibzemployee-grid-view.vue'),
                },
                {
                    path: 'operationunits/:operationunit?/deptmasterquickview/:deptmasterquickview?',
                    meta: {
                        caption: 'entities.operationunit.views.deptmasterquickview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'operationunits', parameterName: 'operationunit' },
                            { pathName: 'deptmasterquickview', parameterName: 'deptmasterquickview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/base/operation-unit-dept-master-quick-view/operation-unit-dept-master-quick-view.vue'),
                },
                {
                    path: 'uoms/:uom?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.uom.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'uoms', parameterName: 'uom' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/base/uom-grid-view/uom-grid-view.vue'),
                },
                {
                    path: 'transactioncurrencies/:transactioncurrency?/pickupgridview/:pickupgridview?',
                    meta: {
                        caption: 'entities.transactioncurrency.views.pickupgridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'transactioncurrencies', parameterName: 'transactioncurrency' },
                            { pathName: 'pickupgridview', parameterName: 'pickupgridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/base/transaction-currency-pickup-grid-view/transaction-currency-pickup-grid-view.vue'),
                },
                {
                    path: 'operationunits/:operationunit?/omhierarchies/:omhierarchy?/treeexpview/:treeexpview?',
                    meta: {
                        caption: 'entities.omhierarchy.views.treeexpview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'operationunits', parameterName: 'operationunit' },
                            { pathName: 'omhierarchies', parameterName: 'omhierarchy' },
                            { pathName: 'treeexpview', parameterName: 'treeexpview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/base/omhierarchy-tree-exp-view/omhierarchy-tree-exp-view.vue'),
                },
                {
                    path: 'omhierarchycats/:omhierarchycat?/omhierarchies/:omhierarchy?/treeexpview/:treeexpview?',
                    meta: {
                        caption: 'entities.omhierarchy.views.treeexpview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'omhierarchycats', parameterName: 'omhierarchycat' },
                            { pathName: 'omhierarchies', parameterName: 'omhierarchy' },
                            { pathName: 'treeexpview', parameterName: 'treeexpview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/base/omhierarchy-tree-exp-view/omhierarchy-tree-exp-view.vue'),
                },
                {
                    path: 'legals/:legal?/omhierarchies/:omhierarchy?/treeexpview/:treeexpview?',
                    meta: {
                        caption: 'entities.omhierarchy.views.treeexpview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'legals', parameterName: 'legal' },
                            { pathName: 'omhierarchies', parameterName: 'omhierarchy' },
                            { pathName: 'treeexpview', parameterName: 'treeexpview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/base/omhierarchy-tree-exp-view/omhierarchy-tree-exp-view.vue'),
                },
                {
                    path: 'omhierarchies/:omhierarchy?/treeexpview/:treeexpview?',
                    meta: {
                        caption: 'entities.omhierarchy.views.treeexpview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'omhierarchies', parameterName: 'omhierarchy' },
                            { pathName: 'treeexpview', parameterName: 'treeexpview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/base/omhierarchy-tree-exp-view/omhierarchy-tree-exp-view.vue'),
                },
                {
                    path: 'operationunits/:operationunit?/masterinfoview/:masterinfoview?',
                    meta: {
                        caption: 'entities.operationunit.views.masterinfoview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'operationunits', parameterName: 'operationunit' },
                            { pathName: 'masterinfoview', parameterName: 'masterinfoview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/base/operation-unit-master-info-view/operation-unit-master-info-view.vue'),
                },
                {
                    path: 'ibzposts/:ibzpost?/pickupgridview/:pickupgridview?',
                    meta: {
                        caption: 'entities.ibzpost.views.pickupgridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzposts', parameterName: 'ibzpost' },
                            { pathName: 'pickupgridview', parameterName: 'pickupgridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzpost-pickup-grid-view/ibzpost-pickup-grid-view.vue'),
                },
                {
                    path: 'omhierarchycats/:omhierarchycat?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.omhierarchycat.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'omhierarchycats', parameterName: 'omhierarchycat' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/base/omhierarchy-cat-grid-view/omhierarchy-cat-grid-view.vue'),
                },
                {
                    path: 'sysusers/:sysuser?/sysuserroles/:sysuserrole?/editview/:editview?',
                    meta: {
                        caption: 'entities.sysuserrole.views.editview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'sysusers', parameterName: 'sysuser' },
                            { pathName: 'sysuserroles', parameterName: 'sysuserrole' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/uaa/sys-user-role-edit-view/sys-user-role-edit-view.vue'),
                },
                {
                    path: 'sysroles/:sysrole?/sysuserroles/:sysuserrole?/editview/:editview?',
                    meta: {
                        caption: 'entities.sysuserrole.views.editview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'sysroles', parameterName: 'sysrole' },
                            { pathName: 'sysuserroles', parameterName: 'sysuserrole' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/uaa/sys-user-role-edit-view/sys-user-role-edit-view.vue'),
                },
                {
                    path: 'sysuserroles/:sysuserrole?/editview/:editview?',
                    meta: {
                        caption: 'entities.sysuserrole.views.editview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'sysuserroles', parameterName: 'sysuserrole' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/uaa/sys-user-role-edit-view/sys-user-role-edit-view.vue'),
                },
                {
                    path: 'connectionroles/:connectionrole?/editview/:editview?',
                    meta: {
                        caption: 'entities.connectionrole.views.editview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'connectionroles', parameterName: 'connectionrole' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/runtime/connection-role-edit-view/connection-role-edit-view.vue'),
                },
                {
                    path: 'wfusers/:wfuser?/pickupgridview/:pickupgridview?',
                    meta: {
                        caption: 'entities.wfuser.views.pickupgridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'wfusers', parameterName: 'wfuser' },
                            { pathName: 'pickupgridview', parameterName: 'pickupgridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/workflow/wfuser-pickup-grid-view/wfuser-pickup-grid-view.vue'),
                },
                {
                    path: 'sysroles/:sysrole?/pickupgridview/:pickupgridview?',
                    meta: {
                        caption: 'entities.sysrole.views.pickupgridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'sysroles', parameterName: 'sysrole' },
                            { pathName: 'pickupgridview', parameterName: 'pickupgridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/uaa/sys-role-pickup-grid-view/sys-role-pickup-grid-view.vue'),
                },
                {
                    path: 'ibzorganizations/:ibzorganization?/ibzdepartments/:ibzdepartment?/pickupgridview/:pickupgridview?',
                    meta: {
                        caption: 'entities.ibzdepartment.views.pickupgridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzorganizations', parameterName: 'ibzorganization' },
                            { pathName: 'ibzdepartments', parameterName: 'ibzdepartment' },
                            { pathName: 'pickupgridview', parameterName: 'pickupgridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzdepartment-pickup-grid-view/ibzdepartment-pickup-grid-view.vue'),
                },
                {
                    path: 'ibzdepartments/:ibzdepartment?/pickupgridview/:pickupgridview?',
                    meta: {
                        caption: 'entities.ibzdepartment.views.pickupgridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzdepartments', parameterName: 'ibzdepartment' },
                            { pathName: 'pickupgridview', parameterName: 'pickupgridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzdepartment-pickup-grid-view/ibzdepartment-pickup-grid-view.vue'),
                },
                {
                    path: 'ibzorganizations/:ibzorganization?/editview/:editview?',
                    meta: {
                        caption: 'entities.ibzorganization.views.editview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzorganizations', parameterName: 'ibzorganization' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzorganization-edit-view/ibzorganization-edit-view.vue'),
                },
                {
                    path: 'jobsinfos/:jobsinfo?/editview/:editview?',
                    meta: {
                        caption: 'entities.jobsinfo.views.editview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'jobsinfos', parameterName: 'jobsinfo' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/task/jobs-info-edit-view/jobs-info-edit-view.vue'),
                },
                {
                    path: 'territories/:territory?/editview/:editview?',
                    meta: {
                        caption: 'entities.territory.views.editview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'territories', parameterName: 'territory' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/base/territory-edit-view/territory-edit-view.vue'),
                },
                {
                    path: 'transactioncurrencies/:transactioncurrency?/pickupview/:pickupview?',
                    meta: {
                        caption: 'entities.transactioncurrency.views.pickupview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'transactioncurrencies', parameterName: 'transactioncurrency' },
                            { pathName: 'pickupview', parameterName: 'pickupview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/base/transaction-currency-pickup-view/transaction-currency-pickup-view.vue'),
                },
                {
                    path: 'ibzorganizations/:ibzorganization?/ibzdepartments/:ibzdepartment?/ibzemployees/:ibzemployee?/pickupgridview/:pickupgridview?',
                    meta: {
                        caption: 'entities.ibzemployee.views.pickupgridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzorganizations', parameterName: 'ibzorganization' },
                            { pathName: 'ibzdepartments', parameterName: 'ibzdepartment' },
                            { pathName: 'ibzemployees', parameterName: 'ibzemployee' },
                            { pathName: 'pickupgridview', parameterName: 'pickupgridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzemployee-pickup-grid-view/ibzemployee-pickup-grid-view.vue'),
                },
                {
                    path: 'ibzorganizations/:ibzorganization?/ibzemployees/:ibzemployee?/pickupgridview/:pickupgridview?',
                    meta: {
                        caption: 'entities.ibzemployee.views.pickupgridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzorganizations', parameterName: 'ibzorganization' },
                            { pathName: 'ibzemployees', parameterName: 'ibzemployee' },
                            { pathName: 'pickupgridview', parameterName: 'pickupgridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzemployee-pickup-grid-view/ibzemployee-pickup-grid-view.vue'),
                },
                {
                    path: 'ibzdepartments/:ibzdepartment?/ibzemployees/:ibzemployee?/pickupgridview/:pickupgridview?',
                    meta: {
                        caption: 'entities.ibzemployee.views.pickupgridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzdepartments', parameterName: 'ibzdepartment' },
                            { pathName: 'ibzemployees', parameterName: 'ibzemployee' },
                            { pathName: 'pickupgridview', parameterName: 'pickupgridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzemployee-pickup-grid-view/ibzemployee-pickup-grid-view.vue'),
                },
                {
                    path: 'ibzemployees/:ibzemployee?/pickupgridview/:pickupgridview?',
                    meta: {
                        caption: 'entities.ibzemployee.views.pickupgridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzemployees', parameterName: 'ibzemployee' },
                            { pathName: 'pickupgridview', parameterName: 'pickupgridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzemployee-pickup-grid-view/ibzemployee-pickup-grid-view.vue'),
                },
                {
                    path: 'wfusers/:wfuser?/wfmembers/:wfmember?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.wfmember.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'wfusers', parameterName: 'wfuser' },
                            { pathName: 'wfmembers', parameterName: 'wfmember' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/workflow/wfmember-grid-view/wfmember-grid-view.vue'),
                },
                {
                    path: 'wfgroups/:wfgroup?/wfmembers/:wfmember?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.wfmember.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'wfgroups', parameterName: 'wfgroup' },
                            { pathName: 'wfmembers', parameterName: 'wfmember' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/workflow/wfmember-grid-view/wfmember-grid-view.vue'),
                },
                {
                    path: 'wfmembers/:wfmember?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.wfmember.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'wfmembers', parameterName: 'wfmember' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/workflow/wfmember-grid-view/wfmember-grid-view.vue'),
                },
                {
                    path: 'operationunits/:operationunit?/deptmastergridview/:deptmastergridview?',
                    meta: {
                        caption: 'entities.operationunit.views.deptmastergridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'operationunits', parameterName: 'operationunit' },
                            { pathName: 'deptmastergridview', parameterName: 'deptmastergridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/base/operation-unit-dept-master-grid-view/operation-unit-dept-master-grid-view.vue'),
                },
                {
                    path: 'jobslogs/:jobslog?/editview/:editview?',
                    meta: {
                        caption: 'entities.jobslog.views.editview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'jobslogs', parameterName: 'jobslog' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/task/jobs-log-edit-view/jobs-log-edit-view.vue'),
                },
                {
                    path: 'jobsregistries/:jobsregistry?/editview/:editview?',
                    meta: {
                        caption: 'entities.jobsregistry.views.editview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'jobsregistries', parameterName: 'jobsregistry' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/task/jobs-registry-edit-view/jobs-registry-edit-view.vue'),
                },
                {
                    path: 'dictcatalogs/:dictcatalog?/pickupview/:pickupview?',
                    meta: {
                        caption: 'entities.dictcatalog.views.pickupview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'dictcatalogs', parameterName: 'dictcatalog' },
                            { pathName: 'pickupview', parameterName: 'pickupview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/dict/dict-catalog-pickup-view/dict-catalog-pickup-view.vue'),
                },
                {
                    path: 'sysusers/:sysuser?/editview/:editview?',
                    meta: {
                        caption: 'entities.sysuser.views.editview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'sysusers', parameterName: 'sysuser' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/uaa/sys-user-edit-view/sys-user-edit-view.vue'),
                },
                {
                    path: 'ibzorganizations/:ibzorganization?/ibzdepartments/:ibzdepartment?/treeexpview/:treeexpview?',
                    meta: {
                        caption: 'entities.ibzdepartment.views.treeexpview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzorganizations', parameterName: 'ibzorganization' },
                            { pathName: 'ibzdepartments', parameterName: 'ibzdepartment' },
                            { pathName: 'treeexpview', parameterName: 'treeexpview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzdepartment-tree-exp-view/ibzdepartment-tree-exp-view.vue'),
                },
                {
                    path: 'ibzdepartments/:ibzdepartment?/treeexpview/:treeexpview?',
                    meta: {
                        caption: 'entities.ibzdepartment.views.treeexpview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzdepartments', parameterName: 'ibzdepartment' },
                            { pathName: 'treeexpview', parameterName: 'treeexpview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzdepartment-tree-exp-view/ibzdepartment-tree-exp-view.vue'),
                },
                {
                    path: 'sysauthlogs/:sysauthlog?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.sysauthlog.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'sysauthlogs', parameterName: 'sysauthlog' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/uaa/sys-auth-log-grid-view/sys-auth-log-grid-view.vue'),
                },
                {
                    path: 'wfremodels/:wfremodel?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.wfremodel.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'wfremodels', parameterName: 'wfremodel' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/workflow/wfremodel-grid-view/wfremodel-grid-view.vue'),
                },
                {
                    path: 'sysusers/:sysuser?/pickupview/:pickupview?',
                    meta: {
                        caption: 'entities.sysuser.views.pickupview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'sysusers', parameterName: 'sysuser' },
                            { pathName: 'pickupview', parameterName: 'pickupview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/uaa/sys-user-pickup-view/sys-user-pickup-view.vue'),
                },
                {
                    path: 'jobsregistries/:jobsregistry?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.jobsregistry.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'jobsregistries', parameterName: 'jobsregistry' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/task/jobs-registry-grid-view/jobs-registry-grid-view.vue'),
                },
                {
                    path: 'wfprocessdefinitions/:wfprocessdefinition?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.wfprocessdefinition.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'wfprocessdefinitions', parameterName: 'wfprocessdefinition' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/workflow/wfprocess-definition-grid-view/wfprocess-definition-grid-view.vue'),
                },
                {
                    path: 'ibzorganizations/:ibzorganization?/ibzdepartments/:ibzdepartment?/editview/:editview?',
                    meta: {
                        caption: 'entities.ibzdepartment.views.editview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzorganizations', parameterName: 'ibzorganization' },
                            { pathName: 'ibzdepartments', parameterName: 'ibzdepartment' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzdepartment-edit-view/ibzdepartment-edit-view.vue'),
                },
                {
                    path: 'ibzdepartments/:ibzdepartment?/editview/:editview?',
                    meta: {
                        caption: 'entities.ibzdepartment.views.editview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzdepartments', parameterName: 'ibzdepartment' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzdepartment-edit-view/ibzdepartment-edit-view.vue'),
                },
                {
                    path: 'omhierarchycats/:omhierarchycat?/v_002/:v_002?',
                    meta: {
                        caption: 'entities.omhierarchycat.views.v_002.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'omhierarchycats', parameterName: 'omhierarchycat' },
                            { pathName: 'v_002', parameterName: 'v_002' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/base/omhierarchy-cat-v-002/omhierarchy-cat-v-002.vue'),
                },
                {
                    path: 'connectionroles/:connectionrole?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.connectionrole.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'connectionroles', parameterName: 'connectionrole' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/runtime/connection-role-grid-view/connection-role-grid-view.vue'),
                },
                {
                    path: 'wfremodels/:wfremodel?/editview/:editview?',
                    meta: {
                        caption: 'entities.wfremodel.views.editview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'wfremodels', parameterName: 'wfremodel' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/workflow/wfremodel-edit-view/wfremodel-edit-view.vue'),
                },
                {
                    path: 'sysroles/:sysrole?/editview/:editview?',
                    meta: {
                        caption: 'entities.sysrole.views.editview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'sysroles', parameterName: 'sysrole' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/uaa/sys-role-edit-view/sys-role-edit-view.vue'),
                },
                {
                    path: 'legals/:legal?/mastersummaryview/:mastersummaryview?',
                    meta: {
                        caption: 'entities.legal.views.mastersummaryview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'legals', parameterName: 'legal' },
                            { pathName: 'mastersummaryview', parameterName: 'mastersummaryview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/base/legal-master-summary-view/legal-master-summary-view.vue'),
                },
                {
                    path: 'wfusers/:wfuser?/pickupview/:pickupview?',
                    meta: {
                        caption: 'entities.wfuser.views.pickupview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'wfusers', parameterName: 'wfuser' },
                            { pathName: 'pickupview', parameterName: 'pickupview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/workflow/wfuser-pickup-view/wfuser-pickup-view.vue'),
                },
                {
                    path: 'uomschedules/:uomschedule?/editview/:editview?',
                    meta: {
                        caption: 'entities.uomschedule.views.editview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'uomschedules', parameterName: 'uomschedule' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/base/uom-schedule-edit-view/uom-schedule-edit-view.vue'),
                },
                {
                    path: 'operationunits/:operationunit?/omhierarchies/:omhierarchy?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.omhierarchy.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'operationunits', parameterName: 'operationunit' },
                            { pathName: 'omhierarchies', parameterName: 'omhierarchy' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/base/omhierarchy-grid-view/omhierarchy-grid-view.vue'),
                },
                {
                    path: 'omhierarchycats/:omhierarchycat?/omhierarchies/:omhierarchy?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.omhierarchy.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'omhierarchycats', parameterName: 'omhierarchycat' },
                            { pathName: 'omhierarchies', parameterName: 'omhierarchy' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/base/omhierarchy-grid-view/omhierarchy-grid-view.vue'),
                },
                {
                    path: 'legals/:legal?/omhierarchies/:omhierarchy?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.omhierarchy.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'legals', parameterName: 'legal' },
                            { pathName: 'omhierarchies', parameterName: 'omhierarchy' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/base/omhierarchy-grid-view/omhierarchy-grid-view.vue'),
                },
                {
                    path: 'omhierarchies/:omhierarchy?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.omhierarchy.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'omhierarchies', parameterName: 'omhierarchy' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/base/omhierarchy-grid-view/omhierarchy-grid-view.vue'),
                },
                {
                    path: 'ibzorganizations/:ibzorganization?/ibzdepartments/:ibzdepartment?/pickupview/:pickupview?',
                    meta: {
                        caption: 'entities.ibzdepartment.views.pickupview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzorganizations', parameterName: 'ibzorganization' },
                            { pathName: 'ibzdepartments', parameterName: 'ibzdepartment' },
                            { pathName: 'pickupview', parameterName: 'pickupview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzdepartment-pickup-view/ibzdepartment-pickup-view.vue'),
                },
                {
                    path: 'ibzdepartments/:ibzdepartment?/pickupview/:pickupview?',
                    meta: {
                        caption: 'entities.ibzdepartment.views.pickupview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzdepartments', parameterName: 'ibzdepartment' },
                            { pathName: 'pickupview', parameterName: 'pickupview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzdepartment-pickup-view/ibzdepartment-pickup-view.vue'),
                },
                {
                    path: 'dictcatalogs/:dictcatalog?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.dictcatalog.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'dictcatalogs', parameterName: 'dictcatalog' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/dict/dict-catalog-grid-view/dict-catalog-grid-view.vue'),
                },
                {
                    path: 'transactioncurrencies/:transactioncurrency?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.transactioncurrency.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'transactioncurrencies', parameterName: 'transactioncurrency' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/base/transaction-currency-grid-view/transaction-currency-grid-view.vue'),
                },
                {
                    path: 'legals/:legal?/masterinfoview/:masterinfoview?',
                    meta: {
                        caption: 'entities.legal.views.masterinfoview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'legals', parameterName: 'legal' },
                            { pathName: 'masterinfoview', parameterName: 'masterinfoview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/base/legal-master-info-view/legal-master-info-view.vue'),
                },
                {
                    path: 'discounttypes/:discounttype?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.discounttype.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'discounttypes', parameterName: 'discounttype' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/sales/discount-type-grid-view/discount-type-grid-view.vue'),
                },
                {
                    path: 'pricelevels/:pricelevel?/editview/:editview?',
                    meta: {
                        caption: 'entities.pricelevel.views.editview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'pricelevels', parameterName: 'pricelevel' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/product/price-level-edit-view/price-level-edit-view.vue'),
                },
                {
                    path: 'ibzorganizations/:ibzorganization?/ibzdepartments/:ibzdepartment?/ibzemployees/:ibzemployee?/ibzdeptmembers/:ibzdeptmember?/editview/:editview?',
                    meta: {
                        caption: 'entities.ibzdeptmember.views.editview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzorganizations', parameterName: 'ibzorganization' },
                            { pathName: 'ibzdepartments', parameterName: 'ibzdepartment' },
                            { pathName: 'ibzemployees', parameterName: 'ibzemployee' },
                            { pathName: 'ibzdeptmembers', parameterName: 'ibzdeptmember' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzdept-member-edit-view/ibzdept-member-edit-view.vue'),
                },
                {
                    path: 'ibzorganizations/:ibzorganization?/ibzemployees/:ibzemployee?/ibzdeptmembers/:ibzdeptmember?/editview/:editview?',
                    meta: {
                        caption: 'entities.ibzdeptmember.views.editview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzorganizations', parameterName: 'ibzorganization' },
                            { pathName: 'ibzemployees', parameterName: 'ibzemployee' },
                            { pathName: 'ibzdeptmembers', parameterName: 'ibzdeptmember' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzdept-member-edit-view/ibzdept-member-edit-view.vue'),
                },
                {
                    path: 'ibzorganizations/:ibzorganization?/ibzdepartments/:ibzdepartment?/ibzdeptmembers/:ibzdeptmember?/editview/:editview?',
                    meta: {
                        caption: 'entities.ibzdeptmember.views.editview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzorganizations', parameterName: 'ibzorganization' },
                            { pathName: 'ibzdepartments', parameterName: 'ibzdepartment' },
                            { pathName: 'ibzdeptmembers', parameterName: 'ibzdeptmember' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzdept-member-edit-view/ibzdept-member-edit-view.vue'),
                },
                {
                    path: 'ibzdepartments/:ibzdepartment?/ibzemployees/:ibzemployee?/ibzdeptmembers/:ibzdeptmember?/editview/:editview?',
                    meta: {
                        caption: 'entities.ibzdeptmember.views.editview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzdepartments', parameterName: 'ibzdepartment' },
                            { pathName: 'ibzemployees', parameterName: 'ibzemployee' },
                            { pathName: 'ibzdeptmembers', parameterName: 'ibzdeptmember' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzdept-member-edit-view/ibzdept-member-edit-view.vue'),
                },
                {
                    path: 'ibzemployees/:ibzemployee?/ibzdeptmembers/:ibzdeptmember?/editview/:editview?',
                    meta: {
                        caption: 'entities.ibzdeptmember.views.editview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzemployees', parameterName: 'ibzemployee' },
                            { pathName: 'ibzdeptmembers', parameterName: 'ibzdeptmember' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzdept-member-edit-view/ibzdept-member-edit-view.vue'),
                },
                {
                    path: 'ibzdepartments/:ibzdepartment?/ibzdeptmembers/:ibzdeptmember?/editview/:editview?',
                    meta: {
                        caption: 'entities.ibzdeptmember.views.editview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzdepartments', parameterName: 'ibzdepartment' },
                            { pathName: 'ibzdeptmembers', parameterName: 'ibzdeptmember' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzdept-member-edit-view/ibzdept-member-edit-view.vue'),
                },
                {
                    path: 'ibzdeptmembers/:ibzdeptmember?/editview/:editview?',
                    meta: {
                        caption: 'entities.ibzdeptmember.views.editview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzdeptmembers', parameterName: 'ibzdeptmember' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzdept-member-edit-view/ibzdept-member-edit-view.vue'),
                },
                {
                    path: 'jobslogs/:jobslog?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.jobslog.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'jobslogs', parameterName: 'jobslog' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/task/jobs-log-grid-view/jobs-log-grid-view.vue'),
                },
                {
                    path: 'ibzorganizations/:ibzorganization?/ibzdepartments/:ibzdepartment?/ibzemployees/:ibzemployee?/pickupview/:pickupview?',
                    meta: {
                        caption: 'entities.ibzemployee.views.pickupview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzorganizations', parameterName: 'ibzorganization' },
                            { pathName: 'ibzdepartments', parameterName: 'ibzdepartment' },
                            { pathName: 'ibzemployees', parameterName: 'ibzemployee' },
                            { pathName: 'pickupview', parameterName: 'pickupview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzemployee-pickup-view/ibzemployee-pickup-view.vue'),
                },
                {
                    path: 'ibzorganizations/:ibzorganization?/ibzemployees/:ibzemployee?/pickupview/:pickupview?',
                    meta: {
                        caption: 'entities.ibzemployee.views.pickupview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzorganizations', parameterName: 'ibzorganization' },
                            { pathName: 'ibzemployees', parameterName: 'ibzemployee' },
                            { pathName: 'pickupview', parameterName: 'pickupview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzemployee-pickup-view/ibzemployee-pickup-view.vue'),
                },
                {
                    path: 'ibzdepartments/:ibzdepartment?/ibzemployees/:ibzemployee?/pickupview/:pickupview?',
                    meta: {
                        caption: 'entities.ibzemployee.views.pickupview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzdepartments', parameterName: 'ibzdepartment' },
                            { pathName: 'ibzemployees', parameterName: 'ibzemployee' },
                            { pathName: 'pickupview', parameterName: 'pickupview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzemployee-pickup-view/ibzemployee-pickup-view.vue'),
                },
                {
                    path: 'ibzemployees/:ibzemployee?/pickupview/:pickupview?',
                    meta: {
                        caption: 'entities.ibzemployee.views.pickupview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzemployees', parameterName: 'ibzemployee' },
                            { pathName: 'pickupview', parameterName: 'pickupview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzemployee-pickup-view/ibzemployee-pickup-view.vue'),
                },
                {
                    path: 'ibzorganizations/:ibzorganization?/ibzdepartments/:ibzdepartment?/ibzemployees/:ibzemployee?/ibzdeptmembers/:ibzdeptmember?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.ibzdeptmember.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzorganizations', parameterName: 'ibzorganization' },
                            { pathName: 'ibzdepartments', parameterName: 'ibzdepartment' },
                            { pathName: 'ibzemployees', parameterName: 'ibzemployee' },
                            { pathName: 'ibzdeptmembers', parameterName: 'ibzdeptmember' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzdept-member-grid-view/ibzdept-member-grid-view.vue'),
                },
                {
                    path: 'ibzorganizations/:ibzorganization?/ibzemployees/:ibzemployee?/ibzdeptmembers/:ibzdeptmember?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.ibzdeptmember.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzorganizations', parameterName: 'ibzorganization' },
                            { pathName: 'ibzemployees', parameterName: 'ibzemployee' },
                            { pathName: 'ibzdeptmembers', parameterName: 'ibzdeptmember' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzdept-member-grid-view/ibzdept-member-grid-view.vue'),
                },
                {
                    path: 'ibzorganizations/:ibzorganization?/ibzdepartments/:ibzdepartment?/ibzdeptmembers/:ibzdeptmember?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.ibzdeptmember.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzorganizations', parameterName: 'ibzorganization' },
                            { pathName: 'ibzdepartments', parameterName: 'ibzdepartment' },
                            { pathName: 'ibzdeptmembers', parameterName: 'ibzdeptmember' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzdept-member-grid-view/ibzdept-member-grid-view.vue'),
                },
                {
                    path: 'ibzdepartments/:ibzdepartment?/ibzemployees/:ibzemployee?/ibzdeptmembers/:ibzdeptmember?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.ibzdeptmember.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzdepartments', parameterName: 'ibzdepartment' },
                            { pathName: 'ibzemployees', parameterName: 'ibzemployee' },
                            { pathName: 'ibzdeptmembers', parameterName: 'ibzdeptmember' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzdept-member-grid-view/ibzdept-member-grid-view.vue'),
                },
                {
                    path: 'ibzemployees/:ibzemployee?/ibzdeptmembers/:ibzdeptmember?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.ibzdeptmember.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzemployees', parameterName: 'ibzemployee' },
                            { pathName: 'ibzdeptmembers', parameterName: 'ibzdeptmember' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzdept-member-grid-view/ibzdept-member-grid-view.vue'),
                },
                {
                    path: 'ibzdepartments/:ibzdepartment?/ibzdeptmembers/:ibzdeptmember?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.ibzdeptmember.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzdepartments', parameterName: 'ibzdepartment' },
                            { pathName: 'ibzdeptmembers', parameterName: 'ibzdeptmember' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzdept-member-grid-view/ibzdept-member-grid-view.vue'),
                },
                {
                    path: 'ibzdeptmembers/:ibzdeptmember?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.ibzdeptmember.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzdeptmembers', parameterName: 'ibzdeptmember' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzdept-member-grid-view/ibzdept-member-grid-view.vue'),
                },
                {
                    path: 'ibzposts/:ibzpost?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.ibzpost.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzposts', parameterName: 'ibzpost' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzpost-grid-view/ibzpost-grid-view.vue'),
                },
                {
                    path: 'operationunits/:operationunit?/bumasterquickview/:bumasterquickview?',
                    meta: {
                        caption: 'entities.operationunit.views.bumasterquickview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'operationunits', parameterName: 'operationunit' },
                            { pathName: 'bumasterquickview', parameterName: 'bumasterquickview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/base/operation-unit-bumaster-quick-view/operation-unit-bumaster-quick-view.vue'),
                },
                {
                    path: 'sysroles/:sysrole?/sysrolepermissions/:sysrolepermission?/customview/:customview?',
                    meta: {
                        caption: 'entities.sysrolepermission.views.customview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'sysroles', parameterName: 'sysrole' },
                            { pathName: 'sysrolepermissions', parameterName: 'sysrolepermission' },
                            { pathName: 'customview', parameterName: 'customview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/uaa/sys-role-permission-custom-view/sys-role-permission-custom-view.vue'),
                },
                {
                    path: 'syspermissions/:syspermission?/sysrolepermissions/:sysrolepermission?/customview/:customview?',
                    meta: {
                        caption: 'entities.sysrolepermission.views.customview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'syspermissions', parameterName: 'syspermission' },
                            { pathName: 'sysrolepermissions', parameterName: 'sysrolepermission' },
                            { pathName: 'customview', parameterName: 'customview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/uaa/sys-role-permission-custom-view/sys-role-permission-custom-view.vue'),
                },
                {
                    path: 'sysrolepermissions/:sysrolepermission?/customview/:customview?',
                    meta: {
                        caption: 'entities.sysrolepermission.views.customview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'sysrolepermissions', parameterName: 'sysrolepermission' },
                            { pathName: 'customview', parameterName: 'customview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/uaa/sys-role-permission-custom-view/sys-role-permission-custom-view.vue'),
                },
                {
                    path: 'dictcatalogs/:dictcatalog?/dictoptions/:dictoption?/editview/:editview?',
                    meta: {
                        caption: 'entities.dictoption.views.editview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'dictcatalogs', parameterName: 'dictcatalog' },
                            { pathName: 'dictoptions', parameterName: 'dictoption' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/dict/dict-option-edit-view/dict-option-edit-view.vue'),
                },
                {
                    path: 'dictoptions/:dictoption?/editview/:editview?',
                    meta: {
                        caption: 'entities.dictoption.views.editview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'dictoptions', parameterName: 'dictoption' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/dict/dict-option-edit-view/dict-option-edit-view.vue'),
                },
                {
                    path: 'sysroles/:sysrole?/pickupview/:pickupview?',
                    meta: {
                        caption: 'entities.sysrole.views.pickupview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'sysroles', parameterName: 'sysrole' },
                            { pathName: 'pickupview', parameterName: 'pickupview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/uaa/sys-role-pickup-view/sys-role-pickup-view.vue'),
                },
                {
                    path: 'employees/:employee?/mastersummaryview/:mastersummaryview?',
                    meta: {
                        caption: 'entities.employee.views.mastersummaryview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'employees', parameterName: 'employee' },
                            { pathName: 'mastersummaryview', parameterName: 'mastersummaryview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/human-resource/employee-master-summary-view/employee-master-summary-view.vue'),
                },
                {
                    path: 'sysapps/:sysapp?/editview/:editview?',
                    meta: {
                        caption: 'entities.sysapp.views.editview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'sysapps', parameterName: 'sysapp' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/uaa/sys-app-edit-view/sys-app-edit-view.vue'),
                },
                {
                    path: 'ibzorganizations/:ibzorganization?/pickupgridview/:pickupgridview?',
                    meta: {
                        caption: 'entities.ibzorganization.views.pickupgridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzorganizations', parameterName: 'ibzorganization' },
                            { pathName: 'pickupgridview', parameterName: 'pickupgridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzorganization-pickup-grid-view/ibzorganization-pickup-grid-view.vue'),
                },
                {
                    path: 'ibzposts/:ibzpost?/editview/:editview?',
                    meta: {
                        caption: 'entities.ibzpost.views.editview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzposts', parameterName: 'ibzpost' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzpost-edit-view/ibzpost-edit-view.vue'),
                },
                {
                    path: 'discounttypes/:discounttype?/editview/:editview?',
                    meta: {
                        caption: 'entities.discounttype.views.editview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'discounttypes', parameterName: 'discounttype' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/sales/discount-type-edit-view/discount-type-edit-view.vue'),
                },
                {
                    path: 'operationunits/:operationunit?/bumastergridview/:bumastergridview?',
                    meta: {
                        caption: 'entities.operationunit.views.bumastergridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'operationunits', parameterName: 'operationunit' },
                            { pathName: 'bumastergridview', parameterName: 'bumastergridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/base/operation-unit-bumaster-grid-view/operation-unit-bumaster-grid-view.vue'),
                },
                {
                    path: 'legals/:legal?/mastertabinfoview/:mastertabinfoview?',
                    meta: {
                        caption: 'entities.legal.views.mastertabinfoview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'legals', parameterName: 'legal' },
                            { pathName: 'mastertabinfoview', parameterName: 'mastertabinfoview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/base/legal-master-tab-info-view/legal-master-tab-info-view.vue'),
                },
                {
                    path: 'wfgroups/:wfgroup?/pickupview/:pickupview?',
                    meta: {
                        caption: 'entities.wfgroup.views.pickupview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'wfgroups', parameterName: 'wfgroup' },
                            { pathName: 'pickupview', parameterName: 'pickupview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/workflow/wfgroup-pickup-view/wfgroup-pickup-view.vue'),
                },
                {
                    path: 'ibzorganizations/:ibzorganization?/ibzdepartments/:ibzdepartment?/ibzemployees/:ibzemployee?/editview/:editview?',
                    meta: {
                        caption: 'entities.ibzemployee.views.editview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzorganizations', parameterName: 'ibzorganization' },
                            { pathName: 'ibzdepartments', parameterName: 'ibzdepartment' },
                            { pathName: 'ibzemployees', parameterName: 'ibzemployee' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzemployee-edit-view/ibzemployee-edit-view.vue'),
                },
                {
                    path: 'ibzorganizations/:ibzorganization?/ibzemployees/:ibzemployee?/editview/:editview?',
                    meta: {
                        caption: 'entities.ibzemployee.views.editview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzorganizations', parameterName: 'ibzorganization' },
                            { pathName: 'ibzemployees', parameterName: 'ibzemployee' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzemployee-edit-view/ibzemployee-edit-view.vue'),
                },
                {
                    path: 'ibzdepartments/:ibzdepartment?/ibzemployees/:ibzemployee?/editview/:editview?',
                    meta: {
                        caption: 'entities.ibzemployee.views.editview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzdepartments', parameterName: 'ibzdepartment' },
                            { pathName: 'ibzemployees', parameterName: 'ibzemployee' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzemployee-edit-view/ibzemployee-edit-view.vue'),
                },
                {
                    path: 'ibzemployees/:ibzemployee?/editview/:editview?',
                    meta: {
                        caption: 'entities.ibzemployee.views.editview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzemployees', parameterName: 'ibzemployee' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzemployee-edit-view/ibzemployee-edit-view.vue'),
                },
                {
                    path: 'sysusers/:sysuser?/mpickupview/:mpickupview?',
                    meta: {
                        caption: 'entities.sysuser.views.mpickupview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'sysusers', parameterName: 'sysuser' },
                            { pathName: 'mpickupview', parameterName: 'mpickupview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/uaa/sys-user-mpickup-view/sys-user-mpickup-view.vue'),
                },
                {
                    path: 'operationunits/:operationunit?/omhierarchies/:omhierarchy?/editview/:editview?',
                    meta: {
                        caption: 'entities.omhierarchy.views.editview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'operationunits', parameterName: 'operationunit' },
                            { pathName: 'omhierarchies', parameterName: 'omhierarchy' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/base/omhierarchy-edit-view/omhierarchy-edit-view.vue'),
                },
                {
                    path: 'omhierarchycats/:omhierarchycat?/omhierarchies/:omhierarchy?/editview/:editview?',
                    meta: {
                        caption: 'entities.omhierarchy.views.editview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'omhierarchycats', parameterName: 'omhierarchycat' },
                            { pathName: 'omhierarchies', parameterName: 'omhierarchy' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/base/omhierarchy-edit-view/omhierarchy-edit-view.vue'),
                },
                {
                    path: 'legals/:legal?/omhierarchies/:omhierarchy?/editview/:editview?',
                    meta: {
                        caption: 'entities.omhierarchy.views.editview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'legals', parameterName: 'legal' },
                            { pathName: 'omhierarchies', parameterName: 'omhierarchy' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/base/omhierarchy-edit-view/omhierarchy-edit-view.vue'),
                },
                {
                    path: 'omhierarchies/:omhierarchy?/editview/:editview?',
                    meta: {
                        caption: 'entities.omhierarchy.views.editview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'omhierarchies', parameterName: 'omhierarchy' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/base/omhierarchy-edit-view/omhierarchy-edit-view.vue'),
                },
                {
                    path: 'dictcatalogs/:dictcatalog?/dictoptions/:dictoption?/grideditview/:grideditview?',
                    meta: {
                        caption: 'entities.dictoption.views.grideditview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'dictcatalogs', parameterName: 'dictcatalog' },
                            { pathName: 'dictoptions', parameterName: 'dictoption' },
                            { pathName: 'grideditview', parameterName: 'grideditview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/dict/dict-option-grid-edit-view/dict-option-grid-edit-view.vue'),
                },
                {
                    path: 'dictoptions/:dictoption?/grideditview/:grideditview?',
                    meta: {
                        caption: 'entities.dictoption.views.grideditview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'dictoptions', parameterName: 'dictoption' },
                            { pathName: 'grideditview', parameterName: 'grideditview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/dict/dict-option-grid-edit-view/dict-option-grid-edit-view.vue'),
                },
                {
                    path: 'sysusers/:sysuser?/pickupgridview/:pickupgridview?',
                    meta: {
                        caption: 'entities.sysuser.views.pickupgridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'sysusers', parameterName: 'sysuser' },
                            { pathName: 'pickupgridview', parameterName: 'pickupgridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/uaa/sys-user-pickup-grid-view/sys-user-pickup-grid-view.vue'),
                },
                {
                    path: 'jobsinfos/:jobsinfo?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.jobsinfo.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'jobsinfos', parameterName: 'jobsinfo' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/task/jobs-info-grid-view/jobs-info-grid-view.vue'),
                },
                {
                    path: 'setting/:setting?',
                    meta: {
                        caption: 'app.views.setting.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'setting', parameterName: 'setting' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ungroup/setting/setting.vue'),
                },
                {
                    path: 'legals/:legal?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.legal.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'legals', parameterName: 'legal' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/base/legal-grid-view/legal-grid-view.vue'),
                },
                {
                    path: 'metrics/:metric?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.metric.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'metrics', parameterName: 'metric' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/base/metric-grid-view/metric-grid-view.vue'),
                },
                {
                    path: 'dictcatalogs/:dictcatalog?/editview/:editview?',
                    meta: {
                        caption: 'entities.dictcatalog.views.editview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'dictcatalogs', parameterName: 'dictcatalog' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/dict/dict-catalog-edit-view/dict-catalog-edit-view.vue'),
                },
                {
                    path: 'wfgroups/:wfgroup?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.wfgroup.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'wfgroups', parameterName: 'wfgroup' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/workflow/wfgroup-grid-view/wfgroup-grid-view.vue'),
                },
                {
                    path: 'wfprocessdefinitions/:wfprocessdefinition?/editview/:editview?',
                    meta: {
                        caption: 'entities.wfprocessdefinition.views.editview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'wfprocessdefinitions', parameterName: 'wfprocessdefinition' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/workflow/wfprocess-definition-edit-view/wfprocess-definition-edit-view.vue'),
                },
                {
                    path: 'dictcatalogs/:dictcatalog?/pickupgridview/:pickupgridview?',
                    meta: {
                        caption: 'entities.dictcatalog.views.pickupgridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'dictcatalogs', parameterName: 'dictcatalog' },
                            { pathName: 'pickupgridview', parameterName: 'pickupgridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/dict/dict-catalog-pickup-grid-view/dict-catalog-pickup-grid-view.vue'),
                },
                {
                    path: 'pricelevels/:pricelevel?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.pricelevel.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'pricelevels', parameterName: 'pricelevel' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/product/price-level-grid-view/price-level-grid-view.vue'),
                },
                {
                    path: 'ibzposts/:ibzpost?/pickupview/:pickupview?',
                    meta: {
                        caption: 'entities.ibzpost.views.pickupview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'ibzposts', parameterName: 'ibzpost' },
                            { pathName: 'pickupview', parameterName: 'pickupview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ou/ibzpost-pickup-view/ibzpost-pickup-view.vue'),
                },
                {
                    path: 'transactioncurrencies/:transactioncurrency?/editview/:editview?',
                    meta: {
                        caption: 'entities.transactioncurrency.views.editview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'transactioncurrencies', parameterName: 'transactioncurrency' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/base/transaction-currency-edit-view/transaction-currency-edit-view.vue'),
                },
                {
                    path: 'wfusers/:wfuser?/mpickupview/:mpickupview?',
                    meta: {
                        caption: 'entities.wfuser.views.mpickupview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'wfusers', parameterName: 'wfuser' },
                            { pathName: 'mpickupview', parameterName: 'mpickupview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/workflow/wfuser-mpickup-view/wfuser-mpickup-view.vue'),
                },
                {
                    path: 'sysroles/:sysrole?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.sysrole.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'sysroles', parameterName: 'sysrole' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/uaa/sys-role-grid-view/sys-role-grid-view.vue'),
                },
                {
                    path: 'uomschedules/:uomschedule?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.uomschedule.views.gridview.title',
                        info:'',
                        parameters: [
                            { pathName: 'central', parameterName: 'central' },
                            { pathName: 'uomschedules', parameterName: 'uomschedule' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/base/uom-schedule-grid-view/uom-schedule-grid-view.vue'),
                },
            {
                path: 'activitypointers/:activitypointer?/redirectview/:redirectview?',
                meta: {
                    caption: 'entities.activitypointer.views.redirectview.caption',
                    info:'',
                    viewType: 'REDIRECTVIEW',
                    parameters: [
                        { pathName: 'central', parameterName: 'central' },
                        { pathName: 'activitypointers', parameterName: 'activitypointer' },
                        { pathName: 'redirectview', parameterName: 'redirectview' },
                    ],
                    requireAuth: true,
                },
                component: () => import('@pages/base/activity-pointer-redirect-view/activity-pointer-redirect-view.vue'),
            },
            ...indexRoutes,
            ],
        },
        ...globalRoutes,
        {
            path: '/login/:login?',
            name: 'login',
            meta: {  
                caption: '登录',
                viewType: 'login',
                requireAuth: false,
                ignoreAddPage: true,
            },
            beforeEnter: (to: any, from: any, next: any) => {
                appService.navHistory.reset();
                next();
            },
            component: () => import('@components/login/login'),
        },
        {
            path: '/404',
            component: () => import('@components/404/404.vue')
        },
        {
            path: '/500',
            component: () => import('@components/500/500.vue')
        },
        {
            path: '*',
            redirect: 'central'
        }
    ]
});

router.beforeEach((to: any, from: any, next: any) => {
    if (to.meta && !to.meta.ignoreAddPage) {
        appService.navHistory.add(to);
    }
    next();
});

// 解决路由跳转时报 => 路由重复
const originalPush = Router.prototype.push
Router.prototype.push = function push(location: any) {
    let result: any = originalPush.call(this, location);
    return result.catch((err: any) => err);
}

export default router;