import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { OptionViewBase } from '@/studio-core';
import ContactService from '@/service/contact/contact-service';
import ContactAuthService from '@/authservice/contact/contact-auth-service';
import OptionViewEngine from '@engine/view/option-view-engine';
import ContactUIService from '@/uiservice/contact/contact-ui-service';

/**
 * 联系人选项操作视图视图基类
 *
 * @export
 * @class ContactEditAddressBase
 * @extends {OptionViewBase}
 */
export class ContactEditAddressBase extends OptionViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof ContactEditAddressBase
     */
    protected appDeName: string = 'contact';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof ContactEditAddressBase
     */
    protected appDeKey: string = 'contactid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof ContactEditAddressBase
     */
    protected appDeMajor: string = 'fullname';

    /**
     * 实体服务对象
     *
     * @type {ContactService}
     * @memberof ContactEditAddressBase
     */
    protected appEntityService: ContactService = new ContactService;

    /**
     * 实体权限服务对象
     *
     * @type ContactUIService
     * @memberof ContactEditAddressBase
     */
    public appUIService: ContactUIService = new ContactUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof ContactEditAddressBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof ContactEditAddressBase
     */
    protected model: any = {
        srfCaption: 'entities.contact.views.editaddress.caption',
        srfTitle: 'entities.contact.views.editaddress.title',
        srfSubTitle: 'entities.contact.views.editaddress.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof ContactEditAddressBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '6b15651a990dc9900f2d839d79e8f866';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof ContactEditAddressBase
     */
    public engine: OptionViewEngine = new OptionViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof ContactEditAddressBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'contact',
            majorPSDEField: 'fullname',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof ContactEditAddressBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof ContactEditAddressBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof ContactEditAddressBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}