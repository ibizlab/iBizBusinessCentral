import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { OptionViewBase } from '@/studio-core';
import LegalService from '@/service/legal/legal-service';
import LegalAuthService from '@/authservice/legal/legal-auth-service';
import OptionViewEngine from '@engine/view/option-view-engine';
import LegalUIService from '@/uiservice/legal/legal-ui-service';

/**
 * 法人快速新建视图视图基类
 *
 * @export
 * @class LegalMasterQuickViewBase
 * @extends {OptionViewBase}
 */
export class LegalMasterQuickViewBase extends OptionViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof LegalMasterQuickViewBase
     */
    protected appDeName: string = 'legal';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof LegalMasterQuickViewBase
     */
    protected appDeKey: string = 'legalid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof LegalMasterQuickViewBase
     */
    protected appDeMajor: string = 'legalname';

    /**
     * 实体服务对象
     *
     * @type {LegalService}
     * @memberof LegalMasterQuickViewBase
     */
    protected appEntityService: LegalService = new LegalService;

    /**
     * 实体权限服务对象
     *
     * @type LegalUIService
     * @memberof LegalMasterQuickViewBase
     */
    public appUIService: LegalUIService = new LegalUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof LegalMasterQuickViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof LegalMasterQuickViewBase
     */
    protected model: any = {
        srfCaption: 'entities.legal.views.masterquickview.caption',
        srfTitle: 'entities.legal.views.masterquickview.title',
        srfSubTitle: 'entities.legal.views.masterquickview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof LegalMasterQuickViewBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '16f7707748a877dc5592669d2dec227e';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof LegalMasterQuickViewBase
     */
    public engine: OptionViewEngine = new OptionViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof LegalMasterQuickViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'legal',
            majorPSDEField: 'legalname',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof LegalMasterQuickViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof LegalMasterQuickViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof LegalMasterQuickViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}