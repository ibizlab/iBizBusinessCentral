import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import UomService from '@/service/uom/uom-service';
import UomAuthService from '@/authservice/uom/uom-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import UomUIService from '@/uiservice/uom/uom-ui-service';

/**
 * 计价单位数据选择视图视图基类
 *
 * @export
 * @class UomPickupViewBase
 * @extends {PickupViewBase}
 */
export class UomPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof UomPickupViewBase
     */
    protected appDeName: string = 'uom';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof UomPickupViewBase
     */
    protected appDeKey: string = 'uomid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof UomPickupViewBase
     */
    protected appDeMajor: string = 'uomname';

    /**
     * 实体服务对象
     *
     * @type {UomService}
     * @memberof UomPickupViewBase
     */
    protected appEntityService: UomService = new UomService;

    /**
     * 实体权限服务对象
     *
     * @type UomUIService
     * @memberof UomPickupViewBase
     */
    public appUIService: UomUIService = new UomUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof UomPickupViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof UomPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.uom.views.pickupview.caption',
        srfTitle: 'entities.uom.views.pickupview.title',
        srfSubTitle: 'entities.uom.views.pickupview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof UomPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: { name: 'pickupviewpanel', type: 'PICKUPVIEWPANEL' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '7035e5875b25e60d1fc11b06071aee1d';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof UomPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof UomPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'uom',
            majorPSDEField: 'uomname',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof UomPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof UomPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof UomPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}