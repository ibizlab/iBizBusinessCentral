import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { EditViewBase } from '@/studio-core';
import OMHierarchyCatService from '@/service/omhierarchy-cat/omhierarchy-cat-service';
import OMHierarchyCatAuthService from '@/authservice/omhierarchy-cat/omhierarchy-cat-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import OMHierarchyCatUIService from '@/uiservice/omhierarchy-cat/omhierarchy-cat-ui-service';

/**
 * V_002视图基类
 *
 * @export
 * @class OMHierarchyCatV_002Base
 * @extends {EditViewBase}
 */
export class OMHierarchyCatV_002Base extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof OMHierarchyCatV_002Base
     */
    protected appDeName: string = 'omhierarchycat';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof OMHierarchyCatV_002Base
     */
    protected appDeKey: string = 'omhierarchycatid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof OMHierarchyCatV_002Base
     */
    protected appDeMajor: string = 'omhierarchycatname';

    /**
     * 实体服务对象
     *
     * @type {OMHierarchyCatService}
     * @memberof OMHierarchyCatV_002Base
     */
    protected appEntityService: OMHierarchyCatService = new OMHierarchyCatService;

    /**
     * 实体权限服务对象
     *
     * @type OMHierarchyCatUIService
     * @memberof OMHierarchyCatV_002Base
     */
    public appUIService: OMHierarchyCatUIService = new OMHierarchyCatUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof OMHierarchyCatV_002Base
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof OMHierarchyCatV_002Base
     */
    protected model: any = {
        srfCaption: 'entities.omhierarchycat.views.v_002.caption',
        srfTitle: 'entities.omhierarchycat.views.v_002.title',
        srfSubTitle: 'entities.omhierarchycat.views.v_002.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof OMHierarchyCatV_002Base
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'b7b5d2474f0e51c5d378b5b8e7da9153';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof OMHierarchyCatV_002Base
     */
    public engine: EditViewEngine = new EditViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof OMHierarchyCatV_002Base
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'omhierarchycat',
            majorPSDEField: 'omhierarchycatname',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof OMHierarchyCatV_002Base
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof OMHierarchyCatV_002Base
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof OMHierarchyCatV_002Base
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}