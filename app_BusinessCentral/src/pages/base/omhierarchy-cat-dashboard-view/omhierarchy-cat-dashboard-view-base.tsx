import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { DashboardViewBase } from '@/studio-core';
import OMHierarchyCatService from '@/service/omhierarchy-cat/omhierarchy-cat-service';
import OMHierarchyCatAuthService from '@/authservice/omhierarchy-cat/omhierarchy-cat-auth-service';
import PortalViewEngine from '@engine/view/portal-view-engine';
import OMHierarchyCatUIService from '@/uiservice/omhierarchy-cat/omhierarchy-cat-ui-service';

/**
 * 结构层次类别看板视图基类
 *
 * @export
 * @class OMHierarchyCatDashboardViewBase
 * @extends {DashboardViewBase}
 */
export class OMHierarchyCatDashboardViewBase extends DashboardViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof OMHierarchyCatDashboardViewBase
     */
    protected appDeName: string = 'omhierarchycat';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof OMHierarchyCatDashboardViewBase
     */
    protected appDeKey: string = 'omhierarchycatid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof OMHierarchyCatDashboardViewBase
     */
    protected appDeMajor: string = 'omhierarchycatname';

    /**
     * 实体服务对象
     *
     * @type {OMHierarchyCatService}
     * @memberof OMHierarchyCatDashboardViewBase
     */
    protected appEntityService: OMHierarchyCatService = new OMHierarchyCatService;

    /**
     * 实体权限服务对象
     *
     * @type OMHierarchyCatUIService
     * @memberof OMHierarchyCatDashboardViewBase
     */
    public appUIService: OMHierarchyCatUIService = new OMHierarchyCatUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof OMHierarchyCatDashboardViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof OMHierarchyCatDashboardViewBase
     */
    protected model: any = {
        srfCaption: 'entities.omhierarchycat.views.dashboardview.caption',
        srfTitle: 'entities.omhierarchycat.views.dashboardview.title',
        srfSubTitle: 'entities.omhierarchycat.views.dashboardview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof OMHierarchyCatDashboardViewBase
     */
    protected containerModel: any = {
        view_dashboard: { name: 'dashboard', type: 'DASHBOARD' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'f9c65c1342ab966e5e6944e35d9d172c';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof OMHierarchyCatDashboardViewBase
     */
    public engine: PortalViewEngine = new PortalViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof OMHierarchyCatDashboardViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            dashboard: this.$refs.dashboard,
            keyPSDEField: 'omhierarchycat',
            majorPSDEField: 'omhierarchycatname',
            isLoadDefault: true,
        });
    }

    /**
     * dashboard 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof OMHierarchyCatDashboardViewBase
     */
    public dashboard_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('dashboard', 'load', $event);
    }


}