import { Subject } from 'rxjs';
import { EditViewBase } from '@/studio-core';
import KnowledgeArticleService from '@/service/knowledge-article/knowledge-article-service';
import KnowledgeArticleAuthService from '@/authservice/knowledge-article/knowledge-article-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import KnowledgeArticleUIService from '@/uiservice/knowledge-article/knowledge-article-ui-service';

/**
 * 头部信息编辑视图基类
 *
 * @export
 * @class KnowledgeArticleEdit_DataPanelViewBase
 * @extends {EditViewBase}
 */
export class KnowledgeArticleEdit_DataPanelViewBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof KnowledgeArticleEdit_DataPanelViewBase
     */
    protected appDeName: string = 'knowledgearticle';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof KnowledgeArticleEdit_DataPanelViewBase
     */
    protected appDeKey: string = 'knowledgearticleid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof KnowledgeArticleEdit_DataPanelViewBase
     */
    protected appDeMajor: string = 'title';

    /**
     * 实体服务对象
     *
     * @type {KnowledgeArticleService}
     * @memberof KnowledgeArticleEdit_DataPanelViewBase
     */
    protected appEntityService: KnowledgeArticleService = new KnowledgeArticleService;

    /**
     * 实体权限服务对象
     *
     * @type KnowledgeArticleUIService
     * @memberof KnowledgeArticleEdit_DataPanelViewBase
     */
    public appUIService: KnowledgeArticleUIService = new KnowledgeArticleUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof KnowledgeArticleEdit_DataPanelViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof KnowledgeArticleEdit_DataPanelViewBase
     */
    protected model: any = {
        srfCaption: 'entities.knowledgearticle.views.edit_datapanelview.caption',
        srfTitle: 'entities.knowledgearticle.views.edit_datapanelview.title',
        srfSubTitle: 'entities.knowledgearticle.views.edit_datapanelview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof KnowledgeArticleEdit_DataPanelViewBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '3d32edc6311f3f2439502df807425c39';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof KnowledgeArticleEdit_DataPanelViewBase
     */
    public engine: EditViewEngine = new EditViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof KnowledgeArticleEdit_DataPanelViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'knowledgearticle',
            majorPSDEField: 'title',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof KnowledgeArticleEdit_DataPanelViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof KnowledgeArticleEdit_DataPanelViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof KnowledgeArticleEdit_DataPanelViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}