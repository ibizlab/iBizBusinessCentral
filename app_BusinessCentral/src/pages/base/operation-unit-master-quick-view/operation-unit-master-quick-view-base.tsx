import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { OptionViewBase } from '@/studio-core';
import OperationUnitService from '@/service/operation-unit/operation-unit-service';
import OperationUnitAuthService from '@/authservice/operation-unit/operation-unit-auth-service';
import OptionViewEngine from '@engine/view/option-view-engine';
import OperationUnitUIService from '@/uiservice/operation-unit/operation-unit-ui-service';

/**
 * 快速新建视图视图基类
 *
 * @export
 * @class OperationUnitMasterQuickViewBase
 * @extends {OptionViewBase}
 */
export class OperationUnitMasterQuickViewBase extends OptionViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof OperationUnitMasterQuickViewBase
     */
    protected appDeName: string = 'operationunit';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof OperationUnitMasterQuickViewBase
     */
    protected appDeKey: string = 'operationunitid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof OperationUnitMasterQuickViewBase
     */
    protected appDeMajor: string = 'operationunitname';

    /**
     * 实体服务对象
     *
     * @type {OperationUnitService}
     * @memberof OperationUnitMasterQuickViewBase
     */
    protected appEntityService: OperationUnitService = new OperationUnitService;

    /**
     * 实体权限服务对象
     *
     * @type OperationUnitUIService
     * @memberof OperationUnitMasterQuickViewBase
     */
    public appUIService: OperationUnitUIService = new OperationUnitUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof OperationUnitMasterQuickViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof OperationUnitMasterQuickViewBase
     */
    protected model: any = {
        srfCaption: 'entities.operationunit.views.masterquickview.caption',
        srfTitle: 'entities.operationunit.views.masterquickview.title',
        srfSubTitle: 'entities.operationunit.views.masterquickview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof OperationUnitMasterQuickViewBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '8ec23e3aba218304418e8b694b402b13';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof OperationUnitMasterQuickViewBase
     */
    public engine: OptionViewEngine = new OptionViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof OperationUnitMasterQuickViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'operationunit',
            majorPSDEField: 'operationunitname',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof OperationUnitMasterQuickViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof OperationUnitMasterQuickViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof OperationUnitMasterQuickViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}