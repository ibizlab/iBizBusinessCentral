import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import ContactService from '@/service/contact/contact-service';
import ContactAuthService from '@/authservice/contact/contact-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import ContactUIService from '@/uiservice/contact/contact-ui-service';

/**
 * 联系人选择表格视图视图基类
 *
 * @export
 * @class ContactPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class ContactPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof ContactPickupGridViewBase
     */
    protected appDeName: string = 'contact';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof ContactPickupGridViewBase
     */
    protected appDeKey: string = 'contactid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof ContactPickupGridViewBase
     */
    protected appDeMajor: string = 'fullname';

    /**
     * 实体服务对象
     *
     * @type {ContactService}
     * @memberof ContactPickupGridViewBase
     */
    protected appEntityService: ContactService = new ContactService;

    /**
     * 实体权限服务对象
     *
     * @type ContactUIService
     * @memberof ContactPickupGridViewBase
     */
    public appUIService: ContactUIService = new ContactUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof ContactPickupGridViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof ContactPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.contact.views.pickupgridview.caption',
        srfTitle: 'entities.contact.views.pickupgridview.title',
        srfSubTitle: 'entities.contact.views.pickupgridview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof ContactPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: { name: 'grid', type: 'GRID' },
        view_searchform: { name: 'searchform', type: 'SEARCHFORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '2821c085b75fe9f25be6b52841c934a0';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof ContactPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof ContactPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'contact',
            majorPSDEField: 'fullname',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof ContactPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof ContactPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof ContactPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof ContactPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof ContactPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof ContactPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof ContactPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof ContactPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}