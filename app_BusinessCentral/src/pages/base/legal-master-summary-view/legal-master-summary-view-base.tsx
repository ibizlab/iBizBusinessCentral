import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { DashboardViewBase } from '@/studio-core';
import LegalService from '@/service/legal/legal-service';
import LegalAuthService from '@/authservice/legal/legal-auth-service';
import PortalViewEngine from '@engine/view/portal-view-engine';
import LegalUIService from '@/uiservice/legal/legal-ui-service';

/**
 * 主信息概览看板视图视图基类
 *
 * @export
 * @class LegalMasterSummaryViewBase
 * @extends {DashboardViewBase}
 */
export class LegalMasterSummaryViewBase extends DashboardViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof LegalMasterSummaryViewBase
     */
    protected appDeName: string = 'legal';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof LegalMasterSummaryViewBase
     */
    protected appDeKey: string = 'legalid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof LegalMasterSummaryViewBase
     */
    protected appDeMajor: string = 'legalname';

    /**
     * 实体服务对象
     *
     * @type {LegalService}
     * @memberof LegalMasterSummaryViewBase
     */
    protected appEntityService: LegalService = new LegalService;

    /**
     * 实体权限服务对象
     *
     * @type LegalUIService
     * @memberof LegalMasterSummaryViewBase
     */
    public appUIService: LegalUIService = new LegalUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof LegalMasterSummaryViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof LegalMasterSummaryViewBase
     */
    protected model: any = {
        srfCaption: 'entities.legal.views.mastersummaryview.caption',
        srfTitle: 'entities.legal.views.mastersummaryview.title',
        srfSubTitle: 'entities.legal.views.mastersummaryview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof LegalMasterSummaryViewBase
     */
    protected containerModel: any = {
        view_dashboard: { name: 'dashboard', type: 'DASHBOARD' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '0af820aaea4d41e3c334e7c8bbcc9680';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof LegalMasterSummaryViewBase
     */
    public engine: PortalViewEngine = new PortalViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof LegalMasterSummaryViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            dashboard: this.$refs.dashboard,
            keyPSDEField: 'legal',
            majorPSDEField: 'legalname',
            isLoadDefault: true,
        });
    }

    /**
     * dashboard 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof LegalMasterSummaryViewBase
     */
    public dashboard_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('dashboard', 'load', $event);
    }


}