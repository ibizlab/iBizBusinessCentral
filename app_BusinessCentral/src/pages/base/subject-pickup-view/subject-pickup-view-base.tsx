import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import SubjectService from '@/service/subject/subject-service';
import SubjectAuthService from '@/authservice/subject/subject-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import SubjectUIService from '@/uiservice/subject/subject-ui-service';

/**
 * 主题数据选择视图视图基类
 *
 * @export
 * @class SubjectPickupViewBase
 * @extends {PickupViewBase}
 */
export class SubjectPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof SubjectPickupViewBase
     */
    protected appDeName: string = 'subject';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof SubjectPickupViewBase
     */
    protected appDeKey: string = 'subjectid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof SubjectPickupViewBase
     */
    protected appDeMajor: string = 'title';

    /**
     * 实体服务对象
     *
     * @type {SubjectService}
     * @memberof SubjectPickupViewBase
     */
    protected appEntityService: SubjectService = new SubjectService;

    /**
     * 实体权限服务对象
     *
     * @type SubjectUIService
     * @memberof SubjectPickupViewBase
     */
    public appUIService: SubjectUIService = new SubjectUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof SubjectPickupViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof SubjectPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.subject.views.pickupview.caption',
        srfTitle: 'entities.subject.views.pickupview.title',
        srfSubTitle: 'entities.subject.views.pickupview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof SubjectPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: { name: 'pickupviewpanel', type: 'PICKUPVIEWPANEL' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'f0a3750df4fb3bdef7bec686c4e68020';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof SubjectPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof SubjectPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'subject',
            majorPSDEField: 'title',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SubjectPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SubjectPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SubjectPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}