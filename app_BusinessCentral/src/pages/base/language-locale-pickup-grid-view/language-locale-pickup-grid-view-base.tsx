import { Subject } from 'rxjs';
import { PickupGridViewBase } from '@/studio-core';
import LanguageLocaleService from '@/service/language-locale/language-locale-service';
import LanguageLocaleAuthService from '@/authservice/language-locale/language-locale-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import LanguageLocaleUIService from '@/uiservice/language-locale/language-locale-ui-service';

/**
 * 语言选择表格视图视图基类
 *
 * @export
 * @class LanguageLocalePickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class LanguageLocalePickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof LanguageLocalePickupGridViewBase
     */
    protected appDeName: string = 'languagelocale';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof LanguageLocalePickupGridViewBase
     */
    protected appDeKey: string = 'languagelocaleid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof LanguageLocalePickupGridViewBase
     */
    protected appDeMajor: string = 'languagelocalename';

    /**
     * 实体服务对象
     *
     * @type {LanguageLocaleService}
     * @memberof LanguageLocalePickupGridViewBase
     */
    protected appEntityService: LanguageLocaleService = new LanguageLocaleService;

    /**
     * 实体权限服务对象
     *
     * @type LanguageLocaleUIService
     * @memberof LanguageLocalePickupGridViewBase
     */
    public appUIService: LanguageLocaleUIService = new LanguageLocaleUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof LanguageLocalePickupGridViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof LanguageLocalePickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.languagelocale.views.pickupgridview.caption',
        srfTitle: 'entities.languagelocale.views.pickupgridview.title',
        srfSubTitle: 'entities.languagelocale.views.pickupgridview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof LanguageLocalePickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: { name: 'grid', type: 'GRID' },
        view_searchform: { name: 'searchform', type: 'SEARCHFORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '598222a692f91930df2a71660504caf2';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof LanguageLocalePickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof LanguageLocalePickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'languagelocale',
            majorPSDEField: 'languagelocalename',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof LanguageLocalePickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof LanguageLocalePickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof LanguageLocalePickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof LanguageLocalePickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof LanguageLocalePickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof LanguageLocalePickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof LanguageLocalePickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof LanguageLocalePickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}