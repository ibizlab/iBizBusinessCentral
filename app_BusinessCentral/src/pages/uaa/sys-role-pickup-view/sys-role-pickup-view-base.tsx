import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import SysRoleService from '@/service/sys-role/sys-role-service';
import SysRoleAuthService from '@/authservice/sys-role/sys-role-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import SysRoleUIService from '@/uiservice/sys-role/sys-role-ui-service';

/**
 * 角色数据选择视图视图基类
 *
 * @export
 * @class SysRolePickupViewBase
 * @extends {PickupViewBase}
 */
export class SysRolePickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof SysRolePickupViewBase
     */
    protected appDeName: string = 'sysrole';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof SysRolePickupViewBase
     */
    protected appDeKey: string = 'sys_roleid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof SysRolePickupViewBase
     */
    protected appDeMajor: string = 'sys_rolename';

    /**
     * 实体服务对象
     *
     * @type {SysRoleService}
     * @memberof SysRolePickupViewBase
     */
    protected appEntityService: SysRoleService = new SysRoleService;

    /**
     * 实体权限服务对象
     *
     * @type SysRoleUIService
     * @memberof SysRolePickupViewBase
     */
    public appUIService: SysRoleUIService = new SysRoleUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof SysRolePickupViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof SysRolePickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.sysrole.views.pickupview.caption',
        srfTitle: 'entities.sysrole.views.pickupview.title',
        srfSubTitle: 'entities.sysrole.views.pickupview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof SysRolePickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: { name: 'pickupviewpanel', type: 'PICKUPVIEWPANEL' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '0713bc3f42db8b49e59593fa2d0bcef2';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof SysRolePickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof SysRolePickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'sysrole',
            majorPSDEField: 'rolename',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SysRolePickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SysRolePickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SysRolePickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}