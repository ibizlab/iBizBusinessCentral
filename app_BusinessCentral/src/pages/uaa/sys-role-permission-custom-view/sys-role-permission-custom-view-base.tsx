import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { ViewBase } from '@/studio-core';
import SysRolePermissionService from '@/service/sys-role-permission/sys-role-permission-service';
import SysRolePermissionAuthService from '@/authservice/sys-role-permission/sys-role-permission-auth-service';
import SysRolePermissionUIService from '@/uiservice/sys-role-permission/sys-role-permission-ui-service';

/**
 * 角色权限关系自定义视图视图基类
 *
 * @export
 * @class SysRolePermissionCustomViewBase
 * @extends {ViewBase}
 */
export class SysRolePermissionCustomViewBase extends ViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof SysRolePermissionCustomViewBase
     */
    protected appDeName: string = 'sysrolepermission';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof SysRolePermissionCustomViewBase
     */
    protected appDeKey: string = 'sys_role_permissionid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof SysRolePermissionCustomViewBase
     */
    protected appDeMajor: string = 'sys_permissionid';

    /**
     * 实体服务对象
     *
     * @type {SysRolePermissionService}
     * @memberof SysRolePermissionCustomViewBase
     */
    protected appEntityService: SysRolePermissionService = new SysRolePermissionService;

    /**
     * 实体权限服务对象
     *
     * @type SysRolePermissionUIService
     * @memberof SysRolePermissionCustomViewBase
     */
    public appUIService: SysRolePermissionUIService = new SysRolePermissionUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof SysRolePermissionCustomViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof SysRolePermissionCustomViewBase
     */
    protected model: any = {
        srfCaption: 'entities.sysrolepermission.views.customview.caption',
        srfTitle: 'entities.sysrolepermission.views.customview.title',
        srfSubTitle: 'entities.sysrolepermission.views.customview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof SysRolePermissionCustomViewBase
     */
    protected containerModel: any = {
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '8cce851a326fc95dde55ad602a9240f1';


    /**
     * 引擎初始化
     *
     * @public
     * @memberof SysRolePermissionCustomViewBase
     */
    public engineInit(): void {
    }


}