import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { EditViewBase } from '@/studio-core';
import OpportunityService from '@/service/opportunity/opportunity-service';
import OpportunityAuthService from '@/authservice/opportunity/opportunity-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import OpportunityUIService from '@/uiservice/opportunity/opportunity-ui-service';

/**
 * 头部信息编辑视图基类
 *
 * @export
 * @class OpportunityEdit_DatePanelViewBase
 * @extends {EditViewBase}
 */
export class OpportunityEdit_DatePanelViewBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof OpportunityEdit_DatePanelViewBase
     */
    protected appDeName: string = 'opportunity';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof OpportunityEdit_DatePanelViewBase
     */
    protected appDeKey: string = 'opportunityid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof OpportunityEdit_DatePanelViewBase
     */
    protected appDeMajor: string = 'opportunityname';

    /**
     * 实体服务对象
     *
     * @type {OpportunityService}
     * @memberof OpportunityEdit_DatePanelViewBase
     */
    protected appEntityService: OpportunityService = new OpportunityService;

    /**
     * 实体权限服务对象
     *
     * @type OpportunityUIService
     * @memberof OpportunityEdit_DatePanelViewBase
     */
    public appUIService: OpportunityUIService = new OpportunityUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof OpportunityEdit_DatePanelViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof OpportunityEdit_DatePanelViewBase
     */
    protected model: any = {
        srfCaption: 'entities.opportunity.views.edit_datepanelview.caption',
        srfTitle: 'entities.opportunity.views.edit_datepanelview.title',
        srfSubTitle: 'entities.opportunity.views.edit_datepanelview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof OpportunityEdit_DatePanelViewBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'cb8322587a1abf703199bc34fefeeada';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof OpportunityEdit_DatePanelViewBase
     */
    public engine: EditViewEngine = new EditViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof OpportunityEdit_DatePanelViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'opportunity',
            majorPSDEField: 'opportunityname',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof OpportunityEdit_DatePanelViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof OpportunityEdit_DatePanelViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof OpportunityEdit_DatePanelViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}