import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import DiscountTypeService from '@/service/discount-type/discount-type-service';
import DiscountTypeAuthService from '@/authservice/discount-type/discount-type-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import DiscountTypeUIService from '@/uiservice/discount-type/discount-type-ui-service';

/**
 * 折扣表选择表格视图视图基类
 *
 * @export
 * @class DiscountTypePickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class DiscountTypePickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof DiscountTypePickupGridViewBase
     */
    protected appDeName: string = 'discounttype';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof DiscountTypePickupGridViewBase
     */
    protected appDeKey: string = 'discounttypeid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof DiscountTypePickupGridViewBase
     */
    protected appDeMajor: string = 'discounttypename';

    /**
     * 实体服务对象
     *
     * @type {DiscountTypeService}
     * @memberof DiscountTypePickupGridViewBase
     */
    protected appEntityService: DiscountTypeService = new DiscountTypeService;

    /**
     * 实体权限服务对象
     *
     * @type DiscountTypeUIService
     * @memberof DiscountTypePickupGridViewBase
     */
    public appUIService: DiscountTypeUIService = new DiscountTypeUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof DiscountTypePickupGridViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof DiscountTypePickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.discounttype.views.pickupgridview.caption',
        srfTitle: 'entities.discounttype.views.pickupgridview.title',
        srfSubTitle: 'entities.discounttype.views.pickupgridview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof DiscountTypePickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: { name: 'grid', type: 'GRID' },
        view_searchform: { name: 'searchform', type: 'SEARCHFORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'b9f7052ccfa49f637ea5fa990e1e56f0';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof DiscountTypePickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof DiscountTypePickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'discounttype',
            majorPSDEField: 'discounttypename',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof DiscountTypePickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof DiscountTypePickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof DiscountTypePickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof DiscountTypePickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof DiscountTypePickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof DiscountTypePickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof DiscountTypePickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof DiscountTypePickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}