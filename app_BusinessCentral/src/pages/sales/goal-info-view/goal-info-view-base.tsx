import { Subject } from 'rxjs';
import { TabExpViewBase } from '@/studio-core';
import GoalService from '@/service/goal/goal-service';
import GoalAuthService from '@/authservice/goal/goal-auth-service';
import TabExpViewEngine from '@engine/view/tab-exp-view-engine';
import GoalUIService from '@/uiservice/goal/goal-ui-service';

/**
 * 目标信息视图基类
 *
 * @export
 * @class GoalInfoViewBase
 * @extends {TabExpViewBase}
 */
export class GoalInfoViewBase extends TabExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof GoalInfoViewBase
     */
    protected appDeName: string = 'goal';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof GoalInfoViewBase
     */
    protected appDeKey: string = 'goalid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof GoalInfoViewBase
     */
    protected appDeMajor: string = 'title';

    /**
     * 实体服务对象
     *
     * @type {GoalService}
     * @memberof GoalInfoViewBase
     */
    protected appEntityService: GoalService = new GoalService;

    /**
     * 实体权限服务对象
     *
     * @type GoalUIService
     * @memberof GoalInfoViewBase
     */
    public appUIService: GoalUIService = new GoalUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof GoalInfoViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof GoalInfoViewBase
     */
    protected model: any = {
        srfCaption: 'entities.goal.views.infoview.caption',
        srfTitle: 'entities.goal.views.infoview.title',
        srfSubTitle: 'entities.goal.views.infoview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof GoalInfoViewBase
     */
    protected containerModel: any = {
        view_toolbar: { name: 'toolbar', type: 'TOOLBAR' },
        view_tabexppanel: { name: 'tabexppanel', type: 'TABEXPPANEL' },
    };

    /**
     * 工具栏模型
     *
     * @type {*}
     * @memberof GoalInfoView
     */
    public toolBarModels: any = {
        tbitem1_openmaineditview: { name: 'tbitem1_openmaineditview', caption: '编辑', 'isShowCaption': true, 'isShowIcon': true, tooltip: '编辑', iconcls: 'fa fa-edit', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'OpenMainEditView', target: 'SINGLEKEY', class: '' } },

        tbitem1_remove_sep: {  name: 'tbitem1_remove_sep', type: 'SEPERATOR', visabled: true, dataaccaction: '', uiaction: { } },
        tbitem1_remove: { name: 'tbitem1_remove', caption: '删除并关闭', 'isShowCaption': true, 'isShowIcon': true, tooltip: '删除并关闭', iconcls: 'fa fa-remove', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Remove', target: 'SINGLEKEY', class: '' } },

        seperator4: {  name: 'seperator4', type: 'SEPERATOR', visabled: true, dataaccaction: '', uiaction: { } },
        tbitem17_active: { name: 'tbitem17_active', caption: '激活', 'isShowCaption': true, 'isShowIcon': true, tooltip: '激活', iconcls: 'fa fa-play-circle-o', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Active', target: 'SINGLEKEY', class: '' } },

        tbitem17_close_sep: {  name: 'tbitem17_close_sep', type: 'SEPERATOR', visabled: true, dataaccaction: '', uiaction: { } },
        tbitem17_close: { name: 'tbitem17_close', caption: '关闭目标', 'isShowCaption': true, 'isShowIcon': true, tooltip: '关闭目标', iconcls: 'fa fa-minus-circle', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Close', target: 'SINGLEKEY', class: '' } },

        tbitem2: {  name: 'tbitem2', type: 'SEPERATOR', visabled: true, dataaccaction: '', uiaction: { } },
        tbitem12: { name: 'tbitem12', caption: '关闭', 'isShowCaption': true, 'isShowIcon': true, tooltip: '关闭', iconcls: 'fa fa-sign-out', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Exit', target: '', class: '' } },

    };



	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '7cb9d17716b8adc5677efc79bf190953';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof GoalInfoViewBase
     */
    public engine: TabExpViewEngine = new TabExpViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof GoalInfoViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            keyPSDEField: 'goal',
            majorPSDEField: 'title',
            isLoadDefault: true,
        });
    }

    /**
     * toolbar 部件 click 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof GoalInfoViewBase
     */
    public toolbar_click($event: any, $event2?: any): void {
        if (Object.is($event.tag, 'tbitem1_openmaineditview')) {
            this.toolbar_tbitem1_openmaineditview_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem1_remove')) {
            this.toolbar_tbitem1_remove_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem17_active')) {
            this.toolbar_tbitem17_active_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem17_close')) {
            this.toolbar_tbitem17_close_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem12')) {
            this.toolbar_tbitem12_click(null, '', $event2);
        }
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem1_openmaineditview_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:GoalUIService  = new GoalUIService();
        curUIService.Goal_OpenMainEditView(datas,contextJO, paramJO,  $event, xData,this,"Goal");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem1_remove_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:GoalUIService  = new GoalUIService();
        curUIService.Goal_Remove(datas,contextJO, paramJO,  $event, xData,this,"Goal");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem17_active_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:GoalUIService  = new GoalUIService();
        curUIService.Goal_Active(datas,contextJO, paramJO,  $event, xData,this,"Goal");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem17_close_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:GoalUIService  = new GoalUIService();
        curUIService.Goal_Close(datas,contextJO, paramJO,  $event, xData,this,"Goal");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem12_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        this.Exit(datas, contextJO,paramJO,  $event, xData,this,"Goal");
    }

    /**
     * 关闭
     *
     * @param {any[]} args 当前数据
     * @param {any} contextJO 行为附加上下文
     * @param {*} [params] 附加参数
     * @param {*} [$event] 事件源
     * @param {*} [xData]  执行行为所需当前部件
     * @param {*} [actionContext]  执行行为上下文
     * @memberof GoalInfoViewBase
     */
    public Exit(args: any[],contextJO?:any, params?: any, $event?: any, xData?: any,actionContext?:any,srfParentDeName?:string) {
        this.closeView(args);
        if(window.parent){
            window.parent.postMessage([{ ...args }],'*');
        }
    }


}