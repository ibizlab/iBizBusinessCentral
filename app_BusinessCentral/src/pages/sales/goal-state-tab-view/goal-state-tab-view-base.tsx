import { Subject } from 'rxjs';
import { TabExpViewBase } from '@/studio-core';
import GoalService from '@/service/goal/goal-service';
import GoalAuthService from '@/authservice/goal/goal-auth-service';
import TabExpViewEngine from '@engine/view/tab-exp-view-engine';
import GoalUIService from '@/uiservice/goal/goal-ui-service';

/**
 * 目标视图基类
 *
 * @export
 * @class GoalStateTabViewBase
 * @extends {TabExpViewBase}
 */
export class GoalStateTabViewBase extends TabExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof GoalStateTabViewBase
     */
    protected appDeName: string = 'goal';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof GoalStateTabViewBase
     */
    protected appDeKey: string = 'goalid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof GoalStateTabViewBase
     */
    protected appDeMajor: string = 'title';

    /**
     * 实体服务对象
     *
     * @type {GoalService}
     * @memberof GoalStateTabViewBase
     */
    protected appEntityService: GoalService = new GoalService;

    /**
     * 实体权限服务对象
     *
     * @type GoalUIService
     * @memberof GoalStateTabViewBase
     */
    public appUIService: GoalUIService = new GoalUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof GoalStateTabViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof GoalStateTabViewBase
     */
    protected model: any = {
        srfCaption: 'entities.goal.views.statetabview.caption',
        srfTitle: 'entities.goal.views.statetabview.title',
        srfSubTitle: 'entities.goal.views.statetabview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof GoalStateTabViewBase
     */
    protected containerModel: any = {
        view_tabexppanel: { name: 'tabexppanel', type: 'TABEXPPANEL' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '22e8a76fa7fd741d5924fb98604b46f0';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof GoalStateTabViewBase
     */
    public engine: TabExpViewEngine = new TabExpViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof GoalStateTabViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            keyPSDEField: 'goal',
            majorPSDEField: 'title',
            isLoadDefault: true,
        });
    }


}