import { Subject } from 'rxjs';
import { DashboardViewBase } from '@/studio-core';
import SalesLiteratureService from '@/service/sales-literature/sales-literature-service';
import SalesLiteratureAuthService from '@/authservice/sales-literature/sales-literature-auth-service';
import PortalViewEngine from '@engine/view/portal-view-engine';
import SalesLiteratureUIService from '@/uiservice/sales-literature/sales-literature-ui-service';

/**
 * 销售宣传资料数据看板视图视图基类
 *
 * @export
 * @class SalesLiteratureSummaryViewBase
 * @extends {DashboardViewBase}
 */
export class SalesLiteratureSummaryViewBase extends DashboardViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof SalesLiteratureSummaryViewBase
     */
    protected appDeName: string = 'salesliterature';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof SalesLiteratureSummaryViewBase
     */
    protected appDeKey: string = 'salesliteratureid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof SalesLiteratureSummaryViewBase
     */
    protected appDeMajor: string = 'salesliteraturename';

    /**
     * 实体服务对象
     *
     * @type {SalesLiteratureService}
     * @memberof SalesLiteratureSummaryViewBase
     */
    protected appEntityService: SalesLiteratureService = new SalesLiteratureService;

    /**
     * 实体权限服务对象
     *
     * @type SalesLiteratureUIService
     * @memberof SalesLiteratureSummaryViewBase
     */
    public appUIService: SalesLiteratureUIService = new SalesLiteratureUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof SalesLiteratureSummaryViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof SalesLiteratureSummaryViewBase
     */
    protected model: any = {
        srfCaption: 'entities.salesliterature.views.summaryview.caption',
        srfTitle: 'entities.salesliterature.views.summaryview.title',
        srfSubTitle: 'entities.salesliterature.views.summaryview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof SalesLiteratureSummaryViewBase
     */
    protected containerModel: any = {
        view_dashboard: { name: 'dashboard', type: 'DASHBOARD' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '8e580d9d78c961242281b0a4b8ce5441';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof SalesLiteratureSummaryViewBase
     */
    public engine: PortalViewEngine = new PortalViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof SalesLiteratureSummaryViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            dashboard: this.$refs.dashboard,
            keyPSDEField: 'salesliterature',
            majorPSDEField: 'salesliteraturename',
            isLoadDefault: true,
        });
    }

    /**
     * dashboard 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SalesLiteratureSummaryViewBase
     */
    public dashboard_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('dashboard', 'load', $event);
    }


}