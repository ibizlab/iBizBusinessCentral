import { Subject } from 'rxjs';
import { TabExpViewBase } from '@/studio-core';
import OpportunityService from '@/service/opportunity/opportunity-service';
import OpportunityAuthService from '@/authservice/opportunity/opportunity-auth-service';
import TabExpViewEngine from '@engine/view/tab-exp-view-engine';
import OpportunityUIService from '@/uiservice/opportunity/opportunity-ui-service';

/**
 * 商机信息视图基类
 *
 * @export
 * @class OpportunityStateTabViewBase
 * @extends {TabExpViewBase}
 */
export class OpportunityStateTabViewBase extends TabExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof OpportunityStateTabViewBase
     */
    protected appDeName: string = 'opportunity';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof OpportunityStateTabViewBase
     */
    protected appDeKey: string = 'opportunityid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof OpportunityStateTabViewBase
     */
    protected appDeMajor: string = 'opportunityname';

    /**
     * 实体服务对象
     *
     * @type {OpportunityService}
     * @memberof OpportunityStateTabViewBase
     */
    protected appEntityService: OpportunityService = new OpportunityService;

    /**
     * 实体权限服务对象
     *
     * @type OpportunityUIService
     * @memberof OpportunityStateTabViewBase
     */
    public appUIService: OpportunityUIService = new OpportunityUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof OpportunityStateTabViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof OpportunityStateTabViewBase
     */
    protected model: any = {
        srfCaption: 'entities.opportunity.views.statetabview.caption',
        srfTitle: 'entities.opportunity.views.statetabview.title',
        srfSubTitle: 'entities.opportunity.views.statetabview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof OpportunityStateTabViewBase
     */
    protected containerModel: any = {
        view_tabexppanel: { name: 'tabexppanel', type: 'TABEXPPANEL' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'e20fd7e712b02e804ecee443eb428ad5';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof OpportunityStateTabViewBase
     */
    public engine: TabExpViewEngine = new TabExpViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof OpportunityStateTabViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            keyPSDEField: 'opportunity',
            majorPSDEField: 'opportunityname',
            isLoadDefault: true,
        });
    }


}