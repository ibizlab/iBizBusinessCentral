import { Subject } from 'rxjs';
import { TabExpViewBase } from '@/studio-core';
import LeadService from '@/service/lead/lead-service';
import LeadAuthService from '@/authservice/lead/lead-auth-service';
import TabExpViewEngine from '@engine/view/tab-exp-view-engine';
import LeadUIService from '@/uiservice/lead/lead-ui-service';

/**
 * 潜在顾客视图基类
 *
 * @export
 * @class LeadStatusTabViewBase
 * @extends {TabExpViewBase}
 */
export class LeadStatusTabViewBase extends TabExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof LeadStatusTabViewBase
     */
    protected appDeName: string = 'lead';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof LeadStatusTabViewBase
     */
    protected appDeKey: string = 'leadid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof LeadStatusTabViewBase
     */
    protected appDeMajor: string = 'fullname';

    /**
     * 实体服务对象
     *
     * @type {LeadService}
     * @memberof LeadStatusTabViewBase
     */
    protected appEntityService: LeadService = new LeadService;

    /**
     * 实体权限服务对象
     *
     * @type LeadUIService
     * @memberof LeadStatusTabViewBase
     */
    public appUIService: LeadUIService = new LeadUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof LeadStatusTabViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof LeadStatusTabViewBase
     */
    protected model: any = {
        srfCaption: 'entities.lead.views.statustabview.caption',
        srfTitle: 'entities.lead.views.statustabview.title',
        srfSubTitle: 'entities.lead.views.statustabview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof LeadStatusTabViewBase
     */
    protected containerModel: any = {
        view_tabexppanel: { name: 'tabexppanel', type: 'TABEXPPANEL' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '22c9503382246c4d49455f2b62fcd54b';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof LeadStatusTabViewBase
     */
    public engine: TabExpViewEngine = new TabExpViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof LeadStatusTabViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            keyPSDEField: 'lead',
            majorPSDEField: 'fullname',
            isLoadDefault: true,
        });
    }


}