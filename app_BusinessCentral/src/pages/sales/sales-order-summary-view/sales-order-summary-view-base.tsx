import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { DashboardViewBase } from '@/studio-core';
import SalesOrderService from '@/service/sales-order/sales-order-service';
import SalesOrderAuthService from '@/authservice/sales-order/sales-order-auth-service';
import PortalViewEngine from '@engine/view/portal-view-engine';
import SalesOrderUIService from '@/uiservice/sales-order/sales-order-ui-service';

/**
 * 订单数据看板视图视图基类
 *
 * @export
 * @class SalesOrderSummaryViewBase
 * @extends {DashboardViewBase}
 */
export class SalesOrderSummaryViewBase extends DashboardViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof SalesOrderSummaryViewBase
     */
    protected appDeName: string = 'salesorder';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof SalesOrderSummaryViewBase
     */
    protected appDeKey: string = 'salesorderid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof SalesOrderSummaryViewBase
     */
    protected appDeMajor: string = 'salesordername';

    /**
     * 实体服务对象
     *
     * @type {SalesOrderService}
     * @memberof SalesOrderSummaryViewBase
     */
    protected appEntityService: SalesOrderService = new SalesOrderService;

    /**
     * 实体权限服务对象
     *
     * @type SalesOrderUIService
     * @memberof SalesOrderSummaryViewBase
     */
    public appUIService: SalesOrderUIService = new SalesOrderUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof SalesOrderSummaryViewBase
     */    
    protected counterServiceArray: Array<any> = [];

	/**
	 * 自定义视图导航上下文集合
	 *
     * @protected
	 * @type {*}
	 * @memberof SalesOrderSummaryViewBase
	 */
    protected customViewNavContexts: any = {
        'REGARDINGOBJECTID': { isRawValue: false, value: 'salesorder' },
        'REGARDINGOBJECTTYPECODE': { isRawValue: true, value: 'SALESORDER' }
    };

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof SalesOrderSummaryViewBase
     */
    protected model: any = {
        srfCaption: 'entities.salesorder.views.summaryview.caption',
        srfTitle: 'entities.salesorder.views.summaryview.title',
        srfSubTitle: 'entities.salesorder.views.summaryview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof SalesOrderSummaryViewBase
     */
    protected containerModel: any = {
        view_dashboard: { name: 'dashboard', type: 'DASHBOARD' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '9a96c108dcc1998611eca28f1232990b';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof SalesOrderSummaryViewBase
     */
    public engine: PortalViewEngine = new PortalViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof SalesOrderSummaryViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            dashboard: this.$refs.dashboard,
            keyPSDEField: 'salesorder',
            majorPSDEField: 'salesordername',
            isLoadDefault: true,
        });
    }

    /**
     * dashboard 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SalesOrderSummaryViewBase
     */
    public dashboard_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('dashboard', 'load', $event);
    }


}