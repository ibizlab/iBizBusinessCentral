import { Subject } from 'rxjs';
import { OptionViewBase } from '@/studio-core';
import SalesLiteratureService from '@/service/sales-literature/sales-literature-service';
import SalesLiteratureAuthService from '@/authservice/sales-literature/sales-literature-auth-service';
import OptionViewEngine from '@engine/view/option-view-engine';
import SalesLiteratureUIService from '@/uiservice/sales-literature/sales-literature-ui-service';

/**
 * 快速新建视图基类
 *
 * @export
 * @class SalesLiteratureQuickCreateViewBase
 * @extends {OptionViewBase}
 */
export class SalesLiteratureQuickCreateViewBase extends OptionViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof SalesLiteratureQuickCreateViewBase
     */
    protected appDeName: string = 'salesliterature';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof SalesLiteratureQuickCreateViewBase
     */
    protected appDeKey: string = 'salesliteratureid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof SalesLiteratureQuickCreateViewBase
     */
    protected appDeMajor: string = 'salesliteraturename';

    /**
     * 实体服务对象
     *
     * @type {SalesLiteratureService}
     * @memberof SalesLiteratureQuickCreateViewBase
     */
    protected appEntityService: SalesLiteratureService = new SalesLiteratureService;

    /**
     * 实体权限服务对象
     *
     * @type SalesLiteratureUIService
     * @memberof SalesLiteratureQuickCreateViewBase
     */
    public appUIService: SalesLiteratureUIService = new SalesLiteratureUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof SalesLiteratureQuickCreateViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof SalesLiteratureQuickCreateViewBase
     */
    protected model: any = {
        srfCaption: 'entities.salesliterature.views.quickcreateview.caption',
        srfTitle: 'entities.salesliterature.views.quickcreateview.title',
        srfSubTitle: 'entities.salesliterature.views.quickcreateview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof SalesLiteratureQuickCreateViewBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'defd2767cd83fe2280d92fa6f611c579';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof SalesLiteratureQuickCreateViewBase
     */
    public engine: OptionViewEngine = new OptionViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof SalesLiteratureQuickCreateViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'salesliterature',
            majorPSDEField: 'salesliteraturename',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SalesLiteratureQuickCreateViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SalesLiteratureQuickCreateViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SalesLiteratureQuickCreateViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}