import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import DiscountTypeService from '@/service/discount-type/discount-type-service';
import DiscountTypeAuthService from '@/authservice/discount-type/discount-type-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import DiscountTypeUIService from '@/uiservice/discount-type/discount-type-ui-service';

/**
 * 折扣表数据选择视图视图基类
 *
 * @export
 * @class DiscountTypePickupViewBase
 * @extends {PickupViewBase}
 */
export class DiscountTypePickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof DiscountTypePickupViewBase
     */
    protected appDeName: string = 'discounttype';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof DiscountTypePickupViewBase
     */
    protected appDeKey: string = 'discounttypeid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof DiscountTypePickupViewBase
     */
    protected appDeMajor: string = 'discounttypename';

    /**
     * 实体服务对象
     *
     * @type {DiscountTypeService}
     * @memberof DiscountTypePickupViewBase
     */
    protected appEntityService: DiscountTypeService = new DiscountTypeService;

    /**
     * 实体权限服务对象
     *
     * @type DiscountTypeUIService
     * @memberof DiscountTypePickupViewBase
     */
    public appUIService: DiscountTypeUIService = new DiscountTypeUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof DiscountTypePickupViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof DiscountTypePickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.discounttype.views.pickupview.caption',
        srfTitle: 'entities.discounttype.views.pickupview.title',
        srfSubTitle: 'entities.discounttype.views.pickupview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof DiscountTypePickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: { name: 'pickupviewpanel', type: 'PICKUPVIEWPANEL' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'e4da1aeb86d3b3de7fbca392bbbae64f';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof DiscountTypePickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof DiscountTypePickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'discounttype',
            majorPSDEField: 'discounttypename',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof DiscountTypePickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof DiscountTypePickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof DiscountTypePickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}