import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { DashboardViewBase } from '@/studio-core';
import QuoteService from '@/service/quote/quote-service';
import QuoteAuthService from '@/authservice/quote/quote-auth-service';
import PortalViewEngine from '@engine/view/portal-view-engine';
import QuoteUIService from '@/uiservice/quote/quote-ui-service';

/**
 * 报价单数据看板视图视图基类
 *
 * @export
 * @class QuoteSummaryBase
 * @extends {DashboardViewBase}
 */
export class QuoteSummaryBase extends DashboardViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof QuoteSummaryBase
     */
    protected appDeName: string = 'quote';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof QuoteSummaryBase
     */
    protected appDeKey: string = 'quoteid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof QuoteSummaryBase
     */
    protected appDeMajor: string = 'quotename';

    /**
     * 实体服务对象
     *
     * @type {QuoteService}
     * @memberof QuoteSummaryBase
     */
    protected appEntityService: QuoteService = new QuoteService;

    /**
     * 实体权限服务对象
     *
     * @type QuoteUIService
     * @memberof QuoteSummaryBase
     */
    public appUIService: QuoteUIService = new QuoteUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof QuoteSummaryBase
     */    
    protected counterServiceArray: Array<any> = [];

	/**
	 * 自定义视图导航上下文集合
	 *
     * @protected
	 * @type {*}
	 * @memberof QuoteSummaryBase
	 */
    protected customViewNavContexts: any = {
        'REGARDINGOBJECTID': { isRawValue: false, value: 'quote' },
        'REGARDINGOBJECTTYPECODE': { isRawValue: true, value: 'QUOTE' }
    };

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof QuoteSummaryBase
     */
    protected model: any = {
        srfCaption: 'entities.quote.views.summary.caption',
        srfTitle: 'entities.quote.views.summary.title',
        srfSubTitle: 'entities.quote.views.summary.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof QuoteSummaryBase
     */
    protected containerModel: any = {
        view_dashboard: { name: 'dashboard', type: 'DASHBOARD' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '760898155bde111279981be640053efb';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof QuoteSummaryBase
     */
    public engine: PortalViewEngine = new PortalViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof QuoteSummaryBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            dashboard: this.$refs.dashboard,
            keyPSDEField: 'quote',
            majorPSDEField: 'quotename',
            isLoadDefault: true,
        });
    }

    /**
     * dashboard 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof QuoteSummaryBase
     */
    public dashboard_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('dashboard', 'load', $event);
    }


}