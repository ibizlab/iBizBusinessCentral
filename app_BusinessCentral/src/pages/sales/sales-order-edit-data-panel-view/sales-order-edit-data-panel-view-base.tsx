import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { EditViewBase } from '@/studio-core';
import SalesOrderService from '@/service/sales-order/sales-order-service';
import SalesOrderAuthService from '@/authservice/sales-order/sales-order-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import SalesOrderUIService from '@/uiservice/sales-order/sales-order-ui-service';

/**
 * 头部信息编辑视图基类
 *
 * @export
 * @class SalesOrderEdit_DataPanelViewBase
 * @extends {EditViewBase}
 */
export class SalesOrderEdit_DataPanelViewBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof SalesOrderEdit_DataPanelViewBase
     */
    protected appDeName: string = 'salesorder';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof SalesOrderEdit_DataPanelViewBase
     */
    protected appDeKey: string = 'salesorderid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof SalesOrderEdit_DataPanelViewBase
     */
    protected appDeMajor: string = 'salesordername';

    /**
     * 实体服务对象
     *
     * @type {SalesOrderService}
     * @memberof SalesOrderEdit_DataPanelViewBase
     */
    protected appEntityService: SalesOrderService = new SalesOrderService;

    /**
     * 实体权限服务对象
     *
     * @type SalesOrderUIService
     * @memberof SalesOrderEdit_DataPanelViewBase
     */
    public appUIService: SalesOrderUIService = new SalesOrderUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof SalesOrderEdit_DataPanelViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof SalesOrderEdit_DataPanelViewBase
     */
    protected model: any = {
        srfCaption: 'entities.salesorder.views.edit_datapanelview.caption',
        srfTitle: 'entities.salesorder.views.edit_datapanelview.title',
        srfSubTitle: 'entities.salesorder.views.edit_datapanelview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof SalesOrderEdit_DataPanelViewBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'b32bb4d2c91988b508d51776c83feb0c';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof SalesOrderEdit_DataPanelViewBase
     */
    public engine: EditViewEngine = new EditViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof SalesOrderEdit_DataPanelViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'salesorder',
            majorPSDEField: 'salesordername',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SalesOrderEdit_DataPanelViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SalesOrderEdit_DataPanelViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SalesOrderEdit_DataPanelViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}