import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import SalesOrderService from '@/service/sales-order/sales-order-service';
import SalesOrderAuthService from '@/authservice/sales-order/sales-order-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import SalesOrderUIService from '@/uiservice/sales-order/sales-order-ui-service';

/**
 * 订单选择表格视图视图基类
 *
 * @export
 * @class SalesOrderPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class SalesOrderPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof SalesOrderPickupGridViewBase
     */
    protected appDeName: string = 'salesorder';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof SalesOrderPickupGridViewBase
     */
    protected appDeKey: string = 'salesorderid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof SalesOrderPickupGridViewBase
     */
    protected appDeMajor: string = 'salesordername';

    /**
     * 实体服务对象
     *
     * @type {SalesOrderService}
     * @memberof SalesOrderPickupGridViewBase
     */
    protected appEntityService: SalesOrderService = new SalesOrderService;

    /**
     * 实体权限服务对象
     *
     * @type SalesOrderUIService
     * @memberof SalesOrderPickupGridViewBase
     */
    public appUIService: SalesOrderUIService = new SalesOrderUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof SalesOrderPickupGridViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof SalesOrderPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.salesorder.views.pickupgridview.caption',
        srfTitle: 'entities.salesorder.views.pickupgridview.title',
        srfSubTitle: 'entities.salesorder.views.pickupgridview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof SalesOrderPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: { name: 'grid', type: 'GRID' },
        view_searchform: { name: 'searchform', type: 'SEARCHFORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '5dc8b0d0b7d02f854260d837787a81ec';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof SalesOrderPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof SalesOrderPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'salesorder',
            majorPSDEField: 'salesordername',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SalesOrderPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SalesOrderPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SalesOrderPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SalesOrderPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SalesOrderPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SalesOrderPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof SalesOrderPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof SalesOrderPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}