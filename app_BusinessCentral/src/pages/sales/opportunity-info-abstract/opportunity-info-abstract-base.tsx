import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { EditViewBase } from '@/studio-core';
import OpportunityService from '@/service/opportunity/opportunity-service';
import OpportunityAuthService from '@/authservice/opportunity/opportunity-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import OpportunityUIService from '@/uiservice/opportunity/opportunity-ui-service';

/**
 * 商机编辑视图视图基类
 *
 * @export
 * @class OpportunityInfo_AbstractBase
 * @extends {EditViewBase}
 */
export class OpportunityInfo_AbstractBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof OpportunityInfo_AbstractBase
     */
    protected appDeName: string = 'opportunity';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof OpportunityInfo_AbstractBase
     */
    protected appDeKey: string = 'opportunityid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof OpportunityInfo_AbstractBase
     */
    protected appDeMajor: string = 'opportunityname';

    /**
     * 实体服务对象
     *
     * @type {OpportunityService}
     * @memberof OpportunityInfo_AbstractBase
     */
    protected appEntityService: OpportunityService = new OpportunityService;

    /**
     * 实体权限服务对象
     *
     * @type OpportunityUIService
     * @memberof OpportunityInfo_AbstractBase
     */
    public appUIService: OpportunityUIService = new OpportunityUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof OpportunityInfo_AbstractBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof OpportunityInfo_AbstractBase
     */
    protected model: any = {
        srfCaption: 'entities.opportunity.views.info_abstract.caption',
        srfTitle: 'entities.opportunity.views.info_abstract.title',
        srfSubTitle: 'entities.opportunity.views.info_abstract.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof OpportunityInfo_AbstractBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'ff1553675274b115b167253dd1cf550c';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof OpportunityInfo_AbstractBase
     */
    public engine: EditViewEngine = new EditViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof OpportunityInfo_AbstractBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'opportunity',
            majorPSDEField: 'opportunityname',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof OpportunityInfo_AbstractBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof OpportunityInfo_AbstractBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof OpportunityInfo_AbstractBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}