import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { MPickupViewBase } from '@/studio-core';
import LeadService from '@/service/lead/lead-service';
import LeadAuthService from '@/authservice/lead/lead-auth-service';
import MPickupViewEngine from '@engine/view/mpickup-view-engine';
import LeadUIService from '@/uiservice/lead/lead-ui-service';

/**
 * 潜在顾客数据多项选择视图视图基类
 *
 * @export
 * @class LeadMPickupViewBase
 * @extends {MPickupViewBase}
 */
export class LeadMPickupViewBase extends MPickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof LeadMPickupViewBase
     */
    protected appDeName: string = 'lead';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof LeadMPickupViewBase
     */
    protected appDeKey: string = 'leadid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof LeadMPickupViewBase
     */
    protected appDeMajor: string = 'fullname';

    /**
     * 实体服务对象
     *
     * @type {LeadService}
     * @memberof LeadMPickupViewBase
     */
    protected appEntityService: LeadService = new LeadService;

    /**
     * 实体权限服务对象
     *
     * @type LeadUIService
     * @memberof LeadMPickupViewBase
     */
    public appUIService: LeadUIService = new LeadUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof LeadMPickupViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof LeadMPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.lead.views.mpickupview.caption',
        srfTitle: 'entities.lead.views.mpickupview.title',
        srfSubTitle: 'entities.lead.views.mpickupview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof LeadMPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: { name: 'pickupviewpanel', type: 'PICKUPVIEWPANEL' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '472a562213edd8e96417a52a48bc0548';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof LeadMPickupViewBase
     */
    public engine: MPickupViewEngine = new MPickupViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof LeadMPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'lead',
            majorPSDEField: 'fullname',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof LeadMPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof LeadMPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof LeadMPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }



    /**
     * 添加左侧面板所有数据到右侧
     *
     * @memberof LeadMPickupView
     */
    public onCLickAllRight(): void {
        Object.values(this.containerModel).forEach((model: any) => {
            if (!Object.is(model.type, 'PICKUPVIEWPANEL')) {
                return;
            }
            if (model.datas.length > 0) {
                model.datas.forEach((data: any, index: any) => {
                    Object.assign(data, { srfmajortext: data['fullname'] });
                })
            }
            model.datas.forEach((item: any) => {
                const index: number = this.viewSelections.findIndex((selection: any) => Object.is(item.srfkey, selection.srfkey));
                if (index === -1) {
                    item._select = false;
                    this.viewSelections.push(item);
                }
            });
        });
        this.selectedData = JSON.stringify(this.viewSelections);
    }


}