import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { DashboardViewBase } from '@/studio-core';
import OpportunityService from '@/service/opportunity/opportunity-service';
import OpportunityAuthService from '@/authservice/opportunity/opportunity-auth-service';
import PortalViewEngine from '@engine/view/portal-view-engine';
import OpportunityUIService from '@/uiservice/opportunity/opportunity-ui-service';

/**
 * 商机概览视图基类
 *
 * @export
 * @class OpportunitySummaryBase
 * @extends {DashboardViewBase}
 */
export class OpportunitySummaryBase extends DashboardViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof OpportunitySummaryBase
     */
    protected appDeName: string = 'opportunity';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof OpportunitySummaryBase
     */
    protected appDeKey: string = 'opportunityid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof OpportunitySummaryBase
     */
    protected appDeMajor: string = 'opportunityname';

    /**
     * 实体服务对象
     *
     * @type {OpportunityService}
     * @memberof OpportunitySummaryBase
     */
    protected appEntityService: OpportunityService = new OpportunityService;

    /**
     * 实体权限服务对象
     *
     * @type OpportunityUIService
     * @memberof OpportunitySummaryBase
     */
    public appUIService: OpportunityUIService = new OpportunityUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof OpportunitySummaryBase
     */    
    protected counterServiceArray: Array<any> = [];

	/**
	 * 自定义视图导航上下文集合
	 *
     * @protected
	 * @type {*}
	 * @memberof OpportunitySummaryBase
	 */
    protected customViewNavContexts: any = {
        'REGARDINGOBJECTID': { isRawValue: false, value: 'opportunity' },
        'REGARDINGOBJECTTYPECODE': { isRawValue: true, value: 'OPPORTUNITY' }
    };

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof OpportunitySummaryBase
     */
    protected model: any = {
        srfCaption: 'entities.opportunity.views.summary.caption',
        srfTitle: 'entities.opportunity.views.summary.title',
        srfSubTitle: 'entities.opportunity.views.summary.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof OpportunitySummaryBase
     */
    protected containerModel: any = {
        view_dashboard: { name: 'dashboard', type: 'DASHBOARD' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '0845ca2722fffcce4896282c8f4eaa54';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof OpportunitySummaryBase
     */
    public engine: PortalViewEngine = new PortalViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof OpportunitySummaryBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            dashboard: this.$refs.dashboard,
            keyPSDEField: 'opportunity',
            majorPSDEField: 'opportunityname',
            isLoadDefault: true,
        });
    }

    /**
     * dashboard 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof OpportunitySummaryBase
     */
    public dashboard_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('dashboard', 'load', $event);
    }


}