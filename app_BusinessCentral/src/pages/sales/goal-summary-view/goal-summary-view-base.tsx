import { Subject } from 'rxjs';
import { DashboardViewBase } from '@/studio-core';
import GoalService from '@/service/goal/goal-service';
import GoalAuthService from '@/authservice/goal/goal-auth-service';
import PortalViewEngine from '@engine/view/portal-view-engine';
import GoalUIService from '@/uiservice/goal/goal-ui-service';

/**
 * 目标数据看板视图视图基类
 *
 * @export
 * @class GoalSummaryViewBase
 * @extends {DashboardViewBase}
 */
export class GoalSummaryViewBase extends DashboardViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof GoalSummaryViewBase
     */
    protected appDeName: string = 'goal';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof GoalSummaryViewBase
     */
    protected appDeKey: string = 'goalid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof GoalSummaryViewBase
     */
    protected appDeMajor: string = 'title';

    /**
     * 实体服务对象
     *
     * @type {GoalService}
     * @memberof GoalSummaryViewBase
     */
    protected appEntityService: GoalService = new GoalService;

    /**
     * 实体权限服务对象
     *
     * @type GoalUIService
     * @memberof GoalSummaryViewBase
     */
    public appUIService: GoalUIService = new GoalUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof GoalSummaryViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof GoalSummaryViewBase
     */
    protected model: any = {
        srfCaption: 'entities.goal.views.summaryview.caption',
        srfTitle: 'entities.goal.views.summaryview.title',
        srfSubTitle: 'entities.goal.views.summaryview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof GoalSummaryViewBase
     */
    protected containerModel: any = {
        view_dashboard: { name: 'dashboard', type: 'DASHBOARD' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '42587b9e3ef774294165ad5687e49442';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof GoalSummaryViewBase
     */
    public engine: PortalViewEngine = new PortalViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof GoalSummaryViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            dashboard: this.$refs.dashboard,
            keyPSDEField: 'goal',
            majorPSDEField: 'title',
            isLoadDefault: true,
        });
    }

    /**
     * dashboard 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof GoalSummaryViewBase
     */
    public dashboard_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('dashboard', 'load', $event);
    }


}