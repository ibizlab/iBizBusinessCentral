import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import IBZPostService from '@/service/ibzpost/ibzpost-service';
import IBZPostAuthService from '@/authservice/ibzpost/ibzpost-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import IBZPostUIService from '@/uiservice/ibzpost/ibzpost-ui-service';

/**
 * 岗位数据选择视图视图基类
 *
 * @export
 * @class IBZPostPickupViewBase
 * @extends {PickupViewBase}
 */
export class IBZPostPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof IBZPostPickupViewBase
     */
    protected appDeName: string = 'ibzpost';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof IBZPostPickupViewBase
     */
    protected appDeKey: string = 'postid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof IBZPostPickupViewBase
     */
    protected appDeMajor: string = 'postname';

    /**
     * 实体服务对象
     *
     * @type {IBZPostService}
     * @memberof IBZPostPickupViewBase
     */
    protected appEntityService: IBZPostService = new IBZPostService;

    /**
     * 实体权限服务对象
     *
     * @type IBZPostUIService
     * @memberof IBZPostPickupViewBase
     */
    public appUIService: IBZPostUIService = new IBZPostUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof IBZPostPickupViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof IBZPostPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.ibzpost.views.pickupview.caption',
        srfTitle: 'entities.ibzpost.views.pickupview.title',
        srfSubTitle: 'entities.ibzpost.views.pickupview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof IBZPostPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: { name: 'pickupviewpanel', type: 'PICKUPVIEWPANEL' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'cf7af9f3892de5fd8c05e9d23bdbb77a';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof IBZPostPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof IBZPostPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'ibzpost',
            majorPSDEField: 'postname',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBZPostPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBZPostPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBZPostPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}