import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import IBZOrganizationService from '@/service/ibzorganization/ibzorganization-service';
import IBZOrganizationAuthService from '@/authservice/ibzorganization/ibzorganization-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import IBZOrganizationUIService from '@/uiservice/ibzorganization/ibzorganization-ui-service';

/**
 * 单位机构选择表格视图视图基类
 *
 * @export
 * @class IBZOrganizationPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class IBZOrganizationPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof IBZOrganizationPickupGridViewBase
     */
    protected appDeName: string = 'ibzorganization';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof IBZOrganizationPickupGridViewBase
     */
    protected appDeKey: string = 'orgid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof IBZOrganizationPickupGridViewBase
     */
    protected appDeMajor: string = 'orgname';

    /**
     * 实体服务对象
     *
     * @type {IBZOrganizationService}
     * @memberof IBZOrganizationPickupGridViewBase
     */
    protected appEntityService: IBZOrganizationService = new IBZOrganizationService;

    /**
     * 实体权限服务对象
     *
     * @type IBZOrganizationUIService
     * @memberof IBZOrganizationPickupGridViewBase
     */
    public appUIService: IBZOrganizationUIService = new IBZOrganizationUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof IBZOrganizationPickupGridViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof IBZOrganizationPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.ibzorganization.views.pickupgridview.caption',
        srfTitle: 'entities.ibzorganization.views.pickupgridview.title',
        srfSubTitle: 'entities.ibzorganization.views.pickupgridview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof IBZOrganizationPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: { name: 'grid', type: 'GRID' },
        view_searchform: { name: 'searchform', type: 'SEARCHFORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'ec5b5212220d0036f639bf637ed38119';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof IBZOrganizationPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof IBZOrganizationPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'ibzorganization',
            majorPSDEField: 'orgname',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBZOrganizationPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBZOrganizationPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBZOrganizationPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBZOrganizationPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBZOrganizationPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBZOrganizationPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBZOrganizationPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof IBZOrganizationPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}