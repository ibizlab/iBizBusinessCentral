import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { TreeExpViewBase } from '@/studio-core';
import IBZDepartmentService from '@/service/ibzdepartment/ibzdepartment-service';
import IBZDepartmentAuthService from '@/authservice/ibzdepartment/ibzdepartment-auth-service';
import TreeExpViewEngine from '@engine/view/tree-exp-view-engine';
import IBZDepartmentUIService from '@/uiservice/ibzdepartment/ibzdepartment-ui-service';

/**
 * 人员管理视图基类
 *
 * @export
 * @class IBZDepartmentTreeExpViewBase
 * @extends {TreeExpViewBase}
 */
export class IBZDepartmentTreeExpViewBase extends TreeExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof IBZDepartmentTreeExpViewBase
     */
    protected appDeName: string = 'ibzdepartment';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof IBZDepartmentTreeExpViewBase
     */
    protected appDeKey: string = 'deptid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof IBZDepartmentTreeExpViewBase
     */
    protected appDeMajor: string = 'deptname';

    /**
     * 实体服务对象
     *
     * @type {IBZDepartmentService}
     * @memberof IBZDepartmentTreeExpViewBase
     */
    protected appEntityService: IBZDepartmentService = new IBZDepartmentService;

    /**
     * 实体权限服务对象
     *
     * @type IBZDepartmentUIService
     * @memberof IBZDepartmentTreeExpViewBase
     */
    public appUIService: IBZDepartmentUIService = new IBZDepartmentUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof IBZDepartmentTreeExpViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof IBZDepartmentTreeExpViewBase
     */
    protected model: any = {
        srfCaption: 'entities.ibzdepartment.views.treeexpview.caption',
        srfTitle: 'entities.ibzdepartment.views.treeexpview.title',
        srfSubTitle: 'entities.ibzdepartment.views.treeexpview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof IBZDepartmentTreeExpViewBase
     */
    protected containerModel: any = {
        view_treeexpbar: { name: 'treeexpbar', type: 'TREEEXPBAR' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '9580f6a22ab4c3c0c4b9424833b79279';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof IBZDepartmentTreeExpViewBase
     */
    public engine: TreeExpViewEngine = new TreeExpViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof IBZDepartmentTreeExpViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            treeexpbar: this.$refs.treeexpbar,
            keyPSDEField: 'ibzdepartment',
            majorPSDEField: 'deptname',
            isLoadDefault: true,
        });
    }

    /**
     * treeexpbar 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBZDepartmentTreeExpViewBase
     */
    public treeexpbar_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('treeexpbar', 'selectionchange', $event);
    }

    /**
     * treeexpbar 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBZDepartmentTreeExpViewBase
     */
    public treeexpbar_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('treeexpbar', 'activated', $event);
    }

    /**
     * treeexpbar 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBZDepartmentTreeExpViewBase
     */
    public treeexpbar_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('treeexpbar', 'load', $event);
    }

    /**
     * 打开新建数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof IBZDepartmentTreeExpView
     */
    public newdata(args: any[],fullargs?:any[], params?: any, $event?: any, xData?: any) {
        let localContext:any = null;
        let localViewParam:any =null;
    this.$Notice.warning({ title: '错误', desc: '未指定关系视图' });
    }


    /**
     * 打开编辑数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof IBZDepartmentTreeExpView
     */
    public opendata(args: any[],fullargs?:any[],params?: any, $event?: any, xData?: any) {
    this.$Notice.warning({ title: '错误', desc: '未指定关系视图' });
    }



    /**
     * 视图唯一标识
     *
     * @type {string}
     * @memberof IBZDepartmentTreeExpView
     */
    public viewUID: string = 'ou-ibzdepartment-tree-exp-view';


}