import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { TreeExpViewBase } from '@/studio-core';
import IBZOrganizationService from '@/service/ibzorganization/ibzorganization-service';
import IBZOrganizationAuthService from '@/authservice/ibzorganization/ibzorganization-auth-service';
import TreeExpViewEngine from '@engine/view/tree-exp-view-engine';
import IBZOrganizationUIService from '@/uiservice/ibzorganization/ibzorganization-ui-service';

/**
 * 部门管理视图基类
 *
 * @export
 * @class IBZOrganizationTreeExpViewBase
 * @extends {TreeExpViewBase}
 */
export class IBZOrganizationTreeExpViewBase extends TreeExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof IBZOrganizationTreeExpViewBase
     */
    protected appDeName: string = 'ibzorganization';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof IBZOrganizationTreeExpViewBase
     */
    protected appDeKey: string = 'orgid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof IBZOrganizationTreeExpViewBase
     */
    protected appDeMajor: string = 'orgname';

    /**
     * 实体服务对象
     *
     * @type {IBZOrganizationService}
     * @memberof IBZOrganizationTreeExpViewBase
     */
    protected appEntityService: IBZOrganizationService = new IBZOrganizationService;

    /**
     * 实体权限服务对象
     *
     * @type IBZOrganizationUIService
     * @memberof IBZOrganizationTreeExpViewBase
     */
    public appUIService: IBZOrganizationUIService = new IBZOrganizationUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof IBZOrganizationTreeExpViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof IBZOrganizationTreeExpViewBase
     */
    protected model: any = {
        srfCaption: 'entities.ibzorganization.views.treeexpview.caption',
        srfTitle: 'entities.ibzorganization.views.treeexpview.title',
        srfSubTitle: 'entities.ibzorganization.views.treeexpview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof IBZOrganizationTreeExpViewBase
     */
    protected containerModel: any = {
        view_treeexpbar: { name: 'treeexpbar', type: 'TREEEXPBAR' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = '6ea2ff98846a9cefb635214c90968987';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof IBZOrganizationTreeExpViewBase
     */
    public engine: TreeExpViewEngine = new TreeExpViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof IBZOrganizationTreeExpViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            treeexpbar: this.$refs.treeexpbar,
            keyPSDEField: 'ibzorganization',
            majorPSDEField: 'orgname',
            isLoadDefault: true,
        });
    }

    /**
     * treeexpbar 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBZOrganizationTreeExpViewBase
     */
    public treeexpbar_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('treeexpbar', 'selectionchange', $event);
    }

    /**
     * treeexpbar 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBZOrganizationTreeExpViewBase
     */
    public treeexpbar_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('treeexpbar', 'activated', $event);
    }

    /**
     * treeexpbar 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBZOrganizationTreeExpViewBase
     */
    public treeexpbar_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('treeexpbar', 'load', $event);
    }

    /**
     * 打开新建数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof IBZOrganizationTreeExpView
     */
    public newdata(args: any[],fullargs?:any[], params?: any, $event?: any, xData?: any) {
        let localContext:any = null;
        let localViewParam:any =null;
    this.$Notice.warning({ title: '错误', desc: '未指定关系视图' });
    }


    /**
     * 打开编辑数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof IBZOrganizationTreeExpView
     */
    public opendata(args: any[],fullargs?:any[],params?: any, $event?: any, xData?: any) {
    this.$Notice.warning({ title: '错误', desc: '未指定关系视图' });
    }



    /**
     * 视图唯一标识
     *
     * @type {string}
     * @memberof IBZOrganizationTreeExpView
     */
    public viewUID: string = 'ou-ibzorganization-tree-exp-view';


}