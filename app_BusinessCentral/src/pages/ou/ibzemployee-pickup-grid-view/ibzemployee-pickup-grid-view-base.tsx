import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import IBZEmployeeService from '@/service/ibzemployee/ibzemployee-service';
import IBZEmployeeAuthService from '@/authservice/ibzemployee/ibzemployee-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import IBZEmployeeUIService from '@/uiservice/ibzemployee/ibzemployee-ui-service';

/**
 * 人员选择表格视图视图基类
 *
 * @export
 * @class IBZEmployeePickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class IBZEmployeePickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof IBZEmployeePickupGridViewBase
     */
    protected appDeName: string = 'ibzemployee';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof IBZEmployeePickupGridViewBase
     */
    protected appDeKey: string = 'userid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof IBZEmployeePickupGridViewBase
     */
    protected appDeMajor: string = 'personname';

    /**
     * 实体服务对象
     *
     * @type {IBZEmployeeService}
     * @memberof IBZEmployeePickupGridViewBase
     */
    protected appEntityService: IBZEmployeeService = new IBZEmployeeService;

    /**
     * 实体权限服务对象
     *
     * @type IBZEmployeeUIService
     * @memberof IBZEmployeePickupGridViewBase
     */
    public appUIService: IBZEmployeeUIService = new IBZEmployeeUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof IBZEmployeePickupGridViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof IBZEmployeePickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.ibzemployee.views.pickupgridview.caption',
        srfTitle: 'entities.ibzemployee.views.pickupgridview.title',
        srfSubTitle: 'entities.ibzemployee.views.pickupgridview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof IBZEmployeePickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: { name: 'grid', type: 'GRID' },
        view_searchform: { name: 'searchform', type: 'SEARCHFORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'f9fea148573d603c7bf77e149b1eab47';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof IBZEmployeePickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof IBZEmployeePickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'ibzemployee',
            majorPSDEField: 'personname',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBZEmployeePickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBZEmployeePickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBZEmployeePickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBZEmployeePickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBZEmployeePickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBZEmployeePickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof IBZEmployeePickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof IBZEmployeePickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}