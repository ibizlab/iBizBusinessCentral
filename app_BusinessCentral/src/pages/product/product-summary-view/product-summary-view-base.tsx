import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { DashboardViewBase } from '@/studio-core';
import ProductService from '@/service/product/product-service';
import ProductAuthService from '@/authservice/product/product-auth-service';
import PortalViewEngine from '@engine/view/portal-view-engine';
import ProductUIService from '@/uiservice/product/product-ui-service';

/**
 * 产品数据看板视图视图基类
 *
 * @export
 * @class ProductSummaryViewBase
 * @extends {DashboardViewBase}
 */
export class ProductSummaryViewBase extends DashboardViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof ProductSummaryViewBase
     */
    protected appDeName: string = 'product';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof ProductSummaryViewBase
     */
    protected appDeKey: string = 'productid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof ProductSummaryViewBase
     */
    protected appDeMajor: string = 'productname';

    /**
     * 实体服务对象
     *
     * @type {ProductService}
     * @memberof ProductSummaryViewBase
     */
    protected appEntityService: ProductService = new ProductService;

    /**
     * 实体权限服务对象
     *
     * @type ProductUIService
     * @memberof ProductSummaryViewBase
     */
    public appUIService: ProductUIService = new ProductUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof ProductSummaryViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof ProductSummaryViewBase
     */
    protected model: any = {
        srfCaption: 'entities.product.views.summaryview.caption',
        srfTitle: 'entities.product.views.summaryview.title',
        srfSubTitle: 'entities.product.views.summaryview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof ProductSummaryViewBase
     */
    protected containerModel: any = {
        view_dashboard: { name: 'dashboard', type: 'DASHBOARD' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'ea2d6d51c76f2d7870f58523cbad8fcd';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof ProductSummaryViewBase
     */
    public engine: PortalViewEngine = new PortalViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof ProductSummaryViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            dashboard: this.$refs.dashboard,
            keyPSDEField: 'product',
            majorPSDEField: 'productname',
            isLoadDefault: true,
        });
    }

    /**
     * dashboard 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof ProductSummaryViewBase
     */
    public dashboard_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('dashboard', 'load', $event);
    }


}