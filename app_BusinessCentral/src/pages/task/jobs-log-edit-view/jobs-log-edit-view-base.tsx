import { Subject } from 'rxjs';
import { ViewTool } from '@/utils';
import { EditViewBase } from '@/studio-core';
import JobsLogService from '@/service/jobs-log/jobs-log-service';
import JobsLogAuthService from '@/authservice/jobs-log/jobs-log-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import JobsLogUIService from '@/uiservice/jobs-log/jobs-log-ui-service';

/**
 * 日志视图基类
 *
 * @export
 * @class JobsLogEditViewBase
 * @extends {EditViewBase}
 */
export class JobsLogEditViewBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof JobsLogEditViewBase
     */
    protected appDeName: string = 'jobslog';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof JobsLogEditViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof JobsLogEditViewBase
     */
    protected appDeMajor: string = 'handler';

    /**
     * 实体服务对象
     *
     * @type {JobsLogService}
     * @memberof JobsLogEditViewBase
     */
    protected appEntityService: JobsLogService = new JobsLogService;

    /**
     * 实体权限服务对象
     *
     * @type JobsLogUIService
     * @memberof JobsLogEditViewBase
     */
    public appUIService: JobsLogUIService = new JobsLogUIService(this.$store);


    /**
     * 计数器服务对象集合
     *
     * @protected
     * @type {Array<*>}
     * @memberof JobsLogEditViewBase
     */    
    protected counterServiceArray: Array<any> = [];

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof JobsLogEditViewBase
     */
    protected model: any = {
        srfCaption: 'entities.jobslog.views.editview.caption',
        srfTitle: 'entities.jobslog.views.editview.title',
        srfSubTitle: 'entities.jobslog.views.editview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof JobsLogEditViewBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof ViewBase
     */
	protected viewtag: string = 'c2a3242cd8f69982d759edcf776aabcc';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof JobsLogEditViewBase
     */
    public engine: EditViewEngine = new EditViewEngine();

    /**
     * 引擎初始化
     *
     * @public
     * @memberof JobsLogEditViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'jobslog',
            majorPSDEField: 'handler',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof JobsLogEditViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof JobsLogEditViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof JobsLogEditViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}