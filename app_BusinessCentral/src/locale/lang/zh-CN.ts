import dictoption_zh_CN from '@locale/lanres/entities/dict-option/dict-option_zh_CN';
import uomschedule_zh_CN from '@locale/lanres/entities/uom-schedule/uom-schedule_zh_CN';
import wfremodel_zh_CN from '@locale/lanres/entities/wfremodel/wfremodel_zh_CN';
import productsalesliterature_zh_CN from '@locale/lanres/entities/product-sales-literature/product-sales-literature_zh_CN';
import listcontact_zh_CN from '@locale/lanres/entities/list-contact/list-contact_zh_CN';
import territory_zh_CN from '@locale/lanres/entities/territory/territory_zh_CN';
import quotedetail_zh_CN from '@locale/lanres/entities/quote-detail/quote-detail_zh_CN';
import sysapp_zh_CN from '@locale/lanres/entities/sys-app/sys-app_zh_CN';
import opportunity_zh_CN from '@locale/lanres/entities/opportunity/opportunity_zh_CN';
import entitlement_zh_CN from '@locale/lanres/entities/entitlement/entitlement_zh_CN';
import discounttype_zh_CN from '@locale/lanres/entities/discount-type/discount-type_zh_CN';
import salesliteratureitem_zh_CN from '@locale/lanres/entities/sales-literature-item/sales-literature-item_zh_CN';
import campaigncampaign_zh_CN from '@locale/lanres/entities/campaign-campaign/campaign-campaign_zh_CN';
import campaignlist_zh_CN from '@locale/lanres/entities/campaign-list/campaign-list_zh_CN';
import ibizlist_zh_CN from '@locale/lanres/entities/ibiz-list/ibiz-list_zh_CN';
import dictcatalog_zh_CN from '@locale/lanres/entities/dict-catalog/dict-catalog_zh_CN';
import salesorder_zh_CN from '@locale/lanres/entities/sales-order/sales-order_zh_CN';
import account_zh_CN from '@locale/lanres/entities/account/account_zh_CN';
import languagelocale_zh_CN from '@locale/lanres/entities/language-locale/language-locale_zh_CN';
import serviceappointment_zh_CN from '@locale/lanres/entities/service-appointment/service-appointment_zh_CN';
import omhierarchy_zh_CN from '@locale/lanres/entities/omhierarchy/omhierarchy_zh_CN';
import ibizservice_zh_CN from '@locale/lanres/entities/ibiz-service/ibiz-service_zh_CN';
import goal_zh_CN from '@locale/lanres/entities/goal/goal_zh_CN';
import opportunitycompetitor_zh_CN from '@locale/lanres/entities/opportunity-competitor/opportunity-competitor_zh_CN';
import ibzdepartment_zh_CN from '@locale/lanres/entities/ibzdepartment/ibzdepartment_zh_CN';
import quote_zh_CN from '@locale/lanres/entities/quote/quote_zh_CN';
import transactioncurrency_zh_CN from '@locale/lanres/entities/transaction-currency/transaction-currency_zh_CN';
import invoicedetail_zh_CN from '@locale/lanres/entities/invoice-detail/invoice-detail_zh_CN';
import employee_zh_CN from '@locale/lanres/entities/employee/employee_zh_CN';
import knowledgearticle_zh_CN from '@locale/lanres/entities/knowledge-article/knowledge-article_zh_CN';
import jobsregistry_zh_CN from '@locale/lanres/entities/jobs-registry/jobs-registry_zh_CN';
import wfgroup_zh_CN from '@locale/lanres/entities/wfgroup/wfgroup_zh_CN';
import wfuser_zh_CN from '@locale/lanres/entities/wfuser/wfuser_zh_CN';
import lead_zh_CN from '@locale/lanres/entities/lead/lead_zh_CN';
import competitor_zh_CN from '@locale/lanres/entities/competitor/competitor_zh_CN';
import campaign_zh_CN from '@locale/lanres/entities/campaign/campaign_zh_CN';
import phonecall_zh_CN from '@locale/lanres/entities/phone-call/phone-call_zh_CN';
import sysrolepermission_zh_CN from '@locale/lanres/entities/sys-role-permission/sys-role-permission_zh_CN';
import appointment_zh_CN from '@locale/lanres/entities/appointment/appointment_zh_CN';
import sysuser_zh_CN from '@locale/lanres/entities/sys-user/sys-user_zh_CN';
import invoice_zh_CN from '@locale/lanres/entities/invoice/invoice_zh_CN';
import jobslog_zh_CN from '@locale/lanres/entities/jobs-log/jobs-log_zh_CN';
import incidentcustomer_zh_CN from '@locale/lanres/entities/incident-customer/incident-customer_zh_CN';
import metric_zh_CN from '@locale/lanres/entities/metric/metric_zh_CN';
import leadcompetitor_zh_CN from '@locale/lanres/entities/lead-competitor/lead-competitor_zh_CN';
import syspermission_zh_CN from '@locale/lanres/entities/sys-permission/sys-permission_zh_CN';
import pricelevel_zh_CN from '@locale/lanres/entities/price-level/price-level_zh_CN';
import productpricelevel_zh_CN from '@locale/lanres/entities/product-price-level/product-price-level_zh_CN';
import connectionrole_zh_CN from '@locale/lanres/entities/connection-role/connection-role_zh_CN';
import wfprocessdefinition_zh_CN from '@locale/lanres/entities/wfprocess-definition/wfprocess-definition_zh_CN';
import competitorsalesliterature_zh_CN from '@locale/lanres/entities/competitor-sales-literature/competitor-sales-literature_zh_CN';
import legal_zh_CN from '@locale/lanres/entities/legal/legal_zh_CN';
import knowledgearticleincident_zh_CN from '@locale/lanres/entities/knowledge-article-incident/knowledge-article-incident_zh_CN';
import task_zh_CN from '@locale/lanres/entities/task/task_zh_CN';
import ibzemployee_zh_CN from '@locale/lanres/entities/ibzemployee/ibzemployee_zh_CN';
import salesorderdetail_zh_CN from '@locale/lanres/entities/sales-order-detail/sales-order-detail_zh_CN';
import sysauthlog_zh_CN from '@locale/lanres/entities/sys-auth-log/sys-auth-log_zh_CN';
import omhierarchypurposeref_zh_CN from '@locale/lanres/entities/omhierarchy-purpose-ref/omhierarchy-purpose-ref_zh_CN';
import sysuserrole_zh_CN from '@locale/lanres/entities/sys-user-role/sys-user-role_zh_CN';
import bulkoperation_zh_CN from '@locale/lanres/entities/bulk-operation/bulk-operation_zh_CN';
import jobsinfo_zh_CN from '@locale/lanres/entities/jobs-info/jobs-info_zh_CN';
import ibzorganization_zh_CN from '@locale/lanres/entities/ibzorganization/ibzorganization_zh_CN';
import omhierarchycat_zh_CN from '@locale/lanres/entities/omhierarchy-cat/omhierarchy-cat_zh_CN';
import productassociation_zh_CN from '@locale/lanres/entities/product-association/product-association_zh_CN';
import campaignactivity_zh_CN from '@locale/lanres/entities/campaign-activity/campaign-activity_zh_CN';
import activitypointer_zh_CN from '@locale/lanres/entities/activity-pointer/activity-pointer_zh_CN';
import campaignresponse_zh_CN from '@locale/lanres/entities/campaign-response/campaign-response_zh_CN';
import listaccount_zh_CN from '@locale/lanres/entities/list-account/list-account_zh_CN';
import sysrole_zh_CN from '@locale/lanres/entities/sys-role/sys-role_zh_CN';
import salesliterature_zh_CN from '@locale/lanres/entities/sales-literature/sales-literature_zh_CN';
import subject_zh_CN from '@locale/lanres/entities/subject/subject_zh_CN';
import operationunit_zh_CN from '@locale/lanres/entities/operation-unit/operation-unit_zh_CN';
import ibzdeptmember_zh_CN from '@locale/lanres/entities/ibzdept-member/ibzdept-member_zh_CN';
import letter_zh_CN from '@locale/lanres/entities/letter/letter_zh_CN';
import uom_zh_CN from '@locale/lanres/entities/uom/uom_zh_CN';
import product_zh_CN from '@locale/lanres/entities/product/product_zh_CN';
import multipickdata_zh_CN from '@locale/lanres/entities/multi-pick-data/multi-pick-data_zh_CN';
import wfmember_zh_CN from '@locale/lanres/entities/wfmember/wfmember_zh_CN';
import opportunityproduct_zh_CN from '@locale/lanres/entities/opportunity-product/opportunity-product_zh_CN';
import contact_zh_CN from '@locale/lanres/entities/contact/contact_zh_CN';
import omhierarchypurpose_zh_CN from '@locale/lanres/entities/omhierarchy-purpose/omhierarchy-purpose_zh_CN';
import incident_zh_CN from '@locale/lanres/entities/incident/incident_zh_CN';
import fax_zh_CN from '@locale/lanres/entities/fax/fax_zh_CN';
import competitorproduct_zh_CN from '@locale/lanres/entities/competitor-product/competitor-product_zh_CN';
import listlead_zh_CN from '@locale/lanres/entities/list-lead/list-lead_zh_CN';
import productsubstitute_zh_CN from '@locale/lanres/entities/product-substitute/product-substitute_zh_CN';
import websitecontent_zh_CN from '@locale/lanres/entities/web-site-content/web-site-content_zh_CN';
import organization_zh_CN from '@locale/lanres/entities/organization/organization_zh_CN';
import email_zh_CN from '@locale/lanres/entities/email/email_zh_CN';
import ibzpost_zh_CN from '@locale/lanres/entities/ibzpost/ibzpost_zh_CN';
import connection_zh_CN from '@locale/lanres/entities/connection/connection_zh_CN';
import components_zh_CN from '@locale/lanres/components/components_zh_CN';
import codelist_zh_CN from '@locale/lanres/codelist/codelist_zh_CN';
import userCustom_zh_CN from '@locale/lanres/userCustom/userCustom_zh_CN';

export default {
    app: {
        commonWords:{
            error: "失败",
            success: "成功",
            ok: "确认",
            cancel: "取消",
            save: "保存",
            codeNotExist: "代码表不存在",
            reqException: "请求异常",
            sysException: "系统异常",
            warning: "警告",
            wrong: "错误",
            rulesException: "值规则校验异常",
            saveSuccess: "保存成功",
            saveFailed: "保存失败",
            deleteSuccess: "删除成功！",
            deleteError: "删除失败！",
            delDataFail: "删除数据失败",
            noData: "暂无数据",
            startsuccess:"启动成功"
        },
        local:{
            new: "新建",
            add: "增加",
        },
        gridpage: {
            choicecolumns: "选择列",
            refresh: "刷新",
            show: "显示",
            records: "条",
            totle: "共",
            noData: "无数据",
            valueVail: "值不能为空",
            notConfig: {
                fetchAction: "视图表格fetchAction参数未配置",
                removeAction: "视图表格removeAction参数未配置",
                createAction: "视图表格createAction参数未配置",
                updateAction: "视图表格updateAction参数未配置",
                loaddraftAction: "视图表格loaddraftAction参数未配置",
            },
            data: "数据",
            delDataFail: "删除数据失败",
            delSuccess: "删除成功!",
            confirmDel: "确认要删除",
            notRecoverable: "删除操作将不可恢复？",
            notBatch: "批量添加未实现",
            grid: "表",
            exportFail: "数据导出失败",
            sum: "合计",
            formitemFailed: "表单项更新失败",
        },
        list: {
            notConfig: {
                fetchAction: "视图列表fetchAction参数未配置",
                removeAction: "视图表格removeAction参数未配置",
                createAction: "视图列表createAction参数未配置",
                updateAction: "视图列表updateAction参数未配置",
            },
            confirmDel: "确认要删除",
            notRecoverable: "删除操作将不可恢复？",
        },
        listExpBar: {
            title: "列表导航栏",
        },
        wfExpBar: {
            title: "流程导航栏",
        },
        calendarExpBar:{
            title: "日历导航栏",
        },
        treeExpBar: {
            title: "树视图导航栏",
        },
        portlet: {
            noExtensions: "无扩展插件",
        },
        tabpage: {
            sureclosetip: {
                title: "关闭提醒",
                content: "表单数据已经修改，确定要关闭？",
            },
            closeall: "关闭所有",
            closeother: "关闭其他",
        },
        fileUpload: {
            caption: "上传",
        },
        searchButton: {
            search: "搜索",
            reset: "重置",
        },
        calendar:{
          today: "今天",
          month: "月",
          week: "周",
          day: "天",
          list: "列",
          dateSelectModalTitle: "选择要跳转的时间",
          gotoDate: "跳转",
          from: "从",
          to: "至",
        },
        // 非实体视图
        views: {
            setting: {
                caption: "设置",
                title: "设置",
            },
            central: {
                caption: "企业中心",
                title: "企业中心",
            },
        },
        utilview:{
            importview:"导入数据",
            warning:"警告",
            info:"请配置数据导入项" 
        },
        menus: {
            central: {
                user_menus: "用户菜单",
                top_menus: "顶部菜单",
                menuitem3: "系统设置",
                menuitem39: "区域",
                menuitem26: "货币",
                menuitem4: "链接角色",
                menuitem33: "计价单位",
                menuitem36: "计价单位组",
                menuitem37: "价目表",
                menuitem38: "折扣表",
                menuitem31: "目标度量",
                menuitem44: "字典管理",
                menuitem53: "组织管理",
                menuitem54: "单位管理",
                menuitem55: "部门管理",
                menuitem56: "人员管理",
                menuitem57: "岗位管理",
                menuitem17: "系统权限",
                menuitem40: "系统用户",
                menuitem41: " 用户角色",
                menuitem42: "认证日志",
                menuitem43: "接入应用",
                menuitem45: "任务管理",
                menuitem46: " 任务注册",
                menuitem47: " 任务管理",
                menuitem48: "任务日志",
                menuitem49: "流程管理",
                menuitem50: "流程定义",
                menuitem51: "流程角色",
                menuitem52: "发布新流程",
                menuitem34: "消息通知",
                menuitem35: "帮助",
                left_exp: "左侧菜单",
                menuitem18: "最近",
                menuitem19: "固定",
                menuitem24: "组织权限管理",
                menuitem60: "法人",
                menuitem58: "业务单位",
                menuitem59: "部门",
                menuitem29: "组织层次",
                menuitem61: "员工",
                bottom_exp: "底部内容",
                footer_left: "底部左侧",
                footer_center: "底部中间",
                footer_right: "底部右侧",
            },
        },
        formpage:{
            desc1: "操作失败,未能找到当前表单项",
            desc2: "无法继续操作",
            notconfig: {
                loadaction: "视图表单loadAction参数未配置",
                loaddraftaction: "视图表单loaddraftAction参数未配置",
                actionname: "视图表单'+actionName+'参数未配置",
                removeaction: "视图表单removeAction参数未配置",
            },
            saveerror: "保存数据发生错误",
            savecontent: "数据不一致，可能后台数据已经被修改,是否要重新加载数据？",
            valuecheckex: "值规则校验异常",
            savesuccess: "保存成功！",
            deletesuccess: "删除成功！",  
            workflow: {
                starterror: "工作流启动失败",
                startsuccess: "工作流启动成功",
                submiterror: "工作流提交失败",
                submitsuccess: "工作流提交成功",
            },
            updateerror: "表单项更新失败",     
        },
        gridBar: {
            title: "表格导航栏",
        },
        multiEditView: {
            notConfig: {
                fetchAction: "视图多编辑视图面板fetchAction参数未配置",
                loaddraftAction: "视图多编辑视图面板loaddraftAction参数未配置",
            },
        },
        dataViewExpBar: {
            title: "卡片视图导航栏",
        },
        kanban: {
            notConfig: {
                fetchAction: "视图列表fetchAction参数未配置",
                removeAction: "视图表格removeAction参数未配置",
            },
            delete1: "确认要删除 ",
            delete2: "删除操作将不可恢复？",
        },
        dashBoard: {
            handleClick: {
                title: "面板设计",
            },
        },
        dataView: {
            sum: "共",
            data: "条数据",
        },
        chart: {
            undefined: "未定义",
            quarter: "季度",   
            year: "年",
        },
        searchForm: {
            notConfig: {
                loadAction: "视图搜索表单loadAction参数未配置",
                loaddraftAction: "视图搜索表单loaddraftAction参数未配置",
            },
            custom: "存储自定义查询",
            title: "名称",
        },
        wizardPanel: {
            back: "上一步",
            next: "下一步",
            complete: "完成",
            preactionmessage:"未配置计算上一步行为"
        },
        viewLayoutPanel: {
            appLogoutView: {
                prompt1: "尊敬的客户您好，您已成功退出系统，将在",
                prompt2: "秒后跳转至",
                logingPage: "登录页",
            },
            appWfstepTraceView: {
                title: "应用流程处理记录视图",
            },
            appWfstepDataView: {
                title: "应用流程跟踪视图",
            },
            appLoginView: {
                username: "用户名",
                password: "密码",
                login: "登录",
            },
        },
    },
    form: {
        group: {
            show_more: "显示更多",
            hidden_more: "隐藏更多"
        }
    },
    entities: {
        dictoption: dictoption_zh_CN,
        uomschedule: uomschedule_zh_CN,
        wfremodel: wfremodel_zh_CN,
        productsalesliterature: productsalesliterature_zh_CN,
        listcontact: listcontact_zh_CN,
        territory: territory_zh_CN,
        quotedetail: quotedetail_zh_CN,
        sysapp: sysapp_zh_CN,
        opportunity: opportunity_zh_CN,
        entitlement: entitlement_zh_CN,
        discounttype: discounttype_zh_CN,
        salesliteratureitem: salesliteratureitem_zh_CN,
        campaigncampaign: campaigncampaign_zh_CN,
        campaignlist: campaignlist_zh_CN,
        ibizlist: ibizlist_zh_CN,
        dictcatalog: dictcatalog_zh_CN,
        salesorder: salesorder_zh_CN,
        account: account_zh_CN,
        languagelocale: languagelocale_zh_CN,
        serviceappointment: serviceappointment_zh_CN,
        omhierarchy: omhierarchy_zh_CN,
        ibizservice: ibizservice_zh_CN,
        goal: goal_zh_CN,
        opportunitycompetitor: opportunitycompetitor_zh_CN,
        ibzdepartment: ibzdepartment_zh_CN,
        quote: quote_zh_CN,
        transactioncurrency: transactioncurrency_zh_CN,
        invoicedetail: invoicedetail_zh_CN,
        employee: employee_zh_CN,
        knowledgearticle: knowledgearticle_zh_CN,
        jobsregistry: jobsregistry_zh_CN,
        wfgroup: wfgroup_zh_CN,
        wfuser: wfuser_zh_CN,
        lead: lead_zh_CN,
        competitor: competitor_zh_CN,
        campaign: campaign_zh_CN,
        phonecall: phonecall_zh_CN,
        sysrolepermission: sysrolepermission_zh_CN,
        appointment: appointment_zh_CN,
        sysuser: sysuser_zh_CN,
        invoice: invoice_zh_CN,
        jobslog: jobslog_zh_CN,
        incidentcustomer: incidentcustomer_zh_CN,
        metric: metric_zh_CN,
        leadcompetitor: leadcompetitor_zh_CN,
        syspermission: syspermission_zh_CN,
        pricelevel: pricelevel_zh_CN,
        productpricelevel: productpricelevel_zh_CN,
        connectionrole: connectionrole_zh_CN,
        wfprocessdefinition: wfprocessdefinition_zh_CN,
        competitorsalesliterature: competitorsalesliterature_zh_CN,
        legal: legal_zh_CN,
        knowledgearticleincident: knowledgearticleincident_zh_CN,
        task: task_zh_CN,
        ibzemployee: ibzemployee_zh_CN,
        salesorderdetail: salesorderdetail_zh_CN,
        sysauthlog: sysauthlog_zh_CN,
        omhierarchypurposeref: omhierarchypurposeref_zh_CN,
        sysuserrole: sysuserrole_zh_CN,
        bulkoperation: bulkoperation_zh_CN,
        jobsinfo: jobsinfo_zh_CN,
        ibzorganization: ibzorganization_zh_CN,
        omhierarchycat: omhierarchycat_zh_CN,
        productassociation: productassociation_zh_CN,
        campaignactivity: campaignactivity_zh_CN,
        activitypointer: activitypointer_zh_CN,
        campaignresponse: campaignresponse_zh_CN,
        listaccount: listaccount_zh_CN,
        sysrole: sysrole_zh_CN,
        salesliterature: salesliterature_zh_CN,
        subject: subject_zh_CN,
        operationunit: operationunit_zh_CN,
        ibzdeptmember: ibzdeptmember_zh_CN,
        letter: letter_zh_CN,
        uom: uom_zh_CN,
        product: product_zh_CN,
        multipickdata: multipickdata_zh_CN,
        wfmember: wfmember_zh_CN,
        opportunityproduct: opportunityproduct_zh_CN,
        contact: contact_zh_CN,
        omhierarchypurpose: omhierarchypurpose_zh_CN,
        incident: incident_zh_CN,
        fax: fax_zh_CN,
        competitorproduct: competitorproduct_zh_CN,
        listlead: listlead_zh_CN,
        productsubstitute: productsubstitute_zh_CN,
        websitecontent: websitecontent_zh_CN,
        organization: organization_zh_CN,
        email: email_zh_CN,
        ibzpost: ibzpost_zh_CN,
        connection: connection_zh_CN,
    },
    components: components_zh_CN,
    codelist: codelist_zh_CN,
    userCustom: userCustom_zh_CN,
};