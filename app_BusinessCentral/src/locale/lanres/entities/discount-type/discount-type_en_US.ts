
export default {
  fields: {
    timezoneruleversionnumber: "Time Zone Rule Version Number",
    statuscode: "状态描述",
    discounttypeid: "折扣表",
    createdate: "建立时间",
    updateman: "更新人",
    amounttype: "类型",
    versionnumber: "Version Number",
    createman: "建立人",
    discounttypename: "折扣类型名称",
    updatedate: "更新时间",
    description: "说明",
    utcconversiontimezonecode: "UTC Conversion Time Zone Code",
    overriddencreatedon: "Record Created On",
    importsequencenumber: "Import Sequence Number",
    statecode: "状态",
    transactioncurrencyid: "货币",
  },
	views: {
		editview: {
			caption: "折扣表",
      		title: "折扣表",
		},
		gridview: {
			caption: "折扣表",
      		title: "折扣表",
		},
		pickupgridview: {
			caption: "折扣表",
      		title: "折扣表选择表格视图",
		},
		pickupview: {
			caption: "折扣表",
      		title: "折扣表数据选择视图",
		},
	},
	main_form: {
		details: {
			group1: "discounttype基本信息", 
			formpage1: "基本信息", 
			group2: "操作信息", 
			formpage2: "其它", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "折扣表", 
			srfmajortext: "折扣类型名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			discounttypename: "折扣类型名称", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			discounttypeid: "折扣表", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			discounttypename: "折扣类型名称",
			updateman: "更新人",
			updatedate: "更新时间",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "Save",
			tip: "Save",
		},
		tbitem4: {
			caption: "Save And New",
			tip: "Save And New",
		},
		tbitem5: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "Remove And Close",
			tip: "Remove And Close Window",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "New",
			tip: "New",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "Copy",
			tip: "Copy {0}",
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem6: {
			caption: "Copy",
			tip: "Copy {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
};