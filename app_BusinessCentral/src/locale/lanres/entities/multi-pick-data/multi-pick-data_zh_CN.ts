export default {
  fields: {
    pickdatainfo: "信息描述",
    userdata: "用户数据",
    pickdataname: "数据名称",
    pickdatatype: "数据类型",
    userdate2: "用户时间2",
    userdate: "用户时间",
    pickdataid: "数据标识",
    userdata4: "用户数据4",
    userdata2: "用户数据2",
    userdata3: "用户数据3",
  },
	views: {
		acgrid: {
			caption: "多类选择实体",
      		title: "负责人（客户、联系人）表格",
		},
		ac: {
			caption: "负责人",
      		title: "负责人",
		},
	},
	main_grid: {
		columns: {
			pickdataname: "数据名称",
			pickdatatype: "数据类型",
			userdata: "用户数据",
			userdata2: "用户数据2",
			pickdatainfo: "信息描述",
		},
		uiactions: {
		},
	},
	ac_searchform: {
		details: {
			formpage1: "常规条件", 
			n_pickdataname_like: "负责人", 
		},
		uiactions: {
		},
	},
};