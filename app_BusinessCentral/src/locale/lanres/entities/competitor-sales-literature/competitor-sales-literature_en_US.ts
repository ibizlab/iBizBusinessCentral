
export default {
  fields: {
    updatedate: "更新时间",
    relationshipstype: "关系类型",
    updateman: "更新人",
    createman: "建立人",
    relationshipsname: "关系名称",
    relationshipsid: "关系标识",
    createdate: "建立时间",
    entityname: "竞争对手",
    entity2name: "销售宣传资料",
    entity2id: "销售宣传资料",
    entityid: "竞争对手",
  },
	views: {
		sallitcompeditview: {
			caption: "竞争对手宣传资料",
      		title: "宣传资料竞争对手明细",
		},
		sallitcompgridview: {
			caption: "竞争对手宣传资料",
      		title: "宣传资料竞争对手明细",
		},
	},
	sallitcomp_form: {
		details: {
			group1: "竞争对手宣传资料基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "关系标识", 
			srfmajortext: "关系名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			entity2id: "销售宣传资料", 
			entityname: "竞争对手", 
			relationshipsid: "关系标识", 
			entityid: "竞争对手", 
		},
		uiactions: {
		},
	},
	sallitcompgrid_grid: {
		columns: {
			entity2name: "销售宣传资料",
			entityname: "竞争对手",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	sallitcompgridviewtoolbar_toolbar: {
		deuiaction1: {
			caption: "New",
			tip: "New",
		},
		tbitem2: {
			caption: "-",
			tip: "",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	sallitcompeditviewtoolbar_toolbar: {
		tbitem1: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
};