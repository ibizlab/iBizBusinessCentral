export default {
  fields: {
    vendorid: "供应商 ID",
    productstructure: "产品结构",
    versionnumber: "Version Number",
    stageid: "Stage Id",
    statuscode: "状态描述",
    entityimage: "实体图像",
    overriddencreatedon: "Record Created On",
    stockitem: "库存项",
    importsequencenumber: "Import Sequence Number",
    validfromdate: "有效期的开始日期",
    suppliername: "提供商名称",
    producttypecode: "产品类型",
    entityimageid: "EntityImageId",
    entityimage_timestamp: "EntityImage_Timestamp",
    producturl: "URL",
    standardcost: "标准成本",
    quantityonhand: "现存数量",
    currentcost: "当前成本",
    exchangerate: "汇率",
    productid: "产品",
    description: "说明",
    vendorname: "供应商",
    dmtimportstate: "仅供内部使用",
    entityimage_url: "EntityImage_URL",
    stockvolume: "库存容量",
    quantitydecimal: "支持小数",
    vendorpartnumber: "供应商名称",
    validtodate: "有效期的结束日期",
    reparented: "Reparented",
    createman: "建立人",
    processid: "Process Id",
    createdate: "建立时间",
    productnumber: "产品 ID",
    kit: "为配套件",
    updatedate: "更新时间",
    currentcost_base: "当前成本 (Base)",
    updateman: "更新人",
    stockweight: "库存重量",
    size: "大小",
    timezoneruleversionnumber: "Time Zone Rule Version Number",
    utcconversiontimezonecode: "UTC Conversion Time Zone Code",
    productname: "产品名称",
    price_base: "定价 (Base)",
    price: "定价",
    traversedpath: "Traversed Path",
    hierarchypath: "层次结构路径",
    standardcost_base: "标准成本 (Base)",
    statecode: "状态",
    defaultuomschedulename: "计价单位组",
    subjectname: "主题",
    currencyname: "货币",
    pricelevelname: "默认价目表",
    defaultuomname: "默认计价单位",
    parentproductname: "父级",
    parentproductid: "父级",
    transactioncurrencyid: "货币",
    pricelevelid: "默认价目表",
    defaultuomid: "默认计价单位",
    defaultuomscheduleid: "计价单位组",
    subjectid: "主题",
  },
	views: {
		pickupgridview: {
			caption: "产品",
      		title: "产品选择表格视图",
		},
		gridview: {
			caption: "产品",
      		title: "产品信息",
		},
		info: {
			caption: "产品",
      		title: "产品信息",
		},
		statetabview: {
			caption: "产品信息",
      		title: "产品信息",
		},
		pickupview: {
			caption: "产品",
      		title: "产品数据选择视图",
		},
		edit_datapanelview: {
			caption: "产品",
      		title: "头部信息编辑",
		},
		stopgridview: {
			caption: "产品",
      		title: "产品信息",
		},
		effectivegridview: {
			caption: "产品",
      		title: "产品信息",
		},
		editview: {
			caption: "产品",
      		title: "产品主信息编辑",
		},
		revisegridview: {
			caption: "产品",
      		title: "产品信息",
		},
		info_proinfo: {
			caption: "产品",
      		title: "产品编辑视图",
		},
		summaryview: {
			caption: "产品",
      		title: "产品数据看板视图",
		},
		quickcreateview: {
			caption: "产品",
      		title: "快速新建",
		},
	},
	proinfo_form: {
		details: {
			group1: "产品基本信息", 
			grouppanel1: "详细信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "产品", 
			srfmajortext: "产品名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			productname: "产品名称", 
			productnumber: "产品 ID", 
			validfromdate: "有效期的开始日期", 
			validtodate: "有效期的结束日期", 
			defaultuomschedulename: "计价单位组", 
			defaultuomname: "默认计价单位", 
			pricelevelname: "默认价目表", 
			quantitydecimal: "支持小数", 
			subjectname: "主题", 
			description: "说明", 
			productid: "产品", 
		},
		uiactions: {
		},
	},
	edit_datapanel_form: {
		details: {
			group1: "产品基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "产品", 
			srfmajortext: "产品名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			statecode: "状态", 
			productid: "产品", 
		},
		uiactions: {
		},
	},
	datapanel_form: {
		details: {
			button1: "头信息编辑", 
			grouppanel1: "分组面板", 
			group1: "产品基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "产品", 
			srfmajortext: "产品名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			statecode: "状态", 
			productid: "产品", 
		},
		uiactions: {
			product_openedit_datapanelview: "头信息编辑",
		},
	},
	edit_main_form: {
		details: {
			group1: "产品基本信息", 
			grouppanel1: "详细信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "产品", 
			srfmajortext: "产品名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			productname: "产品名称", 
			productnumber: "产品 ID", 
			validfromdate: "有效期的开始日期", 
			validtodate: "有效期的结束日期", 
			defaultuomschedulename: "计价单位组", 
			defaultuomname: "默认计价单位", 
			pricelevelname: "默认价目表", 
			quantitydecimal: "支持小数", 
			subjectname: "主题", 
			description: "说明", 
			defaultuomid: "默认计价单位", 
			productid: "产品", 
			defaultuomscheduleid: "计价单位组", 
			subjectid: "主题", 
			pricelevelid: "默认价目表", 
		},
		uiactions: {
		},
	},
	quickcreate_form: {
		details: {
			group1: "产品基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "产品", 
			srfmajortext: "产品名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			productnumber: "产品 ID", 
			productname: "产品名称", 
			validfromdate: "有效期的开始日期", 
			validtodate: "有效期的结束日期", 
			productid: "产品", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			productname: "产品名称",
			productnumber: "产品 ID",
			validfromdate: "有效期的开始日期",
			validtodate: "有效期的结束日期",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	editviewtoolbar_toolbar: {
		tbitem1: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	infotoolbar_toolbar: {
		tbitem1_editmain: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem1_remove_sep: {
			caption: "",
			tip: "",
		},
		tbitem1_remove: {
			caption: "删除并关闭",
			tip: "删除并关闭",
		},
		seperator4: {
			caption: "",
			tip: "",
		},
		tbitem17_publish: {
			caption: "发布",
			tip: "发布",
		},
		tbitem17_revise: {
			caption: "修订",
			tip: "修订",
		},
		tbitem17_stop: {
			caption: "停用",
			tip: "停用",
		},
		tbitem2: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem1_quickcreate: {
			caption: "新建",
			tip: "新建",
		},
		tbitem2: {
			caption: "-",
			tip: "",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem6: {
			caption: "拷贝",
			tip: "拷贝",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
	effectivegridviewtoolbar_toolbar: {
		tbitem1_quickcreate: {
			caption: "新建",
			tip: "新建",
		},
		tbitem2: {
			caption: "-",
			tip: "",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem6: {
			caption: "拷贝",
			tip: "拷贝",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem14_revise: {
			caption: "修订",
			tip: "修订",
		},
		tbitem14_stop: {
			caption: "停用",
			tip: "停用",
		},
		tbitem15: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
	revisegridviewtoolbar_toolbar: {
		tbitem1_quickcreate: {
			caption: "新建",
			tip: "新建",
		},
		tbitem2: {
			caption: "-",
			tip: "",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem6: {
			caption: "拷贝",
			tip: "拷贝",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem14_publish: {
			caption: "发布",
			tip: "发布",
		},
		tbitem14_stop: {
			caption: "停用",
			tip: "停用",
		},
		tbitem15: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
	stopgridviewtoolbar_toolbar: {
		tbitem1_quickcreate: {
			caption: "新建",
			tip: "新建",
		},
		tbitem2: {
			caption: "-",
			tip: "",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem6: {
			caption: "拷贝",
			tip: "拷贝",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem14_publish: {
			caption: "发布",
			tip: "发布",
		},
		tbitem15: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
};