export default {
  fields: {
    updateman: "更新人",
    createdate: "建立时间",
    exchangerate: "汇率",
    managername: "管理员",
    territoryid: "区域",
    updatedate: "更新时间",
    entityimageid: "EntityImageId",
    managerid: "经理",
    entityimage_url: "EntityImage_URL",
    importsequencenumber: "Import Sequence Number",
    utcconversiontimezonecode: "时区",
    territoryname: "地区名称",
    createman: "建立人",
    overriddencreatedon: "Record Created On",
    entityimage: "实体图像",
    versionnumber: "Version Number",
    entityimage_timestamp: "EntityImage_Timestamp",
    timezoneruleversionnumber: "Time Zone Rule Version Number",
    description: "说明",
    currencyname: "货币",
    transactioncurrencyid: "货币",
  },
	views: {
		editview: {
			caption: "区域",
      		title: "区域",
		},
		gridview: {
			caption: "区域",
      		title: "区域",
		},
	},
	main_form: {
		details: {
			group1: "territory基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "区域", 
			srfmajortext: "地区名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			territoryname: "地区名称", 
			managername: "管理员", 
			transactioncurrencyname: "货币", 
			exchangerate: "汇率", 
			transactioncurrencyid: "货币", 
			territoryid: "区域", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			territoryname: "地区名称",
			managername: "管理员",
			transactioncurrencyname: "货币",
			exchangerate: "汇率",
			updatedate: "更新时间",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "新建",
			tip: "新建",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem6: {
			caption: "拷贝",
			tip: "拷贝",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "保存",
			tip: "保存",
		},
		tbitem4: {
			caption: "保存并新建",
			tip: "保存并新建",
		},
		tbitem5: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "删除并关闭",
			tip: "删除并关闭",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "新建",
			tip: "新建",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "拷贝",
			tip: "拷贝",
		},
	},
};