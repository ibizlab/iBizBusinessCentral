
export default {
  fields: {
    updateman: "更新人",
    createdate: "建立时间",
    exchangerate: "汇率",
    managername: "管理员",
    territoryid: "区域",
    updatedate: "更新时间",
    entityimageid: "EntityImageId",
    managerid: "经理",
    entityimage_url: "EntityImage_URL",
    importsequencenumber: "Import Sequence Number",
    utcconversiontimezonecode: "时区",
    territoryname: "地区名称",
    createman: "建立人",
    overriddencreatedon: "Record Created On",
    entityimage: "实体图像",
    versionnumber: "Version Number",
    entityimage_timestamp: "EntityImage_Timestamp",
    timezoneruleversionnumber: "Time Zone Rule Version Number",
    description: "说明",
    currencyname: "货币",
    transactioncurrencyid: "货币",
  },
	views: {
		editview: {
			caption: "区域",
      		title: "区域",
		},
		gridview: {
			caption: "区域",
      		title: "区域",
		},
	},
	main_form: {
		details: {
			group1: "territory基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "区域", 
			srfmajortext: "地区名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			territoryname: "地区名称", 
			managername: "管理员", 
			transactioncurrencyname: "货币", 
			exchangerate: "汇率", 
			transactioncurrencyid: "货币", 
			territoryid: "区域", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			territoryname: "地区名称",
			managername: "管理员",
			transactioncurrencyname: "货币",
			exchangerate: "汇率",
			updatedate: "更新时间",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem6: {
			caption: "Copy",
			tip: "Copy {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "Save",
			tip: "Save",
		},
		tbitem4: {
			caption: "Save And New",
			tip: "Save And New",
		},
		tbitem5: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "Remove And Close",
			tip: "Remove And Close Window",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "New",
			tip: "New",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "Copy",
			tip: "Copy {0}",
		},
	},
};