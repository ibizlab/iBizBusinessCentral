
export default {
  fields: {
    productsubstitutename: "产品子套件名称",
    statuscode: "状态描述",
    versionnumber: "Version Number",
    createman: "建立人",
    updateman: "更新人",
    importsequencenumber: "Import Sequence Number",
    exchangerate: "汇率",
    utcconversiontimezonecode: "UTC Conversion Time Zone Code",
    overriddencreatedon: "Record Created On",
    createdate: "建立时间",
    direction: "方向",
    salesrelationshiptype: "销售关系类型",
    timezoneruleversionnumber: "Time Zone Rule Version Number",
    productsubstituteid: "产品关系 ID",
    statecode: "状态",
    updatedate: "更新时间",
    productname: "产品",
    substitutedproductname: "相关产品",
    currencyname: "货币",
    productid: "产品",
    substitutedproductid: "相关产品",
    transactioncurrencyid: "货币",
  },
	views: {
		editview: {
			caption: "产品替换",
      		title: "产品关系编辑视图",
		},
		gridview: {
			caption: "产品替换",
      		title: "产品关系表格视图",
		},
	},
	main_form: {
		details: {
			group1: "productsubstitute基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "产品关系 ID", 
			srfmajortext: "产品子套件名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			productid: "产品", 
			substitutedproductname: "相关产品", 
			salesrelationshiptype: "销售关系类型", 
			direction: "方向", 
			substitutedproductid: "相关产品", 
			productsubstituteid: "产品关系 ID", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			substitutedproductname: "相关产品",
			salesrelationshiptype: "销售关系类型",
			direction: "方向",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		deuiaction1: {
			caption: "New",
			tip: "New",
		},
		tbitem2: {
			caption: "-",
			tip: "",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem1: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
};