
export default {
  fields: {
    createdate: "建立时间",
    languagelocaleid: "LanguageLocaleId",
    languagelocalename: "语言环境名称",
    localeid: "区域设置 ID",
    language: "语言",
    versionnumber: "VersionNumber",
    updateman: "更新人",
    updatedate: "更新时间",
    createman: "建立人",
    code: "代码",
    statuscode: "语言状态代码",
    statecode: "状态代码",
    region: "地区",
  },
	views: {
		pickupgridview: {
			caption: "语言",
      		title: "语言选择表格视图",
		},
		pickupview: {
			caption: "语言",
      		title: "语言数据选择视图",
		},
	},
	main_grid: {
		columns: {
			languagelocalename: "语言环境名称",
			updateman: "更新人",
			updatedate: "更新时间",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
};