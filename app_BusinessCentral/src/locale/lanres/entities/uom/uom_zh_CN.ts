export default {
  fields: {
    overriddencreatedon: "Record Created On",
    baseuomname: "BaseUoMName",
    updateman: "更新人",
    createman: "建立人",
    versionnumber: "Version Number",
    quantity: "数量",
    uomid: "计价单位",
    schedulebaseuom: "为计划基础单位",
    organizationid: "组织",
    createdate: "建立时间",
    updatedate: "更新时间",
    uomname: "计量单位名称",
    utcconversiontimezonecode: "UTC Conversion Time Zone Code",
    timezoneruleversionnumber: "Time Zone Rule Version Number",
    importsequencenumber: "Import Sequence Number",
    uomscheduleid: "单位计划",
    baseuom: "基础单位",
  },
	views: {
		editview: {
			caption: "计价单位",
      		title: "计价单位",
		},
		gridview: {
			caption: "计价单位",
      		title: "计价单位",
		},
		pickupview: {
			caption: "计价单位",
      		title: "计价单位数据选择视图",
		},
		pickupgridview: {
			caption: "计价单位",
      		title: "计价单位选择表格视图",
		},
	},
	main_form: {
		details: {
			group1: "uom基本信息", 
			formpage1: "基本信息", 
			group2: "操作信息", 
			formpage2: "其它", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "计价单位", 
			srfmajortext: "计量单位名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			uomname: "计量单位名称", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			uomid: "计价单位", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			uomname: "计量单位名称",
			updateman: "更新人",
			updatedate: "更新时间",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "新建",
			tip: "新建",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem6: {
			caption: "拷贝",
			tip: "拷贝",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "保存",
			tip: "保存",
		},
		tbitem4: {
			caption: "保存并新建",
			tip: "保存并新建",
		},
		tbitem5: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "删除并关闭",
			tip: "删除并关闭",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "新建",
			tip: "新建",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "拷贝",
			tip: "拷贝",
		},
	},
};