
export default {
  fields: {
    createman: "建立人",
    statuscode: "状态描述",
    amount: "度量类型",
    timezoneruleversionnumber: "时区规则版本号",
    updateman: "更新人",
    description: "说明",
    utcconversiontimezonecode: "UTC 转换时区代码",
    statecode: "状态",
    overriddencreatedon: "创建记录的时间",
    metricname: "公制名称",
    importsequencenumber: "导入序列号",
    metricid: "目标度量",
    amountdatatype: "金额数据类型",
    createdate: "建立时间",
    updatedate: "更新时间",
    versionnumber: "版本号",
    stretchtracked: "跟踪扩展目标值",
  },
	views: {
		gridview: {
			caption: "目标度量",
      		title: "目标度量信息",
		},
		pickupview: {
			caption: "目标度量",
      		title: "目标度量数据选择视图",
		},
		pickupgridview: {
			caption: "目标度量",
      		title: "目标度量选择表格视图",
		},
		editview: {
			caption: "目标度量",
      		title: "目标度量信息",
		},
	},
	main_form: {
		details: {
			group1: "metric基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "目标度量", 
			srfmajortext: "公制名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			metricname: "公制名称", 
			amount: "度量类型", 
			amountdatatype: "金额数据类型", 
			stretchtracked: "跟踪扩展目标值", 
			description: "说明", 
			metricid: "目标度量", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			metricname: "公制名称",
			updateman: "更新人",
			updatedate: "更新时间",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem6: {
			caption: "Copy",
			tip: "Copy {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "Save",
			tip: "Save",
		},
		tbitem4: {
			caption: "Save And New",
			tip: "Save And New",
		},
		tbitem5: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "Remove And Close",
			tip: "Remove And Close Window",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "New",
			tip: "New",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "Copy",
			tip: "Copy {0}",
		},
	},
};