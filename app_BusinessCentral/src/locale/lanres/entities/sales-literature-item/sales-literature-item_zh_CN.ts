export default {
  fields: {
    authorname: "作者姓名",
    timezoneruleversionnumber: "Time Zone Rule Version Number",
    title: "标题",
    filesize: "文件大小(字节)",
    mode: "Mode",
    filetypecode: "文件类型",
    importsequencenumber: "Import Sequence Number",
    salesliteratureitemid: "销售宣传资料项",
    updatedate: "更新时间",
    updateman: "更新人",
    mimetype: "MIME 类型",
    createman: "建立人",
    keywords: "关键字",
    filetype: "FileType",
    attacheddocumenturl: "附加文档 URL",
    documentbody: "显示销售宣传资料文档附件的编码内容。",
    versionnumber: "Version Number",
    ibizabstract: "是否抽象",
    overriddencreatedon: "Record Created On",
    customerviewable: "向客户公开",
    filename: "文件名",
    organizationid: "组织",
    createdate: "建立时间",
    utcconversiontimezonecode: "UTC Conversion Time Zone Code",
    salesliteratureid: "销售宣传资料",
  },
	views: {
		gridview: {
			caption: "销售附件",
      		title: "销售附件表格视图",
		},
		editview: {
			caption: "销售附件",
      		title: "销售附件编辑视图",
		},
	},
	main_form: {
		details: {
			group1: "salesliteratureitem基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "销售宣传资料项", 
			srfmajortext: "标题", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			title: "标题", 
			authorname: "作者姓名", 
			keywords: "关键字", 
			attacheddocumenturl: "附件", 
			salesliteratureitemid: "销售宣传资料项", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			title: "标题",
			authorname: "作者姓名",
			keywords: "关键字",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		deuiaction1: {
			caption: "新建",
			tip: "新建",
		},
		tbitem2: {
			caption: "-",
			tip: "",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem1: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
};