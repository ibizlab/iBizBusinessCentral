
export default {
  fields: {
    importsequencenumber: "Import Sequence Number",
    roundingoptionamount: "舍入金额",
    versionnumber: "Version Number",
    amount_base: "金额 (Base)",
    processid: "Process Id",
    amount: "金额",
    createdate: "建立时间",
    roundingoptionamount_base: "舍入金额 (Base)",
    updateman: "更新人",
    overriddencreatedon: "Record Created On",
    exchangerate: "汇率",
    organizationid: "组织",
    percentage: "百分比",
    traversedpath: "Traversed Path",
    productnumber: "产品 ID",
    pricingmethodcode: "定价方式",
    roundingoptioncode: "舍入选项",
    timezoneruleversionnumber: "Time Zone Rule Version Number",
    utcconversiontimezonecode: "UTC Conversion Time Zone Code",
    productpricelevelid: "产品价目表",
    quantitysellingcode: "销售数量控制",
    updatedate: "更新时间",
    roundingpolicycode: "舍入原则",
    createman: "建立人",
    stageid: "Stage Id",
    uomschedulename: "计量单位组",
    productname: "产品名称",
    currencyname: "货币",
    uomname: "计量单位",
    discounttypename: "折扣表",
    pricelevelname: "价目表",
    productid: "产品",
    uomscheduleid: "单位计划 ID",
    transactioncurrencyid: "货币",
    uomid: "计价单位",
    discounttypeid: "折扣表",
    pricelevelid: "价目表",
  },
	views: {
		proprilvgridview: {
			caption: "价目表项",
      		title: "产品价目表项",
		},
		proprilveditview: {
			caption: "价目表项",
      		title: "产品价目表项",
		},
	},
	proprilv_form: {
		details: {
			group1: "价目表项基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "产品价目表", 
			srfmajortext: "产品名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			productname: "产品名称", 
			pricelevelname: "价目表", 
			uomname: "计量单位", 
			currencyname: "货币", 
			discounttypename: "折扣表", 
			quantitysellingcode: "销售数量控制", 
			pricingmethodcode: "定价方式", 
			amount: "金额", 
			productid: "产品", 
			transactioncurrencyid: "货币", 
			uomid: "计价单位", 
			discounttypeid: "折扣表", 
			pricelevelid: "价目表", 
			productpricelevelid: "产品价目表", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			pricelevelname: "价目表",
			uomname: "计量单位",
			pricingmethodcode: "定价方式",
			amount: "金额",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	proprilvgridviewtoolbar_toolbar: {
		deuiaction1: {
			caption: "New",
			tip: "New",
		},
		tbitem2: {
			caption: "-",
			tip: "",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	proprilveditviewtoolbar_toolbar: {
		tbitem1: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
};