
export default {
  fields: {
    createman: "建立人",
    relationshipstype: "关系类型",
    relationshipsname: "关系名称",
    updatedate: "更新时间",
    relationshipsid: "关系标识",
    createdate: "建立时间",
    updateman: "更新人",
    entityname: "活动名称",
    entity2name: "活动2名称",
    entity2id: "市场活动",
    entityid: "市场活动",
  },
};