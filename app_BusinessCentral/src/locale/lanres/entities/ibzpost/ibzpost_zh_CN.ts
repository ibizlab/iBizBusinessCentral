export default {
  fields: {
    postid: "岗位标识",
    postcode: "岗位编码",
    postname: "岗位名称",
    domains: "区属",
    memo: "备注",
  },
	views: {
		gridview: {
			caption: "岗位管理",
      		title: "岗位管理",
		},
		editview: {
			caption: "岗位管理",
      		title: "岗位管理",
		},
		pickupview: {
			caption: "岗位",
      		title: "岗位数据选择视图",
		},
		pickupgridview: {
			caption: "岗位",
      		title: "岗位选择表格视图",
		},
	},
	main_form: {
		details: {
			group1: "岗位基本信息", 
			formpage1: "基本信息", 
			srforikey: "", 
			srfkey: "岗位标识", 
			srfmajortext: "岗位名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			postcode: "岗位编码", 
			postname: "岗位名称", 
			domains: "区属", 
			memo: "备注", 
			postid: "岗位标识", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			postcode: "岗位编码",
			postname: "岗位名称",
			memo: "备注",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "新建",
			tip: "新建",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem6: {
			caption: "拷贝",
			tip: "拷贝",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "保存",
			tip: "保存",
		},
		tbitem4: {
			caption: "保存并新建",
			tip: "保存并新建",
		},
		tbitem5: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "删除并关闭",
			tip: "删除并关闭",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "新建",
			tip: "新建",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "拷贝",
			tip: "拷贝",
		},
	},
};