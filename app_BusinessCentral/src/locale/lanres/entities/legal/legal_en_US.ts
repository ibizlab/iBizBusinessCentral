
export default {
  fields: {
    legalname: "法人名称",
    organizationtype: "组织类型",
    createman: "建立人",
    updateman: "更新人",
    legalid: "法人标识",
    createdate: "建立时间",
    updatedate: "更新时间",
    orgcode: "组织编码",
    orglevel: "组织层级",
    showorder: "排序号",
    shortname: "组织简称",
  },
	views: {
		mastersummaryview: {
			caption: "主信息概览",
      		title: "主信息概览看板视图",
		},
		masterquickview: {
			caption: "快速新建",
      		title: "法人快速新建视图",
		},
		gridview: {
			caption: "法人",
      		title: "法人表格视图",
		},
		mastertabinfoview: {
			caption: "法人信息",
      		title: "主信息总览",
		},
		masterinfoview: {
			caption: "法人",
      		title: "主信息概览视图",
		},
	},
	if_master_form: {
		details: {
			group1: "基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "法人标识", 
			srfmajortext: "法人名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			orgcode: "组织编码", 
			legalname: "法人名称", 
			shortname: "组织简称", 
			legalid: "法人标识", 
		},
		uiactions: {
		},
	},
	ef_masterquick_form: {
		details: {
			group1: "法人基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "法人标识", 
			srfmajortext: "法人名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			orgcode: "组织编码", 
			legalname: "法人名称", 
			shortname: "组织简称", 
			legalid: "法人标识", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			orgcode: "组织编码",
			legalname: "法人名称",
			shortname: "组织简称",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem6: {
			caption: "Copy",
			tip: "Copy {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
};