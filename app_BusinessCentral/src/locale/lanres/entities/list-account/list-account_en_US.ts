
export default {
  fields: {
    createdate: "建立时间",
    relationshipsname: "关系名称",
    relationshipstype: "关系类型",
    createman: "建立人",
    updatedate: "更新时间",
    relationshipsid: "关系标识",
    updateman: "更新人",
    statecode: "状态",
    primarycontactid: "主要联系人",
    telephone1: "主要电话",
    primarycontactname: "主要联系人",
    entityname: "营销列表",
    entity2name: "选择客户：",
    entityid: "列表",
    entity2id: "客户",
  },
	views: {
		inner: {
			caption: "营销列表-账户",
      		title: "营销列表-账户表格视图",
		},
		quickcreatebylist: {
			caption: "查找客户",
      		title: "查找客户",
		},
		bylist: {
			caption: "营销列表-账户",
      		title: "营销列表-账户表格视图",
		},
		editview: {
			caption: "营销列表-账户",
      		title: "营销列表-账户编辑视图",
		},
	},
	quickcreatebylist_form: {
		details: {
			group1: "营销列表-账户基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "关系标识", 
			srfmajortext: "关系名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			entity2name: "选择客户：", 
			entity2id: "客户", 
			relationshipsid: "关系标识", 
		},
		uiactions: {
		},
	},
	main_form: {
		details: {
			group1: "营销列表-账户基本信息", 
			formpage1: "基本信息", 
			group2: "操作信息", 
			formpage2: "其它", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "关系标识", 
			srfmajortext: "关系名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			relationshipsid: "关系标识", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			entity2name: "选择客户：",
			telephone1: "主要电话",
			primarycontactname: "主要联系人",
			statecode: "状态",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	bylisttoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem6: {
			caption: "Copy",
			tip: "Copy {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem12: {
			caption: "关闭",
			tip: "关闭",
		},
	},
};