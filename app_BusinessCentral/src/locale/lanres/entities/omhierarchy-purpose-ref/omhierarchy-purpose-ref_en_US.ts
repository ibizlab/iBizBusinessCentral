
export default {
  fields: {
    updateman: "更新人",
    omhierarchypurposerefname: "组织层次结构分配名称",
    createdate: "建立时间",
    omhierarchypurposerefid: "组织层次结构分配标识",
    updatedate: "更新时间",
    createman: "建立人",
    omhierarchycatid: "结构层次类别标识",
    omhierarchypurposeid: "组织层次机构目的标识",
    isdefault: "是否默认",
    enddate: "结束日期",
    begindate: "开始日期",
  },
};