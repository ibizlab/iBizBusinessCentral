
export default {
  fields: {
    id: "组标识",
    name: "组名称",
    groupscope: "范围",
  },
	views: {
		pickupgridview: {
			caption: "角色/用户组",
      		title: "角色/用户组选择表格视图",
		},
		editview: {
			caption: "流程角色",
      		title: "流程角色",
		},
		pickupview: {
			caption: "角色/用户组",
      		title: "角色/用户组数据选择视图",
		},
		gridview: {
			caption: "流程角色",
      		title: "流程角色",
		},
	},
	main_form: {
		details: {
			druipart1: "成员", 
			group1: "角色/用户组基本信息", 
			formpage1: "基本信息", 
			srforikey: "", 
			srfkey: "组标识", 
			srfmajortext: "组名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			groupname: "组名称", 
			groupscope: "范围", 
			groupid: "组标识", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			groupid: "组标识",
			groupname: "组名称",
			groupscope: "范围",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
			n_groupname_like: "组名称(文本包含(%))", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem6: {
			caption: "Copy",
			tip: "Copy {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "Save",
			tip: "Save",
		},
		tbitem4: {
			caption: "Save And New",
			tip: "Save And New",
		},
		tbitem5: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "Remove And Close",
			tip: "Remove And Close Window",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "New",
			tip: "New",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "Copy",
			tip: "Copy {0}",
		},
	},
};