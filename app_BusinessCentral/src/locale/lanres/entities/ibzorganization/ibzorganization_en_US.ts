
export default {
  fields: {
    orgid: "The workflow start node was not found",
    orgcode: "Updated By",
    orgname: "名称",
    parentorgid: "上级单位",
    shortname: "Cancel",
    orglevel: "单位级别",
    showorder: "排序",
    parentorgname: "上级单位",
    domains: "区属",
    enable: "the workflow instance current processing step is invalid",
    createdate: "Print",
    updatedate: "Actor",
  },
	views: {
		gridview: {
			caption: "Input data check failed.",
      		title: "单位管理",
		},
		pickupview: {
			caption: "Input data check failed.",
      		title: "Upload file.",
		},
		treeexpview: {
			caption: "Input data check failed.",
      		title: "部门管理",
		},
		editview: {
			caption: "Input data check failed.",
      		title: "单位管理",
		},
		pickupgridview: {
			caption: "Input data check failed.",
      		title: "Restart workflow",
		},
	},
	main_form: {
		details: {
			formpage1: "基本信息", 
			srfupdatedate: "Actor", 
			srforikey: "", 
			srfkey: "The workflow start node was not found", 
			srfmajortext: "名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			orgid: "The workflow start node was not found", 
			orgcode: "Updated By", 
			orgname: "名称", 
			porgname: "上级单位", 
			orglevel: "单位级别", 
			shortname: "Cancel", 
			showorder: "排序", 
			porgid: "上级单位", 
			createdate: "Print", 
			updatedate: "Actor", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			orgid: "The workflow start node was not found",
			orgcode: "Updated By",
			orgname: "名称",
			orglevel: "单位级别",
			shortname: "Cancel",
			porgname: "上级单位",
			porgid: "上级单位",
			showorder: "排序",
			createdate: "Print",
			updatedate: "Actor",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
			orgcode: "Actor", 
			n_orgname_like: "Print", 
			n_porgid_eq: "上级单位(=)", 
			n_porgname_eq: "上级单位(等于(=))", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem6: {
			caption: "Copy",
			tip: "Copy {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "Save",
			tip: "Save",
		},
		tbitem4: {
			caption: "Save And New",
			tip: "Save And New",
		},
		tbitem5: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "Remove And Close",
			tip: "Remove And Close Window",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "New",
			tip: "New",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "Copy",
			tip: "Copy {0}",
		},
	},
	orgtree_treeview: {
		nodes: {
			root: "默认根节点",
		},
		uiactions: {
		},
	},
};