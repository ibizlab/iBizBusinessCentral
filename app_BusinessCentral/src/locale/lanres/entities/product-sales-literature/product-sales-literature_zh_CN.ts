export default {
  fields: {
    updateman: "更新人",
    relationshipsname: "关系名称",
    createman: "建立人",
    relationshipsid: "关系标识",
    createdate: "建立时间",
    updatedate: "更新时间",
    relationshipstype: "关系类型",
    entity2name: "销售宣传资料",
    entityname: "产品",
    entityid: "产品",
    entity2id: "销售宣传资料",
  },
	views: {
		sallitproeditview: {
			caption: "产品宣传资料",
      		title: "宣传资料产品明细",
		},
		sallitprogridview: {
			caption: "产品宣传资料",
      		title: "宣传资料产品明细",
		},
	},
	sallitpro_form: {
		details: {
			group1: "产品宣传资料基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "关系标识", 
			srfmajortext: "关系名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			entity2id: "销售宣传资料", 
			entityname: "产品", 
			relationshipsid: "关系标识", 
			entityid: "产品", 
		},
		uiactions: {
		},
	},
	sallitprogrid_grid: {
		columns: {
			entity2name: "销售宣传资料",
			entityname: "产品",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	sallitproeditviewtoolbar_toolbar: {
		tbitem1: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	sallitprogridviewtoolbar_toolbar: {
		deuiaction1: {
			caption: "新建",
			tip: "新建",
		},
		tbitem2: {
			caption: "-",
			tip: "",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
};