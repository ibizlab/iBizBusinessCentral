export default {
  fields: {
    updatedate: "更新时间",
    createman: "建立人",
    updateman: "更新人",
    operationunitid: "运营单位标识",
    createdate: "建立时间",
    operationunitname: "运营单位名称",
    organizationtype: "组织类型",
    showorder: "排序号",
    orglevel: "组织层级",
    shortname: "组织简称",
    orgcode: "组织编码",
    operationunittype: "运营单位类型",
  },
	views: {
		mastertabinfoview: {
			caption: "运营单位",
      		title: "主信息总览视图",
		},
		mastereditview: {
			caption: "运营单位编辑",
      		title: "主编辑视图",
		},
		bumastergridview: {
			caption: "业务单位",
      		title: "运营单位表格视图",
		},
		deptmastergridview: {
			caption: "部门",
      		title: "运营单位表格视图",
		},
		mastersummaryview: {
			caption: "运营单位",
      		title: "主信息概览看板视图",
		},
		gridview: {
			caption: "运营单位",
      		title: "运营单位表格视图",
		},
		masterquickview: {
			caption: "运营单位",
      		title: "快速新建视图",
		},
		masterinfoview: {
			caption: "运营单位",
      		title: "主信息概览视图",
		},
		deptmasterquickview: {
			caption: "快速新建",
      		title: "部门快速新建视图",
		},
		bumasterquickview: {
			caption: "运营单位",
      		title: "业务单位快速新建视图",
		},
	},
	if_master_form: {
		details: {
			group1: "运营单位基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "运营单位标识", 
			srfmajortext: "运营单位名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			orgcode: "组织编码", 
			operationunitname: "运营单位名称", 
			shortname: "组织简称", 
			operationunittype: "运营单位类型", 
			operationunitid: "运营单位标识", 
		},
		uiactions: {
		},
	},
	ef_deptmasterquick_form: {
		details: {
			group1: "运营单位基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "运营单位标识", 
			srfmajortext: "运营单位名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			orgcode: "组织编码", 
			operationunitname: "运营单位名称", 
			operationunittype: "运营单位类型", 
			operationunitid: "运营单位标识", 
		},
		uiactions: {
		},
	},
	ef_bumasterquick_form: {
		details: {
			group1: "运营单位基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "运营单位标识", 
			srfmajortext: "运营单位名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			orgcode: "组织编码", 
			operationunitname: "运营单位名称", 
			operationunittype: "运营单位类型", 
			operationunitid: "运营单位标识", 
		},
		uiactions: {
		},
	},
	main_form: {
		details: {
			group1: "运营单位基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "运营单位标识", 
			srfmajortext: "运营单位名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			operationunitname: "运营单位名称", 
			orgcode: "组织编码", 
			shortname: "组织简称", 
			operationunittype: "运营单位类型", 
			operationunitid: "运营单位标识", 
		},
		uiactions: {
		},
	},
	ef_masterquick_form: {
		details: {
			group1: "运营单位基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "运营单位标识", 
			srfmajortext: "运营单位名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			orgcode: "组织编码", 
			operationunitname: "运营单位名称", 
			operationunittype: "运营单位类型", 
			operationunitid: "运营单位标识", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			orgcode: "组织编码",
			operationunitname: "运营单位名称",
			operationunittype: "运营单位类型",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	deptmastergridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "新建",
			tip: "新建",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem6: {
			caption: "拷贝",
			tip: "拷贝",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
	bumastergridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "新建",
			tip: "新建",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem6: {
			caption: "拷贝",
			tip: "拷贝",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
	mastereditviewtoolbar_toolbar: {
		tbitem1: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "新建",
			tip: "新建",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem6: {
			caption: "拷贝",
			tip: "拷贝",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
};