
export default {
  fields: {
    overwritetime: "替代时间",
    updatedate: "更新时间",
    managed: "状态",
    supportingsolutionid: "解决方案",
    componentstate: "组件状态",
    category: "连接角色类别",
    createdate: "建立时间",
    importsequencenumber: "导入序列号",
    connectionrolename: "关联角色名称",
    statuscode: "状态描述",
    connectionroleid: "连接角色",
    connectionroleidunique: "唯一 ID",
    versionnumber: "版本号",
    updateman: "更新人",
    solutionid: "解决方案",
    description: "说明",
    customizable: "可自定义",
    introducedversion: "引入的版本",
    createman: "建立人",
    statecode: "状态",
  },
	views: {
		pickupgridview: {
			caption: "连接角色",
      		title: "连接角色选择表格视图",
		},
		gridview: {
			caption: "连接角色",
      		title: "连接角色",
		},
		pickupview: {
			caption: "连接角色",
      		title: "连接角色数据选择视图",
		},
		editview: {
			caption: "连接角色",
      		title: "连接角色",
		},
	},
	main_form: {
		details: {
			group1: "connectionrole基本信息", 
			formpage1: "基本信息", 
			group2: "操作信息", 
			formpage2: "其它", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "连接角色", 
			srfmajortext: "关联角色名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			connectionrolename: "关联角色名称", 
			category: "连接角色类别", 
			componentstate: "组件状态", 
			managed: "状态", 
			statuscode: "状态描述", 
			statecode: "状态", 
			description: "说明", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			connectionroleid: "连接角色", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			connectionrolename: "关联角色名称",
			category: "连接角色类别",
			componentstate: "组件状态",
			managed: "状态",
			statecode: "状态",
			statuscode: "状态描述",
			description: "说明",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem6: {
			caption: "Copy",
			tip: "Copy {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "Save",
			tip: "Save",
		},
		tbitem4: {
			caption: "Save And New",
			tip: "Save And New",
		},
		tbitem5: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "Remove And Close",
			tip: "Remove And Close Window",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "New",
			tip: "New",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "Copy",
			tip: "Copy {0}",
		},
	},
};