export default {
  fields: {
    importsequencenumber: "Import Sequence Number",
    createdate: "建立时间",
    createman: "建立人",
    timezoneruleversionnumber: "Time Zone Rule Version Number",
    customerviewable: "向客户公开",
    expirationdate: "到期日期",
    exchangerate: "汇率",
    updatedate: "更新时间",
    description: "说明",
    salesliteraturename: "销售资料名称",
    processid: "Process Id",
    stageid: "Stage Id",
    updateman: "更新人",
    entityimage: "实体图像",
    entityimage_url: "EntityImage_URL",
    hasattachments: "有附件",
    overriddencreatedon: "Record Created On",
    entityimage_timestamp: "EntityImage_Timestamp",
    employeecontactid: "责任人",
    keywords: "关键字",
    versionnumber: "Version Number",
    utcconversiontimezonecode: "UTC Conversion Time Zone Code",
    literaturetypecode: "类型",
    salesliteratureid: "销售宣传资料",
    entityimageid: "EntityImageId",
    employeecontactname: "联系人",
    traversedpath: "Traversed Path",
    currencyname: "货币",
    subjectname: "主题",
    transactioncurrencyid: "货币",
    subjectid: "主题",
  },
	views: {
		editview: {
			caption: "销售宣传资料",
      		title: "销售宣传资料编辑视图",
		},
		summaryview: {
			caption: "销售宣传资料",
      		title: "销售宣传资料数据看板视图",
		},
		info_sallitview: {
			caption: "销售宣传资料",
      		title: "销售宣传资料编辑视图",
		},
		edit_datapanelview: {
			caption: "销售宣传资料",
      		title: "头部信息编辑",
		},
		quickcreateview: {
			caption: "销售宣传资料",
      		title: "快速新建",
		},
		infoview: {
			caption: "销售宣传资料",
      		title: "销售宣传资料",
		},
		gridview: {
			caption: "销售宣传资料",
      		title: "销售宣传资料",
		},
	},
	edit_main_form: {
		details: {
			group1: "销售宣传资料基本信息", 
			druipart1: "销售附件", 
			grouppanel1: "销售附件", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "销售宣传资料", 
			srfmajortext: "销售资料名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			salesliteraturename: "标题", 
			subjectname: "主题", 
			literaturetypecode: "类型", 
			description: "说明", 
			subjectid: "主题", 
			salesliteratureid: "销售宣传资料", 
		},
		uiactions: {
		},
	},
	quickcreate_form: {
		details: {
			group1: "销售宣传资料基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "销售宣传资料", 
			srfmajortext: "销售资料名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			salesliteraturename: "标题", 
			subjectname: "主题", 
			literaturetypecode: "类型", 
			subjectid: "主题", 
			salesliteratureid: "销售宣传资料", 
		},
		uiactions: {
		},
	},
	info_form: {
		details: {
			group1: "销售宣传资料基本信息", 
			druipart1: "销售附件", 
			grouppanel1: "销售附件", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "销售宣传资料", 
			srfmajortext: "销售资料名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			salesliteraturename: "标题", 
			subjectname: "主题", 
			literaturetypecode: "类型", 
			description: "说明", 
			salesliteratureid: "销售宣传资料", 
		},
		uiactions: {
		},
	},
	edit_datapanel_form: {
		details: {
			group1: "销售宣传资料基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "销售宣传资料", 
			srfmajortext: "销售资料名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			expirationdate: "到期日期", 
			employeecontactid: "责任人", 
			salesliteratureid: "销售宣传资料", 
		},
		uiactions: {
		},
	},
	datapanel_form: {
		details: {
			button1: "头信息编辑", 
			grouppanel1: "分组面板", 
			group1: "销售宣传资料基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "销售宣传资料", 
			srfmajortext: "销售资料名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			expirationdate: "到期日期", 
			employeecontactid: "责任人", 
			salesliteratureid: "销售宣传资料", 
		},
		uiactions: {
			salesliterature_openedit_datapanelview: "头信息编辑",
		},
	},
	main_grid: {
		columns: {
			salesliteraturename: "销售资料名称",
			subjectname: "主题",
			literaturetypecode: "类型",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	editviewtoolbar_toolbar: {
		tbitem1: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	infoviewtoolbar_toolbar: {
		tbitem1_openmaineditview: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem1_remove_sep: {
			caption: "",
			tip: "",
		},
		tbitem1_remove: {
			caption: "删除并关闭",
			tip: "删除并关闭",
		},
		seperator4: {
			caption: "",
			tip: "",
		},
		tbitem12: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem1_quickcreate: {
			caption: "新建",
			tip: "新建",
		},
		tbitem2: {
			caption: "-",
			tip: "",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem6: {
			caption: "拷贝",
			tip: "拷贝",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
};