export default {
  fields: {
    createman: "建立人",
    updateman: "更新人",
    description: "说明",
    statecode: "状态",
    updatedate: "更新时间",
    paymentmethodcode: "付款方式",
    pricelevelid: "价目表",
    shippingmethodcode: "送货方式",
    overriddencreatedon: "Record Created On",
    utcconversiontimezonecode: "UTC Conversion Time Zone Code",
    versionnumber: "Version Number",
    enddate: "结束日期",
    exchangerate: "汇率",
    begindate: "开始日期",
    importsequencenumber: "Import Sequence Number",
    pricelevelname: "价目表名称",
    statuscode: "状态描述",
    freighttermscode: "货运条款",
    createdate: "建立时间",
    timezoneruleversionnumber: "Time Zone Rule Version Number",
    transactioncurrencyid: "货币",
  },
	views: {
		editview: {
			caption: "价目表",
      		title: "价目表",
		},
		pickupgridview: {
			caption: "价目表",
      		title: "价目表选择表格视图",
		},
		gridview: {
			caption: "价目表",
      		title: "价目表",
		},
		pickupview: {
			caption: "价目表",
      		title: "价目表数据选择视图",
		},
	},
	main_form: {
		details: {
			group1: "pricelevel基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "价目表", 
			srfmajortext: "价目表名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			pricelevelname: "价目表名称", 
			begindate: "开始日期", 
			enddate: "结束日期", 
			paymentmethodcode: "付款方式", 
			description: "说明", 
			pricelevelid: "价目表", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			pricelevelname: "价目表名称",
			begindate: "开始日期",
			enddate: "结束日期",
			paymentmethodcode: "付款方式",
			description: "说明",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "保存",
			tip: "保存",
		},
		tbitem4: {
			caption: "保存并新建",
			tip: "保存并新建",
		},
		tbitem5: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "删除并关闭",
			tip: "删除并关闭",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "新建",
			tip: "新建",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "拷贝",
			tip: "拷贝",
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "新建",
			tip: "新建",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem6: {
			caption: "拷贝",
			tip: "拷贝",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
};