
export default {
  fields: {
    createman: "建立人",
    updateman: "更新人",
    description: "说明",
    statecode: "状态",
    updatedate: "更新时间",
    paymentmethodcode: "付款方式",
    pricelevelid: "价目表",
    shippingmethodcode: "送货方式",
    overriddencreatedon: "Record Created On",
    utcconversiontimezonecode: "UTC Conversion Time Zone Code",
    versionnumber: "Version Number",
    enddate: "结束日期",
    exchangerate: "汇率",
    begindate: "开始日期",
    importsequencenumber: "Import Sequence Number",
    pricelevelname: "价目表名称",
    statuscode: "状态描述",
    freighttermscode: "货运条款",
    createdate: "建立时间",
    timezoneruleversionnumber: "Time Zone Rule Version Number",
    transactioncurrencyid: "货币",
  },
	views: {
		editview: {
			caption: "价目表",
      		title: "价目表",
		},
		pickupgridview: {
			caption: "价目表",
      		title: "价目表选择表格视图",
		},
		gridview: {
			caption: "价目表",
      		title: "价目表",
		},
		pickupview: {
			caption: "价目表",
      		title: "价目表数据选择视图",
		},
	},
	main_form: {
		details: {
			group1: "pricelevel基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "价目表", 
			srfmajortext: "价目表名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			pricelevelname: "价目表名称", 
			begindate: "开始日期", 
			enddate: "结束日期", 
			paymentmethodcode: "付款方式", 
			description: "说明", 
			pricelevelid: "价目表", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			pricelevelname: "价目表名称",
			begindate: "开始日期",
			enddate: "结束日期",
			paymentmethodcode: "付款方式",
			description: "说明",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "Save",
			tip: "Save",
		},
		tbitem4: {
			caption: "Save And New",
			tip: "Save And New",
		},
		tbitem5: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "Remove And Close",
			tip: "Remove And Close Window",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "New",
			tip: "New",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "Copy",
			tip: "Copy {0}",
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem6: {
			caption: "Copy",
			tip: "Copy {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
};