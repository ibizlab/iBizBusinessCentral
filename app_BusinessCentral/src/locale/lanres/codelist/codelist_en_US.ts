export default {
    List__CreatedFromCode: {
        "1": "帐户",
        "4": "潜在顾客",
        "2": "联系人",
        "empty": ""
    },
    Campaignactivity__ChannelTypeCode: {
        "5": "传真",
        "3": "信件",
        "9": "其他",
        "7": "电子邮件",
        "1": "电话",
        "2": "约会",
        "6": "通过邮件合并的传真",
        "4": "通过邮件合并的信件",
        "8": "通过邮件合并的电子邮件",
        "empty": ""
    },
    Salesorder__StateCode: {
        "2": "已取消",
        "3": "已完成",
        "4": "已开发票",
        "1": "已提交",
        "0": "有效",
        "empty": ""
    },
    Connection__StateCode: {
        "1": "停用",
        "0": "可用",
        "empty": ""
    },
    Account__StateCode: {
        "1": "停用",
        "0": "可用",
        "empty": ""
    },
    CodeListJobStatus: {
        "0": "ENABLED",
        "1": "DISABLED",
        "empty": ""
    },
    Account__StatusCode: {
        "2": "停用",
        "1": "可用",
        "empty": ""
    },
    Contact__PaymentTermsCode: {
        "2": "2/10 N30",
        "1": "N30",
        "3": "N45",
        "4": "N60",
        "empty": ""
    },
    AccountClassificationCode: {
        "1": "默认值",
        "empty": ""
    },
    Connection__Record2ObjectTypeCode: {
        "1": "Account",
        "4200": "Activity",
        "10156": "Agreement",
        "10157": "Agreement Booking Date",
        "10158": "Agreement Booking Incident",
        "10159": "Agreement Booking Product",
        "10160": "Agreement Booking Service",
        "10161": "Agreement Booking Service Task",
        "10162": "Agreement Booking Setup",
        "10163": "Agreement Invoice Date",
        "10164": "Agreement Invoice Product",
        "10165": "Agreement Invoice Setup",
        "4201": "Appointment",
        "10042": "Booking Alert",
        "10043": "Booking Alert Status",
        "10045": "Booking Rule",
        "10168": "Booking Timestamp",
        "4400": "Campaign",
        "4402": "Campaign Activity",
        "112": "Case",
        "9400": "Channel Access Profile Rule",
        "123": "Competitor",
        "2": "Contact",
        "1010": "Contract",
        "10173": "Customer Asset",
        "4202": "Email",
        "9700": "Entitlement",
        "9701": "Entitlement Channel",
        "9703": "Entitlement Template Channel",
        "4000": "Facility/Equipment",
        "4204": "Fax",
        "10065": "Fulfillment Preference",
        "9600": "Goal",
        "10181": "Incident Type Characteristic",
        "10182": "Incident Type Product",
        "10183": "Incident Type Service",
        "10187": "Inventory Adjustment",
        "10188": "Inventory Adjustment Product",
        "10189": "Inventory Journal",
        "10190": "Inventory Transfer",
        "1090": "Invoice",
        "10241": "IoT Alert",
        "10242": "IoT Device",
        "10243": "IoT Device Category",
        "10244": "IoT Device Command",
        "10248": "IoT Device Registration History",
        "9953": "Knowledge Article",
        "9930": "Knowledge Base Record",
        "4": "Lead",
        "4207": "Letter",
        "4300": "Marketing List",
        "3": "Opportunity",
        "1088": "Order",
        "10195": "Payment",
        "10196": "Payment Detail",
        "10197": "Payment Method",
        "10198": "Payment Term",
        "4210": "Phone Call",
        "50": "Position",
        "10199": "Postal Code",
        "1022": "Price List",
        "10108": "Process Notes",
        "4710": "Process Session",
        "1024": "Product",
        "10200": "Product Inventory",
        "10027": "Profile Album",
        "10109": "Project",
        "10070": "Project Service Approval",
        "10117": "Project Team Member",
        "10201": "Purchase Order",
        "10202": "Purchase Order Bill",
        "10203": "Purchase Order Product",
        "10204": "Purchase Order Receipt",
        "10205": "Purchase Order Receipt Product",
        "10206": "Purchase Order SubStatus",
        "1084": "Quote",
        "10207": "Quote Booking Incident",
        "10208": "Quote Booking Product",
        "10209": "Quote Booking Service",
        "10210": "Quote Booking Service Task",
        "4251": "Recurring Appointment",
        "4007": "Resource Group",
        "10235": "Resource Restriction (Deprecated)",
        "10061": "Resource Territory",
        "10215": "RMA",
        "10216": "RMA Product",
        "10217": "RMA Receipt",
        "10218": "RMA Receipt Product",
        "10219": "RMA SubStatus",
        "10220": "RTV",
        "10221": "RTV Product",
        "10222": "RTV Substatus",
        "4005": "Scheduling Group",
        "4214": "Service Activity",
        "10224": "Ship Via",
        "4216": "Social Activity",
        "99": "Social Profile",
        "10036": "Survey invite",
        "10037": "Survey response",
        "10064": "System User Scheduler Setting",
        "4212": "Task",
        "10225": "Tax Code",
        "9": "Team",
        "2013": "Territory",
        "10066": "Time Group Detail",
        "10227": "Time Off Request",
        "8": "User",
        "10229": "Warehouse",
        "10230": "Work Order",
        "10231": "Work Order Characteristic (Deprecated)",
        "10233": "Work Order Incident",
        "10234": "Work Order Product",
        "10236": "Work Order Service",
        "10237": "Work Order Service Task",
        "empty": ""
    },
    Quote__StateCode: {
        "3": "已关闭",
        "1": "有效",
        "0": "草稿",
        "2": "赢单",
        "empty": ""
    },
    Productsubstitute__SalesRelationshipType: {
        "1": "交叉销售",
        "3": "替代品",
        "0": "追加销售",
        "2": "配件",
        "empty": ""
    },
    Opportunity__PriorityCode: {
        "1": "默认值",
        "empty": ""
    },
    Invoice__StatusCode: {
        "7": "全额付款(已弃用)",
        "100001": "完成",
        "100003": "已取消",
        "3": "已取消(已弃用)",
        "6": "已安装(适用于服务)",
        "4": "已记帐",
        "5": "已预定(适用于服务)",
        "1": "新建",
        "100002": "部分",
        "2": "部分发货",
        "empty": ""
    },
    Contact__StateCode: {
        "1": "停用",
        "0": "可用",
        "empty": ""
    },
    Knowledgearticleincident__StatusCode: {
        "2": "停用",
        "1": "有效",
        "empty": ""
    },
    Lead__PurchaseTimeFrame: {
        "2": "下一季度",
        "3": "今年",
        "4": "未知",
        "1": "本季度",
        "0": "立即",
        "empty": ""
    },
    Campaign__TypeCode: {
        "3": "事件",
        "5": "其他",
        "1": "广告",
        "2": "直销",
        "4": "联合品牌",
        "empty": ""
    },
    Invoice__StateCode: {
        "3": "已取消",
        "2": "已支付",
        "1": "已结束(已弃用)",
        "0": "有效",
        "empty": ""
    },
    CLAuthCode: {
        "200": "成功",
        "400": "用户不存在",
        "401.1": "密码错误",
        "401.2": "配置错误",
        "403.6": "地址被拒绝",
        "empty": ""
    },
    Lead__LeadSourceCode: {
        "8": "Web",
        "5": "公共关系",
        "10": "其他",
        "9": "口碑",
        "4": "合作伙伴",
        "2": "员工推荐",
        "3": "外部推荐",
        "7": "展销会",
        "1": "广告",
        "6": "研讨会",
        "empty": ""
    },
    Contact__StatusCode: {
        "2": "停用",
        "1": "可用",
        "empty": ""
    },
    Salesliterature__LiteratureTypeCode: {
        "1": "产品表单",
        "7": "价格表单",
        "9": "公司背景",
        "6": "公告",
        "100001": "市场营销宣传材料",
        "8": "手册",
        "5": "新闻",
        "0": "演示文稿",
        "4": "电子表格",
        "2": "策略和步骤",
        "3": "销售宣传资料",
        "empty": ""
    },
    SysOperator: {
        "empty": "",
    },
    CL_HR_0009: {
        "IDCARD": "身份证",
        "PASSPORT": "护照",
        "OTHER": "其他",
        "empty": ""
    },
    Account__OwnershipCode: {
        "1": "公共事业",
        "4": "其他",
        "3": "子公司",
        "2": "私人公司",
        "empty": ""
    },
    AccountCategoryCode: {
        "2": "标准",
        "1": "首选客户",
        "empty": ""
    },
    Connectionrole__ComponentState: {
        "2": "已删除",
        "3": "已删除未发布内容",
        "0": "已发布",
        "1": "未发布",
        "empty": ""
    },
    Campaign__StatusCode: {
        "6": "停用",
        "1": "准备启动",
        "4": "已取消",
        "2": "已启动",
        "3": "已完成",
        "0": "已建议",
        "5": "已暂停",
        "empty": ""
    },
    Lead__StateCode: {
        "2": "不合格",
        "1": "已合格",
        "0": "已开始",
        "empty": ""
    },
    YesNo: {
        "1": "是",
        "0": "否",
        "empty": ""
    },
    Task__PriorityCode: {
        "0": "低",
        "1": "正常",
        "2": "高",
        "empty": ""
    },
    AccountRatingCode: {
        "1": "默认值",
        "empty": ""
    },
    Productsubstitute__Direction: {
        "0": "单向",
        "1": "双向",
        "empty": ""
    },
    Campaign__StateCode: {
        "1": "停用",
        "0": "有效",
        "empty": ""
    },
    PickDataType_AC: {
        "ACCOUNT": "客户",
        "CONTACT": "联系人",
        "empty": ""
    },
    Salesorder__PaymentTermsCode: {
        "2": "2/10 N30",
        "1": "N30",
        "3": "N45",
        "4": "N60",
        "empty": ""
    },
    Account__CustomerTypeCode: {
        "11": "供应商",
        "10": "供货商",
        "12": "其他",
        "5": "合作伙伴",
        "3": "客户",
        "6": "影响者",
        "4": "投资者",
        "7": "新闻界",
        "8": "目标客户",
        "1": "竞争对手",
        "9": "经销商",
        "2": "顾问",
        "empty": ""
    },
    AppType: {
        "INNER": "内置应用",
        "THIRD-PARTY": "第三方应用",
        "empty": ""
    },
    Product__StateCode: {
        "1": "已停用",
        "0": "有效",
        "3": "正在修订",
        "2": "草稿",
        "empty": ""
    },
    PickDataType: {
        "ACCOUNT": "客户",
        "CONTACT": "联系人",
        "LEAD": "潜在客户",
        "empty": ""
    },
    Lead__PurchaseProcess: {
        "0": "个人",
        "1": "委员会",
        "2": "未知",
        "empty": ""
    },
    Salesorder__StatusCode: {
        "100001": "完成",
        "100003": "已开发票",
        "2": "待定",
        "1": "新建",
        "4": "无现金",
        "3": "正在进行",
        "100002": "部分",
        "empty": ""
    },
    CL_HR_0008: {
        "汉": "汉",
        "empty": ""
    },
    Lead__StatusCode: {
        "6": "不再感兴趣",
        "4": "丢单",
        "7": "已取消",
        "3": "已合格",
        "2": "已联系",
        "1": "新建",
        "5": "无法联系",
        "empty": ""
    },
    Incident__StateCode: {
        "2": "已取消",
        "1": "已解决",
        "0": "有效",
        "empty": ""
    },
    Opportunity__PurchaseProcess: {
        "0": "个人",
        "1": "委员会",
        "2": "未知",
        "empty": ""
    },
    Activitypointer__StateCode: {
        "2": "已取消",
        "1": "已完成",
        "0": "已开启",
        "3": "已计划",
        "empty": ""
    },
    Contact__CustomerSizeCode: {
        "1": "默认值",
        "empty": ""
    },
    Goal__FiscalYear: {
        "1970": "1970 财年",
        "1971": "1971 财年",
        "1972": "1972 财年",
        "1973": "1973 财年",
        "1974": "1974 财年",
        "1975": "1975 财年",
        "1976": "1976 财年",
        "1977": "1977 财年",
        "1978": "1978 财年",
        "1979": "1979 财年",
        "1980": "1980 财年",
        "1981": "1981 财年",
        "1982": "1982 财年",
        "1983": "1983 财年",
        "1984": "1984 财年",
        "1985": "1985 财年",
        "1986": "1986 财年",
        "1987": "1987 财年",
        "1988": "1988 财年",
        "1989": "1989 财年",
        "1990": "1990 财年",
        "1991": "1991 财年",
        "1992": "1992 财年",
        "1993": "1993 财年",
        "1994": "1994 财年",
        "1995": "1995 财年",
        "1996": "1996 财年",
        "1997": "1997 财年",
        "1998": "1998 财年",
        "1999": "1999 财年",
        "2000": "2000 财年",
        "2001": "2001 财年",
        "2002": "2002 财年",
        "2003": "2003 财年",
        "2004": "2004 财年",
        "2005": "2005 财年",
        "2006": "2006 财年",
        "2007": "2007 财年",
        "2008": "2008 财年",
        "2009": "2009 财年",
        "2010": "2010 财年",
        "2011": "2011 财年",
        "2012": "2012 财年",
        "2013": "2013 财年",
        "2014": "2014 财年",
        "2015": "2015 财年",
        "2016": "2016 财年",
        "2017": "2017 财年",
        "2018": "2018 财年",
        "2019": "2019 财年",
        "2020": "2020 财年",
        "2021": "2021 财年",
        "2022": "2022 财年",
        "2023": "2023 财年",
        "2024": "2024 财年",
        "2025": "2025 财年",
        "2026": "2026 财年",
        "2027": "2027 财年",
        "2028": "2028 财年",
        "2029": "2029 财年",
        "2030": "2030 财年",
        "2031": "2031 财年",
        "2032": "2032 财年",
        "2033": "2033 财年",
        "2034": "2034 财年",
        "2035": "2035 财年",
        "2036": "2036 财年",
        "2037": "2037 财年",
        "2038": "2038 财年",
        "empty": ""
    },
    Connectionrole__Category: {
        "1": "业务",
        "5": "其他",
        "1000": "利益干系人",
        "2": "家庭",
        "1002": "服务",
        "3": "社团组织",
        "4": "销售",
        "1001": "销售团队",
        "empty": ""
    },
    FreightTermsCode: {
        "1": "FOB",
        "2": "免收费用",
        "empty": ""
    },
    Connectionrole__StatusCode: {
        "2": "停用",
        "1": "可用",
        "empty": ""
    },
    Productpricelevel__PricingMethodCode: {
        "2": "定价百分比",
        "5": "成本加成百分比 - 标准成本",
        "3": "成本加成百分比 – 当前成本",
        "1": "货币金额",
        "4": "边际百分比 – 当前成本",
        "6": "边际百分比 – 标准成本",
        "empty": ""
    },
    Productpricelevel__QuantitySellingCode: {
        "2": "整数",
        "3": "整数和小数",
        "1": "无控制",
        "empty": ""
    },
    IndustryCode: {
        "2": "Agriculture and Non-petrol Natural Resource Extraction",
        "3": "Broadcasting Printing and Publishing",
        "4": "Brokers",
        "5": "Building Supply Retail",
        "6": "Business Services",
        "7": "Consulting",
        "8": "Consumer Services",
        "9": "Design, Direction and Creative Management",
        "10": "Distributors, Dispatchers and Processors",
        "11": "Doctors Offices and Clinics",
        "12": "Durable Manufacturing",
        "13": "Eating and Drinking Places",
        "14": "Entertainment Retail",
        "15": "Equipment Rental and Leasing",
        "17": "Food and Tobacco Processing",
        "18": "Inbound Capital Intensive Processing",
        "19": "Inbound Repair and Services",
        "20": "Insurance",
        "21": "Legal Services",
        "22": "Non-Durable Merchandise Retail",
        "23": "Outbound Consumer Service",
        "24": "Petrochemical Extraction and Distribution",
        "25": "Service Retail",
        "26": "SIG Affiliations",
        "27": "Social Services",
        "28": "Special Outbound Trade Contractors",
        "29": "Specialty Realty",
        "30": "Transportation",
        "31": "Utility Creation and Distribution",
        "32": "Vehicle Retail",
        "33": "Wholesale",
        "1": "会计",
        "16": "金融业",
        "empty": ""
    },
    Activitypointer__StatusCode: {
        "3": "已取消",
        "2": "已完成",
        "1": "已开启",
        "4": "已计划",
        "empty": ""
    },
    Opportunity__StatusCode: {
        "5": "售完",
        "4": "已取消",
        "2": "暂候",
        "1": "正在进行",
        "3": "赢单",
        "empty": ""
    },
    Activitypointer__PriorityCode: {
        "0": "低",
        "1": "正常",
        "2": "高",
        "empty": ""
    },
    Incident__CaseTypeCode: {
        "3": "请求",
        "1": "问题",
        "2": "难题",
        "empty": ""
    },
    List__MemberType: {
        "Account": "客户",
        "Lead": "潜在客户",
        "Contact": "联系人",
        "empty": ""
    },
    CLIBZSex: {
        "男": "男性",
        "女": "女性",
        "性别不详": "性别不详",
        "empty": ""
    },
    ActivityTypeCode: {
        "TASK": "任务",
        "EMAIL": "电子邮件",
        "APPOINTMENT": "约会",
        "PHONECALL": "电话联络",
        "LETTER": "信件",
        "FAX": "传真",
        "SERVICEAPPOINTMENT": "服务活动",
        "CAMPAIGNRESPONSE": "市场活动响应",
        "CAMPAIGNACTIVITY": "市场活动项目",
        "empty": ""
    },
    Pricelevel__PaymentMethodCode: {
        "1": "默认值",
        "empty": ""
    },
    Goal__FiscalPeriod: {
        "1": "1 季度",
        "2": "2 季度",
        "3": "3 季度",
        "4": "4 季度",
        "401": "P1",
        "410": "P10",
        "411": "P11",
        "412": "P12",
        "413": "P13",
        "402": "P2",
        "403": "P3",
        "404": "P4",
        "405": "P5",
        "406": "P6",
        "407": "P7",
        "408": "P8",
        "409": "P9",
        "101": "一月",
        "107": "七月",
        "103": "三月",
        "201": "上半年",
        "202": "下半年",
        "109": "九月",
        "102": "二月",
        "105": "五月",
        "108": "八月",
        "106": "六月",
        "111": "十一月",
        "112": "十二月",
        "110": "十月",
        "104": "四月",
        "301": "年度",
        "empty": ""
    },
    Transactioncurrency__StatusCode: {
        "2": "停用",
        "1": "可用",
        "empty": ""
    },
    Phonecall__PriorityCode: {
        "0": "低",
        "1": "正常",
        "2": "高",
        "empty": ""
    },
    IncidentCustomer: {
        "ACCOUNT": "客户",
        "CONTACT": "联系人",
        "empty": ""
    },
    AddressTypeCode: {
        "empty": ""
    },
    ContentType: {
        "CONTENT": "内容",
        "LINK": "链接",
        "empty": ""
    },
    CL_HR_0001: {
        "COSTCENTER": "成本中心",
        "BUSINESSUNIT": "业务单位",
        "DEPARTMENT": "部门",
        "COMMERCIAL": "商业渠道",
        "empty": ""
    },
    Knowledgearticleincident__StateCode: {
        "1": "停用",
        "0": "有效",
        "empty": ""
    },
    RelationshipsType: {
        "empty": ""
    },
    CL_HR_0010: {
        "MALE": "男",
        "FEMALE": "女",
        "empty": ""
    },
    Transactioncurrency__StateCode: {
        "1": "停用",
        "0": "可用",
        "empty": ""
    },
    Connectionrole__StateCode: {
        "1": "停用",
        "0": "可用",
        "empty": ""
    },
    Contact__FamilyStatusCode: {
        "4": "丧偶",
        "1": "单身",
        "2": "已婚",
        "3": "离异",
        "empty": ""
    },
    Incident__CaseOriginCode: {
        "2483": "Facebook",
        "3986": "Twitter",
        "2": "电子邮件",
        "1": "电话",
        "3": "网站/网页",
        "empty": ""
    },
    Quote__PaymentTermsCode: {
        "2": "2/10 N30",
        "1": "N30",
        "3": "N45",
        "4": "N60",
        "empty": ""
    },
    Knowledgearticle__StatusCode: {
        "4": "审阅中",
        "7": "已发布",
        "12": "已存档",
        "5": "已审批",
        "14": "已拒绝",
        "11": "已拒绝",
        "1": "已拟定",
        "13": "已放弃",
        "6": "已计划",
        "10": "已过期",
        "9": "正在更新",
        "2": "草稿",
        "3": "需要审阅",
        "8": "需要审阅",
        "empty": ""
    },
    FQG_ActivityPointer: {
        "ALL": "全部活动",
        "TASK": "任务",
        "EMAIL": "电子邮件",
        "APPOINTMENT": "约会",
        "PHONECALL": "电话联络",
        "OTHER": "其他",
        "LETTER": "信件",
        "FAX": "传真",
        "SERVICEAPPOINTMENT": "服务活动",
        "CAMPAIGNRESPONSE": "市场活动响应",
        "empty": ""
    },
    Contact__LeadSourceCode: {
        "1": "默认值",
        "empty": ""
    },
    Connection__StatusCode: {
        "2": "停用",
        "1": "可用",
        "empty": ""
    },
    Contact__GenderCode: {
        "2": "女",
        "1": "男",
        "empty": ""
    },
    Knowledgearticle__StateCode: {
        "3": "已发布",
        "5": "已存档",
        "1": "已审批",
        "6": "已放弃",
        "2": "已计划",
        "4": "已过期",
        "0": "草稿",
        "empty": ""
    },
    Metric__AmountDataType: {
        "1": "十进制",
        "2": "整数",
        "0": "金钱",
        "empty": ""
    },
    Incident__PriorityCode: {
        "3": "低",
        "2": "标准",
        "1": "高",
        "empty": ""
    },
    Opportunity__StateCode: {
        "2": "丢单",
        "0": "已开始",
        "1": "赢单",
        "empty": ""
    },
    ShippingMethodCode: {
        "1": "默认值",
        "empty": ""
    },
    Invoice__PaymentTermsCode: {
        "2": "2/10 N30",
        "1": "N30",
        "3": "N45",
        "4": "N60",
        "empty": ""
    },
    Account__PreferredContactMethodCode: {
        "1": "任何方式",
        "4": "传真",
        "2": "电子邮件",
        "3": "电话",
        "5": "邮件",
        "empty": ""
    },
    Opportunity__PurchaseTimeframe: {
        "2": "下一季度",
        "3": "今年",
        "4": "未知",
        "1": "本季度",
        "0": "立即",
        "empty": ""
    },
};