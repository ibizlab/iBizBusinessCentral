import ConnectionRoleAuthServiceBase from './connection-role-auth-service-base';


/**
 * 连接角色权限服务对象
 *
 * @export
 * @class ConnectionRoleAuthService
 * @extends {ConnectionRoleAuthServiceBase}
 */
export default class ConnectionRoleAuthService extends ConnectionRoleAuthServiceBase {

    /**
     * Creates an instance of  ConnectionRoleAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  ConnectionRoleAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}