import SalesOrderDetailAuthServiceBase from './sales-order-detail-auth-service-base';


/**
 * 订单产品权限服务对象
 *
 * @export
 * @class SalesOrderDetailAuthService
 * @extends {SalesOrderDetailAuthServiceBase}
 */
export default class SalesOrderDetailAuthService extends SalesOrderDetailAuthServiceBase {

    /**
     * Creates an instance of  SalesOrderDetailAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  SalesOrderDetailAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}