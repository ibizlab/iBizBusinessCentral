import ProductPriceLevelAuthServiceBase from './product-price-level-auth-service-base';


/**
 * 价目表项权限服务对象
 *
 * @export
 * @class ProductPriceLevelAuthService
 * @extends {ProductPriceLevelAuthServiceBase}
 */
export default class ProductPriceLevelAuthService extends ProductPriceLevelAuthServiceBase {

    /**
     * Creates an instance of  ProductPriceLevelAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  ProductPriceLevelAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}