import KnowledgeArticleAuthServiceBase from './knowledge-article-auth-service-base';


/**
 * 知识文章权限服务对象
 *
 * @export
 * @class KnowledgeArticleAuthService
 * @extends {KnowledgeArticleAuthServiceBase}
 */
export default class KnowledgeArticleAuthService extends KnowledgeArticleAuthServiceBase {

    /**
     * Creates an instance of  KnowledgeArticleAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  KnowledgeArticleAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}