import SalesOrderAuthServiceBase from './sales-order-auth-service-base';


/**
 * 订单权限服务对象
 *
 * @export
 * @class SalesOrderAuthService
 * @extends {SalesOrderAuthServiceBase}
 */
export default class SalesOrderAuthService extends SalesOrderAuthServiceBase {

    /**
     * Creates an instance of  SalesOrderAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  SalesOrderAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}