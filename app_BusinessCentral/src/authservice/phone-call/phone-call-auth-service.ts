import PhoneCallAuthServiceBase from './phone-call-auth-service-base';


/**
 * 电话联络权限服务对象
 *
 * @export
 * @class PhoneCallAuthService
 * @extends {PhoneCallAuthServiceBase}
 */
export default class PhoneCallAuthService extends PhoneCallAuthServiceBase {

    /**
     * Creates an instance of  PhoneCallAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  PhoneCallAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}