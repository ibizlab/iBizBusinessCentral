import IBZPostAuthServiceBase from './ibzpost-auth-service-base';


/**
 * 岗位权限服务对象
 *
 * @export
 * @class IBZPostAuthService
 * @extends {IBZPostAuthServiceBase}
 */
export default class IBZPostAuthService extends IBZPostAuthServiceBase {

    /**
     * Creates an instance of  IBZPostAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  IBZPostAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}