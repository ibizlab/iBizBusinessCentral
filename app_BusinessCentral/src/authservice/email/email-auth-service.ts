import EmailAuthServiceBase from './email-auth-service-base';


/**
 * 电子邮件权限服务对象
 *
 * @export
 * @class EmailAuthService
 * @extends {EmailAuthServiceBase}
 */
export default class EmailAuthService extends EmailAuthServiceBase {

    /**
     * Creates an instance of  EmailAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EmailAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}