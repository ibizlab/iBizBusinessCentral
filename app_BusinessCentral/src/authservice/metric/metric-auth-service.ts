import MetricAuthServiceBase from './metric-auth-service-base';


/**
 * 目标度量权限服务对象
 *
 * @export
 * @class MetricAuthService
 * @extends {MetricAuthServiceBase}
 */
export default class MetricAuthService extends MetricAuthServiceBase {

    /**
     * Creates an instance of  MetricAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  MetricAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}