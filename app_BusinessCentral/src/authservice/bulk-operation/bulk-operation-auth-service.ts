import BulkOperationAuthServiceBase from './bulk-operation-auth-service-base';


/**
 * 快速市场活动权限服务对象
 *
 * @export
 * @class BulkOperationAuthService
 * @extends {BulkOperationAuthServiceBase}
 */
export default class BulkOperationAuthService extends BulkOperationAuthServiceBase {

    /**
     * Creates an instance of  BulkOperationAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  BulkOperationAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}