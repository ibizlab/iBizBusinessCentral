import SalesLiteratureItemAuthServiceBase from './sales-literature-item-auth-service-base';


/**
 * 销售附件权限服务对象
 *
 * @export
 * @class SalesLiteratureItemAuthService
 * @extends {SalesLiteratureItemAuthServiceBase}
 */
export default class SalesLiteratureItemAuthService extends SalesLiteratureItemAuthServiceBase {

    /**
     * Creates an instance of  SalesLiteratureItemAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  SalesLiteratureItemAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}