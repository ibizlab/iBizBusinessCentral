import IBizServiceAuthServiceBase from './ibiz-service-auth-service-base';


/**
 * 服务权限服务对象
 *
 * @export
 * @class IBizServiceAuthService
 * @extends {IBizServiceAuthServiceBase}
 */
export default class IBizServiceAuthService extends IBizServiceAuthServiceBase {

    /**
     * Creates an instance of  IBizServiceAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  IBizServiceAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}