import ProductSubstituteAuthServiceBase from './product-substitute-auth-service-base';


/**
 * 产品替换权限服务对象
 *
 * @export
 * @class ProductSubstituteAuthService
 * @extends {ProductSubstituteAuthServiceBase}
 */
export default class ProductSubstituteAuthService extends ProductSubstituteAuthServiceBase {

    /**
     * Creates an instance of  ProductSubstituteAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  ProductSubstituteAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}