import OpportunityAuthServiceBase from './opportunity-auth-service-base';


/**
 * 商机权限服务对象
 *
 * @export
 * @class OpportunityAuthService
 * @extends {OpportunityAuthServiceBase}
 */
export default class OpportunityAuthService extends OpportunityAuthServiceBase {

    /**
     * Creates an instance of  OpportunityAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  OpportunityAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}