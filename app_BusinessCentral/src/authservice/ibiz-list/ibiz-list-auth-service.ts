import IBizListAuthServiceBase from './ibiz-list-auth-service-base';


/**
 * 市场营销列表权限服务对象
 *
 * @export
 * @class IBizListAuthService
 * @extends {IBizListAuthServiceBase}
 */
export default class IBizListAuthService extends IBizListAuthServiceBase {

    /**
     * Creates an instance of  IBizListAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  IBizListAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}