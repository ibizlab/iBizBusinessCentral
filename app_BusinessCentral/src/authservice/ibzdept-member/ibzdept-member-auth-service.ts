import IBZDeptMemberAuthServiceBase from './ibzdept-member-auth-service-base';


/**
 * 部门成员权限服务对象
 *
 * @export
 * @class IBZDeptMemberAuthService
 * @extends {IBZDeptMemberAuthServiceBase}
 */
export default class IBZDeptMemberAuthService extends IBZDeptMemberAuthServiceBase {

    /**
     * Creates an instance of  IBZDeptMemberAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  IBZDeptMemberAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}