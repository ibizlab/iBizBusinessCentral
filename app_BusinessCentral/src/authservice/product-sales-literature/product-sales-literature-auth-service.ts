import ProductSalesLiteratureAuthServiceBase from './product-sales-literature-auth-service-base';


/**
 * 产品宣传资料权限服务对象
 *
 * @export
 * @class ProductSalesLiteratureAuthService
 * @extends {ProductSalesLiteratureAuthServiceBase}
 */
export default class ProductSalesLiteratureAuthService extends ProductSalesLiteratureAuthServiceBase {

    /**
     * Creates an instance of  ProductSalesLiteratureAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  ProductSalesLiteratureAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}