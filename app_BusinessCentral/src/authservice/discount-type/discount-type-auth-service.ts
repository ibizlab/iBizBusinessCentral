import DiscountTypeAuthServiceBase from './discount-type-auth-service-base';


/**
 * 折扣表权限服务对象
 *
 * @export
 * @class DiscountTypeAuthService
 * @extends {DiscountTypeAuthServiceBase}
 */
export default class DiscountTypeAuthService extends DiscountTypeAuthServiceBase {

    /**
     * Creates an instance of  DiscountTypeAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  DiscountTypeAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}