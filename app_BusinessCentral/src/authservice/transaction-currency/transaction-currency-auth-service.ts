import TransactionCurrencyAuthServiceBase from './transaction-currency-auth-service-base';


/**
 * 货币权限服务对象
 *
 * @export
 * @class TransactionCurrencyAuthService
 * @extends {TransactionCurrencyAuthServiceBase}
 */
export default class TransactionCurrencyAuthService extends TransactionCurrencyAuthServiceBase {

    /**
     * Creates an instance of  TransactionCurrencyAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  TransactionCurrencyAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}