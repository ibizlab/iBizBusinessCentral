import CampaignCampaignAuthServiceBase from './campaign-campaign-auth-service-base';


/**
 * 市场活动-市场活动权限服务对象
 *
 * @export
 * @class CampaignCampaignAuthService
 * @extends {CampaignCampaignAuthServiceBase}
 */
export default class CampaignCampaignAuthService extends CampaignCampaignAuthServiceBase {

    /**
     * Creates an instance of  CampaignCampaignAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  CampaignCampaignAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}