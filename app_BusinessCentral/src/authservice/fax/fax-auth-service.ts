import FaxAuthServiceBase from './fax-auth-service-base';


/**
 * 传真权限服务对象
 *
 * @export
 * @class FaxAuthService
 * @extends {FaxAuthServiceBase}
 */
export default class FaxAuthService extends FaxAuthServiceBase {

    /**
     * Creates an instance of  FaxAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  FaxAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}