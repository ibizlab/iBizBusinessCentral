import QuoteDetailAuthServiceBase from './quote-detail-auth-service-base';


/**
 * 报价单产品权限服务对象
 *
 * @export
 * @class QuoteDetailAuthService
 * @extends {QuoteDetailAuthServiceBase}
 */
export default class QuoteDetailAuthService extends QuoteDetailAuthServiceBase {

    /**
     * Creates an instance of  QuoteDetailAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  QuoteDetailAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}