import CompetitorSalesLiteratureAuthServiceBase from './competitor-sales-literature-auth-service-base';


/**
 * 竞争对手宣传资料权限服务对象
 *
 * @export
 * @class CompetitorSalesLiteratureAuthService
 * @extends {CompetitorSalesLiteratureAuthServiceBase}
 */
export default class CompetitorSalesLiteratureAuthService extends CompetitorSalesLiteratureAuthServiceBase {

    /**
     * Creates an instance of  CompetitorSalesLiteratureAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  CompetitorSalesLiteratureAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}