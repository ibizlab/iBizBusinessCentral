import KnowledgeArticleIncidentAuthServiceBase from './knowledge-article-incident-auth-service-base';


/**
 * 知识文章事件权限服务对象
 *
 * @export
 * @class KnowledgeArticleIncidentAuthService
 * @extends {KnowledgeArticleIncidentAuthServiceBase}
 */
export default class KnowledgeArticleIncidentAuthService extends KnowledgeArticleIncidentAuthServiceBase {

    /**
     * Creates an instance of  KnowledgeArticleIncidentAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  KnowledgeArticleIncidentAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}