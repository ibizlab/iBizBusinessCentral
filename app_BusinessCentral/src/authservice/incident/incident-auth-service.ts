import IncidentAuthServiceBase from './incident-auth-service-base';


/**
 * 案例权限服务对象
 *
 * @export
 * @class IncidentAuthService
 * @extends {IncidentAuthServiceBase}
 */
export default class IncidentAuthService extends IncidentAuthServiceBase {

    /**
     * Creates an instance of  IncidentAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  IncidentAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}