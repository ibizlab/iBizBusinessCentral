import ProductAssociationAuthServiceBase from './product-association-auth-service-base';


/**
 * 产品关联权限服务对象
 *
 * @export
 * @class ProductAssociationAuthService
 * @extends {ProductAssociationAuthServiceBase}
 */
export default class ProductAssociationAuthService extends ProductAssociationAuthServiceBase {

    /**
     * Creates an instance of  ProductAssociationAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  ProductAssociationAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}