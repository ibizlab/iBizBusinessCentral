import OMHierarchyCatAuthServiceBase from './omhierarchy-cat-auth-service-base';


/**
 * 结构层次类别权限服务对象
 *
 * @export
 * @class OMHierarchyCatAuthService
 * @extends {OMHierarchyCatAuthServiceBase}
 */
export default class OMHierarchyCatAuthService extends OMHierarchyCatAuthServiceBase {

    /**
     * Creates an instance of  OMHierarchyCatAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  OMHierarchyCatAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}