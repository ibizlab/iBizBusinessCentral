import OMHierarchyAuthServiceBase from './omhierarchy-auth-service-base';


/**
 * 组织层次结构权限服务对象
 *
 * @export
 * @class OMHierarchyAuthService
 * @extends {OMHierarchyAuthServiceBase}
 */
export default class OMHierarchyAuthService extends OMHierarchyAuthServiceBase {

    /**
     * Creates an instance of  OMHierarchyAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  OMHierarchyAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}