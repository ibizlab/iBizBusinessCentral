import OrganizationAuthServiceBase from './organization-auth-service-base';


/**
 * 组织权限服务对象
 *
 * @export
 * @class OrganizationAuthService
 * @extends {OrganizationAuthServiceBase}
 */
export default class OrganizationAuthService extends OrganizationAuthServiceBase {

    /**
     * Creates an instance of  OrganizationAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  OrganizationAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}