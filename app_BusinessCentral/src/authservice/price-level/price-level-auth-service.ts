import PriceLevelAuthServiceBase from './price-level-auth-service-base';


/**
 * 价目表权限服务对象
 *
 * @export
 * @class PriceLevelAuthService
 * @extends {PriceLevelAuthServiceBase}
 */
export default class PriceLevelAuthService extends PriceLevelAuthServiceBase {

    /**
     * Creates an instance of  PriceLevelAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  PriceLevelAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}