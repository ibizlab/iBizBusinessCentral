import LegalAuthServiceBase from './legal-auth-service-base';


/**
 * 法人权限服务对象
 *
 * @export
 * @class LegalAuthService
 * @extends {LegalAuthServiceBase}
 */
export default class LegalAuthService extends LegalAuthServiceBase {

    /**
     * Creates an instance of  LegalAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  LegalAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}