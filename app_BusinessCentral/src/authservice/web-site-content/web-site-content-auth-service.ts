import WebSiteContentAuthServiceBase from './web-site-content-auth-service-base';


/**
 * 站点内容权限服务对象
 *
 * @export
 * @class WebSiteContentAuthService
 * @extends {WebSiteContentAuthServiceBase}
 */
export default class WebSiteContentAuthService extends WebSiteContentAuthServiceBase {

    /**
     * Creates an instance of  WebSiteContentAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  WebSiteContentAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}