import OperationUnitAuthServiceBase from './operation-unit-auth-service-base';


/**
 * 运营单位权限服务对象
 *
 * @export
 * @class OperationUnitAuthService
 * @extends {OperationUnitAuthServiceBase}
 */
export default class OperationUnitAuthService extends OperationUnitAuthServiceBase {

    /**
     * Creates an instance of  OperationUnitAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  OperationUnitAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}