import EmployeeAuthServiceBase from './employee-auth-service-base';


/**
 * 员工权限服务对象
 *
 * @export
 * @class EmployeeAuthService
 * @extends {EmployeeAuthServiceBase}
 */
export default class EmployeeAuthService extends EmployeeAuthServiceBase {

    /**
     * Creates an instance of  EmployeeAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EmployeeAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}