import LeadCompetitorAuthServiceBase from './lead-competitor-auth-service-base';


/**
 * 潜在客户对手权限服务对象
 *
 * @export
 * @class LeadCompetitorAuthService
 * @extends {LeadCompetitorAuthServiceBase}
 */
export default class LeadCompetitorAuthService extends LeadCompetitorAuthServiceBase {

    /**
     * Creates an instance of  LeadCompetitorAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  LeadCompetitorAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}