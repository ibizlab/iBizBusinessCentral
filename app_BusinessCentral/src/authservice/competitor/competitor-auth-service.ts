import CompetitorAuthServiceBase from './competitor-auth-service-base';


/**
 * 竞争对手权限服务对象
 *
 * @export
 * @class CompetitorAuthService
 * @extends {CompetitorAuthServiceBase}
 */
export default class CompetitorAuthService extends CompetitorAuthServiceBase {

    /**
     * Creates an instance of  CompetitorAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  CompetitorAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}