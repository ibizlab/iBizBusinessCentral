import OrganizationUIServiceBase from './organization-ui-service-base';

/**
 * 组织UI服务对象
 *
 * @export
 * @class OrganizationUIService
 */
export default class OrganizationUIService extends OrganizationUIServiceBase {

    /**
     * Creates an instance of  OrganizationUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  OrganizationUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}