import KnowledgeArticleUIServiceBase from './knowledge-article-ui-service-base';

/**
 * 知识文章UI服务对象
 *
 * @export
 * @class KnowledgeArticleUIService
 */
export default class KnowledgeArticleUIService extends KnowledgeArticleUIServiceBase {

    /**
     * Creates an instance of  KnowledgeArticleUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  KnowledgeArticleUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}