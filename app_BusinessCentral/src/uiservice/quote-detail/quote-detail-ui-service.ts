import QuoteDetailUIServiceBase from './quote-detail-ui-service-base';

/**
 * 报价单产品UI服务对象
 *
 * @export
 * @class QuoteDetailUIService
 */
export default class QuoteDetailUIService extends QuoteDetailUIServiceBase {

    /**
     * Creates an instance of  QuoteDetailUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  QuoteDetailUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}