import IBizServiceUIServiceBase from './ibiz-service-ui-service-base';

/**
 * 服务UI服务对象
 *
 * @export
 * @class IBizServiceUIService
 */
export default class IBizServiceUIService extends IBizServiceUIServiceBase {

    /**
     * Creates an instance of  IBizServiceUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  IBizServiceUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}