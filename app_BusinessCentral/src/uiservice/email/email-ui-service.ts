import EmailUIServiceBase from './email-ui-service-base';

/**
 * 电子邮件UI服务对象
 *
 * @export
 * @class EmailUIService
 */
export default class EmailUIService extends EmailUIServiceBase {

    /**
     * Creates an instance of  EmailUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EmailUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}