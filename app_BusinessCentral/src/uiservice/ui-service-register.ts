/**
 * UI服务注册中心
 *
 * @export
 * @class UIServiceRegister
 */
export class UIServiceRegister {

    /**
     * 所有UI实体服务Map
     *
     * @protected
     * @type {*}
     * @memberof UIServiceRegister
     */
    protected allUIService: Map<string, () => Promise<any>> = new Map();

    /**
     * 已加载UI实体服务Map缓存
     *
     * @protected
     * @type {Map<string, any>}
     * @memberof UIServiceRegister
     */
    protected serviceCache: Map<string, any> = new Map();

    /**
     * Creates an instance of UIServiceRegister.
     * @memberof UIServiceRegister
     */
    constructor() {
        this.init();
    }

    /**
     * 初始化
     *
     * @protected
     * @memberof UIServiceRegister
     */
    protected init(): void {
                this.allUIService.set('dictoption', () => import('@/uiservice/dict-option/dict-option-ui-service'));
        this.allUIService.set('uomschedule', () => import('@/uiservice/uom-schedule/uom-schedule-ui-service'));
        this.allUIService.set('wfremodel', () => import('@/uiservice/wfremodel/wfremodel-ui-service'));
        this.allUIService.set('productsalesliterature', () => import('@/uiservice/product-sales-literature/product-sales-literature-ui-service'));
        this.allUIService.set('listcontact', () => import('@/uiservice/list-contact/list-contact-ui-service'));
        this.allUIService.set('territory', () => import('@/uiservice/territory/territory-ui-service'));
        this.allUIService.set('quotedetail', () => import('@/uiservice/quote-detail/quote-detail-ui-service'));
        this.allUIService.set('sysapp', () => import('@/uiservice/sys-app/sys-app-ui-service'));
        this.allUIService.set('opportunity', () => import('@/uiservice/opportunity/opportunity-ui-service'));
        this.allUIService.set('entitlement', () => import('@/uiservice/entitlement/entitlement-ui-service'));
        this.allUIService.set('discounttype', () => import('@/uiservice/discount-type/discount-type-ui-service'));
        this.allUIService.set('salesliteratureitem', () => import('@/uiservice/sales-literature-item/sales-literature-item-ui-service'));
        this.allUIService.set('campaigncampaign', () => import('@/uiservice/campaign-campaign/campaign-campaign-ui-service'));
        this.allUIService.set('campaignlist', () => import('@/uiservice/campaign-list/campaign-list-ui-service'));
        this.allUIService.set('ibizlist', () => import('@/uiservice/ibiz-list/ibiz-list-ui-service'));
        this.allUIService.set('dictcatalog', () => import('@/uiservice/dict-catalog/dict-catalog-ui-service'));
        this.allUIService.set('salesorder', () => import('@/uiservice/sales-order/sales-order-ui-service'));
        this.allUIService.set('account', () => import('@/uiservice/account/account-ui-service'));
        this.allUIService.set('languagelocale', () => import('@/uiservice/language-locale/language-locale-ui-service'));
        this.allUIService.set('serviceappointment', () => import('@/uiservice/service-appointment/service-appointment-ui-service'));
        this.allUIService.set('omhierarchy', () => import('@/uiservice/omhierarchy/omhierarchy-ui-service'));
        this.allUIService.set('ibizservice', () => import('@/uiservice/ibiz-service/ibiz-service-ui-service'));
        this.allUIService.set('goal', () => import('@/uiservice/goal/goal-ui-service'));
        this.allUIService.set('opportunitycompetitor', () => import('@/uiservice/opportunity-competitor/opportunity-competitor-ui-service'));
        this.allUIService.set('ibzdepartment', () => import('@/uiservice/ibzdepartment/ibzdepartment-ui-service'));
        this.allUIService.set('quote', () => import('@/uiservice/quote/quote-ui-service'));
        this.allUIService.set('transactioncurrency', () => import('@/uiservice/transaction-currency/transaction-currency-ui-service'));
        this.allUIService.set('invoicedetail', () => import('@/uiservice/invoice-detail/invoice-detail-ui-service'));
        this.allUIService.set('employee', () => import('@/uiservice/employee/employee-ui-service'));
        this.allUIService.set('knowledgearticle', () => import('@/uiservice/knowledge-article/knowledge-article-ui-service'));
        this.allUIService.set('jobsregistry', () => import('@/uiservice/jobs-registry/jobs-registry-ui-service'));
        this.allUIService.set('wfgroup', () => import('@/uiservice/wfgroup/wfgroup-ui-service'));
        this.allUIService.set('wfuser', () => import('@/uiservice/wfuser/wfuser-ui-service'));
        this.allUIService.set('lead', () => import('@/uiservice/lead/lead-ui-service'));
        this.allUIService.set('competitor', () => import('@/uiservice/competitor/competitor-ui-service'));
        this.allUIService.set('campaign', () => import('@/uiservice/campaign/campaign-ui-service'));
        this.allUIService.set('phonecall', () => import('@/uiservice/phone-call/phone-call-ui-service'));
        this.allUIService.set('sysrolepermission', () => import('@/uiservice/sys-role-permission/sys-role-permission-ui-service'));
        this.allUIService.set('appointment', () => import('@/uiservice/appointment/appointment-ui-service'));
        this.allUIService.set('sysuser', () => import('@/uiservice/sys-user/sys-user-ui-service'));
        this.allUIService.set('invoice', () => import('@/uiservice/invoice/invoice-ui-service'));
        this.allUIService.set('jobslog', () => import('@/uiservice/jobs-log/jobs-log-ui-service'));
        this.allUIService.set('incidentcustomer', () => import('@/uiservice/incident-customer/incident-customer-ui-service'));
        this.allUIService.set('metric', () => import('@/uiservice/metric/metric-ui-service'));
        this.allUIService.set('leadcompetitor', () => import('@/uiservice/lead-competitor/lead-competitor-ui-service'));
        this.allUIService.set('syspermission', () => import('@/uiservice/sys-permission/sys-permission-ui-service'));
        this.allUIService.set('pricelevel', () => import('@/uiservice/price-level/price-level-ui-service'));
        this.allUIService.set('productpricelevel', () => import('@/uiservice/product-price-level/product-price-level-ui-service'));
        this.allUIService.set('connectionrole', () => import('@/uiservice/connection-role/connection-role-ui-service'));
        this.allUIService.set('wfprocessdefinition', () => import('@/uiservice/wfprocess-definition/wfprocess-definition-ui-service'));
        this.allUIService.set('competitorsalesliterature', () => import('@/uiservice/competitor-sales-literature/competitor-sales-literature-ui-service'));
        this.allUIService.set('legal', () => import('@/uiservice/legal/legal-ui-service'));
        this.allUIService.set('knowledgearticleincident', () => import('@/uiservice/knowledge-article-incident/knowledge-article-incident-ui-service'));
        this.allUIService.set('task', () => import('@/uiservice/task/task-ui-service'));
        this.allUIService.set('ibzemployee', () => import('@/uiservice/ibzemployee/ibzemployee-ui-service'));
        this.allUIService.set('salesorderdetail', () => import('@/uiservice/sales-order-detail/sales-order-detail-ui-service'));
        this.allUIService.set('sysauthlog', () => import('@/uiservice/sys-auth-log/sys-auth-log-ui-service'));
        this.allUIService.set('omhierarchypurposeref', () => import('@/uiservice/omhierarchy-purpose-ref/omhierarchy-purpose-ref-ui-service'));
        this.allUIService.set('sysuserrole', () => import('@/uiservice/sys-user-role/sys-user-role-ui-service'));
        this.allUIService.set('bulkoperation', () => import('@/uiservice/bulk-operation/bulk-operation-ui-service'));
        this.allUIService.set('jobsinfo', () => import('@/uiservice/jobs-info/jobs-info-ui-service'));
        this.allUIService.set('ibzorganization', () => import('@/uiservice/ibzorganization/ibzorganization-ui-service'));
        this.allUIService.set('omhierarchycat', () => import('@/uiservice/omhierarchy-cat/omhierarchy-cat-ui-service'));
        this.allUIService.set('productassociation', () => import('@/uiservice/product-association/product-association-ui-service'));
        this.allUIService.set('campaignactivity', () => import('@/uiservice/campaign-activity/campaign-activity-ui-service'));
        this.allUIService.set('activitypointer', () => import('@/uiservice/activity-pointer/activity-pointer-ui-service'));
        this.allUIService.set('campaignresponse', () => import('@/uiservice/campaign-response/campaign-response-ui-service'));
        this.allUIService.set('listaccount', () => import('@/uiservice/list-account/list-account-ui-service'));
        this.allUIService.set('sysrole', () => import('@/uiservice/sys-role/sys-role-ui-service'));
        this.allUIService.set('salesliterature', () => import('@/uiservice/sales-literature/sales-literature-ui-service'));
        this.allUIService.set('subject', () => import('@/uiservice/subject/subject-ui-service'));
        this.allUIService.set('operationunit', () => import('@/uiservice/operation-unit/operation-unit-ui-service'));
        this.allUIService.set('ibzdeptmember', () => import('@/uiservice/ibzdept-member/ibzdept-member-ui-service'));
        this.allUIService.set('letter', () => import('@/uiservice/letter/letter-ui-service'));
        this.allUIService.set('uom', () => import('@/uiservice/uom/uom-ui-service'));
        this.allUIService.set('product', () => import('@/uiservice/product/product-ui-service'));
        this.allUIService.set('multipickdata', () => import('@/uiservice/multi-pick-data/multi-pick-data-ui-service'));
        this.allUIService.set('wfmember', () => import('@/uiservice/wfmember/wfmember-ui-service'));
        this.allUIService.set('opportunityproduct', () => import('@/uiservice/opportunity-product/opportunity-product-ui-service'));
        this.allUIService.set('contact', () => import('@/uiservice/contact/contact-ui-service'));
        this.allUIService.set('omhierarchypurpose', () => import('@/uiservice/omhierarchy-purpose/omhierarchy-purpose-ui-service'));
        this.allUIService.set('incident', () => import('@/uiservice/incident/incident-ui-service'));
        this.allUIService.set('fax', () => import('@/uiservice/fax/fax-ui-service'));
        this.allUIService.set('competitorproduct', () => import('@/uiservice/competitor-product/competitor-product-ui-service'));
        this.allUIService.set('listlead', () => import('@/uiservice/list-lead/list-lead-ui-service'));
        this.allUIService.set('productsubstitute', () => import('@/uiservice/product-substitute/product-substitute-ui-service'));
        this.allUIService.set('websitecontent', () => import('@/uiservice/web-site-content/web-site-content-ui-service'));
        this.allUIService.set('organization', () => import('@/uiservice/organization/organization-ui-service'));
        this.allUIService.set('email', () => import('@/uiservice/email/email-ui-service'));
        this.allUIService.set('ibzpost', () => import('@/uiservice/ibzpost/ibzpost-ui-service'));
        this.allUIService.set('connection', () => import('@/uiservice/connection/connection-ui-service'));
    }

    /**
     * 加载服务实体
     *
     * @protected
     * @param {string} serviceName
     * @returns {Promise<any>}
     * @memberof UIServiceRegister
     */
    protected async loadService(serviceName: string): Promise<any> {
        const service = this.allUIService.get(serviceName);
        if (service) {
            return service();
        }
    }

    /**
     * 获取应用实体服务
     *
     * @param {string} name
     * @returns {Promise<any>}
     * @memberof UIServiceRegister
     */
    public async getService(name: string): Promise<any> {
        if (this.serviceCache.has(name)) {
            return this.serviceCache.get(name);
        }
        const entityService: any = await this.loadService(name);
        if (entityService && entityService.default) {
            const instance: any = new entityService.default();
            this.serviceCache.set(name, instance);
            return instance;
        }
    }

}
export const uiServiceRegister: UIServiceRegister = new UIServiceRegister();