import OpportunityUIServiceBase from './opportunity-ui-service-base';

/**
 * 商机UI服务对象
 *
 * @export
 * @class OpportunityUIService
 */
export default class OpportunityUIService extends OpportunityUIServiceBase {

    /**
     * Creates an instance of  OpportunityUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  OpportunityUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}