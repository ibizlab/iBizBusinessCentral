import TerritoryUIServiceBase from './territory-ui-service-base';

/**
 * 区域UI服务对象
 *
 * @export
 * @class TerritoryUIService
 */
export default class TerritoryUIService extends TerritoryUIServiceBase {

    /**
     * Creates an instance of  TerritoryUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  TerritoryUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}