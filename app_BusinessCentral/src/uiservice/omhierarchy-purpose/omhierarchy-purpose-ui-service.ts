import OMHierarchyPurposeUIServiceBase from './omhierarchy-purpose-ui-service-base';

/**
 * 组织层次结构应用UI服务对象
 *
 * @export
 * @class OMHierarchyPurposeUIService
 */
export default class OMHierarchyPurposeUIService extends OMHierarchyPurposeUIServiceBase {

    /**
     * Creates an instance of  OMHierarchyPurposeUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  OMHierarchyPurposeUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}