import ConnectionUIServiceBase from './connection-ui-service-base';

/**
 * 连接UI服务对象
 *
 * @export
 * @class ConnectionUIService
 */
export default class ConnectionUIService extends ConnectionUIServiceBase {

    /**
     * Creates an instance of  ConnectionUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  ConnectionUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}