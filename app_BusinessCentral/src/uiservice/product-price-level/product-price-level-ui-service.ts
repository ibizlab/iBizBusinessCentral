import ProductPriceLevelUIServiceBase from './product-price-level-ui-service-base';

/**
 * 价目表项UI服务对象
 *
 * @export
 * @class ProductPriceLevelUIService
 */
export default class ProductPriceLevelUIService extends ProductPriceLevelUIServiceBase {

    /**
     * Creates an instance of  ProductPriceLevelUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  ProductPriceLevelUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}