import MultiPickDataUIServiceBase from './multi-pick-data-ui-service-base';

/**
 * 多类选择实体UI服务对象
 *
 * @export
 * @class MultiPickDataUIService
 */
export default class MultiPickDataUIService extends MultiPickDataUIServiceBase {

    /**
     * Creates an instance of  MultiPickDataUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  MultiPickDataUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}