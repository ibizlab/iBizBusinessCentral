import KnowledgeArticleIncidentUIServiceBase from './knowledge-article-incident-ui-service-base';

/**
 * 知识文章事件UI服务对象
 *
 * @export
 * @class KnowledgeArticleIncidentUIService
 */
export default class KnowledgeArticleIncidentUIService extends KnowledgeArticleIncidentUIServiceBase {

    /**
     * Creates an instance of  KnowledgeArticleIncidentUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  KnowledgeArticleIncidentUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}