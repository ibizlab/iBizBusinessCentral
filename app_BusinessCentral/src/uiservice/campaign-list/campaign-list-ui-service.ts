import CampaignListUIServiceBase from './campaign-list-ui-service-base';

/**
 * 市场活动-营销列表UI服务对象
 *
 * @export
 * @class CampaignListUIService
 */
export default class CampaignListUIService extends CampaignListUIServiceBase {

    /**
     * Creates an instance of  CampaignListUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  CampaignListUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}