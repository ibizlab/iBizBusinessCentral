import QuoteUIServiceBase from './quote-ui-service-base';

/**
 * 报价单UI服务对象
 *
 * @export
 * @class QuoteUIService
 */
export default class QuoteUIService extends QuoteUIServiceBase {

    /**
     * Creates an instance of  QuoteUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  QuoteUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}