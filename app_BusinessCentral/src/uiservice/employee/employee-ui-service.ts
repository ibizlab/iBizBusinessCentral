import EmployeeUIServiceBase from './employee-ui-service-base';

/**
 * 员工UI服务对象
 *
 * @export
 * @class EmployeeUIService
 */
export default class EmployeeUIService extends EmployeeUIServiceBase {

    /**
     * Creates an instance of  EmployeeUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EmployeeUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}