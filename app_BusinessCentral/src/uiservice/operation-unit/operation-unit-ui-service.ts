import OperationUnitUIServiceBase from './operation-unit-ui-service-base';

/**
 * 运营单位UI服务对象
 *
 * @export
 * @class OperationUnitUIService
 */
export default class OperationUnitUIService extends OperationUnitUIServiceBase {

    /**
     * Creates an instance of  OperationUnitUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  OperationUnitUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}