import OMHierarchyCatUIServiceBase from './omhierarchy-cat-ui-service-base';

/**
 * 结构层次类别UI服务对象
 *
 * @export
 * @class OMHierarchyCatUIService
 */
export default class OMHierarchyCatUIService extends OMHierarchyCatUIServiceBase {

    /**
     * Creates an instance of  OMHierarchyCatUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  OMHierarchyCatUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}