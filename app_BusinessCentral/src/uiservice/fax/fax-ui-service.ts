import FaxUIServiceBase from './fax-ui-service-base';

/**
 * 传真UI服务对象
 *
 * @export
 * @class FaxUIService
 */
export default class FaxUIService extends FaxUIServiceBase {

    /**
     * Creates an instance of  FaxUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  FaxUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}