import SalesOrderUIServiceBase from './sales-order-ui-service-base';

/**
 * 订单UI服务对象
 *
 * @export
 * @class SalesOrderUIService
 */
export default class SalesOrderUIService extends SalesOrderUIServiceBase {

    /**
     * Creates an instance of  SalesOrderUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  SalesOrderUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}