import DiscountTypeUIServiceBase from './discount-type-ui-service-base';

/**
 * 折扣表UI服务对象
 *
 * @export
 * @class DiscountTypeUIService
 */
export default class DiscountTypeUIService extends DiscountTypeUIServiceBase {

    /**
     * Creates an instance of  DiscountTypeUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  DiscountTypeUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}