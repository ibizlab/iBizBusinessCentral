import ActivityPointerUIServiceBase from './activity-pointer-ui-service-base';

/**
 * 活动UI服务对象
 *
 * @export
 * @class ActivityPointerUIService
 */
export default class ActivityPointerUIService extends ActivityPointerUIServiceBase {

    /**
     * Creates an instance of  ActivityPointerUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  ActivityPointerUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}