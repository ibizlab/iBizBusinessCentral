import IBZDeptMemberUIServiceBase from './ibzdept-member-ui-service-base';

/**
 * 部门成员UI服务对象
 *
 * @export
 * @class IBZDeptMemberUIService
 */
export default class IBZDeptMemberUIService extends IBZDeptMemberUIServiceBase {

    /**
     * Creates an instance of  IBZDeptMemberUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  IBZDeptMemberUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}