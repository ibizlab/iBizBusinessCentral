import ProductAssociationUIServiceBase from './product-association-ui-service-base';

/**
 * 产品关联UI服务对象
 *
 * @export
 * @class ProductAssociationUIService
 */
export default class ProductAssociationUIService extends ProductAssociationUIServiceBase {

    /**
     * Creates an instance of  ProductAssociationUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  ProductAssociationUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}