import ContactUIServiceBase from './contact-ui-service-base';

/**
 * 联系人UI服务对象
 *
 * @export
 * @class ContactUIService
 */
export default class ContactUIService extends ContactUIServiceBase {

    /**
     * Creates an instance of  ContactUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  ContactUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}