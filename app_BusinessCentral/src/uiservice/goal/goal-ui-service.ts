import GoalUIServiceBase from './goal-ui-service-base';

/**
 * 目标UI服务对象
 *
 * @export
 * @class GoalUIService
 */
export default class GoalUIService extends GoalUIServiceBase {

    /**
     * Creates an instance of  GoalUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  GoalUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}