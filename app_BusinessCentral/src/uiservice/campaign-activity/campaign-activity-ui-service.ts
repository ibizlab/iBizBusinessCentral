import CampaignActivityUIServiceBase from './campaign-activity-ui-service-base';

/**
 * 市场活动项目UI服务对象
 *
 * @export
 * @class CampaignActivityUIService
 */
export default class CampaignActivityUIService extends CampaignActivityUIServiceBase {

    /**
     * Creates an instance of  CampaignActivityUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  CampaignActivityUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}