import PhoneCallUIServiceBase from './phone-call-ui-service-base';

/**
 * 电话联络UI服务对象
 *
 * @export
 * @class PhoneCallUIService
 */
export default class PhoneCallUIService extends PhoneCallUIServiceBase {

    /**
     * Creates an instance of  PhoneCallUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  PhoneCallUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}