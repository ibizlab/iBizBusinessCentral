import SalesLiteratureItemUIServiceBase from './sales-literature-item-ui-service-base';

/**
 * 销售附件UI服务对象
 *
 * @export
 * @class SalesLiteratureItemUIService
 */
export default class SalesLiteratureItemUIService extends SalesLiteratureItemUIServiceBase {

    /**
     * Creates an instance of  SalesLiteratureItemUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  SalesLiteratureItemUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}