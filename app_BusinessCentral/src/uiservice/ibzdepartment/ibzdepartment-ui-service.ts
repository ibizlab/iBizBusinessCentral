import IBZDepartmentUIServiceBase from './ibzdepartment-ui-service-base';

/**
 * 部门UI服务对象
 *
 * @export
 * @class IBZDepartmentUIService
 */
export default class IBZDepartmentUIService extends IBZDepartmentUIServiceBase {

    /**
     * Creates an instance of  IBZDepartmentUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  IBZDepartmentUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}