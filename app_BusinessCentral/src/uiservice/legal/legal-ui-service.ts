import LegalUIServiceBase from './legal-ui-service-base';

/**
 * 法人UI服务对象
 *
 * @export
 * @class LegalUIService
 */
export default class LegalUIService extends LegalUIServiceBase {

    /**
     * Creates an instance of  LegalUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  LegalUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}