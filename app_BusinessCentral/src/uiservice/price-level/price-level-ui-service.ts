import PriceLevelUIServiceBase from './price-level-ui-service-base';

/**
 * 价目表UI服务对象
 *
 * @export
 * @class PriceLevelUIService
 */
export default class PriceLevelUIService extends PriceLevelUIServiceBase {

    /**
     * Creates an instance of  PriceLevelUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  PriceLevelUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}