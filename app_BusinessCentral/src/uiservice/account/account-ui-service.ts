import AccountUIServiceBase from './account-ui-service-base';

/**
 * 客户UI服务对象
 *
 * @export
 * @class AccountUIService
 */
export default class AccountUIService extends AccountUIServiceBase {

    /**
     * Creates an instance of  AccountUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  AccountUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}