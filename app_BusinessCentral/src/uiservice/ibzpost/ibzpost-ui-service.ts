import IBZPostUIServiceBase from './ibzpost-ui-service-base';

/**
 * 岗位UI服务对象
 *
 * @export
 * @class IBZPostUIService
 */
export default class IBZPostUIService extends IBZPostUIServiceBase {

    /**
     * Creates an instance of  IBZPostUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  IBZPostUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}