package cn.ibizlab.businesscentral.centralapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.service.domain.KnowledgeArticleIncident;
import cn.ibizlab.businesscentral.centralapi.dto.KnowledgeArticleIncidentDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CentralApiKnowledgeArticleIncidentMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface KnowledgeArticleIncidentMapping extends MappingBase<KnowledgeArticleIncidentDTO, KnowledgeArticleIncident> {


}

