package cn.ibizlab.businesscentral.centralapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[MultiPickDataDTO]
 */
@Data
public class MultiPickDataDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [PICKDATAINFO]
     *
     */
    @JSONField(name = "pickdatainfo")
    @JsonProperty("pickdatainfo")
    private String pickdatainfo;

    /**
     * 属性 [USERDATA]
     *
     */
    @JSONField(name = "userdata")
    @JsonProperty("userdata")
    private String userdata;

    /**
     * 属性 [PICKDATANAME]
     *
     */
    @JSONField(name = "pickdataname")
    @JsonProperty("pickdataname")
    private String pickdataname;

    /**
     * 属性 [PICKDATATYPE]
     *
     */
    @JSONField(name = "pickdatatype")
    @JsonProperty("pickdatatype")
    private String pickdatatype;

    /**
     * 属性 [USERDATE2]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "userdate2" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("userdate2")
    private Timestamp userdate2;

    /**
     * 属性 [USERDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "userdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("userdate")
    private Timestamp userdate;

    /**
     * 属性 [PICKDATAID]
     *
     */
    @JSONField(name = "pickdataid")
    @JsonProperty("pickdataid")
    private String pickdataid;

    /**
     * 属性 [USERDATA4]
     *
     */
    @JSONField(name = "userdata4")
    @JsonProperty("userdata4")
    private String userdata4;

    /**
     * 属性 [USERDATA2]
     *
     */
    @JSONField(name = "userdata2")
    @JsonProperty("userdata2")
    private String userdata2;

    /**
     * 属性 [USERDATA3]
     *
     */
    @JSONField(name = "userdata3")
    @JsonProperty("userdata3")
    private String userdata3;


    /**
     * 设置 [PICKDATAINFO]
     */
    public void setPickdatainfo(String  pickdatainfo){
        this.pickdatainfo = pickdatainfo ;
        this.modify("pickdatainfo",pickdatainfo);
    }

    /**
     * 设置 [USERDATA]
     */
    public void setUserdata(String  userdata){
        this.userdata = userdata ;
        this.modify("userdata",userdata);
    }

    /**
     * 设置 [PICKDATANAME]
     */
    public void setPickdataname(String  pickdataname){
        this.pickdataname = pickdataname ;
        this.modify("pickdataname",pickdataname);
    }

    /**
     * 设置 [PICKDATATYPE]
     */
    public void setPickdatatype(String  pickdatatype){
        this.pickdatatype = pickdatatype ;
        this.modify("pickdatatype",pickdatatype);
    }

    /**
     * 设置 [USERDATE2]
     */
    public void setUserdate2(Timestamp  userdate2){
        this.userdate2 = userdate2 ;
        this.modify("userdate2",userdate2);
    }

    /**
     * 设置 [USERDATE]
     */
    public void setUserdate(Timestamp  userdate){
        this.userdate = userdate ;
        this.modify("userdate",userdate);
    }

    /**
     * 设置 [USERDATA4]
     */
    public void setUserdata4(String  userdata4){
        this.userdata4 = userdata4 ;
        this.modify("userdata4",userdata4);
    }

    /**
     * 设置 [USERDATA2]
     */
    public void setUserdata2(String  userdata2){
        this.userdata2 = userdata2 ;
        this.modify("userdata2",userdata2);
    }

    /**
     * 设置 [USERDATA3]
     */
    public void setUserdata3(String  userdata3){
        this.userdata3 = userdata3 ;
        this.modify("userdata3",userdata3);
    }


}

