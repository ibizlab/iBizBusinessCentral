package cn.ibizlab.businesscentral.centralapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[InvoiceDetailDTO]
 */
@Data
public class InvoiceDetailDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [INVOICEDETAILNAME]
     *
     */
    @JSONField(name = "invoicedetailname")
    @JsonProperty("invoicedetailname")
    private String invoicedetailname;

    /**
     * 属性 [QUANTITYBACKORDERED]
     *
     */
    @JSONField(name = "quantitybackordered")
    @JsonProperty("quantitybackordered")
    private BigDecimal quantitybackordered;

    /**
     * 属性 [PRODUCTTYPECODE]
     *
     */
    @JSONField(name = "producttypecode")
    @JsonProperty("producttypecode")
    private String producttypecode;

    /**
     * 属性 [INVOICESTATECODE]
     *
     */
    @JSONField(name = "invoicestatecode")
    @JsonProperty("invoicestatecode")
    private String invoicestatecode;

    /**
     * 属性 [TIMEZONERULEVERSIONNUMBER]
     *
     */
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;

    /**
     * 属性 [SHIPTO_STATEORPROVINCE]
     *
     */
    @JSONField(name = "shipto_stateorprovince")
    @JsonProperty("shipto_stateorprovince")
    private String shiptoStateorprovince;

    /**
     * 属性 [BASEAMOUNT_BASE]
     *
     */
    @JSONField(name = "baseamount_base")
    @JsonProperty("baseamount_base")
    private BigDecimal baseamountBase;

    /**
     * 属性 [WILLCALL]
     *
     */
    @JSONField(name = "willcall")
    @JsonProperty("willcall")
    private Integer willcall;

    /**
     * 属性 [OVERRIDDENCREATEDON]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;

    /**
     * 属性 [PARENTBUNDLEID]
     *
     */
    @JSONField(name = "parentbundleid")
    @JsonProperty("parentbundleid")
    private String parentbundleid;

    /**
     * 属性 [IMPORTSEQUENCENUMBER]
     *
     */
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;

    /**
     * 属性 [PROPERTYCONFIGURATIONSTATUS]
     *
     */
    @JSONField(name = "propertyconfigurationstatus")
    @JsonProperty("propertyconfigurationstatus")
    private String propertyconfigurationstatus;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;

    /**
     * 属性 [PRICEPERUNIT_BASE]
     *
     */
    @JSONField(name = "priceperunit_base")
    @JsonProperty("priceperunit_base")
    private BigDecimal priceperunitBase;

    /**
     * 属性 [SHIPTO_COUNTRY]
     *
     */
    @JSONField(name = "shipto_country")
    @JsonProperty("shipto_country")
    private String shiptoCountry;

    /**
     * 属性 [COPIED]
     *
     */
    @JSONField(name = "copied")
    @JsonProperty("copied")
    private Integer copied;

    /**
     * 属性 [OWNERTYPE]
     *
     */
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;

    /**
     * 属性 [LINEITEMNUMBER]
     *
     */
    @JSONField(name = "lineitemnumber")
    @JsonProperty("lineitemnumber")
    private Integer lineitemnumber;

    /**
     * 属性 [PRICEPERUNIT]
     *
     */
    @JSONField(name = "priceperunit")
    @JsonProperty("priceperunit")
    private BigDecimal priceperunit;

    /**
     * 属性 [SALESREPNAME]
     *
     */
    @JSONField(name = "salesrepname")
    @JsonProperty("salesrepname")
    private String salesrepname;

    /**
     * 属性 [VOLUMEDISCOUNTAMOUNT_BASE]
     *
     */
    @JSONField(name = "volumediscountamount_base")
    @JsonProperty("volumediscountamount_base")
    private BigDecimal volumediscountamountBase;

    /**
     * 属性 [SHIPTO_FAX]
     *
     */
    @JSONField(name = "shipto_fax")
    @JsonProperty("shipto_fax")
    private String shiptoFax;

    /**
     * 属性 [SHIPTO_LINE1]
     *
     */
    @JSONField(name = "shipto_line1")
    @JsonProperty("shipto_line1")
    private String shiptoLine1;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 属性 [SHIPTO_FREIGHTTERMSCODE]
     *
     */
    @JSONField(name = "shipto_freighttermscode")
    @JsonProperty("shipto_freighttermscode")
    private String shiptoFreighttermscode;

    /**
     * 属性 [OWNERID]
     *
     */
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;

    /**
     * 属性 [TAX_BASE]
     *
     */
    @JSONField(name = "tax_base")
    @JsonProperty("tax_base")
    private BigDecimal taxBase;

    /**
     * 属性 [SHIPTO_TELEPHONE]
     *
     */
    @JSONField(name = "shipto_telephone")
    @JsonProperty("shipto_telephone")
    private String shiptoTelephone;

    /**
     * 属性 [PRICEOVERRIDDEN]
     *
     */
    @JSONField(name = "priceoverridden")
    @JsonProperty("priceoverridden")
    private Integer priceoverridden;

    /**
     * 属性 [ACTUALDELIVERYON]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "actualdeliveryon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("actualdeliveryon")
    private Timestamp actualdeliveryon;

    /**
     * 属性 [SHIPTO_NAME]
     *
     */
    @JSONField(name = "shipto_name")
    @JsonProperty("shipto_name")
    private String shiptoName;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;

    /**
     * 属性 [BASEAMOUNT]
     *
     */
    @JSONField(name = "baseamount")
    @JsonProperty("baseamount")
    private BigDecimal baseamount;

    /**
     * 属性 [SHIPPINGTRACKINGNUMBER]
     *
     */
    @JSONField(name = "shippingtrackingnumber")
    @JsonProperty("shippingtrackingnumber")
    private String shippingtrackingnumber;

    /**
     * 属性 [INVOICEDETAILID]
     *
     */
    @JSONField(name = "invoicedetailid")
    @JsonProperty("invoicedetailid")
    private String invoicedetailid;

    /**
     * 属性 [OWNERNAME]
     *
     */
    @JSONField(name = "ownername")
    @JsonProperty("ownername")
    private String ownername;

    /**
     * 属性 [EXCHANGERATE]
     *
     */
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;

    /**
     * 属性 [SHIPTO_LINE3]
     *
     */
    @JSONField(name = "shipto_line3")
    @JsonProperty("shipto_line3")
    private String shiptoLine3;

    /**
     * 属性 [PRODUCTASSOCIATIONID]
     *
     */
    @JSONField(name = "productassociationid")
    @JsonProperty("productassociationid")
    private String productassociationid;

    /**
     * 属性 [SHIPTO_POSTALCODE]
     *
     */
    @JSONField(name = "shipto_postalcode")
    @JsonProperty("shipto_postalcode")
    private String shiptoPostalcode;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [PRODUCTOVERRIDDEN]
     *
     */
    @JSONField(name = "productoverridden")
    @JsonProperty("productoverridden")
    private Integer productoverridden;

    /**
     * 属性 [SALESREPID]
     *
     */
    @JSONField(name = "salesrepid")
    @JsonProperty("salesrepid")
    private String salesrepid;

    /**
     * 属性 [UTCCONVERSIONTIMEZONECODE]
     *
     */
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;

    /**
     * 属性 [EXTENDEDAMOUNT]
     *
     */
    @JSONField(name = "extendedamount")
    @JsonProperty("extendedamount")
    private BigDecimal extendedamount;

    /**
     * 属性 [PRODUCTDESCRIPTION]
     *
     */
    @JSONField(name = "productdescription")
    @JsonProperty("productdescription")
    private String productdescription;

    /**
     * 属性 [SHIPTO_LINE2]
     *
     */
    @JSONField(name = "shipto_line2")
    @JsonProperty("shipto_line2")
    private String shiptoLine2;

    /**
     * 属性 [MANUALDISCOUNTAMOUNT]
     *
     */
    @JSONField(name = "manualdiscountamount")
    @JsonProperty("manualdiscountamount")
    private BigDecimal manualdiscountamount;

    /**
     * 属性 [SEQUENCENUMBER]
     *
     */
    @JSONField(name = "sequencenumber")
    @JsonProperty("sequencenumber")
    private Integer sequencenumber;

    /**
     * 属性 [QUANTITYSHIPPED]
     *
     */
    @JSONField(name = "quantityshipped")
    @JsonProperty("quantityshipped")
    private BigDecimal quantityshipped;

    /**
     * 属性 [QUANTITYCANCELLED]
     *
     */
    @JSONField(name = "quantitycancelled")
    @JsonProperty("quantitycancelled")
    private BigDecimal quantitycancelled;

    /**
     * 属性 [PRICINGERRORCODE]
     *
     */
    @JSONField(name = "pricingerrorcode")
    @JsonProperty("pricingerrorcode")
    private String pricingerrorcode;

    /**
     * 属性 [VERSIONNUMBER]
     *
     */
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;

    /**
     * 属性 [SKIPPRICECALCULATION]
     *
     */
    @JSONField(name = "skippricecalculation")
    @JsonProperty("skippricecalculation")
    private String skippricecalculation;

    /**
     * 属性 [QUANTITY]
     *
     */
    @JSONField(name = "quantity")
    @JsonProperty("quantity")
    private BigDecimal quantity;

    /**
     * 属性 [TAX]
     *
     */
    @JSONField(name = "tax")
    @JsonProperty("tax")
    private BigDecimal tax;

    /**
     * 属性 [EXTENDEDAMOUNT_BASE]
     *
     */
    @JSONField(name = "extendedamount_base")
    @JsonProperty("extendedamount_base")
    private BigDecimal extendedamountBase;

    /**
     * 属性 [VOLUMEDISCOUNTAMOUNT]
     *
     */
    @JSONField(name = "volumediscountamount")
    @JsonProperty("volumediscountamount")
    private BigDecimal volumediscountamount;

    /**
     * 属性 [SHIPTO_CITY]
     *
     */
    @JSONField(name = "shipto_city")
    @JsonProperty("shipto_city")
    private String shiptoCity;

    /**
     * 属性 [INVOICEISPRICELOCKED]
     *
     */
    @JSONField(name = "invoiceispricelocked")
    @JsonProperty("invoiceispricelocked")
    private Integer invoiceispricelocked;

    /**
     * 属性 [PRODUCTNAME]
     *
     */
    @JSONField(name = "productname")
    @JsonProperty("productname")
    private String productname;

    /**
     * 属性 [UOMNAME]
     *
     */
    @JSONField(name = "uomname")
    @JsonProperty("uomname")
    private String uomname;

    /**
     * 属性 [TRANSACTIONCURRENCYID]
     *
     */
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;

    /**
     * 属性 [SALESORDERDETAILID]
     *
     */
    @JSONField(name = "salesorderdetailid")
    @JsonProperty("salesorderdetailid")
    private String salesorderdetailid;

    /**
     * 属性 [PARENTBUNDLEIDREF]
     *
     */
    @JSONField(name = "parentbundleidref")
    @JsonProperty("parentbundleidref")
    private String parentbundleidref;

    /**
     * 属性 [PRODUCTID]
     *
     */
    @JSONField(name = "productid")
    @JsonProperty("productid")
    private String productid;

    /**
     * 属性 [INVOICEID]
     *
     */
    @JSONField(name = "invoiceid")
    @JsonProperty("invoiceid")
    private String invoiceid;

    /**
     * 属性 [UOMID]
     *
     */
    @JSONField(name = "uomid")
    @JsonProperty("uomid")
    private String uomid;


    /**
     * 设置 [INVOICEDETAILNAME]
     */
    public void setInvoicedetailname(String  invoicedetailname){
        this.invoicedetailname = invoicedetailname ;
        this.modify("invoicedetailname",invoicedetailname);
    }

    /**
     * 设置 [QUANTITYBACKORDERED]
     */
    public void setQuantitybackordered(BigDecimal  quantitybackordered){
        this.quantitybackordered = quantitybackordered ;
        this.modify("quantitybackordered",quantitybackordered);
    }

    /**
     * 设置 [PRODUCTTYPECODE]
     */
    public void setProducttypecode(String  producttypecode){
        this.producttypecode = producttypecode ;
        this.modify("producttypecode",producttypecode);
    }

    /**
     * 设置 [INVOICESTATECODE]
     */
    public void setInvoicestatecode(String  invoicestatecode){
        this.invoicestatecode = invoicestatecode ;
        this.modify("invoicestatecode",invoicestatecode);
    }

    /**
     * 设置 [TIMEZONERULEVERSIONNUMBER]
     */
    public void setTimezoneruleversionnumber(Integer  timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [SHIPTO_STATEORPROVINCE]
     */
    public void setShiptoStateorprovince(String  shiptoStateorprovince){
        this.shiptoStateorprovince = shiptoStateorprovince ;
        this.modify("shipto_stateorprovince",shiptoStateorprovince);
    }

    /**
     * 设置 [BASEAMOUNT_BASE]
     */
    public void setBaseamountBase(BigDecimal  baseamountBase){
        this.baseamountBase = baseamountBase ;
        this.modify("baseamount_base",baseamountBase);
    }

    /**
     * 设置 [WILLCALL]
     */
    public void setWillcall(Integer  willcall){
        this.willcall = willcall ;
        this.modify("willcall",willcall);
    }

    /**
     * 设置 [OVERRIDDENCREATEDON]
     */
    public void setOverriddencreatedon(Timestamp  overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 设置 [PARENTBUNDLEID]
     */
    public void setParentbundleid(String  parentbundleid){
        this.parentbundleid = parentbundleid ;
        this.modify("parentbundleid",parentbundleid);
    }

    /**
     * 设置 [IMPORTSEQUENCENUMBER]
     */
    public void setImportsequencenumber(Integer  importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [PROPERTYCONFIGURATIONSTATUS]
     */
    public void setPropertyconfigurationstatus(String  propertyconfigurationstatus){
        this.propertyconfigurationstatus = propertyconfigurationstatus ;
        this.modify("propertyconfigurationstatus",propertyconfigurationstatus);
    }

    /**
     * 设置 [PRICEPERUNIT_BASE]
     */
    public void setPriceperunitBase(BigDecimal  priceperunitBase){
        this.priceperunitBase = priceperunitBase ;
        this.modify("priceperunit_base",priceperunitBase);
    }

    /**
     * 设置 [SHIPTO_COUNTRY]
     */
    public void setShiptoCountry(String  shiptoCountry){
        this.shiptoCountry = shiptoCountry ;
        this.modify("shipto_country",shiptoCountry);
    }

    /**
     * 设置 [COPIED]
     */
    public void setCopied(Integer  copied){
        this.copied = copied ;
        this.modify("copied",copied);
    }

    /**
     * 设置 [OWNERTYPE]
     */
    public void setOwnertype(String  ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [LINEITEMNUMBER]
     */
    public void setLineitemnumber(Integer  lineitemnumber){
        this.lineitemnumber = lineitemnumber ;
        this.modify("lineitemnumber",lineitemnumber);
    }

    /**
     * 设置 [PRICEPERUNIT]
     */
    public void setPriceperunit(BigDecimal  priceperunit){
        this.priceperunit = priceperunit ;
        this.modify("priceperunit",priceperunit);
    }

    /**
     * 设置 [SALESREPNAME]
     */
    public void setSalesrepname(String  salesrepname){
        this.salesrepname = salesrepname ;
        this.modify("salesrepname",salesrepname);
    }

    /**
     * 设置 [VOLUMEDISCOUNTAMOUNT_BASE]
     */
    public void setVolumediscountamountBase(BigDecimal  volumediscountamountBase){
        this.volumediscountamountBase = volumediscountamountBase ;
        this.modify("volumediscountamount_base",volumediscountamountBase);
    }

    /**
     * 设置 [SHIPTO_FAX]
     */
    public void setShiptoFax(String  shiptoFax){
        this.shiptoFax = shiptoFax ;
        this.modify("shipto_fax",shiptoFax);
    }

    /**
     * 设置 [SHIPTO_LINE1]
     */
    public void setShiptoLine1(String  shiptoLine1){
        this.shiptoLine1 = shiptoLine1 ;
        this.modify("shipto_line1",shiptoLine1);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [SHIPTO_FREIGHTTERMSCODE]
     */
    public void setShiptoFreighttermscode(String  shiptoFreighttermscode){
        this.shiptoFreighttermscode = shiptoFreighttermscode ;
        this.modify("shipto_freighttermscode",shiptoFreighttermscode);
    }

    /**
     * 设置 [OWNERID]
     */
    public void setOwnerid(String  ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [TAX_BASE]
     */
    public void setTaxBase(BigDecimal  taxBase){
        this.taxBase = taxBase ;
        this.modify("tax_base",taxBase);
    }

    /**
     * 设置 [SHIPTO_TELEPHONE]
     */
    public void setShiptoTelephone(String  shiptoTelephone){
        this.shiptoTelephone = shiptoTelephone ;
        this.modify("shipto_telephone",shiptoTelephone);
    }

    /**
     * 设置 [PRICEOVERRIDDEN]
     */
    public void setPriceoverridden(Integer  priceoverridden){
        this.priceoverridden = priceoverridden ;
        this.modify("priceoverridden",priceoverridden);
    }

    /**
     * 设置 [ACTUALDELIVERYON]
     */
    public void setActualdeliveryon(Timestamp  actualdeliveryon){
        this.actualdeliveryon = actualdeliveryon ;
        this.modify("actualdeliveryon",actualdeliveryon);
    }

    /**
     * 设置 [SHIPTO_NAME]
     */
    public void setShiptoName(String  shiptoName){
        this.shiptoName = shiptoName ;
        this.modify("shipto_name",shiptoName);
    }

    /**
     * 设置 [BASEAMOUNT]
     */
    public void setBaseamount(BigDecimal  baseamount){
        this.baseamount = baseamount ;
        this.modify("baseamount",baseamount);
    }

    /**
     * 设置 [SHIPPINGTRACKINGNUMBER]
     */
    public void setShippingtrackingnumber(String  shippingtrackingnumber){
        this.shippingtrackingnumber = shippingtrackingnumber ;
        this.modify("shippingtrackingnumber",shippingtrackingnumber);
    }

    /**
     * 设置 [OWNERNAME]
     */
    public void setOwnername(String  ownername){
        this.ownername = ownername ;
        this.modify("ownername",ownername);
    }

    /**
     * 设置 [EXCHANGERATE]
     */
    public void setExchangerate(BigDecimal  exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [SHIPTO_LINE3]
     */
    public void setShiptoLine3(String  shiptoLine3){
        this.shiptoLine3 = shiptoLine3 ;
        this.modify("shipto_line3",shiptoLine3);
    }

    /**
     * 设置 [PRODUCTASSOCIATIONID]
     */
    public void setProductassociationid(String  productassociationid){
        this.productassociationid = productassociationid ;
        this.modify("productassociationid",productassociationid);
    }

    /**
     * 设置 [SHIPTO_POSTALCODE]
     */
    public void setShiptoPostalcode(String  shiptoPostalcode){
        this.shiptoPostalcode = shiptoPostalcode ;
        this.modify("shipto_postalcode",shiptoPostalcode);
    }

    /**
     * 设置 [PRODUCTOVERRIDDEN]
     */
    public void setProductoverridden(Integer  productoverridden){
        this.productoverridden = productoverridden ;
        this.modify("productoverridden",productoverridden);
    }

    /**
     * 设置 [SALESREPID]
     */
    public void setSalesrepid(String  salesrepid){
        this.salesrepid = salesrepid ;
        this.modify("salesrepid",salesrepid);
    }

    /**
     * 设置 [UTCCONVERSIONTIMEZONECODE]
     */
    public void setUtcconversiontimezonecode(Integer  utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [EXTENDEDAMOUNT]
     */
    public void setExtendedamount(BigDecimal  extendedamount){
        this.extendedamount = extendedamount ;
        this.modify("extendedamount",extendedamount);
    }

    /**
     * 设置 [PRODUCTDESCRIPTION]
     */
    public void setProductdescription(String  productdescription){
        this.productdescription = productdescription ;
        this.modify("productdescription",productdescription);
    }

    /**
     * 设置 [SHIPTO_LINE2]
     */
    public void setShiptoLine2(String  shiptoLine2){
        this.shiptoLine2 = shiptoLine2 ;
        this.modify("shipto_line2",shiptoLine2);
    }

    /**
     * 设置 [MANUALDISCOUNTAMOUNT]
     */
    public void setManualdiscountamount(BigDecimal  manualdiscountamount){
        this.manualdiscountamount = manualdiscountamount ;
        this.modify("manualdiscountamount",manualdiscountamount);
    }

    /**
     * 设置 [SEQUENCENUMBER]
     */
    public void setSequencenumber(Integer  sequencenumber){
        this.sequencenumber = sequencenumber ;
        this.modify("sequencenumber",sequencenumber);
    }

    /**
     * 设置 [QUANTITYSHIPPED]
     */
    public void setQuantityshipped(BigDecimal  quantityshipped){
        this.quantityshipped = quantityshipped ;
        this.modify("quantityshipped",quantityshipped);
    }

    /**
     * 设置 [QUANTITYCANCELLED]
     */
    public void setQuantitycancelled(BigDecimal  quantitycancelled){
        this.quantitycancelled = quantitycancelled ;
        this.modify("quantitycancelled",quantitycancelled);
    }

    /**
     * 设置 [PRICINGERRORCODE]
     */
    public void setPricingerrorcode(String  pricingerrorcode){
        this.pricingerrorcode = pricingerrorcode ;
        this.modify("pricingerrorcode",pricingerrorcode);
    }

    /**
     * 设置 [VERSIONNUMBER]
     */
    public void setVersionnumber(BigInteger  versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [SKIPPRICECALCULATION]
     */
    public void setSkippricecalculation(String  skippricecalculation){
        this.skippricecalculation = skippricecalculation ;
        this.modify("skippricecalculation",skippricecalculation);
    }

    /**
     * 设置 [QUANTITY]
     */
    public void setQuantity(BigDecimal  quantity){
        this.quantity = quantity ;
        this.modify("quantity",quantity);
    }

    /**
     * 设置 [TAX]
     */
    public void setTax(BigDecimal  tax){
        this.tax = tax ;
        this.modify("tax",tax);
    }

    /**
     * 设置 [EXTENDEDAMOUNT_BASE]
     */
    public void setExtendedamountBase(BigDecimal  extendedamountBase){
        this.extendedamountBase = extendedamountBase ;
        this.modify("extendedamount_base",extendedamountBase);
    }

    /**
     * 设置 [VOLUMEDISCOUNTAMOUNT]
     */
    public void setVolumediscountamount(BigDecimal  volumediscountamount){
        this.volumediscountamount = volumediscountamount ;
        this.modify("volumediscountamount",volumediscountamount);
    }

    /**
     * 设置 [SHIPTO_CITY]
     */
    public void setShiptoCity(String  shiptoCity){
        this.shiptoCity = shiptoCity ;
        this.modify("shipto_city",shiptoCity);
    }

    /**
     * 设置 [INVOICEISPRICELOCKED]
     */
    public void setInvoiceispricelocked(Integer  invoiceispricelocked){
        this.invoiceispricelocked = invoiceispricelocked ;
        this.modify("invoiceispricelocked",invoiceispricelocked);
    }

    /**
     * 设置 [PRODUCTNAME]
     */
    public void setProductname(String  productname){
        this.productname = productname ;
        this.modify("productname",productname);
    }

    /**
     * 设置 [UOMNAME]
     */
    public void setUomname(String  uomname){
        this.uomname = uomname ;
        this.modify("uomname",uomname);
    }

    /**
     * 设置 [TRANSACTIONCURRENCYID]
     */
    public void setTransactioncurrencyid(String  transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }

    /**
     * 设置 [SALESORDERDETAILID]
     */
    public void setSalesorderdetailid(String  salesorderdetailid){
        this.salesorderdetailid = salesorderdetailid ;
        this.modify("salesorderdetailid",salesorderdetailid);
    }

    /**
     * 设置 [PARENTBUNDLEIDREF]
     */
    public void setParentbundleidref(String  parentbundleidref){
        this.parentbundleidref = parentbundleidref ;
        this.modify("parentbundleidref",parentbundleidref);
    }

    /**
     * 设置 [PRODUCTID]
     */
    public void setProductid(String  productid){
        this.productid = productid ;
        this.modify("productid",productid);
    }

    /**
     * 设置 [INVOICEID]
     */
    public void setInvoiceid(String  invoiceid){
        this.invoiceid = invoiceid ;
        this.modify("invoiceid",invoiceid);
    }

    /**
     * 设置 [UOMID]
     */
    public void setUomid(String  uomid){
        this.uomid = uomid ;
        this.modify("uomid",uomid);
    }


}

