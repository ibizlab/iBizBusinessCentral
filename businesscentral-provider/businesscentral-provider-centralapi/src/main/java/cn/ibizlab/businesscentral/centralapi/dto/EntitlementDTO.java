package cn.ibizlab.businesscentral.centralapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EntitlementDTO]
 */
@Data
public class EntitlementDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [STARTDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "startdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("startdate")
    private Timestamp startdate;

    /**
     * 属性 [PROCESSID]
     *
     */
    @JSONField(name = "processid")
    @JsonProperty("processid")
    private String processid;

    /**
     * 属性 [TRAVERSEDPATH]
     *
     */
    @JSONField(name = "traversedpath")
    @JsonProperty("traversedpath")
    private String traversedpath;

    /**
     * 属性 [DECREASEREMAININGON]
     *
     */
    @JSONField(name = "decreaseremainingon")
    @JsonProperty("decreaseremainingon")
    private String decreaseremainingon;

    /**
     * 属性 [ENTITYTYPE]
     *
     */
    @JSONField(name = "entitytype")
    @JsonProperty("entitytype")
    private String entitytype;

    /**
     * 属性 [CONTACTNAME]
     *
     */
    @JSONField(name = "contactname")
    @JsonProperty("contactname")
    private String contactname;

    /**
     * 属性 [IMPORTSEQUENCENUMBER]
     *
     */
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;

    /**
     * 属性 [ENDDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "enddate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("enddate")
    private Timestamp enddate;

    /**
     * 属性 [STATECODE]
     *
     */
    @JSONField(name = "statecode")
    @JsonProperty("statecode")
    private Integer statecode;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;

    /**
     * 属性 [CUSTOMERNAME]
     *
     */
    @JSONField(name = "customername")
    @JsonProperty("customername")
    private String customername;

    /**
     * 属性 [KBACCESSLEVEL]
     *
     */
    @JSONField(name = "kbaccesslevel")
    @JsonProperty("kbaccesslevel")
    private String kbaccesslevel;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 属性 [EMAILADDRESS]
     *
     */
    @JSONField(name = "emailaddress")
    @JsonProperty("emailaddress")
    private String emailaddress;

    /**
     * 属性 [TIMEZONERULEVERSIONNUMBER]
     *
     */
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;

    /**
     * 属性 [EXCHANGERATE]
     *
     */
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;

    /**
     * 属性 [CUSTOMERTYPE]
     *
     */
    @JSONField(name = "customertype")
    @JsonProperty("customertype")
    private String customertype;

    /**
     * 属性 [VERSIONNUMBER]
     *
     */
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;

    /**
     * 属性 [OWNERID]
     *
     */
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;

    /**
     * 属性 [UTCCONVERSIONTIMEZONECODE]
     *
     */
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [ALLOCATIONTYPECODE]
     *
     */
    @JSONField(name = "allocationtypecode")
    @JsonProperty("allocationtypecode")
    private String allocationtypecode;

    /**
     * 属性 [STATUSCODE]
     *
     */
    @JSONField(name = "statuscode")
    @JsonProperty("statuscode")
    private Integer statuscode;

    /**
     * 属性 [TOTALTERMS]
     *
     */
    @JSONField(name = "totalterms")
    @JsonProperty("totalterms")
    private BigDecimal totalterms;

    /**
     * 属性 [CUSTOMERID]
     *
     */
    @JSONField(name = "customerid")
    @JsonProperty("customerid")
    private String customerid;

    /**
     * 属性 [OWNERNAME]
     *
     */
    @JSONField(name = "ownername")
    @JsonProperty("ownername")
    private String ownername;

    /**
     * 属性 [RESTRICTCASECREATION]
     *
     */
    @JSONField(name = "restrictcasecreation")
    @JsonProperty("restrictcasecreation")
    private Integer restrictcasecreation;

    /**
     * 属性 [ENTITLEMENTID]
     *
     */
    @JSONField(name = "entitlementid")
    @JsonProperty("entitlementid")
    private String entitlementid;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;

    /**
     * 属性 [REMAININGTERMS]
     *
     */
    @JSONField(name = "remainingterms")
    @JsonProperty("remainingterms")
    private BigDecimal remainingterms;

    /**
     * 属性 [ACCOUNTNAME]
     *
     */
    @JSONField(name = "accountname")
    @JsonProperty("accountname")
    private String accountname;

    /**
     * 属性 [OVERRIDDENCREATEDON]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;

    /**
     * 属性 [OWNERTYPE]
     *
     */
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;

    /**
     * 属性 [STAGEID]
     *
     */
    @JSONField(name = "stageid")
    @JsonProperty("stageid")
    private String stageid;

    /**
     * 属性 [ENTITLEMENTNAME]
     *
     */
    @JSONField(name = "entitlementname")
    @JsonProperty("entitlementname")
    private String entitlementname;

    /**
     * 属性 [DEFAULT]
     *
     */
    @JSONField(name = "ibizdefault")
    @JsonProperty("ibizdefault")
    private Integer ibizdefault;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [TRANSACTIONCURRENCYID]
     *
     */
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;

    /**
     * 属性 [SLAID]
     *
     */
    @JSONField(name = "slaid")
    @JsonProperty("slaid")
    private String slaid;

    /**
     * 属性 [ENTITLEMENTTEMPLATEID]
     *
     */
    @JSONField(name = "entitlementtemplateid")
    @JsonProperty("entitlementtemplateid")
    private String entitlementtemplateid;


    /**
     * 设置 [STARTDATE]
     */
    public void setStartdate(Timestamp  startdate){
        this.startdate = startdate ;
        this.modify("startdate",startdate);
    }

    /**
     * 设置 [PROCESSID]
     */
    public void setProcessid(String  processid){
        this.processid = processid ;
        this.modify("processid",processid);
    }

    /**
     * 设置 [TRAVERSEDPATH]
     */
    public void setTraversedpath(String  traversedpath){
        this.traversedpath = traversedpath ;
        this.modify("traversedpath",traversedpath);
    }

    /**
     * 设置 [DECREASEREMAININGON]
     */
    public void setDecreaseremainingon(String  decreaseremainingon){
        this.decreaseremainingon = decreaseremainingon ;
        this.modify("decreaseremainingon",decreaseremainingon);
    }

    /**
     * 设置 [ENTITYTYPE]
     */
    public void setEntitytype(String  entitytype){
        this.entitytype = entitytype ;
        this.modify("entitytype",entitytype);
    }

    /**
     * 设置 [CONTACTNAME]
     */
    public void setContactname(String  contactname){
        this.contactname = contactname ;
        this.modify("contactname",contactname);
    }

    /**
     * 设置 [IMPORTSEQUENCENUMBER]
     */
    public void setImportsequencenumber(Integer  importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [ENDDATE]
     */
    public void setEnddate(Timestamp  enddate){
        this.enddate = enddate ;
        this.modify("enddate",enddate);
    }

    /**
     * 设置 [STATECODE]
     */
    public void setStatecode(Integer  statecode){
        this.statecode = statecode ;
        this.modify("statecode",statecode);
    }

    /**
     * 设置 [CUSTOMERNAME]
     */
    public void setCustomername(String  customername){
        this.customername = customername ;
        this.modify("customername",customername);
    }

    /**
     * 设置 [KBACCESSLEVEL]
     */
    public void setKbaccesslevel(String  kbaccesslevel){
        this.kbaccesslevel = kbaccesslevel ;
        this.modify("kbaccesslevel",kbaccesslevel);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [EMAILADDRESS]
     */
    public void setEmailaddress(String  emailaddress){
        this.emailaddress = emailaddress ;
        this.modify("emailaddress",emailaddress);
    }

    /**
     * 设置 [TIMEZONERULEVERSIONNUMBER]
     */
    public void setTimezoneruleversionnumber(Integer  timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [EXCHANGERATE]
     */
    public void setExchangerate(BigDecimal  exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [CUSTOMERTYPE]
     */
    public void setCustomertype(String  customertype){
        this.customertype = customertype ;
        this.modify("customertype",customertype);
    }

    /**
     * 设置 [VERSIONNUMBER]
     */
    public void setVersionnumber(BigInteger  versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [OWNERID]
     */
    public void setOwnerid(String  ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [UTCCONVERSIONTIMEZONECODE]
     */
    public void setUtcconversiontimezonecode(Integer  utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [ALLOCATIONTYPECODE]
     */
    public void setAllocationtypecode(String  allocationtypecode){
        this.allocationtypecode = allocationtypecode ;
        this.modify("allocationtypecode",allocationtypecode);
    }

    /**
     * 设置 [STATUSCODE]
     */
    public void setStatuscode(Integer  statuscode){
        this.statuscode = statuscode ;
        this.modify("statuscode",statuscode);
    }

    /**
     * 设置 [TOTALTERMS]
     */
    public void setTotalterms(BigDecimal  totalterms){
        this.totalterms = totalterms ;
        this.modify("totalterms",totalterms);
    }

    /**
     * 设置 [CUSTOMERID]
     */
    public void setCustomerid(String  customerid){
        this.customerid = customerid ;
        this.modify("customerid",customerid);
    }

    /**
     * 设置 [OWNERNAME]
     */
    public void setOwnername(String  ownername){
        this.ownername = ownername ;
        this.modify("ownername",ownername);
    }

    /**
     * 设置 [RESTRICTCASECREATION]
     */
    public void setRestrictcasecreation(Integer  restrictcasecreation){
        this.restrictcasecreation = restrictcasecreation ;
        this.modify("restrictcasecreation",restrictcasecreation);
    }

    /**
     * 设置 [REMAININGTERMS]
     */
    public void setRemainingterms(BigDecimal  remainingterms){
        this.remainingterms = remainingterms ;
        this.modify("remainingterms",remainingterms);
    }

    /**
     * 设置 [ACCOUNTNAME]
     */
    public void setAccountname(String  accountname){
        this.accountname = accountname ;
        this.modify("accountname",accountname);
    }

    /**
     * 设置 [OVERRIDDENCREATEDON]
     */
    public void setOverriddencreatedon(Timestamp  overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 设置 [OWNERTYPE]
     */
    public void setOwnertype(String  ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [STAGEID]
     */
    public void setStageid(String  stageid){
        this.stageid = stageid ;
        this.modify("stageid",stageid);
    }

    /**
     * 设置 [ENTITLEMENTNAME]
     */
    public void setEntitlementname(String  entitlementname){
        this.entitlementname = entitlementname ;
        this.modify("entitlementname",entitlementname);
    }

    /**
     * 设置 [DEFAULT]
     */
    public void setIbizdefault(Integer  ibizdefault){
        this.ibizdefault = ibizdefault ;
        this.modify("default",ibizdefault);
    }

    /**
     * 设置 [TRANSACTIONCURRENCYID]
     */
    public void setTransactioncurrencyid(String  transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }

    /**
     * 设置 [SLAID]
     */
    public void setSlaid(String  slaid){
        this.slaid = slaid ;
        this.modify("slaid",slaid);
    }

    /**
     * 设置 [ENTITLEMENTTEMPLATEID]
     */
    public void setEntitlementtemplateid(String  entitlementtemplateid){
        this.entitlementtemplateid = entitlementtemplateid ;
        this.modify("entitlementtemplateid",entitlementtemplateid);
    }


}

