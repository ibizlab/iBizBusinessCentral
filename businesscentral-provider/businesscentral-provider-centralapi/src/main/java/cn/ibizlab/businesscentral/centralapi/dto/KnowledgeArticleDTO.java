package cn.ibizlab.businesscentral.centralapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[KnowledgeArticleDTO]
 */
@Data
public class KnowledgeArticleDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [KNOWLEDGEARTICLEVIEWS_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "knowledgearticleviews_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("knowledgearticleviews_date")
    private Timestamp knowledgearticleviewsDate;

    /**
     * 属性 [KEYWORDS]
     *
     */
    @JSONField(name = "keywords")
    @JsonProperty("keywords")
    private String keywords;

    /**
     * 属性 [STATUSCODE]
     *
     */
    @JSONField(name = "statuscode")
    @JsonProperty("statuscode")
    private Integer statuscode;

    /**
     * 属性 [EXPIRATIONSTATEID]
     *
     */
    @JSONField(name = "expirationstateid")
    @JsonProperty("expirationstateid")
    private Integer expirationstateid;

    /**
     * 属性 [ISPRIMARY]
     *
     */
    @JSONField(name = "isprimary")
    @JsonProperty("isprimary")
    private Integer isprimary;

    /**
     * 属性 [RATING_COUNT]
     *
     */
    @JSONField(name = "rating_count")
    @JsonProperty("rating_count")
    private Integer ratingCount;

    /**
     * 属性 [CONTENT]
     *
     */
    @JSONField(name = "content")
    @JsonProperty("content")
    private String content;

    /**
     * 属性 [OWNERID]
     *
     */
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;

    /**
     * 属性 [ROOTARTICLE]
     *
     */
    @JSONField(name = "rootarticled")
    @JsonProperty("rootarticled")
    private Integer rootarticled;

    /**
     * 属性 [KNOWLEDGEARTICLEVIEWS_STATE]
     *
     */
    @JSONField(name = "knowledgearticleviews_state")
    @JsonProperty("knowledgearticleviews_state")
    private Integer knowledgearticleviewsState;

    /**
     * 属性 [RATING_SUM]
     *
     */
    @JSONField(name = "rating_sum")
    @JsonProperty("rating_sum")
    private BigDecimal ratingSum;

    /**
     * 属性 [STATECODE]
     *
     */
    @JSONField(name = "statecode")
    @JsonProperty("statecode")
    private Integer statecode;

    /**
     * 属性 [TRAVERSEDPATH]
     *
     */
    @JSONField(name = "traversedpath")
    @JsonProperty("traversedpath")
    private String traversedpath;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;

    /**
     * 属性 [PUBLISHSTATUSID]
     *
     */
    @JSONField(name = "publishstatusid")
    @JsonProperty("publishstatusid")
    private Integer publishstatusid;

    /**
     * 属性 [OWNERTYPE]
     *
     */
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;

    /**
     * 属性 [ARTICLEPUBLICNUMBER]
     *
     */
    @JSONField(name = "articlepublicnumber")
    @JsonProperty("articlepublicnumber")
    private String articlepublicnumber;

    /**
     * 属性 [PRIMARYAUTHORIDNAME]
     *
     */
    @JSONField(name = "primaryauthoridname")
    @JsonProperty("primaryauthoridname")
    private String primaryauthoridname;

    /**
     * 属性 [OVERRIDDENCREATEDON]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;

    /**
     * 属性 [PRIMARYAUTHORID]
     *
     */
    @JSONField(name = "primaryauthorid")
    @JsonProperty("primaryauthorid")
    private String primaryauthorid;

    /**
     * 属性 [LATESTVERSION]
     *
     */
    @JSONField(name = "latestversion")
    @JsonProperty("latestversion")
    private Integer latestversion;

    /**
     * 属性 [RATING_STATE]
     *
     */
    @JSONField(name = "rating_state")
    @JsonProperty("rating_state")
    private Integer ratingState;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [VERSIONNUMBER]
     *
     */
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;

    /**
     * 属性 [UTCCONVERSIONTIMEZONECODE]
     *
     */
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;

    /**
     * 属性 [EXPIRATIONSTATUSID]
     *
     */
    @JSONField(name = "expirationstatusid")
    @JsonProperty("expirationstatusid")
    private Integer expirationstatusid;

    /**
     * 属性 [KNOWLEDGEARTICLEID]
     *
     */
    @JSONField(name = "knowledgearticleid")
    @JsonProperty("knowledgearticleid")
    private String knowledgearticleid;

    /**
     * 属性 [SETCATEGORYASSOCIATIONS]
     *
     */
    @JSONField(name = "setcategoryassociations")
    @JsonProperty("setcategoryassociations")
    private Integer setcategoryassociations;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 属性 [SETPRODUCTASSOCIATIONS]
     *
     */
    @JSONField(name = "setproductassociations")
    @JsonProperty("setproductassociations")
    private Integer setproductassociations;

    /**
     * 属性 [MINORVERSIONNUMBER]
     *
     */
    @JSONField(name = "minorversionnumber")
    @JsonProperty("minorversionnumber")
    private Integer minorversionnumber;

    /**
     * 属性 [STAGEID]
     *
     */
    @JSONField(name = "stageid")
    @JsonProperty("stageid")
    private String stageid;

    /**
     * 属性 [PROCESSID]
     *
     */
    @JSONField(name = "processid")
    @JsonProperty("processid")
    private String processid;

    /**
     * 属性 [MAJORVERSIONNUMBER]
     *
     */
    @JSONField(name = "majorversionnumber")
    @JsonProperty("majorversionnumber")
    private Integer majorversionnumber;

    /**
     * 属性 [RATING]
     *
     */
    @JSONField(name = "rating")
    @JsonProperty("rating")
    private BigDecimal rating;

    /**
     * 属性 [PUBLISHON]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "publishon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("publishon")
    private Timestamp publishon;

    /**
     * 属性 [KNOWLEDGEARTICLEVIEWS]
     *
     */
    @JSONField(name = "knowledgearticleviews")
    @JsonProperty("knowledgearticleviews")
    private Integer knowledgearticleviews;

    /**
     * 属性 [RATING_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "rating_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("rating_date")
    private Timestamp ratingDate;

    /**
     * 属性 [EXCHANGERATE]
     *
     */
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;

    /**
     * 属性 [READYFORREVIEW]
     *
     */
    @JSONField(name = "readyforreview")
    @JsonProperty("readyforreview")
    private Integer readyforreview;

    /**
     * 属性 [IMPORTSEQUENCENUMBER]
     *
     */
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;

    /**
     * 属性 [SUBJECTIDDSC]
     *
     */
    @JSONField(name = "subjectiddsc")
    @JsonProperty("subjectiddsc")
    private Integer subjectiddsc;

    /**
     * 属性 [OWNERNAME]
     *
     */
    @JSONField(name = "ownername")
    @JsonProperty("ownername")
    private String ownername;

    /**
     * 属性 [REVIEW]
     *
     */
    @JSONField(name = "review")
    @JsonProperty("review")
    private String review;

    /**
     * 属性 [LANGUAGELOCALEIDLOCALEID]
     *
     */
    @JSONField(name = "languagelocaleidlocaleid")
    @JsonProperty("languagelocaleidlocaleid")
    private Integer languagelocaleidlocaleid;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;

    /**
     * 属性 [SCHEDULEDSTATUSID]
     *
     */
    @JSONField(name = "scheduledstatusid")
    @JsonProperty("scheduledstatusid")
    private Integer scheduledstatusid;

    /**
     * 属性 [TIMEZONERULEVERSIONNUMBER]
     *
     */
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;

    /**
     * 属性 [INTERNAL]
     *
     */
    @JSONField(name = "internal")
    @JsonProperty("internal")
    private Integer internal;

    /**
     * 属性 [TITLE]
     *
     */
    @JSONField(name = "title")
    @JsonProperty("title")
    private String title;

    /**
     * 属性 [UPDATECONTENT]
     *
     */
    @JSONField(name = "updatecontent")
    @JsonProperty("updatecontent")
    private Integer updatecontent;

    /**
     * 属性 [EXPIRATIONDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "expirationdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("expirationdate")
    private Timestamp expirationdate;

    /**
     * 属性 [EXPIREDREVIEWOPTIONS]
     *
     */
    @JSONField(name = "expiredreviewoptions")
    @JsonProperty("expiredreviewoptions")
    private String expiredreviewoptions;

    /**
     * 属性 [CURRENCYNAME]
     *
     */
    @JSONField(name = "currencyname")
    @JsonProperty("currencyname")
    private String currencyname;

    /**
     * 属性 [PARENTARTICLECONTENTNAME]
     *
     */
    @JSONField(name = "parentarticlecontentname")
    @JsonProperty("parentarticlecontentname")
    private String parentarticlecontentname;

    /**
     * 属性 [ROOTARTICLENAME]
     *
     */
    @JSONField(name = "rootarticlename")
    @JsonProperty("rootarticlename")
    private String rootarticlename;

    /**
     * 属性 [SUBJECTNAME]
     *
     */
    @JSONField(name = "subjectname")
    @JsonProperty("subjectname")
    private String subjectname;

    /**
     * 属性 [LANGUAGELOCALENAME]
     *
     */
    @JSONField(name = "languagelocalename")
    @JsonProperty("languagelocalename")
    private String languagelocalename;

    /**
     * 属性 [PREVIOUSARTICLECONTENTNAME]
     *
     */
    @JSONField(name = "previousarticlecontentname")
    @JsonProperty("previousarticlecontentname")
    private String previousarticlecontentname;

    /**
     * 属性 [PARENTARTICLECONTENTID]
     *
     */
    @JSONField(name = "parentarticlecontentid")
    @JsonProperty("parentarticlecontentid")
    private String parentarticlecontentid;

    /**
     * 属性 [PREVIOUSARTICLECONTENTID]
     *
     */
    @JSONField(name = "previousarticlecontentid")
    @JsonProperty("previousarticlecontentid")
    private String previousarticlecontentid;

    /**
     * 属性 [TRANSACTIONCURRENCYID]
     *
     */
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;

    /**
     * 属性 [ROOTARTICLEID]
     *
     */
    @JSONField(name = "rootarticleid")
    @JsonProperty("rootarticleid")
    private String rootarticleid;

    /**
     * 属性 [SUBJECTID]
     *
     */
    @JSONField(name = "subjectid")
    @JsonProperty("subjectid")
    private String subjectid;

    /**
     * 属性 [LANGUAGELOCALEID]
     *
     */
    @JSONField(name = "languagelocaleid")
    @JsonProperty("languagelocaleid")
    private String languagelocaleid;


    /**
     * 设置 [KNOWLEDGEARTICLEVIEWS_DATE]
     */
    public void setKnowledgearticleviewsDate(Timestamp  knowledgearticleviewsDate){
        this.knowledgearticleviewsDate = knowledgearticleviewsDate ;
        this.modify("knowledgearticleviews_date",knowledgearticleviewsDate);
    }

    /**
     * 设置 [KEYWORDS]
     */
    public void setKeywords(String  keywords){
        this.keywords = keywords ;
        this.modify("keywords",keywords);
    }

    /**
     * 设置 [STATUSCODE]
     */
    public void setStatuscode(Integer  statuscode){
        this.statuscode = statuscode ;
        this.modify("statuscode",statuscode);
    }

    /**
     * 设置 [EXPIRATIONSTATEID]
     */
    public void setExpirationstateid(Integer  expirationstateid){
        this.expirationstateid = expirationstateid ;
        this.modify("expirationstateid",expirationstateid);
    }

    /**
     * 设置 [ISPRIMARY]
     */
    public void setIsprimary(Integer  isprimary){
        this.isprimary = isprimary ;
        this.modify("isprimary",isprimary);
    }

    /**
     * 设置 [RATING_COUNT]
     */
    public void setRatingCount(Integer  ratingCount){
        this.ratingCount = ratingCount ;
        this.modify("rating_count",ratingCount);
    }

    /**
     * 设置 [CONTENT]
     */
    public void setContent(String  content){
        this.content = content ;
        this.modify("content",content);
    }

    /**
     * 设置 [OWNERID]
     */
    public void setOwnerid(String  ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [ROOTARTICLE]
     */
    public void setRootarticled(Integer  rootarticled){
        this.rootarticled = rootarticled ;
        this.modify("rootarticle",rootarticled);
    }

    /**
     * 设置 [KNOWLEDGEARTICLEVIEWS_STATE]
     */
    public void setKnowledgearticleviewsState(Integer  knowledgearticleviewsState){
        this.knowledgearticleviewsState = knowledgearticleviewsState ;
        this.modify("knowledgearticleviews_state",knowledgearticleviewsState);
    }

    /**
     * 设置 [RATING_SUM]
     */
    public void setRatingSum(BigDecimal  ratingSum){
        this.ratingSum = ratingSum ;
        this.modify("rating_sum",ratingSum);
    }

    /**
     * 设置 [STATECODE]
     */
    public void setStatecode(Integer  statecode){
        this.statecode = statecode ;
        this.modify("statecode",statecode);
    }

    /**
     * 设置 [TRAVERSEDPATH]
     */
    public void setTraversedpath(String  traversedpath){
        this.traversedpath = traversedpath ;
        this.modify("traversedpath",traversedpath);
    }

    /**
     * 设置 [PUBLISHSTATUSID]
     */
    public void setPublishstatusid(Integer  publishstatusid){
        this.publishstatusid = publishstatusid ;
        this.modify("publishstatusid",publishstatusid);
    }

    /**
     * 设置 [OWNERTYPE]
     */
    public void setOwnertype(String  ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [ARTICLEPUBLICNUMBER]
     */
    public void setArticlepublicnumber(String  articlepublicnumber){
        this.articlepublicnumber = articlepublicnumber ;
        this.modify("articlepublicnumber",articlepublicnumber);
    }

    /**
     * 设置 [PRIMARYAUTHORIDNAME]
     */
    public void setPrimaryauthoridname(String  primaryauthoridname){
        this.primaryauthoridname = primaryauthoridname ;
        this.modify("primaryauthoridname",primaryauthoridname);
    }

    /**
     * 设置 [OVERRIDDENCREATEDON]
     */
    public void setOverriddencreatedon(Timestamp  overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 设置 [PRIMARYAUTHORID]
     */
    public void setPrimaryauthorid(String  primaryauthorid){
        this.primaryauthorid = primaryauthorid ;
        this.modify("primaryauthorid",primaryauthorid);
    }

    /**
     * 设置 [LATESTVERSION]
     */
    public void setLatestversion(Integer  latestversion){
        this.latestversion = latestversion ;
        this.modify("latestversion",latestversion);
    }

    /**
     * 设置 [RATING_STATE]
     */
    public void setRatingState(Integer  ratingState){
        this.ratingState = ratingState ;
        this.modify("rating_state",ratingState);
    }

    /**
     * 设置 [VERSIONNUMBER]
     */
    public void setVersionnumber(BigInteger  versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [UTCCONVERSIONTIMEZONECODE]
     */
    public void setUtcconversiontimezonecode(Integer  utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [EXPIRATIONSTATUSID]
     */
    public void setExpirationstatusid(Integer  expirationstatusid){
        this.expirationstatusid = expirationstatusid ;
        this.modify("expirationstatusid",expirationstatusid);
    }

    /**
     * 设置 [SETCATEGORYASSOCIATIONS]
     */
    public void setSetcategoryassociations(Integer  setcategoryassociations){
        this.setcategoryassociations = setcategoryassociations ;
        this.modify("setcategoryassociations",setcategoryassociations);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [SETPRODUCTASSOCIATIONS]
     */
    public void setSetproductassociations(Integer  setproductassociations){
        this.setproductassociations = setproductassociations ;
        this.modify("setproductassociations",setproductassociations);
    }

    /**
     * 设置 [MINORVERSIONNUMBER]
     */
    public void setMinorversionnumber(Integer  minorversionnumber){
        this.minorversionnumber = minorversionnumber ;
        this.modify("minorversionnumber",minorversionnumber);
    }

    /**
     * 设置 [STAGEID]
     */
    public void setStageid(String  stageid){
        this.stageid = stageid ;
        this.modify("stageid",stageid);
    }

    /**
     * 设置 [PROCESSID]
     */
    public void setProcessid(String  processid){
        this.processid = processid ;
        this.modify("processid",processid);
    }

    /**
     * 设置 [MAJORVERSIONNUMBER]
     */
    public void setMajorversionnumber(Integer  majorversionnumber){
        this.majorversionnumber = majorversionnumber ;
        this.modify("majorversionnumber",majorversionnumber);
    }

    /**
     * 设置 [RATING]
     */
    public void setRating(BigDecimal  rating){
        this.rating = rating ;
        this.modify("rating",rating);
    }

    /**
     * 设置 [PUBLISHON]
     */
    public void setPublishon(Timestamp  publishon){
        this.publishon = publishon ;
        this.modify("publishon",publishon);
    }

    /**
     * 设置 [KNOWLEDGEARTICLEVIEWS]
     */
    public void setKnowledgearticleviews(Integer  knowledgearticleviews){
        this.knowledgearticleviews = knowledgearticleviews ;
        this.modify("knowledgearticleviews",knowledgearticleviews);
    }

    /**
     * 设置 [RATING_DATE]
     */
    public void setRatingDate(Timestamp  ratingDate){
        this.ratingDate = ratingDate ;
        this.modify("rating_date",ratingDate);
    }

    /**
     * 设置 [EXCHANGERATE]
     */
    public void setExchangerate(BigDecimal  exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [READYFORREVIEW]
     */
    public void setReadyforreview(Integer  readyforreview){
        this.readyforreview = readyforreview ;
        this.modify("readyforreview",readyforreview);
    }

    /**
     * 设置 [IMPORTSEQUENCENUMBER]
     */
    public void setImportsequencenumber(Integer  importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [SUBJECTIDDSC]
     */
    public void setSubjectiddsc(Integer  subjectiddsc){
        this.subjectiddsc = subjectiddsc ;
        this.modify("subjectiddsc",subjectiddsc);
    }

    /**
     * 设置 [OWNERNAME]
     */
    public void setOwnername(String  ownername){
        this.ownername = ownername ;
        this.modify("ownername",ownername);
    }

    /**
     * 设置 [REVIEW]
     */
    public void setReview(String  review){
        this.review = review ;
        this.modify("review",review);
    }

    /**
     * 设置 [LANGUAGELOCALEIDLOCALEID]
     */
    public void setLanguagelocaleidlocaleid(Integer  languagelocaleidlocaleid){
        this.languagelocaleidlocaleid = languagelocaleidlocaleid ;
        this.modify("languagelocaleidlocaleid",languagelocaleidlocaleid);
    }

    /**
     * 设置 [SCHEDULEDSTATUSID]
     */
    public void setScheduledstatusid(Integer  scheduledstatusid){
        this.scheduledstatusid = scheduledstatusid ;
        this.modify("scheduledstatusid",scheduledstatusid);
    }

    /**
     * 设置 [TIMEZONERULEVERSIONNUMBER]
     */
    public void setTimezoneruleversionnumber(Integer  timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [INTERNAL]
     */
    public void setInternal(Integer  internal){
        this.internal = internal ;
        this.modify("internal",internal);
    }

    /**
     * 设置 [TITLE]
     */
    public void setTitle(String  title){
        this.title = title ;
        this.modify("title",title);
    }

    /**
     * 设置 [UPDATECONTENT]
     */
    public void setUpdatecontent(Integer  updatecontent){
        this.updatecontent = updatecontent ;
        this.modify("updatecontent",updatecontent);
    }

    /**
     * 设置 [EXPIRATIONDATE]
     */
    public void setExpirationdate(Timestamp  expirationdate){
        this.expirationdate = expirationdate ;
        this.modify("expirationdate",expirationdate);
    }

    /**
     * 设置 [EXPIREDREVIEWOPTIONS]
     */
    public void setExpiredreviewoptions(String  expiredreviewoptions){
        this.expiredreviewoptions = expiredreviewoptions ;
        this.modify("expiredreviewoptions",expiredreviewoptions);
    }

    /**
     * 设置 [CURRENCYNAME]
     */
    public void setCurrencyname(String  currencyname){
        this.currencyname = currencyname ;
        this.modify("currencyname",currencyname);
    }

    /**
     * 设置 [PARENTARTICLECONTENTNAME]
     */
    public void setParentarticlecontentname(String  parentarticlecontentname){
        this.parentarticlecontentname = parentarticlecontentname ;
        this.modify("parentarticlecontentname",parentarticlecontentname);
    }

    /**
     * 设置 [ROOTARTICLENAME]
     */
    public void setRootarticlename(String  rootarticlename){
        this.rootarticlename = rootarticlename ;
        this.modify("rootarticlename",rootarticlename);
    }

    /**
     * 设置 [SUBJECTNAME]
     */
    public void setSubjectname(String  subjectname){
        this.subjectname = subjectname ;
        this.modify("subjectname",subjectname);
    }

    /**
     * 设置 [LANGUAGELOCALENAME]
     */
    public void setLanguagelocalename(String  languagelocalename){
        this.languagelocalename = languagelocalename ;
        this.modify("languagelocalename",languagelocalename);
    }

    /**
     * 设置 [PREVIOUSARTICLECONTENTNAME]
     */
    public void setPreviousarticlecontentname(String  previousarticlecontentname){
        this.previousarticlecontentname = previousarticlecontentname ;
        this.modify("previousarticlecontentname",previousarticlecontentname);
    }

    /**
     * 设置 [PARENTARTICLECONTENTID]
     */
    public void setParentarticlecontentid(String  parentarticlecontentid){
        this.parentarticlecontentid = parentarticlecontentid ;
        this.modify("parentarticlecontentid",parentarticlecontentid);
    }

    /**
     * 设置 [PREVIOUSARTICLECONTENTID]
     */
    public void setPreviousarticlecontentid(String  previousarticlecontentid){
        this.previousarticlecontentid = previousarticlecontentid ;
        this.modify("previousarticlecontentid",previousarticlecontentid);
    }

    /**
     * 设置 [TRANSACTIONCURRENCYID]
     */
    public void setTransactioncurrencyid(String  transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }

    /**
     * 设置 [ROOTARTICLEID]
     */
    public void setRootarticleid(String  rootarticleid){
        this.rootarticleid = rootarticleid ;
        this.modify("rootarticleid",rootarticleid);
    }

    /**
     * 设置 [SUBJECTID]
     */
    public void setSubjectid(String  subjectid){
        this.subjectid = subjectid ;
        this.modify("subjectid",subjectid);
    }

    /**
     * 设置 [LANGUAGELOCALEID]
     */
    public void setLanguagelocaleid(String  languagelocaleid){
        this.languagelocaleid = languagelocaleid ;
        this.modify("languagelocaleid",languagelocaleid);
    }


}

