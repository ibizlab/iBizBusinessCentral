package cn.ibizlab.businesscentral.centralapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[LanguageLocaleDTO]
 */
@Data
public class LanguageLocaleDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [LANGUAGELOCALEID]
     *
     */
    @JSONField(name = "languagelocaleid")
    @JsonProperty("languagelocaleid")
    private String languagelocaleid;

    /**
     * 属性 [LANGUAGELOCALENAME]
     *
     */
    @JSONField(name = "languagelocalename")
    @JsonProperty("languagelocalename")
    private String languagelocalename;

    /**
     * 属性 [LOCALEID]
     *
     */
    @JSONField(name = "localeid")
    @JsonProperty("localeid")
    private Integer localeid;

    /**
     * 属性 [LANGUAGE]
     *
     */
    @JSONField(name = "language")
    @JsonProperty("language")
    private String language;

    /**
     * 属性 [VERSIONNUMBER]
     *
     */
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;

    /**
     * 属性 [CODE]
     *
     */
    @JSONField(name = "code")
    @JsonProperty("code")
    private String code;

    /**
     * 属性 [STATUSCODE]
     *
     */
    @JSONField(name = "statuscode")
    @JsonProperty("statuscode")
    private Integer statuscode;

    /**
     * 属性 [STATECODE]
     *
     */
    @JSONField(name = "statecode")
    @JsonProperty("statecode")
    private Integer statecode;

    /**
     * 属性 [REGION]
     *
     */
    @JSONField(name = "region")
    @JsonProperty("region")
    private String region;


    /**
     * 设置 [LANGUAGELOCALENAME]
     */
    public void setLanguagelocalename(String  languagelocalename){
        this.languagelocalename = languagelocalename ;
        this.modify("languagelocalename",languagelocalename);
    }

    /**
     * 设置 [LOCALEID]
     */
    public void setLocaleid(Integer  localeid){
        this.localeid = localeid ;
        this.modify("localeid",localeid);
    }

    /**
     * 设置 [LANGUAGE]
     */
    public void setLanguage(String  language){
        this.language = language ;
        this.modify("language",language);
    }

    /**
     * 设置 [VERSIONNUMBER]
     */
    public void setVersionnumber(BigInteger  versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [CODE]
     */
    public void setCode(String  code){
        this.code = code ;
        this.modify("code",code);
    }

    /**
     * 设置 [STATUSCODE]
     */
    public void setStatuscode(Integer  statuscode){
        this.statuscode = statuscode ;
        this.modify("statuscode",statuscode);
    }

    /**
     * 设置 [STATECODE]
     */
    public void setStatecode(Integer  statecode){
        this.statecode = statecode ;
        this.modify("statecode",statecode);
    }

    /**
     * 设置 [REGION]
     */
    public void setRegion(String  region){
        this.region = region ;
        this.modify("region",region);
    }


}

