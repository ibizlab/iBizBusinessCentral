package cn.ibizlab.businesscentral.centralapi.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.businesscentral.centralapi")
public class CentralApiRestConfiguration {

}
