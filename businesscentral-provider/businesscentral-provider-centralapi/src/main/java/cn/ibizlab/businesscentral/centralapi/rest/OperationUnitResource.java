package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.base.domain.OperationUnit;
import cn.ibizlab.businesscentral.core.base.service.IOperationUnitService;
import cn.ibizlab.businesscentral.core.base.filter.OperationUnitSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"运营单位" })
@RestController("CentralApi-operationunit")
@RequestMapping("")
public class OperationUnitResource {

    @Autowired
    public IOperationUnitService operationunitService;

    @Autowired
    @Lazy
    public OperationUnitMapping operationunitMapping;

    @PreAuthorize("hasPermission(this.operationunitMapping.toDomain(#operationunitdto),'iBizBusinessCentral-OperationUnit-Create')")
    @ApiOperation(value = "新建运营单位", tags = {"运营单位" },  notes = "新建运营单位")
	@RequestMapping(method = RequestMethod.POST, value = "/operationunits")
    public ResponseEntity<OperationUnitDTO> create(@RequestBody OperationUnitDTO operationunitdto) {
        OperationUnit domain = operationunitMapping.toDomain(operationunitdto);
		operationunitService.create(domain);
        OperationUnitDTO dto = operationunitMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.operationunitMapping.toDomain(#operationunitdtos),'iBizBusinessCentral-OperationUnit-Create')")
    @ApiOperation(value = "批量新建运营单位", tags = {"运营单位" },  notes = "批量新建运营单位")
	@RequestMapping(method = RequestMethod.POST, value = "/operationunits/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<OperationUnitDTO> operationunitdtos) {
        operationunitService.createBatch(operationunitMapping.toDomain(operationunitdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "operationunit" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.operationunitService.get(#operationunit_id),'iBizBusinessCentral-OperationUnit-Update')")
    @ApiOperation(value = "更新运营单位", tags = {"运营单位" },  notes = "更新运营单位")
	@RequestMapping(method = RequestMethod.PUT, value = "/operationunits/{operationunit_id}")
    public ResponseEntity<OperationUnitDTO> update(@PathVariable("operationunit_id") String operationunit_id, @RequestBody OperationUnitDTO operationunitdto) {
		OperationUnit domain  = operationunitMapping.toDomain(operationunitdto);
        domain .setOperationunitid(operationunit_id);
		operationunitService.update(domain );
		OperationUnitDTO dto = operationunitMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.operationunitService.getOperationunitByEntities(this.operationunitMapping.toDomain(#operationunitdtos)),'iBizBusinessCentral-OperationUnit-Update')")
    @ApiOperation(value = "批量更新运营单位", tags = {"运营单位" },  notes = "批量更新运营单位")
	@RequestMapping(method = RequestMethod.PUT, value = "/operationunits/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<OperationUnitDTO> operationunitdtos) {
        operationunitService.updateBatch(operationunitMapping.toDomain(operationunitdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.operationunitService.get(#operationunit_id),'iBizBusinessCentral-OperationUnit-Remove')")
    @ApiOperation(value = "删除运营单位", tags = {"运营单位" },  notes = "删除运营单位")
	@RequestMapping(method = RequestMethod.DELETE, value = "/operationunits/{operationunit_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("operationunit_id") String operationunit_id) {
         return ResponseEntity.status(HttpStatus.OK).body(operationunitService.remove(operationunit_id));
    }

    @PreAuthorize("hasPermission(this.operationunitService.getOperationunitByIds(#ids),'iBizBusinessCentral-OperationUnit-Remove')")
    @ApiOperation(value = "批量删除运营单位", tags = {"运营单位" },  notes = "批量删除运营单位")
	@RequestMapping(method = RequestMethod.DELETE, value = "/operationunits/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        operationunitService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.operationunitMapping.toDomain(returnObject.body),'iBizBusinessCentral-OperationUnit-Get')")
    @ApiOperation(value = "获取运营单位", tags = {"运营单位" },  notes = "获取运营单位")
	@RequestMapping(method = RequestMethod.GET, value = "/operationunits/{operationunit_id}")
    public ResponseEntity<OperationUnitDTO> get(@PathVariable("operationunit_id") String operationunit_id) {
        OperationUnit domain = operationunitService.get(operationunit_id);
        OperationUnitDTO dto = operationunitMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取运营单位草稿", tags = {"运营单位" },  notes = "获取运营单位草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/operationunits/getdraft")
    public ResponseEntity<OperationUnitDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(operationunitMapping.toDto(operationunitService.getDraft(new OperationUnit())));
    }

    @ApiOperation(value = "检查运营单位", tags = {"运营单位" },  notes = "检查运营单位")
	@RequestMapping(method = RequestMethod.POST, value = "/operationunits/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody OperationUnitDTO operationunitdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(operationunitService.checkKey(operationunitMapping.toDomain(operationunitdto)));
    }

    @PreAuthorize("hasPermission(this.operationunitMapping.toDomain(#operationunitdto),'iBizBusinessCentral-OperationUnit-Save')")
    @ApiOperation(value = "保存运营单位", tags = {"运营单位" },  notes = "保存运营单位")
	@RequestMapping(method = RequestMethod.POST, value = "/operationunits/save")
    public ResponseEntity<Boolean> save(@RequestBody OperationUnitDTO operationunitdto) {
        return ResponseEntity.status(HttpStatus.OK).body(operationunitService.save(operationunitMapping.toDomain(operationunitdto)));
    }

    @PreAuthorize("hasPermission(this.operationunitMapping.toDomain(#operationunitdtos),'iBizBusinessCentral-OperationUnit-Save')")
    @ApiOperation(value = "批量保存运营单位", tags = {"运营单位" },  notes = "批量保存运营单位")
	@RequestMapping(method = RequestMethod.POST, value = "/operationunits/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<OperationUnitDTO> operationunitdtos) {
        operationunitService.saveBatch(operationunitMapping.toDomain(operationunitdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OperationUnit-searchBusinessUnit-all') and hasPermission(#context,'iBizBusinessCentral-OperationUnit-Get')")
	@ApiOperation(value = "获取业务单位", tags = {"运营单位" } ,notes = "获取业务单位")
    @RequestMapping(method= RequestMethod.GET , value="/operationunits/fetchbusinessunit")
	public ResponseEntity<List<OperationUnitDTO>> fetchBusinessUnit(OperationUnitSearchContext context) {
        Page<OperationUnit> domains = operationunitService.searchBusinessUnit(context) ;
        List<OperationUnitDTO> list = operationunitMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OperationUnit-searchBusinessUnit-all') and hasPermission(#context,'iBizBusinessCentral-OperationUnit-Get')")
	@ApiOperation(value = "查询业务单位", tags = {"运营单位" } ,notes = "查询业务单位")
    @RequestMapping(method= RequestMethod.POST , value="/operationunits/searchbusinessunit")
	public ResponseEntity<Page<OperationUnitDTO>> searchBusinessUnit(@RequestBody OperationUnitSearchContext context) {
        Page<OperationUnit> domains = operationunitService.searchBusinessUnit(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(operationunitMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OperationUnit-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-OperationUnit-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"运营单位" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/operationunits/fetchdefault")
	public ResponseEntity<List<OperationUnitDTO>> fetchDefault(OperationUnitSearchContext context) {
        Page<OperationUnit> domains = operationunitService.searchDefault(context) ;
        List<OperationUnitDTO> list = operationunitMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OperationUnit-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-OperationUnit-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"运营单位" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/operationunits/searchdefault")
	public ResponseEntity<Page<OperationUnitDTO>> searchDefault(@RequestBody OperationUnitSearchContext context) {
        Page<OperationUnit> domains = operationunitService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(operationunitMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OperationUnit-searchDepartment-all') and hasPermission(#context,'iBizBusinessCentral-OperationUnit-Get')")
	@ApiOperation(value = "获取部门", tags = {"运营单位" } ,notes = "获取部门")
    @RequestMapping(method= RequestMethod.GET , value="/operationunits/fetchdepartment")
	public ResponseEntity<List<OperationUnitDTO>> fetchDepartment(OperationUnitSearchContext context) {
        Page<OperationUnit> domains = operationunitService.searchDepartment(context) ;
        List<OperationUnitDTO> list = operationunitMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OperationUnit-searchDepartment-all') and hasPermission(#context,'iBizBusinessCentral-OperationUnit-Get')")
	@ApiOperation(value = "查询部门", tags = {"运营单位" } ,notes = "查询部门")
    @RequestMapping(method= RequestMethod.POST , value="/operationunits/searchdepartment")
	public ResponseEntity<Page<OperationUnitDTO>> searchDepartment(@RequestBody OperationUnitSearchContext context) {
        Page<OperationUnit> domains = operationunitService.searchDepartment(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(operationunitMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

