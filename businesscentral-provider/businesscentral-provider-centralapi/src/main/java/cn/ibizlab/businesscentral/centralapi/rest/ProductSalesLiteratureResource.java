package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.product.domain.ProductSalesLiterature;
import cn.ibizlab.businesscentral.core.product.service.IProductSalesLiteratureService;
import cn.ibizlab.businesscentral.core.product.filter.ProductSalesLiteratureSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"产品宣传资料" })
@RestController("CentralApi-productsalesliterature")
@RequestMapping("")
public class ProductSalesLiteratureResource {

    @Autowired
    public IProductSalesLiteratureService productsalesliteratureService;

    @Autowired
    @Lazy
    public ProductSalesLiteratureMapping productsalesliteratureMapping;

    @PreAuthorize("hasPermission(this.productsalesliteratureMapping.toDomain(#productsalesliteraturedto),'iBizBusinessCentral-ProductSalesLiterature-Create')")
    @ApiOperation(value = "新建产品宣传资料", tags = {"产品宣传资料" },  notes = "新建产品宣传资料")
	@RequestMapping(method = RequestMethod.POST, value = "/productsalesliteratures")
    public ResponseEntity<ProductSalesLiteratureDTO> create(@RequestBody ProductSalesLiteratureDTO productsalesliteraturedto) {
        ProductSalesLiterature domain = productsalesliteratureMapping.toDomain(productsalesliteraturedto);
		productsalesliteratureService.create(domain);
        ProductSalesLiteratureDTO dto = productsalesliteratureMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.productsalesliteratureMapping.toDomain(#productsalesliteraturedtos),'iBizBusinessCentral-ProductSalesLiterature-Create')")
    @ApiOperation(value = "批量新建产品宣传资料", tags = {"产品宣传资料" },  notes = "批量新建产品宣传资料")
	@RequestMapping(method = RequestMethod.POST, value = "/productsalesliteratures/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<ProductSalesLiteratureDTO> productsalesliteraturedtos) {
        productsalesliteratureService.createBatch(productsalesliteratureMapping.toDomain(productsalesliteraturedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "productsalesliterature" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.productsalesliteratureService.get(#productsalesliterature_id),'iBizBusinessCentral-ProductSalesLiterature-Update')")
    @ApiOperation(value = "更新产品宣传资料", tags = {"产品宣传资料" },  notes = "更新产品宣传资料")
	@RequestMapping(method = RequestMethod.PUT, value = "/productsalesliteratures/{productsalesliterature_id}")
    public ResponseEntity<ProductSalesLiteratureDTO> update(@PathVariable("productsalesliterature_id") String productsalesliterature_id, @RequestBody ProductSalesLiteratureDTO productsalesliteraturedto) {
		ProductSalesLiterature domain  = productsalesliteratureMapping.toDomain(productsalesliteraturedto);
        domain .setRelationshipsid(productsalesliterature_id);
		productsalesliteratureService.update(domain );
		ProductSalesLiteratureDTO dto = productsalesliteratureMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.productsalesliteratureService.getProductsalesliteratureByEntities(this.productsalesliteratureMapping.toDomain(#productsalesliteraturedtos)),'iBizBusinessCentral-ProductSalesLiterature-Update')")
    @ApiOperation(value = "批量更新产品宣传资料", tags = {"产品宣传资料" },  notes = "批量更新产品宣传资料")
	@RequestMapping(method = RequestMethod.PUT, value = "/productsalesliteratures/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<ProductSalesLiteratureDTO> productsalesliteraturedtos) {
        productsalesliteratureService.updateBatch(productsalesliteratureMapping.toDomain(productsalesliteraturedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.productsalesliteratureService.get(#productsalesliterature_id),'iBizBusinessCentral-ProductSalesLiterature-Remove')")
    @ApiOperation(value = "删除产品宣传资料", tags = {"产品宣传资料" },  notes = "删除产品宣传资料")
	@RequestMapping(method = RequestMethod.DELETE, value = "/productsalesliteratures/{productsalesliterature_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("productsalesliterature_id") String productsalesliterature_id) {
         return ResponseEntity.status(HttpStatus.OK).body(productsalesliteratureService.remove(productsalesliterature_id));
    }

    @PreAuthorize("hasPermission(this.productsalesliteratureService.getProductsalesliteratureByIds(#ids),'iBizBusinessCentral-ProductSalesLiterature-Remove')")
    @ApiOperation(value = "批量删除产品宣传资料", tags = {"产品宣传资料" },  notes = "批量删除产品宣传资料")
	@RequestMapping(method = RequestMethod.DELETE, value = "/productsalesliteratures/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        productsalesliteratureService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.productsalesliteratureMapping.toDomain(returnObject.body),'iBizBusinessCentral-ProductSalesLiterature-Get')")
    @ApiOperation(value = "获取产品宣传资料", tags = {"产品宣传资料" },  notes = "获取产品宣传资料")
	@RequestMapping(method = RequestMethod.GET, value = "/productsalesliteratures/{productsalesliterature_id}")
    public ResponseEntity<ProductSalesLiteratureDTO> get(@PathVariable("productsalesliterature_id") String productsalesliterature_id) {
        ProductSalesLiterature domain = productsalesliteratureService.get(productsalesliterature_id);
        ProductSalesLiteratureDTO dto = productsalesliteratureMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取产品宣传资料草稿", tags = {"产品宣传资料" },  notes = "获取产品宣传资料草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/productsalesliteratures/getdraft")
    public ResponseEntity<ProductSalesLiteratureDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(productsalesliteratureMapping.toDto(productsalesliteratureService.getDraft(new ProductSalesLiterature())));
    }

    @ApiOperation(value = "检查产品宣传资料", tags = {"产品宣传资料" },  notes = "检查产品宣传资料")
	@RequestMapping(method = RequestMethod.POST, value = "/productsalesliteratures/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody ProductSalesLiteratureDTO productsalesliteraturedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(productsalesliteratureService.checkKey(productsalesliteratureMapping.toDomain(productsalesliteraturedto)));
    }

    @PreAuthorize("hasPermission(this.productsalesliteratureMapping.toDomain(#productsalesliteraturedto),'iBizBusinessCentral-ProductSalesLiterature-Save')")
    @ApiOperation(value = "保存产品宣传资料", tags = {"产品宣传资料" },  notes = "保存产品宣传资料")
	@RequestMapping(method = RequestMethod.POST, value = "/productsalesliteratures/save")
    public ResponseEntity<Boolean> save(@RequestBody ProductSalesLiteratureDTO productsalesliteraturedto) {
        return ResponseEntity.status(HttpStatus.OK).body(productsalesliteratureService.save(productsalesliteratureMapping.toDomain(productsalesliteraturedto)));
    }

    @PreAuthorize("hasPermission(this.productsalesliteratureMapping.toDomain(#productsalesliteraturedtos),'iBizBusinessCentral-ProductSalesLiterature-Save')")
    @ApiOperation(value = "批量保存产品宣传资料", tags = {"产品宣传资料" },  notes = "批量保存产品宣传资料")
	@RequestMapping(method = RequestMethod.POST, value = "/productsalesliteratures/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<ProductSalesLiteratureDTO> productsalesliteraturedtos) {
        productsalesliteratureService.saveBatch(productsalesliteratureMapping.toDomain(productsalesliteraturedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ProductSalesLiterature-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-ProductSalesLiterature-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"产品宣传资料" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/productsalesliteratures/fetchdefault")
	public ResponseEntity<List<ProductSalesLiteratureDTO>> fetchDefault(ProductSalesLiteratureSearchContext context) {
        Page<ProductSalesLiterature> domains = productsalesliteratureService.searchDefault(context) ;
        List<ProductSalesLiteratureDTO> list = productsalesliteratureMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ProductSalesLiterature-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-ProductSalesLiterature-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"产品宣传资料" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/productsalesliteratures/searchdefault")
	public ResponseEntity<Page<ProductSalesLiteratureDTO>> searchDefault(@RequestBody ProductSalesLiteratureSearchContext context) {
        Page<ProductSalesLiterature> domains = productsalesliteratureService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(productsalesliteratureMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.productsalesliteratureMapping.toDomain(#productsalesliteraturedto),'iBizBusinessCentral-ProductSalesLiterature-Create')")
    @ApiOperation(value = "根据销售宣传资料建立产品宣传资料", tags = {"产品宣传资料" },  notes = "根据销售宣传资料建立产品宣传资料")
	@RequestMapping(method = RequestMethod.POST, value = "/salesliteratures/{salesliterature_id}/productsalesliteratures")
    public ResponseEntity<ProductSalesLiteratureDTO> createBySalesLiterature(@PathVariable("salesliterature_id") String salesliterature_id, @RequestBody ProductSalesLiteratureDTO productsalesliteraturedto) {
        ProductSalesLiterature domain = productsalesliteratureMapping.toDomain(productsalesliteraturedto);
        domain.setEntity2id(salesliterature_id);
		productsalesliteratureService.create(domain);
        ProductSalesLiteratureDTO dto = productsalesliteratureMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.productsalesliteratureMapping.toDomain(#productsalesliteraturedtos),'iBizBusinessCentral-ProductSalesLiterature-Create')")
    @ApiOperation(value = "根据销售宣传资料批量建立产品宣传资料", tags = {"产品宣传资料" },  notes = "根据销售宣传资料批量建立产品宣传资料")
	@RequestMapping(method = RequestMethod.POST, value = "/salesliteratures/{salesliterature_id}/productsalesliteratures/batch")
    public ResponseEntity<Boolean> createBatchBySalesLiterature(@PathVariable("salesliterature_id") String salesliterature_id, @RequestBody List<ProductSalesLiteratureDTO> productsalesliteraturedtos) {
        List<ProductSalesLiterature> domainlist=productsalesliteratureMapping.toDomain(productsalesliteraturedtos);
        for(ProductSalesLiterature domain:domainlist){
            domain.setEntity2id(salesliterature_id);
        }
        productsalesliteratureService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "productsalesliterature" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.productsalesliteratureService.get(#productsalesliterature_id),'iBizBusinessCentral-ProductSalesLiterature-Update')")
    @ApiOperation(value = "根据销售宣传资料更新产品宣传资料", tags = {"产品宣传资料" },  notes = "根据销售宣传资料更新产品宣传资料")
	@RequestMapping(method = RequestMethod.PUT, value = "/salesliteratures/{salesliterature_id}/productsalesliteratures/{productsalesliterature_id}")
    public ResponseEntity<ProductSalesLiteratureDTO> updateBySalesLiterature(@PathVariable("salesliterature_id") String salesliterature_id, @PathVariable("productsalesliterature_id") String productsalesliterature_id, @RequestBody ProductSalesLiteratureDTO productsalesliteraturedto) {
        ProductSalesLiterature domain = productsalesliteratureMapping.toDomain(productsalesliteraturedto);
        domain.setEntity2id(salesliterature_id);
        domain.setRelationshipsid(productsalesliterature_id);
		productsalesliteratureService.update(domain);
        ProductSalesLiteratureDTO dto = productsalesliteratureMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.productsalesliteratureService.getProductsalesliteratureByEntities(this.productsalesliteratureMapping.toDomain(#productsalesliteraturedtos)),'iBizBusinessCentral-ProductSalesLiterature-Update')")
    @ApiOperation(value = "根据销售宣传资料批量更新产品宣传资料", tags = {"产品宣传资料" },  notes = "根据销售宣传资料批量更新产品宣传资料")
	@RequestMapping(method = RequestMethod.PUT, value = "/salesliteratures/{salesliterature_id}/productsalesliteratures/batch")
    public ResponseEntity<Boolean> updateBatchBySalesLiterature(@PathVariable("salesliterature_id") String salesliterature_id, @RequestBody List<ProductSalesLiteratureDTO> productsalesliteraturedtos) {
        List<ProductSalesLiterature> domainlist=productsalesliteratureMapping.toDomain(productsalesliteraturedtos);
        for(ProductSalesLiterature domain:domainlist){
            domain.setEntity2id(salesliterature_id);
        }
        productsalesliteratureService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.productsalesliteratureService.get(#productsalesliterature_id),'iBizBusinessCentral-ProductSalesLiterature-Remove')")
    @ApiOperation(value = "根据销售宣传资料删除产品宣传资料", tags = {"产品宣传资料" },  notes = "根据销售宣传资料删除产品宣传资料")
	@RequestMapping(method = RequestMethod.DELETE, value = "/salesliteratures/{salesliterature_id}/productsalesliteratures/{productsalesliterature_id}")
    public ResponseEntity<Boolean> removeBySalesLiterature(@PathVariable("salesliterature_id") String salesliterature_id, @PathVariable("productsalesliterature_id") String productsalesliterature_id) {
		return ResponseEntity.status(HttpStatus.OK).body(productsalesliteratureService.remove(productsalesliterature_id));
    }

    @PreAuthorize("hasPermission(this.productsalesliteratureService.getProductsalesliteratureByIds(#ids),'iBizBusinessCentral-ProductSalesLiterature-Remove')")
    @ApiOperation(value = "根据销售宣传资料批量删除产品宣传资料", tags = {"产品宣传资料" },  notes = "根据销售宣传资料批量删除产品宣传资料")
	@RequestMapping(method = RequestMethod.DELETE, value = "/salesliteratures/{salesliterature_id}/productsalesliteratures/batch")
    public ResponseEntity<Boolean> removeBatchBySalesLiterature(@RequestBody List<String> ids) {
        productsalesliteratureService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.productsalesliteratureMapping.toDomain(returnObject.body),'iBizBusinessCentral-ProductSalesLiterature-Get')")
    @ApiOperation(value = "根据销售宣传资料获取产品宣传资料", tags = {"产品宣传资料" },  notes = "根据销售宣传资料获取产品宣传资料")
	@RequestMapping(method = RequestMethod.GET, value = "/salesliteratures/{salesliterature_id}/productsalesliteratures/{productsalesliterature_id}")
    public ResponseEntity<ProductSalesLiteratureDTO> getBySalesLiterature(@PathVariable("salesliterature_id") String salesliterature_id, @PathVariable("productsalesliterature_id") String productsalesliterature_id) {
        ProductSalesLiterature domain = productsalesliteratureService.get(productsalesliterature_id);
        ProductSalesLiteratureDTO dto = productsalesliteratureMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据销售宣传资料获取产品宣传资料草稿", tags = {"产品宣传资料" },  notes = "根据销售宣传资料获取产品宣传资料草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/salesliteratures/{salesliterature_id}/productsalesliteratures/getdraft")
    public ResponseEntity<ProductSalesLiteratureDTO> getDraftBySalesLiterature(@PathVariable("salesliterature_id") String salesliterature_id) {
        ProductSalesLiterature domain = new ProductSalesLiterature();
        domain.setEntity2id(salesliterature_id);
        return ResponseEntity.status(HttpStatus.OK).body(productsalesliteratureMapping.toDto(productsalesliteratureService.getDraft(domain)));
    }

    @ApiOperation(value = "根据销售宣传资料检查产品宣传资料", tags = {"产品宣传资料" },  notes = "根据销售宣传资料检查产品宣传资料")
	@RequestMapping(method = RequestMethod.POST, value = "/salesliteratures/{salesliterature_id}/productsalesliteratures/checkkey")
    public ResponseEntity<Boolean> checkKeyBySalesLiterature(@PathVariable("salesliterature_id") String salesliterature_id, @RequestBody ProductSalesLiteratureDTO productsalesliteraturedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(productsalesliteratureService.checkKey(productsalesliteratureMapping.toDomain(productsalesliteraturedto)));
    }

    @PreAuthorize("hasPermission(this.productsalesliteratureMapping.toDomain(#productsalesliteraturedto),'iBizBusinessCentral-ProductSalesLiterature-Save')")
    @ApiOperation(value = "根据销售宣传资料保存产品宣传资料", tags = {"产品宣传资料" },  notes = "根据销售宣传资料保存产品宣传资料")
	@RequestMapping(method = RequestMethod.POST, value = "/salesliteratures/{salesliterature_id}/productsalesliteratures/save")
    public ResponseEntity<Boolean> saveBySalesLiterature(@PathVariable("salesliterature_id") String salesliterature_id, @RequestBody ProductSalesLiteratureDTO productsalesliteraturedto) {
        ProductSalesLiterature domain = productsalesliteratureMapping.toDomain(productsalesliteraturedto);
        domain.setEntity2id(salesliterature_id);
        return ResponseEntity.status(HttpStatus.OK).body(productsalesliteratureService.save(domain));
    }

    @PreAuthorize("hasPermission(this.productsalesliteratureMapping.toDomain(#productsalesliteraturedtos),'iBizBusinessCentral-ProductSalesLiterature-Save')")
    @ApiOperation(value = "根据销售宣传资料批量保存产品宣传资料", tags = {"产品宣传资料" },  notes = "根据销售宣传资料批量保存产品宣传资料")
	@RequestMapping(method = RequestMethod.POST, value = "/salesliteratures/{salesliterature_id}/productsalesliteratures/savebatch")
    public ResponseEntity<Boolean> saveBatchBySalesLiterature(@PathVariable("salesliterature_id") String salesliterature_id, @RequestBody List<ProductSalesLiteratureDTO> productsalesliteraturedtos) {
        List<ProductSalesLiterature> domainlist=productsalesliteratureMapping.toDomain(productsalesliteraturedtos);
        for(ProductSalesLiterature domain:domainlist){
             domain.setEntity2id(salesliterature_id);
        }
        productsalesliteratureService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ProductSalesLiterature-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-ProductSalesLiterature-Get')")
	@ApiOperation(value = "根据销售宣传资料获取DEFAULT", tags = {"产品宣传资料" } ,notes = "根据销售宣传资料获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/salesliteratures/{salesliterature_id}/productsalesliteratures/fetchdefault")
	public ResponseEntity<List<ProductSalesLiteratureDTO>> fetchProductSalesLiteratureDefaultBySalesLiterature(@PathVariable("salesliterature_id") String salesliterature_id,ProductSalesLiteratureSearchContext context) {
        context.setN_entity2id_eq(salesliterature_id);
        Page<ProductSalesLiterature> domains = productsalesliteratureService.searchDefault(context) ;
        List<ProductSalesLiteratureDTO> list = productsalesliteratureMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ProductSalesLiterature-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-ProductSalesLiterature-Get')")
	@ApiOperation(value = "根据销售宣传资料查询DEFAULT", tags = {"产品宣传资料" } ,notes = "根据销售宣传资料查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/salesliteratures/{salesliterature_id}/productsalesliteratures/searchdefault")
	public ResponseEntity<Page<ProductSalesLiteratureDTO>> searchProductSalesLiteratureDefaultBySalesLiterature(@PathVariable("salesliterature_id") String salesliterature_id, @RequestBody ProductSalesLiteratureSearchContext context) {
        context.setN_entity2id_eq(salesliterature_id);
        Page<ProductSalesLiterature> domains = productsalesliteratureService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(productsalesliteratureMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

