package cn.ibizlab.businesscentral.centralapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[OpportunityCompetitorDTO]
 */
@Data
public class OpportunityCompetitorDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [RELATIONSHIPSID]
     *
     */
    @JSONField(name = "relationshipsid")
    @JsonProperty("relationshipsid")
    private String relationshipsid;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;

    /**
     * 属性 [RELATIONSHIPSTYPE]
     *
     */
    @JSONField(name = "relationshipstype")
    @JsonProperty("relationshipstype")
    private String relationshipstype;

    /**
     * 属性 [RELATIONSHIPSNAME]
     *
     */
    @JSONField(name = "relationshipsname")
    @JsonProperty("relationshipsname")
    private String relationshipsname;

    /**
     * 属性 [ORIGINATINGLEADID]
     *
     */
    @JSONField(name = "originatingleadid")
    @JsonProperty("originatingleadid")
    private String originatingleadid;

    /**
     * 属性 [ENTITYNAME]
     *
     */
    @JSONField(name = "entityname")
    @JsonProperty("entityname")
    private String entityname;

    /**
     * 属性 [ESTIMATEDVALUE]
     *
     */
    @JSONField(name = "estimatedvalue")
    @JsonProperty("estimatedvalue")
    private BigDecimal estimatedvalue;

    /**
     * 属性 [ORIGINATINGLEADNAME]
     *
     */
    @JSONField(name = "originatingleadname")
    @JsonProperty("originatingleadname")
    private String originatingleadname;

    /**
     * 属性 [ESTIMATEDCLOSEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "estimatedclosedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("estimatedclosedate")
    private Timestamp estimatedclosedate;

    /**
     * 属性 [ACTUALVALUE]
     *
     */
    @JSONField(name = "actualvalue")
    @JsonProperty("actualvalue")
    private BigDecimal actualvalue;

    /**
     * 属性 [STATECODE]
     *
     */
    @JSONField(name = "statecode")
    @JsonProperty("statecode")
    private Integer statecode;

    /**
     * 属性 [ENTITY2NAME]
     *
     */
    @JSONField(name = "entity2name")
    @JsonProperty("entity2name")
    private String entity2name;

    /**
     * 属性 [ENTITY2ID]
     *
     */
    @JSONField(name = "entity2id")
    @JsonProperty("entity2id")
    private String entity2id;

    /**
     * 属性 [ENTITYID]
     *
     */
    @JSONField(name = "entityid")
    @JsonProperty("entityid")
    private String entityid;


    /**
     * 设置 [RELATIONSHIPSTYPE]
     */
    public void setRelationshipstype(String  relationshipstype){
        this.relationshipstype = relationshipstype ;
        this.modify("relationshipstype",relationshipstype);
    }

    /**
     * 设置 [RELATIONSHIPSNAME]
     */
    public void setRelationshipsname(String  relationshipsname){
        this.relationshipsname = relationshipsname ;
        this.modify("relationshipsname",relationshipsname);
    }

    /**
     * 设置 [ENTITYNAME]
     */
    public void setEntityname(String  entityname){
        this.entityname = entityname ;
        this.modify("entityname",entityname);
    }

    /**
     * 设置 [ENTITY2NAME]
     */
    public void setEntity2name(String  entity2name){
        this.entity2name = entity2name ;
        this.modify("entity2name",entity2name);
    }

    /**
     * 设置 [ENTITY2ID]
     */
    public void setEntity2id(String  entity2id){
        this.entity2id = entity2id ;
        this.modify("entity2id",entity2id);
    }

    /**
     * 设置 [ENTITYID]
     */
    public void setEntityid(String  entityid){
        this.entityid = entityid ;
        this.modify("entityid",entityid);
    }


}

