package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.base.domain.Fax;
import cn.ibizlab.businesscentral.core.base.service.IFaxService;
import cn.ibizlab.businesscentral.core.base.filter.FaxSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"传真" })
@RestController("CentralApi-fax")
@RequestMapping("")
public class FaxResource {

    @Autowired
    public IFaxService faxService;

    @Autowired
    @Lazy
    public FaxMapping faxMapping;

    @PreAuthorize("hasPermission(this.faxMapping.toDomain(#faxdto),'iBizBusinessCentral-Fax-Create')")
    @ApiOperation(value = "新建传真", tags = {"传真" },  notes = "新建传真")
	@RequestMapping(method = RequestMethod.POST, value = "/faxes")
    public ResponseEntity<FaxDTO> create(@RequestBody FaxDTO faxdto) {
        Fax domain = faxMapping.toDomain(faxdto);
		faxService.create(domain);
        FaxDTO dto = faxMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.faxMapping.toDomain(#faxdtos),'iBizBusinessCentral-Fax-Create')")
    @ApiOperation(value = "批量新建传真", tags = {"传真" },  notes = "批量新建传真")
	@RequestMapping(method = RequestMethod.POST, value = "/faxes/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<FaxDTO> faxdtos) {
        faxService.createBatch(faxMapping.toDomain(faxdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "fax" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.faxService.get(#fax_id),'iBizBusinessCentral-Fax-Update')")
    @ApiOperation(value = "更新传真", tags = {"传真" },  notes = "更新传真")
	@RequestMapping(method = RequestMethod.PUT, value = "/faxes/{fax_id}")
    public ResponseEntity<FaxDTO> update(@PathVariable("fax_id") String fax_id, @RequestBody FaxDTO faxdto) {
		Fax domain  = faxMapping.toDomain(faxdto);
        domain .setActivityid(fax_id);
		faxService.update(domain );
		FaxDTO dto = faxMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.faxService.getFaxByEntities(this.faxMapping.toDomain(#faxdtos)),'iBizBusinessCentral-Fax-Update')")
    @ApiOperation(value = "批量更新传真", tags = {"传真" },  notes = "批量更新传真")
	@RequestMapping(method = RequestMethod.PUT, value = "/faxes/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<FaxDTO> faxdtos) {
        faxService.updateBatch(faxMapping.toDomain(faxdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.faxService.get(#fax_id),'iBizBusinessCentral-Fax-Remove')")
    @ApiOperation(value = "删除传真", tags = {"传真" },  notes = "删除传真")
	@RequestMapping(method = RequestMethod.DELETE, value = "/faxes/{fax_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("fax_id") String fax_id) {
         return ResponseEntity.status(HttpStatus.OK).body(faxService.remove(fax_id));
    }

    @PreAuthorize("hasPermission(this.faxService.getFaxByIds(#ids),'iBizBusinessCentral-Fax-Remove')")
    @ApiOperation(value = "批量删除传真", tags = {"传真" },  notes = "批量删除传真")
	@RequestMapping(method = RequestMethod.DELETE, value = "/faxes/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        faxService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.faxMapping.toDomain(returnObject.body),'iBizBusinessCentral-Fax-Get')")
    @ApiOperation(value = "获取传真", tags = {"传真" },  notes = "获取传真")
	@RequestMapping(method = RequestMethod.GET, value = "/faxes/{fax_id}")
    public ResponseEntity<FaxDTO> get(@PathVariable("fax_id") String fax_id) {
        Fax domain = faxService.get(fax_id);
        FaxDTO dto = faxMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取传真草稿", tags = {"传真" },  notes = "获取传真草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/faxes/getdraft")
    public ResponseEntity<FaxDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(faxMapping.toDto(faxService.getDraft(new Fax())));
    }

    @ApiOperation(value = "检查传真", tags = {"传真" },  notes = "检查传真")
	@RequestMapping(method = RequestMethod.POST, value = "/faxes/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody FaxDTO faxdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(faxService.checkKey(faxMapping.toDomain(faxdto)));
    }

    @PreAuthorize("hasPermission(this.faxMapping.toDomain(#faxdto),'iBizBusinessCentral-Fax-Save')")
    @ApiOperation(value = "保存传真", tags = {"传真" },  notes = "保存传真")
	@RequestMapping(method = RequestMethod.POST, value = "/faxes/save")
    public ResponseEntity<Boolean> save(@RequestBody FaxDTO faxdto) {
        return ResponseEntity.status(HttpStatus.OK).body(faxService.save(faxMapping.toDomain(faxdto)));
    }

    @PreAuthorize("hasPermission(this.faxMapping.toDomain(#faxdtos),'iBizBusinessCentral-Fax-Save')")
    @ApiOperation(value = "批量保存传真", tags = {"传真" },  notes = "批量保存传真")
	@RequestMapping(method = RequestMethod.POST, value = "/faxes/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<FaxDTO> faxdtos) {
        faxService.saveBatch(faxMapping.toDomain(faxdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Fax-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Fax-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"传真" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/faxes/fetchdefault")
	public ResponseEntity<List<FaxDTO>> fetchDefault(FaxSearchContext context) {
        Page<Fax> domains = faxService.searchDefault(context) ;
        List<FaxDTO> list = faxMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Fax-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Fax-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"传真" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/faxes/searchdefault")
	public ResponseEntity<Page<FaxDTO>> searchDefault(@RequestBody FaxSearchContext context) {
        Page<Fax> domains = faxService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(faxMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

