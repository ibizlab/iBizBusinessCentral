package cn.ibizlab.businesscentral.centralapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[ProductPriceLevelDTO]
 */
@Data
public class ProductPriceLevelDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [IMPORTSEQUENCENUMBER]
     *
     */
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;

    /**
     * 属性 [ROUNDINGOPTIONAMOUNT]
     *
     */
    @JSONField(name = "roundingoptionamount")
    @JsonProperty("roundingoptionamount")
    private BigDecimal roundingoptionamount;

    /**
     * 属性 [VERSIONNUMBER]
     *
     */
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;

    /**
     * 属性 [AMOUNT_BASE]
     *
     */
    @JSONField(name = "amount_base")
    @JsonProperty("amount_base")
    private BigDecimal amountBase;

    /**
     * 属性 [PROCESSID]
     *
     */
    @JSONField(name = "processid")
    @JsonProperty("processid")
    private String processid;

    /**
     * 属性 [AMOUNT]
     *
     */
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private BigDecimal amount;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [ROUNDINGOPTIONAMOUNT_BASE]
     *
     */
    @JSONField(name = "roundingoptionamount_base")
    @JsonProperty("roundingoptionamount_base")
    private BigDecimal roundingoptionamountBase;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;

    /**
     * 属性 [OVERRIDDENCREATEDON]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;

    /**
     * 属性 [EXCHANGERATE]
     *
     */
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;

    /**
     * 属性 [ORGANIZATIONID]
     *
     */
    @JSONField(name = "organizationid")
    @JsonProperty("organizationid")
    private String organizationid;

    /**
     * 属性 [PERCENTAGE]
     *
     */
    @JSONField(name = "percentage")
    @JsonProperty("percentage")
    private BigDecimal percentage;

    /**
     * 属性 [TRAVERSEDPATH]
     *
     */
    @JSONField(name = "traversedpath")
    @JsonProperty("traversedpath")
    private String traversedpath;

    /**
     * 属性 [PRODUCTNUMBER]
     *
     */
    @JSONField(name = "productnumber")
    @JsonProperty("productnumber")
    private String productnumber;

    /**
     * 属性 [PRICINGMETHODCODE]
     *
     */
    @JSONField(name = "pricingmethodcode")
    @JsonProperty("pricingmethodcode")
    private String pricingmethodcode;

    /**
     * 属性 [ROUNDINGOPTIONCODE]
     *
     */
    @JSONField(name = "roundingoptioncode")
    @JsonProperty("roundingoptioncode")
    private String roundingoptioncode;

    /**
     * 属性 [TIMEZONERULEVERSIONNUMBER]
     *
     */
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;

    /**
     * 属性 [UTCCONVERSIONTIMEZONECODE]
     *
     */
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;

    /**
     * 属性 [PRODUCTPRICELEVELID]
     *
     */
    @JSONField(name = "productpricelevelid")
    @JsonProperty("productpricelevelid")
    private String productpricelevelid;

    /**
     * 属性 [QUANTITYSELLINGCODE]
     *
     */
    @JSONField(name = "quantitysellingcode")
    @JsonProperty("quantitysellingcode")
    private String quantitysellingcode;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [ROUNDINGPOLICYCODE]
     *
     */
    @JSONField(name = "roundingpolicycode")
    @JsonProperty("roundingpolicycode")
    private String roundingpolicycode;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;

    /**
     * 属性 [STAGEID]
     *
     */
    @JSONField(name = "stageid")
    @JsonProperty("stageid")
    private String stageid;

    /**
     * 属性 [UOMSCHEDULENAME]
     *
     */
    @JSONField(name = "uomschedulename")
    @JsonProperty("uomschedulename")
    private String uomschedulename;

    /**
     * 属性 [PRODUCTNAME]
     *
     */
    @JSONField(name = "productname")
    @JsonProperty("productname")
    private String productname;

    /**
     * 属性 [CURRENCYNAME]
     *
     */
    @JSONField(name = "currencyname")
    @JsonProperty("currencyname")
    private String currencyname;

    /**
     * 属性 [UOMNAME]
     *
     */
    @JSONField(name = "uomname")
    @JsonProperty("uomname")
    private String uomname;

    /**
     * 属性 [DISCOUNTTYPENAME]
     *
     */
    @JSONField(name = "discounttypename")
    @JsonProperty("discounttypename")
    private String discounttypename;

    /**
     * 属性 [PRICELEVELNAME]
     *
     */
    @JSONField(name = "pricelevelname")
    @JsonProperty("pricelevelname")
    private String pricelevelname;

    /**
     * 属性 [PRODUCTID]
     *
     */
    @JSONField(name = "productid")
    @JsonProperty("productid")
    private String productid;

    /**
     * 属性 [UOMSCHEDULEID]
     *
     */
    @JSONField(name = "uomscheduleid")
    @JsonProperty("uomscheduleid")
    private String uomscheduleid;

    /**
     * 属性 [TRANSACTIONCURRENCYID]
     *
     */
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;

    /**
     * 属性 [UOMID]
     *
     */
    @JSONField(name = "uomid")
    @JsonProperty("uomid")
    private String uomid;

    /**
     * 属性 [DISCOUNTTYPEID]
     *
     */
    @JSONField(name = "discounttypeid")
    @JsonProperty("discounttypeid")
    private String discounttypeid;

    /**
     * 属性 [PRICELEVELID]
     *
     */
    @JSONField(name = "pricelevelid")
    @JsonProperty("pricelevelid")
    private String pricelevelid;


    /**
     * 设置 [IMPORTSEQUENCENUMBER]
     */
    public void setImportsequencenumber(Integer  importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [ROUNDINGOPTIONAMOUNT]
     */
    public void setRoundingoptionamount(BigDecimal  roundingoptionamount){
        this.roundingoptionamount = roundingoptionamount ;
        this.modify("roundingoptionamount",roundingoptionamount);
    }

    /**
     * 设置 [VERSIONNUMBER]
     */
    public void setVersionnumber(BigInteger  versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [AMOUNT_BASE]
     */
    public void setAmountBase(BigDecimal  amountBase){
        this.amountBase = amountBase ;
        this.modify("amount_base",amountBase);
    }

    /**
     * 设置 [PROCESSID]
     */
    public void setProcessid(String  processid){
        this.processid = processid ;
        this.modify("processid",processid);
    }

    /**
     * 设置 [AMOUNT]
     */
    public void setAmount(BigDecimal  amount){
        this.amount = amount ;
        this.modify("amount",amount);
    }

    /**
     * 设置 [ROUNDINGOPTIONAMOUNT_BASE]
     */
    public void setRoundingoptionamountBase(BigDecimal  roundingoptionamountBase){
        this.roundingoptionamountBase = roundingoptionamountBase ;
        this.modify("roundingoptionamount_base",roundingoptionamountBase);
    }

    /**
     * 设置 [OVERRIDDENCREATEDON]
     */
    public void setOverriddencreatedon(Timestamp  overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 设置 [EXCHANGERATE]
     */
    public void setExchangerate(BigDecimal  exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [ORGANIZATIONID]
     */
    public void setOrganizationid(String  organizationid){
        this.organizationid = organizationid ;
        this.modify("organizationid",organizationid);
    }

    /**
     * 设置 [PERCENTAGE]
     */
    public void setPercentage(BigDecimal  percentage){
        this.percentage = percentage ;
        this.modify("percentage",percentage);
    }

    /**
     * 设置 [TRAVERSEDPATH]
     */
    public void setTraversedpath(String  traversedpath){
        this.traversedpath = traversedpath ;
        this.modify("traversedpath",traversedpath);
    }

    /**
     * 设置 [PRODUCTNUMBER]
     */
    public void setProductnumber(String  productnumber){
        this.productnumber = productnumber ;
        this.modify("productnumber",productnumber);
    }

    /**
     * 设置 [PRICINGMETHODCODE]
     */
    public void setPricingmethodcode(String  pricingmethodcode){
        this.pricingmethodcode = pricingmethodcode ;
        this.modify("pricingmethodcode",pricingmethodcode);
    }

    /**
     * 设置 [ROUNDINGOPTIONCODE]
     */
    public void setRoundingoptioncode(String  roundingoptioncode){
        this.roundingoptioncode = roundingoptioncode ;
        this.modify("roundingoptioncode",roundingoptioncode);
    }

    /**
     * 设置 [TIMEZONERULEVERSIONNUMBER]
     */
    public void setTimezoneruleversionnumber(Integer  timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [UTCCONVERSIONTIMEZONECODE]
     */
    public void setUtcconversiontimezonecode(Integer  utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [QUANTITYSELLINGCODE]
     */
    public void setQuantitysellingcode(String  quantitysellingcode){
        this.quantitysellingcode = quantitysellingcode ;
        this.modify("quantitysellingcode",quantitysellingcode);
    }

    /**
     * 设置 [ROUNDINGPOLICYCODE]
     */
    public void setRoundingpolicycode(String  roundingpolicycode){
        this.roundingpolicycode = roundingpolicycode ;
        this.modify("roundingpolicycode",roundingpolicycode);
    }

    /**
     * 设置 [STAGEID]
     */
    public void setStageid(String  stageid){
        this.stageid = stageid ;
        this.modify("stageid",stageid);
    }

    /**
     * 设置 [UOMSCHEDULENAME]
     */
    public void setUomschedulename(String  uomschedulename){
        this.uomschedulename = uomschedulename ;
        this.modify("uomschedulename",uomschedulename);
    }

    /**
     * 设置 [PRODUCTNAME]
     */
    public void setProductname(String  productname){
        this.productname = productname ;
        this.modify("productname",productname);
    }

    /**
     * 设置 [CURRENCYNAME]
     */
    public void setCurrencyname(String  currencyname){
        this.currencyname = currencyname ;
        this.modify("currencyname",currencyname);
    }

    /**
     * 设置 [UOMNAME]
     */
    public void setUomname(String  uomname){
        this.uomname = uomname ;
        this.modify("uomname",uomname);
    }

    /**
     * 设置 [DISCOUNTTYPENAME]
     */
    public void setDiscounttypename(String  discounttypename){
        this.discounttypename = discounttypename ;
        this.modify("discounttypename",discounttypename);
    }

    /**
     * 设置 [PRICELEVELNAME]
     */
    public void setPricelevelname(String  pricelevelname){
        this.pricelevelname = pricelevelname ;
        this.modify("pricelevelname",pricelevelname);
    }

    /**
     * 设置 [PRODUCTID]
     */
    public void setProductid(String  productid){
        this.productid = productid ;
        this.modify("productid",productid);
    }

    /**
     * 设置 [UOMSCHEDULEID]
     */
    public void setUomscheduleid(String  uomscheduleid){
        this.uomscheduleid = uomscheduleid ;
        this.modify("uomscheduleid",uomscheduleid);
    }

    /**
     * 设置 [TRANSACTIONCURRENCYID]
     */
    public void setTransactioncurrencyid(String  transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }

    /**
     * 设置 [UOMID]
     */
    public void setUomid(String  uomid){
        this.uomid = uomid ;
        this.modify("uomid",uomid);
    }

    /**
     * 设置 [DISCOUNTTYPEID]
     */
    public void setDiscounttypeid(String  discounttypeid){
        this.discounttypeid = discounttypeid ;
        this.modify("discounttypeid",discounttypeid);
    }

    /**
     * 设置 [PRICELEVELID]
     */
    public void setPricelevelid(String  pricelevelid){
        this.pricelevelid = pricelevelid ;
        this.modify("pricelevelid",pricelevelid);
    }


}

