package cn.ibizlab.businesscentral.centralapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[GoalDTO]
 */
@Data
public class GoalDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [INPROGRESSDECIMAL]
     *
     */
    @JSONField(name = "inprogressdecimal")
    @JsonProperty("inprogressdecimal")
    private BigDecimal inprogressdecimal;

    /**
     * 属性 [GOALID]
     *
     */
    @JSONField(name = "goalid")
    @JsonProperty("goalid")
    private String goalid;

    /**
     * 属性 [OWNERTYPE]
     *
     */
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;

    /**
     * 属性 [STRETCHTARGETMONEY]
     *
     */
    @JSONField(name = "stretchtargetmoney")
    @JsonProperty("stretchtargetmoney")
    private BigDecimal stretchtargetmoney;

    /**
     * 属性 [ACTUALDECIMAL]
     *
     */
    @JSONField(name = "actualdecimal")
    @JsonProperty("actualdecimal")
    private BigDecimal actualdecimal;

    /**
     * 属性 [COMPUTEDTARGETASOFTODAYINTEGER]
     *
     */
    @JSONField(name = "computedtargetasoftodayinteger")
    @JsonProperty("computedtargetasoftodayinteger")
    private Integer computedtargetasoftodayinteger;

    /**
     * 属性 [TIMEZONERULEVERSIONNUMBER]
     *
     */
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;

    /**
     * 属性 [EXCHANGERATE]
     *
     */
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;

    /**
     * 属性 [ENTITYIMAGEID]
     *
     */
    @JSONField(name = "entityimageid")
    @JsonProperty("entityimageid")
    private String entityimageid;

    /**
     * 属性 [TARGETINTEGER]
     *
     */
    @JSONField(name = "targetinteger")
    @JsonProperty("targetinteger")
    private Integer targetinteger;

    /**
     * 属性 [CUSTOMROLLUPFIELDMONEY_BASE]
     *
     */
    @JSONField(name = "customrollupfieldmoney_base")
    @JsonProperty("customrollupfieldmoney_base")
    private BigDecimal customrollupfieldmoneyBase;

    /**
     * 属性 [ENTITYIMAGE_URL]
     *
     */
    @JSONField(name = "entityimage_url")
    @JsonProperty("entityimage_url")
    private String entityimageUrl;

    /**
     * 属性 [ACTUALMONEY_BASE]
     *
     */
    @JSONField(name = "actualmoney_base")
    @JsonProperty("actualmoney_base")
    private BigDecimal actualmoneyBase;

    /**
     * 属性 [UTCCONVERSIONTIMEZONECODE]
     *
     */
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;

    /**
     * 属性 [INPROGRESSINTEGER]
     *
     */
    @JSONField(name = "inprogressinteger")
    @JsonProperty("inprogressinteger")
    private Integer inprogressinteger;

    /**
     * 属性 [STATECODE]
     *
     */
    @JSONField(name = "statecode")
    @JsonProperty("statecode")
    private Integer statecode;

    /**
     * 属性 [TARGETMONEY_BASE]
     *
     */
    @JSONField(name = "targetmoney_base")
    @JsonProperty("targetmoney_base")
    private BigDecimal targetmoneyBase;

    /**
     * 属性 [INPROGRESSSTRING]
     *
     */
    @JSONField(name = "inprogressstring")
    @JsonProperty("inprogressstring")
    private String inprogressstring;

    /**
     * 属性 [ENTITYIMAGE_TIMESTAMP]
     *
     */
    @JSONField(name = "entityimage_timestamp")
    @JsonProperty("entityimage_timestamp")
    private BigInteger entityimageTimestamp;

    /**
     * 属性 [GOALSTARTDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "goalstartdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("goalstartdate")
    private Timestamp goalstartdate;

    /**
     * 属性 [PERCENTAGE]
     *
     */
    @JSONField(name = "percentage")
    @JsonProperty("percentage")
    private BigDecimal percentage;

    /**
     * 属性 [OWNERID]
     *
     */
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;

    /**
     * 属性 [TARGETDECIMAL]
     *
     */
    @JSONField(name = "targetdecimal")
    @JsonProperty("targetdecimal")
    private BigDecimal targetdecimal;

    /**
     * 属性 [AMOUNT]
     *
     */
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private Integer amount;

    /**
     * 属性 [STATUSCODE]
     *
     */
    @JSONField(name = "statuscode")
    @JsonProperty("statuscode")
    private Integer statuscode;

    /**
     * 属性 [AMOUNTDATATYPE]
     *
     */
    @JSONField(name = "amountdatatype")
    @JsonProperty("amountdatatype")
    private String amountdatatype;

    /**
     * 属性 [CUSTOMROLLUPFIELDINTEGER]
     *
     */
    @JSONField(name = "customrollupfieldinteger")
    @JsonProperty("customrollupfieldinteger")
    private Integer customrollupfieldinteger;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [GOALOWNERNAME]
     *
     */
    @JSONField(name = "goalownername")
    @JsonProperty("goalownername")
    private String goalownername;

    /**
     * 属性 [INPROGRESSMONEY]
     *
     */
    @JSONField(name = "inprogressmoney")
    @JsonProperty("inprogressmoney")
    private BigDecimal inprogressmoney;

    /**
     * 属性 [FISCALYEAR]
     *
     */
    @JSONField(name = "fiscalyear")
    @JsonProperty("fiscalyear")
    private String fiscalyear;

    /**
     * 属性 [FISCALPERIOD]
     *
     */
    @JSONField(name = "fiscalperiod")
    @JsonProperty("fiscalperiod")
    private String fiscalperiod;

    /**
     * 属性 [TITLE]
     *
     */
    @JSONField(name = "title")
    @JsonProperty("title")
    private String title;

    /**
     * 属性 [STRETCHTARGETDECIMAL]
     *
     */
    @JSONField(name = "stretchtargetdecimal")
    @JsonProperty("stretchtargetdecimal")
    private BigDecimal stretchtargetdecimal;

    /**
     * 属性 [CUSTOMROLLUPFIELDDECIMAL]
     *
     */
    @JSONField(name = "customrollupfielddecimal")
    @JsonProperty("customrollupfielddecimal")
    private BigDecimal customrollupfielddecimal;

    /**
     * 属性 [CONSIDERONLYGOALOWNERSRECORDS]
     *
     */
    @JSONField(name = "consideronlygoalownersrecords")
    @JsonProperty("consideronlygoalownersrecords")
    private Integer consideronlygoalownersrecords;

    /**
     * 属性 [OVERRIDDENCREATEDON]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;

    /**
     * 属性 [ACTUALMONEY]
     *
     */
    @JSONField(name = "actualmoney")
    @JsonProperty("actualmoney")
    private BigDecimal actualmoney;

    /**
     * 属性 [OWNERNAME]
     *
     */
    @JSONField(name = "ownername")
    @JsonProperty("ownername")
    private String ownername;

    /**
     * 属性 [ROLLUPONLYFROMCHILDGOALS]
     *
     */
    @JSONField(name = "rolluponlyfromchildgoals")
    @JsonProperty("rolluponlyfromchildgoals")
    private Integer rolluponlyfromchildgoals;

    /**
     * 属性 [GOALENDDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "goalenddate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("goalenddate")
    private Timestamp goalenddate;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [CUSTOMROLLUPFIELDMONEY]
     *
     */
    @JSONField(name = "customrollupfieldmoney")
    @JsonProperty("customrollupfieldmoney")
    private BigDecimal customrollupfieldmoney;

    /**
     * 属性 [ROLLUPERRORCODE]
     *
     */
    @JSONField(name = "rolluperrorcode")
    @JsonProperty("rolluperrorcode")
    private Integer rolluperrorcode;

    /**
     * 属性 [VERSIONNUMBER]
     *
     */
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;

    /**
     * 属性 [STRETCHTARGETMONEY_BASE]
     *
     */
    @JSONField(name = "stretchtargetmoney_base")
    @JsonProperty("stretchtargetmoney_base")
    private BigDecimal stretchtargetmoneyBase;

    /**
     * 属性 [TREEID]
     *
     */
    @JSONField(name = "treeid")
    @JsonProperty("treeid")
    private String treeid;

    /**
     * 属性 [GOALOWNERTYPE]
     *
     */
    @JSONField(name = "goalownertype")
    @JsonProperty("goalownertype")
    private String goalownertype;

    /**
     * 属性 [ENTITYIMAGE]
     *
     */
    @JSONField(name = "entityimage")
    @JsonProperty("entityimage")
    private String entityimage;

    /**
     * 属性 [STRETCHTARGETSTRING]
     *
     */
    @JSONField(name = "stretchtargetstring")
    @JsonProperty("stretchtargetstring")
    private String stretchtargetstring;

    /**
     * 属性 [STRETCHTARGETINTEGER]
     *
     */
    @JSONField(name = "stretchtargetinteger")
    @JsonProperty("stretchtargetinteger")
    private Integer stretchtargetinteger;

    /**
     * 属性 [FISCALPERIODGOAL]
     *
     */
    @JSONField(name = "fiscalperiodgoal")
    @JsonProperty("fiscalperiodgoal")
    private Integer fiscalperiodgoal;

    /**
     * 属性 [INPROGRESSMONEY_BASE]
     *
     */
    @JSONField(name = "inprogressmoney_base")
    @JsonProperty("inprogressmoney_base")
    private BigDecimal inprogressmoneyBase;

    /**
     * 属性 [OVERRIDDEN]
     *
     */
    @JSONField(name = "overridden")
    @JsonProperty("overridden")
    private Integer overridden;

    /**
     * 属性 [ACTUALSTRING]
     *
     */
    @JSONField(name = "actualstring")
    @JsonProperty("actualstring")
    private String actualstring;

    /**
     * 属性 [ACTUALINTEGER]
     *
     */
    @JSONField(name = "actualinteger")
    @JsonProperty("actualinteger")
    private Integer actualinteger;

    /**
     * 属性 [IMPORTSEQUENCENUMBER]
     *
     */
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;

    /**
     * 属性 [GOALOWNERID]
     *
     */
    @JSONField(name = "goalownerid")
    @JsonProperty("goalownerid")
    private String goalownerid;

    /**
     * 属性 [COMPUTEDTARGETASOFTODAYDECIMAL]
     *
     */
    @JSONField(name = "computedtargetasoftodaydecimal")
    @JsonProperty("computedtargetasoftodaydecimal")
    private BigDecimal computedtargetasoftodaydecimal;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;

    /**
     * 属性 [TARGETSTRING]
     *
     */
    @JSONField(name = "targetstring")
    @JsonProperty("targetstring")
    private String targetstring;

    /**
     * 属性 [CUSTOMROLLUPFIELDSTRING]
     *
     */
    @JSONField(name = "customrollupfieldstring")
    @JsonProperty("customrollupfieldstring")
    private String customrollupfieldstring;

    /**
     * 属性 [TARGETMONEY]
     *
     */
    @JSONField(name = "targetmoney")
    @JsonProperty("targetmoney")
    private BigDecimal targetmoney;

    /**
     * 属性 [LASTROLLEDUPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "lastrolledupdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("lastrolledupdate")
    private Timestamp lastrolledupdate;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;

    /**
     * 属性 [OVERRIDE]
     *
     */
    @JSONField(name = "override")
    @JsonProperty("override")
    private Integer override;

    /**
     * 属性 [COMPUTEDTARGETASOFTODAYMONEY]
     *
     */
    @JSONField(name = "computedtargetasoftodaymoney")
    @JsonProperty("computedtargetasoftodaymoney")
    private BigDecimal computedtargetasoftodaymoney;

    /**
     * 属性 [METRICNAME]
     *
     */
    @JSONField(name = "metricname")
    @JsonProperty("metricname")
    private String metricname;

    /**
     * 属性 [PARENTGOALNAME]
     *
     */
    @JSONField(name = "parentgoalname")
    @JsonProperty("parentgoalname")
    private String parentgoalname;

    /**
     * 属性 [PARENTGOALID]
     *
     */
    @JSONField(name = "parentgoalid")
    @JsonProperty("parentgoalid")
    private String parentgoalid;

    /**
     * 属性 [ROLLUPQUERYINPROGRESSMONEYID]
     *
     */
    @JSONField(name = "rollupqueryinprogressmoneyid")
    @JsonProperty("rollupqueryinprogressmoneyid")
    private String rollupqueryinprogressmoneyid;

    /**
     * 属性 [ROLLUPQUERYCUSTOMMONEYID]
     *
     */
    @JSONField(name = "rollupquerycustommoneyid")
    @JsonProperty("rollupquerycustommoneyid")
    private String rollupquerycustommoneyid;

    /**
     * 属性 [ROLLUPQUERYINPROGRESSINTEGERID]
     *
     */
    @JSONField(name = "rollupqueryinprogressintegerid")
    @JsonProperty("rollupqueryinprogressintegerid")
    private String rollupqueryinprogressintegerid;

    /**
     * 属性 [ROLLUPQUERYCUSTOMDECIMALID]
     *
     */
    @JSONField(name = "rollupquerycustomdecimalid")
    @JsonProperty("rollupquerycustomdecimalid")
    private String rollupquerycustomdecimalid;

    /**
     * 属性 [ROLLUPQUERYACTUALMONEYID]
     *
     */
    @JSONField(name = "rollupqueryactualmoneyid")
    @JsonProperty("rollupqueryactualmoneyid")
    private String rollupqueryactualmoneyid;

    /**
     * 属性 [ROLLUPQUERYCUSTOMINTEGERID]
     *
     */
    @JSONField(name = "rollupquerycustomintegerid")
    @JsonProperty("rollupquerycustomintegerid")
    private String rollupquerycustomintegerid;

    /**
     * 属性 [GOALWITHERRORID]
     *
     */
    @JSONField(name = "goalwitherrorid")
    @JsonProperty("goalwitherrorid")
    private String goalwitherrorid;

    /**
     * 属性 [METRICID]
     *
     */
    @JSONField(name = "metricid")
    @JsonProperty("metricid")
    private String metricid;

    /**
     * 属性 [ROLLUPQUERYINPROGRESSDECIMALID]
     *
     */
    @JSONField(name = "rollupqueryinprogressdecimalid")
    @JsonProperty("rollupqueryinprogressdecimalid")
    private String rollupqueryinprogressdecimalid;

    /**
     * 属性 [ROLLUPQUERYACTUALINTEGERID]
     *
     */
    @JSONField(name = "rollupqueryactualintegerid")
    @JsonProperty("rollupqueryactualintegerid")
    private String rollupqueryactualintegerid;

    /**
     * 属性 [ROLLUPQUERYACTUALDECIMALID]
     *
     */
    @JSONField(name = "rollupqueryactualdecimalid")
    @JsonProperty("rollupqueryactualdecimalid")
    private String rollupqueryactualdecimalid;


    /**
     * 设置 [INPROGRESSDECIMAL]
     */
    public void setInprogressdecimal(BigDecimal  inprogressdecimal){
        this.inprogressdecimal = inprogressdecimal ;
        this.modify("inprogressdecimal",inprogressdecimal);
    }

    /**
     * 设置 [OWNERTYPE]
     */
    public void setOwnertype(String  ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [STRETCHTARGETMONEY]
     */
    public void setStretchtargetmoney(BigDecimal  stretchtargetmoney){
        this.stretchtargetmoney = stretchtargetmoney ;
        this.modify("stretchtargetmoney",stretchtargetmoney);
    }

    /**
     * 设置 [ACTUALDECIMAL]
     */
    public void setActualdecimal(BigDecimal  actualdecimal){
        this.actualdecimal = actualdecimal ;
        this.modify("actualdecimal",actualdecimal);
    }

    /**
     * 设置 [COMPUTEDTARGETASOFTODAYINTEGER]
     */
    public void setComputedtargetasoftodayinteger(Integer  computedtargetasoftodayinteger){
        this.computedtargetasoftodayinteger = computedtargetasoftodayinteger ;
        this.modify("computedtargetasoftodayinteger",computedtargetasoftodayinteger);
    }

    /**
     * 设置 [TIMEZONERULEVERSIONNUMBER]
     */
    public void setTimezoneruleversionnumber(Integer  timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [EXCHANGERATE]
     */
    public void setExchangerate(BigDecimal  exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [ENTITYIMAGEID]
     */
    public void setEntityimageid(String  entityimageid){
        this.entityimageid = entityimageid ;
        this.modify("entityimageid",entityimageid);
    }

    /**
     * 设置 [TARGETINTEGER]
     */
    public void setTargetinteger(Integer  targetinteger){
        this.targetinteger = targetinteger ;
        this.modify("targetinteger",targetinteger);
    }

    /**
     * 设置 [CUSTOMROLLUPFIELDMONEY_BASE]
     */
    public void setCustomrollupfieldmoneyBase(BigDecimal  customrollupfieldmoneyBase){
        this.customrollupfieldmoneyBase = customrollupfieldmoneyBase ;
        this.modify("customrollupfieldmoney_base",customrollupfieldmoneyBase);
    }

    /**
     * 设置 [ENTITYIMAGE_URL]
     */
    public void setEntityimageUrl(String  entityimageUrl){
        this.entityimageUrl = entityimageUrl ;
        this.modify("entityimage_url",entityimageUrl);
    }

    /**
     * 设置 [ACTUALMONEY_BASE]
     */
    public void setActualmoneyBase(BigDecimal  actualmoneyBase){
        this.actualmoneyBase = actualmoneyBase ;
        this.modify("actualmoney_base",actualmoneyBase);
    }

    /**
     * 设置 [UTCCONVERSIONTIMEZONECODE]
     */
    public void setUtcconversiontimezonecode(Integer  utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [INPROGRESSINTEGER]
     */
    public void setInprogressinteger(Integer  inprogressinteger){
        this.inprogressinteger = inprogressinteger ;
        this.modify("inprogressinteger",inprogressinteger);
    }

    /**
     * 设置 [STATECODE]
     */
    public void setStatecode(Integer  statecode){
        this.statecode = statecode ;
        this.modify("statecode",statecode);
    }

    /**
     * 设置 [TARGETMONEY_BASE]
     */
    public void setTargetmoneyBase(BigDecimal  targetmoneyBase){
        this.targetmoneyBase = targetmoneyBase ;
        this.modify("targetmoney_base",targetmoneyBase);
    }

    /**
     * 设置 [INPROGRESSSTRING]
     */
    public void setInprogressstring(String  inprogressstring){
        this.inprogressstring = inprogressstring ;
        this.modify("inprogressstring",inprogressstring);
    }

    /**
     * 设置 [ENTITYIMAGE_TIMESTAMP]
     */
    public void setEntityimageTimestamp(BigInteger  entityimageTimestamp){
        this.entityimageTimestamp = entityimageTimestamp ;
        this.modify("entityimage_timestamp",entityimageTimestamp);
    }

    /**
     * 设置 [GOALSTARTDATE]
     */
    public void setGoalstartdate(Timestamp  goalstartdate){
        this.goalstartdate = goalstartdate ;
        this.modify("goalstartdate",goalstartdate);
    }

    /**
     * 设置 [PERCENTAGE]
     */
    public void setPercentage(BigDecimal  percentage){
        this.percentage = percentage ;
        this.modify("percentage",percentage);
    }

    /**
     * 设置 [OWNERID]
     */
    public void setOwnerid(String  ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [TARGETDECIMAL]
     */
    public void setTargetdecimal(BigDecimal  targetdecimal){
        this.targetdecimal = targetdecimal ;
        this.modify("targetdecimal",targetdecimal);
    }

    /**
     * 设置 [AMOUNT]
     */
    public void setAmount(Integer  amount){
        this.amount = amount ;
        this.modify("amount",amount);
    }

    /**
     * 设置 [STATUSCODE]
     */
    public void setStatuscode(Integer  statuscode){
        this.statuscode = statuscode ;
        this.modify("statuscode",statuscode);
    }

    /**
     * 设置 [AMOUNTDATATYPE]
     */
    public void setAmountdatatype(String  amountdatatype){
        this.amountdatatype = amountdatatype ;
        this.modify("amountdatatype",amountdatatype);
    }

    /**
     * 设置 [CUSTOMROLLUPFIELDINTEGER]
     */
    public void setCustomrollupfieldinteger(Integer  customrollupfieldinteger){
        this.customrollupfieldinteger = customrollupfieldinteger ;
        this.modify("customrollupfieldinteger",customrollupfieldinteger);
    }

    /**
     * 设置 [GOALOWNERNAME]
     */
    public void setGoalownername(String  goalownername){
        this.goalownername = goalownername ;
        this.modify("goalownername",goalownername);
    }

    /**
     * 设置 [INPROGRESSMONEY]
     */
    public void setInprogressmoney(BigDecimal  inprogressmoney){
        this.inprogressmoney = inprogressmoney ;
        this.modify("inprogressmoney",inprogressmoney);
    }

    /**
     * 设置 [FISCALYEAR]
     */
    public void setFiscalyear(String  fiscalyear){
        this.fiscalyear = fiscalyear ;
        this.modify("fiscalyear",fiscalyear);
    }

    /**
     * 设置 [FISCALPERIOD]
     */
    public void setFiscalperiod(String  fiscalperiod){
        this.fiscalperiod = fiscalperiod ;
        this.modify("fiscalperiod",fiscalperiod);
    }

    /**
     * 设置 [TITLE]
     */
    public void setTitle(String  title){
        this.title = title ;
        this.modify("title",title);
    }

    /**
     * 设置 [STRETCHTARGETDECIMAL]
     */
    public void setStretchtargetdecimal(BigDecimal  stretchtargetdecimal){
        this.stretchtargetdecimal = stretchtargetdecimal ;
        this.modify("stretchtargetdecimal",stretchtargetdecimal);
    }

    /**
     * 设置 [CUSTOMROLLUPFIELDDECIMAL]
     */
    public void setCustomrollupfielddecimal(BigDecimal  customrollupfielddecimal){
        this.customrollupfielddecimal = customrollupfielddecimal ;
        this.modify("customrollupfielddecimal",customrollupfielddecimal);
    }

    /**
     * 设置 [CONSIDERONLYGOALOWNERSRECORDS]
     */
    public void setConsideronlygoalownersrecords(Integer  consideronlygoalownersrecords){
        this.consideronlygoalownersrecords = consideronlygoalownersrecords ;
        this.modify("consideronlygoalownersrecords",consideronlygoalownersrecords);
    }

    /**
     * 设置 [OVERRIDDENCREATEDON]
     */
    public void setOverriddencreatedon(Timestamp  overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 设置 [ACTUALMONEY]
     */
    public void setActualmoney(BigDecimal  actualmoney){
        this.actualmoney = actualmoney ;
        this.modify("actualmoney",actualmoney);
    }

    /**
     * 设置 [OWNERNAME]
     */
    public void setOwnername(String  ownername){
        this.ownername = ownername ;
        this.modify("ownername",ownername);
    }

    /**
     * 设置 [ROLLUPONLYFROMCHILDGOALS]
     */
    public void setRolluponlyfromchildgoals(Integer  rolluponlyfromchildgoals){
        this.rolluponlyfromchildgoals = rolluponlyfromchildgoals ;
        this.modify("rolluponlyfromchildgoals",rolluponlyfromchildgoals);
    }

    /**
     * 设置 [GOALENDDATE]
     */
    public void setGoalenddate(Timestamp  goalenddate){
        this.goalenddate = goalenddate ;
        this.modify("goalenddate",goalenddate);
    }

    /**
     * 设置 [CUSTOMROLLUPFIELDMONEY]
     */
    public void setCustomrollupfieldmoney(BigDecimal  customrollupfieldmoney){
        this.customrollupfieldmoney = customrollupfieldmoney ;
        this.modify("customrollupfieldmoney",customrollupfieldmoney);
    }

    /**
     * 设置 [ROLLUPERRORCODE]
     */
    public void setRolluperrorcode(Integer  rolluperrorcode){
        this.rolluperrorcode = rolluperrorcode ;
        this.modify("rolluperrorcode",rolluperrorcode);
    }

    /**
     * 设置 [VERSIONNUMBER]
     */
    public void setVersionnumber(BigInteger  versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [STRETCHTARGETMONEY_BASE]
     */
    public void setStretchtargetmoneyBase(BigDecimal  stretchtargetmoneyBase){
        this.stretchtargetmoneyBase = stretchtargetmoneyBase ;
        this.modify("stretchtargetmoney_base",stretchtargetmoneyBase);
    }

    /**
     * 设置 [TREEID]
     */
    public void setTreeid(String  treeid){
        this.treeid = treeid ;
        this.modify("treeid",treeid);
    }

    /**
     * 设置 [GOALOWNERTYPE]
     */
    public void setGoalownertype(String  goalownertype){
        this.goalownertype = goalownertype ;
        this.modify("goalownertype",goalownertype);
    }

    /**
     * 设置 [ENTITYIMAGE]
     */
    public void setEntityimage(String  entityimage){
        this.entityimage = entityimage ;
        this.modify("entityimage",entityimage);
    }

    /**
     * 设置 [STRETCHTARGETSTRING]
     */
    public void setStretchtargetstring(String  stretchtargetstring){
        this.stretchtargetstring = stretchtargetstring ;
        this.modify("stretchtargetstring",stretchtargetstring);
    }

    /**
     * 设置 [STRETCHTARGETINTEGER]
     */
    public void setStretchtargetinteger(Integer  stretchtargetinteger){
        this.stretchtargetinteger = stretchtargetinteger ;
        this.modify("stretchtargetinteger",stretchtargetinteger);
    }

    /**
     * 设置 [FISCALPERIODGOAL]
     */
    public void setFiscalperiodgoal(Integer  fiscalperiodgoal){
        this.fiscalperiodgoal = fiscalperiodgoal ;
        this.modify("fiscalperiodgoal",fiscalperiodgoal);
    }

    /**
     * 设置 [INPROGRESSMONEY_BASE]
     */
    public void setInprogressmoneyBase(BigDecimal  inprogressmoneyBase){
        this.inprogressmoneyBase = inprogressmoneyBase ;
        this.modify("inprogressmoney_base",inprogressmoneyBase);
    }

    /**
     * 设置 [OVERRIDDEN]
     */
    public void setOverridden(Integer  overridden){
        this.overridden = overridden ;
        this.modify("overridden",overridden);
    }

    /**
     * 设置 [ACTUALSTRING]
     */
    public void setActualstring(String  actualstring){
        this.actualstring = actualstring ;
        this.modify("actualstring",actualstring);
    }

    /**
     * 设置 [ACTUALINTEGER]
     */
    public void setActualinteger(Integer  actualinteger){
        this.actualinteger = actualinteger ;
        this.modify("actualinteger",actualinteger);
    }

    /**
     * 设置 [IMPORTSEQUENCENUMBER]
     */
    public void setImportsequencenumber(Integer  importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [GOALOWNERID]
     */
    public void setGoalownerid(String  goalownerid){
        this.goalownerid = goalownerid ;
        this.modify("goalownerid",goalownerid);
    }

    /**
     * 设置 [COMPUTEDTARGETASOFTODAYDECIMAL]
     */
    public void setComputedtargetasoftodaydecimal(BigDecimal  computedtargetasoftodaydecimal){
        this.computedtargetasoftodaydecimal = computedtargetasoftodaydecimal ;
        this.modify("computedtargetasoftodaydecimal",computedtargetasoftodaydecimal);
    }

    /**
     * 设置 [TARGETSTRING]
     */
    public void setTargetstring(String  targetstring){
        this.targetstring = targetstring ;
        this.modify("targetstring",targetstring);
    }

    /**
     * 设置 [CUSTOMROLLUPFIELDSTRING]
     */
    public void setCustomrollupfieldstring(String  customrollupfieldstring){
        this.customrollupfieldstring = customrollupfieldstring ;
        this.modify("customrollupfieldstring",customrollupfieldstring);
    }

    /**
     * 设置 [TARGETMONEY]
     */
    public void setTargetmoney(BigDecimal  targetmoney){
        this.targetmoney = targetmoney ;
        this.modify("targetmoney",targetmoney);
    }

    /**
     * 设置 [LASTROLLEDUPDATE]
     */
    public void setLastrolledupdate(Timestamp  lastrolledupdate){
        this.lastrolledupdate = lastrolledupdate ;
        this.modify("lastrolledupdate",lastrolledupdate);
    }

    /**
     * 设置 [OVERRIDE]
     */
    public void setOverride(Integer  override){
        this.override = override ;
        this.modify("override",override);
    }

    /**
     * 设置 [COMPUTEDTARGETASOFTODAYMONEY]
     */
    public void setComputedtargetasoftodaymoney(BigDecimal  computedtargetasoftodaymoney){
        this.computedtargetasoftodaymoney = computedtargetasoftodaymoney ;
        this.modify("computedtargetasoftodaymoney",computedtargetasoftodaymoney);
    }

    /**
     * 设置 [METRICNAME]
     */
    public void setMetricname(String  metricname){
        this.metricname = metricname ;
        this.modify("metricname",metricname);
    }

    /**
     * 设置 [PARENTGOALNAME]
     */
    public void setParentgoalname(String  parentgoalname){
        this.parentgoalname = parentgoalname ;
        this.modify("parentgoalname",parentgoalname);
    }

    /**
     * 设置 [PARENTGOALID]
     */
    public void setParentgoalid(String  parentgoalid){
        this.parentgoalid = parentgoalid ;
        this.modify("parentgoalid",parentgoalid);
    }

    /**
     * 设置 [ROLLUPQUERYINPROGRESSMONEYID]
     */
    public void setRollupqueryinprogressmoneyid(String  rollupqueryinprogressmoneyid){
        this.rollupqueryinprogressmoneyid = rollupqueryinprogressmoneyid ;
        this.modify("rollupqueryinprogressmoneyid",rollupqueryinprogressmoneyid);
    }

    /**
     * 设置 [ROLLUPQUERYCUSTOMMONEYID]
     */
    public void setRollupquerycustommoneyid(String  rollupquerycustommoneyid){
        this.rollupquerycustommoneyid = rollupquerycustommoneyid ;
        this.modify("rollupquerycustommoneyid",rollupquerycustommoneyid);
    }

    /**
     * 设置 [ROLLUPQUERYINPROGRESSINTEGERID]
     */
    public void setRollupqueryinprogressintegerid(String  rollupqueryinprogressintegerid){
        this.rollupqueryinprogressintegerid = rollupqueryinprogressintegerid ;
        this.modify("rollupqueryinprogressintegerid",rollupqueryinprogressintegerid);
    }

    /**
     * 设置 [ROLLUPQUERYCUSTOMDECIMALID]
     */
    public void setRollupquerycustomdecimalid(String  rollupquerycustomdecimalid){
        this.rollupquerycustomdecimalid = rollupquerycustomdecimalid ;
        this.modify("rollupquerycustomdecimalid",rollupquerycustomdecimalid);
    }

    /**
     * 设置 [ROLLUPQUERYACTUALMONEYID]
     */
    public void setRollupqueryactualmoneyid(String  rollupqueryactualmoneyid){
        this.rollupqueryactualmoneyid = rollupqueryactualmoneyid ;
        this.modify("rollupqueryactualmoneyid",rollupqueryactualmoneyid);
    }

    /**
     * 设置 [ROLLUPQUERYCUSTOMINTEGERID]
     */
    public void setRollupquerycustomintegerid(String  rollupquerycustomintegerid){
        this.rollupquerycustomintegerid = rollupquerycustomintegerid ;
        this.modify("rollupquerycustomintegerid",rollupquerycustomintegerid);
    }

    /**
     * 设置 [GOALWITHERRORID]
     */
    public void setGoalwitherrorid(String  goalwitherrorid){
        this.goalwitherrorid = goalwitherrorid ;
        this.modify("goalwitherrorid",goalwitherrorid);
    }

    /**
     * 设置 [METRICID]
     */
    public void setMetricid(String  metricid){
        this.metricid = metricid ;
        this.modify("metricid",metricid);
    }

    /**
     * 设置 [ROLLUPQUERYINPROGRESSDECIMALID]
     */
    public void setRollupqueryinprogressdecimalid(String  rollupqueryinprogressdecimalid){
        this.rollupqueryinprogressdecimalid = rollupqueryinprogressdecimalid ;
        this.modify("rollupqueryinprogressdecimalid",rollupqueryinprogressdecimalid);
    }

    /**
     * 设置 [ROLLUPQUERYACTUALINTEGERID]
     */
    public void setRollupqueryactualintegerid(String  rollupqueryactualintegerid){
        this.rollupqueryactualintegerid = rollupqueryactualintegerid ;
        this.modify("rollupqueryactualintegerid",rollupqueryactualintegerid);
    }

    /**
     * 设置 [ROLLUPQUERYACTUALDECIMALID]
     */
    public void setRollupqueryactualdecimalid(String  rollupqueryactualdecimalid){
        this.rollupqueryactualdecimalid = rollupqueryactualdecimalid ;
        this.modify("rollupqueryactualdecimalid",rollupqueryactualdecimalid);
    }


}

