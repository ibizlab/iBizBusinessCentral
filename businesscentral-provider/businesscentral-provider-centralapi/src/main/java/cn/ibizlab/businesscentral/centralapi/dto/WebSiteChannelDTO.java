package cn.ibizlab.businesscentral.centralapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[WebSiteChannelDTO]
 */
@Data
public class WebSiteChannelDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [WEBSITECHANNELID]
     *
     */
    @JSONField(name = "websitechannelid")
    @JsonProperty("websitechannelid")
    private String websitechannelid;

    /**
     * 属性 [CHANNELTYPE]
     *
     */
    @JSONField(name = "channeltype")
    @JsonProperty("channeltype")
    private String channeltype;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;

    /**
     * 属性 [WEBSITECHANNELNAME]
     *
     */
    @JSONField(name = "websitechannelname")
    @JsonProperty("websitechannelname")
    private String websitechannelname;

    /**
     * 属性 [VALIDFLAG]
     *
     */
    @JSONField(name = "validflag")
    @JsonProperty("validflag")
    private Integer validflag;

    /**
     * 属性 [MEMO]
     *
     */
    @JSONField(name = "memo")
    @JsonProperty("memo")
    private String memo;

    /**
     * 属性 [SN]
     *
     */
    @JSONField(name = "sn")
    @JsonProperty("sn")
    private Integer sn;

    /**
     * 属性 [CHANNELCODE]
     *
     */
    @JSONField(name = "channelcode")
    @JsonProperty("channelcode")
    private String channelcode;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;

    /**
     * 属性 [WEBSITENAME]
     *
     */
    @JSONField(name = "websitename")
    @JsonProperty("websitename")
    private String websitename;

    /**
     * 属性 [PWEBSITECHANNELNAME]
     *
     */
    @JSONField(name = "pwebsitechannelname")
    @JsonProperty("pwebsitechannelname")
    private String pwebsitechannelname;

    /**
     * 属性 [WEBSITEID]
     *
     */
    @JSONField(name = "websiteid")
    @JsonProperty("websiteid")
    private String websiteid;

    /**
     * 属性 [PWEBSITECHANNELID]
     *
     */
    @JSONField(name = "pwebsitechannelid")
    @JsonProperty("pwebsitechannelid")
    private String pwebsitechannelid;


    /**
     * 设置 [CHANNELTYPE]
     */
    public void setChanneltype(String  channeltype){
        this.channeltype = channeltype ;
        this.modify("channeltype",channeltype);
    }

    /**
     * 设置 [WEBSITECHANNELNAME]
     */
    public void setWebsitechannelname(String  websitechannelname){
        this.websitechannelname = websitechannelname ;
        this.modify("websitechannelname",websitechannelname);
    }

    /**
     * 设置 [VALIDFLAG]
     */
    public void setValidflag(Integer  validflag){
        this.validflag = validflag ;
        this.modify("validflag",validflag);
    }

    /**
     * 设置 [MEMO]
     */
    public void setMemo(String  memo){
        this.memo = memo ;
        this.modify("memo",memo);
    }

    /**
     * 设置 [SN]
     */
    public void setSn(Integer  sn){
        this.sn = sn ;
        this.modify("sn",sn);
    }

    /**
     * 设置 [CHANNELCODE]
     */
    public void setChannelcode(String  channelcode){
        this.channelcode = channelcode ;
        this.modify("channelcode",channelcode);
    }

    /**
     * 设置 [WEBSITEID]
     */
    public void setWebsiteid(String  websiteid){
        this.websiteid = websiteid ;
        this.modify("websiteid",websiteid);
    }

    /**
     * 设置 [PWEBSITECHANNELID]
     */
    public void setPwebsitechannelid(String  pwebsitechannelid){
        this.pwebsitechannelid = pwebsitechannelid ;
        this.modify("pwebsitechannelid",pwebsitechannelid);
    }


}

