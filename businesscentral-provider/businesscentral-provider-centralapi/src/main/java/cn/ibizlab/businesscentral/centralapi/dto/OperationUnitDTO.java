package cn.ibizlab.businesscentral.centralapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[OperationUnitDTO]
 */
@Data
public class OperationUnitDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;

    /**
     * 属性 [OPERATIONUNITID]
     *
     */
    @JSONField(name = "operationunitid")
    @JsonProperty("operationunitid")
    private String operationunitid;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [OPERATIONUNITNAME]
     *
     */
    @JSONField(name = "operationunitname")
    @JsonProperty("operationunitname")
    private String operationunitname;

    /**
     * 属性 [ORGANIZATIONTYPE]
     *
     */
    @JSONField(name = "organizationtype")
    @JsonProperty("organizationtype")
    private String organizationtype;

    /**
     * 属性 [SHOWORDER]
     *
     */
    @JSONField(name = "showorder")
    @JsonProperty("showorder")
    private BigInteger showorder;

    /**
     * 属性 [ORGLEVEL]
     *
     */
    @JSONField(name = "orglevel")
    @JsonProperty("orglevel")
    private BigInteger orglevel;

    /**
     * 属性 [SHORTNAME]
     *
     */
    @JSONField(name = "shortname")
    @JsonProperty("shortname")
    private String shortname;

    /**
     * 属性 [ORGCODE]
     *
     */
    @JSONField(name = "orgcode")
    @JsonProperty("orgcode")
    private String orgcode;

    /**
     * 属性 [OPERATIONUNITTYPE]
     *
     */
    @JSONField(name = "operationunittype")
    @JsonProperty("operationunittype")
    private String operationunittype;


    /**
     * 设置 [OPERATIONUNITNAME]
     */
    public void setOperationunitname(String  operationunitname){
        this.operationunitname = operationunitname ;
        this.modify("operationunitname",operationunitname);
    }

    /**
     * 设置 [OPERATIONUNITTYPE]
     */
    public void setOperationunittype(String  operationunittype){
        this.operationunittype = operationunittype ;
        this.modify("operationunittype",operationunittype);
    }


}

