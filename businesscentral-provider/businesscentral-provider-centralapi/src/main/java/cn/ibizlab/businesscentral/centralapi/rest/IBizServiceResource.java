package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.service.domain.IBizService;
import cn.ibizlab.businesscentral.core.service.service.IIBizServiceService;
import cn.ibizlab.businesscentral.core.service.filter.IBizServiceSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"服务" })
@RestController("CentralApi-ibizservice")
@RequestMapping("")
public class IBizServiceResource {

    @Autowired
    public IIBizServiceService ibizserviceService;

    @Autowired
    @Lazy
    public IBizServiceMapping ibizserviceMapping;

    @PreAuthorize("hasPermission(this.ibizserviceMapping.toDomain(#ibizservicedto),'iBizBusinessCentral-IBizService-Create')")
    @ApiOperation(value = "新建服务", tags = {"服务" },  notes = "新建服务")
	@RequestMapping(method = RequestMethod.POST, value = "/ibizservices")
    public ResponseEntity<IBizServiceDTO> create(@RequestBody IBizServiceDTO ibizservicedto) {
        IBizService domain = ibizserviceMapping.toDomain(ibizservicedto);
		ibizserviceService.create(domain);
        IBizServiceDTO dto = ibizserviceMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.ibizserviceMapping.toDomain(#ibizservicedtos),'iBizBusinessCentral-IBizService-Create')")
    @ApiOperation(value = "批量新建服务", tags = {"服务" },  notes = "批量新建服务")
	@RequestMapping(method = RequestMethod.POST, value = "/ibizservices/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<IBizServiceDTO> ibizservicedtos) {
        ibizserviceService.createBatch(ibizserviceMapping.toDomain(ibizservicedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "ibizservice" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.ibizserviceService.get(#ibizservice_id),'iBizBusinessCentral-IBizService-Update')")
    @ApiOperation(value = "更新服务", tags = {"服务" },  notes = "更新服务")
	@RequestMapping(method = RequestMethod.PUT, value = "/ibizservices/{ibizservice_id}")
    public ResponseEntity<IBizServiceDTO> update(@PathVariable("ibizservice_id") String ibizservice_id, @RequestBody IBizServiceDTO ibizservicedto) {
		IBizService domain  = ibizserviceMapping.toDomain(ibizservicedto);
        domain .setServiceid(ibizservice_id);
		ibizserviceService.update(domain );
		IBizServiceDTO dto = ibizserviceMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.ibizserviceService.getIbizserviceByEntities(this.ibizserviceMapping.toDomain(#ibizservicedtos)),'iBizBusinessCentral-IBizService-Update')")
    @ApiOperation(value = "批量更新服务", tags = {"服务" },  notes = "批量更新服务")
	@RequestMapping(method = RequestMethod.PUT, value = "/ibizservices/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<IBizServiceDTO> ibizservicedtos) {
        ibizserviceService.updateBatch(ibizserviceMapping.toDomain(ibizservicedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.ibizserviceService.get(#ibizservice_id),'iBizBusinessCentral-IBizService-Remove')")
    @ApiOperation(value = "删除服务", tags = {"服务" },  notes = "删除服务")
	@RequestMapping(method = RequestMethod.DELETE, value = "/ibizservices/{ibizservice_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("ibizservice_id") String ibizservice_id) {
         return ResponseEntity.status(HttpStatus.OK).body(ibizserviceService.remove(ibizservice_id));
    }

    @PreAuthorize("hasPermission(this.ibizserviceService.getIbizserviceByIds(#ids),'iBizBusinessCentral-IBizService-Remove')")
    @ApiOperation(value = "批量删除服务", tags = {"服务" },  notes = "批量删除服务")
	@RequestMapping(method = RequestMethod.DELETE, value = "/ibizservices/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        ibizserviceService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.ibizserviceMapping.toDomain(returnObject.body),'iBizBusinessCentral-IBizService-Get')")
    @ApiOperation(value = "获取服务", tags = {"服务" },  notes = "获取服务")
	@RequestMapping(method = RequestMethod.GET, value = "/ibizservices/{ibizservice_id}")
    public ResponseEntity<IBizServiceDTO> get(@PathVariable("ibizservice_id") String ibizservice_id) {
        IBizService domain = ibizserviceService.get(ibizservice_id);
        IBizServiceDTO dto = ibizserviceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取服务草稿", tags = {"服务" },  notes = "获取服务草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/ibizservices/getdraft")
    public ResponseEntity<IBizServiceDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(ibizserviceMapping.toDto(ibizserviceService.getDraft(new IBizService())));
    }

    @ApiOperation(value = "检查服务", tags = {"服务" },  notes = "检查服务")
	@RequestMapping(method = RequestMethod.POST, value = "/ibizservices/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody IBizServiceDTO ibizservicedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(ibizserviceService.checkKey(ibizserviceMapping.toDomain(ibizservicedto)));
    }

    @PreAuthorize("hasPermission(this.ibizserviceMapping.toDomain(#ibizservicedto),'iBizBusinessCentral-IBizService-Save')")
    @ApiOperation(value = "保存服务", tags = {"服务" },  notes = "保存服务")
	@RequestMapping(method = RequestMethod.POST, value = "/ibizservices/save")
    public ResponseEntity<Boolean> save(@RequestBody IBizServiceDTO ibizservicedto) {
        return ResponseEntity.status(HttpStatus.OK).body(ibizserviceService.save(ibizserviceMapping.toDomain(ibizservicedto)));
    }

    @PreAuthorize("hasPermission(this.ibizserviceMapping.toDomain(#ibizservicedtos),'iBizBusinessCentral-IBizService-Save')")
    @ApiOperation(value = "批量保存服务", tags = {"服务" },  notes = "批量保存服务")
	@RequestMapping(method = RequestMethod.POST, value = "/ibizservices/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<IBizServiceDTO> ibizservicedtos) {
        ibizserviceService.saveBatch(ibizserviceMapping.toDomain(ibizservicedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-IBizService-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-IBizService-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"服务" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/ibizservices/fetchdefault")
	public ResponseEntity<List<IBizServiceDTO>> fetchDefault(IBizServiceSearchContext context) {
        Page<IBizService> domains = ibizserviceService.searchDefault(context) ;
        List<IBizServiceDTO> list = ibizserviceMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-IBizService-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-IBizService-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"服务" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/ibizservices/searchdefault")
	public ResponseEntity<Page<IBizServiceDTO>> searchDefault(@RequestBody IBizServiceSearchContext context) {
        Page<IBizService> domains = ibizserviceService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(ibizserviceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

