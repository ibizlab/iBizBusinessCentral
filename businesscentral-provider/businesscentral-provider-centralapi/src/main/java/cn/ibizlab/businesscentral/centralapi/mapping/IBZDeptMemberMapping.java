package cn.ibizlab.businesscentral.centralapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.ou.domain.IBZDeptMember;
import cn.ibizlab.businesscentral.centralapi.dto.IBZDeptMemberDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CentralApiIBZDeptMemberMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface IBZDeptMemberMapping extends MappingBase<IBZDeptMemberDTO, IBZDeptMember> {


}

