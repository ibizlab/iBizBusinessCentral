package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.finance.domain.Invoice;
import cn.ibizlab.businesscentral.core.finance.service.IInvoiceService;
import cn.ibizlab.businesscentral.core.finance.filter.InvoiceSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"发票" })
@RestController("CentralApi-invoice")
@RequestMapping("")
public class InvoiceResource {

    @Autowired
    public IInvoiceService invoiceService;

    @Autowired
    @Lazy
    public InvoiceMapping invoiceMapping;

    @PreAuthorize("hasPermission(this.invoiceMapping.toDomain(#invoicedto),'iBizBusinessCentral-Invoice-Create')")
    @ApiOperation(value = "新建发票", tags = {"发票" },  notes = "新建发票")
	@RequestMapping(method = RequestMethod.POST, value = "/invoices")
    public ResponseEntity<InvoiceDTO> create(@RequestBody InvoiceDTO invoicedto) {
        Invoice domain = invoiceMapping.toDomain(invoicedto);
		invoiceService.create(domain);
        InvoiceDTO dto = invoiceMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.invoiceMapping.toDomain(#invoicedtos),'iBizBusinessCentral-Invoice-Create')")
    @ApiOperation(value = "批量新建发票", tags = {"发票" },  notes = "批量新建发票")
	@RequestMapping(method = RequestMethod.POST, value = "/invoices/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<InvoiceDTO> invoicedtos) {
        invoiceService.createBatch(invoiceMapping.toDomain(invoicedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "invoice" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.invoiceService.get(#invoice_id),'iBizBusinessCentral-Invoice-Update')")
    @ApiOperation(value = "更新发票", tags = {"发票" },  notes = "更新发票")
	@RequestMapping(method = RequestMethod.PUT, value = "/invoices/{invoice_id}")
    public ResponseEntity<InvoiceDTO> update(@PathVariable("invoice_id") String invoice_id, @RequestBody InvoiceDTO invoicedto) {
		Invoice domain  = invoiceMapping.toDomain(invoicedto);
        domain .setInvoiceid(invoice_id);
		invoiceService.update(domain );
		InvoiceDTO dto = invoiceMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.invoiceService.getInvoiceByEntities(this.invoiceMapping.toDomain(#invoicedtos)),'iBizBusinessCentral-Invoice-Update')")
    @ApiOperation(value = "批量更新发票", tags = {"发票" },  notes = "批量更新发票")
	@RequestMapping(method = RequestMethod.PUT, value = "/invoices/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<InvoiceDTO> invoicedtos) {
        invoiceService.updateBatch(invoiceMapping.toDomain(invoicedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.invoiceService.get(#invoice_id),'iBizBusinessCentral-Invoice-Remove')")
    @ApiOperation(value = "删除发票", tags = {"发票" },  notes = "删除发票")
	@RequestMapping(method = RequestMethod.DELETE, value = "/invoices/{invoice_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("invoice_id") String invoice_id) {
         return ResponseEntity.status(HttpStatus.OK).body(invoiceService.remove(invoice_id));
    }

    @PreAuthorize("hasPermission(this.invoiceService.getInvoiceByIds(#ids),'iBizBusinessCentral-Invoice-Remove')")
    @ApiOperation(value = "批量删除发票", tags = {"发票" },  notes = "批量删除发票")
	@RequestMapping(method = RequestMethod.DELETE, value = "/invoices/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        invoiceService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.invoiceMapping.toDomain(returnObject.body),'iBizBusinessCentral-Invoice-Get')")
    @ApiOperation(value = "获取发票", tags = {"发票" },  notes = "获取发票")
	@RequestMapping(method = RequestMethod.GET, value = "/invoices/{invoice_id}")
    public ResponseEntity<InvoiceDTO> get(@PathVariable("invoice_id") String invoice_id) {
        Invoice domain = invoiceService.get(invoice_id);
        InvoiceDTO dto = invoiceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取发票草稿", tags = {"发票" },  notes = "获取发票草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/invoices/getdraft")
    public ResponseEntity<InvoiceDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(invoiceMapping.toDto(invoiceService.getDraft(new Invoice())));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-Cancel-all')")
    @ApiOperation(value = "取消发票", tags = {"发票" },  notes = "取消发票")
	@RequestMapping(method = RequestMethod.POST, value = "/invoices/{invoice_id}/cancel")
    public ResponseEntity<InvoiceDTO> cancel(@PathVariable("invoice_id") String invoice_id, @RequestBody InvoiceDTO invoicedto) {
        Invoice domain = invoiceMapping.toDomain(invoicedto);
domain.setInvoiceid(invoice_id);
        domain = invoiceService.cancel(domain);
        invoicedto = invoiceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(invoicedto);
    }

    @ApiOperation(value = "检查发票", tags = {"发票" },  notes = "检查发票")
	@RequestMapping(method = RequestMethod.POST, value = "/invoices/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody InvoiceDTO invoicedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(invoiceService.checkKey(invoiceMapping.toDomain(invoicedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-Finish-all')")
    @ApiOperation(value = "确认发票", tags = {"发票" },  notes = "确认发票")
	@RequestMapping(method = RequestMethod.POST, value = "/invoices/{invoice_id}/finish")
    public ResponseEntity<InvoiceDTO> finish(@PathVariable("invoice_id") String invoice_id, @RequestBody InvoiceDTO invoicedto) {
        Invoice domain = invoiceMapping.toDomain(invoicedto);
domain.setInvoiceid(invoice_id);
        domain = invoiceService.finish(domain);
        invoicedto = invoiceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(invoicedto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-Paid-all')")
    @ApiOperation(value = "发票已支付", tags = {"发票" },  notes = "发票已支付")
	@RequestMapping(method = RequestMethod.POST, value = "/invoices/{invoice_id}/paid")
    public ResponseEntity<InvoiceDTO> paid(@PathVariable("invoice_id") String invoice_id, @RequestBody InvoiceDTO invoicedto) {
        Invoice domain = invoiceMapping.toDomain(invoicedto);
domain.setInvoiceid(invoice_id);
        domain = invoiceService.paid(domain);
        invoicedto = invoiceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(invoicedto);
    }

    @PreAuthorize("hasPermission(this.invoiceMapping.toDomain(#invoicedto),'iBizBusinessCentral-Invoice-Save')")
    @ApiOperation(value = "保存发票", tags = {"发票" },  notes = "保存发票")
	@RequestMapping(method = RequestMethod.POST, value = "/invoices/save")
    public ResponseEntity<Boolean> save(@RequestBody InvoiceDTO invoicedto) {
        return ResponseEntity.status(HttpStatus.OK).body(invoiceService.save(invoiceMapping.toDomain(invoicedto)));
    }

    @PreAuthorize("hasPermission(this.invoiceMapping.toDomain(#invoicedtos),'iBizBusinessCentral-Invoice-Save')")
    @ApiOperation(value = "批量保存发票", tags = {"发票" },  notes = "批量保存发票")
	@RequestMapping(method = RequestMethod.POST, value = "/invoices/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<InvoiceDTO> invoicedtos) {
        invoiceService.saveBatch(invoiceMapping.toDomain(invoicedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchByParentKey-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "获取ByParentKey", tags = {"发票" } ,notes = "获取ByParentKey")
    @RequestMapping(method= RequestMethod.GET , value="/invoices/fetchbyparentkey")
	public ResponseEntity<List<InvoiceDTO>> fetchByParentKey(InvoiceSearchContext context) {
        Page<Invoice> domains = invoiceService.searchByParentKey(context) ;
        List<InvoiceDTO> list = invoiceMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchByParentKey-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "查询ByParentKey", tags = {"发票" } ,notes = "查询ByParentKey")
    @RequestMapping(method= RequestMethod.POST , value="/invoices/searchbyparentkey")
	public ResponseEntity<Page<InvoiceDTO>> searchByParentKey(@RequestBody InvoiceSearchContext context) {
        Page<Invoice> domains = invoiceService.searchByParentKey(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(invoiceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchCancel-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "获取已取消", tags = {"发票" } ,notes = "获取已取消")
    @RequestMapping(method= RequestMethod.GET , value="/invoices/fetchcancel")
	public ResponseEntity<List<InvoiceDTO>> fetchCancel(InvoiceSearchContext context) {
        Page<Invoice> domains = invoiceService.searchCancel(context) ;
        List<InvoiceDTO> list = invoiceMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchCancel-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "查询已取消", tags = {"发票" } ,notes = "查询已取消")
    @RequestMapping(method= RequestMethod.POST , value="/invoices/searchcancel")
	public ResponseEntity<Page<InvoiceDTO>> searchCancel(@RequestBody InvoiceSearchContext context) {
        Page<Invoice> domains = invoiceService.searchCancel(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(invoiceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"发票" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/invoices/fetchdefault")
	public ResponseEntity<List<InvoiceDTO>> fetchDefault(InvoiceSearchContext context) {
        Page<Invoice> domains = invoiceService.searchDefault(context) ;
        List<InvoiceDTO> list = invoiceMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"发票" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/invoices/searchdefault")
	public ResponseEntity<Page<InvoiceDTO>> searchDefault(@RequestBody InvoiceSearchContext context) {
        Page<Invoice> domains = invoiceService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(invoiceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchPaid-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "获取已支付", tags = {"发票" } ,notes = "获取已支付")
    @RequestMapping(method= RequestMethod.GET , value="/invoices/fetchpaid")
	public ResponseEntity<List<InvoiceDTO>> fetchPaid(InvoiceSearchContext context) {
        Page<Invoice> domains = invoiceService.searchPaid(context) ;
        List<InvoiceDTO> list = invoiceMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchPaid-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "查询已支付", tags = {"发票" } ,notes = "查询已支付")
    @RequestMapping(method= RequestMethod.POST , value="/invoices/searchpaid")
	public ResponseEntity<Page<InvoiceDTO>> searchPaid(@RequestBody InvoiceSearchContext context) {
        Page<Invoice> domains = invoiceService.searchPaid(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(invoiceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.invoiceMapping.toDomain(#invoicedto),'iBizBusinessCentral-Invoice-Create')")
    @ApiOperation(value = "根据订单建立发票", tags = {"发票" },  notes = "根据订单建立发票")
	@RequestMapping(method = RequestMethod.POST, value = "/salesorders/{salesorder_id}/invoices")
    public ResponseEntity<InvoiceDTO> createBySalesOrder(@PathVariable("salesorder_id") String salesorder_id, @RequestBody InvoiceDTO invoicedto) {
        Invoice domain = invoiceMapping.toDomain(invoicedto);
        domain.setSalesorderid(salesorder_id);
		invoiceService.create(domain);
        InvoiceDTO dto = invoiceMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.invoiceMapping.toDomain(#invoicedtos),'iBizBusinessCentral-Invoice-Create')")
    @ApiOperation(value = "根据订单批量建立发票", tags = {"发票" },  notes = "根据订单批量建立发票")
	@RequestMapping(method = RequestMethod.POST, value = "/salesorders/{salesorder_id}/invoices/batch")
    public ResponseEntity<Boolean> createBatchBySalesOrder(@PathVariable("salesorder_id") String salesorder_id, @RequestBody List<InvoiceDTO> invoicedtos) {
        List<Invoice> domainlist=invoiceMapping.toDomain(invoicedtos);
        for(Invoice domain:domainlist){
            domain.setSalesorderid(salesorder_id);
        }
        invoiceService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "invoice" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.invoiceService.get(#invoice_id),'iBizBusinessCentral-Invoice-Update')")
    @ApiOperation(value = "根据订单更新发票", tags = {"发票" },  notes = "根据订单更新发票")
	@RequestMapping(method = RequestMethod.PUT, value = "/salesorders/{salesorder_id}/invoices/{invoice_id}")
    public ResponseEntity<InvoiceDTO> updateBySalesOrder(@PathVariable("salesorder_id") String salesorder_id, @PathVariable("invoice_id") String invoice_id, @RequestBody InvoiceDTO invoicedto) {
        Invoice domain = invoiceMapping.toDomain(invoicedto);
        domain.setSalesorderid(salesorder_id);
        domain.setInvoiceid(invoice_id);
		invoiceService.update(domain);
        InvoiceDTO dto = invoiceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.invoiceService.getInvoiceByEntities(this.invoiceMapping.toDomain(#invoicedtos)),'iBizBusinessCentral-Invoice-Update')")
    @ApiOperation(value = "根据订单批量更新发票", tags = {"发票" },  notes = "根据订单批量更新发票")
	@RequestMapping(method = RequestMethod.PUT, value = "/salesorders/{salesorder_id}/invoices/batch")
    public ResponseEntity<Boolean> updateBatchBySalesOrder(@PathVariable("salesorder_id") String salesorder_id, @RequestBody List<InvoiceDTO> invoicedtos) {
        List<Invoice> domainlist=invoiceMapping.toDomain(invoicedtos);
        for(Invoice domain:domainlist){
            domain.setSalesorderid(salesorder_id);
        }
        invoiceService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.invoiceService.get(#invoice_id),'iBizBusinessCentral-Invoice-Remove')")
    @ApiOperation(value = "根据订单删除发票", tags = {"发票" },  notes = "根据订单删除发票")
	@RequestMapping(method = RequestMethod.DELETE, value = "/salesorders/{salesorder_id}/invoices/{invoice_id}")
    public ResponseEntity<Boolean> removeBySalesOrder(@PathVariable("salesorder_id") String salesorder_id, @PathVariable("invoice_id") String invoice_id) {
		return ResponseEntity.status(HttpStatus.OK).body(invoiceService.remove(invoice_id));
    }

    @PreAuthorize("hasPermission(this.invoiceService.getInvoiceByIds(#ids),'iBizBusinessCentral-Invoice-Remove')")
    @ApiOperation(value = "根据订单批量删除发票", tags = {"发票" },  notes = "根据订单批量删除发票")
	@RequestMapping(method = RequestMethod.DELETE, value = "/salesorders/{salesorder_id}/invoices/batch")
    public ResponseEntity<Boolean> removeBatchBySalesOrder(@RequestBody List<String> ids) {
        invoiceService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.invoiceMapping.toDomain(returnObject.body),'iBizBusinessCentral-Invoice-Get')")
    @ApiOperation(value = "根据订单获取发票", tags = {"发票" },  notes = "根据订单获取发票")
	@RequestMapping(method = RequestMethod.GET, value = "/salesorders/{salesorder_id}/invoices/{invoice_id}")
    public ResponseEntity<InvoiceDTO> getBySalesOrder(@PathVariable("salesorder_id") String salesorder_id, @PathVariable("invoice_id") String invoice_id) {
        Invoice domain = invoiceService.get(invoice_id);
        InvoiceDTO dto = invoiceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据订单获取发票草稿", tags = {"发票" },  notes = "根据订单获取发票草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/salesorders/{salesorder_id}/invoices/getdraft")
    public ResponseEntity<InvoiceDTO> getDraftBySalesOrder(@PathVariable("salesorder_id") String salesorder_id) {
        Invoice domain = new Invoice();
        domain.setSalesorderid(salesorder_id);
        return ResponseEntity.status(HttpStatus.OK).body(invoiceMapping.toDto(invoiceService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-Cancel-all')")
    @ApiOperation(value = "根据订单发票", tags = {"发票" },  notes = "根据订单发票")
	@RequestMapping(method = RequestMethod.POST, value = "/salesorders/{salesorder_id}/invoices/{invoice_id}/cancel")
    public ResponseEntity<InvoiceDTO> cancelBySalesOrder(@PathVariable("salesorder_id") String salesorder_id, @PathVariable("invoice_id") String invoice_id, @RequestBody InvoiceDTO invoicedto) {
        Invoice domain = invoiceMapping.toDomain(invoicedto);
        domain.setSalesorderid(salesorder_id);
        domain = invoiceService.cancel(domain) ;
        invoicedto = invoiceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(invoicedto);
    }

    @ApiOperation(value = "根据订单检查发票", tags = {"发票" },  notes = "根据订单检查发票")
	@RequestMapping(method = RequestMethod.POST, value = "/salesorders/{salesorder_id}/invoices/checkkey")
    public ResponseEntity<Boolean> checkKeyBySalesOrder(@PathVariable("salesorder_id") String salesorder_id, @RequestBody InvoiceDTO invoicedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(invoiceService.checkKey(invoiceMapping.toDomain(invoicedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-Finish-all')")
    @ApiOperation(value = "根据订单发票", tags = {"发票" },  notes = "根据订单发票")
	@RequestMapping(method = RequestMethod.POST, value = "/salesorders/{salesorder_id}/invoices/{invoice_id}/finish")
    public ResponseEntity<InvoiceDTO> finishBySalesOrder(@PathVariable("salesorder_id") String salesorder_id, @PathVariable("invoice_id") String invoice_id, @RequestBody InvoiceDTO invoicedto) {
        Invoice domain = invoiceMapping.toDomain(invoicedto);
        domain.setSalesorderid(salesorder_id);
        domain = invoiceService.finish(domain) ;
        invoicedto = invoiceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(invoicedto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-Paid-all')")
    @ApiOperation(value = "根据订单发票", tags = {"发票" },  notes = "根据订单发票")
	@RequestMapping(method = RequestMethod.POST, value = "/salesorders/{salesorder_id}/invoices/{invoice_id}/paid")
    public ResponseEntity<InvoiceDTO> paidBySalesOrder(@PathVariable("salesorder_id") String salesorder_id, @PathVariable("invoice_id") String invoice_id, @RequestBody InvoiceDTO invoicedto) {
        Invoice domain = invoiceMapping.toDomain(invoicedto);
        domain.setSalesorderid(salesorder_id);
        domain = invoiceService.paid(domain) ;
        invoicedto = invoiceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(invoicedto);
    }

    @PreAuthorize("hasPermission(this.invoiceMapping.toDomain(#invoicedto),'iBizBusinessCentral-Invoice-Save')")
    @ApiOperation(value = "根据订单保存发票", tags = {"发票" },  notes = "根据订单保存发票")
	@RequestMapping(method = RequestMethod.POST, value = "/salesorders/{salesorder_id}/invoices/save")
    public ResponseEntity<Boolean> saveBySalesOrder(@PathVariable("salesorder_id") String salesorder_id, @RequestBody InvoiceDTO invoicedto) {
        Invoice domain = invoiceMapping.toDomain(invoicedto);
        domain.setSalesorderid(salesorder_id);
        return ResponseEntity.status(HttpStatus.OK).body(invoiceService.save(domain));
    }

    @PreAuthorize("hasPermission(this.invoiceMapping.toDomain(#invoicedtos),'iBizBusinessCentral-Invoice-Save')")
    @ApiOperation(value = "根据订单批量保存发票", tags = {"发票" },  notes = "根据订单批量保存发票")
	@RequestMapping(method = RequestMethod.POST, value = "/salesorders/{salesorder_id}/invoices/savebatch")
    public ResponseEntity<Boolean> saveBatchBySalesOrder(@PathVariable("salesorder_id") String salesorder_id, @RequestBody List<InvoiceDTO> invoicedtos) {
        List<Invoice> domainlist=invoiceMapping.toDomain(invoicedtos);
        for(Invoice domain:domainlist){
             domain.setSalesorderid(salesorder_id);
        }
        invoiceService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchByParentKey-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据订单获取ByParentKey", tags = {"发票" } ,notes = "根据订单获取ByParentKey")
    @RequestMapping(method= RequestMethod.GET , value="/salesorders/{salesorder_id}/invoices/fetchbyparentkey")
	public ResponseEntity<List<InvoiceDTO>> fetchInvoiceByParentKeyBySalesOrder(@PathVariable("salesorder_id") String salesorder_id,InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchByParentKey(context) ;
        List<InvoiceDTO> list = invoiceMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchByParentKey-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据订单查询ByParentKey", tags = {"发票" } ,notes = "根据订单查询ByParentKey")
    @RequestMapping(method= RequestMethod.POST , value="/salesorders/{salesorder_id}/invoices/searchbyparentkey")
	public ResponseEntity<Page<InvoiceDTO>> searchInvoiceByParentKeyBySalesOrder(@PathVariable("salesorder_id") String salesorder_id, @RequestBody InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchByParentKey(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(invoiceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchCancel-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据订单获取已取消", tags = {"发票" } ,notes = "根据订单获取已取消")
    @RequestMapping(method= RequestMethod.GET , value="/salesorders/{salesorder_id}/invoices/fetchcancel")
	public ResponseEntity<List<InvoiceDTO>> fetchInvoiceCancelBySalesOrder(@PathVariable("salesorder_id") String salesorder_id,InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchCancel(context) ;
        List<InvoiceDTO> list = invoiceMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchCancel-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据订单查询已取消", tags = {"发票" } ,notes = "根据订单查询已取消")
    @RequestMapping(method= RequestMethod.POST , value="/salesorders/{salesorder_id}/invoices/searchcancel")
	public ResponseEntity<Page<InvoiceDTO>> searchInvoiceCancelBySalesOrder(@PathVariable("salesorder_id") String salesorder_id, @RequestBody InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchCancel(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(invoiceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据订单获取DEFAULT", tags = {"发票" } ,notes = "根据订单获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/salesorders/{salesorder_id}/invoices/fetchdefault")
	public ResponseEntity<List<InvoiceDTO>> fetchInvoiceDefaultBySalesOrder(@PathVariable("salesorder_id") String salesorder_id,InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchDefault(context) ;
        List<InvoiceDTO> list = invoiceMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据订单查询DEFAULT", tags = {"发票" } ,notes = "根据订单查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/salesorders/{salesorder_id}/invoices/searchdefault")
	public ResponseEntity<Page<InvoiceDTO>> searchInvoiceDefaultBySalesOrder(@PathVariable("salesorder_id") String salesorder_id, @RequestBody InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(invoiceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchPaid-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据订单获取已支付", tags = {"发票" } ,notes = "根据订单获取已支付")
    @RequestMapping(method= RequestMethod.GET , value="/salesorders/{salesorder_id}/invoices/fetchpaid")
	public ResponseEntity<List<InvoiceDTO>> fetchInvoicePaidBySalesOrder(@PathVariable("salesorder_id") String salesorder_id,InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchPaid(context) ;
        List<InvoiceDTO> list = invoiceMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchPaid-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据订单查询已支付", tags = {"发票" } ,notes = "根据订单查询已支付")
    @RequestMapping(method= RequestMethod.POST , value="/salesorders/{salesorder_id}/invoices/searchpaid")
	public ResponseEntity<Page<InvoiceDTO>> searchInvoicePaidBySalesOrder(@PathVariable("salesorder_id") String salesorder_id, @RequestBody InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchPaid(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(invoiceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.invoiceMapping.toDomain(#invoicedto),'iBizBusinessCentral-Invoice-Create')")
    @ApiOperation(value = "根据报价单订单建立发票", tags = {"发票" },  notes = "根据报价单订单建立发票")
	@RequestMapping(method = RequestMethod.POST, value = "/quotes/{quote_id}/salesorders/{salesorder_id}/invoices")
    public ResponseEntity<InvoiceDTO> createByQuoteSalesOrder(@PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody InvoiceDTO invoicedto) {
        Invoice domain = invoiceMapping.toDomain(invoicedto);
        domain.setSalesorderid(salesorder_id);
		invoiceService.create(domain);
        InvoiceDTO dto = invoiceMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.invoiceMapping.toDomain(#invoicedtos),'iBizBusinessCentral-Invoice-Create')")
    @ApiOperation(value = "根据报价单订单批量建立发票", tags = {"发票" },  notes = "根据报价单订单批量建立发票")
	@RequestMapping(method = RequestMethod.POST, value = "/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/batch")
    public ResponseEntity<Boolean> createBatchByQuoteSalesOrder(@PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody List<InvoiceDTO> invoicedtos) {
        List<Invoice> domainlist=invoiceMapping.toDomain(invoicedtos);
        for(Invoice domain:domainlist){
            domain.setSalesorderid(salesorder_id);
        }
        invoiceService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "invoice" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.invoiceService.get(#invoice_id),'iBizBusinessCentral-Invoice-Update')")
    @ApiOperation(value = "根据报价单订单更新发票", tags = {"发票" },  notes = "根据报价单订单更新发票")
	@RequestMapping(method = RequestMethod.PUT, value = "/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/{invoice_id}")
    public ResponseEntity<InvoiceDTO> updateByQuoteSalesOrder(@PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @PathVariable("invoice_id") String invoice_id, @RequestBody InvoiceDTO invoicedto) {
        Invoice domain = invoiceMapping.toDomain(invoicedto);
        domain.setSalesorderid(salesorder_id);
        domain.setInvoiceid(invoice_id);
		invoiceService.update(domain);
        InvoiceDTO dto = invoiceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.invoiceService.getInvoiceByEntities(this.invoiceMapping.toDomain(#invoicedtos)),'iBizBusinessCentral-Invoice-Update')")
    @ApiOperation(value = "根据报价单订单批量更新发票", tags = {"发票" },  notes = "根据报价单订单批量更新发票")
	@RequestMapping(method = RequestMethod.PUT, value = "/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/batch")
    public ResponseEntity<Boolean> updateBatchByQuoteSalesOrder(@PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody List<InvoiceDTO> invoicedtos) {
        List<Invoice> domainlist=invoiceMapping.toDomain(invoicedtos);
        for(Invoice domain:domainlist){
            domain.setSalesorderid(salesorder_id);
        }
        invoiceService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.invoiceService.get(#invoice_id),'iBizBusinessCentral-Invoice-Remove')")
    @ApiOperation(value = "根据报价单订单删除发票", tags = {"发票" },  notes = "根据报价单订单删除发票")
	@RequestMapping(method = RequestMethod.DELETE, value = "/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/{invoice_id}")
    public ResponseEntity<Boolean> removeByQuoteSalesOrder(@PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @PathVariable("invoice_id") String invoice_id) {
		return ResponseEntity.status(HttpStatus.OK).body(invoiceService.remove(invoice_id));
    }

    @PreAuthorize("hasPermission(this.invoiceService.getInvoiceByIds(#ids),'iBizBusinessCentral-Invoice-Remove')")
    @ApiOperation(value = "根据报价单订单批量删除发票", tags = {"发票" },  notes = "根据报价单订单批量删除发票")
	@RequestMapping(method = RequestMethod.DELETE, value = "/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/batch")
    public ResponseEntity<Boolean> removeBatchByQuoteSalesOrder(@RequestBody List<String> ids) {
        invoiceService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.invoiceMapping.toDomain(returnObject.body),'iBizBusinessCentral-Invoice-Get')")
    @ApiOperation(value = "根据报价单订单获取发票", tags = {"发票" },  notes = "根据报价单订单获取发票")
	@RequestMapping(method = RequestMethod.GET, value = "/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/{invoice_id}")
    public ResponseEntity<InvoiceDTO> getByQuoteSalesOrder(@PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @PathVariable("invoice_id") String invoice_id) {
        Invoice domain = invoiceService.get(invoice_id);
        InvoiceDTO dto = invoiceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据报价单订单获取发票草稿", tags = {"发票" },  notes = "根据报价单订单获取发票草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/getdraft")
    public ResponseEntity<InvoiceDTO> getDraftByQuoteSalesOrder(@PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id) {
        Invoice domain = new Invoice();
        domain.setSalesorderid(salesorder_id);
        return ResponseEntity.status(HttpStatus.OK).body(invoiceMapping.toDto(invoiceService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-Cancel-all')")
    @ApiOperation(value = "根据报价单订单发票", tags = {"发票" },  notes = "根据报价单订单发票")
	@RequestMapping(method = RequestMethod.POST, value = "/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/{invoice_id}/cancel")
    public ResponseEntity<InvoiceDTO> cancelByQuoteSalesOrder(@PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @PathVariable("invoice_id") String invoice_id, @RequestBody InvoiceDTO invoicedto) {
        Invoice domain = invoiceMapping.toDomain(invoicedto);
        domain.setSalesorderid(salesorder_id);
        domain = invoiceService.cancel(domain) ;
        invoicedto = invoiceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(invoicedto);
    }

    @ApiOperation(value = "根据报价单订单检查发票", tags = {"发票" },  notes = "根据报价单订单检查发票")
	@RequestMapping(method = RequestMethod.POST, value = "/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/checkkey")
    public ResponseEntity<Boolean> checkKeyByQuoteSalesOrder(@PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody InvoiceDTO invoicedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(invoiceService.checkKey(invoiceMapping.toDomain(invoicedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-Finish-all')")
    @ApiOperation(value = "根据报价单订单发票", tags = {"发票" },  notes = "根据报价单订单发票")
	@RequestMapping(method = RequestMethod.POST, value = "/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/{invoice_id}/finish")
    public ResponseEntity<InvoiceDTO> finishByQuoteSalesOrder(@PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @PathVariable("invoice_id") String invoice_id, @RequestBody InvoiceDTO invoicedto) {
        Invoice domain = invoiceMapping.toDomain(invoicedto);
        domain.setSalesorderid(salesorder_id);
        domain = invoiceService.finish(domain) ;
        invoicedto = invoiceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(invoicedto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-Paid-all')")
    @ApiOperation(value = "根据报价单订单发票", tags = {"发票" },  notes = "根据报价单订单发票")
	@RequestMapping(method = RequestMethod.POST, value = "/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/{invoice_id}/paid")
    public ResponseEntity<InvoiceDTO> paidByQuoteSalesOrder(@PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @PathVariable("invoice_id") String invoice_id, @RequestBody InvoiceDTO invoicedto) {
        Invoice domain = invoiceMapping.toDomain(invoicedto);
        domain.setSalesorderid(salesorder_id);
        domain = invoiceService.paid(domain) ;
        invoicedto = invoiceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(invoicedto);
    }

    @PreAuthorize("hasPermission(this.invoiceMapping.toDomain(#invoicedto),'iBizBusinessCentral-Invoice-Save')")
    @ApiOperation(value = "根据报价单订单保存发票", tags = {"发票" },  notes = "根据报价单订单保存发票")
	@RequestMapping(method = RequestMethod.POST, value = "/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/save")
    public ResponseEntity<Boolean> saveByQuoteSalesOrder(@PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody InvoiceDTO invoicedto) {
        Invoice domain = invoiceMapping.toDomain(invoicedto);
        domain.setSalesorderid(salesorder_id);
        return ResponseEntity.status(HttpStatus.OK).body(invoiceService.save(domain));
    }

    @PreAuthorize("hasPermission(this.invoiceMapping.toDomain(#invoicedtos),'iBizBusinessCentral-Invoice-Save')")
    @ApiOperation(value = "根据报价单订单批量保存发票", tags = {"发票" },  notes = "根据报价单订单批量保存发票")
	@RequestMapping(method = RequestMethod.POST, value = "/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/savebatch")
    public ResponseEntity<Boolean> saveBatchByQuoteSalesOrder(@PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody List<InvoiceDTO> invoicedtos) {
        List<Invoice> domainlist=invoiceMapping.toDomain(invoicedtos);
        for(Invoice domain:domainlist){
             domain.setSalesorderid(salesorder_id);
        }
        invoiceService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchByParentKey-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据报价单订单获取ByParentKey", tags = {"发票" } ,notes = "根据报价单订单获取ByParentKey")
    @RequestMapping(method= RequestMethod.GET , value="/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/fetchbyparentkey")
	public ResponseEntity<List<InvoiceDTO>> fetchInvoiceByParentKeyByQuoteSalesOrder(@PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id,InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchByParentKey(context) ;
        List<InvoiceDTO> list = invoiceMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchByParentKey-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据报价单订单查询ByParentKey", tags = {"发票" } ,notes = "根据报价单订单查询ByParentKey")
    @RequestMapping(method= RequestMethod.POST , value="/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/searchbyparentkey")
	public ResponseEntity<Page<InvoiceDTO>> searchInvoiceByParentKeyByQuoteSalesOrder(@PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchByParentKey(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(invoiceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchCancel-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据报价单订单获取已取消", tags = {"发票" } ,notes = "根据报价单订单获取已取消")
    @RequestMapping(method= RequestMethod.GET , value="/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/fetchcancel")
	public ResponseEntity<List<InvoiceDTO>> fetchInvoiceCancelByQuoteSalesOrder(@PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id,InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchCancel(context) ;
        List<InvoiceDTO> list = invoiceMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchCancel-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据报价单订单查询已取消", tags = {"发票" } ,notes = "根据报价单订单查询已取消")
    @RequestMapping(method= RequestMethod.POST , value="/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/searchcancel")
	public ResponseEntity<Page<InvoiceDTO>> searchInvoiceCancelByQuoteSalesOrder(@PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchCancel(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(invoiceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据报价单订单获取DEFAULT", tags = {"发票" } ,notes = "根据报价单订单获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/fetchdefault")
	public ResponseEntity<List<InvoiceDTO>> fetchInvoiceDefaultByQuoteSalesOrder(@PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id,InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchDefault(context) ;
        List<InvoiceDTO> list = invoiceMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据报价单订单查询DEFAULT", tags = {"发票" } ,notes = "根据报价单订单查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/searchdefault")
	public ResponseEntity<Page<InvoiceDTO>> searchInvoiceDefaultByQuoteSalesOrder(@PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(invoiceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchPaid-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据报价单订单获取已支付", tags = {"发票" } ,notes = "根据报价单订单获取已支付")
    @RequestMapping(method= RequestMethod.GET , value="/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/fetchpaid")
	public ResponseEntity<List<InvoiceDTO>> fetchInvoicePaidByQuoteSalesOrder(@PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id,InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchPaid(context) ;
        List<InvoiceDTO> list = invoiceMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchPaid-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据报价单订单查询已支付", tags = {"发票" } ,notes = "根据报价单订单查询已支付")
    @RequestMapping(method= RequestMethod.POST , value="/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/searchpaid")
	public ResponseEntity<Page<InvoiceDTO>> searchInvoicePaidByQuoteSalesOrder(@PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchPaid(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(invoiceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.invoiceMapping.toDomain(#invoicedto),'iBizBusinessCentral-Invoice-Create')")
    @ApiOperation(value = "根据商机报价单订单建立发票", tags = {"发票" },  notes = "根据商机报价单订单建立发票")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices")
    public ResponseEntity<InvoiceDTO> createByOpportunityQuoteSalesOrder(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody InvoiceDTO invoicedto) {
        Invoice domain = invoiceMapping.toDomain(invoicedto);
        domain.setSalesorderid(salesorder_id);
		invoiceService.create(domain);
        InvoiceDTO dto = invoiceMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.invoiceMapping.toDomain(#invoicedtos),'iBizBusinessCentral-Invoice-Create')")
    @ApiOperation(value = "根据商机报价单订单批量建立发票", tags = {"发票" },  notes = "根据商机报价单订单批量建立发票")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/batch")
    public ResponseEntity<Boolean> createBatchByOpportunityQuoteSalesOrder(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody List<InvoiceDTO> invoicedtos) {
        List<Invoice> domainlist=invoiceMapping.toDomain(invoicedtos);
        for(Invoice domain:domainlist){
            domain.setSalesorderid(salesorder_id);
        }
        invoiceService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "invoice" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.invoiceService.get(#invoice_id),'iBizBusinessCentral-Invoice-Update')")
    @ApiOperation(value = "根据商机报价单订单更新发票", tags = {"发票" },  notes = "根据商机报价单订单更新发票")
	@RequestMapping(method = RequestMethod.PUT, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/{invoice_id}")
    public ResponseEntity<InvoiceDTO> updateByOpportunityQuoteSalesOrder(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @PathVariable("invoice_id") String invoice_id, @RequestBody InvoiceDTO invoicedto) {
        Invoice domain = invoiceMapping.toDomain(invoicedto);
        domain.setSalesorderid(salesorder_id);
        domain.setInvoiceid(invoice_id);
		invoiceService.update(domain);
        InvoiceDTO dto = invoiceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.invoiceService.getInvoiceByEntities(this.invoiceMapping.toDomain(#invoicedtos)),'iBizBusinessCentral-Invoice-Update')")
    @ApiOperation(value = "根据商机报价单订单批量更新发票", tags = {"发票" },  notes = "根据商机报价单订单批量更新发票")
	@RequestMapping(method = RequestMethod.PUT, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/batch")
    public ResponseEntity<Boolean> updateBatchByOpportunityQuoteSalesOrder(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody List<InvoiceDTO> invoicedtos) {
        List<Invoice> domainlist=invoiceMapping.toDomain(invoicedtos);
        for(Invoice domain:domainlist){
            domain.setSalesorderid(salesorder_id);
        }
        invoiceService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.invoiceService.get(#invoice_id),'iBizBusinessCentral-Invoice-Remove')")
    @ApiOperation(value = "根据商机报价单订单删除发票", tags = {"发票" },  notes = "根据商机报价单订单删除发票")
	@RequestMapping(method = RequestMethod.DELETE, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/{invoice_id}")
    public ResponseEntity<Boolean> removeByOpportunityQuoteSalesOrder(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @PathVariable("invoice_id") String invoice_id) {
		return ResponseEntity.status(HttpStatus.OK).body(invoiceService.remove(invoice_id));
    }

    @PreAuthorize("hasPermission(this.invoiceService.getInvoiceByIds(#ids),'iBizBusinessCentral-Invoice-Remove')")
    @ApiOperation(value = "根据商机报价单订单批量删除发票", tags = {"发票" },  notes = "根据商机报价单订单批量删除发票")
	@RequestMapping(method = RequestMethod.DELETE, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/batch")
    public ResponseEntity<Boolean> removeBatchByOpportunityQuoteSalesOrder(@RequestBody List<String> ids) {
        invoiceService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.invoiceMapping.toDomain(returnObject.body),'iBizBusinessCentral-Invoice-Get')")
    @ApiOperation(value = "根据商机报价单订单获取发票", tags = {"发票" },  notes = "根据商机报价单订单获取发票")
	@RequestMapping(method = RequestMethod.GET, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/{invoice_id}")
    public ResponseEntity<InvoiceDTO> getByOpportunityQuoteSalesOrder(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @PathVariable("invoice_id") String invoice_id) {
        Invoice domain = invoiceService.get(invoice_id);
        InvoiceDTO dto = invoiceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据商机报价单订单获取发票草稿", tags = {"发票" },  notes = "根据商机报价单订单获取发票草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/getdraft")
    public ResponseEntity<InvoiceDTO> getDraftByOpportunityQuoteSalesOrder(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id) {
        Invoice domain = new Invoice();
        domain.setSalesorderid(salesorder_id);
        return ResponseEntity.status(HttpStatus.OK).body(invoiceMapping.toDto(invoiceService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-Cancel-all')")
    @ApiOperation(value = "根据商机报价单订单发票", tags = {"发票" },  notes = "根据商机报价单订单发票")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/{invoice_id}/cancel")
    public ResponseEntity<InvoiceDTO> cancelByOpportunityQuoteSalesOrder(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @PathVariable("invoice_id") String invoice_id, @RequestBody InvoiceDTO invoicedto) {
        Invoice domain = invoiceMapping.toDomain(invoicedto);
        domain.setSalesorderid(salesorder_id);
        domain = invoiceService.cancel(domain) ;
        invoicedto = invoiceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(invoicedto);
    }

    @ApiOperation(value = "根据商机报价单订单检查发票", tags = {"发票" },  notes = "根据商机报价单订单检查发票")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/checkkey")
    public ResponseEntity<Boolean> checkKeyByOpportunityQuoteSalesOrder(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody InvoiceDTO invoicedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(invoiceService.checkKey(invoiceMapping.toDomain(invoicedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-Finish-all')")
    @ApiOperation(value = "根据商机报价单订单发票", tags = {"发票" },  notes = "根据商机报价单订单发票")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/{invoice_id}/finish")
    public ResponseEntity<InvoiceDTO> finishByOpportunityQuoteSalesOrder(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @PathVariable("invoice_id") String invoice_id, @RequestBody InvoiceDTO invoicedto) {
        Invoice domain = invoiceMapping.toDomain(invoicedto);
        domain.setSalesorderid(salesorder_id);
        domain = invoiceService.finish(domain) ;
        invoicedto = invoiceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(invoicedto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-Paid-all')")
    @ApiOperation(value = "根据商机报价单订单发票", tags = {"发票" },  notes = "根据商机报价单订单发票")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/{invoice_id}/paid")
    public ResponseEntity<InvoiceDTO> paidByOpportunityQuoteSalesOrder(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @PathVariable("invoice_id") String invoice_id, @RequestBody InvoiceDTO invoicedto) {
        Invoice domain = invoiceMapping.toDomain(invoicedto);
        domain.setSalesorderid(salesorder_id);
        domain = invoiceService.paid(domain) ;
        invoicedto = invoiceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(invoicedto);
    }

    @PreAuthorize("hasPermission(this.invoiceMapping.toDomain(#invoicedto),'iBizBusinessCentral-Invoice-Save')")
    @ApiOperation(value = "根据商机报价单订单保存发票", tags = {"发票" },  notes = "根据商机报价单订单保存发票")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/save")
    public ResponseEntity<Boolean> saveByOpportunityQuoteSalesOrder(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody InvoiceDTO invoicedto) {
        Invoice domain = invoiceMapping.toDomain(invoicedto);
        domain.setSalesorderid(salesorder_id);
        return ResponseEntity.status(HttpStatus.OK).body(invoiceService.save(domain));
    }

    @PreAuthorize("hasPermission(this.invoiceMapping.toDomain(#invoicedtos),'iBizBusinessCentral-Invoice-Save')")
    @ApiOperation(value = "根据商机报价单订单批量保存发票", tags = {"发票" },  notes = "根据商机报价单订单批量保存发票")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/savebatch")
    public ResponseEntity<Boolean> saveBatchByOpportunityQuoteSalesOrder(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody List<InvoiceDTO> invoicedtos) {
        List<Invoice> domainlist=invoiceMapping.toDomain(invoicedtos);
        for(Invoice domain:domainlist){
             domain.setSalesorderid(salesorder_id);
        }
        invoiceService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchByParentKey-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据商机报价单订单获取ByParentKey", tags = {"发票" } ,notes = "根据商机报价单订单获取ByParentKey")
    @RequestMapping(method= RequestMethod.GET , value="/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/fetchbyparentkey")
	public ResponseEntity<List<InvoiceDTO>> fetchInvoiceByParentKeyByOpportunityQuoteSalesOrder(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id,InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchByParentKey(context) ;
        List<InvoiceDTO> list = invoiceMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchByParentKey-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据商机报价单订单查询ByParentKey", tags = {"发票" } ,notes = "根据商机报价单订单查询ByParentKey")
    @RequestMapping(method= RequestMethod.POST , value="/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/searchbyparentkey")
	public ResponseEntity<Page<InvoiceDTO>> searchInvoiceByParentKeyByOpportunityQuoteSalesOrder(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchByParentKey(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(invoiceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchCancel-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据商机报价单订单获取已取消", tags = {"发票" } ,notes = "根据商机报价单订单获取已取消")
    @RequestMapping(method= RequestMethod.GET , value="/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/fetchcancel")
	public ResponseEntity<List<InvoiceDTO>> fetchInvoiceCancelByOpportunityQuoteSalesOrder(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id,InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchCancel(context) ;
        List<InvoiceDTO> list = invoiceMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchCancel-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据商机报价单订单查询已取消", tags = {"发票" } ,notes = "根据商机报价单订单查询已取消")
    @RequestMapping(method= RequestMethod.POST , value="/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/searchcancel")
	public ResponseEntity<Page<InvoiceDTO>> searchInvoiceCancelByOpportunityQuoteSalesOrder(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchCancel(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(invoiceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据商机报价单订单获取DEFAULT", tags = {"发票" } ,notes = "根据商机报价单订单获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/fetchdefault")
	public ResponseEntity<List<InvoiceDTO>> fetchInvoiceDefaultByOpportunityQuoteSalesOrder(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id,InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchDefault(context) ;
        List<InvoiceDTO> list = invoiceMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据商机报价单订单查询DEFAULT", tags = {"发票" } ,notes = "根据商机报价单订单查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/searchdefault")
	public ResponseEntity<Page<InvoiceDTO>> searchInvoiceDefaultByOpportunityQuoteSalesOrder(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(invoiceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchPaid-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据商机报价单订单获取已支付", tags = {"发票" } ,notes = "根据商机报价单订单获取已支付")
    @RequestMapping(method= RequestMethod.GET , value="/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/fetchpaid")
	public ResponseEntity<List<InvoiceDTO>> fetchInvoicePaidByOpportunityQuoteSalesOrder(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id,InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchPaid(context) ;
        List<InvoiceDTO> list = invoiceMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchPaid-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据商机报价单订单查询已支付", tags = {"发票" } ,notes = "根据商机报价单订单查询已支付")
    @RequestMapping(method= RequestMethod.POST , value="/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/searchpaid")
	public ResponseEntity<Page<InvoiceDTO>> searchInvoicePaidByOpportunityQuoteSalesOrder(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchPaid(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(invoiceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.invoiceMapping.toDomain(#invoicedto),'iBizBusinessCentral-Invoice-Create')")
    @ApiOperation(value = "根据客户商机报价单订单建立发票", tags = {"发票" },  notes = "根据客户商机报价单订单建立发票")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices")
    public ResponseEntity<InvoiceDTO> createByAccountOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody InvoiceDTO invoicedto) {
        Invoice domain = invoiceMapping.toDomain(invoicedto);
        domain.setSalesorderid(salesorder_id);
		invoiceService.create(domain);
        InvoiceDTO dto = invoiceMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.invoiceMapping.toDomain(#invoicedtos),'iBizBusinessCentral-Invoice-Create')")
    @ApiOperation(value = "根据客户商机报价单订单批量建立发票", tags = {"发票" },  notes = "根据客户商机报价单订单批量建立发票")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/batch")
    public ResponseEntity<Boolean> createBatchByAccountOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody List<InvoiceDTO> invoicedtos) {
        List<Invoice> domainlist=invoiceMapping.toDomain(invoicedtos);
        for(Invoice domain:domainlist){
            domain.setSalesorderid(salesorder_id);
        }
        invoiceService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "invoice" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.invoiceService.get(#invoice_id),'iBizBusinessCentral-Invoice-Update')")
    @ApiOperation(value = "根据客户商机报价单订单更新发票", tags = {"发票" },  notes = "根据客户商机报价单订单更新发票")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/{invoice_id}")
    public ResponseEntity<InvoiceDTO> updateByAccountOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @PathVariable("invoice_id") String invoice_id, @RequestBody InvoiceDTO invoicedto) {
        Invoice domain = invoiceMapping.toDomain(invoicedto);
        domain.setSalesorderid(salesorder_id);
        domain.setInvoiceid(invoice_id);
		invoiceService.update(domain);
        InvoiceDTO dto = invoiceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.invoiceService.getInvoiceByEntities(this.invoiceMapping.toDomain(#invoicedtos)),'iBizBusinessCentral-Invoice-Update')")
    @ApiOperation(value = "根据客户商机报价单订单批量更新发票", tags = {"发票" },  notes = "根据客户商机报价单订单批量更新发票")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/batch")
    public ResponseEntity<Boolean> updateBatchByAccountOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody List<InvoiceDTO> invoicedtos) {
        List<Invoice> domainlist=invoiceMapping.toDomain(invoicedtos);
        for(Invoice domain:domainlist){
            domain.setSalesorderid(salesorder_id);
        }
        invoiceService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.invoiceService.get(#invoice_id),'iBizBusinessCentral-Invoice-Remove')")
    @ApiOperation(value = "根据客户商机报价单订单删除发票", tags = {"发票" },  notes = "根据客户商机报价单订单删除发票")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/{invoice_id}")
    public ResponseEntity<Boolean> removeByAccountOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @PathVariable("invoice_id") String invoice_id) {
		return ResponseEntity.status(HttpStatus.OK).body(invoiceService.remove(invoice_id));
    }

    @PreAuthorize("hasPermission(this.invoiceService.getInvoiceByIds(#ids),'iBizBusinessCentral-Invoice-Remove')")
    @ApiOperation(value = "根据客户商机报价单订单批量删除发票", tags = {"发票" },  notes = "根据客户商机报价单订单批量删除发票")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/batch")
    public ResponseEntity<Boolean> removeBatchByAccountOpportunityQuoteSalesOrder(@RequestBody List<String> ids) {
        invoiceService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.invoiceMapping.toDomain(returnObject.body),'iBizBusinessCentral-Invoice-Get')")
    @ApiOperation(value = "根据客户商机报价单订单获取发票", tags = {"发票" },  notes = "根据客户商机报价单订单获取发票")
	@RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/{invoice_id}")
    public ResponseEntity<InvoiceDTO> getByAccountOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @PathVariable("invoice_id") String invoice_id) {
        Invoice domain = invoiceService.get(invoice_id);
        InvoiceDTO dto = invoiceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据客户商机报价单订单获取发票草稿", tags = {"发票" },  notes = "根据客户商机报价单订单获取发票草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/getdraft")
    public ResponseEntity<InvoiceDTO> getDraftByAccountOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id) {
        Invoice domain = new Invoice();
        domain.setSalesorderid(salesorder_id);
        return ResponseEntity.status(HttpStatus.OK).body(invoiceMapping.toDto(invoiceService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-Cancel-all')")
    @ApiOperation(value = "根据客户商机报价单订单发票", tags = {"发票" },  notes = "根据客户商机报价单订单发票")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/{invoice_id}/cancel")
    public ResponseEntity<InvoiceDTO> cancelByAccountOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @PathVariable("invoice_id") String invoice_id, @RequestBody InvoiceDTO invoicedto) {
        Invoice domain = invoiceMapping.toDomain(invoicedto);
        domain.setSalesorderid(salesorder_id);
        domain = invoiceService.cancel(domain) ;
        invoicedto = invoiceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(invoicedto);
    }

    @ApiOperation(value = "根据客户商机报价单订单检查发票", tags = {"发票" },  notes = "根据客户商机报价单订单检查发票")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/checkkey")
    public ResponseEntity<Boolean> checkKeyByAccountOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody InvoiceDTO invoicedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(invoiceService.checkKey(invoiceMapping.toDomain(invoicedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-Finish-all')")
    @ApiOperation(value = "根据客户商机报价单订单发票", tags = {"发票" },  notes = "根据客户商机报价单订单发票")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/{invoice_id}/finish")
    public ResponseEntity<InvoiceDTO> finishByAccountOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @PathVariable("invoice_id") String invoice_id, @RequestBody InvoiceDTO invoicedto) {
        Invoice domain = invoiceMapping.toDomain(invoicedto);
        domain.setSalesorderid(salesorder_id);
        domain = invoiceService.finish(domain) ;
        invoicedto = invoiceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(invoicedto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-Paid-all')")
    @ApiOperation(value = "根据客户商机报价单订单发票", tags = {"发票" },  notes = "根据客户商机报价单订单发票")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/{invoice_id}/paid")
    public ResponseEntity<InvoiceDTO> paidByAccountOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @PathVariable("invoice_id") String invoice_id, @RequestBody InvoiceDTO invoicedto) {
        Invoice domain = invoiceMapping.toDomain(invoicedto);
        domain.setSalesorderid(salesorder_id);
        domain = invoiceService.paid(domain) ;
        invoicedto = invoiceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(invoicedto);
    }

    @PreAuthorize("hasPermission(this.invoiceMapping.toDomain(#invoicedto),'iBizBusinessCentral-Invoice-Save')")
    @ApiOperation(value = "根据客户商机报价单订单保存发票", tags = {"发票" },  notes = "根据客户商机报价单订单保存发票")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/save")
    public ResponseEntity<Boolean> saveByAccountOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody InvoiceDTO invoicedto) {
        Invoice domain = invoiceMapping.toDomain(invoicedto);
        domain.setSalesorderid(salesorder_id);
        return ResponseEntity.status(HttpStatus.OK).body(invoiceService.save(domain));
    }

    @PreAuthorize("hasPermission(this.invoiceMapping.toDomain(#invoicedtos),'iBizBusinessCentral-Invoice-Save')")
    @ApiOperation(value = "根据客户商机报价单订单批量保存发票", tags = {"发票" },  notes = "根据客户商机报价单订单批量保存发票")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/savebatch")
    public ResponseEntity<Boolean> saveBatchByAccountOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody List<InvoiceDTO> invoicedtos) {
        List<Invoice> domainlist=invoiceMapping.toDomain(invoicedtos);
        for(Invoice domain:domainlist){
             domain.setSalesorderid(salesorder_id);
        }
        invoiceService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchByParentKey-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据客户商机报价单订单获取ByParentKey", tags = {"发票" } ,notes = "根据客户商机报价单订单获取ByParentKey")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/fetchbyparentkey")
	public ResponseEntity<List<InvoiceDTO>> fetchInvoiceByParentKeyByAccountOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id,InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchByParentKey(context) ;
        List<InvoiceDTO> list = invoiceMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchByParentKey-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据客户商机报价单订单查询ByParentKey", tags = {"发票" } ,notes = "根据客户商机报价单订单查询ByParentKey")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/searchbyparentkey")
	public ResponseEntity<Page<InvoiceDTO>> searchInvoiceByParentKeyByAccountOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchByParentKey(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(invoiceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchCancel-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据客户商机报价单订单获取已取消", tags = {"发票" } ,notes = "根据客户商机报价单订单获取已取消")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/fetchcancel")
	public ResponseEntity<List<InvoiceDTO>> fetchInvoiceCancelByAccountOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id,InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchCancel(context) ;
        List<InvoiceDTO> list = invoiceMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchCancel-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据客户商机报价单订单查询已取消", tags = {"发票" } ,notes = "根据客户商机报价单订单查询已取消")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/searchcancel")
	public ResponseEntity<Page<InvoiceDTO>> searchInvoiceCancelByAccountOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchCancel(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(invoiceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据客户商机报价单订单获取DEFAULT", tags = {"发票" } ,notes = "根据客户商机报价单订单获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/fetchdefault")
	public ResponseEntity<List<InvoiceDTO>> fetchInvoiceDefaultByAccountOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id,InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchDefault(context) ;
        List<InvoiceDTO> list = invoiceMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据客户商机报价单订单查询DEFAULT", tags = {"发票" } ,notes = "根据客户商机报价单订单查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/searchdefault")
	public ResponseEntity<Page<InvoiceDTO>> searchInvoiceDefaultByAccountOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(invoiceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchPaid-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据客户商机报价单订单获取已支付", tags = {"发票" } ,notes = "根据客户商机报价单订单获取已支付")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/fetchpaid")
	public ResponseEntity<List<InvoiceDTO>> fetchInvoicePaidByAccountOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id,InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchPaid(context) ;
        List<InvoiceDTO> list = invoiceMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchPaid-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据客户商机报价单订单查询已支付", tags = {"发票" } ,notes = "根据客户商机报价单订单查询已支付")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/searchpaid")
	public ResponseEntity<Page<InvoiceDTO>> searchInvoicePaidByAccountOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchPaid(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(invoiceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.invoiceMapping.toDomain(#invoicedto),'iBizBusinessCentral-Invoice-Create')")
    @ApiOperation(value = "根据联系人商机报价单订单建立发票", tags = {"发票" },  notes = "根据联系人商机报价单订单建立发票")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices")
    public ResponseEntity<InvoiceDTO> createByContactOpportunityQuoteSalesOrder(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody InvoiceDTO invoicedto) {
        Invoice domain = invoiceMapping.toDomain(invoicedto);
        domain.setSalesorderid(salesorder_id);
		invoiceService.create(domain);
        InvoiceDTO dto = invoiceMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.invoiceMapping.toDomain(#invoicedtos),'iBizBusinessCentral-Invoice-Create')")
    @ApiOperation(value = "根据联系人商机报价单订单批量建立发票", tags = {"发票" },  notes = "根据联系人商机报价单订单批量建立发票")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/batch")
    public ResponseEntity<Boolean> createBatchByContactOpportunityQuoteSalesOrder(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody List<InvoiceDTO> invoicedtos) {
        List<Invoice> domainlist=invoiceMapping.toDomain(invoicedtos);
        for(Invoice domain:domainlist){
            domain.setSalesorderid(salesorder_id);
        }
        invoiceService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "invoice" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.invoiceService.get(#invoice_id),'iBizBusinessCentral-Invoice-Update')")
    @ApiOperation(value = "根据联系人商机报价单订单更新发票", tags = {"发票" },  notes = "根据联系人商机报价单订单更新发票")
	@RequestMapping(method = RequestMethod.PUT, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/{invoice_id}")
    public ResponseEntity<InvoiceDTO> updateByContactOpportunityQuoteSalesOrder(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @PathVariable("invoice_id") String invoice_id, @RequestBody InvoiceDTO invoicedto) {
        Invoice domain = invoiceMapping.toDomain(invoicedto);
        domain.setSalesorderid(salesorder_id);
        domain.setInvoiceid(invoice_id);
		invoiceService.update(domain);
        InvoiceDTO dto = invoiceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.invoiceService.getInvoiceByEntities(this.invoiceMapping.toDomain(#invoicedtos)),'iBizBusinessCentral-Invoice-Update')")
    @ApiOperation(value = "根据联系人商机报价单订单批量更新发票", tags = {"发票" },  notes = "根据联系人商机报价单订单批量更新发票")
	@RequestMapping(method = RequestMethod.PUT, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/batch")
    public ResponseEntity<Boolean> updateBatchByContactOpportunityQuoteSalesOrder(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody List<InvoiceDTO> invoicedtos) {
        List<Invoice> domainlist=invoiceMapping.toDomain(invoicedtos);
        for(Invoice domain:domainlist){
            domain.setSalesorderid(salesorder_id);
        }
        invoiceService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.invoiceService.get(#invoice_id),'iBizBusinessCentral-Invoice-Remove')")
    @ApiOperation(value = "根据联系人商机报价单订单删除发票", tags = {"发票" },  notes = "根据联系人商机报价单订单删除发票")
	@RequestMapping(method = RequestMethod.DELETE, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/{invoice_id}")
    public ResponseEntity<Boolean> removeByContactOpportunityQuoteSalesOrder(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @PathVariable("invoice_id") String invoice_id) {
		return ResponseEntity.status(HttpStatus.OK).body(invoiceService.remove(invoice_id));
    }

    @PreAuthorize("hasPermission(this.invoiceService.getInvoiceByIds(#ids),'iBizBusinessCentral-Invoice-Remove')")
    @ApiOperation(value = "根据联系人商机报价单订单批量删除发票", tags = {"发票" },  notes = "根据联系人商机报价单订单批量删除发票")
	@RequestMapping(method = RequestMethod.DELETE, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/batch")
    public ResponseEntity<Boolean> removeBatchByContactOpportunityQuoteSalesOrder(@RequestBody List<String> ids) {
        invoiceService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.invoiceMapping.toDomain(returnObject.body),'iBizBusinessCentral-Invoice-Get')")
    @ApiOperation(value = "根据联系人商机报价单订单获取发票", tags = {"发票" },  notes = "根据联系人商机报价单订单获取发票")
	@RequestMapping(method = RequestMethod.GET, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/{invoice_id}")
    public ResponseEntity<InvoiceDTO> getByContactOpportunityQuoteSalesOrder(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @PathVariable("invoice_id") String invoice_id) {
        Invoice domain = invoiceService.get(invoice_id);
        InvoiceDTO dto = invoiceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据联系人商机报价单订单获取发票草稿", tags = {"发票" },  notes = "根据联系人商机报价单订单获取发票草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/getdraft")
    public ResponseEntity<InvoiceDTO> getDraftByContactOpportunityQuoteSalesOrder(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id) {
        Invoice domain = new Invoice();
        domain.setSalesorderid(salesorder_id);
        return ResponseEntity.status(HttpStatus.OK).body(invoiceMapping.toDto(invoiceService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-Cancel-all')")
    @ApiOperation(value = "根据联系人商机报价单订单发票", tags = {"发票" },  notes = "根据联系人商机报价单订单发票")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/{invoice_id}/cancel")
    public ResponseEntity<InvoiceDTO> cancelByContactOpportunityQuoteSalesOrder(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @PathVariable("invoice_id") String invoice_id, @RequestBody InvoiceDTO invoicedto) {
        Invoice domain = invoiceMapping.toDomain(invoicedto);
        domain.setSalesorderid(salesorder_id);
        domain = invoiceService.cancel(domain) ;
        invoicedto = invoiceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(invoicedto);
    }

    @ApiOperation(value = "根据联系人商机报价单订单检查发票", tags = {"发票" },  notes = "根据联系人商机报价单订单检查发票")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/checkkey")
    public ResponseEntity<Boolean> checkKeyByContactOpportunityQuoteSalesOrder(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody InvoiceDTO invoicedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(invoiceService.checkKey(invoiceMapping.toDomain(invoicedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-Finish-all')")
    @ApiOperation(value = "根据联系人商机报价单订单发票", tags = {"发票" },  notes = "根据联系人商机报价单订单发票")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/{invoice_id}/finish")
    public ResponseEntity<InvoiceDTO> finishByContactOpportunityQuoteSalesOrder(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @PathVariable("invoice_id") String invoice_id, @RequestBody InvoiceDTO invoicedto) {
        Invoice domain = invoiceMapping.toDomain(invoicedto);
        domain.setSalesorderid(salesorder_id);
        domain = invoiceService.finish(domain) ;
        invoicedto = invoiceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(invoicedto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-Paid-all')")
    @ApiOperation(value = "根据联系人商机报价单订单发票", tags = {"发票" },  notes = "根据联系人商机报价单订单发票")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/{invoice_id}/paid")
    public ResponseEntity<InvoiceDTO> paidByContactOpportunityQuoteSalesOrder(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @PathVariable("invoice_id") String invoice_id, @RequestBody InvoiceDTO invoicedto) {
        Invoice domain = invoiceMapping.toDomain(invoicedto);
        domain.setSalesorderid(salesorder_id);
        domain = invoiceService.paid(domain) ;
        invoicedto = invoiceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(invoicedto);
    }

    @PreAuthorize("hasPermission(this.invoiceMapping.toDomain(#invoicedto),'iBizBusinessCentral-Invoice-Save')")
    @ApiOperation(value = "根据联系人商机报价单订单保存发票", tags = {"发票" },  notes = "根据联系人商机报价单订单保存发票")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/save")
    public ResponseEntity<Boolean> saveByContactOpportunityQuoteSalesOrder(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody InvoiceDTO invoicedto) {
        Invoice domain = invoiceMapping.toDomain(invoicedto);
        domain.setSalesorderid(salesorder_id);
        return ResponseEntity.status(HttpStatus.OK).body(invoiceService.save(domain));
    }

    @PreAuthorize("hasPermission(this.invoiceMapping.toDomain(#invoicedtos),'iBizBusinessCentral-Invoice-Save')")
    @ApiOperation(value = "根据联系人商机报价单订单批量保存发票", tags = {"发票" },  notes = "根据联系人商机报价单订单批量保存发票")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/savebatch")
    public ResponseEntity<Boolean> saveBatchByContactOpportunityQuoteSalesOrder(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody List<InvoiceDTO> invoicedtos) {
        List<Invoice> domainlist=invoiceMapping.toDomain(invoicedtos);
        for(Invoice domain:domainlist){
             domain.setSalesorderid(salesorder_id);
        }
        invoiceService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchByParentKey-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据联系人商机报价单订单获取ByParentKey", tags = {"发票" } ,notes = "根据联系人商机报价单订单获取ByParentKey")
    @RequestMapping(method= RequestMethod.GET , value="/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/fetchbyparentkey")
	public ResponseEntity<List<InvoiceDTO>> fetchInvoiceByParentKeyByContactOpportunityQuoteSalesOrder(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id,InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchByParentKey(context) ;
        List<InvoiceDTO> list = invoiceMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchByParentKey-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据联系人商机报价单订单查询ByParentKey", tags = {"发票" } ,notes = "根据联系人商机报价单订单查询ByParentKey")
    @RequestMapping(method= RequestMethod.POST , value="/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/searchbyparentkey")
	public ResponseEntity<Page<InvoiceDTO>> searchInvoiceByParentKeyByContactOpportunityQuoteSalesOrder(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchByParentKey(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(invoiceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchCancel-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据联系人商机报价单订单获取已取消", tags = {"发票" } ,notes = "根据联系人商机报价单订单获取已取消")
    @RequestMapping(method= RequestMethod.GET , value="/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/fetchcancel")
	public ResponseEntity<List<InvoiceDTO>> fetchInvoiceCancelByContactOpportunityQuoteSalesOrder(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id,InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchCancel(context) ;
        List<InvoiceDTO> list = invoiceMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchCancel-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据联系人商机报价单订单查询已取消", tags = {"发票" } ,notes = "根据联系人商机报价单订单查询已取消")
    @RequestMapping(method= RequestMethod.POST , value="/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/searchcancel")
	public ResponseEntity<Page<InvoiceDTO>> searchInvoiceCancelByContactOpportunityQuoteSalesOrder(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchCancel(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(invoiceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据联系人商机报价单订单获取DEFAULT", tags = {"发票" } ,notes = "根据联系人商机报价单订单获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/fetchdefault")
	public ResponseEntity<List<InvoiceDTO>> fetchInvoiceDefaultByContactOpportunityQuoteSalesOrder(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id,InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchDefault(context) ;
        List<InvoiceDTO> list = invoiceMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据联系人商机报价单订单查询DEFAULT", tags = {"发票" } ,notes = "根据联系人商机报价单订单查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/searchdefault")
	public ResponseEntity<Page<InvoiceDTO>> searchInvoiceDefaultByContactOpportunityQuoteSalesOrder(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(invoiceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchPaid-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据联系人商机报价单订单获取已支付", tags = {"发票" } ,notes = "根据联系人商机报价单订单获取已支付")
    @RequestMapping(method= RequestMethod.GET , value="/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/fetchpaid")
	public ResponseEntity<List<InvoiceDTO>> fetchInvoicePaidByContactOpportunityQuoteSalesOrder(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id,InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchPaid(context) ;
        List<InvoiceDTO> list = invoiceMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchPaid-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据联系人商机报价单订单查询已支付", tags = {"发票" } ,notes = "根据联系人商机报价单订单查询已支付")
    @RequestMapping(method= RequestMethod.POST , value="/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/searchpaid")
	public ResponseEntity<Page<InvoiceDTO>> searchInvoicePaidByContactOpportunityQuoteSalesOrder(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchPaid(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(invoiceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.invoiceMapping.toDomain(#invoicedto),'iBizBusinessCentral-Invoice-Create')")
    @ApiOperation(value = "根据客户联系人商机报价单订单建立发票", tags = {"发票" },  notes = "根据客户联系人商机报价单订单建立发票")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices")
    public ResponseEntity<InvoiceDTO> createByAccountContactOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody InvoiceDTO invoicedto) {
        Invoice domain = invoiceMapping.toDomain(invoicedto);
        domain.setSalesorderid(salesorder_id);
		invoiceService.create(domain);
        InvoiceDTO dto = invoiceMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.invoiceMapping.toDomain(#invoicedtos),'iBizBusinessCentral-Invoice-Create')")
    @ApiOperation(value = "根据客户联系人商机报价单订单批量建立发票", tags = {"发票" },  notes = "根据客户联系人商机报价单订单批量建立发票")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/batch")
    public ResponseEntity<Boolean> createBatchByAccountContactOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody List<InvoiceDTO> invoicedtos) {
        List<Invoice> domainlist=invoiceMapping.toDomain(invoicedtos);
        for(Invoice domain:domainlist){
            domain.setSalesorderid(salesorder_id);
        }
        invoiceService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "invoice" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.invoiceService.get(#invoice_id),'iBizBusinessCentral-Invoice-Update')")
    @ApiOperation(value = "根据客户联系人商机报价单订单更新发票", tags = {"发票" },  notes = "根据客户联系人商机报价单订单更新发票")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/{invoice_id}")
    public ResponseEntity<InvoiceDTO> updateByAccountContactOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @PathVariable("invoice_id") String invoice_id, @RequestBody InvoiceDTO invoicedto) {
        Invoice domain = invoiceMapping.toDomain(invoicedto);
        domain.setSalesorderid(salesorder_id);
        domain.setInvoiceid(invoice_id);
		invoiceService.update(domain);
        InvoiceDTO dto = invoiceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.invoiceService.getInvoiceByEntities(this.invoiceMapping.toDomain(#invoicedtos)),'iBizBusinessCentral-Invoice-Update')")
    @ApiOperation(value = "根据客户联系人商机报价单订单批量更新发票", tags = {"发票" },  notes = "根据客户联系人商机报价单订单批量更新发票")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/batch")
    public ResponseEntity<Boolean> updateBatchByAccountContactOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody List<InvoiceDTO> invoicedtos) {
        List<Invoice> domainlist=invoiceMapping.toDomain(invoicedtos);
        for(Invoice domain:domainlist){
            domain.setSalesorderid(salesorder_id);
        }
        invoiceService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.invoiceService.get(#invoice_id),'iBizBusinessCentral-Invoice-Remove')")
    @ApiOperation(value = "根据客户联系人商机报价单订单删除发票", tags = {"发票" },  notes = "根据客户联系人商机报价单订单删除发票")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/{invoice_id}")
    public ResponseEntity<Boolean> removeByAccountContactOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @PathVariable("invoice_id") String invoice_id) {
		return ResponseEntity.status(HttpStatus.OK).body(invoiceService.remove(invoice_id));
    }

    @PreAuthorize("hasPermission(this.invoiceService.getInvoiceByIds(#ids),'iBizBusinessCentral-Invoice-Remove')")
    @ApiOperation(value = "根据客户联系人商机报价单订单批量删除发票", tags = {"发票" },  notes = "根据客户联系人商机报价单订单批量删除发票")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/batch")
    public ResponseEntity<Boolean> removeBatchByAccountContactOpportunityQuoteSalesOrder(@RequestBody List<String> ids) {
        invoiceService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.invoiceMapping.toDomain(returnObject.body),'iBizBusinessCentral-Invoice-Get')")
    @ApiOperation(value = "根据客户联系人商机报价单订单获取发票", tags = {"发票" },  notes = "根据客户联系人商机报价单订单获取发票")
	@RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/{invoice_id}")
    public ResponseEntity<InvoiceDTO> getByAccountContactOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @PathVariable("invoice_id") String invoice_id) {
        Invoice domain = invoiceService.get(invoice_id);
        InvoiceDTO dto = invoiceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据客户联系人商机报价单订单获取发票草稿", tags = {"发票" },  notes = "根据客户联系人商机报价单订单获取发票草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/getdraft")
    public ResponseEntity<InvoiceDTO> getDraftByAccountContactOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id) {
        Invoice domain = new Invoice();
        domain.setSalesorderid(salesorder_id);
        return ResponseEntity.status(HttpStatus.OK).body(invoiceMapping.toDto(invoiceService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-Cancel-all')")
    @ApiOperation(value = "根据客户联系人商机报价单订单发票", tags = {"发票" },  notes = "根据客户联系人商机报价单订单发票")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/{invoice_id}/cancel")
    public ResponseEntity<InvoiceDTO> cancelByAccountContactOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @PathVariable("invoice_id") String invoice_id, @RequestBody InvoiceDTO invoicedto) {
        Invoice domain = invoiceMapping.toDomain(invoicedto);
        domain.setSalesorderid(salesorder_id);
        domain = invoiceService.cancel(domain) ;
        invoicedto = invoiceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(invoicedto);
    }

    @ApiOperation(value = "根据客户联系人商机报价单订单检查发票", tags = {"发票" },  notes = "根据客户联系人商机报价单订单检查发票")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/checkkey")
    public ResponseEntity<Boolean> checkKeyByAccountContactOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody InvoiceDTO invoicedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(invoiceService.checkKey(invoiceMapping.toDomain(invoicedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-Finish-all')")
    @ApiOperation(value = "根据客户联系人商机报价单订单发票", tags = {"发票" },  notes = "根据客户联系人商机报价单订单发票")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/{invoice_id}/finish")
    public ResponseEntity<InvoiceDTO> finishByAccountContactOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @PathVariable("invoice_id") String invoice_id, @RequestBody InvoiceDTO invoicedto) {
        Invoice domain = invoiceMapping.toDomain(invoicedto);
        domain.setSalesorderid(salesorder_id);
        domain = invoiceService.finish(domain) ;
        invoicedto = invoiceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(invoicedto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-Paid-all')")
    @ApiOperation(value = "根据客户联系人商机报价单订单发票", tags = {"发票" },  notes = "根据客户联系人商机报价单订单发票")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/{invoice_id}/paid")
    public ResponseEntity<InvoiceDTO> paidByAccountContactOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @PathVariable("invoice_id") String invoice_id, @RequestBody InvoiceDTO invoicedto) {
        Invoice domain = invoiceMapping.toDomain(invoicedto);
        domain.setSalesorderid(salesorder_id);
        domain = invoiceService.paid(domain) ;
        invoicedto = invoiceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(invoicedto);
    }

    @PreAuthorize("hasPermission(this.invoiceMapping.toDomain(#invoicedto),'iBizBusinessCentral-Invoice-Save')")
    @ApiOperation(value = "根据客户联系人商机报价单订单保存发票", tags = {"发票" },  notes = "根据客户联系人商机报价单订单保存发票")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/save")
    public ResponseEntity<Boolean> saveByAccountContactOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody InvoiceDTO invoicedto) {
        Invoice domain = invoiceMapping.toDomain(invoicedto);
        domain.setSalesorderid(salesorder_id);
        return ResponseEntity.status(HttpStatus.OK).body(invoiceService.save(domain));
    }

    @PreAuthorize("hasPermission(this.invoiceMapping.toDomain(#invoicedtos),'iBizBusinessCentral-Invoice-Save')")
    @ApiOperation(value = "根据客户联系人商机报价单订单批量保存发票", tags = {"发票" },  notes = "根据客户联系人商机报价单订单批量保存发票")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/savebatch")
    public ResponseEntity<Boolean> saveBatchByAccountContactOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody List<InvoiceDTO> invoicedtos) {
        List<Invoice> domainlist=invoiceMapping.toDomain(invoicedtos);
        for(Invoice domain:domainlist){
             domain.setSalesorderid(salesorder_id);
        }
        invoiceService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchByParentKey-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据客户联系人商机报价单订单获取ByParentKey", tags = {"发票" } ,notes = "根据客户联系人商机报价单订单获取ByParentKey")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/fetchbyparentkey")
	public ResponseEntity<List<InvoiceDTO>> fetchInvoiceByParentKeyByAccountContactOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id,InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchByParentKey(context) ;
        List<InvoiceDTO> list = invoiceMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchByParentKey-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据客户联系人商机报价单订单查询ByParentKey", tags = {"发票" } ,notes = "根据客户联系人商机报价单订单查询ByParentKey")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/searchbyparentkey")
	public ResponseEntity<Page<InvoiceDTO>> searchInvoiceByParentKeyByAccountContactOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchByParentKey(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(invoiceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchCancel-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据客户联系人商机报价单订单获取已取消", tags = {"发票" } ,notes = "根据客户联系人商机报价单订单获取已取消")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/fetchcancel")
	public ResponseEntity<List<InvoiceDTO>> fetchInvoiceCancelByAccountContactOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id,InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchCancel(context) ;
        List<InvoiceDTO> list = invoiceMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchCancel-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据客户联系人商机报价单订单查询已取消", tags = {"发票" } ,notes = "根据客户联系人商机报价单订单查询已取消")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/searchcancel")
	public ResponseEntity<Page<InvoiceDTO>> searchInvoiceCancelByAccountContactOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchCancel(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(invoiceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据客户联系人商机报价单订单获取DEFAULT", tags = {"发票" } ,notes = "根据客户联系人商机报价单订单获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/fetchdefault")
	public ResponseEntity<List<InvoiceDTO>> fetchInvoiceDefaultByAccountContactOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id,InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchDefault(context) ;
        List<InvoiceDTO> list = invoiceMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据客户联系人商机报价单订单查询DEFAULT", tags = {"发票" } ,notes = "根据客户联系人商机报价单订单查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/searchdefault")
	public ResponseEntity<Page<InvoiceDTO>> searchInvoiceDefaultByAccountContactOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(invoiceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchPaid-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据客户联系人商机报价单订单获取已支付", tags = {"发票" } ,notes = "根据客户联系人商机报价单订单获取已支付")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/fetchpaid")
	public ResponseEntity<List<InvoiceDTO>> fetchInvoicePaidByAccountContactOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id,InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchPaid(context) ;
        List<InvoiceDTO> list = invoiceMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Invoice-searchPaid-all') and hasPermission(#context,'iBizBusinessCentral-Invoice-Get')")
	@ApiOperation(value = "根据客户联系人商机报价单订单查询已支付", tags = {"发票" } ,notes = "根据客户联系人商机报价单订单查询已支付")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/salesorders/{salesorder_id}/invoices/searchpaid")
	public ResponseEntity<Page<InvoiceDTO>> searchInvoicePaidByAccountContactOpportunityQuoteSalesOrder(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @PathVariable("salesorder_id") String salesorder_id, @RequestBody InvoiceSearchContext context) {
        context.setN_salesorderid_eq(salesorder_id);
        Page<Invoice> domains = invoiceService.searchPaid(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(invoiceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

