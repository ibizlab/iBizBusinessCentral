package cn.ibizlab.businesscentral.centralapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[ConnectionDTO]
 */
@Data
public class ConnectionDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [RECORD2OBJECTTYPECODE]
     *
     */
    @JSONField(name = "record2objecttypecode")
    @JsonProperty("record2objecttypecode")
    private String record2objecttypecode;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [ENTITYIMAGE_TIMESTAMP]
     *
     */
    @JSONField(name = "entityimage_timestamp")
    @JsonProperty("entityimage_timestamp")
    private BigInteger entityimageTimestamp;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;

    /**
     * 属性 [RECORD1OBJECTTYPECODE]
     *
     */
    @JSONField(name = "record1objecttypecode")
    @JsonProperty("record1objecttypecode")
    private String record1objecttypecode;

    /**
     * 属性 [RECORD2ID]
     *
     */
    @JSONField(name = "record2id")
    @JsonProperty("record2id")
    private String record2id;

    /**
     * 属性 [MASTER]
     *
     */
    @JSONField(name = "master")
    @JsonProperty("master")
    private Integer master;

    /**
     * 属性 [RECORD1IDOBJECTTYPECODE]
     *
     */
    @JSONField(name = "record1idobjecttypecode")
    @JsonProperty("record1idobjecttypecode")
    private String record1idobjecttypecode;

    /**
     * 属性 [OWNERID]
     *
     */
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;

    /**
     * 属性 [EFFECTIVESTART]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "effectivestart" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("effectivestart")
    private Timestamp effectivestart;

    /**
     * 属性 [RECORD1ID]
     *
     */
    @JSONField(name = "record1id")
    @JsonProperty("record1id")
    private String record1id;

    /**
     * 属性 [OVERRIDDENCREATEDON]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;

    /**
     * 属性 [IMPORTSEQUENCENUMBER]
     *
     */
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;

    /**
     * 属性 [OWNERNAME]
     *
     */
    @JSONField(name = "ownername")
    @JsonProperty("ownername")
    private String ownername;

    /**
     * 属性 [OWNERTYPE]
     *
     */
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;

    /**
     * 属性 [RECORD2IDOBJECTTYPECODE]
     *
     */
    @JSONField(name = "record2idobjecttypecode")
    @JsonProperty("record2idobjecttypecode")
    private String record2idobjecttypecode;

    /**
     * 属性 [EXCHANGERATE]
     *
     */
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;

    /**
     * 属性 [ENTITYIMAGEID]
     *
     */
    @JSONField(name = "entityimageid")
    @JsonProperty("entityimageid")
    private String entityimageid;

    /**
     * 属性 [VERSIONNUMBER]
     *
     */
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;

    /**
     * 属性 [EFFECTIVEEND]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "effectiveend" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("effectiveend")
    private Timestamp effectiveend;

    /**
     * 属性 [ENTITYIMAGE_URL]
     *
     */
    @JSONField(name = "entityimage_url")
    @JsonProperty("entityimage_url")
    private String entityimageUrl;

    /**
     * 属性 [CONNECTIONNAME]
     *
     */
    @JSONField(name = "connectionname")
    @JsonProperty("connectionname")
    private String connectionname;

    /**
     * 属性 [STATUSCODE]
     *
     */
    @JSONField(name = "statuscode")
    @JsonProperty("statuscode")
    private Integer statuscode;

    /**
     * 属性 [CONNECTIONID]
     *
     */
    @JSONField(name = "connectionid")
    @JsonProperty("connectionid")
    private String connectionid;

    /**
     * 属性 [STATECODE]
     *
     */
    @JSONField(name = "statecode")
    @JsonProperty("statecode")
    private Integer statecode;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 属性 [ENTITYIMAGE]
     *
     */
    @JSONField(name = "entityimage")
    @JsonProperty("entityimage")
    private String entityimage;

    /**
     * 属性 [RECORD1ROLENAME]
     *
     */
    @JSONField(name = "record1rolename")
    @JsonProperty("record1rolename")
    private String record1rolename;

    /**
     * 属性 [RECORD2ROLENAME]
     *
     */
    @JSONField(name = "record2rolename")
    @JsonProperty("record2rolename")
    private String record2rolename;

    /**
     * 属性 [RECORD1ROLEID]
     *
     */
    @JSONField(name = "record1roleid")
    @JsonProperty("record1roleid")
    private String record1roleid;

    /**
     * 属性 [TRANSACTIONCURRENCYID]
     *
     */
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;

    /**
     * 属性 [RECORD2ROLEID]
     *
     */
    @JSONField(name = "record2roleid")
    @JsonProperty("record2roleid")
    private String record2roleid;


    /**
     * 设置 [RECORD2OBJECTTYPECODE]
     */
    public void setRecord2objecttypecode(String  record2objecttypecode){
        this.record2objecttypecode = record2objecttypecode ;
        this.modify("record2objecttypecode",record2objecttypecode);
    }

    /**
     * 设置 [ENTITYIMAGE_TIMESTAMP]
     */
    public void setEntityimageTimestamp(BigInteger  entityimageTimestamp){
        this.entityimageTimestamp = entityimageTimestamp ;
        this.modify("entityimage_timestamp",entityimageTimestamp);
    }

    /**
     * 设置 [RECORD1OBJECTTYPECODE]
     */
    public void setRecord1objecttypecode(String  record1objecttypecode){
        this.record1objecttypecode = record1objecttypecode ;
        this.modify("record1objecttypecode",record1objecttypecode);
    }

    /**
     * 设置 [RECORD2ID]
     */
    public void setRecord2id(String  record2id){
        this.record2id = record2id ;
        this.modify("record2id",record2id);
    }

    /**
     * 设置 [MASTER]
     */
    public void setMaster(Integer  master){
        this.master = master ;
        this.modify("master",master);
    }

    /**
     * 设置 [RECORD1IDOBJECTTYPECODE]
     */
    public void setRecord1idobjecttypecode(String  record1idobjecttypecode){
        this.record1idobjecttypecode = record1idobjecttypecode ;
        this.modify("record1idobjecttypecode",record1idobjecttypecode);
    }

    /**
     * 设置 [OWNERID]
     */
    public void setOwnerid(String  ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [EFFECTIVESTART]
     */
    public void setEffectivestart(Timestamp  effectivestart){
        this.effectivestart = effectivestart ;
        this.modify("effectivestart",effectivestart);
    }

    /**
     * 设置 [RECORD1ID]
     */
    public void setRecord1id(String  record1id){
        this.record1id = record1id ;
        this.modify("record1id",record1id);
    }

    /**
     * 设置 [OVERRIDDENCREATEDON]
     */
    public void setOverriddencreatedon(Timestamp  overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 设置 [IMPORTSEQUENCENUMBER]
     */
    public void setImportsequencenumber(Integer  importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [OWNERNAME]
     */
    public void setOwnername(String  ownername){
        this.ownername = ownername ;
        this.modify("ownername",ownername);
    }

    /**
     * 设置 [OWNERTYPE]
     */
    public void setOwnertype(String  ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [RECORD2IDOBJECTTYPECODE]
     */
    public void setRecord2idobjecttypecode(String  record2idobjecttypecode){
        this.record2idobjecttypecode = record2idobjecttypecode ;
        this.modify("record2idobjecttypecode",record2idobjecttypecode);
    }

    /**
     * 设置 [EXCHANGERATE]
     */
    public void setExchangerate(BigDecimal  exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [ENTITYIMAGEID]
     */
    public void setEntityimageid(String  entityimageid){
        this.entityimageid = entityimageid ;
        this.modify("entityimageid",entityimageid);
    }

    /**
     * 设置 [VERSIONNUMBER]
     */
    public void setVersionnumber(BigInteger  versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [EFFECTIVEEND]
     */
    public void setEffectiveend(Timestamp  effectiveend){
        this.effectiveend = effectiveend ;
        this.modify("effectiveend",effectiveend);
    }

    /**
     * 设置 [ENTITYIMAGE_URL]
     */
    public void setEntityimageUrl(String  entityimageUrl){
        this.entityimageUrl = entityimageUrl ;
        this.modify("entityimage_url",entityimageUrl);
    }

    /**
     * 设置 [CONNECTIONNAME]
     */
    public void setConnectionname(String  connectionname){
        this.connectionname = connectionname ;
        this.modify("connectionname",connectionname);
    }

    /**
     * 设置 [STATUSCODE]
     */
    public void setStatuscode(Integer  statuscode){
        this.statuscode = statuscode ;
        this.modify("statuscode",statuscode);
    }

    /**
     * 设置 [STATECODE]
     */
    public void setStatecode(Integer  statecode){
        this.statecode = statecode ;
        this.modify("statecode",statecode);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [ENTITYIMAGE]
     */
    public void setEntityimage(String  entityimage){
        this.entityimage = entityimage ;
        this.modify("entityimage",entityimage);
    }

    /**
     * 设置 [RECORD1ROLENAME]
     */
    public void setRecord1rolename(String  record1rolename){
        this.record1rolename = record1rolename ;
        this.modify("record1rolename",record1rolename);
    }

    /**
     * 设置 [RECORD2ROLENAME]
     */
    public void setRecord2rolename(String  record2rolename){
        this.record2rolename = record2rolename ;
        this.modify("record2rolename",record2rolename);
    }

    /**
     * 设置 [RECORD1ROLEID]
     */
    public void setRecord1roleid(String  record1roleid){
        this.record1roleid = record1roleid ;
        this.modify("record1roleid",record1roleid);
    }

    /**
     * 设置 [TRANSACTIONCURRENCYID]
     */
    public void setTransactioncurrencyid(String  transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }

    /**
     * 设置 [RECORD2ROLEID]
     */
    public void setRecord2roleid(String  record2roleid){
        this.record2roleid = record2roleid ;
        this.modify("record2roleid",record2roleid);
    }


}

