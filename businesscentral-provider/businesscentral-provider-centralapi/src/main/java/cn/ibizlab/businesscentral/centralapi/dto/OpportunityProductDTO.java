package cn.ibizlab.businesscentral.centralapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[OpportunityProductDTO]
 */
@Data
public class OpportunityProductDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [PRICEOVERRIDDEN]
     *
     */
    @JSONField(name = "priceoverridden")
    @JsonProperty("priceoverridden")
    private Integer priceoverridden;

    /**
     * 属性 [TAX_BASE]
     *
     */
    @JSONField(name = "tax_base")
    @JsonProperty("tax_base")
    private BigDecimal taxBase;

    /**
     * 属性 [SKIPPRICECALCULATION]
     *
     */
    @JSONField(name = "skippricecalculation")
    @JsonProperty("skippricecalculation")
    private String skippricecalculation;

    /**
     * 属性 [VOLUMEDISCOUNTAMOUNT_BASE]
     *
     */
    @JSONField(name = "volumediscountamount_base")
    @JsonProperty("volumediscountamount_base")
    private BigDecimal volumediscountamountBase;

    /**
     * 属性 [MANUALDISCOUNTAMOUNT]
     *
     */
    @JSONField(name = "manualdiscountamount")
    @JsonProperty("manualdiscountamount")
    private BigDecimal manualdiscountamount;

    /**
     * 属性 [ENTITYIMAGE_TIMESTAMP]
     *
     */
    @JSONField(name = "entityimage_timestamp")
    @JsonProperty("entityimage_timestamp")
    private BigInteger entityimageTimestamp;

    /**
     * 属性 [OPPORTUNITYSTATECODE]
     *
     */
    @JSONField(name = "opportunitystatecode")
    @JsonProperty("opportunitystatecode")
    private String opportunitystatecode;

    /**
     * 属性 [OWNERTYPE]
     *
     */
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;

    /**
     * 属性 [VOLUMEDISCOUNTAMOUNT]
     *
     */
    @JSONField(name = "volumediscountamount")
    @JsonProperty("volumediscountamount")
    private BigDecimal volumediscountamount;

    /**
     * 属性 [EXTENDEDAMOUNT_BASE]
     *
     */
    @JSONField(name = "extendedamount_base")
    @JsonProperty("extendedamount_base")
    private BigDecimal extendedamountBase;

    /**
     * 属性 [OWNERNAME]
     *
     */
    @JSONField(name = "ownername")
    @JsonProperty("ownername")
    private String ownername;

    /**
     * 属性 [PRICEPERUNIT_BASE]
     *
     */
    @JSONField(name = "priceperunit_base")
    @JsonProperty("priceperunit_base")
    private BigDecimal priceperunitBase;

    /**
     * 属性 [OPPORTUNITYPRODUCTID]
     *
     */
    @JSONField(name = "opportunityproductid")
    @JsonProperty("opportunityproductid")
    private String opportunityproductid;

    /**
     * 属性 [VERSIONNUMBER]
     *
     */
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;

    /**
     * 属性 [ENTITYIMAGEID]
     *
     */
    @JSONField(name = "entityimageid")
    @JsonProperty("entityimageid")
    private String entityimageid;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 属性 [MANUALDISCOUNTAMOUNT_BASE]
     *
     */
    @JSONField(name = "manualdiscountamount_base")
    @JsonProperty("manualdiscountamount_base")
    private BigDecimal manualdiscountamountBase;

    /**
     * 属性 [SEQUENCENUMBER]
     *
     */
    @JSONField(name = "sequencenumber")
    @JsonProperty("sequencenumber")
    private Integer sequencenumber;

    /**
     * 属性 [PRODUCTDESCRIPTION]
     *
     */
    @JSONField(name = "productdescription")
    @JsonProperty("productdescription")
    private String productdescription;

    /**
     * 属性 [PRODUCTOVERRIDDEN]
     *
     */
    @JSONField(name = "productoverridden")
    @JsonProperty("productoverridden")
    private Integer productoverridden;

    /**
     * 属性 [PRODUCTNAME]
     *
     */
    @JSONField(name = "productname")
    @JsonProperty("productname")
    private String productname;

    /**
     * 属性 [EXCHANGERATE]
     *
     */
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;

    /**
     * 属性 [OPPORTUNITYPRODUCTNAME]
     *
     */
    @JSONField(name = "opportunityproductname")
    @JsonProperty("opportunityproductname")
    private String opportunityproductname;

    /**
     * 属性 [PROPERTYCONFIGURATIONSTATUS]
     *
     */
    @JSONField(name = "propertyconfigurationstatus")
    @JsonProperty("propertyconfigurationstatus")
    private String propertyconfigurationstatus;

    /**
     * 属性 [TAX]
     *
     */
    @JSONField(name = "tax")
    @JsonProperty("tax")
    private BigDecimal tax;

    /**
     * 属性 [PRICEPERUNIT]
     *
     */
    @JSONField(name = "priceperunit")
    @JsonProperty("priceperunit")
    private BigDecimal priceperunit;

    /**
     * 属性 [PARENTBUNDLEID]
     *
     */
    @JSONField(name = "parentbundleid")
    @JsonProperty("parentbundleid")
    private String parentbundleid;

    /**
     * 属性 [UTCCONVERSIONTIMEZONECODE]
     *
     */
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;

    /**
     * 属性 [PRODUCTASSOCIATIONID]
     *
     */
    @JSONField(name = "productassociationid")
    @JsonProperty("productassociationid")
    private String productassociationid;

    /**
     * 属性 [BASEAMOUNT]
     *
     */
    @JSONField(name = "baseamount")
    @JsonProperty("baseamount")
    private BigDecimal baseamount;

    /**
     * 属性 [PRODUCTTYPECODE]
     *
     */
    @JSONField(name = "producttypecode")
    @JsonProperty("producttypecode")
    private String producttypecode;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [TIMEZONERULEVERSIONNUMBER]
     *
     */
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;

    /**
     * 属性 [QUANTITY]
     *
     */
    @JSONField(name = "quantity")
    @JsonProperty("quantity")
    private BigDecimal quantity;

    /**
     * 属性 [LINEITEMNUMBER]
     *
     */
    @JSONField(name = "lineitemnumber")
    @JsonProperty("lineitemnumber")
    private Integer lineitemnumber;

    /**
     * 属性 [OVERRIDDENCREATEDON]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [EXTENDEDAMOUNT]
     *
     */
    @JSONField(name = "extendedamount")
    @JsonProperty("extendedamount")
    private BigDecimal extendedamount;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;

    /**
     * 属性 [ENTITYIMAGE_URL]
     *
     */
    @JSONField(name = "entityimage_url")
    @JsonProperty("entityimage_url")
    private String entityimageUrl;

    /**
     * 属性 [PRICINGERRORCODE]
     *
     */
    @JSONField(name = "pricingerrorcode")
    @JsonProperty("pricingerrorcode")
    private String pricingerrorcode;

    /**
     * 属性 [ENTITYIMAGE]
     *
     */
    @JSONField(name = "entityimage")
    @JsonProperty("entityimage")
    private String entityimage;

    /**
     * 属性 [BASEAMOUNT_BASE]
     *
     */
    @JSONField(name = "baseamount_base")
    @JsonProperty("baseamount_base")
    private BigDecimal baseamountBase;

    /**
     * 属性 [IMPORTSEQUENCENUMBER]
     *
     */
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;

    /**
     * 属性 [PRODUCTID]
     *
     */
    @JSONField(name = "productid")
    @JsonProperty("productid")
    private String productid;

    /**
     * 属性 [PARENTBUNDLEIDREF]
     *
     */
    @JSONField(name = "parentbundleidref")
    @JsonProperty("parentbundleidref")
    private String parentbundleidref;

    /**
     * 属性 [UOMID]
     *
     */
    @JSONField(name = "uomid")
    @JsonProperty("uomid")
    private String uomid;

    /**
     * 属性 [TRANSACTIONCURRENCYID]
     *
     */
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;

    /**
     * 属性 [OPPORTUNITYID]
     *
     */
    @JSONField(name = "opportunityid")
    @JsonProperty("opportunityid")
    private String opportunityid;


    /**
     * 设置 [PRICEOVERRIDDEN]
     */
    public void setPriceoverridden(Integer  priceoverridden){
        this.priceoverridden = priceoverridden ;
        this.modify("priceoverridden",priceoverridden);
    }

    /**
     * 设置 [TAX_BASE]
     */
    public void setTaxBase(BigDecimal  taxBase){
        this.taxBase = taxBase ;
        this.modify("tax_base",taxBase);
    }

    /**
     * 设置 [SKIPPRICECALCULATION]
     */
    public void setSkippricecalculation(String  skippricecalculation){
        this.skippricecalculation = skippricecalculation ;
        this.modify("skippricecalculation",skippricecalculation);
    }

    /**
     * 设置 [VOLUMEDISCOUNTAMOUNT_BASE]
     */
    public void setVolumediscountamountBase(BigDecimal  volumediscountamountBase){
        this.volumediscountamountBase = volumediscountamountBase ;
        this.modify("volumediscountamount_base",volumediscountamountBase);
    }

    /**
     * 设置 [MANUALDISCOUNTAMOUNT]
     */
    public void setManualdiscountamount(BigDecimal  manualdiscountamount){
        this.manualdiscountamount = manualdiscountamount ;
        this.modify("manualdiscountamount",manualdiscountamount);
    }

    /**
     * 设置 [ENTITYIMAGE_TIMESTAMP]
     */
    public void setEntityimageTimestamp(BigInteger  entityimageTimestamp){
        this.entityimageTimestamp = entityimageTimestamp ;
        this.modify("entityimage_timestamp",entityimageTimestamp);
    }

    /**
     * 设置 [OPPORTUNITYSTATECODE]
     */
    public void setOpportunitystatecode(String  opportunitystatecode){
        this.opportunitystatecode = opportunitystatecode ;
        this.modify("opportunitystatecode",opportunitystatecode);
    }

    /**
     * 设置 [OWNERTYPE]
     */
    public void setOwnertype(String  ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [VOLUMEDISCOUNTAMOUNT]
     */
    public void setVolumediscountamount(BigDecimal  volumediscountamount){
        this.volumediscountamount = volumediscountamount ;
        this.modify("volumediscountamount",volumediscountamount);
    }

    /**
     * 设置 [EXTENDEDAMOUNT_BASE]
     */
    public void setExtendedamountBase(BigDecimal  extendedamountBase){
        this.extendedamountBase = extendedamountBase ;
        this.modify("extendedamount_base",extendedamountBase);
    }

    /**
     * 设置 [OWNERNAME]
     */
    public void setOwnername(String  ownername){
        this.ownername = ownername ;
        this.modify("ownername",ownername);
    }

    /**
     * 设置 [PRICEPERUNIT_BASE]
     */
    public void setPriceperunitBase(BigDecimal  priceperunitBase){
        this.priceperunitBase = priceperunitBase ;
        this.modify("priceperunit_base",priceperunitBase);
    }

    /**
     * 设置 [VERSIONNUMBER]
     */
    public void setVersionnumber(BigInteger  versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [ENTITYIMAGEID]
     */
    public void setEntityimageid(String  entityimageid){
        this.entityimageid = entityimageid ;
        this.modify("entityimageid",entityimageid);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [MANUALDISCOUNTAMOUNT_BASE]
     */
    public void setManualdiscountamountBase(BigDecimal  manualdiscountamountBase){
        this.manualdiscountamountBase = manualdiscountamountBase ;
        this.modify("manualdiscountamount_base",manualdiscountamountBase);
    }

    /**
     * 设置 [SEQUENCENUMBER]
     */
    public void setSequencenumber(Integer  sequencenumber){
        this.sequencenumber = sequencenumber ;
        this.modify("sequencenumber",sequencenumber);
    }

    /**
     * 设置 [PRODUCTDESCRIPTION]
     */
    public void setProductdescription(String  productdescription){
        this.productdescription = productdescription ;
        this.modify("productdescription",productdescription);
    }

    /**
     * 设置 [PRODUCTOVERRIDDEN]
     */
    public void setProductoverridden(Integer  productoverridden){
        this.productoverridden = productoverridden ;
        this.modify("productoverridden",productoverridden);
    }

    /**
     * 设置 [PRODUCTNAME]
     */
    public void setProductname(String  productname){
        this.productname = productname ;
        this.modify("productname",productname);
    }

    /**
     * 设置 [EXCHANGERATE]
     */
    public void setExchangerate(BigDecimal  exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [OPPORTUNITYPRODUCTNAME]
     */
    public void setOpportunityproductname(String  opportunityproductname){
        this.opportunityproductname = opportunityproductname ;
        this.modify("opportunityproductname",opportunityproductname);
    }

    /**
     * 设置 [PROPERTYCONFIGURATIONSTATUS]
     */
    public void setPropertyconfigurationstatus(String  propertyconfigurationstatus){
        this.propertyconfigurationstatus = propertyconfigurationstatus ;
        this.modify("propertyconfigurationstatus",propertyconfigurationstatus);
    }

    /**
     * 设置 [TAX]
     */
    public void setTax(BigDecimal  tax){
        this.tax = tax ;
        this.modify("tax",tax);
    }

    /**
     * 设置 [PRICEPERUNIT]
     */
    public void setPriceperunit(BigDecimal  priceperunit){
        this.priceperunit = priceperunit ;
        this.modify("priceperunit",priceperunit);
    }

    /**
     * 设置 [PARENTBUNDLEID]
     */
    public void setParentbundleid(String  parentbundleid){
        this.parentbundleid = parentbundleid ;
        this.modify("parentbundleid",parentbundleid);
    }

    /**
     * 设置 [UTCCONVERSIONTIMEZONECODE]
     */
    public void setUtcconversiontimezonecode(Integer  utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [PRODUCTASSOCIATIONID]
     */
    public void setProductassociationid(String  productassociationid){
        this.productassociationid = productassociationid ;
        this.modify("productassociationid",productassociationid);
    }

    /**
     * 设置 [BASEAMOUNT]
     */
    public void setBaseamount(BigDecimal  baseamount){
        this.baseamount = baseamount ;
        this.modify("baseamount",baseamount);
    }

    /**
     * 设置 [PRODUCTTYPECODE]
     */
    public void setProducttypecode(String  producttypecode){
        this.producttypecode = producttypecode ;
        this.modify("producttypecode",producttypecode);
    }

    /**
     * 设置 [TIMEZONERULEVERSIONNUMBER]
     */
    public void setTimezoneruleversionnumber(Integer  timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [QUANTITY]
     */
    public void setQuantity(BigDecimal  quantity){
        this.quantity = quantity ;
        this.modify("quantity",quantity);
    }

    /**
     * 设置 [LINEITEMNUMBER]
     */
    public void setLineitemnumber(Integer  lineitemnumber){
        this.lineitemnumber = lineitemnumber ;
        this.modify("lineitemnumber",lineitemnumber);
    }

    /**
     * 设置 [OVERRIDDENCREATEDON]
     */
    public void setOverriddencreatedon(Timestamp  overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 设置 [EXTENDEDAMOUNT]
     */
    public void setExtendedamount(BigDecimal  extendedamount){
        this.extendedamount = extendedamount ;
        this.modify("extendedamount",extendedamount);
    }

    /**
     * 设置 [ENTITYIMAGE_URL]
     */
    public void setEntityimageUrl(String  entityimageUrl){
        this.entityimageUrl = entityimageUrl ;
        this.modify("entityimage_url",entityimageUrl);
    }

    /**
     * 设置 [PRICINGERRORCODE]
     */
    public void setPricingerrorcode(String  pricingerrorcode){
        this.pricingerrorcode = pricingerrorcode ;
        this.modify("pricingerrorcode",pricingerrorcode);
    }

    /**
     * 设置 [ENTITYIMAGE]
     */
    public void setEntityimage(String  entityimage){
        this.entityimage = entityimage ;
        this.modify("entityimage",entityimage);
    }

    /**
     * 设置 [BASEAMOUNT_BASE]
     */
    public void setBaseamountBase(BigDecimal  baseamountBase){
        this.baseamountBase = baseamountBase ;
        this.modify("baseamount_base",baseamountBase);
    }

    /**
     * 设置 [IMPORTSEQUENCENUMBER]
     */
    public void setImportsequencenumber(Integer  importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [PRODUCTID]
     */
    public void setProductid(String  productid){
        this.productid = productid ;
        this.modify("productid",productid);
    }

    /**
     * 设置 [PARENTBUNDLEIDREF]
     */
    public void setParentbundleidref(String  parentbundleidref){
        this.parentbundleidref = parentbundleidref ;
        this.modify("parentbundleidref",parentbundleidref);
    }

    /**
     * 设置 [UOMID]
     */
    public void setUomid(String  uomid){
        this.uomid = uomid ;
        this.modify("uomid",uomid);
    }

    /**
     * 设置 [TRANSACTIONCURRENCYID]
     */
    public void setTransactioncurrencyid(String  transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }

    /**
     * 设置 [OPPORTUNITYID]
     */
    public void setOpportunityid(String  opportunityid){
        this.opportunityid = opportunityid ;
        this.modify("opportunityid",opportunityid);
    }


}

