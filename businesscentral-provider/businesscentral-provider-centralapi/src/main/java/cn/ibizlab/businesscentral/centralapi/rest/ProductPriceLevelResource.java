package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.product.domain.ProductPriceLevel;
import cn.ibizlab.businesscentral.core.product.service.IProductPriceLevelService;
import cn.ibizlab.businesscentral.core.product.filter.ProductPriceLevelSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"价目表项" })
@RestController("CentralApi-productpricelevel")
@RequestMapping("")
public class ProductPriceLevelResource {

    @Autowired
    public IProductPriceLevelService productpricelevelService;

    @Autowired
    @Lazy
    public ProductPriceLevelMapping productpricelevelMapping;

    @PreAuthorize("hasPermission(this.productpricelevelMapping.toDomain(#productpriceleveldto),'iBizBusinessCentral-ProductPriceLevel-Create')")
    @ApiOperation(value = "新建价目表项", tags = {"价目表项" },  notes = "新建价目表项")
	@RequestMapping(method = RequestMethod.POST, value = "/productpricelevels")
    public ResponseEntity<ProductPriceLevelDTO> create(@RequestBody ProductPriceLevelDTO productpriceleveldto) {
        ProductPriceLevel domain = productpricelevelMapping.toDomain(productpriceleveldto);
		productpricelevelService.create(domain);
        ProductPriceLevelDTO dto = productpricelevelMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.productpricelevelMapping.toDomain(#productpriceleveldtos),'iBizBusinessCentral-ProductPriceLevel-Create')")
    @ApiOperation(value = "批量新建价目表项", tags = {"价目表项" },  notes = "批量新建价目表项")
	@RequestMapping(method = RequestMethod.POST, value = "/productpricelevels/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<ProductPriceLevelDTO> productpriceleveldtos) {
        productpricelevelService.createBatch(productpricelevelMapping.toDomain(productpriceleveldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "productpricelevel" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.productpricelevelService.get(#productpricelevel_id),'iBizBusinessCentral-ProductPriceLevel-Update')")
    @ApiOperation(value = "更新价目表项", tags = {"价目表项" },  notes = "更新价目表项")
	@RequestMapping(method = RequestMethod.PUT, value = "/productpricelevels/{productpricelevel_id}")
    public ResponseEntity<ProductPriceLevelDTO> update(@PathVariable("productpricelevel_id") String productpricelevel_id, @RequestBody ProductPriceLevelDTO productpriceleveldto) {
		ProductPriceLevel domain  = productpricelevelMapping.toDomain(productpriceleveldto);
        domain .setProductpricelevelid(productpricelevel_id);
		productpricelevelService.update(domain );
		ProductPriceLevelDTO dto = productpricelevelMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.productpricelevelService.getProductpricelevelByEntities(this.productpricelevelMapping.toDomain(#productpriceleveldtos)),'iBizBusinessCentral-ProductPriceLevel-Update')")
    @ApiOperation(value = "批量更新价目表项", tags = {"价目表项" },  notes = "批量更新价目表项")
	@RequestMapping(method = RequestMethod.PUT, value = "/productpricelevels/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<ProductPriceLevelDTO> productpriceleveldtos) {
        productpricelevelService.updateBatch(productpricelevelMapping.toDomain(productpriceleveldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.productpricelevelService.get(#productpricelevel_id),'iBizBusinessCentral-ProductPriceLevel-Remove')")
    @ApiOperation(value = "删除价目表项", tags = {"价目表项" },  notes = "删除价目表项")
	@RequestMapping(method = RequestMethod.DELETE, value = "/productpricelevels/{productpricelevel_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("productpricelevel_id") String productpricelevel_id) {
         return ResponseEntity.status(HttpStatus.OK).body(productpricelevelService.remove(productpricelevel_id));
    }

    @PreAuthorize("hasPermission(this.productpricelevelService.getProductpricelevelByIds(#ids),'iBizBusinessCentral-ProductPriceLevel-Remove')")
    @ApiOperation(value = "批量删除价目表项", tags = {"价目表项" },  notes = "批量删除价目表项")
	@RequestMapping(method = RequestMethod.DELETE, value = "/productpricelevels/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        productpricelevelService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.productpricelevelMapping.toDomain(returnObject.body),'iBizBusinessCentral-ProductPriceLevel-Get')")
    @ApiOperation(value = "获取价目表项", tags = {"价目表项" },  notes = "获取价目表项")
	@RequestMapping(method = RequestMethod.GET, value = "/productpricelevels/{productpricelevel_id}")
    public ResponseEntity<ProductPriceLevelDTO> get(@PathVariable("productpricelevel_id") String productpricelevel_id) {
        ProductPriceLevel domain = productpricelevelService.get(productpricelevel_id);
        ProductPriceLevelDTO dto = productpricelevelMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取价目表项草稿", tags = {"价目表项" },  notes = "获取价目表项草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/productpricelevels/getdraft")
    public ResponseEntity<ProductPriceLevelDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(productpricelevelMapping.toDto(productpricelevelService.getDraft(new ProductPriceLevel())));
    }

    @ApiOperation(value = "检查价目表项", tags = {"价目表项" },  notes = "检查价目表项")
	@RequestMapping(method = RequestMethod.POST, value = "/productpricelevels/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody ProductPriceLevelDTO productpriceleveldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(productpricelevelService.checkKey(productpricelevelMapping.toDomain(productpriceleveldto)));
    }

    @PreAuthorize("hasPermission(this.productpricelevelMapping.toDomain(#productpriceleveldto),'iBizBusinessCentral-ProductPriceLevel-Save')")
    @ApiOperation(value = "保存价目表项", tags = {"价目表项" },  notes = "保存价目表项")
	@RequestMapping(method = RequestMethod.POST, value = "/productpricelevels/save")
    public ResponseEntity<Boolean> save(@RequestBody ProductPriceLevelDTO productpriceleveldto) {
        return ResponseEntity.status(HttpStatus.OK).body(productpricelevelService.save(productpricelevelMapping.toDomain(productpriceleveldto)));
    }

    @PreAuthorize("hasPermission(this.productpricelevelMapping.toDomain(#productpriceleveldtos),'iBizBusinessCentral-ProductPriceLevel-Save')")
    @ApiOperation(value = "批量保存价目表项", tags = {"价目表项" },  notes = "批量保存价目表项")
	@RequestMapping(method = RequestMethod.POST, value = "/productpricelevels/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<ProductPriceLevelDTO> productpriceleveldtos) {
        productpricelevelService.saveBatch(productpricelevelMapping.toDomain(productpriceleveldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ProductPriceLevel-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-ProductPriceLevel-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"价目表项" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/productpricelevels/fetchdefault")
	public ResponseEntity<List<ProductPriceLevelDTO>> fetchDefault(ProductPriceLevelSearchContext context) {
        Page<ProductPriceLevel> domains = productpricelevelService.searchDefault(context) ;
        List<ProductPriceLevelDTO> list = productpricelevelMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ProductPriceLevel-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-ProductPriceLevel-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"价目表项" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/productpricelevels/searchdefault")
	public ResponseEntity<Page<ProductPriceLevelDTO>> searchDefault(@RequestBody ProductPriceLevelSearchContext context) {
        Page<ProductPriceLevel> domains = productpricelevelService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(productpricelevelMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.productpricelevelMapping.toDomain(#productpriceleveldto),'iBizBusinessCentral-ProductPriceLevel-Create')")
    @ApiOperation(value = "根据产品建立价目表项", tags = {"价目表项" },  notes = "根据产品建立价目表项")
	@RequestMapping(method = RequestMethod.POST, value = "/products/{product_id}/productpricelevels")
    public ResponseEntity<ProductPriceLevelDTO> createByProduct(@PathVariable("product_id") String product_id, @RequestBody ProductPriceLevelDTO productpriceleveldto) {
        ProductPriceLevel domain = productpricelevelMapping.toDomain(productpriceleveldto);
        domain.setProductid(product_id);
		productpricelevelService.create(domain);
        ProductPriceLevelDTO dto = productpricelevelMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.productpricelevelMapping.toDomain(#productpriceleveldtos),'iBizBusinessCentral-ProductPriceLevel-Create')")
    @ApiOperation(value = "根据产品批量建立价目表项", tags = {"价目表项" },  notes = "根据产品批量建立价目表项")
	@RequestMapping(method = RequestMethod.POST, value = "/products/{product_id}/productpricelevels/batch")
    public ResponseEntity<Boolean> createBatchByProduct(@PathVariable("product_id") String product_id, @RequestBody List<ProductPriceLevelDTO> productpriceleveldtos) {
        List<ProductPriceLevel> domainlist=productpricelevelMapping.toDomain(productpriceleveldtos);
        for(ProductPriceLevel domain:domainlist){
            domain.setProductid(product_id);
        }
        productpricelevelService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "productpricelevel" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.productpricelevelService.get(#productpricelevel_id),'iBizBusinessCentral-ProductPriceLevel-Update')")
    @ApiOperation(value = "根据产品更新价目表项", tags = {"价目表项" },  notes = "根据产品更新价目表项")
	@RequestMapping(method = RequestMethod.PUT, value = "/products/{product_id}/productpricelevels/{productpricelevel_id}")
    public ResponseEntity<ProductPriceLevelDTO> updateByProduct(@PathVariable("product_id") String product_id, @PathVariable("productpricelevel_id") String productpricelevel_id, @RequestBody ProductPriceLevelDTO productpriceleveldto) {
        ProductPriceLevel domain = productpricelevelMapping.toDomain(productpriceleveldto);
        domain.setProductid(product_id);
        domain.setProductpricelevelid(productpricelevel_id);
		productpricelevelService.update(domain);
        ProductPriceLevelDTO dto = productpricelevelMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.productpricelevelService.getProductpricelevelByEntities(this.productpricelevelMapping.toDomain(#productpriceleveldtos)),'iBizBusinessCentral-ProductPriceLevel-Update')")
    @ApiOperation(value = "根据产品批量更新价目表项", tags = {"价目表项" },  notes = "根据产品批量更新价目表项")
	@RequestMapping(method = RequestMethod.PUT, value = "/products/{product_id}/productpricelevels/batch")
    public ResponseEntity<Boolean> updateBatchByProduct(@PathVariable("product_id") String product_id, @RequestBody List<ProductPriceLevelDTO> productpriceleveldtos) {
        List<ProductPriceLevel> domainlist=productpricelevelMapping.toDomain(productpriceleveldtos);
        for(ProductPriceLevel domain:domainlist){
            domain.setProductid(product_id);
        }
        productpricelevelService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.productpricelevelService.get(#productpricelevel_id),'iBizBusinessCentral-ProductPriceLevel-Remove')")
    @ApiOperation(value = "根据产品删除价目表项", tags = {"价目表项" },  notes = "根据产品删除价目表项")
	@RequestMapping(method = RequestMethod.DELETE, value = "/products/{product_id}/productpricelevels/{productpricelevel_id}")
    public ResponseEntity<Boolean> removeByProduct(@PathVariable("product_id") String product_id, @PathVariable("productpricelevel_id") String productpricelevel_id) {
		return ResponseEntity.status(HttpStatus.OK).body(productpricelevelService.remove(productpricelevel_id));
    }

    @PreAuthorize("hasPermission(this.productpricelevelService.getProductpricelevelByIds(#ids),'iBizBusinessCentral-ProductPriceLevel-Remove')")
    @ApiOperation(value = "根据产品批量删除价目表项", tags = {"价目表项" },  notes = "根据产品批量删除价目表项")
	@RequestMapping(method = RequestMethod.DELETE, value = "/products/{product_id}/productpricelevels/batch")
    public ResponseEntity<Boolean> removeBatchByProduct(@RequestBody List<String> ids) {
        productpricelevelService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.productpricelevelMapping.toDomain(returnObject.body),'iBizBusinessCentral-ProductPriceLevel-Get')")
    @ApiOperation(value = "根据产品获取价目表项", tags = {"价目表项" },  notes = "根据产品获取价目表项")
	@RequestMapping(method = RequestMethod.GET, value = "/products/{product_id}/productpricelevels/{productpricelevel_id}")
    public ResponseEntity<ProductPriceLevelDTO> getByProduct(@PathVariable("product_id") String product_id, @PathVariable("productpricelevel_id") String productpricelevel_id) {
        ProductPriceLevel domain = productpricelevelService.get(productpricelevel_id);
        ProductPriceLevelDTO dto = productpricelevelMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据产品获取价目表项草稿", tags = {"价目表项" },  notes = "根据产品获取价目表项草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/products/{product_id}/productpricelevels/getdraft")
    public ResponseEntity<ProductPriceLevelDTO> getDraftByProduct(@PathVariable("product_id") String product_id) {
        ProductPriceLevel domain = new ProductPriceLevel();
        domain.setProductid(product_id);
        return ResponseEntity.status(HttpStatus.OK).body(productpricelevelMapping.toDto(productpricelevelService.getDraft(domain)));
    }

    @ApiOperation(value = "根据产品检查价目表项", tags = {"价目表项" },  notes = "根据产品检查价目表项")
	@RequestMapping(method = RequestMethod.POST, value = "/products/{product_id}/productpricelevels/checkkey")
    public ResponseEntity<Boolean> checkKeyByProduct(@PathVariable("product_id") String product_id, @RequestBody ProductPriceLevelDTO productpriceleveldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(productpricelevelService.checkKey(productpricelevelMapping.toDomain(productpriceleveldto)));
    }

    @PreAuthorize("hasPermission(this.productpricelevelMapping.toDomain(#productpriceleveldto),'iBizBusinessCentral-ProductPriceLevel-Save')")
    @ApiOperation(value = "根据产品保存价目表项", tags = {"价目表项" },  notes = "根据产品保存价目表项")
	@RequestMapping(method = RequestMethod.POST, value = "/products/{product_id}/productpricelevels/save")
    public ResponseEntity<Boolean> saveByProduct(@PathVariable("product_id") String product_id, @RequestBody ProductPriceLevelDTO productpriceleveldto) {
        ProductPriceLevel domain = productpricelevelMapping.toDomain(productpriceleveldto);
        domain.setProductid(product_id);
        return ResponseEntity.status(HttpStatus.OK).body(productpricelevelService.save(domain));
    }

    @PreAuthorize("hasPermission(this.productpricelevelMapping.toDomain(#productpriceleveldtos),'iBizBusinessCentral-ProductPriceLevel-Save')")
    @ApiOperation(value = "根据产品批量保存价目表项", tags = {"价目表项" },  notes = "根据产品批量保存价目表项")
	@RequestMapping(method = RequestMethod.POST, value = "/products/{product_id}/productpricelevels/savebatch")
    public ResponseEntity<Boolean> saveBatchByProduct(@PathVariable("product_id") String product_id, @RequestBody List<ProductPriceLevelDTO> productpriceleveldtos) {
        List<ProductPriceLevel> domainlist=productpricelevelMapping.toDomain(productpriceleveldtos);
        for(ProductPriceLevel domain:domainlist){
             domain.setProductid(product_id);
        }
        productpricelevelService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ProductPriceLevel-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-ProductPriceLevel-Get')")
	@ApiOperation(value = "根据产品获取DEFAULT", tags = {"价目表项" } ,notes = "根据产品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/products/{product_id}/productpricelevels/fetchdefault")
	public ResponseEntity<List<ProductPriceLevelDTO>> fetchProductPriceLevelDefaultByProduct(@PathVariable("product_id") String product_id,ProductPriceLevelSearchContext context) {
        context.setN_productid_eq(product_id);
        Page<ProductPriceLevel> domains = productpricelevelService.searchDefault(context) ;
        List<ProductPriceLevelDTO> list = productpricelevelMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ProductPriceLevel-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-ProductPriceLevel-Get')")
	@ApiOperation(value = "根据产品查询DEFAULT", tags = {"价目表项" } ,notes = "根据产品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/products/{product_id}/productpricelevels/searchdefault")
	public ResponseEntity<Page<ProductPriceLevelDTO>> searchProductPriceLevelDefaultByProduct(@PathVariable("product_id") String product_id, @RequestBody ProductPriceLevelSearchContext context) {
        context.setN_productid_eq(product_id);
        Page<ProductPriceLevel> domains = productpricelevelService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(productpricelevelMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

