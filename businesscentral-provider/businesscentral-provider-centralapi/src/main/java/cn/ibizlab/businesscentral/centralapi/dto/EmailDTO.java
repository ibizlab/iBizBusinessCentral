package cn.ibizlab.businesscentral.centralapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EmailDTO]
 */
@Data
public class EmailDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [MIMETYPE]
     *
     */
    @JSONField(name = "mimetype")
    @JsonProperty("mimetype")
    private String mimetype;

    /**
     * 属性 [TO]
     *
     */
    @JSONField(name = "to")
    @JsonProperty("to")
    private String to;

    /**
     * 属性 [LASTOPENEDTIME]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "lastopenedtime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("lastopenedtime")
    private Timestamp lastopenedtime;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;

    /**
     * 属性 [SCHEDULEDEND]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "scheduledend" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("scheduledend")
    private Timestamp scheduledend;

    /**
     * 属性 [REGARDINGOBJECTID]
     *
     */
    @JSONField(name = "regardingobjectid")
    @JsonProperty("regardingobjectid")
    private String regardingobjectid;

    /**
     * 属性 [OPENCOUNT]
     *
     */
    @JSONField(name = "opencount")
    @JsonProperty("opencount")
    private Integer opencount;

    /**
     * 属性 [OWNERTYPE]
     *
     */
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;

    /**
     * 属性 [SENDERSACCOUNTNAME]
     *
     */
    @JSONField(name = "sendersaccountname")
    @JsonProperty("sendersaccountname")
    private String sendersaccountname;

    /**
     * 属性 [EMAILREMINDERTYPE]
     *
     */
    @JSONField(name = "emailremindertype")
    @JsonProperty("emailremindertype")
    private String emailremindertype;

    /**
     * 属性 [CORRELATIONMETHOD]
     *
     */
    @JSONField(name = "correlationmethod")
    @JsonProperty("correlationmethod")
    private String correlationmethod;

    /**
     * 属性 [MESSAGEIDDUPCHECK]
     *
     */
    @JSONField(name = "messageiddupcheck")
    @JsonProperty("messageiddupcheck")
    private String messageiddupcheck;

    /**
     * 属性 [SENTON]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "senton" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("senton")
    private Timestamp senton;

    /**
     * 属性 [LINKSCLICKEDCOUNT]
     *
     */
    @JSONField(name = "linksclickedcount")
    @JsonProperty("linksclickedcount")
    private Integer linksclickedcount;

    /**
     * 属性 [COMPRESSED]
     *
     */
    @JSONField(name = "compressed")
    @JsonProperty("compressed")
    private Integer compressed;

    /**
     * 属性 [STATECODE]
     *
     */
    @JSONField(name = "statecode")
    @JsonProperty("statecode")
    private Integer statecode;

    /**
     * 属性 [INREPLYTO]
     *
     */
    @JSONField(name = "inreplyto")
    @JsonProperty("inreplyto")
    private String inreplyto;

    /**
     * 属性 [EMAILREMINDERSTATUS]
     *
     */
    @JSONField(name = "emailreminderstatus")
    @JsonProperty("emailreminderstatus")
    private String emailreminderstatus;

    /**
     * 属性 [SAFEDESCRIPTION]
     *
     */
    @JSONField(name = "safedescription")
    @JsonProperty("safedescription")
    private String safedescription;

    /**
     * 属性 [TRAVERSEDPATH]
     *
     */
    @JSONField(name = "traversedpath")
    @JsonProperty("traversedpath")
    private String traversedpath;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 属性 [REMINDERACTIONCARDID]
     *
     */
    @JSONField(name = "reminderactioncardid")
    @JsonProperty("reminderactioncardid")
    private String reminderactioncardid;

    /**
     * 属性 [VERSIONNUMBER]
     *
     */
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;

    /**
     * 属性 [ONHOLDTIME]
     *
     */
    @JSONField(name = "onholdtime")
    @JsonProperty("onholdtime")
    private Integer onholdtime;

    /**
     * 属性 [SUBCATEGORY]
     *
     */
    @JSONField(name = "subcategory")
    @JsonProperty("subcategory")
    private String subcategory;

    /**
     * 属性 [EMAILFOLLOWED]
     *
     */
    @JSONField(name = "emailfollowed")
    @JsonProperty("emailfollowed")
    private Integer emailfollowed;

    /**
     * 属性 [FOLLOWEMAILUSERPREFERENCE]
     *
     */
    @JSONField(name = "followemailuserpreference")
    @JsonProperty("followemailuserpreference")
    private Integer followemailuserpreference;

    /**
     * 属性 [OWNERNAME]
     *
     */
    @JSONField(name = "ownername")
    @JsonProperty("ownername")
    private String ownername;

    /**
     * 属性 [ACTIVITYID]
     *
     */
    @JSONField(name = "activityid")
    @JsonProperty("activityid")
    private String activityid;

    /**
     * 属性 [EMAILTRACKINGID]
     *
     */
    @JSONField(name = "emailtrackingid")
    @JsonProperty("emailtrackingid")
    private String emailtrackingid;

    /**
     * 属性 [REGARDINGOBJECTNAME]
     *
     */
    @JSONField(name = "regardingobjectname")
    @JsonProperty("regardingobjectname")
    private String regardingobjectname;

    /**
     * 属性 [SCHEDULEDSTART]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "scheduledstart" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("scheduledstart")
    private Timestamp scheduledstart;

    /**
     * 属性 [EMAILREMINDERTEXT]
     *
     */
    @JSONField(name = "emailremindertext")
    @JsonProperty("emailremindertext")
    private String emailremindertext;

    /**
     * 属性 [TRACKINGTOKEN]
     *
     */
    @JSONField(name = "trackingtoken")
    @JsonProperty("trackingtoken")
    private String trackingtoken;

    /**
     * 属性 [ACTIVITYTYPECODE]
     *
     */
    @JSONField(name = "activitytypecode")
    @JsonProperty("activitytypecode")
    private String activitytypecode;

    /**
     * 属性 [DELIVERYATTEMPTS]
     *
     */
    @JSONField(name = "deliveryattempts")
    @JsonProperty("deliveryattempts")
    private Integer deliveryattempts;

    /**
     * 属性 [SENDERSACCOUNTOBJECTTYPECODE]
     *
     */
    @JSONField(name = "sendersaccountobjecttypecode")
    @JsonProperty("sendersaccountobjecttypecode")
    private String sendersaccountobjecttypecode;

    /**
     * 属性 [DELIVERYRECEIPTREQUESTED]
     *
     */
    @JSONField(name = "deliveryreceiptrequested")
    @JsonProperty("deliveryreceiptrequested")
    private Integer deliveryreceiptrequested;

    /**
     * 属性 [EMAILREMINDEREXPIRYTIME]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "emailreminderexpirytime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("emailreminderexpirytime")
    private Timestamp emailreminderexpirytime;

    /**
     * 属性 [EXCHANGERATE]
     *
     */
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;

    /**
     * 属性 [OVERRIDDENCREATEDON]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;

    /**
     * 属性 [OWNERID]
     *
     */
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;

    /**
     * 属性 [BILLED]
     *
     */
    @JSONField(name = "billed")
    @JsonProperty("billed")
    private Integer billed;

    /**
     * 属性 [CATEGORY]
     *
     */
    @JSONField(name = "category")
    @JsonProperty("category")
    private String category;

    /**
     * 属性 [BASECONVERSATIONINDEXHASH]
     *
     */
    @JSONField(name = "baseconversationindexhash")
    @JsonProperty("baseconversationindexhash")
    private Integer baseconversationindexhash;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [PROCESSID]
     *
     */
    @JSONField(name = "processid")
    @JsonProperty("processid")
    private String processid;

    /**
     * 属性 [CONVERSATIONINDEX]
     *
     */
    @JSONField(name = "conversationindex")
    @JsonProperty("conversationindex")
    private String conversationindex;

    /**
     * 属性 [STAGEID]
     *
     */
    @JSONField(name = "stageid")
    @JsonProperty("stageid")
    private String stageid;

    /**
     * 属性 [POSTPONEEMAILPROCESSINGUNTIL]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "postponeemailprocessinguntil" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("postponeemailprocessinguntil")
    private Timestamp postponeemailprocessinguntil;

    /**
     * 属性 [DIRECTIONCODE]
     *
     */
    @JSONField(name = "directioncode")
    @JsonProperty("directioncode")
    private Integer directioncode;

    /**
     * 属性 [NOTIFICATIONS]
     *
     */
    @JSONField(name = "notifications")
    @JsonProperty("notifications")
    private String notifications;

    /**
     * 属性 [EMAILSENDEROBJECTTYPECODE]
     *
     */
    @JSONField(name = "emailsenderobjecttypecode")
    @JsonProperty("emailsenderobjecttypecode")
    private String emailsenderobjecttypecode;

    /**
     * 属性 [SORTDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "sortdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("sortdate")
    private Timestamp sortdate;

    /**
     * 属性 [MESSAGEID]
     *
     */
    @JSONField(name = "messageid")
    @JsonProperty("messageid")
    private String messageid;

    /**
     * 属性 [ACTIVITYADDITIONALPARAMS]
     *
     */
    @JSONField(name = "activityadditionalparams")
    @JsonProperty("activityadditionalparams")
    private String activityadditionalparams;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [READRECEIPTREQUESTED]
     *
     */
    @JSONField(name = "readreceiptrequested")
    @JsonProperty("readreceiptrequested")
    private Integer readreceiptrequested;

    /**
     * 属性 [WORKFLOWCREATED]
     *
     */
    @JSONField(name = "workflowcreated")
    @JsonProperty("workflowcreated")
    private Integer workflowcreated;

    /**
     * 属性 [UNSAFE]
     *
     */
    @JSONField(name = "unsafe")
    @JsonProperty("unsafe")
    private Integer unsafe;

    /**
     * 属性 [REGARDINGOBJECTTYPECODE]
     *
     */
    @JSONField(name = "regardingobjecttypecode")
    @JsonProperty("regardingobjecttypecode")
    private String regardingobjecttypecode;

    /**
     * 属性 [PRIORITYCODE]
     *
     */
    @JSONField(name = "prioritycode")
    @JsonProperty("prioritycode")
    private String prioritycode;

    /**
     * 属性 [DELAYEDEMAILSENDTIME]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "delayedemailsendtime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("delayedemailsendtime")
    private Timestamp delayedemailsendtime;

    /**
     * 属性 [FROM]
     *
     */
    @JSONField(name = "from")
    @JsonProperty("from")
    private String from;

    /**
     * 属性 [EMAILSENDERNAME]
     *
     */
    @JSONField(name = "emailsendername")
    @JsonProperty("emailsendername")
    private String emailsendername;

    /**
     * 属性 [BCC]
     *
     */
    @JSONField(name = "bcc")
    @JsonProperty("bcc")
    private String bcc;

    /**
     * 属性 [ACTUALSTART]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "actualstart" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("actualstart")
    private Timestamp actualstart;

    /**
     * 属性 [ACTUALEND]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "actualend" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("actualend")
    private Timestamp actualend;

    /**
     * 属性 [SLANAME]
     *
     */
    @JSONField(name = "slaname")
    @JsonProperty("slaname")
    private String slaname;

    /**
     * 属性 [CONVERSATIONTRACKINGID]
     *
     */
    @JSONField(name = "conversationtrackingid")
    @JsonProperty("conversationtrackingid")
    private String conversationtrackingid;

    /**
     * 属性 [SENDER]
     *
     */
    @JSONField(name = "sender")
    @JsonProperty("sender")
    private String sender;

    /**
     * 属性 [LASTONHOLDTIME]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "lastonholdtime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("lastonholdtime")
    private Timestamp lastonholdtime;

    /**
     * 属性 [SUBMITTEDBY]
     *
     */
    @JSONField(name = "submittedby")
    @JsonProperty("submittedby")
    private String submittedby;

    /**
     * 属性 [TORECIPIENTS]
     *
     */
    @JSONField(name = "torecipients")
    @JsonProperty("torecipients")
    private String torecipients;

    /**
     * 属性 [DELIVERYPRIORITYCODE]
     *
     */
    @JSONField(name = "deliveryprioritycode")
    @JsonProperty("deliveryprioritycode")
    private String deliveryprioritycode;

    /**
     * 属性 [ATTACHMENTCOUNT]
     *
     */
    @JSONField(name = "attachmentcount")
    @JsonProperty("attachmentcount")
    private Integer attachmentcount;

    /**
     * 属性 [ATTACHMENTOPENCOUNT]
     *
     */
    @JSONField(name = "attachmentopencount")
    @JsonProperty("attachmentopencount")
    private Integer attachmentopencount;

    /**
     * 属性 [SCHEDULEDDURATIONMINUTES]
     *
     */
    @JSONField(name = "scheduleddurationminutes")
    @JsonProperty("scheduleddurationminutes")
    private Integer scheduleddurationminutes;

    /**
     * 属性 [IMPORTSEQUENCENUMBER]
     *
     */
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;

    /**
     * 属性 [TIMEZONERULEVERSIONNUMBER]
     *
     */
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;

    /**
     * 属性 [REPLYCOUNT]
     *
     */
    @JSONField(name = "replycount")
    @JsonProperty("replycount")
    private Integer replycount;

    /**
     * 属性 [REGULARACTIVITY]
     *
     */
    @JSONField(name = "regularactivity")
    @JsonProperty("regularactivity")
    private Integer regularactivity;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;

    /**
     * 属性 [CC]
     *
     */
    @JSONField(name = "cc")
    @JsonProperty("cc")
    private String cc;

    /**
     * 属性 [STATUSCODE]
     *
     */
    @JSONField(name = "statuscode")
    @JsonProperty("statuscode")
    private Integer statuscode;

    /**
     * 属性 [SUBJECT]
     *
     */
    @JSONField(name = "subject")
    @JsonProperty("subject")
    private String subject;

    /**
     * 属性 [UTCCONVERSIONTIMEZONECODE]
     *
     */
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;

    /**
     * 属性 [EMAILREMINDERSET]
     *
     */
    @JSONField(name = "emailreminderset")
    @JsonProperty("emailreminderset")
    private Integer emailreminderset;

    /**
     * 属性 [ACTUALDURATIONMINUTES]
     *
     */
    @JSONField(name = "actualdurationminutes")
    @JsonProperty("actualdurationminutes")
    private Integer actualdurationminutes;

    /**
     * 属性 [PARENTACTIVITYID]
     *
     */
    @JSONField(name = "parentactivityid")
    @JsonProperty("parentactivityid")
    private String parentactivityid;

    /**
     * 属性 [SERVICEID]
     *
     */
    @JSONField(name = "serviceid")
    @JsonProperty("serviceid")
    private String serviceid;

    /**
     * 属性 [TEMPLATEID]
     *
     */
    @JSONField(name = "templateid")
    @JsonProperty("templateid")
    private String templateid;

    /**
     * 属性 [SLAID]
     *
     */
    @JSONField(name = "slaid")
    @JsonProperty("slaid")
    private String slaid;

    /**
     * 属性 [TRANSACTIONCURRENCYID]
     *
     */
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;


    /**
     * 设置 [MIMETYPE]
     */
    public void setMimetype(String  mimetype){
        this.mimetype = mimetype ;
        this.modify("mimetype",mimetype);
    }

    /**
     * 设置 [TO]
     */
    public void setTo(String  to){
        this.to = to ;
        this.modify("to",to);
    }

    /**
     * 设置 [LASTOPENEDTIME]
     */
    public void setLastopenedtime(Timestamp  lastopenedtime){
        this.lastopenedtime = lastopenedtime ;
        this.modify("lastopenedtime",lastopenedtime);
    }

    /**
     * 设置 [SCHEDULEDEND]
     */
    public void setScheduledend(Timestamp  scheduledend){
        this.scheduledend = scheduledend ;
        this.modify("scheduledend",scheduledend);
    }

    /**
     * 设置 [REGARDINGOBJECTID]
     */
    public void setRegardingobjectid(String  regardingobjectid){
        this.regardingobjectid = regardingobjectid ;
        this.modify("regardingobjectid",regardingobjectid);
    }

    /**
     * 设置 [OPENCOUNT]
     */
    public void setOpencount(Integer  opencount){
        this.opencount = opencount ;
        this.modify("opencount",opencount);
    }

    /**
     * 设置 [OWNERTYPE]
     */
    public void setOwnertype(String  ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [SENDERSACCOUNTNAME]
     */
    public void setSendersaccountname(String  sendersaccountname){
        this.sendersaccountname = sendersaccountname ;
        this.modify("sendersaccountname",sendersaccountname);
    }

    /**
     * 设置 [EMAILREMINDERTYPE]
     */
    public void setEmailremindertype(String  emailremindertype){
        this.emailremindertype = emailremindertype ;
        this.modify("emailremindertype",emailremindertype);
    }

    /**
     * 设置 [CORRELATIONMETHOD]
     */
    public void setCorrelationmethod(String  correlationmethod){
        this.correlationmethod = correlationmethod ;
        this.modify("correlationmethod",correlationmethod);
    }

    /**
     * 设置 [MESSAGEIDDUPCHECK]
     */
    public void setMessageiddupcheck(String  messageiddupcheck){
        this.messageiddupcheck = messageiddupcheck ;
        this.modify("messageiddupcheck",messageiddupcheck);
    }

    /**
     * 设置 [SENTON]
     */
    public void setSenton(Timestamp  senton){
        this.senton = senton ;
        this.modify("senton",senton);
    }

    /**
     * 设置 [LINKSCLICKEDCOUNT]
     */
    public void setLinksclickedcount(Integer  linksclickedcount){
        this.linksclickedcount = linksclickedcount ;
        this.modify("linksclickedcount",linksclickedcount);
    }

    /**
     * 设置 [COMPRESSED]
     */
    public void setCompressed(Integer  compressed){
        this.compressed = compressed ;
        this.modify("compressed",compressed);
    }

    /**
     * 设置 [STATECODE]
     */
    public void setStatecode(Integer  statecode){
        this.statecode = statecode ;
        this.modify("statecode",statecode);
    }

    /**
     * 设置 [INREPLYTO]
     */
    public void setInreplyto(String  inreplyto){
        this.inreplyto = inreplyto ;
        this.modify("inreplyto",inreplyto);
    }

    /**
     * 设置 [EMAILREMINDERSTATUS]
     */
    public void setEmailreminderstatus(String  emailreminderstatus){
        this.emailreminderstatus = emailreminderstatus ;
        this.modify("emailreminderstatus",emailreminderstatus);
    }

    /**
     * 设置 [SAFEDESCRIPTION]
     */
    public void setSafedescription(String  safedescription){
        this.safedescription = safedescription ;
        this.modify("safedescription",safedescription);
    }

    /**
     * 设置 [TRAVERSEDPATH]
     */
    public void setTraversedpath(String  traversedpath){
        this.traversedpath = traversedpath ;
        this.modify("traversedpath",traversedpath);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [REMINDERACTIONCARDID]
     */
    public void setReminderactioncardid(String  reminderactioncardid){
        this.reminderactioncardid = reminderactioncardid ;
        this.modify("reminderactioncardid",reminderactioncardid);
    }

    /**
     * 设置 [VERSIONNUMBER]
     */
    public void setVersionnumber(BigInteger  versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [ONHOLDTIME]
     */
    public void setOnholdtime(Integer  onholdtime){
        this.onholdtime = onholdtime ;
        this.modify("onholdtime",onholdtime);
    }

    /**
     * 设置 [SUBCATEGORY]
     */
    public void setSubcategory(String  subcategory){
        this.subcategory = subcategory ;
        this.modify("subcategory",subcategory);
    }

    /**
     * 设置 [EMAILFOLLOWED]
     */
    public void setEmailfollowed(Integer  emailfollowed){
        this.emailfollowed = emailfollowed ;
        this.modify("emailfollowed",emailfollowed);
    }

    /**
     * 设置 [FOLLOWEMAILUSERPREFERENCE]
     */
    public void setFollowemailuserpreference(Integer  followemailuserpreference){
        this.followemailuserpreference = followemailuserpreference ;
        this.modify("followemailuserpreference",followemailuserpreference);
    }

    /**
     * 设置 [OWNERNAME]
     */
    public void setOwnername(String  ownername){
        this.ownername = ownername ;
        this.modify("ownername",ownername);
    }

    /**
     * 设置 [EMAILTRACKINGID]
     */
    public void setEmailtrackingid(String  emailtrackingid){
        this.emailtrackingid = emailtrackingid ;
        this.modify("emailtrackingid",emailtrackingid);
    }

    /**
     * 设置 [REGARDINGOBJECTNAME]
     */
    public void setRegardingobjectname(String  regardingobjectname){
        this.regardingobjectname = regardingobjectname ;
        this.modify("regardingobjectname",regardingobjectname);
    }

    /**
     * 设置 [SCHEDULEDSTART]
     */
    public void setScheduledstart(Timestamp  scheduledstart){
        this.scheduledstart = scheduledstart ;
        this.modify("scheduledstart",scheduledstart);
    }

    /**
     * 设置 [EMAILREMINDERTEXT]
     */
    public void setEmailremindertext(String  emailremindertext){
        this.emailremindertext = emailremindertext ;
        this.modify("emailremindertext",emailremindertext);
    }

    /**
     * 设置 [TRACKINGTOKEN]
     */
    public void setTrackingtoken(String  trackingtoken){
        this.trackingtoken = trackingtoken ;
        this.modify("trackingtoken",trackingtoken);
    }

    /**
     * 设置 [ACTIVITYTYPECODE]
     */
    public void setActivitytypecode(String  activitytypecode){
        this.activitytypecode = activitytypecode ;
        this.modify("activitytypecode",activitytypecode);
    }

    /**
     * 设置 [DELIVERYATTEMPTS]
     */
    public void setDeliveryattempts(Integer  deliveryattempts){
        this.deliveryattempts = deliveryattempts ;
        this.modify("deliveryattempts",deliveryattempts);
    }

    /**
     * 设置 [SENDERSACCOUNTOBJECTTYPECODE]
     */
    public void setSendersaccountobjecttypecode(String  sendersaccountobjecttypecode){
        this.sendersaccountobjecttypecode = sendersaccountobjecttypecode ;
        this.modify("sendersaccountobjecttypecode",sendersaccountobjecttypecode);
    }

    /**
     * 设置 [DELIVERYRECEIPTREQUESTED]
     */
    public void setDeliveryreceiptrequested(Integer  deliveryreceiptrequested){
        this.deliveryreceiptrequested = deliveryreceiptrequested ;
        this.modify("deliveryreceiptrequested",deliveryreceiptrequested);
    }

    /**
     * 设置 [EMAILREMINDEREXPIRYTIME]
     */
    public void setEmailreminderexpirytime(Timestamp  emailreminderexpirytime){
        this.emailreminderexpirytime = emailreminderexpirytime ;
        this.modify("emailreminderexpirytime",emailreminderexpirytime);
    }

    /**
     * 设置 [EXCHANGERATE]
     */
    public void setExchangerate(BigDecimal  exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [OVERRIDDENCREATEDON]
     */
    public void setOverriddencreatedon(Timestamp  overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 设置 [OWNERID]
     */
    public void setOwnerid(String  ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [BILLED]
     */
    public void setBilled(Integer  billed){
        this.billed = billed ;
        this.modify("billed",billed);
    }

    /**
     * 设置 [CATEGORY]
     */
    public void setCategory(String  category){
        this.category = category ;
        this.modify("category",category);
    }

    /**
     * 设置 [BASECONVERSATIONINDEXHASH]
     */
    public void setBaseconversationindexhash(Integer  baseconversationindexhash){
        this.baseconversationindexhash = baseconversationindexhash ;
        this.modify("baseconversationindexhash",baseconversationindexhash);
    }

    /**
     * 设置 [PROCESSID]
     */
    public void setProcessid(String  processid){
        this.processid = processid ;
        this.modify("processid",processid);
    }

    /**
     * 设置 [CONVERSATIONINDEX]
     */
    public void setConversationindex(String  conversationindex){
        this.conversationindex = conversationindex ;
        this.modify("conversationindex",conversationindex);
    }

    /**
     * 设置 [STAGEID]
     */
    public void setStageid(String  stageid){
        this.stageid = stageid ;
        this.modify("stageid",stageid);
    }

    /**
     * 设置 [POSTPONEEMAILPROCESSINGUNTIL]
     */
    public void setPostponeemailprocessinguntil(Timestamp  postponeemailprocessinguntil){
        this.postponeemailprocessinguntil = postponeemailprocessinguntil ;
        this.modify("postponeemailprocessinguntil",postponeemailprocessinguntil);
    }

    /**
     * 设置 [DIRECTIONCODE]
     */
    public void setDirectioncode(Integer  directioncode){
        this.directioncode = directioncode ;
        this.modify("directioncode",directioncode);
    }

    /**
     * 设置 [NOTIFICATIONS]
     */
    public void setNotifications(String  notifications){
        this.notifications = notifications ;
        this.modify("notifications",notifications);
    }

    /**
     * 设置 [EMAILSENDEROBJECTTYPECODE]
     */
    public void setEmailsenderobjecttypecode(String  emailsenderobjecttypecode){
        this.emailsenderobjecttypecode = emailsenderobjecttypecode ;
        this.modify("emailsenderobjecttypecode",emailsenderobjecttypecode);
    }

    /**
     * 设置 [SORTDATE]
     */
    public void setSortdate(Timestamp  sortdate){
        this.sortdate = sortdate ;
        this.modify("sortdate",sortdate);
    }

    /**
     * 设置 [MESSAGEID]
     */
    public void setMessageid(String  messageid){
        this.messageid = messageid ;
        this.modify("messageid",messageid);
    }

    /**
     * 设置 [ACTIVITYADDITIONALPARAMS]
     */
    public void setActivityadditionalparams(String  activityadditionalparams){
        this.activityadditionalparams = activityadditionalparams ;
        this.modify("activityadditionalparams",activityadditionalparams);
    }

    /**
     * 设置 [READRECEIPTREQUESTED]
     */
    public void setReadreceiptrequested(Integer  readreceiptrequested){
        this.readreceiptrequested = readreceiptrequested ;
        this.modify("readreceiptrequested",readreceiptrequested);
    }

    /**
     * 设置 [WORKFLOWCREATED]
     */
    public void setWorkflowcreated(Integer  workflowcreated){
        this.workflowcreated = workflowcreated ;
        this.modify("workflowcreated",workflowcreated);
    }

    /**
     * 设置 [UNSAFE]
     */
    public void setUnsafe(Integer  unsafe){
        this.unsafe = unsafe ;
        this.modify("unsafe",unsafe);
    }

    /**
     * 设置 [REGARDINGOBJECTTYPECODE]
     */
    public void setRegardingobjecttypecode(String  regardingobjecttypecode){
        this.regardingobjecttypecode = regardingobjecttypecode ;
        this.modify("regardingobjecttypecode",regardingobjecttypecode);
    }

    /**
     * 设置 [PRIORITYCODE]
     */
    public void setPrioritycode(String  prioritycode){
        this.prioritycode = prioritycode ;
        this.modify("prioritycode",prioritycode);
    }

    /**
     * 设置 [DELAYEDEMAILSENDTIME]
     */
    public void setDelayedemailsendtime(Timestamp  delayedemailsendtime){
        this.delayedemailsendtime = delayedemailsendtime ;
        this.modify("delayedemailsendtime",delayedemailsendtime);
    }

    /**
     * 设置 [FROM]
     */
    public void setFrom(String  from){
        this.from = from ;
        this.modify("from",from);
    }

    /**
     * 设置 [EMAILSENDERNAME]
     */
    public void setEmailsendername(String  emailsendername){
        this.emailsendername = emailsendername ;
        this.modify("emailsendername",emailsendername);
    }

    /**
     * 设置 [BCC]
     */
    public void setBcc(String  bcc){
        this.bcc = bcc ;
        this.modify("bcc",bcc);
    }

    /**
     * 设置 [ACTUALSTART]
     */
    public void setActualstart(Timestamp  actualstart){
        this.actualstart = actualstart ;
        this.modify("actualstart",actualstart);
    }

    /**
     * 设置 [ACTUALEND]
     */
    public void setActualend(Timestamp  actualend){
        this.actualend = actualend ;
        this.modify("actualend",actualend);
    }

    /**
     * 设置 [SLANAME]
     */
    public void setSlaname(String  slaname){
        this.slaname = slaname ;
        this.modify("slaname",slaname);
    }

    /**
     * 设置 [CONVERSATIONTRACKINGID]
     */
    public void setConversationtrackingid(String  conversationtrackingid){
        this.conversationtrackingid = conversationtrackingid ;
        this.modify("conversationtrackingid",conversationtrackingid);
    }

    /**
     * 设置 [SENDER]
     */
    public void setSender(String  sender){
        this.sender = sender ;
        this.modify("sender",sender);
    }

    /**
     * 设置 [LASTONHOLDTIME]
     */
    public void setLastonholdtime(Timestamp  lastonholdtime){
        this.lastonholdtime = lastonholdtime ;
        this.modify("lastonholdtime",lastonholdtime);
    }

    /**
     * 设置 [SUBMITTEDBY]
     */
    public void setSubmittedby(String  submittedby){
        this.submittedby = submittedby ;
        this.modify("submittedby",submittedby);
    }

    /**
     * 设置 [TORECIPIENTS]
     */
    public void setTorecipients(String  torecipients){
        this.torecipients = torecipients ;
        this.modify("torecipients",torecipients);
    }

    /**
     * 设置 [DELIVERYPRIORITYCODE]
     */
    public void setDeliveryprioritycode(String  deliveryprioritycode){
        this.deliveryprioritycode = deliveryprioritycode ;
        this.modify("deliveryprioritycode",deliveryprioritycode);
    }

    /**
     * 设置 [ATTACHMENTCOUNT]
     */
    public void setAttachmentcount(Integer  attachmentcount){
        this.attachmentcount = attachmentcount ;
        this.modify("attachmentcount",attachmentcount);
    }

    /**
     * 设置 [ATTACHMENTOPENCOUNT]
     */
    public void setAttachmentopencount(Integer  attachmentopencount){
        this.attachmentopencount = attachmentopencount ;
        this.modify("attachmentopencount",attachmentopencount);
    }

    /**
     * 设置 [SCHEDULEDDURATIONMINUTES]
     */
    public void setScheduleddurationminutes(Integer  scheduleddurationminutes){
        this.scheduleddurationminutes = scheduleddurationminutes ;
        this.modify("scheduleddurationminutes",scheduleddurationminutes);
    }

    /**
     * 设置 [IMPORTSEQUENCENUMBER]
     */
    public void setImportsequencenumber(Integer  importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [TIMEZONERULEVERSIONNUMBER]
     */
    public void setTimezoneruleversionnumber(Integer  timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [REPLYCOUNT]
     */
    public void setReplycount(Integer  replycount){
        this.replycount = replycount ;
        this.modify("replycount",replycount);
    }

    /**
     * 设置 [REGULARACTIVITY]
     */
    public void setRegularactivity(Integer  regularactivity){
        this.regularactivity = regularactivity ;
        this.modify("regularactivity",regularactivity);
    }

    /**
     * 设置 [CC]
     */
    public void setCc(String  cc){
        this.cc = cc ;
        this.modify("cc",cc);
    }

    /**
     * 设置 [STATUSCODE]
     */
    public void setStatuscode(Integer  statuscode){
        this.statuscode = statuscode ;
        this.modify("statuscode",statuscode);
    }

    /**
     * 设置 [SUBJECT]
     */
    public void setSubject(String  subject){
        this.subject = subject ;
        this.modify("subject",subject);
    }

    /**
     * 设置 [UTCCONVERSIONTIMEZONECODE]
     */
    public void setUtcconversiontimezonecode(Integer  utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [EMAILREMINDERSET]
     */
    public void setEmailreminderset(Integer  emailreminderset){
        this.emailreminderset = emailreminderset ;
        this.modify("emailreminderset",emailreminderset);
    }

    /**
     * 设置 [ACTUALDURATIONMINUTES]
     */
    public void setActualdurationminutes(Integer  actualdurationminutes){
        this.actualdurationminutes = actualdurationminutes ;
        this.modify("actualdurationminutes",actualdurationminutes);
    }

    /**
     * 设置 [PARENTACTIVITYID]
     */
    public void setParentactivityid(String  parentactivityid){
        this.parentactivityid = parentactivityid ;
        this.modify("parentactivityid",parentactivityid);
    }

    /**
     * 设置 [SERVICEID]
     */
    public void setServiceid(String  serviceid){
        this.serviceid = serviceid ;
        this.modify("serviceid",serviceid);
    }

    /**
     * 设置 [TEMPLATEID]
     */
    public void setTemplateid(String  templateid){
        this.templateid = templateid ;
        this.modify("templateid",templateid);
    }

    /**
     * 设置 [SLAID]
     */
    public void setSlaid(String  slaid){
        this.slaid = slaid ;
        this.modify("slaid",slaid);
    }

    /**
     * 设置 [TRANSACTIONCURRENCYID]
     */
    public void setTransactioncurrencyid(String  transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }


}

