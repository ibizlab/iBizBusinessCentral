package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.sales.domain.Quote;
import cn.ibizlab.businesscentral.core.sales.service.IQuoteService;
import cn.ibizlab.businesscentral.core.sales.filter.QuoteSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"报价单" })
@RestController("CentralApi-quote")
@RequestMapping("")
public class QuoteResource {

    @Autowired
    public IQuoteService quoteService;

    @Autowired
    @Lazy
    public QuoteMapping quoteMapping;

    @PreAuthorize("hasPermission(this.quoteMapping.toDomain(#quotedto),'iBizBusinessCentral-Quote-Create')")
    @ApiOperation(value = "新建报价单", tags = {"报价单" },  notes = "新建报价单")
	@RequestMapping(method = RequestMethod.POST, value = "/quotes")
    public ResponseEntity<QuoteDTO> create(@RequestBody QuoteDTO quotedto) {
        Quote domain = quoteMapping.toDomain(quotedto);
		quoteService.create(domain);
        QuoteDTO dto = quoteMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.quoteMapping.toDomain(#quotedtos),'iBizBusinessCentral-Quote-Create')")
    @ApiOperation(value = "批量新建报价单", tags = {"报价单" },  notes = "批量新建报价单")
	@RequestMapping(method = RequestMethod.POST, value = "/quotes/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<QuoteDTO> quotedtos) {
        quoteService.createBatch(quoteMapping.toDomain(quotedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "quote" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.quoteService.get(#quote_id),'iBizBusinessCentral-Quote-Update')")
    @ApiOperation(value = "更新报价单", tags = {"报价单" },  notes = "更新报价单")
	@RequestMapping(method = RequestMethod.PUT, value = "/quotes/{quote_id}")
    public ResponseEntity<QuoteDTO> update(@PathVariable("quote_id") String quote_id, @RequestBody QuoteDTO quotedto) {
		Quote domain  = quoteMapping.toDomain(quotedto);
        domain .setQuoteid(quote_id);
		quoteService.update(domain );
		QuoteDTO dto = quoteMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.quoteService.getQuoteByEntities(this.quoteMapping.toDomain(#quotedtos)),'iBizBusinessCentral-Quote-Update')")
    @ApiOperation(value = "批量更新报价单", tags = {"报价单" },  notes = "批量更新报价单")
	@RequestMapping(method = RequestMethod.PUT, value = "/quotes/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<QuoteDTO> quotedtos) {
        quoteService.updateBatch(quoteMapping.toDomain(quotedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.quoteService.get(#quote_id),'iBizBusinessCentral-Quote-Remove')")
    @ApiOperation(value = "删除报价单", tags = {"报价单" },  notes = "删除报价单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/quotes/{quote_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("quote_id") String quote_id) {
         return ResponseEntity.status(HttpStatus.OK).body(quoteService.remove(quote_id));
    }

    @PreAuthorize("hasPermission(this.quoteService.getQuoteByIds(#ids),'iBizBusinessCentral-Quote-Remove')")
    @ApiOperation(value = "批量删除报价单", tags = {"报价单" },  notes = "批量删除报价单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/quotes/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        quoteService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.quoteMapping.toDomain(returnObject.body),'iBizBusinessCentral-Quote-Get')")
    @ApiOperation(value = "获取报价单", tags = {"报价单" },  notes = "获取报价单")
	@RequestMapping(method = RequestMethod.GET, value = "/quotes/{quote_id}")
    public ResponseEntity<QuoteDTO> get(@PathVariable("quote_id") String quote_id) {
        Quote domain = quoteService.get(quote_id);
        QuoteDTO dto = quoteMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取报价单草稿", tags = {"报价单" },  notes = "获取报价单草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/quotes/getdraft")
    public ResponseEntity<QuoteDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(quoteMapping.toDto(quoteService.getDraft(new Quote())));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-Active-all')")
    @ApiOperation(value = "激活报价单", tags = {"报价单" },  notes = "激活报价单")
	@RequestMapping(method = RequestMethod.POST, value = "/quotes/{quote_id}/active")
    public ResponseEntity<QuoteDTO> active(@PathVariable("quote_id") String quote_id, @RequestBody QuoteDTO quotedto) {
        Quote domain = quoteMapping.toDomain(quotedto);
domain.setQuoteid(quote_id);
        domain = quoteService.active(domain);
        quotedto = quoteMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(quotedto);
    }

    @ApiOperation(value = "检查报价单", tags = {"报价单" },  notes = "检查报价单")
	@RequestMapping(method = RequestMethod.POST, value = "/quotes/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody QuoteDTO quotedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(quoteService.checkKey(quoteMapping.toDomain(quotedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-Close-all')")
    @ApiOperation(value = "结束报价单", tags = {"报价单" },  notes = "结束报价单")
	@RequestMapping(method = RequestMethod.POST, value = "/quotes/{quote_id}/close")
    public ResponseEntity<QuoteDTO> close(@PathVariable("quote_id") String quote_id, @RequestBody QuoteDTO quotedto) {
        Quote domain = quoteMapping.toDomain(quotedto);
domain.setQuoteid(quote_id);
        domain = quoteService.close(domain);
        quotedto = quoteMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(quotedto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-GenSalesOrder-all')")
    @ApiOperation(value = "创建订单", tags = {"报价单" },  notes = "创建订单")
	@RequestMapping(method = RequestMethod.POST, value = "/quotes/{quote_id}/gensalesorder")
    public ResponseEntity<QuoteDTO> genSalesOrder(@PathVariable("quote_id") String quote_id, @RequestBody QuoteDTO quotedto) {
        Quote domain = quoteMapping.toDomain(quotedto);
domain.setQuoteid(quote_id);
        domain = quoteService.genSalesOrder(domain);
        quotedto = quoteMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(quotedto);
    }

    @PreAuthorize("hasPermission(this.quoteMapping.toDomain(#quotedto),'iBizBusinessCentral-Quote-Save')")
    @ApiOperation(value = "保存报价单", tags = {"报价单" },  notes = "保存报价单")
	@RequestMapping(method = RequestMethod.POST, value = "/quotes/save")
    public ResponseEntity<Boolean> save(@RequestBody QuoteDTO quotedto) {
        return ResponseEntity.status(HttpStatus.OK).body(quoteService.save(quoteMapping.toDomain(quotedto)));
    }

    @PreAuthorize("hasPermission(this.quoteMapping.toDomain(#quotedtos),'iBizBusinessCentral-Quote-Save')")
    @ApiOperation(value = "批量保存报价单", tags = {"报价单" },  notes = "批量保存报价单")
	@RequestMapping(method = RequestMethod.POST, value = "/quotes/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<QuoteDTO> quotedtos) {
        quoteService.saveBatch(quoteMapping.toDomain(quotedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-Win-all')")
    @ApiOperation(value = "赢单", tags = {"报价单" },  notes = "赢单")
	@RequestMapping(method = RequestMethod.POST, value = "/quotes/{quote_id}/win")
    public ResponseEntity<QuoteDTO> win(@PathVariable("quote_id") String quote_id, @RequestBody QuoteDTO quotedto) {
        Quote domain = quoteMapping.toDomain(quotedto);
domain.setQuoteid(quote_id);
        domain = quoteService.win(domain);
        quotedto = quoteMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(quotedto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchByParentKey-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "获取ByParentKey", tags = {"报价单" } ,notes = "获取ByParentKey")
    @RequestMapping(method= RequestMethod.GET , value="/quotes/fetchbyparentkey")
	public ResponseEntity<List<QuoteDTO>> fetchByParentKey(QuoteSearchContext context) {
        Page<Quote> domains = quoteService.searchByParentKey(context) ;
        List<QuoteDTO> list = quoteMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchByParentKey-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "查询ByParentKey", tags = {"报价单" } ,notes = "查询ByParentKey")
    @RequestMapping(method= RequestMethod.POST , value="/quotes/searchbyparentkey")
	public ResponseEntity<Page<QuoteDTO>> searchByParentKey(@RequestBody QuoteSearchContext context) {
        Page<Quote> domains = quoteService.searchByParentKey(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(quoteMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchClosed-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "获取已关闭", tags = {"报价单" } ,notes = "获取已关闭")
    @RequestMapping(method= RequestMethod.GET , value="/quotes/fetchclosed")
	public ResponseEntity<List<QuoteDTO>> fetchClosed(QuoteSearchContext context) {
        Page<Quote> domains = quoteService.searchClosed(context) ;
        List<QuoteDTO> list = quoteMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchClosed-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "查询已关闭", tags = {"报价单" } ,notes = "查询已关闭")
    @RequestMapping(method= RequestMethod.POST , value="/quotes/searchclosed")
	public ResponseEntity<Page<QuoteDTO>> searchClosed(@RequestBody QuoteSearchContext context) {
        Page<Quote> domains = quoteService.searchClosed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(quoteMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"报价单" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/quotes/fetchdefault")
	public ResponseEntity<List<QuoteDTO>> fetchDefault(QuoteSearchContext context) {
        Page<Quote> domains = quoteService.searchDefault(context) ;
        List<QuoteDTO> list = quoteMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"报价单" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/quotes/searchdefault")
	public ResponseEntity<Page<QuoteDTO>> searchDefault(@RequestBody QuoteSearchContext context) {
        Page<Quote> domains = quoteService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(quoteMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchDraft-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "获取草稿", tags = {"报价单" } ,notes = "获取草稿")
    @RequestMapping(method= RequestMethod.GET , value="/quotes/fetchdraft")
	public ResponseEntity<List<QuoteDTO>> fetchDraft(QuoteSearchContext context) {
        Page<Quote> domains = quoteService.searchDraft(context) ;
        List<QuoteDTO> list = quoteMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchDraft-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "查询草稿", tags = {"报价单" } ,notes = "查询草稿")
    @RequestMapping(method= RequestMethod.POST , value="/quotes/searchdraft")
	public ResponseEntity<Page<QuoteDTO>> searchDraft(@RequestBody QuoteSearchContext context) {
        Page<Quote> domains = quoteService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(quoteMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchEffective-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "获取有效报价单", tags = {"报价单" } ,notes = "获取有效报价单")
    @RequestMapping(method= RequestMethod.GET , value="/quotes/fetcheffective")
	public ResponseEntity<List<QuoteDTO>> fetchEffective(QuoteSearchContext context) {
        Page<Quote> domains = quoteService.searchEffective(context) ;
        List<QuoteDTO> list = quoteMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchEffective-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "查询有效报价单", tags = {"报价单" } ,notes = "查询有效报价单")
    @RequestMapping(method= RequestMethod.POST , value="/quotes/searcheffective")
	public ResponseEntity<Page<QuoteDTO>> searchEffective(@RequestBody QuoteSearchContext context) {
        Page<Quote> domains = quoteService.searchEffective(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(quoteMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchWin-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "获取赢单", tags = {"报价单" } ,notes = "获取赢单")
    @RequestMapping(method= RequestMethod.GET , value="/quotes/fetchwin")
	public ResponseEntity<List<QuoteDTO>> fetchWin(QuoteSearchContext context) {
        Page<Quote> domains = quoteService.searchWin(context) ;
        List<QuoteDTO> list = quoteMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchWin-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "查询赢单", tags = {"报价单" } ,notes = "查询赢单")
    @RequestMapping(method= RequestMethod.POST , value="/quotes/searchwin")
	public ResponseEntity<Page<QuoteDTO>> searchWin(@RequestBody QuoteSearchContext context) {
        Page<Quote> domains = quoteService.searchWin(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(quoteMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.quoteMapping.toDomain(#quotedto),'iBizBusinessCentral-Quote-Create')")
    @ApiOperation(value = "根据商机建立报价单", tags = {"报价单" },  notes = "根据商机建立报价单")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/quotes")
    public ResponseEntity<QuoteDTO> createByOpportunity(@PathVariable("opportunity_id") String opportunity_id, @RequestBody QuoteDTO quotedto) {
        Quote domain = quoteMapping.toDomain(quotedto);
        domain.setOpportunityid(opportunity_id);
		quoteService.create(domain);
        QuoteDTO dto = quoteMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.quoteMapping.toDomain(#quotedtos),'iBizBusinessCentral-Quote-Create')")
    @ApiOperation(value = "根据商机批量建立报价单", tags = {"报价单" },  notes = "根据商机批量建立报价单")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/quotes/batch")
    public ResponseEntity<Boolean> createBatchByOpportunity(@PathVariable("opportunity_id") String opportunity_id, @RequestBody List<QuoteDTO> quotedtos) {
        List<Quote> domainlist=quoteMapping.toDomain(quotedtos);
        for(Quote domain:domainlist){
            domain.setOpportunityid(opportunity_id);
        }
        quoteService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "quote" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.quoteService.get(#quote_id),'iBizBusinessCentral-Quote-Update')")
    @ApiOperation(value = "根据商机更新报价单", tags = {"报价单" },  notes = "根据商机更新报价单")
	@RequestMapping(method = RequestMethod.PUT, value = "/opportunities/{opportunity_id}/quotes/{quote_id}")
    public ResponseEntity<QuoteDTO> updateByOpportunity(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody QuoteDTO quotedto) {
        Quote domain = quoteMapping.toDomain(quotedto);
        domain.setOpportunityid(opportunity_id);
        domain.setQuoteid(quote_id);
		quoteService.update(domain);
        QuoteDTO dto = quoteMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.quoteService.getQuoteByEntities(this.quoteMapping.toDomain(#quotedtos)),'iBizBusinessCentral-Quote-Update')")
    @ApiOperation(value = "根据商机批量更新报价单", tags = {"报价单" },  notes = "根据商机批量更新报价单")
	@RequestMapping(method = RequestMethod.PUT, value = "/opportunities/{opportunity_id}/quotes/batch")
    public ResponseEntity<Boolean> updateBatchByOpportunity(@PathVariable("opportunity_id") String opportunity_id, @RequestBody List<QuoteDTO> quotedtos) {
        List<Quote> domainlist=quoteMapping.toDomain(quotedtos);
        for(Quote domain:domainlist){
            domain.setOpportunityid(opportunity_id);
        }
        quoteService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.quoteService.get(#quote_id),'iBizBusinessCentral-Quote-Remove')")
    @ApiOperation(value = "根据商机删除报价单", tags = {"报价单" },  notes = "根据商机删除报价单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/opportunities/{opportunity_id}/quotes/{quote_id}")
    public ResponseEntity<Boolean> removeByOpportunity(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id) {
		return ResponseEntity.status(HttpStatus.OK).body(quoteService.remove(quote_id));
    }

    @PreAuthorize("hasPermission(this.quoteService.getQuoteByIds(#ids),'iBizBusinessCentral-Quote-Remove')")
    @ApiOperation(value = "根据商机批量删除报价单", tags = {"报价单" },  notes = "根据商机批量删除报价单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/opportunities/{opportunity_id}/quotes/batch")
    public ResponseEntity<Boolean> removeBatchByOpportunity(@RequestBody List<String> ids) {
        quoteService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.quoteMapping.toDomain(returnObject.body),'iBizBusinessCentral-Quote-Get')")
    @ApiOperation(value = "根据商机获取报价单", tags = {"报价单" },  notes = "根据商机获取报价单")
	@RequestMapping(method = RequestMethod.GET, value = "/opportunities/{opportunity_id}/quotes/{quote_id}")
    public ResponseEntity<QuoteDTO> getByOpportunity(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id) {
        Quote domain = quoteService.get(quote_id);
        QuoteDTO dto = quoteMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据商机获取报价单草稿", tags = {"报价单" },  notes = "根据商机获取报价单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/opportunities/{opportunity_id}/quotes/getdraft")
    public ResponseEntity<QuoteDTO> getDraftByOpportunity(@PathVariable("opportunity_id") String opportunity_id) {
        Quote domain = new Quote();
        domain.setOpportunityid(opportunity_id);
        return ResponseEntity.status(HttpStatus.OK).body(quoteMapping.toDto(quoteService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-Active-all')")
    @ApiOperation(value = "根据商机报价单", tags = {"报价单" },  notes = "根据商机报价单")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/active")
    public ResponseEntity<QuoteDTO> activeByOpportunity(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody QuoteDTO quotedto) {
        Quote domain = quoteMapping.toDomain(quotedto);
        domain.setOpportunityid(opportunity_id);
        domain = quoteService.active(domain) ;
        quotedto = quoteMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(quotedto);
    }

    @ApiOperation(value = "根据商机检查报价单", tags = {"报价单" },  notes = "根据商机检查报价单")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/quotes/checkkey")
    public ResponseEntity<Boolean> checkKeyByOpportunity(@PathVariable("opportunity_id") String opportunity_id, @RequestBody QuoteDTO quotedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(quoteService.checkKey(quoteMapping.toDomain(quotedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-Close-all')")
    @ApiOperation(value = "根据商机报价单", tags = {"报价单" },  notes = "根据商机报价单")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/close")
    public ResponseEntity<QuoteDTO> closeByOpportunity(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody QuoteDTO quotedto) {
        Quote domain = quoteMapping.toDomain(quotedto);
        domain.setOpportunityid(opportunity_id);
        domain = quoteService.close(domain) ;
        quotedto = quoteMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(quotedto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-GenSalesOrder-all')")
    @ApiOperation(value = "根据商机报价单", tags = {"报价单" },  notes = "根据商机报价单")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/gensalesorder")
    public ResponseEntity<QuoteDTO> genSalesOrderByOpportunity(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody QuoteDTO quotedto) {
        Quote domain = quoteMapping.toDomain(quotedto);
        domain.setOpportunityid(opportunity_id);
        domain = quoteService.genSalesOrder(domain) ;
        quotedto = quoteMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(quotedto);
    }

    @PreAuthorize("hasPermission(this.quoteMapping.toDomain(#quotedto),'iBizBusinessCentral-Quote-Save')")
    @ApiOperation(value = "根据商机保存报价单", tags = {"报价单" },  notes = "根据商机保存报价单")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/quotes/save")
    public ResponseEntity<Boolean> saveByOpportunity(@PathVariable("opportunity_id") String opportunity_id, @RequestBody QuoteDTO quotedto) {
        Quote domain = quoteMapping.toDomain(quotedto);
        domain.setOpportunityid(opportunity_id);
        return ResponseEntity.status(HttpStatus.OK).body(quoteService.save(domain));
    }

    @PreAuthorize("hasPermission(this.quoteMapping.toDomain(#quotedtos),'iBizBusinessCentral-Quote-Save')")
    @ApiOperation(value = "根据商机批量保存报价单", tags = {"报价单" },  notes = "根据商机批量保存报价单")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/quotes/savebatch")
    public ResponseEntity<Boolean> saveBatchByOpportunity(@PathVariable("opportunity_id") String opportunity_id, @RequestBody List<QuoteDTO> quotedtos) {
        List<Quote> domainlist=quoteMapping.toDomain(quotedtos);
        for(Quote domain:domainlist){
             domain.setOpportunityid(opportunity_id);
        }
        quoteService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-Win-all')")
    @ApiOperation(value = "根据商机报价单", tags = {"报价单" },  notes = "根据商机报价单")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/quotes/{quote_id}/win")
    public ResponseEntity<QuoteDTO> winByOpportunity(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody QuoteDTO quotedto) {
        Quote domain = quoteMapping.toDomain(quotedto);
        domain.setOpportunityid(opportunity_id);
        domain = quoteService.win(domain) ;
        quotedto = quoteMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(quotedto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchByParentKey-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据商机获取ByParentKey", tags = {"报价单" } ,notes = "根据商机获取ByParentKey")
    @RequestMapping(method= RequestMethod.GET , value="/opportunities/{opportunity_id}/quotes/fetchbyparentkey")
	public ResponseEntity<List<QuoteDTO>> fetchQuoteByParentKeyByOpportunity(@PathVariable("opportunity_id") String opportunity_id,QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchByParentKey(context) ;
        List<QuoteDTO> list = quoteMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchByParentKey-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据商机查询ByParentKey", tags = {"报价单" } ,notes = "根据商机查询ByParentKey")
    @RequestMapping(method= RequestMethod.POST , value="/opportunities/{opportunity_id}/quotes/searchbyparentkey")
	public ResponseEntity<Page<QuoteDTO>> searchQuoteByParentKeyByOpportunity(@PathVariable("opportunity_id") String opportunity_id, @RequestBody QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchByParentKey(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(quoteMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchClosed-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据商机获取已关闭", tags = {"报价单" } ,notes = "根据商机获取已关闭")
    @RequestMapping(method= RequestMethod.GET , value="/opportunities/{opportunity_id}/quotes/fetchclosed")
	public ResponseEntity<List<QuoteDTO>> fetchQuoteClosedByOpportunity(@PathVariable("opportunity_id") String opportunity_id,QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchClosed(context) ;
        List<QuoteDTO> list = quoteMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchClosed-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据商机查询已关闭", tags = {"报价单" } ,notes = "根据商机查询已关闭")
    @RequestMapping(method= RequestMethod.POST , value="/opportunities/{opportunity_id}/quotes/searchclosed")
	public ResponseEntity<Page<QuoteDTO>> searchQuoteClosedByOpportunity(@PathVariable("opportunity_id") String opportunity_id, @RequestBody QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchClosed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(quoteMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据商机获取DEFAULT", tags = {"报价单" } ,notes = "根据商机获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/opportunities/{opportunity_id}/quotes/fetchdefault")
	public ResponseEntity<List<QuoteDTO>> fetchQuoteDefaultByOpportunity(@PathVariable("opportunity_id") String opportunity_id,QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchDefault(context) ;
        List<QuoteDTO> list = quoteMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据商机查询DEFAULT", tags = {"报价单" } ,notes = "根据商机查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/opportunities/{opportunity_id}/quotes/searchdefault")
	public ResponseEntity<Page<QuoteDTO>> searchQuoteDefaultByOpportunity(@PathVariable("opportunity_id") String opportunity_id, @RequestBody QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(quoteMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchDraft-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据商机获取草稿", tags = {"报价单" } ,notes = "根据商机获取草稿")
    @RequestMapping(method= RequestMethod.GET , value="/opportunities/{opportunity_id}/quotes/fetchdraft")
	public ResponseEntity<List<QuoteDTO>> fetchQuoteDraftByOpportunity(@PathVariable("opportunity_id") String opportunity_id,QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchDraft(context) ;
        List<QuoteDTO> list = quoteMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchDraft-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据商机查询草稿", tags = {"报价单" } ,notes = "根据商机查询草稿")
    @RequestMapping(method= RequestMethod.POST , value="/opportunities/{opportunity_id}/quotes/searchdraft")
	public ResponseEntity<Page<QuoteDTO>> searchQuoteDraftByOpportunity(@PathVariable("opportunity_id") String opportunity_id, @RequestBody QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(quoteMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchEffective-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据商机获取有效报价单", tags = {"报价单" } ,notes = "根据商机获取有效报价单")
    @RequestMapping(method= RequestMethod.GET , value="/opportunities/{opportunity_id}/quotes/fetcheffective")
	public ResponseEntity<List<QuoteDTO>> fetchQuoteEffectiveByOpportunity(@PathVariable("opportunity_id") String opportunity_id,QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchEffective(context) ;
        List<QuoteDTO> list = quoteMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchEffective-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据商机查询有效报价单", tags = {"报价单" } ,notes = "根据商机查询有效报价单")
    @RequestMapping(method= RequestMethod.POST , value="/opportunities/{opportunity_id}/quotes/searcheffective")
	public ResponseEntity<Page<QuoteDTO>> searchQuoteEffectiveByOpportunity(@PathVariable("opportunity_id") String opportunity_id, @RequestBody QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchEffective(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(quoteMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchWin-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据商机获取赢单", tags = {"报价单" } ,notes = "根据商机获取赢单")
    @RequestMapping(method= RequestMethod.GET , value="/opportunities/{opportunity_id}/quotes/fetchwin")
	public ResponseEntity<List<QuoteDTO>> fetchQuoteWinByOpportunity(@PathVariable("opportunity_id") String opportunity_id,QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchWin(context) ;
        List<QuoteDTO> list = quoteMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchWin-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据商机查询赢单", tags = {"报价单" } ,notes = "根据商机查询赢单")
    @RequestMapping(method= RequestMethod.POST , value="/opportunities/{opportunity_id}/quotes/searchwin")
	public ResponseEntity<Page<QuoteDTO>> searchQuoteWinByOpportunity(@PathVariable("opportunity_id") String opportunity_id, @RequestBody QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchWin(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(quoteMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.quoteMapping.toDomain(#quotedto),'iBizBusinessCentral-Quote-Create')")
    @ApiOperation(value = "根据客户商机建立报价单", tags = {"报价单" },  notes = "根据客户商机建立报价单")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes")
    public ResponseEntity<QuoteDTO> createByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody QuoteDTO quotedto) {
        Quote domain = quoteMapping.toDomain(quotedto);
        domain.setOpportunityid(opportunity_id);
		quoteService.create(domain);
        QuoteDTO dto = quoteMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.quoteMapping.toDomain(#quotedtos),'iBizBusinessCentral-Quote-Create')")
    @ApiOperation(value = "根据客户商机批量建立报价单", tags = {"报价单" },  notes = "根据客户商机批量建立报价单")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/batch")
    public ResponseEntity<Boolean> createBatchByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody List<QuoteDTO> quotedtos) {
        List<Quote> domainlist=quoteMapping.toDomain(quotedtos);
        for(Quote domain:domainlist){
            domain.setOpportunityid(opportunity_id);
        }
        quoteService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "quote" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.quoteService.get(#quote_id),'iBizBusinessCentral-Quote-Update')")
    @ApiOperation(value = "根据客户商机更新报价单", tags = {"报价单" },  notes = "根据客户商机更新报价单")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}")
    public ResponseEntity<QuoteDTO> updateByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody QuoteDTO quotedto) {
        Quote domain = quoteMapping.toDomain(quotedto);
        domain.setOpportunityid(opportunity_id);
        domain.setQuoteid(quote_id);
		quoteService.update(domain);
        QuoteDTO dto = quoteMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.quoteService.getQuoteByEntities(this.quoteMapping.toDomain(#quotedtos)),'iBizBusinessCentral-Quote-Update')")
    @ApiOperation(value = "根据客户商机批量更新报价单", tags = {"报价单" },  notes = "根据客户商机批量更新报价单")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/batch")
    public ResponseEntity<Boolean> updateBatchByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody List<QuoteDTO> quotedtos) {
        List<Quote> domainlist=quoteMapping.toDomain(quotedtos);
        for(Quote domain:domainlist){
            domain.setOpportunityid(opportunity_id);
        }
        quoteService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.quoteService.get(#quote_id),'iBizBusinessCentral-Quote-Remove')")
    @ApiOperation(value = "根据客户商机删除报价单", tags = {"报价单" },  notes = "根据客户商机删除报价单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}")
    public ResponseEntity<Boolean> removeByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id) {
		return ResponseEntity.status(HttpStatus.OK).body(quoteService.remove(quote_id));
    }

    @PreAuthorize("hasPermission(this.quoteService.getQuoteByIds(#ids),'iBizBusinessCentral-Quote-Remove')")
    @ApiOperation(value = "根据客户商机批量删除报价单", tags = {"报价单" },  notes = "根据客户商机批量删除报价单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/batch")
    public ResponseEntity<Boolean> removeBatchByAccountOpportunity(@RequestBody List<String> ids) {
        quoteService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.quoteMapping.toDomain(returnObject.body),'iBizBusinessCentral-Quote-Get')")
    @ApiOperation(value = "根据客户商机获取报价单", tags = {"报价单" },  notes = "根据客户商机获取报价单")
	@RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}")
    public ResponseEntity<QuoteDTO> getByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id) {
        Quote domain = quoteService.get(quote_id);
        QuoteDTO dto = quoteMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据客户商机获取报价单草稿", tags = {"报价单" },  notes = "根据客户商机获取报价单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/getdraft")
    public ResponseEntity<QuoteDTO> getDraftByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id) {
        Quote domain = new Quote();
        domain.setOpportunityid(opportunity_id);
        return ResponseEntity.status(HttpStatus.OK).body(quoteMapping.toDto(quoteService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-Active-all')")
    @ApiOperation(value = "根据客户商机报价单", tags = {"报价单" },  notes = "根据客户商机报价单")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/active")
    public ResponseEntity<QuoteDTO> activeByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody QuoteDTO quotedto) {
        Quote domain = quoteMapping.toDomain(quotedto);
        domain.setOpportunityid(opportunity_id);
        domain = quoteService.active(domain) ;
        quotedto = quoteMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(quotedto);
    }

    @ApiOperation(value = "根据客户商机检查报价单", tags = {"报价单" },  notes = "根据客户商机检查报价单")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/checkkey")
    public ResponseEntity<Boolean> checkKeyByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody QuoteDTO quotedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(quoteService.checkKey(quoteMapping.toDomain(quotedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-Close-all')")
    @ApiOperation(value = "根据客户商机报价单", tags = {"报价单" },  notes = "根据客户商机报价单")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/close")
    public ResponseEntity<QuoteDTO> closeByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody QuoteDTO quotedto) {
        Quote domain = quoteMapping.toDomain(quotedto);
        domain.setOpportunityid(opportunity_id);
        domain = quoteService.close(domain) ;
        quotedto = quoteMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(quotedto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-GenSalesOrder-all')")
    @ApiOperation(value = "根据客户商机报价单", tags = {"报价单" },  notes = "根据客户商机报价单")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/gensalesorder")
    public ResponseEntity<QuoteDTO> genSalesOrderByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody QuoteDTO quotedto) {
        Quote domain = quoteMapping.toDomain(quotedto);
        domain.setOpportunityid(opportunity_id);
        domain = quoteService.genSalesOrder(domain) ;
        quotedto = quoteMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(quotedto);
    }

    @PreAuthorize("hasPermission(this.quoteMapping.toDomain(#quotedto),'iBizBusinessCentral-Quote-Save')")
    @ApiOperation(value = "根据客户商机保存报价单", tags = {"报价单" },  notes = "根据客户商机保存报价单")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/save")
    public ResponseEntity<Boolean> saveByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody QuoteDTO quotedto) {
        Quote domain = quoteMapping.toDomain(quotedto);
        domain.setOpportunityid(opportunity_id);
        return ResponseEntity.status(HttpStatus.OK).body(quoteService.save(domain));
    }

    @PreAuthorize("hasPermission(this.quoteMapping.toDomain(#quotedtos),'iBizBusinessCentral-Quote-Save')")
    @ApiOperation(value = "根据客户商机批量保存报价单", tags = {"报价单" },  notes = "根据客户商机批量保存报价单")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/savebatch")
    public ResponseEntity<Boolean> saveBatchByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody List<QuoteDTO> quotedtos) {
        List<Quote> domainlist=quoteMapping.toDomain(quotedtos);
        for(Quote domain:domainlist){
             domain.setOpportunityid(opportunity_id);
        }
        quoteService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-Win-all')")
    @ApiOperation(value = "根据客户商机报价单", tags = {"报价单" },  notes = "根据客户商机报价单")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/quotes/{quote_id}/win")
    public ResponseEntity<QuoteDTO> winByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody QuoteDTO quotedto) {
        Quote domain = quoteMapping.toDomain(quotedto);
        domain.setOpportunityid(opportunity_id);
        domain = quoteService.win(domain) ;
        quotedto = quoteMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(quotedto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchByParentKey-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据客户商机获取ByParentKey", tags = {"报价单" } ,notes = "根据客户商机获取ByParentKey")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/opportunities/{opportunity_id}/quotes/fetchbyparentkey")
	public ResponseEntity<List<QuoteDTO>> fetchQuoteByParentKeyByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id,QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchByParentKey(context) ;
        List<QuoteDTO> list = quoteMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchByParentKey-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据客户商机查询ByParentKey", tags = {"报价单" } ,notes = "根据客户商机查询ByParentKey")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/opportunities/{opportunity_id}/quotes/searchbyparentkey")
	public ResponseEntity<Page<QuoteDTO>> searchQuoteByParentKeyByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchByParentKey(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(quoteMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchClosed-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据客户商机获取已关闭", tags = {"报价单" } ,notes = "根据客户商机获取已关闭")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/opportunities/{opportunity_id}/quotes/fetchclosed")
	public ResponseEntity<List<QuoteDTO>> fetchQuoteClosedByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id,QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchClosed(context) ;
        List<QuoteDTO> list = quoteMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchClosed-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据客户商机查询已关闭", tags = {"报价单" } ,notes = "根据客户商机查询已关闭")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/opportunities/{opportunity_id}/quotes/searchclosed")
	public ResponseEntity<Page<QuoteDTO>> searchQuoteClosedByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchClosed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(quoteMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据客户商机获取DEFAULT", tags = {"报价单" } ,notes = "根据客户商机获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/opportunities/{opportunity_id}/quotes/fetchdefault")
	public ResponseEntity<List<QuoteDTO>> fetchQuoteDefaultByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id,QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchDefault(context) ;
        List<QuoteDTO> list = quoteMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据客户商机查询DEFAULT", tags = {"报价单" } ,notes = "根据客户商机查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/opportunities/{opportunity_id}/quotes/searchdefault")
	public ResponseEntity<Page<QuoteDTO>> searchQuoteDefaultByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(quoteMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchDraft-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据客户商机获取草稿", tags = {"报价单" } ,notes = "根据客户商机获取草稿")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/opportunities/{opportunity_id}/quotes/fetchdraft")
	public ResponseEntity<List<QuoteDTO>> fetchQuoteDraftByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id,QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchDraft(context) ;
        List<QuoteDTO> list = quoteMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchDraft-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据客户商机查询草稿", tags = {"报价单" } ,notes = "根据客户商机查询草稿")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/opportunities/{opportunity_id}/quotes/searchdraft")
	public ResponseEntity<Page<QuoteDTO>> searchQuoteDraftByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(quoteMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchEffective-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据客户商机获取有效报价单", tags = {"报价单" } ,notes = "根据客户商机获取有效报价单")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/opportunities/{opportunity_id}/quotes/fetcheffective")
	public ResponseEntity<List<QuoteDTO>> fetchQuoteEffectiveByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id,QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchEffective(context) ;
        List<QuoteDTO> list = quoteMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchEffective-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据客户商机查询有效报价单", tags = {"报价单" } ,notes = "根据客户商机查询有效报价单")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/opportunities/{opportunity_id}/quotes/searcheffective")
	public ResponseEntity<Page<QuoteDTO>> searchQuoteEffectiveByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchEffective(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(quoteMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchWin-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据客户商机获取赢单", tags = {"报价单" } ,notes = "根据客户商机获取赢单")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/opportunities/{opportunity_id}/quotes/fetchwin")
	public ResponseEntity<List<QuoteDTO>> fetchQuoteWinByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id,QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchWin(context) ;
        List<QuoteDTO> list = quoteMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchWin-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据客户商机查询赢单", tags = {"报价单" } ,notes = "根据客户商机查询赢单")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/opportunities/{opportunity_id}/quotes/searchwin")
	public ResponseEntity<Page<QuoteDTO>> searchQuoteWinByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchWin(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(quoteMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.quoteMapping.toDomain(#quotedto),'iBizBusinessCentral-Quote-Create')")
    @ApiOperation(value = "根据联系人商机建立报价单", tags = {"报价单" },  notes = "根据联系人商机建立报价单")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes")
    public ResponseEntity<QuoteDTO> createByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody QuoteDTO quotedto) {
        Quote domain = quoteMapping.toDomain(quotedto);
        domain.setOpportunityid(opportunity_id);
		quoteService.create(domain);
        QuoteDTO dto = quoteMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.quoteMapping.toDomain(#quotedtos),'iBizBusinessCentral-Quote-Create')")
    @ApiOperation(value = "根据联系人商机批量建立报价单", tags = {"报价单" },  notes = "根据联系人商机批量建立报价单")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/batch")
    public ResponseEntity<Boolean> createBatchByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody List<QuoteDTO> quotedtos) {
        List<Quote> domainlist=quoteMapping.toDomain(quotedtos);
        for(Quote domain:domainlist){
            domain.setOpportunityid(opportunity_id);
        }
        quoteService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "quote" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.quoteService.get(#quote_id),'iBizBusinessCentral-Quote-Update')")
    @ApiOperation(value = "根据联系人商机更新报价单", tags = {"报价单" },  notes = "根据联系人商机更新报价单")
	@RequestMapping(method = RequestMethod.PUT, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}")
    public ResponseEntity<QuoteDTO> updateByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody QuoteDTO quotedto) {
        Quote domain = quoteMapping.toDomain(quotedto);
        domain.setOpportunityid(opportunity_id);
        domain.setQuoteid(quote_id);
		quoteService.update(domain);
        QuoteDTO dto = quoteMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.quoteService.getQuoteByEntities(this.quoteMapping.toDomain(#quotedtos)),'iBizBusinessCentral-Quote-Update')")
    @ApiOperation(value = "根据联系人商机批量更新报价单", tags = {"报价单" },  notes = "根据联系人商机批量更新报价单")
	@RequestMapping(method = RequestMethod.PUT, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/batch")
    public ResponseEntity<Boolean> updateBatchByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody List<QuoteDTO> quotedtos) {
        List<Quote> domainlist=quoteMapping.toDomain(quotedtos);
        for(Quote domain:domainlist){
            domain.setOpportunityid(opportunity_id);
        }
        quoteService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.quoteService.get(#quote_id),'iBizBusinessCentral-Quote-Remove')")
    @ApiOperation(value = "根据联系人商机删除报价单", tags = {"报价单" },  notes = "根据联系人商机删除报价单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}")
    public ResponseEntity<Boolean> removeByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id) {
		return ResponseEntity.status(HttpStatus.OK).body(quoteService.remove(quote_id));
    }

    @PreAuthorize("hasPermission(this.quoteService.getQuoteByIds(#ids),'iBizBusinessCentral-Quote-Remove')")
    @ApiOperation(value = "根据联系人商机批量删除报价单", tags = {"报价单" },  notes = "根据联系人商机批量删除报价单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/batch")
    public ResponseEntity<Boolean> removeBatchByContactOpportunity(@RequestBody List<String> ids) {
        quoteService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.quoteMapping.toDomain(returnObject.body),'iBizBusinessCentral-Quote-Get')")
    @ApiOperation(value = "根据联系人商机获取报价单", tags = {"报价单" },  notes = "根据联系人商机获取报价单")
	@RequestMapping(method = RequestMethod.GET, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}")
    public ResponseEntity<QuoteDTO> getByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id) {
        Quote domain = quoteService.get(quote_id);
        QuoteDTO dto = quoteMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据联系人商机获取报价单草稿", tags = {"报价单" },  notes = "根据联系人商机获取报价单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/getdraft")
    public ResponseEntity<QuoteDTO> getDraftByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id) {
        Quote domain = new Quote();
        domain.setOpportunityid(opportunity_id);
        return ResponseEntity.status(HttpStatus.OK).body(quoteMapping.toDto(quoteService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-Active-all')")
    @ApiOperation(value = "根据联系人商机报价单", tags = {"报价单" },  notes = "根据联系人商机报价单")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/active")
    public ResponseEntity<QuoteDTO> activeByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody QuoteDTO quotedto) {
        Quote domain = quoteMapping.toDomain(quotedto);
        domain.setOpportunityid(opportunity_id);
        domain = quoteService.active(domain) ;
        quotedto = quoteMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(quotedto);
    }

    @ApiOperation(value = "根据联系人商机检查报价单", tags = {"报价单" },  notes = "根据联系人商机检查报价单")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/checkkey")
    public ResponseEntity<Boolean> checkKeyByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody QuoteDTO quotedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(quoteService.checkKey(quoteMapping.toDomain(quotedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-Close-all')")
    @ApiOperation(value = "根据联系人商机报价单", tags = {"报价单" },  notes = "根据联系人商机报价单")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/close")
    public ResponseEntity<QuoteDTO> closeByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody QuoteDTO quotedto) {
        Quote domain = quoteMapping.toDomain(quotedto);
        domain.setOpportunityid(opportunity_id);
        domain = quoteService.close(domain) ;
        quotedto = quoteMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(quotedto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-GenSalesOrder-all')")
    @ApiOperation(value = "根据联系人商机报价单", tags = {"报价单" },  notes = "根据联系人商机报价单")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/gensalesorder")
    public ResponseEntity<QuoteDTO> genSalesOrderByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody QuoteDTO quotedto) {
        Quote domain = quoteMapping.toDomain(quotedto);
        domain.setOpportunityid(opportunity_id);
        domain = quoteService.genSalesOrder(domain) ;
        quotedto = quoteMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(quotedto);
    }

    @PreAuthorize("hasPermission(this.quoteMapping.toDomain(#quotedto),'iBizBusinessCentral-Quote-Save')")
    @ApiOperation(value = "根据联系人商机保存报价单", tags = {"报价单" },  notes = "根据联系人商机保存报价单")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/save")
    public ResponseEntity<Boolean> saveByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody QuoteDTO quotedto) {
        Quote domain = quoteMapping.toDomain(quotedto);
        domain.setOpportunityid(opportunity_id);
        return ResponseEntity.status(HttpStatus.OK).body(quoteService.save(domain));
    }

    @PreAuthorize("hasPermission(this.quoteMapping.toDomain(#quotedtos),'iBizBusinessCentral-Quote-Save')")
    @ApiOperation(value = "根据联系人商机批量保存报价单", tags = {"报价单" },  notes = "根据联系人商机批量保存报价单")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/savebatch")
    public ResponseEntity<Boolean> saveBatchByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody List<QuoteDTO> quotedtos) {
        List<Quote> domainlist=quoteMapping.toDomain(quotedtos);
        for(Quote domain:domainlist){
             domain.setOpportunityid(opportunity_id);
        }
        quoteService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-Win-all')")
    @ApiOperation(value = "根据联系人商机报价单", tags = {"报价单" },  notes = "根据联系人商机报价单")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/win")
    public ResponseEntity<QuoteDTO> winByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody QuoteDTO quotedto) {
        Quote domain = quoteMapping.toDomain(quotedto);
        domain.setOpportunityid(opportunity_id);
        domain = quoteService.win(domain) ;
        quotedto = quoteMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(quotedto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchByParentKey-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据联系人商机获取ByParentKey", tags = {"报价单" } ,notes = "根据联系人商机获取ByParentKey")
    @RequestMapping(method= RequestMethod.GET , value="/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/fetchbyparentkey")
	public ResponseEntity<List<QuoteDTO>> fetchQuoteByParentKeyByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id,QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchByParentKey(context) ;
        List<QuoteDTO> list = quoteMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchByParentKey-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据联系人商机查询ByParentKey", tags = {"报价单" } ,notes = "根据联系人商机查询ByParentKey")
    @RequestMapping(method= RequestMethod.POST , value="/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/searchbyparentkey")
	public ResponseEntity<Page<QuoteDTO>> searchQuoteByParentKeyByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchByParentKey(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(quoteMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchClosed-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据联系人商机获取已关闭", tags = {"报价单" } ,notes = "根据联系人商机获取已关闭")
    @RequestMapping(method= RequestMethod.GET , value="/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/fetchclosed")
	public ResponseEntity<List<QuoteDTO>> fetchQuoteClosedByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id,QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchClosed(context) ;
        List<QuoteDTO> list = quoteMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchClosed-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据联系人商机查询已关闭", tags = {"报价单" } ,notes = "根据联系人商机查询已关闭")
    @RequestMapping(method= RequestMethod.POST , value="/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/searchclosed")
	public ResponseEntity<Page<QuoteDTO>> searchQuoteClosedByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchClosed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(quoteMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据联系人商机获取DEFAULT", tags = {"报价单" } ,notes = "根据联系人商机获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/fetchdefault")
	public ResponseEntity<List<QuoteDTO>> fetchQuoteDefaultByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id,QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchDefault(context) ;
        List<QuoteDTO> list = quoteMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据联系人商机查询DEFAULT", tags = {"报价单" } ,notes = "根据联系人商机查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/searchdefault")
	public ResponseEntity<Page<QuoteDTO>> searchQuoteDefaultByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(quoteMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchDraft-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据联系人商机获取草稿", tags = {"报价单" } ,notes = "根据联系人商机获取草稿")
    @RequestMapping(method= RequestMethod.GET , value="/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/fetchdraft")
	public ResponseEntity<List<QuoteDTO>> fetchQuoteDraftByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id,QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchDraft(context) ;
        List<QuoteDTO> list = quoteMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchDraft-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据联系人商机查询草稿", tags = {"报价单" } ,notes = "根据联系人商机查询草稿")
    @RequestMapping(method= RequestMethod.POST , value="/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/searchdraft")
	public ResponseEntity<Page<QuoteDTO>> searchQuoteDraftByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(quoteMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchEffective-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据联系人商机获取有效报价单", tags = {"报价单" } ,notes = "根据联系人商机获取有效报价单")
    @RequestMapping(method= RequestMethod.GET , value="/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/fetcheffective")
	public ResponseEntity<List<QuoteDTO>> fetchQuoteEffectiveByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id,QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchEffective(context) ;
        List<QuoteDTO> list = quoteMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchEffective-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据联系人商机查询有效报价单", tags = {"报价单" } ,notes = "根据联系人商机查询有效报价单")
    @RequestMapping(method= RequestMethod.POST , value="/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/searcheffective")
	public ResponseEntity<Page<QuoteDTO>> searchQuoteEffectiveByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchEffective(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(quoteMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchWin-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据联系人商机获取赢单", tags = {"报价单" } ,notes = "根据联系人商机获取赢单")
    @RequestMapping(method= RequestMethod.GET , value="/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/fetchwin")
	public ResponseEntity<List<QuoteDTO>> fetchQuoteWinByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id,QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchWin(context) ;
        List<QuoteDTO> list = quoteMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchWin-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据联系人商机查询赢单", tags = {"报价单" } ,notes = "根据联系人商机查询赢单")
    @RequestMapping(method= RequestMethod.POST , value="/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/searchwin")
	public ResponseEntity<Page<QuoteDTO>> searchQuoteWinByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchWin(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(quoteMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.quoteMapping.toDomain(#quotedto),'iBizBusinessCentral-Quote-Create')")
    @ApiOperation(value = "根据客户联系人商机建立报价单", tags = {"报价单" },  notes = "根据客户联系人商机建立报价单")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes")
    public ResponseEntity<QuoteDTO> createByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody QuoteDTO quotedto) {
        Quote domain = quoteMapping.toDomain(quotedto);
        domain.setOpportunityid(opportunity_id);
		quoteService.create(domain);
        QuoteDTO dto = quoteMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.quoteMapping.toDomain(#quotedtos),'iBizBusinessCentral-Quote-Create')")
    @ApiOperation(value = "根据客户联系人商机批量建立报价单", tags = {"报价单" },  notes = "根据客户联系人商机批量建立报价单")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/batch")
    public ResponseEntity<Boolean> createBatchByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody List<QuoteDTO> quotedtos) {
        List<Quote> domainlist=quoteMapping.toDomain(quotedtos);
        for(Quote domain:domainlist){
            domain.setOpportunityid(opportunity_id);
        }
        quoteService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "quote" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.quoteService.get(#quote_id),'iBizBusinessCentral-Quote-Update')")
    @ApiOperation(value = "根据客户联系人商机更新报价单", tags = {"报价单" },  notes = "根据客户联系人商机更新报价单")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}")
    public ResponseEntity<QuoteDTO> updateByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody QuoteDTO quotedto) {
        Quote domain = quoteMapping.toDomain(quotedto);
        domain.setOpportunityid(opportunity_id);
        domain.setQuoteid(quote_id);
		quoteService.update(domain);
        QuoteDTO dto = quoteMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.quoteService.getQuoteByEntities(this.quoteMapping.toDomain(#quotedtos)),'iBizBusinessCentral-Quote-Update')")
    @ApiOperation(value = "根据客户联系人商机批量更新报价单", tags = {"报价单" },  notes = "根据客户联系人商机批量更新报价单")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/batch")
    public ResponseEntity<Boolean> updateBatchByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody List<QuoteDTO> quotedtos) {
        List<Quote> domainlist=quoteMapping.toDomain(quotedtos);
        for(Quote domain:domainlist){
            domain.setOpportunityid(opportunity_id);
        }
        quoteService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.quoteService.get(#quote_id),'iBizBusinessCentral-Quote-Remove')")
    @ApiOperation(value = "根据客户联系人商机删除报价单", tags = {"报价单" },  notes = "根据客户联系人商机删除报价单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}")
    public ResponseEntity<Boolean> removeByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id) {
		return ResponseEntity.status(HttpStatus.OK).body(quoteService.remove(quote_id));
    }

    @PreAuthorize("hasPermission(this.quoteService.getQuoteByIds(#ids),'iBizBusinessCentral-Quote-Remove')")
    @ApiOperation(value = "根据客户联系人商机批量删除报价单", tags = {"报价单" },  notes = "根据客户联系人商机批量删除报价单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/batch")
    public ResponseEntity<Boolean> removeBatchByAccountContactOpportunity(@RequestBody List<String> ids) {
        quoteService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.quoteMapping.toDomain(returnObject.body),'iBizBusinessCentral-Quote-Get')")
    @ApiOperation(value = "根据客户联系人商机获取报价单", tags = {"报价单" },  notes = "根据客户联系人商机获取报价单")
	@RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}")
    public ResponseEntity<QuoteDTO> getByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id) {
        Quote domain = quoteService.get(quote_id);
        QuoteDTO dto = quoteMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据客户联系人商机获取报价单草稿", tags = {"报价单" },  notes = "根据客户联系人商机获取报价单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/getdraft")
    public ResponseEntity<QuoteDTO> getDraftByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id) {
        Quote domain = new Quote();
        domain.setOpportunityid(opportunity_id);
        return ResponseEntity.status(HttpStatus.OK).body(quoteMapping.toDto(quoteService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-Active-all')")
    @ApiOperation(value = "根据客户联系人商机报价单", tags = {"报价单" },  notes = "根据客户联系人商机报价单")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/active")
    public ResponseEntity<QuoteDTO> activeByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody QuoteDTO quotedto) {
        Quote domain = quoteMapping.toDomain(quotedto);
        domain.setOpportunityid(opportunity_id);
        domain = quoteService.active(domain) ;
        quotedto = quoteMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(quotedto);
    }

    @ApiOperation(value = "根据客户联系人商机检查报价单", tags = {"报价单" },  notes = "根据客户联系人商机检查报价单")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/checkkey")
    public ResponseEntity<Boolean> checkKeyByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody QuoteDTO quotedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(quoteService.checkKey(quoteMapping.toDomain(quotedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-Close-all')")
    @ApiOperation(value = "根据客户联系人商机报价单", tags = {"报价单" },  notes = "根据客户联系人商机报价单")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/close")
    public ResponseEntity<QuoteDTO> closeByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody QuoteDTO quotedto) {
        Quote domain = quoteMapping.toDomain(quotedto);
        domain.setOpportunityid(opportunity_id);
        domain = quoteService.close(domain) ;
        quotedto = quoteMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(quotedto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-GenSalesOrder-all')")
    @ApiOperation(value = "根据客户联系人商机报价单", tags = {"报价单" },  notes = "根据客户联系人商机报价单")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/gensalesorder")
    public ResponseEntity<QuoteDTO> genSalesOrderByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody QuoteDTO quotedto) {
        Quote domain = quoteMapping.toDomain(quotedto);
        domain.setOpportunityid(opportunity_id);
        domain = quoteService.genSalesOrder(domain) ;
        quotedto = quoteMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(quotedto);
    }

    @PreAuthorize("hasPermission(this.quoteMapping.toDomain(#quotedto),'iBizBusinessCentral-Quote-Save')")
    @ApiOperation(value = "根据客户联系人商机保存报价单", tags = {"报价单" },  notes = "根据客户联系人商机保存报价单")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/save")
    public ResponseEntity<Boolean> saveByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody QuoteDTO quotedto) {
        Quote domain = quoteMapping.toDomain(quotedto);
        domain.setOpportunityid(opportunity_id);
        return ResponseEntity.status(HttpStatus.OK).body(quoteService.save(domain));
    }

    @PreAuthorize("hasPermission(this.quoteMapping.toDomain(#quotedtos),'iBizBusinessCentral-Quote-Save')")
    @ApiOperation(value = "根据客户联系人商机批量保存报价单", tags = {"报价单" },  notes = "根据客户联系人商机批量保存报价单")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/savebatch")
    public ResponseEntity<Boolean> saveBatchByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody List<QuoteDTO> quotedtos) {
        List<Quote> domainlist=quoteMapping.toDomain(quotedtos);
        for(Quote domain:domainlist){
             domain.setOpportunityid(opportunity_id);
        }
        quoteService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-Win-all')")
    @ApiOperation(value = "根据客户联系人商机报价单", tags = {"报价单" },  notes = "根据客户联系人商机报价单")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/{quote_id}/win")
    public ResponseEntity<QuoteDTO> winByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("quote_id") String quote_id, @RequestBody QuoteDTO quotedto) {
        Quote domain = quoteMapping.toDomain(quotedto);
        domain.setOpportunityid(opportunity_id);
        domain = quoteService.win(domain) ;
        quotedto = quoteMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(quotedto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchByParentKey-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据客户联系人商机获取ByParentKey", tags = {"报价单" } ,notes = "根据客户联系人商机获取ByParentKey")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/fetchbyparentkey")
	public ResponseEntity<List<QuoteDTO>> fetchQuoteByParentKeyByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id,QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchByParentKey(context) ;
        List<QuoteDTO> list = quoteMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchByParentKey-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据客户联系人商机查询ByParentKey", tags = {"报价单" } ,notes = "根据客户联系人商机查询ByParentKey")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/searchbyparentkey")
	public ResponseEntity<Page<QuoteDTO>> searchQuoteByParentKeyByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchByParentKey(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(quoteMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchClosed-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据客户联系人商机获取已关闭", tags = {"报价单" } ,notes = "根据客户联系人商机获取已关闭")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/fetchclosed")
	public ResponseEntity<List<QuoteDTO>> fetchQuoteClosedByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id,QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchClosed(context) ;
        List<QuoteDTO> list = quoteMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchClosed-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据客户联系人商机查询已关闭", tags = {"报价单" } ,notes = "根据客户联系人商机查询已关闭")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/searchclosed")
	public ResponseEntity<Page<QuoteDTO>> searchQuoteClosedByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchClosed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(quoteMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据客户联系人商机获取DEFAULT", tags = {"报价单" } ,notes = "根据客户联系人商机获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/fetchdefault")
	public ResponseEntity<List<QuoteDTO>> fetchQuoteDefaultByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id,QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchDefault(context) ;
        List<QuoteDTO> list = quoteMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据客户联系人商机查询DEFAULT", tags = {"报价单" } ,notes = "根据客户联系人商机查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/searchdefault")
	public ResponseEntity<Page<QuoteDTO>> searchQuoteDefaultByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(quoteMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchDraft-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据客户联系人商机获取草稿", tags = {"报价单" } ,notes = "根据客户联系人商机获取草稿")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/fetchdraft")
	public ResponseEntity<List<QuoteDTO>> fetchQuoteDraftByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id,QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchDraft(context) ;
        List<QuoteDTO> list = quoteMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchDraft-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据客户联系人商机查询草稿", tags = {"报价单" } ,notes = "根据客户联系人商机查询草稿")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/searchdraft")
	public ResponseEntity<Page<QuoteDTO>> searchQuoteDraftByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(quoteMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchEffective-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据客户联系人商机获取有效报价单", tags = {"报价单" } ,notes = "根据客户联系人商机获取有效报价单")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/fetcheffective")
	public ResponseEntity<List<QuoteDTO>> fetchQuoteEffectiveByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id,QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchEffective(context) ;
        List<QuoteDTO> list = quoteMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchEffective-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据客户联系人商机查询有效报价单", tags = {"报价单" } ,notes = "根据客户联系人商机查询有效报价单")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/searcheffective")
	public ResponseEntity<Page<QuoteDTO>> searchQuoteEffectiveByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchEffective(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(quoteMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchWin-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据客户联系人商机获取赢单", tags = {"报价单" } ,notes = "根据客户联系人商机获取赢单")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/fetchwin")
	public ResponseEntity<List<QuoteDTO>> fetchQuoteWinByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id,QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchWin(context) ;
        List<QuoteDTO> list = quoteMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Quote-searchWin-all') and hasPermission(#context,'iBizBusinessCentral-Quote-Get')")
	@ApiOperation(value = "根据客户联系人商机查询赢单", tags = {"报价单" } ,notes = "根据客户联系人商机查询赢单")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/quotes/searchwin")
	public ResponseEntity<Page<QuoteDTO>> searchQuoteWinByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody QuoteSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<Quote> domains = quoteService.searchWin(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(quoteMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

