package cn.ibizlab.businesscentral.centralapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[LeadDTO]
 */
@Data
public class LeadDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ADDRESS1_FAX]
     *
     */
    @JSONField(name = "address1_fax")
    @JsonProperty("address1_fax")
    private String address1Fax;

    /**
     * 属性 [ADDRESS2_UTCOFFSET]
     *
     */
    @JSONField(name = "address2_utcoffset")
    @JsonProperty("address2_utcoffset")
    private Integer address2Utcoffset;

    /**
     * 属性 [JOBTITLE]
     *
     */
    @JSONField(name = "jobtitle")
    @JsonProperty("jobtitle")
    private String jobtitle;

    /**
     * 属性 [ADDRESS2_COUNTRY]
     *
     */
    @JSONField(name = "address2_country")
    @JsonProperty("address2_country")
    private String address2Country;

    /**
     * 属性 [BUDGETAMOUNT]
     *
     */
    @JSONField(name = "budgetamount")
    @JsonProperty("budgetamount")
    private BigDecimal budgetamount;

    /**
     * 属性 [ADDRESS2_FAX]
     *
     */
    @JSONField(name = "address2_fax")
    @JsonProperty("address2_fax")
    private String address2Fax;

    /**
     * 属性 [ONHOLDTIME]
     *
     */
    @JSONField(name = "onholdtime")
    @JsonProperty("onholdtime")
    private Integer onholdtime;

    /**
     * 属性 [LASTNAME]
     *
     */
    @JSONField(name = "lastname")
    @JsonProperty("lastname")
    private String lastname;

    /**
     * 属性 [ADDRESS1_TELEPHONE2]
     *
     */
    @JSONField(name = "address1_telephone2")
    @JsonProperty("address1_telephone2")
    private String address1Telephone2;

    /**
     * 属性 [ADDRESS1_STATEORPROVINCE]
     *
     */
    @JSONField(name = "address1_stateorprovince")
    @JsonProperty("address1_stateorprovince")
    private String address1Stateorprovince;

    /**
     * 属性 [ESTIMATEDVALUE]
     *
     */
    @JSONField(name = "estimatedvalue")
    @JsonProperty("estimatedvalue")
    private Double estimatedvalue;

    /**
     * 属性 [LEADID]
     *
     */
    @JSONField(name = "leadid")
    @JsonProperty("leadid")
    private String leadid;

    /**
     * 属性 [ADDRESS1_LONGITUDE]
     *
     */
    @JSONField(name = "address1_longitude")
    @JsonProperty("address1_longitude")
    private Double address1Longitude;

    /**
     * 属性 [ADDRESS1_LINE1]
     *
     */
    @JSONField(name = "address1_line1")
    @JsonProperty("address1_line1")
    private String address1Line1;

    /**
     * 属性 [LEADQUALITYCODE]
     *
     */
    @JSONField(name = "leadqualitycode")
    @JsonProperty("leadqualitycode")
    private String leadqualitycode;

    /**
     * 属性 [DONOTPHONE]
     *
     */
    @JSONField(name = "donotphone")
    @JsonProperty("donotphone")
    private Integer donotphone;

    /**
     * 属性 [EXCHANGERATE]
     *
     */
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;

    /**
     * 属性 [ENTITYIMAGE_URL]
     *
     */
    @JSONField(name = "entityimage_url")
    @JsonProperty("entityimage_url")
    private String entityimageUrl;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;

    /**
     * 属性 [ADDRESS2_STATEORPROVINCE]
     *
     */
    @JSONField(name = "address2_stateorprovince")
    @JsonProperty("address2_stateorprovince")
    private String address2Stateorprovince;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 属性 [NUMBEROFEMPLOYEES]
     *
     */
    @JSONField(name = "numberofemployees")
    @JsonProperty("numberofemployees")
    private Integer numberofemployees;

    /**
     * 属性 [ADDRESS1_CITY]
     *
     */
    @JSONField(name = "address1_city")
    @JsonProperty("address1_city")
    private String address1City;

    /**
     * 属性 [ENTITYIMAGEID]
     *
     */
    @JSONField(name = "entityimageid")
    @JsonProperty("entityimageid")
    private String entityimageid;

    /**
     * 属性 [ADDRESS2_LINE3]
     *
     */
    @JSONField(name = "address2_line3")
    @JsonProperty("address2_line3")
    private String address2Line3;

    /**
     * 属性 [STATUSCODE]
     *
     */
    @JSONField(name = "statuscode")
    @JsonProperty("statuscode")
    private Integer statuscode;

    /**
     * 属性 [ADDRESS2_LINE1]
     *
     */
    @JSONField(name = "address2_line1")
    @JsonProperty("address2_line1")
    private String address2Line1;

    /**
     * 属性 [CUSTOMERID]
     *
     */
    @JSONField(name = "customerid")
    @JsonProperty("customerid")
    private String customerid;

    /**
     * 属性 [COMPANYNAME]
     *
     */
    @JSONField(name = "companyname")
    @JsonProperty("companyname")
    private String companyname;

    /**
     * 属性 [VERSIONNUMBER]
     *
     */
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;

    /**
     * 属性 [QUALIFICATIONCOMMENTS]
     *
     */
    @JSONField(name = "qualificationcomments")
    @JsonProperty("qualificationcomments")
    private String qualificationcomments;

    /**
     * 属性 [ADDRESS2_NAME]
     *
     */
    @JSONField(name = "address2_name")
    @JsonProperty("address2_name")
    private String address2Name;

    /**
     * 属性 [EMAILADDRESS1]
     *
     */
    @JSONField(name = "emailaddress1")
    @JsonProperty("emailaddress1")
    private String emailaddress1;

    /**
     * 属性 [FOLLOWEMAIL]
     *
     */
    @JSONField(name = "followemail")
    @JsonProperty("followemail")
    private Integer followemail;

    /**
     * 属性 [ADDRESS1_COUNTRY]
     *
     */
    @JSONField(name = "address1_country")
    @JsonProperty("address1_country")
    private String address1Country;

    /**
     * 属性 [WEBSITEURL]
     *
     */
    @JSONField(name = "websiteurl")
    @JsonProperty("websiteurl")
    private String websiteurl;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [ADDRESS1_LINE3]
     *
     */
    @JSONField(name = "address1_line3")
    @JsonProperty("address1_line3")
    private String address1Line3;

    /**
     * 属性 [ADDRESS2_ADDRESSID]
     *
     */
    @JSONField(name = "address2_addressid")
    @JsonProperty("address2_addressid")
    private String address2Addressid;

    /**
     * 属性 [ADDRESS1_COMPOSITE]
     *
     */
    @JSONField(name = "address1_composite")
    @JsonProperty("address1_composite")
    private String address1Composite;

    /**
     * 属性 [SUBJECT]
     *
     */
    @JSONField(name = "subject")
    @JsonProperty("subject")
    private String subject;

    /**
     * 属性 [ADDRESS1_UTCOFFSET]
     *
     */
    @JSONField(name = "address1_utcoffset")
    @JsonProperty("address1_utcoffset")
    private Integer address1Utcoffset;

    /**
     * 属性 [BUDGETAMOUNT_BASE]
     *
     */
    @JSONField(name = "budgetamount_base")
    @JsonProperty("budgetamount_base")
    private BigDecimal budgetamountBase;

    /**
     * 属性 [ADDRESS1_ADDRESSTYPECODE]
     *
     */
    @JSONField(name = "address1_addresstypecode")
    @JsonProperty("address1_addresstypecode")
    private String address1Addresstypecode;

    /**
     * 属性 [ADDRESS2_TELEPHONE3]
     *
     */
    @JSONField(name = "address2_telephone3")
    @JsonProperty("address2_telephone3")
    private String address2Telephone3;

    /**
     * 属性 [CUSTOMERNAME]
     *
     */
    @JSONField(name = "customername")
    @JsonProperty("customername")
    private String customername;

    /**
     * 属性 [MASTERLEADNAME]
     *
     */
    @JSONField(name = "masterleadname")
    @JsonProperty("masterleadname")
    private String masterleadname;

    /**
     * 属性 [EVALUATEFIT]
     *
     */
    @JSONField(name = "evaluatefit")
    @JsonProperty("evaluatefit")
    private Integer evaluatefit;

    /**
     * 属性 [ADDRESS1_LATITUDE]
     *
     */
    @JSONField(name = "address1_latitude")
    @JsonProperty("address1_latitude")
    private Double address1Latitude;

    /**
     * 属性 [ADDRESS1_TELEPHONE3]
     *
     */
    @JSONField(name = "address1_telephone3")
    @JsonProperty("address1_telephone3")
    private String address1Telephone3;

    /**
     * 属性 [FULLNAME]
     *
     */
    @JSONField(name = "fullname")
    @JsonProperty("fullname")
    private String fullname;

    /**
     * 属性 [ESTIMATEDAMOUNT_BASE]
     *
     */
    @JSONField(name = "estimatedamount_base")
    @JsonProperty("estimatedamount_base")
    private BigDecimal estimatedamountBase;

    /**
     * 属性 [BUDGETSTATUS]
     *
     */
    @JSONField(name = "budgetstatus")
    @JsonProperty("budgetstatus")
    private String budgetstatus;

    /**
     * 属性 [INDUSTRYCODE]
     *
     */
    @JSONField(name = "industrycode")
    @JsonProperty("industrycode")
    private String industrycode;

    /**
     * 属性 [ADDRESS1_LINE2]
     *
     */
    @JSONField(name = "address1_line2")
    @JsonProperty("address1_line2")
    private String address1Line2;

    /**
     * 属性 [OWNERNAME]
     *
     */
    @JSONField(name = "ownername")
    @JsonProperty("ownername")
    private String ownername;

    /**
     * 属性 [INITIALCOMMUNICATION]
     *
     */
    @JSONField(name = "initialcommunication")
    @JsonProperty("initialcommunication")
    private String initialcommunication;

    /**
     * 属性 [ADDRESS1_POSTOFFICEBOX]
     *
     */
    @JSONField(name = "address1_postofficebox")
    @JsonProperty("address1_postofficebox")
    private String address1Postofficebox;

    /**
     * 属性 [ADDRESS1_TELEPHONE1]
     *
     */
    @JSONField(name = "address1_telephone1")
    @JsonProperty("address1_telephone1")
    private String address1Telephone1;

    /**
     * 属性 [REVENUE]
     *
     */
    @JSONField(name = "revenue")
    @JsonProperty("revenue")
    private BigDecimal revenue;

    /**
     * 属性 [ADDRESS2_COUNTY]
     *
     */
    @JSONField(name = "address2_county")
    @JsonProperty("address2_county")
    private String address2County;

    /**
     * 属性 [STAGEID]
     *
     */
    @JSONField(name = "stageid")
    @JsonProperty("stageid")
    private String stageid;

    /**
     * 属性 [ADDRESS1_SHIPPINGMETHODCODE]
     *
     */
    @JSONField(name = "address1_shippingmethodcode")
    @JsonProperty("address1_shippingmethodcode")
    private String address1Shippingmethodcode;

    /**
     * 属性 [ESTIMATEDAMOUNT]
     *
     */
    @JSONField(name = "estimatedamount")
    @JsonProperty("estimatedamount")
    private BigDecimal estimatedamount;

    /**
     * 属性 [ADDRESS1_COUNTY]
     *
     */
    @JSONField(name = "address1_county")
    @JsonProperty("address1_county")
    private String address1County;

    /**
     * 属性 [UTCCONVERSIONTIMEZONECODE]
     *
     */
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;

    /**
     * 属性 [LASTONHOLDTIME]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "lastonholdtime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("lastonholdtime")
    private Timestamp lastonholdtime;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [TEAMSFOLLOWED]
     *
     */
    @JSONField(name = "teamsfollowed")
    @JsonProperty("teamsfollowed")
    private Integer teamsfollowed;

    /**
     * 属性 [SALUTATION]
     *
     */
    @JSONField(name = "salutation")
    @JsonProperty("salutation")
    private String salutation;

    /**
     * 属性 [ADDRESS2_SHIPPINGMETHODCODE]
     *
     */
    @JSONField(name = "address2_shippingmethodcode")
    @JsonProperty("address2_shippingmethodcode")
    private String address2Shippingmethodcode;

    /**
     * 属性 [ADDRESS2_LATITUDE]
     *
     */
    @JSONField(name = "address2_latitude")
    @JsonProperty("address2_latitude")
    private Double address2Latitude;

    /**
     * 属性 [PARTICIPATESINWORKFLOW]
     *
     */
    @JSONField(name = "participatesinworkflow")
    @JsonProperty("participatesinworkflow")
    private Integer participatesinworkflow;

    /**
     * 属性 [ADDRESS2_COMPOSITE]
     *
     */
    @JSONField(name = "address2_composite")
    @JsonProperty("address2_composite")
    private String address2Composite;

    /**
     * 属性 [SALESSTAGE]
     *
     */
    @JSONField(name = "salesstage")
    @JsonProperty("salesstage")
    private String salesstage;

    /**
     * 属性 [DONOTPOSTALMAIL]
     *
     */
    @JSONField(name = "donotpostalmail")
    @JsonProperty("donotpostalmail")
    private Integer donotpostalmail;

    /**
     * 属性 [REVENUE_BASE]
     *
     */
    @JSONField(name = "revenue_base")
    @JsonProperty("revenue_base")
    private BigDecimal revenueBase;

    /**
     * 属性 [OVERRIDDENCREATEDON]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;

    /**
     * 属性 [SCHEDULEFOLLOWUP_PROSPECT]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "schedulefollowup_prospect" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("schedulefollowup_prospect")
    private Timestamp schedulefollowupProspect;

    /**
     * 属性 [ADDRESS2_CITY]
     *
     */
    @JSONField(name = "address2_city")
    @JsonProperty("address2_city")
    private String address2City;

    /**
     * 属性 [TELEPHONE1]
     *
     */
    @JSONField(name = "telephone1")
    @JsonProperty("telephone1")
    private String telephone1;

    /**
     * 属性 [PRIVATE]
     *
     */
    @JSONField(name = "ibizprivate")
    @JsonProperty("ibizprivate")
    private Integer ibizprivate;

    /**
     * 属性 [MOBILEPHONE]
     *
     */
    @JSONField(name = "mobilephone")
    @JsonProperty("mobilephone")
    private String mobilephone;

    /**
     * 属性 [NEED]
     *
     */
    @JSONField(name = "need")
    @JsonProperty("need")
    private String need;

    /**
     * 属性 [PRIORITYCODE]
     *
     */
    @JSONField(name = "prioritycode")
    @JsonProperty("prioritycode")
    private String prioritycode;

    /**
     * 属性 [ADDRESS1_ADDRESSID]
     *
     */
    @JSONField(name = "address1_addressid")
    @JsonProperty("address1_addressid")
    private String address1Addressid;

    /**
     * 属性 [SALESSTAGECODE]
     *
     */
    @JSONField(name = "salesstagecode")
    @JsonProperty("salesstagecode")
    private String salesstagecode;

    /**
     * 属性 [IMPORTSEQUENCENUMBER]
     *
     */
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;

    /**
     * 属性 [ADDRESS2_TELEPHONE1]
     *
     */
    @JSONField(name = "address2_telephone1")
    @JsonProperty("address2_telephone1")
    private String address2Telephone1;

    /**
     * 属性 [OWNERID]
     *
     */
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;

    /**
     * 属性 [MIDDLENAME]
     *
     */
    @JSONField(name = "middlename")
    @JsonProperty("middlename")
    private String middlename;

    /**
     * 属性 [TELEPHONE2]
     *
     */
    @JSONField(name = "telephone2")
    @JsonProperty("telephone2")
    private String telephone2;

    /**
     * 属性 [PURCHASETIMEFRAME]
     *
     */
    @JSONField(name = "purchasetimeframe")
    @JsonProperty("purchasetimeframe")
    private String purchasetimeframe;

    /**
     * 属性 [LEADSOURCECODE]
     *
     */
    @JSONField(name = "leadsourcecode")
    @JsonProperty("leadsourcecode")
    private String leadsourcecode;

    /**
     * 属性 [ENTITYIMAGE_TIMESTAMP]
     *
     */
    @JSONField(name = "entityimage_timestamp")
    @JsonProperty("entityimage_timestamp")
    private BigInteger entityimageTimestamp;

    /**
     * 属性 [PREFERREDCONTACTMETHODCODE]
     *
     */
    @JSONField(name = "preferredcontactmethodcode")
    @JsonProperty("preferredcontactmethodcode")
    private String preferredcontactmethodcode;

    /**
     * 属性 [ENTITYIMAGE]
     *
     */
    @JSONField(name = "entityimage")
    @JsonProperty("entityimage")
    private String entityimage;

    /**
     * 属性 [ADDRESS2_TELEPHONE2]
     *
     */
    @JSONField(name = "address2_telephone2")
    @JsonProperty("address2_telephone2")
    private String address2Telephone2;

    /**
     * 属性 [DONOTSENDMM]
     *
     */
    @JSONField(name = "donotsendmm")
    @JsonProperty("donotsendmm")
    private Integer donotsendmm;

    /**
     * 属性 [PURCHASEPROCESS]
     *
     */
    @JSONField(name = "purchaseprocess")
    @JsonProperty("purchaseprocess")
    private String purchaseprocess;

    /**
     * 属性 [DONOTBULKEMAIL]
     *
     */
    @JSONField(name = "donotbulkemail")
    @JsonProperty("donotbulkemail")
    private Integer donotbulkemail;

    /**
     * 属性 [SIC]
     *
     */
    @JSONField(name = "sic")
    @JsonProperty("sic")
    private String sic;

    /**
     * 属性 [CONTACTNAME]
     *
     */
    @JSONField(name = "contactname")
    @JsonProperty("contactname")
    private String contactname;

    /**
     * 属性 [DONOTEMAIL]
     *
     */
    @JSONField(name = "donotemail")
    @JsonProperty("donotemail")
    private Integer donotemail;

    /**
     * 属性 [ADDRESS2_LONGITUDE]
     *
     */
    @JSONField(name = "address2_longitude")
    @JsonProperty("address2_longitude")
    private Double address2Longitude;

    /**
     * 属性 [CONFIRMINTEREST]
     *
     */
    @JSONField(name = "confirminterest")
    @JsonProperty("confirminterest")
    private Integer confirminterest;

    /**
     * 属性 [OWNERTYPE]
     *
     */
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;

    /**
     * 属性 [ADDRESS2_POSTOFFICEBOX]
     *
     */
    @JSONField(name = "address2_postofficebox")
    @JsonProperty("address2_postofficebox")
    private String address2Postofficebox;

    /**
     * 属性 [STATECODE]
     *
     */
    @JSONField(name = "statecode")
    @JsonProperty("statecode")
    private Integer statecode;

    /**
     * 属性 [AUTOCREATE]
     *
     */
    @JSONField(name = "autocreate")
    @JsonProperty("autocreate")
    private Integer autocreate;

    /**
     * 属性 [ADDRESS1_NAME]
     *
     */
    @JSONField(name = "address1_name")
    @JsonProperty("address1_name")
    private String address1Name;

    /**
     * 属性 [TIMEZONERULEVERSIONNUMBER]
     *
     */
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;

    /**
     * 属性 [LASTUSEDINCAMPAIGN]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "lastusedincampaign" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("lastusedincampaign")
    private Timestamp lastusedincampaign;

    /**
     * 属性 [ESTIMATEDCLOSEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "estimatedclosedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("estimatedclosedate")
    private Timestamp estimatedclosedate;

    /**
     * 属性 [ADDRESS2_LINE2]
     *
     */
    @JSONField(name = "address2_line2")
    @JsonProperty("address2_line2")
    private String address2Line2;

    /**
     * 属性 [EMAILADDRESS3]
     *
     */
    @JSONField(name = "emailaddress3")
    @JsonProperty("emailaddress3")
    private String emailaddress3;

    /**
     * 属性 [PAGER]
     *
     */
    @JSONField(name = "pager")
    @JsonProperty("pager")
    private String pager;

    /**
     * 属性 [ADDRESS2_UPSZONE]
     *
     */
    @JSONField(name = "address2_upszone")
    @JsonProperty("address2_upszone")
    private String address2Upszone;

    /**
     * 属性 [TRAVERSEDPATH]
     *
     */
    @JSONField(name = "traversedpath")
    @JsonProperty("traversedpath")
    private String traversedpath;

    /**
     * 属性 [FAX]
     *
     */
    @JSONField(name = "fax")
    @JsonProperty("fax")
    private String fax;

    /**
     * 属性 [SCHEDULEFOLLOWUP_QUALIFY]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "schedulefollowup_qualify" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("schedulefollowup_qualify")
    private Timestamp schedulefollowupQualify;

    /**
     * 属性 [TELEPHONE3]
     *
     */
    @JSONField(name = "telephone3")
    @JsonProperty("telephone3")
    private String telephone3;

    /**
     * 属性 [PROCESSID]
     *
     */
    @JSONField(name = "processid")
    @JsonProperty("processid")
    private String processid;

    /**
     * 属性 [EMAILADDRESS2]
     *
     */
    @JSONField(name = "emailaddress2")
    @JsonProperty("emailaddress2")
    private String emailaddress2;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;

    /**
     * 属性 [DECISIONMAKER]
     *
     */
    @JSONField(name = "decisionmaker")
    @JsonProperty("decisionmaker")
    private Integer decisionmaker;

    /**
     * 属性 [ADDRESS1_POSTALCODE]
     *
     */
    @JSONField(name = "address1_postalcode")
    @JsonProperty("address1_postalcode")
    private String address1Postalcode;

    /**
     * 属性 [ADDRESS2_POSTALCODE]
     *
     */
    @JSONField(name = "address2_postalcode")
    @JsonProperty("address2_postalcode")
    private String address2Postalcode;

    /**
     * 属性 [DONOTFAX]
     *
     */
    @JSONField(name = "donotfax")
    @JsonProperty("donotfax")
    private Integer donotfax;

    /**
     * 属性 [CUSTOMERTYPE]
     *
     */
    @JSONField(name = "customertype")
    @JsonProperty("customertype")
    private String customertype;

    /**
     * 属性 [FIRSTNAME]
     *
     */
    @JSONField(name = "firstname")
    @JsonProperty("firstname")
    private String firstname;

    /**
     * 属性 [MERGED]
     *
     */
    @JSONField(name = "merged")
    @JsonProperty("merged")
    private Integer merged;

    /**
     * 属性 [ADDRESS2_ADDRESSTYPECODE]
     *
     */
    @JSONField(name = "address2_addresstypecode")
    @JsonProperty("address2_addresstypecode")
    private String address2Addresstypecode;

    /**
     * 属性 [ADDRESS1_UPSZONE]
     *
     */
    @JSONField(name = "address1_upszone")
    @JsonProperty("address1_upszone")
    private String address1Upszone;

    /**
     * 属性 [SLANAME]
     *
     */
    @JSONField(name = "slaname")
    @JsonProperty("slaname")
    private String slaname;

    /**
     * 属性 [RELATEDOBJECTNAME]
     *
     */
    @JSONField(name = "relatedobjectname")
    @JsonProperty("relatedobjectname")
    private String relatedobjectname;

    /**
     * 属性 [CURRENCYNAME]
     *
     */
    @JSONField(name = "currencyname")
    @JsonProperty("currencyname")
    private String currencyname;

    /**
     * 属性 [ORIGINATINGCASENAME]
     *
     */
    @JSONField(name = "originatingcasename")
    @JsonProperty("originatingcasename")
    private String originatingcasename;

    /**
     * 属性 [CAMPAIGNNAME]
     *
     */
    @JSONField(name = "campaignname")
    @JsonProperty("campaignname")
    private String campaignname;

    /**
     * 属性 [PARENTACCOUNTNAME]
     *
     */
    @JSONField(name = "parentaccountname")
    @JsonProperty("parentaccountname")
    private String parentaccountname;

    /**
     * 属性 [PARENTCONTACTNAME]
     *
     */
    @JSONField(name = "parentcontactname")
    @JsonProperty("parentcontactname")
    private String parentcontactname;

    /**
     * 属性 [QUALIFYINGOPPORTUNITYNAME]
     *
     */
    @JSONField(name = "qualifyingopportunityname")
    @JsonProperty("qualifyingopportunityname")
    private String qualifyingopportunityname;

    /**
     * 属性 [QUALIFYINGOPPORTUNITYID]
     *
     */
    @JSONField(name = "qualifyingopportunityid")
    @JsonProperty("qualifyingopportunityid")
    private String qualifyingopportunityid;

    /**
     * 属性 [SLAID]
     *
     */
    @JSONField(name = "slaid")
    @JsonProperty("slaid")
    private String slaid;

    /**
     * 属性 [CAMPAIGNID]
     *
     */
    @JSONField(name = "campaignid")
    @JsonProperty("campaignid")
    private String campaignid;

    /**
     * 属性 [RELATEDOBJECTID]
     *
     */
    @JSONField(name = "relatedobjectid")
    @JsonProperty("relatedobjectid")
    private String relatedobjectid;

    /**
     * 属性 [TRANSACTIONCURRENCYID]
     *
     */
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;

    /**
     * 属性 [PARENTACCOUNTID]
     *
     */
    @JSONField(name = "parentaccountid")
    @JsonProperty("parentaccountid")
    private String parentaccountid;

    /**
     * 属性 [ORIGINATINGCASEID]
     *
     */
    @JSONField(name = "originatingcaseid")
    @JsonProperty("originatingcaseid")
    private String originatingcaseid;

    /**
     * 属性 [PARENTCONTACTID]
     *
     */
    @JSONField(name = "parentcontactid")
    @JsonProperty("parentcontactid")
    private String parentcontactid;


    /**
     * 设置 [ADDRESS1_FAX]
     */
    public void setAddress1Fax(String  address1Fax){
        this.address1Fax = address1Fax ;
        this.modify("address1_fax",address1Fax);
    }

    /**
     * 设置 [ADDRESS2_UTCOFFSET]
     */
    public void setAddress2Utcoffset(Integer  address2Utcoffset){
        this.address2Utcoffset = address2Utcoffset ;
        this.modify("address2_utcoffset",address2Utcoffset);
    }

    /**
     * 设置 [JOBTITLE]
     */
    public void setJobtitle(String  jobtitle){
        this.jobtitle = jobtitle ;
        this.modify("jobtitle",jobtitle);
    }

    /**
     * 设置 [ADDRESS2_COUNTRY]
     */
    public void setAddress2Country(String  address2Country){
        this.address2Country = address2Country ;
        this.modify("address2_country",address2Country);
    }

    /**
     * 设置 [BUDGETAMOUNT]
     */
    public void setBudgetamount(BigDecimal  budgetamount){
        this.budgetamount = budgetamount ;
        this.modify("budgetamount",budgetamount);
    }

    /**
     * 设置 [ADDRESS2_FAX]
     */
    public void setAddress2Fax(String  address2Fax){
        this.address2Fax = address2Fax ;
        this.modify("address2_fax",address2Fax);
    }

    /**
     * 设置 [ONHOLDTIME]
     */
    public void setOnholdtime(Integer  onholdtime){
        this.onholdtime = onholdtime ;
        this.modify("onholdtime",onholdtime);
    }

    /**
     * 设置 [LASTNAME]
     */
    public void setLastname(String  lastname){
        this.lastname = lastname ;
        this.modify("lastname",lastname);
    }

    /**
     * 设置 [ADDRESS1_TELEPHONE2]
     */
    public void setAddress1Telephone2(String  address1Telephone2){
        this.address1Telephone2 = address1Telephone2 ;
        this.modify("address1_telephone2",address1Telephone2);
    }

    /**
     * 设置 [ADDRESS1_STATEORPROVINCE]
     */
    public void setAddress1Stateorprovince(String  address1Stateorprovince){
        this.address1Stateorprovince = address1Stateorprovince ;
        this.modify("address1_stateorprovince",address1Stateorprovince);
    }

    /**
     * 设置 [ESTIMATEDVALUE]
     */
    public void setEstimatedvalue(Double  estimatedvalue){
        this.estimatedvalue = estimatedvalue ;
        this.modify("estimatedvalue",estimatedvalue);
    }

    /**
     * 设置 [ADDRESS1_LONGITUDE]
     */
    public void setAddress1Longitude(Double  address1Longitude){
        this.address1Longitude = address1Longitude ;
        this.modify("address1_longitude",address1Longitude);
    }

    /**
     * 设置 [ADDRESS1_LINE1]
     */
    public void setAddress1Line1(String  address1Line1){
        this.address1Line1 = address1Line1 ;
        this.modify("address1_line1",address1Line1);
    }

    /**
     * 设置 [LEADQUALITYCODE]
     */
    public void setLeadqualitycode(String  leadqualitycode){
        this.leadqualitycode = leadqualitycode ;
        this.modify("leadqualitycode",leadqualitycode);
    }

    /**
     * 设置 [DONOTPHONE]
     */
    public void setDonotphone(Integer  donotphone){
        this.donotphone = donotphone ;
        this.modify("donotphone",donotphone);
    }

    /**
     * 设置 [EXCHANGERATE]
     */
    public void setExchangerate(BigDecimal  exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [ENTITYIMAGE_URL]
     */
    public void setEntityimageUrl(String  entityimageUrl){
        this.entityimageUrl = entityimageUrl ;
        this.modify("entityimage_url",entityimageUrl);
    }

    /**
     * 设置 [ADDRESS2_STATEORPROVINCE]
     */
    public void setAddress2Stateorprovince(String  address2Stateorprovince){
        this.address2Stateorprovince = address2Stateorprovince ;
        this.modify("address2_stateorprovince",address2Stateorprovince);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [NUMBEROFEMPLOYEES]
     */
    public void setNumberofemployees(Integer  numberofemployees){
        this.numberofemployees = numberofemployees ;
        this.modify("numberofemployees",numberofemployees);
    }

    /**
     * 设置 [ADDRESS1_CITY]
     */
    public void setAddress1City(String  address1City){
        this.address1City = address1City ;
        this.modify("address1_city",address1City);
    }

    /**
     * 设置 [ENTITYIMAGEID]
     */
    public void setEntityimageid(String  entityimageid){
        this.entityimageid = entityimageid ;
        this.modify("entityimageid",entityimageid);
    }

    /**
     * 设置 [ADDRESS2_LINE3]
     */
    public void setAddress2Line3(String  address2Line3){
        this.address2Line3 = address2Line3 ;
        this.modify("address2_line3",address2Line3);
    }

    /**
     * 设置 [STATUSCODE]
     */
    public void setStatuscode(Integer  statuscode){
        this.statuscode = statuscode ;
        this.modify("statuscode",statuscode);
    }

    /**
     * 设置 [ADDRESS2_LINE1]
     */
    public void setAddress2Line1(String  address2Line1){
        this.address2Line1 = address2Line1 ;
        this.modify("address2_line1",address2Line1);
    }

    /**
     * 设置 [CUSTOMERID]
     */
    public void setCustomerid(String  customerid){
        this.customerid = customerid ;
        this.modify("customerid",customerid);
    }

    /**
     * 设置 [COMPANYNAME]
     */
    public void setCompanyname(String  companyname){
        this.companyname = companyname ;
        this.modify("companyname",companyname);
    }

    /**
     * 设置 [VERSIONNUMBER]
     */
    public void setVersionnumber(BigInteger  versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [QUALIFICATIONCOMMENTS]
     */
    public void setQualificationcomments(String  qualificationcomments){
        this.qualificationcomments = qualificationcomments ;
        this.modify("qualificationcomments",qualificationcomments);
    }

    /**
     * 设置 [ADDRESS2_NAME]
     */
    public void setAddress2Name(String  address2Name){
        this.address2Name = address2Name ;
        this.modify("address2_name",address2Name);
    }

    /**
     * 设置 [EMAILADDRESS1]
     */
    public void setEmailaddress1(String  emailaddress1){
        this.emailaddress1 = emailaddress1 ;
        this.modify("emailaddress1",emailaddress1);
    }

    /**
     * 设置 [FOLLOWEMAIL]
     */
    public void setFollowemail(Integer  followemail){
        this.followemail = followemail ;
        this.modify("followemail",followemail);
    }

    /**
     * 设置 [ADDRESS1_COUNTRY]
     */
    public void setAddress1Country(String  address1Country){
        this.address1Country = address1Country ;
        this.modify("address1_country",address1Country);
    }

    /**
     * 设置 [WEBSITEURL]
     */
    public void setWebsiteurl(String  websiteurl){
        this.websiteurl = websiteurl ;
        this.modify("websiteurl",websiteurl);
    }

    /**
     * 设置 [ADDRESS1_LINE3]
     */
    public void setAddress1Line3(String  address1Line3){
        this.address1Line3 = address1Line3 ;
        this.modify("address1_line3",address1Line3);
    }

    /**
     * 设置 [ADDRESS2_ADDRESSID]
     */
    public void setAddress2Addressid(String  address2Addressid){
        this.address2Addressid = address2Addressid ;
        this.modify("address2_addressid",address2Addressid);
    }

    /**
     * 设置 [ADDRESS1_COMPOSITE]
     */
    public void setAddress1Composite(String  address1Composite){
        this.address1Composite = address1Composite ;
        this.modify("address1_composite",address1Composite);
    }

    /**
     * 设置 [SUBJECT]
     */
    public void setSubject(String  subject){
        this.subject = subject ;
        this.modify("subject",subject);
    }

    /**
     * 设置 [ADDRESS1_UTCOFFSET]
     */
    public void setAddress1Utcoffset(Integer  address1Utcoffset){
        this.address1Utcoffset = address1Utcoffset ;
        this.modify("address1_utcoffset",address1Utcoffset);
    }

    /**
     * 设置 [BUDGETAMOUNT_BASE]
     */
    public void setBudgetamountBase(BigDecimal  budgetamountBase){
        this.budgetamountBase = budgetamountBase ;
        this.modify("budgetamount_base",budgetamountBase);
    }

    /**
     * 设置 [ADDRESS1_ADDRESSTYPECODE]
     */
    public void setAddress1Addresstypecode(String  address1Addresstypecode){
        this.address1Addresstypecode = address1Addresstypecode ;
        this.modify("address1_addresstypecode",address1Addresstypecode);
    }

    /**
     * 设置 [ADDRESS2_TELEPHONE3]
     */
    public void setAddress2Telephone3(String  address2Telephone3){
        this.address2Telephone3 = address2Telephone3 ;
        this.modify("address2_telephone3",address2Telephone3);
    }

    /**
     * 设置 [CUSTOMERNAME]
     */
    public void setCustomername(String  customername){
        this.customername = customername ;
        this.modify("customername",customername);
    }

    /**
     * 设置 [MASTERLEADNAME]
     */
    public void setMasterleadname(String  masterleadname){
        this.masterleadname = masterleadname ;
        this.modify("masterleadname",masterleadname);
    }

    /**
     * 设置 [EVALUATEFIT]
     */
    public void setEvaluatefit(Integer  evaluatefit){
        this.evaluatefit = evaluatefit ;
        this.modify("evaluatefit",evaluatefit);
    }

    /**
     * 设置 [ADDRESS1_LATITUDE]
     */
    public void setAddress1Latitude(Double  address1Latitude){
        this.address1Latitude = address1Latitude ;
        this.modify("address1_latitude",address1Latitude);
    }

    /**
     * 设置 [ADDRESS1_TELEPHONE3]
     */
    public void setAddress1Telephone3(String  address1Telephone3){
        this.address1Telephone3 = address1Telephone3 ;
        this.modify("address1_telephone3",address1Telephone3);
    }

    /**
     * 设置 [FULLNAME]
     */
    public void setFullname(String  fullname){
        this.fullname = fullname ;
        this.modify("fullname",fullname);
    }

    /**
     * 设置 [ESTIMATEDAMOUNT_BASE]
     */
    public void setEstimatedamountBase(BigDecimal  estimatedamountBase){
        this.estimatedamountBase = estimatedamountBase ;
        this.modify("estimatedamount_base",estimatedamountBase);
    }

    /**
     * 设置 [BUDGETSTATUS]
     */
    public void setBudgetstatus(String  budgetstatus){
        this.budgetstatus = budgetstatus ;
        this.modify("budgetstatus",budgetstatus);
    }

    /**
     * 设置 [INDUSTRYCODE]
     */
    public void setIndustrycode(String  industrycode){
        this.industrycode = industrycode ;
        this.modify("industrycode",industrycode);
    }

    /**
     * 设置 [ADDRESS1_LINE2]
     */
    public void setAddress1Line2(String  address1Line2){
        this.address1Line2 = address1Line2 ;
        this.modify("address1_line2",address1Line2);
    }

    /**
     * 设置 [OWNERNAME]
     */
    public void setOwnername(String  ownername){
        this.ownername = ownername ;
        this.modify("ownername",ownername);
    }

    /**
     * 设置 [INITIALCOMMUNICATION]
     */
    public void setInitialcommunication(String  initialcommunication){
        this.initialcommunication = initialcommunication ;
        this.modify("initialcommunication",initialcommunication);
    }

    /**
     * 设置 [ADDRESS1_POSTOFFICEBOX]
     */
    public void setAddress1Postofficebox(String  address1Postofficebox){
        this.address1Postofficebox = address1Postofficebox ;
        this.modify("address1_postofficebox",address1Postofficebox);
    }

    /**
     * 设置 [ADDRESS1_TELEPHONE1]
     */
    public void setAddress1Telephone1(String  address1Telephone1){
        this.address1Telephone1 = address1Telephone1 ;
        this.modify("address1_telephone1",address1Telephone1);
    }

    /**
     * 设置 [REVENUE]
     */
    public void setRevenue(BigDecimal  revenue){
        this.revenue = revenue ;
        this.modify("revenue",revenue);
    }

    /**
     * 设置 [ADDRESS2_COUNTY]
     */
    public void setAddress2County(String  address2County){
        this.address2County = address2County ;
        this.modify("address2_county",address2County);
    }

    /**
     * 设置 [STAGEID]
     */
    public void setStageid(String  stageid){
        this.stageid = stageid ;
        this.modify("stageid",stageid);
    }

    /**
     * 设置 [ADDRESS1_SHIPPINGMETHODCODE]
     */
    public void setAddress1Shippingmethodcode(String  address1Shippingmethodcode){
        this.address1Shippingmethodcode = address1Shippingmethodcode ;
        this.modify("address1_shippingmethodcode",address1Shippingmethodcode);
    }

    /**
     * 设置 [ESTIMATEDAMOUNT]
     */
    public void setEstimatedamount(BigDecimal  estimatedamount){
        this.estimatedamount = estimatedamount ;
        this.modify("estimatedamount",estimatedamount);
    }

    /**
     * 设置 [ADDRESS1_COUNTY]
     */
    public void setAddress1County(String  address1County){
        this.address1County = address1County ;
        this.modify("address1_county",address1County);
    }

    /**
     * 设置 [UTCCONVERSIONTIMEZONECODE]
     */
    public void setUtcconversiontimezonecode(Integer  utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [LASTONHOLDTIME]
     */
    public void setLastonholdtime(Timestamp  lastonholdtime){
        this.lastonholdtime = lastonholdtime ;
        this.modify("lastonholdtime",lastonholdtime);
    }

    /**
     * 设置 [TEAMSFOLLOWED]
     */
    public void setTeamsfollowed(Integer  teamsfollowed){
        this.teamsfollowed = teamsfollowed ;
        this.modify("teamsfollowed",teamsfollowed);
    }

    /**
     * 设置 [SALUTATION]
     */
    public void setSalutation(String  salutation){
        this.salutation = salutation ;
        this.modify("salutation",salutation);
    }

    /**
     * 设置 [ADDRESS2_SHIPPINGMETHODCODE]
     */
    public void setAddress2Shippingmethodcode(String  address2Shippingmethodcode){
        this.address2Shippingmethodcode = address2Shippingmethodcode ;
        this.modify("address2_shippingmethodcode",address2Shippingmethodcode);
    }

    /**
     * 设置 [ADDRESS2_LATITUDE]
     */
    public void setAddress2Latitude(Double  address2Latitude){
        this.address2Latitude = address2Latitude ;
        this.modify("address2_latitude",address2Latitude);
    }

    /**
     * 设置 [PARTICIPATESINWORKFLOW]
     */
    public void setParticipatesinworkflow(Integer  participatesinworkflow){
        this.participatesinworkflow = participatesinworkflow ;
        this.modify("participatesinworkflow",participatesinworkflow);
    }

    /**
     * 设置 [ADDRESS2_COMPOSITE]
     */
    public void setAddress2Composite(String  address2Composite){
        this.address2Composite = address2Composite ;
        this.modify("address2_composite",address2Composite);
    }

    /**
     * 设置 [SALESSTAGE]
     */
    public void setSalesstage(String  salesstage){
        this.salesstage = salesstage ;
        this.modify("salesstage",salesstage);
    }

    /**
     * 设置 [DONOTPOSTALMAIL]
     */
    public void setDonotpostalmail(Integer  donotpostalmail){
        this.donotpostalmail = donotpostalmail ;
        this.modify("donotpostalmail",donotpostalmail);
    }

    /**
     * 设置 [REVENUE_BASE]
     */
    public void setRevenueBase(BigDecimal  revenueBase){
        this.revenueBase = revenueBase ;
        this.modify("revenue_base",revenueBase);
    }

    /**
     * 设置 [OVERRIDDENCREATEDON]
     */
    public void setOverriddencreatedon(Timestamp  overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 设置 [SCHEDULEFOLLOWUP_PROSPECT]
     */
    public void setSchedulefollowupProspect(Timestamp  schedulefollowupProspect){
        this.schedulefollowupProspect = schedulefollowupProspect ;
        this.modify("schedulefollowup_prospect",schedulefollowupProspect);
    }

    /**
     * 设置 [ADDRESS2_CITY]
     */
    public void setAddress2City(String  address2City){
        this.address2City = address2City ;
        this.modify("address2_city",address2City);
    }

    /**
     * 设置 [TELEPHONE1]
     */
    public void setTelephone1(String  telephone1){
        this.telephone1 = telephone1 ;
        this.modify("telephone1",telephone1);
    }

    /**
     * 设置 [PRIVATE]
     */
    public void setIbizprivate(Integer  ibizprivate){
        this.ibizprivate = ibizprivate ;
        this.modify("private",ibizprivate);
    }

    /**
     * 设置 [MOBILEPHONE]
     */
    public void setMobilephone(String  mobilephone){
        this.mobilephone = mobilephone ;
        this.modify("mobilephone",mobilephone);
    }

    /**
     * 设置 [NEED]
     */
    public void setNeed(String  need){
        this.need = need ;
        this.modify("need",need);
    }

    /**
     * 设置 [PRIORITYCODE]
     */
    public void setPrioritycode(String  prioritycode){
        this.prioritycode = prioritycode ;
        this.modify("prioritycode",prioritycode);
    }

    /**
     * 设置 [ADDRESS1_ADDRESSID]
     */
    public void setAddress1Addressid(String  address1Addressid){
        this.address1Addressid = address1Addressid ;
        this.modify("address1_addressid",address1Addressid);
    }

    /**
     * 设置 [SALESSTAGECODE]
     */
    public void setSalesstagecode(String  salesstagecode){
        this.salesstagecode = salesstagecode ;
        this.modify("salesstagecode",salesstagecode);
    }

    /**
     * 设置 [IMPORTSEQUENCENUMBER]
     */
    public void setImportsequencenumber(Integer  importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [ADDRESS2_TELEPHONE1]
     */
    public void setAddress2Telephone1(String  address2Telephone1){
        this.address2Telephone1 = address2Telephone1 ;
        this.modify("address2_telephone1",address2Telephone1);
    }

    /**
     * 设置 [OWNERID]
     */
    public void setOwnerid(String  ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [MIDDLENAME]
     */
    public void setMiddlename(String  middlename){
        this.middlename = middlename ;
        this.modify("middlename",middlename);
    }

    /**
     * 设置 [TELEPHONE2]
     */
    public void setTelephone2(String  telephone2){
        this.telephone2 = telephone2 ;
        this.modify("telephone2",telephone2);
    }

    /**
     * 设置 [PURCHASETIMEFRAME]
     */
    public void setPurchasetimeframe(String  purchasetimeframe){
        this.purchasetimeframe = purchasetimeframe ;
        this.modify("purchasetimeframe",purchasetimeframe);
    }

    /**
     * 设置 [LEADSOURCECODE]
     */
    public void setLeadsourcecode(String  leadsourcecode){
        this.leadsourcecode = leadsourcecode ;
        this.modify("leadsourcecode",leadsourcecode);
    }

    /**
     * 设置 [ENTITYIMAGE_TIMESTAMP]
     */
    public void setEntityimageTimestamp(BigInteger  entityimageTimestamp){
        this.entityimageTimestamp = entityimageTimestamp ;
        this.modify("entityimage_timestamp",entityimageTimestamp);
    }

    /**
     * 设置 [PREFERREDCONTACTMETHODCODE]
     */
    public void setPreferredcontactmethodcode(String  preferredcontactmethodcode){
        this.preferredcontactmethodcode = preferredcontactmethodcode ;
        this.modify("preferredcontactmethodcode",preferredcontactmethodcode);
    }

    /**
     * 设置 [ENTITYIMAGE]
     */
    public void setEntityimage(String  entityimage){
        this.entityimage = entityimage ;
        this.modify("entityimage",entityimage);
    }

    /**
     * 设置 [ADDRESS2_TELEPHONE2]
     */
    public void setAddress2Telephone2(String  address2Telephone2){
        this.address2Telephone2 = address2Telephone2 ;
        this.modify("address2_telephone2",address2Telephone2);
    }

    /**
     * 设置 [DONOTSENDMM]
     */
    public void setDonotsendmm(Integer  donotsendmm){
        this.donotsendmm = donotsendmm ;
        this.modify("donotsendmm",donotsendmm);
    }

    /**
     * 设置 [PURCHASEPROCESS]
     */
    public void setPurchaseprocess(String  purchaseprocess){
        this.purchaseprocess = purchaseprocess ;
        this.modify("purchaseprocess",purchaseprocess);
    }

    /**
     * 设置 [DONOTBULKEMAIL]
     */
    public void setDonotbulkemail(Integer  donotbulkemail){
        this.donotbulkemail = donotbulkemail ;
        this.modify("donotbulkemail",donotbulkemail);
    }

    /**
     * 设置 [SIC]
     */
    public void setSic(String  sic){
        this.sic = sic ;
        this.modify("sic",sic);
    }

    /**
     * 设置 [CONTACTNAME]
     */
    public void setContactname(String  contactname){
        this.contactname = contactname ;
        this.modify("contactname",contactname);
    }

    /**
     * 设置 [DONOTEMAIL]
     */
    public void setDonotemail(Integer  donotemail){
        this.donotemail = donotemail ;
        this.modify("donotemail",donotemail);
    }

    /**
     * 设置 [ADDRESS2_LONGITUDE]
     */
    public void setAddress2Longitude(Double  address2Longitude){
        this.address2Longitude = address2Longitude ;
        this.modify("address2_longitude",address2Longitude);
    }

    /**
     * 设置 [CONFIRMINTEREST]
     */
    public void setConfirminterest(Integer  confirminterest){
        this.confirminterest = confirminterest ;
        this.modify("confirminterest",confirminterest);
    }

    /**
     * 设置 [OWNERTYPE]
     */
    public void setOwnertype(String  ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [ADDRESS2_POSTOFFICEBOX]
     */
    public void setAddress2Postofficebox(String  address2Postofficebox){
        this.address2Postofficebox = address2Postofficebox ;
        this.modify("address2_postofficebox",address2Postofficebox);
    }

    /**
     * 设置 [STATECODE]
     */
    public void setStatecode(Integer  statecode){
        this.statecode = statecode ;
        this.modify("statecode",statecode);
    }

    /**
     * 设置 [AUTOCREATE]
     */
    public void setAutocreate(Integer  autocreate){
        this.autocreate = autocreate ;
        this.modify("autocreate",autocreate);
    }

    /**
     * 设置 [ADDRESS1_NAME]
     */
    public void setAddress1Name(String  address1Name){
        this.address1Name = address1Name ;
        this.modify("address1_name",address1Name);
    }

    /**
     * 设置 [TIMEZONERULEVERSIONNUMBER]
     */
    public void setTimezoneruleversionnumber(Integer  timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [LASTUSEDINCAMPAIGN]
     */
    public void setLastusedincampaign(Timestamp  lastusedincampaign){
        this.lastusedincampaign = lastusedincampaign ;
        this.modify("lastusedincampaign",lastusedincampaign);
    }

    /**
     * 设置 [ESTIMATEDCLOSEDATE]
     */
    public void setEstimatedclosedate(Timestamp  estimatedclosedate){
        this.estimatedclosedate = estimatedclosedate ;
        this.modify("estimatedclosedate",estimatedclosedate);
    }

    /**
     * 设置 [ADDRESS2_LINE2]
     */
    public void setAddress2Line2(String  address2Line2){
        this.address2Line2 = address2Line2 ;
        this.modify("address2_line2",address2Line2);
    }

    /**
     * 设置 [EMAILADDRESS3]
     */
    public void setEmailaddress3(String  emailaddress3){
        this.emailaddress3 = emailaddress3 ;
        this.modify("emailaddress3",emailaddress3);
    }

    /**
     * 设置 [PAGER]
     */
    public void setPager(String  pager){
        this.pager = pager ;
        this.modify("pager",pager);
    }

    /**
     * 设置 [ADDRESS2_UPSZONE]
     */
    public void setAddress2Upszone(String  address2Upszone){
        this.address2Upszone = address2Upszone ;
        this.modify("address2_upszone",address2Upszone);
    }

    /**
     * 设置 [TRAVERSEDPATH]
     */
    public void setTraversedpath(String  traversedpath){
        this.traversedpath = traversedpath ;
        this.modify("traversedpath",traversedpath);
    }

    /**
     * 设置 [FAX]
     */
    public void setFax(String  fax){
        this.fax = fax ;
        this.modify("fax",fax);
    }

    /**
     * 设置 [SCHEDULEFOLLOWUP_QUALIFY]
     */
    public void setSchedulefollowupQualify(Timestamp  schedulefollowupQualify){
        this.schedulefollowupQualify = schedulefollowupQualify ;
        this.modify("schedulefollowup_qualify",schedulefollowupQualify);
    }

    /**
     * 设置 [TELEPHONE3]
     */
    public void setTelephone3(String  telephone3){
        this.telephone3 = telephone3 ;
        this.modify("telephone3",telephone3);
    }

    /**
     * 设置 [PROCESSID]
     */
    public void setProcessid(String  processid){
        this.processid = processid ;
        this.modify("processid",processid);
    }

    /**
     * 设置 [EMAILADDRESS2]
     */
    public void setEmailaddress2(String  emailaddress2){
        this.emailaddress2 = emailaddress2 ;
        this.modify("emailaddress2",emailaddress2);
    }

    /**
     * 设置 [DECISIONMAKER]
     */
    public void setDecisionmaker(Integer  decisionmaker){
        this.decisionmaker = decisionmaker ;
        this.modify("decisionmaker",decisionmaker);
    }

    /**
     * 设置 [ADDRESS1_POSTALCODE]
     */
    public void setAddress1Postalcode(String  address1Postalcode){
        this.address1Postalcode = address1Postalcode ;
        this.modify("address1_postalcode",address1Postalcode);
    }

    /**
     * 设置 [ADDRESS2_POSTALCODE]
     */
    public void setAddress2Postalcode(String  address2Postalcode){
        this.address2Postalcode = address2Postalcode ;
        this.modify("address2_postalcode",address2Postalcode);
    }

    /**
     * 设置 [DONOTFAX]
     */
    public void setDonotfax(Integer  donotfax){
        this.donotfax = donotfax ;
        this.modify("donotfax",donotfax);
    }

    /**
     * 设置 [CUSTOMERTYPE]
     */
    public void setCustomertype(String  customertype){
        this.customertype = customertype ;
        this.modify("customertype",customertype);
    }

    /**
     * 设置 [FIRSTNAME]
     */
    public void setFirstname(String  firstname){
        this.firstname = firstname ;
        this.modify("firstname",firstname);
    }

    /**
     * 设置 [MERGED]
     */
    public void setMerged(Integer  merged){
        this.merged = merged ;
        this.modify("merged",merged);
    }

    /**
     * 设置 [ADDRESS2_ADDRESSTYPECODE]
     */
    public void setAddress2Addresstypecode(String  address2Addresstypecode){
        this.address2Addresstypecode = address2Addresstypecode ;
        this.modify("address2_addresstypecode",address2Addresstypecode);
    }

    /**
     * 设置 [ADDRESS1_UPSZONE]
     */
    public void setAddress1Upszone(String  address1Upszone){
        this.address1Upszone = address1Upszone ;
        this.modify("address1_upszone",address1Upszone);
    }

    /**
     * 设置 [SLANAME]
     */
    public void setSlaname(String  slaname){
        this.slaname = slaname ;
        this.modify("slaname",slaname);
    }

    /**
     * 设置 [RELATEDOBJECTNAME]
     */
    public void setRelatedobjectname(String  relatedobjectname){
        this.relatedobjectname = relatedobjectname ;
        this.modify("relatedobjectname",relatedobjectname);
    }

    /**
     * 设置 [CURRENCYNAME]
     */
    public void setCurrencyname(String  currencyname){
        this.currencyname = currencyname ;
        this.modify("currencyname",currencyname);
    }

    /**
     * 设置 [ORIGINATINGCASENAME]
     */
    public void setOriginatingcasename(String  originatingcasename){
        this.originatingcasename = originatingcasename ;
        this.modify("originatingcasename",originatingcasename);
    }

    /**
     * 设置 [CAMPAIGNNAME]
     */
    public void setCampaignname(String  campaignname){
        this.campaignname = campaignname ;
        this.modify("campaignname",campaignname);
    }

    /**
     * 设置 [PARENTACCOUNTNAME]
     */
    public void setParentaccountname(String  parentaccountname){
        this.parentaccountname = parentaccountname ;
        this.modify("parentaccountname",parentaccountname);
    }

    /**
     * 设置 [PARENTCONTACTNAME]
     */
    public void setParentcontactname(String  parentcontactname){
        this.parentcontactname = parentcontactname ;
        this.modify("parentcontactname",parentcontactname);
    }

    /**
     * 设置 [QUALIFYINGOPPORTUNITYNAME]
     */
    public void setQualifyingopportunityname(String  qualifyingopportunityname){
        this.qualifyingopportunityname = qualifyingopportunityname ;
        this.modify("qualifyingopportunityname",qualifyingopportunityname);
    }

    /**
     * 设置 [QUALIFYINGOPPORTUNITYID]
     */
    public void setQualifyingopportunityid(String  qualifyingopportunityid){
        this.qualifyingopportunityid = qualifyingopportunityid ;
        this.modify("qualifyingopportunityid",qualifyingopportunityid);
    }

    /**
     * 设置 [SLAID]
     */
    public void setSlaid(String  slaid){
        this.slaid = slaid ;
        this.modify("slaid",slaid);
    }

    /**
     * 设置 [CAMPAIGNID]
     */
    public void setCampaignid(String  campaignid){
        this.campaignid = campaignid ;
        this.modify("campaignid",campaignid);
    }

    /**
     * 设置 [RELATEDOBJECTID]
     */
    public void setRelatedobjectid(String  relatedobjectid){
        this.relatedobjectid = relatedobjectid ;
        this.modify("relatedobjectid",relatedobjectid);
    }

    /**
     * 设置 [TRANSACTIONCURRENCYID]
     */
    public void setTransactioncurrencyid(String  transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }

    /**
     * 设置 [PARENTACCOUNTID]
     */
    public void setParentaccountid(String  parentaccountid){
        this.parentaccountid = parentaccountid ;
        this.modify("parentaccountid",parentaccountid);
    }

    /**
     * 设置 [ORIGINATINGCASEID]
     */
    public void setOriginatingcaseid(String  originatingcaseid){
        this.originatingcaseid = originatingcaseid ;
        this.modify("originatingcaseid",originatingcaseid);
    }

    /**
     * 设置 [PARENTCONTACTID]
     */
    public void setParentcontactid(String  parentcontactid){
        this.parentcontactid = parentcontactid ;
        this.modify("parentcontactid",parentcontactid);
    }


}

