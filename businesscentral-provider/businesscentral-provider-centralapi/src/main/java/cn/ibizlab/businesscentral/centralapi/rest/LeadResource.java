package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.sales.domain.Lead;
import cn.ibizlab.businesscentral.core.sales.service.ILeadService;
import cn.ibizlab.businesscentral.core.sales.filter.LeadSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"潜在顾客" })
@RestController("CentralApi-lead")
@RequestMapping("")
public class LeadResource {

    @Autowired
    public ILeadService leadService;

    @Autowired
    @Lazy
    public LeadMapping leadMapping;

    @PreAuthorize("hasPermission(this.leadMapping.toDomain(#leaddto),'iBizBusinessCentral-Lead-Create')")
    @ApiOperation(value = "新建潜在顾客", tags = {"潜在顾客" },  notes = "新建潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/leads")
    public ResponseEntity<LeadDTO> create(@RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
		leadService.create(domain);
        LeadDTO dto = leadMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.leadMapping.toDomain(#leaddtos),'iBizBusinessCentral-Lead-Create')")
    @ApiOperation(value = "批量新建潜在顾客", tags = {"潜在顾客" },  notes = "批量新建潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/leads/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<LeadDTO> leaddtos) {
        leadService.createBatch(leadMapping.toDomain(leaddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "lead" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.leadService.get(#lead_id),'iBizBusinessCentral-Lead-Update')")
    @ApiOperation(value = "更新潜在顾客", tags = {"潜在顾客" },  notes = "更新潜在顾客")
	@RequestMapping(method = RequestMethod.PUT, value = "/leads/{lead_id}")
    public ResponseEntity<LeadDTO> update(@PathVariable("lead_id") String lead_id, @RequestBody LeadDTO leaddto) {
		Lead domain  = leadMapping.toDomain(leaddto);
        domain .setLeadid(lead_id);
		leadService.update(domain );
		LeadDTO dto = leadMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.leadService.getLeadByEntities(this.leadMapping.toDomain(#leaddtos)),'iBizBusinessCentral-Lead-Update')")
    @ApiOperation(value = "批量更新潜在顾客", tags = {"潜在顾客" },  notes = "批量更新潜在顾客")
	@RequestMapping(method = RequestMethod.PUT, value = "/leads/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<LeadDTO> leaddtos) {
        leadService.updateBatch(leadMapping.toDomain(leaddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.leadService.get(#lead_id),'iBizBusinessCentral-Lead-Remove')")
    @ApiOperation(value = "删除潜在顾客", tags = {"潜在顾客" },  notes = "删除潜在顾客")
	@RequestMapping(method = RequestMethod.DELETE, value = "/leads/{lead_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("lead_id") String lead_id) {
         return ResponseEntity.status(HttpStatus.OK).body(leadService.remove(lead_id));
    }

    @PreAuthorize("hasPermission(this.leadService.getLeadByIds(#ids),'iBizBusinessCentral-Lead-Remove')")
    @ApiOperation(value = "批量删除潜在顾客", tags = {"潜在顾客" },  notes = "批量删除潜在顾客")
	@RequestMapping(method = RequestMethod.DELETE, value = "/leads/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        leadService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.leadMapping.toDomain(returnObject.body),'iBizBusinessCentral-Lead-Get')")
    @ApiOperation(value = "获取潜在顾客", tags = {"潜在顾客" },  notes = "获取潜在顾客")
	@RequestMapping(method = RequestMethod.GET, value = "/leads/{lead_id}")
    public ResponseEntity<LeadDTO> get(@PathVariable("lead_id") String lead_id) {
        Lead domain = leadService.get(lead_id);
        LeadDTO dto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取潜在顾客草稿", tags = {"潜在顾客" },  notes = "获取潜在顾客草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/leads/getdraft")
    public ResponseEntity<LeadDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(leadMapping.toDto(leadService.getDraft(new Lead())));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-Active-all')")
    @ApiOperation(value = "重新激活", tags = {"潜在顾客" },  notes = "重新激活")
	@RequestMapping(method = RequestMethod.POST, value = "/leads/{lead_id}/active")
    public ResponseEntity<LeadDTO> active(@PathVariable("lead_id") String lead_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
domain.setLeadid(lead_id);
        domain = leadService.active(domain);
        leaddto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(leaddto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-AddList-all')")
    @ApiOperation(value = "添加到市场营销列表", tags = {"潜在顾客" },  notes = "添加到市场营销列表")
	@RequestMapping(method = RequestMethod.POST, value = "/leads/{lead_id}/addlist")
    public ResponseEntity<LeadDTO> addList(@PathVariable("lead_id") String lead_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
domain.setLeadid(lead_id);
        domain = leadService.addList(domain);
        leaddto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(leaddto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-Cancel-all')")
    @ApiOperation(value = "取消", tags = {"潜在顾客" },  notes = "取消")
	@RequestMapping(method = RequestMethod.POST, value = "/leads/{lead_id}/cancel")
    public ResponseEntity<LeadDTO> cancel(@PathVariable("lead_id") String lead_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
domain.setLeadid(lead_id);
        domain = leadService.cancel(domain);
        leaddto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(leaddto);
    }

    @ApiOperation(value = "检查潜在顾客", tags = {"潜在顾客" },  notes = "检查潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/leads/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody LeadDTO leaddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(leadService.checkKey(leadMapping.toDomain(leaddto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-DisQualification-all')")
    @ApiOperation(value = "取消资格", tags = {"潜在顾客" },  notes = "取消资格")
	@RequestMapping(method = RequestMethod.POST, value = "/leads/{lead_id}/disqualification")
    public ResponseEntity<LeadDTO> disQualification(@PathVariable("lead_id") String lead_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
domain.setLeadid(lead_id);
        domain = leadService.disQualification(domain);
        leaddto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(leaddto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-LostOrder-all')")
    @ApiOperation(value = "丢单", tags = {"潜在顾客" },  notes = "丢单")
	@RequestMapping(method = RequestMethod.POST, value = "/leads/{lead_id}/lostorder")
    public ResponseEntity<LeadDTO> lostOrder(@PathVariable("lead_id") String lead_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
domain.setLeadid(lead_id);
        domain = leadService.lostOrder(domain);
        leaddto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(leaddto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-NoInterested-all')")
    @ApiOperation(value = "不再感兴趣", tags = {"潜在顾客" },  notes = "不再感兴趣")
	@RequestMapping(method = RequestMethod.POST, value = "/leads/{lead_id}/nointerested")
    public ResponseEntity<LeadDTO> noInterested(@PathVariable("lead_id") String lead_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
domain.setLeadid(lead_id);
        domain = leadService.noInterested(domain);
        leaddto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(leaddto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-Qualification-all')")
    @ApiOperation(value = "授予资格", tags = {"潜在顾客" },  notes = "授予资格")
	@RequestMapping(method = RequestMethod.POST, value = "/leads/{lead_id}/qualification")
    public ResponseEntity<LeadDTO> qualification(@PathVariable("lead_id") String lead_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
domain.setLeadid(lead_id);
        domain = leadService.qualification(domain);
        leaddto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(leaddto);
    }

    @PreAuthorize("hasPermission(this.leadMapping.toDomain(#leaddto),'iBizBusinessCentral-Lead-Save')")
    @ApiOperation(value = "保存潜在顾客", tags = {"潜在顾客" },  notes = "保存潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/leads/save")
    public ResponseEntity<Boolean> save(@RequestBody LeadDTO leaddto) {
        return ResponseEntity.status(HttpStatus.OK).body(leadService.save(leadMapping.toDomain(leaddto)));
    }

    @PreAuthorize("hasPermission(this.leadMapping.toDomain(#leaddtos),'iBizBusinessCentral-Lead-Save')")
    @ApiOperation(value = "批量保存潜在顾客", tags = {"潜在顾客" },  notes = "批量保存潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/leads/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<LeadDTO> leaddtos) {
        leadService.saveBatch(leadMapping.toDomain(leaddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-Unable-all')")
    @ApiOperation(value = "无法联系", tags = {"潜在顾客" },  notes = "无法联系")
	@RequestMapping(method = RequestMethod.POST, value = "/leads/{lead_id}/unable")
    public ResponseEntity<LeadDTO> unable(@PathVariable("lead_id") String lead_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
domain.setLeadid(lead_id);
        domain = leadService.unable(domain);
        leaddto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(leaddto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Lead-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"潜在顾客" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/leads/fetchdefault")
	public ResponseEntity<List<LeadDTO>> fetchDefault(LeadSearchContext context) {
        Page<Lead> domains = leadService.searchDefault(context) ;
        List<LeadDTO> list = leadMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Lead-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"潜在顾客" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/leads/searchdefault")
	public ResponseEntity<Page<LeadDTO>> searchDefault(@RequestBody LeadSearchContext context) {
        Page<Lead> domains = leadService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(leadMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-searchExcluded-all') and hasPermission(#context,'iBizBusinessCentral-Lead-Get')")
	@ApiOperation(value = "获取已排除潜在顾客", tags = {"潜在顾客" } ,notes = "获取已排除潜在顾客")
    @RequestMapping(method= RequestMethod.GET , value="/leads/fetchexcluded")
	public ResponseEntity<List<LeadDTO>> fetchExcluded(LeadSearchContext context) {
        Page<Lead> domains = leadService.searchExcluded(context) ;
        List<LeadDTO> list = leadMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-searchExcluded-all') and hasPermission(#context,'iBizBusinessCentral-Lead-Get')")
	@ApiOperation(value = "查询已排除潜在顾客", tags = {"潜在顾客" } ,notes = "查询已排除潜在顾客")
    @RequestMapping(method= RequestMethod.POST , value="/leads/searchexcluded")
	public ResponseEntity<Page<LeadDTO>> searchExcluded(@RequestBody LeadSearchContext context) {
        Page<Lead> domains = leadService.searchExcluded(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(leadMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-searchOn-all') and hasPermission(#context,'iBizBusinessCentral-Lead-Get')")
	@ApiOperation(value = "获取已开启潜在顾客", tags = {"潜在顾客" } ,notes = "获取已开启潜在顾客")
    @RequestMapping(method= RequestMethod.GET , value="/leads/fetchon")
	public ResponseEntity<List<LeadDTO>> fetchOn(LeadSearchContext context) {
        Page<Lead> domains = leadService.searchOn(context) ;
        List<LeadDTO> list = leadMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-searchOn-all') and hasPermission(#context,'iBizBusinessCentral-Lead-Get')")
	@ApiOperation(value = "查询已开启潜在顾客", tags = {"潜在顾客" } ,notes = "查询已开启潜在顾客")
    @RequestMapping(method= RequestMethod.POST , value="/leads/searchon")
	public ResponseEntity<Page<LeadDTO>> searchOn(@RequestBody LeadSearchContext context) {
        Page<Lead> domains = leadService.searchOn(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(leadMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.leadMapping.toDomain(#leaddto),'iBizBusinessCentral-Lead-Create')")
    @ApiOperation(value = "根据客户建立潜在顾客", tags = {"潜在顾客" },  notes = "根据客户建立潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/leads")
    public ResponseEntity<LeadDTO> createByAccount(@PathVariable("account_id") String account_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
        domain.setParentaccountid(account_id);
		leadService.create(domain);
        LeadDTO dto = leadMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.leadMapping.toDomain(#leaddtos),'iBizBusinessCentral-Lead-Create')")
    @ApiOperation(value = "根据客户批量建立潜在顾客", tags = {"潜在顾客" },  notes = "根据客户批量建立潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/leads/batch")
    public ResponseEntity<Boolean> createBatchByAccount(@PathVariable("account_id") String account_id, @RequestBody List<LeadDTO> leaddtos) {
        List<Lead> domainlist=leadMapping.toDomain(leaddtos);
        for(Lead domain:domainlist){
            domain.setParentaccountid(account_id);
        }
        leadService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "lead" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.leadService.get(#lead_id),'iBizBusinessCentral-Lead-Update')")
    @ApiOperation(value = "根据客户更新潜在顾客", tags = {"潜在顾客" },  notes = "根据客户更新潜在顾客")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/leads/{lead_id}")
    public ResponseEntity<LeadDTO> updateByAccount(@PathVariable("account_id") String account_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
        domain.setParentaccountid(account_id);
        domain.setLeadid(lead_id);
		leadService.update(domain);
        LeadDTO dto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.leadService.getLeadByEntities(this.leadMapping.toDomain(#leaddtos)),'iBizBusinessCentral-Lead-Update')")
    @ApiOperation(value = "根据客户批量更新潜在顾客", tags = {"潜在顾客" },  notes = "根据客户批量更新潜在顾客")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/leads/batch")
    public ResponseEntity<Boolean> updateBatchByAccount(@PathVariable("account_id") String account_id, @RequestBody List<LeadDTO> leaddtos) {
        List<Lead> domainlist=leadMapping.toDomain(leaddtos);
        for(Lead domain:domainlist){
            domain.setParentaccountid(account_id);
        }
        leadService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.leadService.get(#lead_id),'iBizBusinessCentral-Lead-Remove')")
    @ApiOperation(value = "根据客户删除潜在顾客", tags = {"潜在顾客" },  notes = "根据客户删除潜在顾客")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/leads/{lead_id}")
    public ResponseEntity<Boolean> removeByAccount(@PathVariable("account_id") String account_id, @PathVariable("lead_id") String lead_id) {
		return ResponseEntity.status(HttpStatus.OK).body(leadService.remove(lead_id));
    }

    @PreAuthorize("hasPermission(this.leadService.getLeadByIds(#ids),'iBizBusinessCentral-Lead-Remove')")
    @ApiOperation(value = "根据客户批量删除潜在顾客", tags = {"潜在顾客" },  notes = "根据客户批量删除潜在顾客")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/leads/batch")
    public ResponseEntity<Boolean> removeBatchByAccount(@RequestBody List<String> ids) {
        leadService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.leadMapping.toDomain(returnObject.body),'iBizBusinessCentral-Lead-Get')")
    @ApiOperation(value = "根据客户获取潜在顾客", tags = {"潜在顾客" },  notes = "根据客户获取潜在顾客")
	@RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/leads/{lead_id}")
    public ResponseEntity<LeadDTO> getByAccount(@PathVariable("account_id") String account_id, @PathVariable("lead_id") String lead_id) {
        Lead domain = leadService.get(lead_id);
        LeadDTO dto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据客户获取潜在顾客草稿", tags = {"潜在顾客" },  notes = "根据客户获取潜在顾客草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/leads/getdraft")
    public ResponseEntity<LeadDTO> getDraftByAccount(@PathVariable("account_id") String account_id) {
        Lead domain = new Lead();
        domain.setParentaccountid(account_id);
        return ResponseEntity.status(HttpStatus.OK).body(leadMapping.toDto(leadService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-Active-all')")
    @ApiOperation(value = "根据客户潜在顾客", tags = {"潜在顾客" },  notes = "根据客户潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/leads/{lead_id}/active")
    public ResponseEntity<LeadDTO> activeByAccount(@PathVariable("account_id") String account_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
        domain.setParentaccountid(account_id);
        domain = leadService.active(domain) ;
        leaddto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(leaddto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-AddList-all')")
    @ApiOperation(value = "根据客户潜在顾客", tags = {"潜在顾客" },  notes = "根据客户潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/leads/{lead_id}/addlist")
    public ResponseEntity<LeadDTO> addListByAccount(@PathVariable("account_id") String account_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
        domain.setParentaccountid(account_id);
        domain = leadService.addList(domain) ;
        leaddto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(leaddto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-Cancel-all')")
    @ApiOperation(value = "根据客户潜在顾客", tags = {"潜在顾客" },  notes = "根据客户潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/leads/{lead_id}/cancel")
    public ResponseEntity<LeadDTO> cancelByAccount(@PathVariable("account_id") String account_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
        domain.setParentaccountid(account_id);
        domain = leadService.cancel(domain) ;
        leaddto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(leaddto);
    }

    @ApiOperation(value = "根据客户检查潜在顾客", tags = {"潜在顾客" },  notes = "根据客户检查潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/leads/checkkey")
    public ResponseEntity<Boolean> checkKeyByAccount(@PathVariable("account_id") String account_id, @RequestBody LeadDTO leaddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(leadService.checkKey(leadMapping.toDomain(leaddto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-DisQualification-all')")
    @ApiOperation(value = "根据客户潜在顾客", tags = {"潜在顾客" },  notes = "根据客户潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/leads/{lead_id}/disqualification")
    public ResponseEntity<LeadDTO> disQualificationByAccount(@PathVariable("account_id") String account_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
        domain.setParentaccountid(account_id);
        domain = leadService.disQualification(domain) ;
        leaddto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(leaddto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-LostOrder-all')")
    @ApiOperation(value = "根据客户潜在顾客", tags = {"潜在顾客" },  notes = "根据客户潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/leads/{lead_id}/lostorder")
    public ResponseEntity<LeadDTO> lostOrderByAccount(@PathVariable("account_id") String account_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
        domain.setParentaccountid(account_id);
        domain = leadService.lostOrder(domain) ;
        leaddto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(leaddto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-NoInterested-all')")
    @ApiOperation(value = "根据客户潜在顾客", tags = {"潜在顾客" },  notes = "根据客户潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/leads/{lead_id}/nointerested")
    public ResponseEntity<LeadDTO> noInterestedByAccount(@PathVariable("account_id") String account_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
        domain.setParentaccountid(account_id);
        domain = leadService.noInterested(domain) ;
        leaddto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(leaddto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-Qualification-all')")
    @ApiOperation(value = "根据客户潜在顾客", tags = {"潜在顾客" },  notes = "根据客户潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/leads/{lead_id}/qualification")
    public ResponseEntity<LeadDTO> qualificationByAccount(@PathVariable("account_id") String account_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
        domain.setParentaccountid(account_id);
        domain = leadService.qualification(domain) ;
        leaddto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(leaddto);
    }

    @PreAuthorize("hasPermission(this.leadMapping.toDomain(#leaddto),'iBizBusinessCentral-Lead-Save')")
    @ApiOperation(value = "根据客户保存潜在顾客", tags = {"潜在顾客" },  notes = "根据客户保存潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/leads/save")
    public ResponseEntity<Boolean> saveByAccount(@PathVariable("account_id") String account_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
        domain.setParentaccountid(account_id);
        return ResponseEntity.status(HttpStatus.OK).body(leadService.save(domain));
    }

    @PreAuthorize("hasPermission(this.leadMapping.toDomain(#leaddtos),'iBizBusinessCentral-Lead-Save')")
    @ApiOperation(value = "根据客户批量保存潜在顾客", tags = {"潜在顾客" },  notes = "根据客户批量保存潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/leads/savebatch")
    public ResponseEntity<Boolean> saveBatchByAccount(@PathVariable("account_id") String account_id, @RequestBody List<LeadDTO> leaddtos) {
        List<Lead> domainlist=leadMapping.toDomain(leaddtos);
        for(Lead domain:domainlist){
             domain.setParentaccountid(account_id);
        }
        leadService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-Unable-all')")
    @ApiOperation(value = "根据客户潜在顾客", tags = {"潜在顾客" },  notes = "根据客户潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/leads/{lead_id}/unable")
    public ResponseEntity<LeadDTO> unableByAccount(@PathVariable("account_id") String account_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
        domain.setParentaccountid(account_id);
        domain = leadService.unable(domain) ;
        leaddto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(leaddto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Lead-Get')")
	@ApiOperation(value = "根据客户获取DEFAULT", tags = {"潜在顾客" } ,notes = "根据客户获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/leads/fetchdefault")
	public ResponseEntity<List<LeadDTO>> fetchLeadDefaultByAccount(@PathVariable("account_id") String account_id,LeadSearchContext context) {
        context.setN_parentaccountid_eq(account_id);
        Page<Lead> domains = leadService.searchDefault(context) ;
        List<LeadDTO> list = leadMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Lead-Get')")
	@ApiOperation(value = "根据客户查询DEFAULT", tags = {"潜在顾客" } ,notes = "根据客户查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/leads/searchdefault")
	public ResponseEntity<Page<LeadDTO>> searchLeadDefaultByAccount(@PathVariable("account_id") String account_id, @RequestBody LeadSearchContext context) {
        context.setN_parentaccountid_eq(account_id);
        Page<Lead> domains = leadService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(leadMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-searchExcluded-all') and hasPermission(#context,'iBizBusinessCentral-Lead-Get')")
	@ApiOperation(value = "根据客户获取已排除潜在顾客", tags = {"潜在顾客" } ,notes = "根据客户获取已排除潜在顾客")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/leads/fetchexcluded")
	public ResponseEntity<List<LeadDTO>> fetchLeadExcludedByAccount(@PathVariable("account_id") String account_id,LeadSearchContext context) {
        context.setN_parentaccountid_eq(account_id);
        Page<Lead> domains = leadService.searchExcluded(context) ;
        List<LeadDTO> list = leadMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-searchExcluded-all') and hasPermission(#context,'iBizBusinessCentral-Lead-Get')")
	@ApiOperation(value = "根据客户查询已排除潜在顾客", tags = {"潜在顾客" } ,notes = "根据客户查询已排除潜在顾客")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/leads/searchexcluded")
	public ResponseEntity<Page<LeadDTO>> searchLeadExcludedByAccount(@PathVariable("account_id") String account_id, @RequestBody LeadSearchContext context) {
        context.setN_parentaccountid_eq(account_id);
        Page<Lead> domains = leadService.searchExcluded(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(leadMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-searchOn-all') and hasPermission(#context,'iBizBusinessCentral-Lead-Get')")
	@ApiOperation(value = "根据客户获取已开启潜在顾客", tags = {"潜在顾客" } ,notes = "根据客户获取已开启潜在顾客")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/leads/fetchon")
	public ResponseEntity<List<LeadDTO>> fetchLeadOnByAccount(@PathVariable("account_id") String account_id,LeadSearchContext context) {
        context.setN_parentaccountid_eq(account_id);
        Page<Lead> domains = leadService.searchOn(context) ;
        List<LeadDTO> list = leadMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-searchOn-all') and hasPermission(#context,'iBizBusinessCentral-Lead-Get')")
	@ApiOperation(value = "根据客户查询已开启潜在顾客", tags = {"潜在顾客" } ,notes = "根据客户查询已开启潜在顾客")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/leads/searchon")
	public ResponseEntity<Page<LeadDTO>> searchLeadOnByAccount(@PathVariable("account_id") String account_id, @RequestBody LeadSearchContext context) {
        context.setN_parentaccountid_eq(account_id);
        Page<Lead> domains = leadService.searchOn(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(leadMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.leadMapping.toDomain(#leaddto),'iBizBusinessCentral-Lead-Create')")
    @ApiOperation(value = "根据市场活动建立潜在顾客", tags = {"潜在顾客" },  notes = "根据市场活动建立潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/campaigns/{campaign_id}/leads")
    public ResponseEntity<LeadDTO> createByCampaign(@PathVariable("campaign_id") String campaign_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
        domain.setCampaignid(campaign_id);
		leadService.create(domain);
        LeadDTO dto = leadMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.leadMapping.toDomain(#leaddtos),'iBizBusinessCentral-Lead-Create')")
    @ApiOperation(value = "根据市场活动批量建立潜在顾客", tags = {"潜在顾客" },  notes = "根据市场活动批量建立潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/campaigns/{campaign_id}/leads/batch")
    public ResponseEntity<Boolean> createBatchByCampaign(@PathVariable("campaign_id") String campaign_id, @RequestBody List<LeadDTO> leaddtos) {
        List<Lead> domainlist=leadMapping.toDomain(leaddtos);
        for(Lead domain:domainlist){
            domain.setCampaignid(campaign_id);
        }
        leadService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "lead" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.leadService.get(#lead_id),'iBizBusinessCentral-Lead-Update')")
    @ApiOperation(value = "根据市场活动更新潜在顾客", tags = {"潜在顾客" },  notes = "根据市场活动更新潜在顾客")
	@RequestMapping(method = RequestMethod.PUT, value = "/campaigns/{campaign_id}/leads/{lead_id}")
    public ResponseEntity<LeadDTO> updateByCampaign(@PathVariable("campaign_id") String campaign_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
        domain.setCampaignid(campaign_id);
        domain.setLeadid(lead_id);
		leadService.update(domain);
        LeadDTO dto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.leadService.getLeadByEntities(this.leadMapping.toDomain(#leaddtos)),'iBizBusinessCentral-Lead-Update')")
    @ApiOperation(value = "根据市场活动批量更新潜在顾客", tags = {"潜在顾客" },  notes = "根据市场活动批量更新潜在顾客")
	@RequestMapping(method = RequestMethod.PUT, value = "/campaigns/{campaign_id}/leads/batch")
    public ResponseEntity<Boolean> updateBatchByCampaign(@PathVariable("campaign_id") String campaign_id, @RequestBody List<LeadDTO> leaddtos) {
        List<Lead> domainlist=leadMapping.toDomain(leaddtos);
        for(Lead domain:domainlist){
            domain.setCampaignid(campaign_id);
        }
        leadService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.leadService.get(#lead_id),'iBizBusinessCentral-Lead-Remove')")
    @ApiOperation(value = "根据市场活动删除潜在顾客", tags = {"潜在顾客" },  notes = "根据市场活动删除潜在顾客")
	@RequestMapping(method = RequestMethod.DELETE, value = "/campaigns/{campaign_id}/leads/{lead_id}")
    public ResponseEntity<Boolean> removeByCampaign(@PathVariable("campaign_id") String campaign_id, @PathVariable("lead_id") String lead_id) {
		return ResponseEntity.status(HttpStatus.OK).body(leadService.remove(lead_id));
    }

    @PreAuthorize("hasPermission(this.leadService.getLeadByIds(#ids),'iBizBusinessCentral-Lead-Remove')")
    @ApiOperation(value = "根据市场活动批量删除潜在顾客", tags = {"潜在顾客" },  notes = "根据市场活动批量删除潜在顾客")
	@RequestMapping(method = RequestMethod.DELETE, value = "/campaigns/{campaign_id}/leads/batch")
    public ResponseEntity<Boolean> removeBatchByCampaign(@RequestBody List<String> ids) {
        leadService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.leadMapping.toDomain(returnObject.body),'iBizBusinessCentral-Lead-Get')")
    @ApiOperation(value = "根据市场活动获取潜在顾客", tags = {"潜在顾客" },  notes = "根据市场活动获取潜在顾客")
	@RequestMapping(method = RequestMethod.GET, value = "/campaigns/{campaign_id}/leads/{lead_id}")
    public ResponseEntity<LeadDTO> getByCampaign(@PathVariable("campaign_id") String campaign_id, @PathVariable("lead_id") String lead_id) {
        Lead domain = leadService.get(lead_id);
        LeadDTO dto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据市场活动获取潜在顾客草稿", tags = {"潜在顾客" },  notes = "根据市场活动获取潜在顾客草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/campaigns/{campaign_id}/leads/getdraft")
    public ResponseEntity<LeadDTO> getDraftByCampaign(@PathVariable("campaign_id") String campaign_id) {
        Lead domain = new Lead();
        domain.setCampaignid(campaign_id);
        return ResponseEntity.status(HttpStatus.OK).body(leadMapping.toDto(leadService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-Active-all')")
    @ApiOperation(value = "根据市场活动潜在顾客", tags = {"潜在顾客" },  notes = "根据市场活动潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/campaigns/{campaign_id}/leads/{lead_id}/active")
    public ResponseEntity<LeadDTO> activeByCampaign(@PathVariable("campaign_id") String campaign_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
        domain.setCampaignid(campaign_id);
        domain = leadService.active(domain) ;
        leaddto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(leaddto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-AddList-all')")
    @ApiOperation(value = "根据市场活动潜在顾客", tags = {"潜在顾客" },  notes = "根据市场活动潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/campaigns/{campaign_id}/leads/{lead_id}/addlist")
    public ResponseEntity<LeadDTO> addListByCampaign(@PathVariable("campaign_id") String campaign_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
        domain.setCampaignid(campaign_id);
        domain = leadService.addList(domain) ;
        leaddto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(leaddto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-Cancel-all')")
    @ApiOperation(value = "根据市场活动潜在顾客", tags = {"潜在顾客" },  notes = "根据市场活动潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/campaigns/{campaign_id}/leads/{lead_id}/cancel")
    public ResponseEntity<LeadDTO> cancelByCampaign(@PathVariable("campaign_id") String campaign_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
        domain.setCampaignid(campaign_id);
        domain = leadService.cancel(domain) ;
        leaddto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(leaddto);
    }

    @ApiOperation(value = "根据市场活动检查潜在顾客", tags = {"潜在顾客" },  notes = "根据市场活动检查潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/campaigns/{campaign_id}/leads/checkkey")
    public ResponseEntity<Boolean> checkKeyByCampaign(@PathVariable("campaign_id") String campaign_id, @RequestBody LeadDTO leaddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(leadService.checkKey(leadMapping.toDomain(leaddto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-DisQualification-all')")
    @ApiOperation(value = "根据市场活动潜在顾客", tags = {"潜在顾客" },  notes = "根据市场活动潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/campaigns/{campaign_id}/leads/{lead_id}/disqualification")
    public ResponseEntity<LeadDTO> disQualificationByCampaign(@PathVariable("campaign_id") String campaign_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
        domain.setCampaignid(campaign_id);
        domain = leadService.disQualification(domain) ;
        leaddto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(leaddto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-LostOrder-all')")
    @ApiOperation(value = "根据市场活动潜在顾客", tags = {"潜在顾客" },  notes = "根据市场活动潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/campaigns/{campaign_id}/leads/{lead_id}/lostorder")
    public ResponseEntity<LeadDTO> lostOrderByCampaign(@PathVariable("campaign_id") String campaign_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
        domain.setCampaignid(campaign_id);
        domain = leadService.lostOrder(domain) ;
        leaddto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(leaddto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-NoInterested-all')")
    @ApiOperation(value = "根据市场活动潜在顾客", tags = {"潜在顾客" },  notes = "根据市场活动潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/campaigns/{campaign_id}/leads/{lead_id}/nointerested")
    public ResponseEntity<LeadDTO> noInterestedByCampaign(@PathVariable("campaign_id") String campaign_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
        domain.setCampaignid(campaign_id);
        domain = leadService.noInterested(domain) ;
        leaddto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(leaddto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-Qualification-all')")
    @ApiOperation(value = "根据市场活动潜在顾客", tags = {"潜在顾客" },  notes = "根据市场活动潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/campaigns/{campaign_id}/leads/{lead_id}/qualification")
    public ResponseEntity<LeadDTO> qualificationByCampaign(@PathVariable("campaign_id") String campaign_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
        domain.setCampaignid(campaign_id);
        domain = leadService.qualification(domain) ;
        leaddto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(leaddto);
    }

    @PreAuthorize("hasPermission(this.leadMapping.toDomain(#leaddto),'iBizBusinessCentral-Lead-Save')")
    @ApiOperation(value = "根据市场活动保存潜在顾客", tags = {"潜在顾客" },  notes = "根据市场活动保存潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/campaigns/{campaign_id}/leads/save")
    public ResponseEntity<Boolean> saveByCampaign(@PathVariable("campaign_id") String campaign_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
        domain.setCampaignid(campaign_id);
        return ResponseEntity.status(HttpStatus.OK).body(leadService.save(domain));
    }

    @PreAuthorize("hasPermission(this.leadMapping.toDomain(#leaddtos),'iBizBusinessCentral-Lead-Save')")
    @ApiOperation(value = "根据市场活动批量保存潜在顾客", tags = {"潜在顾客" },  notes = "根据市场活动批量保存潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/campaigns/{campaign_id}/leads/savebatch")
    public ResponseEntity<Boolean> saveBatchByCampaign(@PathVariable("campaign_id") String campaign_id, @RequestBody List<LeadDTO> leaddtos) {
        List<Lead> domainlist=leadMapping.toDomain(leaddtos);
        for(Lead domain:domainlist){
             domain.setCampaignid(campaign_id);
        }
        leadService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-Unable-all')")
    @ApiOperation(value = "根据市场活动潜在顾客", tags = {"潜在顾客" },  notes = "根据市场活动潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/campaigns/{campaign_id}/leads/{lead_id}/unable")
    public ResponseEntity<LeadDTO> unableByCampaign(@PathVariable("campaign_id") String campaign_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
        domain.setCampaignid(campaign_id);
        domain = leadService.unable(domain) ;
        leaddto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(leaddto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Lead-Get')")
	@ApiOperation(value = "根据市场活动获取DEFAULT", tags = {"潜在顾客" } ,notes = "根据市场活动获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/campaigns/{campaign_id}/leads/fetchdefault")
	public ResponseEntity<List<LeadDTO>> fetchLeadDefaultByCampaign(@PathVariable("campaign_id") String campaign_id,LeadSearchContext context) {
        context.setN_campaignid_eq(campaign_id);
        Page<Lead> domains = leadService.searchDefault(context) ;
        List<LeadDTO> list = leadMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Lead-Get')")
	@ApiOperation(value = "根据市场活动查询DEFAULT", tags = {"潜在顾客" } ,notes = "根据市场活动查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/campaigns/{campaign_id}/leads/searchdefault")
	public ResponseEntity<Page<LeadDTO>> searchLeadDefaultByCampaign(@PathVariable("campaign_id") String campaign_id, @RequestBody LeadSearchContext context) {
        context.setN_campaignid_eq(campaign_id);
        Page<Lead> domains = leadService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(leadMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-searchExcluded-all') and hasPermission(#context,'iBizBusinessCentral-Lead-Get')")
	@ApiOperation(value = "根据市场活动获取已排除潜在顾客", tags = {"潜在顾客" } ,notes = "根据市场活动获取已排除潜在顾客")
    @RequestMapping(method= RequestMethod.GET , value="/campaigns/{campaign_id}/leads/fetchexcluded")
	public ResponseEntity<List<LeadDTO>> fetchLeadExcludedByCampaign(@PathVariable("campaign_id") String campaign_id,LeadSearchContext context) {
        context.setN_campaignid_eq(campaign_id);
        Page<Lead> domains = leadService.searchExcluded(context) ;
        List<LeadDTO> list = leadMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-searchExcluded-all') and hasPermission(#context,'iBizBusinessCentral-Lead-Get')")
	@ApiOperation(value = "根据市场活动查询已排除潜在顾客", tags = {"潜在顾客" } ,notes = "根据市场活动查询已排除潜在顾客")
    @RequestMapping(method= RequestMethod.POST , value="/campaigns/{campaign_id}/leads/searchexcluded")
	public ResponseEntity<Page<LeadDTO>> searchLeadExcludedByCampaign(@PathVariable("campaign_id") String campaign_id, @RequestBody LeadSearchContext context) {
        context.setN_campaignid_eq(campaign_id);
        Page<Lead> domains = leadService.searchExcluded(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(leadMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-searchOn-all') and hasPermission(#context,'iBizBusinessCentral-Lead-Get')")
	@ApiOperation(value = "根据市场活动获取已开启潜在顾客", tags = {"潜在顾客" } ,notes = "根据市场活动获取已开启潜在顾客")
    @RequestMapping(method= RequestMethod.GET , value="/campaigns/{campaign_id}/leads/fetchon")
	public ResponseEntity<List<LeadDTO>> fetchLeadOnByCampaign(@PathVariable("campaign_id") String campaign_id,LeadSearchContext context) {
        context.setN_campaignid_eq(campaign_id);
        Page<Lead> domains = leadService.searchOn(context) ;
        List<LeadDTO> list = leadMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-searchOn-all') and hasPermission(#context,'iBizBusinessCentral-Lead-Get')")
	@ApiOperation(value = "根据市场活动查询已开启潜在顾客", tags = {"潜在顾客" } ,notes = "根据市场活动查询已开启潜在顾客")
    @RequestMapping(method= RequestMethod.POST , value="/campaigns/{campaign_id}/leads/searchon")
	public ResponseEntity<Page<LeadDTO>> searchLeadOnByCampaign(@PathVariable("campaign_id") String campaign_id, @RequestBody LeadSearchContext context) {
        context.setN_campaignid_eq(campaign_id);
        Page<Lead> domains = leadService.searchOn(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(leadMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.leadMapping.toDomain(#leaddto),'iBizBusinessCentral-Lead-Create')")
    @ApiOperation(value = "根据联系人建立潜在顾客", tags = {"潜在顾客" },  notes = "根据联系人建立潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/leads")
    public ResponseEntity<LeadDTO> createByContact(@PathVariable("contact_id") String contact_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
        domain.setParentcontactid(contact_id);
		leadService.create(domain);
        LeadDTO dto = leadMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.leadMapping.toDomain(#leaddtos),'iBizBusinessCentral-Lead-Create')")
    @ApiOperation(value = "根据联系人批量建立潜在顾客", tags = {"潜在顾客" },  notes = "根据联系人批量建立潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/leads/batch")
    public ResponseEntity<Boolean> createBatchByContact(@PathVariable("contact_id") String contact_id, @RequestBody List<LeadDTO> leaddtos) {
        List<Lead> domainlist=leadMapping.toDomain(leaddtos);
        for(Lead domain:domainlist){
            domain.setParentcontactid(contact_id);
        }
        leadService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "lead" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.leadService.get(#lead_id),'iBizBusinessCentral-Lead-Update')")
    @ApiOperation(value = "根据联系人更新潜在顾客", tags = {"潜在顾客" },  notes = "根据联系人更新潜在顾客")
	@RequestMapping(method = RequestMethod.PUT, value = "/contacts/{contact_id}/leads/{lead_id}")
    public ResponseEntity<LeadDTO> updateByContact(@PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
        domain.setParentcontactid(contact_id);
        domain.setLeadid(lead_id);
		leadService.update(domain);
        LeadDTO dto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.leadService.getLeadByEntities(this.leadMapping.toDomain(#leaddtos)),'iBizBusinessCentral-Lead-Update')")
    @ApiOperation(value = "根据联系人批量更新潜在顾客", tags = {"潜在顾客" },  notes = "根据联系人批量更新潜在顾客")
	@RequestMapping(method = RequestMethod.PUT, value = "/contacts/{contact_id}/leads/batch")
    public ResponseEntity<Boolean> updateBatchByContact(@PathVariable("contact_id") String contact_id, @RequestBody List<LeadDTO> leaddtos) {
        List<Lead> domainlist=leadMapping.toDomain(leaddtos);
        for(Lead domain:domainlist){
            domain.setParentcontactid(contact_id);
        }
        leadService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.leadService.get(#lead_id),'iBizBusinessCentral-Lead-Remove')")
    @ApiOperation(value = "根据联系人删除潜在顾客", tags = {"潜在顾客" },  notes = "根据联系人删除潜在顾客")
	@RequestMapping(method = RequestMethod.DELETE, value = "/contacts/{contact_id}/leads/{lead_id}")
    public ResponseEntity<Boolean> removeByContact(@PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id) {
		return ResponseEntity.status(HttpStatus.OK).body(leadService.remove(lead_id));
    }

    @PreAuthorize("hasPermission(this.leadService.getLeadByIds(#ids),'iBizBusinessCentral-Lead-Remove')")
    @ApiOperation(value = "根据联系人批量删除潜在顾客", tags = {"潜在顾客" },  notes = "根据联系人批量删除潜在顾客")
	@RequestMapping(method = RequestMethod.DELETE, value = "/contacts/{contact_id}/leads/batch")
    public ResponseEntity<Boolean> removeBatchByContact(@RequestBody List<String> ids) {
        leadService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.leadMapping.toDomain(returnObject.body),'iBizBusinessCentral-Lead-Get')")
    @ApiOperation(value = "根据联系人获取潜在顾客", tags = {"潜在顾客" },  notes = "根据联系人获取潜在顾客")
	@RequestMapping(method = RequestMethod.GET, value = "/contacts/{contact_id}/leads/{lead_id}")
    public ResponseEntity<LeadDTO> getByContact(@PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id) {
        Lead domain = leadService.get(lead_id);
        LeadDTO dto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据联系人获取潜在顾客草稿", tags = {"潜在顾客" },  notes = "根据联系人获取潜在顾客草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/contacts/{contact_id}/leads/getdraft")
    public ResponseEntity<LeadDTO> getDraftByContact(@PathVariable("contact_id") String contact_id) {
        Lead domain = new Lead();
        domain.setParentcontactid(contact_id);
        return ResponseEntity.status(HttpStatus.OK).body(leadMapping.toDto(leadService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-Active-all')")
    @ApiOperation(value = "根据联系人潜在顾客", tags = {"潜在顾客" },  notes = "根据联系人潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/leads/{lead_id}/active")
    public ResponseEntity<LeadDTO> activeByContact(@PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
        domain.setParentcontactid(contact_id);
        domain = leadService.active(domain) ;
        leaddto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(leaddto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-AddList-all')")
    @ApiOperation(value = "根据联系人潜在顾客", tags = {"潜在顾客" },  notes = "根据联系人潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/leads/{lead_id}/addlist")
    public ResponseEntity<LeadDTO> addListByContact(@PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
        domain.setParentcontactid(contact_id);
        domain = leadService.addList(domain) ;
        leaddto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(leaddto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-Cancel-all')")
    @ApiOperation(value = "根据联系人潜在顾客", tags = {"潜在顾客" },  notes = "根据联系人潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/leads/{lead_id}/cancel")
    public ResponseEntity<LeadDTO> cancelByContact(@PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
        domain.setParentcontactid(contact_id);
        domain = leadService.cancel(domain) ;
        leaddto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(leaddto);
    }

    @ApiOperation(value = "根据联系人检查潜在顾客", tags = {"潜在顾客" },  notes = "根据联系人检查潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/leads/checkkey")
    public ResponseEntity<Boolean> checkKeyByContact(@PathVariable("contact_id") String contact_id, @RequestBody LeadDTO leaddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(leadService.checkKey(leadMapping.toDomain(leaddto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-DisQualification-all')")
    @ApiOperation(value = "根据联系人潜在顾客", tags = {"潜在顾客" },  notes = "根据联系人潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/leads/{lead_id}/disqualification")
    public ResponseEntity<LeadDTO> disQualificationByContact(@PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
        domain.setParentcontactid(contact_id);
        domain = leadService.disQualification(domain) ;
        leaddto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(leaddto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-LostOrder-all')")
    @ApiOperation(value = "根据联系人潜在顾客", tags = {"潜在顾客" },  notes = "根据联系人潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/leads/{lead_id}/lostorder")
    public ResponseEntity<LeadDTO> lostOrderByContact(@PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
        domain.setParentcontactid(contact_id);
        domain = leadService.lostOrder(domain) ;
        leaddto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(leaddto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-NoInterested-all')")
    @ApiOperation(value = "根据联系人潜在顾客", tags = {"潜在顾客" },  notes = "根据联系人潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/leads/{lead_id}/nointerested")
    public ResponseEntity<LeadDTO> noInterestedByContact(@PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
        domain.setParentcontactid(contact_id);
        domain = leadService.noInterested(domain) ;
        leaddto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(leaddto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-Qualification-all')")
    @ApiOperation(value = "根据联系人潜在顾客", tags = {"潜在顾客" },  notes = "根据联系人潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/leads/{lead_id}/qualification")
    public ResponseEntity<LeadDTO> qualificationByContact(@PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
        domain.setParentcontactid(contact_id);
        domain = leadService.qualification(domain) ;
        leaddto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(leaddto);
    }

    @PreAuthorize("hasPermission(this.leadMapping.toDomain(#leaddto),'iBizBusinessCentral-Lead-Save')")
    @ApiOperation(value = "根据联系人保存潜在顾客", tags = {"潜在顾客" },  notes = "根据联系人保存潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/leads/save")
    public ResponseEntity<Boolean> saveByContact(@PathVariable("contact_id") String contact_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
        domain.setParentcontactid(contact_id);
        return ResponseEntity.status(HttpStatus.OK).body(leadService.save(domain));
    }

    @PreAuthorize("hasPermission(this.leadMapping.toDomain(#leaddtos),'iBizBusinessCentral-Lead-Save')")
    @ApiOperation(value = "根据联系人批量保存潜在顾客", tags = {"潜在顾客" },  notes = "根据联系人批量保存潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/leads/savebatch")
    public ResponseEntity<Boolean> saveBatchByContact(@PathVariable("contact_id") String contact_id, @RequestBody List<LeadDTO> leaddtos) {
        List<Lead> domainlist=leadMapping.toDomain(leaddtos);
        for(Lead domain:domainlist){
             domain.setParentcontactid(contact_id);
        }
        leadService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-Unable-all')")
    @ApiOperation(value = "根据联系人潜在顾客", tags = {"潜在顾客" },  notes = "根据联系人潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/leads/{lead_id}/unable")
    public ResponseEntity<LeadDTO> unableByContact(@PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
        domain.setParentcontactid(contact_id);
        domain = leadService.unable(domain) ;
        leaddto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(leaddto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Lead-Get')")
	@ApiOperation(value = "根据联系人获取DEFAULT", tags = {"潜在顾客" } ,notes = "根据联系人获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/contacts/{contact_id}/leads/fetchdefault")
	public ResponseEntity<List<LeadDTO>> fetchLeadDefaultByContact(@PathVariable("contact_id") String contact_id,LeadSearchContext context) {
        context.setN_parentcontactid_eq(contact_id);
        Page<Lead> domains = leadService.searchDefault(context) ;
        List<LeadDTO> list = leadMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Lead-Get')")
	@ApiOperation(value = "根据联系人查询DEFAULT", tags = {"潜在顾客" } ,notes = "根据联系人查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/contacts/{contact_id}/leads/searchdefault")
	public ResponseEntity<Page<LeadDTO>> searchLeadDefaultByContact(@PathVariable("contact_id") String contact_id, @RequestBody LeadSearchContext context) {
        context.setN_parentcontactid_eq(contact_id);
        Page<Lead> domains = leadService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(leadMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-searchExcluded-all') and hasPermission(#context,'iBizBusinessCentral-Lead-Get')")
	@ApiOperation(value = "根据联系人获取已排除潜在顾客", tags = {"潜在顾客" } ,notes = "根据联系人获取已排除潜在顾客")
    @RequestMapping(method= RequestMethod.GET , value="/contacts/{contact_id}/leads/fetchexcluded")
	public ResponseEntity<List<LeadDTO>> fetchLeadExcludedByContact(@PathVariable("contact_id") String contact_id,LeadSearchContext context) {
        context.setN_parentcontactid_eq(contact_id);
        Page<Lead> domains = leadService.searchExcluded(context) ;
        List<LeadDTO> list = leadMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-searchExcluded-all') and hasPermission(#context,'iBizBusinessCentral-Lead-Get')")
	@ApiOperation(value = "根据联系人查询已排除潜在顾客", tags = {"潜在顾客" } ,notes = "根据联系人查询已排除潜在顾客")
    @RequestMapping(method= RequestMethod.POST , value="/contacts/{contact_id}/leads/searchexcluded")
	public ResponseEntity<Page<LeadDTO>> searchLeadExcludedByContact(@PathVariable("contact_id") String contact_id, @RequestBody LeadSearchContext context) {
        context.setN_parentcontactid_eq(contact_id);
        Page<Lead> domains = leadService.searchExcluded(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(leadMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-searchOn-all') and hasPermission(#context,'iBizBusinessCentral-Lead-Get')")
	@ApiOperation(value = "根据联系人获取已开启潜在顾客", tags = {"潜在顾客" } ,notes = "根据联系人获取已开启潜在顾客")
    @RequestMapping(method= RequestMethod.GET , value="/contacts/{contact_id}/leads/fetchon")
	public ResponseEntity<List<LeadDTO>> fetchLeadOnByContact(@PathVariable("contact_id") String contact_id,LeadSearchContext context) {
        context.setN_parentcontactid_eq(contact_id);
        Page<Lead> domains = leadService.searchOn(context) ;
        List<LeadDTO> list = leadMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-searchOn-all') and hasPermission(#context,'iBizBusinessCentral-Lead-Get')")
	@ApiOperation(value = "根据联系人查询已开启潜在顾客", tags = {"潜在顾客" } ,notes = "根据联系人查询已开启潜在顾客")
    @RequestMapping(method= RequestMethod.POST , value="/contacts/{contact_id}/leads/searchon")
	public ResponseEntity<Page<LeadDTO>> searchLeadOnByContact(@PathVariable("contact_id") String contact_id, @RequestBody LeadSearchContext context) {
        context.setN_parentcontactid_eq(contact_id);
        Page<Lead> domains = leadService.searchOn(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(leadMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.leadMapping.toDomain(#leaddto),'iBizBusinessCentral-Lead-Create')")
    @ApiOperation(value = "根据客户联系人建立潜在顾客", tags = {"潜在顾客" },  notes = "根据客户联系人建立潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/leads")
    public ResponseEntity<LeadDTO> createByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
        domain.setParentcontactid(contact_id);
		leadService.create(domain);
        LeadDTO dto = leadMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.leadMapping.toDomain(#leaddtos),'iBizBusinessCentral-Lead-Create')")
    @ApiOperation(value = "根据客户联系人批量建立潜在顾客", tags = {"潜在顾客" },  notes = "根据客户联系人批量建立潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/leads/batch")
    public ResponseEntity<Boolean> createBatchByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @RequestBody List<LeadDTO> leaddtos) {
        List<Lead> domainlist=leadMapping.toDomain(leaddtos);
        for(Lead domain:domainlist){
            domain.setParentcontactid(contact_id);
        }
        leadService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "lead" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.leadService.get(#lead_id),'iBizBusinessCentral-Lead-Update')")
    @ApiOperation(value = "根据客户联系人更新潜在顾客", tags = {"潜在顾客" },  notes = "根据客户联系人更新潜在顾客")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/contacts/{contact_id}/leads/{lead_id}")
    public ResponseEntity<LeadDTO> updateByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
        domain.setParentcontactid(contact_id);
        domain.setLeadid(lead_id);
		leadService.update(domain);
        LeadDTO dto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.leadService.getLeadByEntities(this.leadMapping.toDomain(#leaddtos)),'iBizBusinessCentral-Lead-Update')")
    @ApiOperation(value = "根据客户联系人批量更新潜在顾客", tags = {"潜在顾客" },  notes = "根据客户联系人批量更新潜在顾客")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/contacts/{contact_id}/leads/batch")
    public ResponseEntity<Boolean> updateBatchByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @RequestBody List<LeadDTO> leaddtos) {
        List<Lead> domainlist=leadMapping.toDomain(leaddtos);
        for(Lead domain:domainlist){
            domain.setParentcontactid(contact_id);
        }
        leadService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.leadService.get(#lead_id),'iBizBusinessCentral-Lead-Remove')")
    @ApiOperation(value = "根据客户联系人删除潜在顾客", tags = {"潜在顾客" },  notes = "根据客户联系人删除潜在顾客")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/contacts/{contact_id}/leads/{lead_id}")
    public ResponseEntity<Boolean> removeByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id) {
		return ResponseEntity.status(HttpStatus.OK).body(leadService.remove(lead_id));
    }

    @PreAuthorize("hasPermission(this.leadService.getLeadByIds(#ids),'iBizBusinessCentral-Lead-Remove')")
    @ApiOperation(value = "根据客户联系人批量删除潜在顾客", tags = {"潜在顾客" },  notes = "根据客户联系人批量删除潜在顾客")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/contacts/{contact_id}/leads/batch")
    public ResponseEntity<Boolean> removeBatchByAccountContact(@RequestBody List<String> ids) {
        leadService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.leadMapping.toDomain(returnObject.body),'iBizBusinessCentral-Lead-Get')")
    @ApiOperation(value = "根据客户联系人获取潜在顾客", tags = {"潜在顾客" },  notes = "根据客户联系人获取潜在顾客")
	@RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/contacts/{contact_id}/leads/{lead_id}")
    public ResponseEntity<LeadDTO> getByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id) {
        Lead domain = leadService.get(lead_id);
        LeadDTO dto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据客户联系人获取潜在顾客草稿", tags = {"潜在顾客" },  notes = "根据客户联系人获取潜在顾客草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/contacts/{contact_id}/leads/getdraft")
    public ResponseEntity<LeadDTO> getDraftByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id) {
        Lead domain = new Lead();
        domain.setParentcontactid(contact_id);
        return ResponseEntity.status(HttpStatus.OK).body(leadMapping.toDto(leadService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-Active-all')")
    @ApiOperation(value = "根据客户联系人潜在顾客", tags = {"潜在顾客" },  notes = "根据客户联系人潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/leads/{lead_id}/active")
    public ResponseEntity<LeadDTO> activeByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
        domain.setParentcontactid(contact_id);
        domain = leadService.active(domain) ;
        leaddto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(leaddto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-AddList-all')")
    @ApiOperation(value = "根据客户联系人潜在顾客", tags = {"潜在顾客" },  notes = "根据客户联系人潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/leads/{lead_id}/addlist")
    public ResponseEntity<LeadDTO> addListByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
        domain.setParentcontactid(contact_id);
        domain = leadService.addList(domain) ;
        leaddto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(leaddto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-Cancel-all')")
    @ApiOperation(value = "根据客户联系人潜在顾客", tags = {"潜在顾客" },  notes = "根据客户联系人潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/leads/{lead_id}/cancel")
    public ResponseEntity<LeadDTO> cancelByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
        domain.setParentcontactid(contact_id);
        domain = leadService.cancel(domain) ;
        leaddto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(leaddto);
    }

    @ApiOperation(value = "根据客户联系人检查潜在顾客", tags = {"潜在顾客" },  notes = "根据客户联系人检查潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/leads/checkkey")
    public ResponseEntity<Boolean> checkKeyByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @RequestBody LeadDTO leaddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(leadService.checkKey(leadMapping.toDomain(leaddto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-DisQualification-all')")
    @ApiOperation(value = "根据客户联系人潜在顾客", tags = {"潜在顾客" },  notes = "根据客户联系人潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/leads/{lead_id}/disqualification")
    public ResponseEntity<LeadDTO> disQualificationByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
        domain.setParentcontactid(contact_id);
        domain = leadService.disQualification(domain) ;
        leaddto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(leaddto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-LostOrder-all')")
    @ApiOperation(value = "根据客户联系人潜在顾客", tags = {"潜在顾客" },  notes = "根据客户联系人潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/leads/{lead_id}/lostorder")
    public ResponseEntity<LeadDTO> lostOrderByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
        domain.setParentcontactid(contact_id);
        domain = leadService.lostOrder(domain) ;
        leaddto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(leaddto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-NoInterested-all')")
    @ApiOperation(value = "根据客户联系人潜在顾客", tags = {"潜在顾客" },  notes = "根据客户联系人潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/leads/{lead_id}/nointerested")
    public ResponseEntity<LeadDTO> noInterestedByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
        domain.setParentcontactid(contact_id);
        domain = leadService.noInterested(domain) ;
        leaddto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(leaddto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-Qualification-all')")
    @ApiOperation(value = "根据客户联系人潜在顾客", tags = {"潜在顾客" },  notes = "根据客户联系人潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/leads/{lead_id}/qualification")
    public ResponseEntity<LeadDTO> qualificationByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
        domain.setParentcontactid(contact_id);
        domain = leadService.qualification(domain) ;
        leaddto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(leaddto);
    }

    @PreAuthorize("hasPermission(this.leadMapping.toDomain(#leaddto),'iBizBusinessCentral-Lead-Save')")
    @ApiOperation(value = "根据客户联系人保存潜在顾客", tags = {"潜在顾客" },  notes = "根据客户联系人保存潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/leads/save")
    public ResponseEntity<Boolean> saveByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
        domain.setParentcontactid(contact_id);
        return ResponseEntity.status(HttpStatus.OK).body(leadService.save(domain));
    }

    @PreAuthorize("hasPermission(this.leadMapping.toDomain(#leaddtos),'iBizBusinessCentral-Lead-Save')")
    @ApiOperation(value = "根据客户联系人批量保存潜在顾客", tags = {"潜在顾客" },  notes = "根据客户联系人批量保存潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/leads/savebatch")
    public ResponseEntity<Boolean> saveBatchByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @RequestBody List<LeadDTO> leaddtos) {
        List<Lead> domainlist=leadMapping.toDomain(leaddtos);
        for(Lead domain:domainlist){
             domain.setParentcontactid(contact_id);
        }
        leadService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-Unable-all')")
    @ApiOperation(value = "根据客户联系人潜在顾客", tags = {"潜在顾客" },  notes = "根据客户联系人潜在顾客")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/leads/{lead_id}/unable")
    public ResponseEntity<LeadDTO> unableByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @RequestBody LeadDTO leaddto) {
        Lead domain = leadMapping.toDomain(leaddto);
        domain.setParentcontactid(contact_id);
        domain = leadService.unable(domain) ;
        leaddto = leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(leaddto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Lead-Get')")
	@ApiOperation(value = "根据客户联系人获取DEFAULT", tags = {"潜在顾客" } ,notes = "根据客户联系人获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/contacts/{contact_id}/leads/fetchdefault")
	public ResponseEntity<List<LeadDTO>> fetchLeadDefaultByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id,LeadSearchContext context) {
        context.setN_parentcontactid_eq(contact_id);
        Page<Lead> domains = leadService.searchDefault(context) ;
        List<LeadDTO> list = leadMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Lead-Get')")
	@ApiOperation(value = "根据客户联系人查询DEFAULT", tags = {"潜在顾客" } ,notes = "根据客户联系人查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/contacts/{contact_id}/leads/searchdefault")
	public ResponseEntity<Page<LeadDTO>> searchLeadDefaultByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @RequestBody LeadSearchContext context) {
        context.setN_parentcontactid_eq(contact_id);
        Page<Lead> domains = leadService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(leadMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-searchExcluded-all') and hasPermission(#context,'iBizBusinessCentral-Lead-Get')")
	@ApiOperation(value = "根据客户联系人获取已排除潜在顾客", tags = {"潜在顾客" } ,notes = "根据客户联系人获取已排除潜在顾客")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/contacts/{contact_id}/leads/fetchexcluded")
	public ResponseEntity<List<LeadDTO>> fetchLeadExcludedByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id,LeadSearchContext context) {
        context.setN_parentcontactid_eq(contact_id);
        Page<Lead> domains = leadService.searchExcluded(context) ;
        List<LeadDTO> list = leadMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-searchExcluded-all') and hasPermission(#context,'iBizBusinessCentral-Lead-Get')")
	@ApiOperation(value = "根据客户联系人查询已排除潜在顾客", tags = {"潜在顾客" } ,notes = "根据客户联系人查询已排除潜在顾客")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/contacts/{contact_id}/leads/searchexcluded")
	public ResponseEntity<Page<LeadDTO>> searchLeadExcludedByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @RequestBody LeadSearchContext context) {
        context.setN_parentcontactid_eq(contact_id);
        Page<Lead> domains = leadService.searchExcluded(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(leadMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-searchOn-all') and hasPermission(#context,'iBizBusinessCentral-Lead-Get')")
	@ApiOperation(value = "根据客户联系人获取已开启潜在顾客", tags = {"潜在顾客" } ,notes = "根据客户联系人获取已开启潜在顾客")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/contacts/{contact_id}/leads/fetchon")
	public ResponseEntity<List<LeadDTO>> fetchLeadOnByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id,LeadSearchContext context) {
        context.setN_parentcontactid_eq(contact_id);
        Page<Lead> domains = leadService.searchOn(context) ;
        List<LeadDTO> list = leadMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lead-searchOn-all') and hasPermission(#context,'iBizBusinessCentral-Lead-Get')")
	@ApiOperation(value = "根据客户联系人查询已开启潜在顾客", tags = {"潜在顾客" } ,notes = "根据客户联系人查询已开启潜在顾客")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/contacts/{contact_id}/leads/searchon")
	public ResponseEntity<Page<LeadDTO>> searchLeadOnByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @RequestBody LeadSearchContext context) {
        context.setN_parentcontactid_eq(contact_id);
        Page<Lead> domains = leadService.searchOn(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(leadMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

