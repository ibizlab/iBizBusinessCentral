package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.marketing.domain.ListAccount;
import cn.ibizlab.businesscentral.core.marketing.service.IListAccountService;
import cn.ibizlab.businesscentral.core.marketing.filter.ListAccountSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"营销列表-账户" })
@RestController("CentralApi-listaccount")
@RequestMapping("")
public class ListAccountResource {

    @Autowired
    public IListAccountService listaccountService;

    @Autowired
    @Lazy
    public ListAccountMapping listaccountMapping;

    @PreAuthorize("hasPermission(this.listaccountMapping.toDomain(#listaccountdto),'iBizBusinessCentral-ListAccount-Create')")
    @ApiOperation(value = "新建营销列表-账户", tags = {"营销列表-账户" },  notes = "新建营销列表-账户")
	@RequestMapping(method = RequestMethod.POST, value = "/listaccounts")
    public ResponseEntity<ListAccountDTO> create(@RequestBody ListAccountDTO listaccountdto) {
        ListAccount domain = listaccountMapping.toDomain(listaccountdto);
		listaccountService.create(domain);
        ListAccountDTO dto = listaccountMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.listaccountMapping.toDomain(#listaccountdtos),'iBizBusinessCentral-ListAccount-Create')")
    @ApiOperation(value = "批量新建营销列表-账户", tags = {"营销列表-账户" },  notes = "批量新建营销列表-账户")
	@RequestMapping(method = RequestMethod.POST, value = "/listaccounts/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<ListAccountDTO> listaccountdtos) {
        listaccountService.createBatch(listaccountMapping.toDomain(listaccountdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "listaccount" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.listaccountService.get(#listaccount_id),'iBizBusinessCentral-ListAccount-Update')")
    @ApiOperation(value = "更新营销列表-账户", tags = {"营销列表-账户" },  notes = "更新营销列表-账户")
	@RequestMapping(method = RequestMethod.PUT, value = "/listaccounts/{listaccount_id}")
    public ResponseEntity<ListAccountDTO> update(@PathVariable("listaccount_id") String listaccount_id, @RequestBody ListAccountDTO listaccountdto) {
		ListAccount domain  = listaccountMapping.toDomain(listaccountdto);
        domain .setRelationshipsid(listaccount_id);
		listaccountService.update(domain );
		ListAccountDTO dto = listaccountMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.listaccountService.getListaccountByEntities(this.listaccountMapping.toDomain(#listaccountdtos)),'iBizBusinessCentral-ListAccount-Update')")
    @ApiOperation(value = "批量更新营销列表-账户", tags = {"营销列表-账户" },  notes = "批量更新营销列表-账户")
	@RequestMapping(method = RequestMethod.PUT, value = "/listaccounts/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<ListAccountDTO> listaccountdtos) {
        listaccountService.updateBatch(listaccountMapping.toDomain(listaccountdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.listaccountService.get(#listaccount_id),'iBizBusinessCentral-ListAccount-Remove')")
    @ApiOperation(value = "删除营销列表-账户", tags = {"营销列表-账户" },  notes = "删除营销列表-账户")
	@RequestMapping(method = RequestMethod.DELETE, value = "/listaccounts/{listaccount_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("listaccount_id") String listaccount_id) {
         return ResponseEntity.status(HttpStatus.OK).body(listaccountService.remove(listaccount_id));
    }

    @PreAuthorize("hasPermission(this.listaccountService.getListaccountByIds(#ids),'iBizBusinessCentral-ListAccount-Remove')")
    @ApiOperation(value = "批量删除营销列表-账户", tags = {"营销列表-账户" },  notes = "批量删除营销列表-账户")
	@RequestMapping(method = RequestMethod.DELETE, value = "/listaccounts/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        listaccountService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.listaccountMapping.toDomain(returnObject.body),'iBizBusinessCentral-ListAccount-Get')")
    @ApiOperation(value = "获取营销列表-账户", tags = {"营销列表-账户" },  notes = "获取营销列表-账户")
	@RequestMapping(method = RequestMethod.GET, value = "/listaccounts/{listaccount_id}")
    public ResponseEntity<ListAccountDTO> get(@PathVariable("listaccount_id") String listaccount_id) {
        ListAccount domain = listaccountService.get(listaccount_id);
        ListAccountDTO dto = listaccountMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取营销列表-账户草稿", tags = {"营销列表-账户" },  notes = "获取营销列表-账户草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/listaccounts/getdraft")
    public ResponseEntity<ListAccountDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(listaccountMapping.toDto(listaccountService.getDraft(new ListAccount())));
    }

    @ApiOperation(value = "检查营销列表-账户", tags = {"营销列表-账户" },  notes = "检查营销列表-账户")
	@RequestMapping(method = RequestMethod.POST, value = "/listaccounts/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody ListAccountDTO listaccountdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(listaccountService.checkKey(listaccountMapping.toDomain(listaccountdto)));
    }

    @PreAuthorize("hasPermission(this.listaccountMapping.toDomain(#listaccountdto),'iBizBusinessCentral-ListAccount-Save')")
    @ApiOperation(value = "保存营销列表-账户", tags = {"营销列表-账户" },  notes = "保存营销列表-账户")
	@RequestMapping(method = RequestMethod.POST, value = "/listaccounts/save")
    public ResponseEntity<Boolean> save(@RequestBody ListAccountDTO listaccountdto) {
        return ResponseEntity.status(HttpStatus.OK).body(listaccountService.save(listaccountMapping.toDomain(listaccountdto)));
    }

    @PreAuthorize("hasPermission(this.listaccountMapping.toDomain(#listaccountdtos),'iBizBusinessCentral-ListAccount-Save')")
    @ApiOperation(value = "批量保存营销列表-账户", tags = {"营销列表-账户" },  notes = "批量保存营销列表-账户")
	@RequestMapping(method = RequestMethod.POST, value = "/listaccounts/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<ListAccountDTO> listaccountdtos) {
        listaccountService.saveBatch(listaccountMapping.toDomain(listaccountdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ListAccount-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-ListAccount-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"营销列表-账户" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/listaccounts/fetchdefault")
	public ResponseEntity<List<ListAccountDTO>> fetchDefault(ListAccountSearchContext context) {
        Page<ListAccount> domains = listaccountService.searchDefault(context) ;
        List<ListAccountDTO> list = listaccountMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ListAccount-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-ListAccount-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"营销列表-账户" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/listaccounts/searchdefault")
	public ResponseEntity<Page<ListAccountDTO>> searchDefault(@RequestBody ListAccountSearchContext context) {
        Page<ListAccount> domains = listaccountService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(listaccountMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.listaccountMapping.toDomain(#listaccountdto),'iBizBusinessCentral-ListAccount-Create')")
    @ApiOperation(value = "根据客户建立营销列表-账户", tags = {"营销列表-账户" },  notes = "根据客户建立营销列表-账户")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/listaccounts")
    public ResponseEntity<ListAccountDTO> createByAccount(@PathVariable("account_id") String account_id, @RequestBody ListAccountDTO listaccountdto) {
        ListAccount domain = listaccountMapping.toDomain(listaccountdto);
        domain.setEntity2id(account_id);
		listaccountService.create(domain);
        ListAccountDTO dto = listaccountMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.listaccountMapping.toDomain(#listaccountdtos),'iBizBusinessCentral-ListAccount-Create')")
    @ApiOperation(value = "根据客户批量建立营销列表-账户", tags = {"营销列表-账户" },  notes = "根据客户批量建立营销列表-账户")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/listaccounts/batch")
    public ResponseEntity<Boolean> createBatchByAccount(@PathVariable("account_id") String account_id, @RequestBody List<ListAccountDTO> listaccountdtos) {
        List<ListAccount> domainlist=listaccountMapping.toDomain(listaccountdtos);
        for(ListAccount domain:domainlist){
            domain.setEntity2id(account_id);
        }
        listaccountService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "listaccount" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.listaccountService.get(#listaccount_id),'iBizBusinessCentral-ListAccount-Update')")
    @ApiOperation(value = "根据客户更新营销列表-账户", tags = {"营销列表-账户" },  notes = "根据客户更新营销列表-账户")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/listaccounts/{listaccount_id}")
    public ResponseEntity<ListAccountDTO> updateByAccount(@PathVariable("account_id") String account_id, @PathVariable("listaccount_id") String listaccount_id, @RequestBody ListAccountDTO listaccountdto) {
        ListAccount domain = listaccountMapping.toDomain(listaccountdto);
        domain.setEntity2id(account_id);
        domain.setRelationshipsid(listaccount_id);
		listaccountService.update(domain);
        ListAccountDTO dto = listaccountMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.listaccountService.getListaccountByEntities(this.listaccountMapping.toDomain(#listaccountdtos)),'iBizBusinessCentral-ListAccount-Update')")
    @ApiOperation(value = "根据客户批量更新营销列表-账户", tags = {"营销列表-账户" },  notes = "根据客户批量更新营销列表-账户")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/listaccounts/batch")
    public ResponseEntity<Boolean> updateBatchByAccount(@PathVariable("account_id") String account_id, @RequestBody List<ListAccountDTO> listaccountdtos) {
        List<ListAccount> domainlist=listaccountMapping.toDomain(listaccountdtos);
        for(ListAccount domain:domainlist){
            domain.setEntity2id(account_id);
        }
        listaccountService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.listaccountService.get(#listaccount_id),'iBizBusinessCentral-ListAccount-Remove')")
    @ApiOperation(value = "根据客户删除营销列表-账户", tags = {"营销列表-账户" },  notes = "根据客户删除营销列表-账户")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/listaccounts/{listaccount_id}")
    public ResponseEntity<Boolean> removeByAccount(@PathVariable("account_id") String account_id, @PathVariable("listaccount_id") String listaccount_id) {
		return ResponseEntity.status(HttpStatus.OK).body(listaccountService.remove(listaccount_id));
    }

    @PreAuthorize("hasPermission(this.listaccountService.getListaccountByIds(#ids),'iBizBusinessCentral-ListAccount-Remove')")
    @ApiOperation(value = "根据客户批量删除营销列表-账户", tags = {"营销列表-账户" },  notes = "根据客户批量删除营销列表-账户")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/listaccounts/batch")
    public ResponseEntity<Boolean> removeBatchByAccount(@RequestBody List<String> ids) {
        listaccountService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.listaccountMapping.toDomain(returnObject.body),'iBizBusinessCentral-ListAccount-Get')")
    @ApiOperation(value = "根据客户获取营销列表-账户", tags = {"营销列表-账户" },  notes = "根据客户获取营销列表-账户")
	@RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/listaccounts/{listaccount_id}")
    public ResponseEntity<ListAccountDTO> getByAccount(@PathVariable("account_id") String account_id, @PathVariable("listaccount_id") String listaccount_id) {
        ListAccount domain = listaccountService.get(listaccount_id);
        ListAccountDTO dto = listaccountMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据客户获取营销列表-账户草稿", tags = {"营销列表-账户" },  notes = "根据客户获取营销列表-账户草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/listaccounts/getdraft")
    public ResponseEntity<ListAccountDTO> getDraftByAccount(@PathVariable("account_id") String account_id) {
        ListAccount domain = new ListAccount();
        domain.setEntity2id(account_id);
        return ResponseEntity.status(HttpStatus.OK).body(listaccountMapping.toDto(listaccountService.getDraft(domain)));
    }

    @ApiOperation(value = "根据客户检查营销列表-账户", tags = {"营销列表-账户" },  notes = "根据客户检查营销列表-账户")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/listaccounts/checkkey")
    public ResponseEntity<Boolean> checkKeyByAccount(@PathVariable("account_id") String account_id, @RequestBody ListAccountDTO listaccountdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(listaccountService.checkKey(listaccountMapping.toDomain(listaccountdto)));
    }

    @PreAuthorize("hasPermission(this.listaccountMapping.toDomain(#listaccountdto),'iBizBusinessCentral-ListAccount-Save')")
    @ApiOperation(value = "根据客户保存营销列表-账户", tags = {"营销列表-账户" },  notes = "根据客户保存营销列表-账户")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/listaccounts/save")
    public ResponseEntity<Boolean> saveByAccount(@PathVariable("account_id") String account_id, @RequestBody ListAccountDTO listaccountdto) {
        ListAccount domain = listaccountMapping.toDomain(listaccountdto);
        domain.setEntity2id(account_id);
        return ResponseEntity.status(HttpStatus.OK).body(listaccountService.save(domain));
    }

    @PreAuthorize("hasPermission(this.listaccountMapping.toDomain(#listaccountdtos),'iBizBusinessCentral-ListAccount-Save')")
    @ApiOperation(value = "根据客户批量保存营销列表-账户", tags = {"营销列表-账户" },  notes = "根据客户批量保存营销列表-账户")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/listaccounts/savebatch")
    public ResponseEntity<Boolean> saveBatchByAccount(@PathVariable("account_id") String account_id, @RequestBody List<ListAccountDTO> listaccountdtos) {
        List<ListAccount> domainlist=listaccountMapping.toDomain(listaccountdtos);
        for(ListAccount domain:domainlist){
             domain.setEntity2id(account_id);
        }
        listaccountService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ListAccount-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-ListAccount-Get')")
	@ApiOperation(value = "根据客户获取DEFAULT", tags = {"营销列表-账户" } ,notes = "根据客户获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/listaccounts/fetchdefault")
	public ResponseEntity<List<ListAccountDTO>> fetchListAccountDefaultByAccount(@PathVariable("account_id") String account_id,ListAccountSearchContext context) {
        context.setN_entity2id_eq(account_id);
        Page<ListAccount> domains = listaccountService.searchDefault(context) ;
        List<ListAccountDTO> list = listaccountMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ListAccount-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-ListAccount-Get')")
	@ApiOperation(value = "根据客户查询DEFAULT", tags = {"营销列表-账户" } ,notes = "根据客户查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/listaccounts/searchdefault")
	public ResponseEntity<Page<ListAccountDTO>> searchListAccountDefaultByAccount(@PathVariable("account_id") String account_id, @RequestBody ListAccountSearchContext context) {
        context.setN_entity2id_eq(account_id);
        Page<ListAccount> domains = listaccountService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(listaccountMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.listaccountMapping.toDomain(#listaccountdto),'iBizBusinessCentral-ListAccount-Create')")
    @ApiOperation(value = "根据市场营销列表建立营销列表-账户", tags = {"营销列表-账户" },  notes = "根据市场营销列表建立营销列表-账户")
	@RequestMapping(method = RequestMethod.POST, value = "/ibizlists/{ibizlist_id}/listaccounts")
    public ResponseEntity<ListAccountDTO> createByIBizList(@PathVariable("ibizlist_id") String ibizlist_id, @RequestBody ListAccountDTO listaccountdto) {
        ListAccount domain = listaccountMapping.toDomain(listaccountdto);
        domain.setEntityid(ibizlist_id);
		listaccountService.create(domain);
        ListAccountDTO dto = listaccountMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.listaccountMapping.toDomain(#listaccountdtos),'iBizBusinessCentral-ListAccount-Create')")
    @ApiOperation(value = "根据市场营销列表批量建立营销列表-账户", tags = {"营销列表-账户" },  notes = "根据市场营销列表批量建立营销列表-账户")
	@RequestMapping(method = RequestMethod.POST, value = "/ibizlists/{ibizlist_id}/listaccounts/batch")
    public ResponseEntity<Boolean> createBatchByIBizList(@PathVariable("ibizlist_id") String ibizlist_id, @RequestBody List<ListAccountDTO> listaccountdtos) {
        List<ListAccount> domainlist=listaccountMapping.toDomain(listaccountdtos);
        for(ListAccount domain:domainlist){
            domain.setEntityid(ibizlist_id);
        }
        listaccountService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "listaccount" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.listaccountService.get(#listaccount_id),'iBizBusinessCentral-ListAccount-Update')")
    @ApiOperation(value = "根据市场营销列表更新营销列表-账户", tags = {"营销列表-账户" },  notes = "根据市场营销列表更新营销列表-账户")
	@RequestMapping(method = RequestMethod.PUT, value = "/ibizlists/{ibizlist_id}/listaccounts/{listaccount_id}")
    public ResponseEntity<ListAccountDTO> updateByIBizList(@PathVariable("ibizlist_id") String ibizlist_id, @PathVariable("listaccount_id") String listaccount_id, @RequestBody ListAccountDTO listaccountdto) {
        ListAccount domain = listaccountMapping.toDomain(listaccountdto);
        domain.setEntityid(ibizlist_id);
        domain.setRelationshipsid(listaccount_id);
		listaccountService.update(domain);
        ListAccountDTO dto = listaccountMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.listaccountService.getListaccountByEntities(this.listaccountMapping.toDomain(#listaccountdtos)),'iBizBusinessCentral-ListAccount-Update')")
    @ApiOperation(value = "根据市场营销列表批量更新营销列表-账户", tags = {"营销列表-账户" },  notes = "根据市场营销列表批量更新营销列表-账户")
	@RequestMapping(method = RequestMethod.PUT, value = "/ibizlists/{ibizlist_id}/listaccounts/batch")
    public ResponseEntity<Boolean> updateBatchByIBizList(@PathVariable("ibizlist_id") String ibizlist_id, @RequestBody List<ListAccountDTO> listaccountdtos) {
        List<ListAccount> domainlist=listaccountMapping.toDomain(listaccountdtos);
        for(ListAccount domain:domainlist){
            domain.setEntityid(ibizlist_id);
        }
        listaccountService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.listaccountService.get(#listaccount_id),'iBizBusinessCentral-ListAccount-Remove')")
    @ApiOperation(value = "根据市场营销列表删除营销列表-账户", tags = {"营销列表-账户" },  notes = "根据市场营销列表删除营销列表-账户")
	@RequestMapping(method = RequestMethod.DELETE, value = "/ibizlists/{ibizlist_id}/listaccounts/{listaccount_id}")
    public ResponseEntity<Boolean> removeByIBizList(@PathVariable("ibizlist_id") String ibizlist_id, @PathVariable("listaccount_id") String listaccount_id) {
		return ResponseEntity.status(HttpStatus.OK).body(listaccountService.remove(listaccount_id));
    }

    @PreAuthorize("hasPermission(this.listaccountService.getListaccountByIds(#ids),'iBizBusinessCentral-ListAccount-Remove')")
    @ApiOperation(value = "根据市场营销列表批量删除营销列表-账户", tags = {"营销列表-账户" },  notes = "根据市场营销列表批量删除营销列表-账户")
	@RequestMapping(method = RequestMethod.DELETE, value = "/ibizlists/{ibizlist_id}/listaccounts/batch")
    public ResponseEntity<Boolean> removeBatchByIBizList(@RequestBody List<String> ids) {
        listaccountService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.listaccountMapping.toDomain(returnObject.body),'iBizBusinessCentral-ListAccount-Get')")
    @ApiOperation(value = "根据市场营销列表获取营销列表-账户", tags = {"营销列表-账户" },  notes = "根据市场营销列表获取营销列表-账户")
	@RequestMapping(method = RequestMethod.GET, value = "/ibizlists/{ibizlist_id}/listaccounts/{listaccount_id}")
    public ResponseEntity<ListAccountDTO> getByIBizList(@PathVariable("ibizlist_id") String ibizlist_id, @PathVariable("listaccount_id") String listaccount_id) {
        ListAccount domain = listaccountService.get(listaccount_id);
        ListAccountDTO dto = listaccountMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据市场营销列表获取营销列表-账户草稿", tags = {"营销列表-账户" },  notes = "根据市场营销列表获取营销列表-账户草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/ibizlists/{ibizlist_id}/listaccounts/getdraft")
    public ResponseEntity<ListAccountDTO> getDraftByIBizList(@PathVariable("ibizlist_id") String ibizlist_id) {
        ListAccount domain = new ListAccount();
        domain.setEntityid(ibizlist_id);
        return ResponseEntity.status(HttpStatus.OK).body(listaccountMapping.toDto(listaccountService.getDraft(domain)));
    }

    @ApiOperation(value = "根据市场营销列表检查营销列表-账户", tags = {"营销列表-账户" },  notes = "根据市场营销列表检查营销列表-账户")
	@RequestMapping(method = RequestMethod.POST, value = "/ibizlists/{ibizlist_id}/listaccounts/checkkey")
    public ResponseEntity<Boolean> checkKeyByIBizList(@PathVariable("ibizlist_id") String ibizlist_id, @RequestBody ListAccountDTO listaccountdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(listaccountService.checkKey(listaccountMapping.toDomain(listaccountdto)));
    }

    @PreAuthorize("hasPermission(this.listaccountMapping.toDomain(#listaccountdto),'iBizBusinessCentral-ListAccount-Save')")
    @ApiOperation(value = "根据市场营销列表保存营销列表-账户", tags = {"营销列表-账户" },  notes = "根据市场营销列表保存营销列表-账户")
	@RequestMapping(method = RequestMethod.POST, value = "/ibizlists/{ibizlist_id}/listaccounts/save")
    public ResponseEntity<Boolean> saveByIBizList(@PathVariable("ibizlist_id") String ibizlist_id, @RequestBody ListAccountDTO listaccountdto) {
        ListAccount domain = listaccountMapping.toDomain(listaccountdto);
        domain.setEntityid(ibizlist_id);
        return ResponseEntity.status(HttpStatus.OK).body(listaccountService.save(domain));
    }

    @PreAuthorize("hasPermission(this.listaccountMapping.toDomain(#listaccountdtos),'iBizBusinessCentral-ListAccount-Save')")
    @ApiOperation(value = "根据市场营销列表批量保存营销列表-账户", tags = {"营销列表-账户" },  notes = "根据市场营销列表批量保存营销列表-账户")
	@RequestMapping(method = RequestMethod.POST, value = "/ibizlists/{ibizlist_id}/listaccounts/savebatch")
    public ResponseEntity<Boolean> saveBatchByIBizList(@PathVariable("ibizlist_id") String ibizlist_id, @RequestBody List<ListAccountDTO> listaccountdtos) {
        List<ListAccount> domainlist=listaccountMapping.toDomain(listaccountdtos);
        for(ListAccount domain:domainlist){
             domain.setEntityid(ibizlist_id);
        }
        listaccountService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ListAccount-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-ListAccount-Get')")
	@ApiOperation(value = "根据市场营销列表获取DEFAULT", tags = {"营销列表-账户" } ,notes = "根据市场营销列表获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/ibizlists/{ibizlist_id}/listaccounts/fetchdefault")
	public ResponseEntity<List<ListAccountDTO>> fetchListAccountDefaultByIBizList(@PathVariable("ibizlist_id") String ibizlist_id,ListAccountSearchContext context) {
        context.setN_entityid_eq(ibizlist_id);
        Page<ListAccount> domains = listaccountService.searchDefault(context) ;
        List<ListAccountDTO> list = listaccountMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ListAccount-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-ListAccount-Get')")
	@ApiOperation(value = "根据市场营销列表查询DEFAULT", tags = {"营销列表-账户" } ,notes = "根据市场营销列表查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/ibizlists/{ibizlist_id}/listaccounts/searchdefault")
	public ResponseEntity<Page<ListAccountDTO>> searchListAccountDefaultByIBizList(@PathVariable("ibizlist_id") String ibizlist_id, @RequestBody ListAccountSearchContext context) {
        context.setN_entityid_eq(ibizlist_id);
        Page<ListAccount> domains = listaccountService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(listaccountMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

