package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.sales.domain.OpportunityProduct;
import cn.ibizlab.businesscentral.core.sales.service.IOpportunityProductService;
import cn.ibizlab.businesscentral.core.sales.filter.OpportunityProductSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"商机产品" })
@RestController("CentralApi-opportunityproduct")
@RequestMapping("")
public class OpportunityProductResource {

    @Autowired
    public IOpportunityProductService opportunityproductService;

    @Autowired
    @Lazy
    public OpportunityProductMapping opportunityproductMapping;

    @PreAuthorize("hasPermission(this.opportunityproductMapping.toDomain(#opportunityproductdto),'iBizBusinessCentral-OpportunityProduct-Create')")
    @ApiOperation(value = "新建商机产品", tags = {"商机产品" },  notes = "新建商机产品")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunityproducts")
    public ResponseEntity<OpportunityProductDTO> create(@RequestBody OpportunityProductDTO opportunityproductdto) {
        OpportunityProduct domain = opportunityproductMapping.toDomain(opportunityproductdto);
		opportunityproductService.create(domain);
        OpportunityProductDTO dto = opportunityproductMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.opportunityproductMapping.toDomain(#opportunityproductdtos),'iBizBusinessCentral-OpportunityProduct-Create')")
    @ApiOperation(value = "批量新建商机产品", tags = {"商机产品" },  notes = "批量新建商机产品")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunityproducts/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<OpportunityProductDTO> opportunityproductdtos) {
        opportunityproductService.createBatch(opportunityproductMapping.toDomain(opportunityproductdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "opportunityproduct" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.opportunityproductService.get(#opportunityproduct_id),'iBizBusinessCentral-OpportunityProduct-Update')")
    @ApiOperation(value = "更新商机产品", tags = {"商机产品" },  notes = "更新商机产品")
	@RequestMapping(method = RequestMethod.PUT, value = "/opportunityproducts/{opportunityproduct_id}")
    public ResponseEntity<OpportunityProductDTO> update(@PathVariable("opportunityproduct_id") String opportunityproduct_id, @RequestBody OpportunityProductDTO opportunityproductdto) {
		OpportunityProduct domain  = opportunityproductMapping.toDomain(opportunityproductdto);
        domain .setOpportunityproductid(opportunityproduct_id);
		opportunityproductService.update(domain );
		OpportunityProductDTO dto = opportunityproductMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.opportunityproductService.getOpportunityproductByEntities(this.opportunityproductMapping.toDomain(#opportunityproductdtos)),'iBizBusinessCentral-OpportunityProduct-Update')")
    @ApiOperation(value = "批量更新商机产品", tags = {"商机产品" },  notes = "批量更新商机产品")
	@RequestMapping(method = RequestMethod.PUT, value = "/opportunityproducts/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<OpportunityProductDTO> opportunityproductdtos) {
        opportunityproductService.updateBatch(opportunityproductMapping.toDomain(opportunityproductdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.opportunityproductService.get(#opportunityproduct_id),'iBizBusinessCentral-OpportunityProduct-Remove')")
    @ApiOperation(value = "删除商机产品", tags = {"商机产品" },  notes = "删除商机产品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/opportunityproducts/{opportunityproduct_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("opportunityproduct_id") String opportunityproduct_id) {
         return ResponseEntity.status(HttpStatus.OK).body(opportunityproductService.remove(opportunityproduct_id));
    }

    @PreAuthorize("hasPermission(this.opportunityproductService.getOpportunityproductByIds(#ids),'iBizBusinessCentral-OpportunityProduct-Remove')")
    @ApiOperation(value = "批量删除商机产品", tags = {"商机产品" },  notes = "批量删除商机产品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/opportunityproducts/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        opportunityproductService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.opportunityproductMapping.toDomain(returnObject.body),'iBizBusinessCentral-OpportunityProduct-Get')")
    @ApiOperation(value = "获取商机产品", tags = {"商机产品" },  notes = "获取商机产品")
	@RequestMapping(method = RequestMethod.GET, value = "/opportunityproducts/{opportunityproduct_id}")
    public ResponseEntity<OpportunityProductDTO> get(@PathVariable("opportunityproduct_id") String opportunityproduct_id) {
        OpportunityProduct domain = opportunityproductService.get(opportunityproduct_id);
        OpportunityProductDTO dto = opportunityproductMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取商机产品草稿", tags = {"商机产品" },  notes = "获取商机产品草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/opportunityproducts/getdraft")
    public ResponseEntity<OpportunityProductDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(opportunityproductMapping.toDto(opportunityproductService.getDraft(new OpportunityProduct())));
    }

    @ApiOperation(value = "检查商机产品", tags = {"商机产品" },  notes = "检查商机产品")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunityproducts/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody OpportunityProductDTO opportunityproductdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(opportunityproductService.checkKey(opportunityproductMapping.toDomain(opportunityproductdto)));
    }

    @PreAuthorize("hasPermission(this.opportunityproductMapping.toDomain(#opportunityproductdto),'iBizBusinessCentral-OpportunityProduct-Save')")
    @ApiOperation(value = "保存商机产品", tags = {"商机产品" },  notes = "保存商机产品")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunityproducts/save")
    public ResponseEntity<Boolean> save(@RequestBody OpportunityProductDTO opportunityproductdto) {
        return ResponseEntity.status(HttpStatus.OK).body(opportunityproductService.save(opportunityproductMapping.toDomain(opportunityproductdto)));
    }

    @PreAuthorize("hasPermission(this.opportunityproductMapping.toDomain(#opportunityproductdtos),'iBizBusinessCentral-OpportunityProduct-Save')")
    @ApiOperation(value = "批量保存商机产品", tags = {"商机产品" },  notes = "批量保存商机产品")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunityproducts/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<OpportunityProductDTO> opportunityproductdtos) {
        opportunityproductService.saveBatch(opportunityproductMapping.toDomain(opportunityproductdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OpportunityProduct-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-OpportunityProduct-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"商机产品" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/opportunityproducts/fetchdefault")
	public ResponseEntity<List<OpportunityProductDTO>> fetchDefault(OpportunityProductSearchContext context) {
        Page<OpportunityProduct> domains = opportunityproductService.searchDefault(context) ;
        List<OpportunityProductDTO> list = opportunityproductMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OpportunityProduct-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-OpportunityProduct-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"商机产品" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/opportunityproducts/searchdefault")
	public ResponseEntity<Page<OpportunityProductDTO>> searchDefault(@RequestBody OpportunityProductSearchContext context) {
        Page<OpportunityProduct> domains = opportunityproductService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(opportunityproductMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.opportunityproductMapping.toDomain(#opportunityproductdto),'iBizBusinessCentral-OpportunityProduct-Create')")
    @ApiOperation(value = "根据商机建立商机产品", tags = {"商机产品" },  notes = "根据商机建立商机产品")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/opportunityproducts")
    public ResponseEntity<OpportunityProductDTO> createByOpportunity(@PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityProductDTO opportunityproductdto) {
        OpportunityProduct domain = opportunityproductMapping.toDomain(opportunityproductdto);
        domain.setOpportunityid(opportunity_id);
		opportunityproductService.create(domain);
        OpportunityProductDTO dto = opportunityproductMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.opportunityproductMapping.toDomain(#opportunityproductdtos),'iBizBusinessCentral-OpportunityProduct-Create')")
    @ApiOperation(value = "根据商机批量建立商机产品", tags = {"商机产品" },  notes = "根据商机批量建立商机产品")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/opportunityproducts/batch")
    public ResponseEntity<Boolean> createBatchByOpportunity(@PathVariable("opportunity_id") String opportunity_id, @RequestBody List<OpportunityProductDTO> opportunityproductdtos) {
        List<OpportunityProduct> domainlist=opportunityproductMapping.toDomain(opportunityproductdtos);
        for(OpportunityProduct domain:domainlist){
            domain.setOpportunityid(opportunity_id);
        }
        opportunityproductService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "opportunityproduct" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.opportunityproductService.get(#opportunityproduct_id),'iBizBusinessCentral-OpportunityProduct-Update')")
    @ApiOperation(value = "根据商机更新商机产品", tags = {"商机产品" },  notes = "根据商机更新商机产品")
	@RequestMapping(method = RequestMethod.PUT, value = "/opportunities/{opportunity_id}/opportunityproducts/{opportunityproduct_id}")
    public ResponseEntity<OpportunityProductDTO> updateByOpportunity(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("opportunityproduct_id") String opportunityproduct_id, @RequestBody OpportunityProductDTO opportunityproductdto) {
        OpportunityProduct domain = opportunityproductMapping.toDomain(opportunityproductdto);
        domain.setOpportunityid(opportunity_id);
        domain.setOpportunityproductid(opportunityproduct_id);
		opportunityproductService.update(domain);
        OpportunityProductDTO dto = opportunityproductMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.opportunityproductService.getOpportunityproductByEntities(this.opportunityproductMapping.toDomain(#opportunityproductdtos)),'iBizBusinessCentral-OpportunityProduct-Update')")
    @ApiOperation(value = "根据商机批量更新商机产品", tags = {"商机产品" },  notes = "根据商机批量更新商机产品")
	@RequestMapping(method = RequestMethod.PUT, value = "/opportunities/{opportunity_id}/opportunityproducts/batch")
    public ResponseEntity<Boolean> updateBatchByOpportunity(@PathVariable("opportunity_id") String opportunity_id, @RequestBody List<OpportunityProductDTO> opportunityproductdtos) {
        List<OpportunityProduct> domainlist=opportunityproductMapping.toDomain(opportunityproductdtos);
        for(OpportunityProduct domain:domainlist){
            domain.setOpportunityid(opportunity_id);
        }
        opportunityproductService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.opportunityproductService.get(#opportunityproduct_id),'iBizBusinessCentral-OpportunityProduct-Remove')")
    @ApiOperation(value = "根据商机删除商机产品", tags = {"商机产品" },  notes = "根据商机删除商机产品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/opportunities/{opportunity_id}/opportunityproducts/{opportunityproduct_id}")
    public ResponseEntity<Boolean> removeByOpportunity(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("opportunityproduct_id") String opportunityproduct_id) {
		return ResponseEntity.status(HttpStatus.OK).body(opportunityproductService.remove(opportunityproduct_id));
    }

    @PreAuthorize("hasPermission(this.opportunityproductService.getOpportunityproductByIds(#ids),'iBizBusinessCentral-OpportunityProduct-Remove')")
    @ApiOperation(value = "根据商机批量删除商机产品", tags = {"商机产品" },  notes = "根据商机批量删除商机产品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/opportunities/{opportunity_id}/opportunityproducts/batch")
    public ResponseEntity<Boolean> removeBatchByOpportunity(@RequestBody List<String> ids) {
        opportunityproductService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.opportunityproductMapping.toDomain(returnObject.body),'iBizBusinessCentral-OpportunityProduct-Get')")
    @ApiOperation(value = "根据商机获取商机产品", tags = {"商机产品" },  notes = "根据商机获取商机产品")
	@RequestMapping(method = RequestMethod.GET, value = "/opportunities/{opportunity_id}/opportunityproducts/{opportunityproduct_id}")
    public ResponseEntity<OpportunityProductDTO> getByOpportunity(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("opportunityproduct_id") String opportunityproduct_id) {
        OpportunityProduct domain = opportunityproductService.get(opportunityproduct_id);
        OpportunityProductDTO dto = opportunityproductMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据商机获取商机产品草稿", tags = {"商机产品" },  notes = "根据商机获取商机产品草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/opportunities/{opportunity_id}/opportunityproducts/getdraft")
    public ResponseEntity<OpportunityProductDTO> getDraftByOpportunity(@PathVariable("opportunity_id") String opportunity_id) {
        OpportunityProduct domain = new OpportunityProduct();
        domain.setOpportunityid(opportunity_id);
        return ResponseEntity.status(HttpStatus.OK).body(opportunityproductMapping.toDto(opportunityproductService.getDraft(domain)));
    }

    @ApiOperation(value = "根据商机检查商机产品", tags = {"商机产品" },  notes = "根据商机检查商机产品")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/opportunityproducts/checkkey")
    public ResponseEntity<Boolean> checkKeyByOpportunity(@PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityProductDTO opportunityproductdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(opportunityproductService.checkKey(opportunityproductMapping.toDomain(opportunityproductdto)));
    }

    @PreAuthorize("hasPermission(this.opportunityproductMapping.toDomain(#opportunityproductdto),'iBizBusinessCentral-OpportunityProduct-Save')")
    @ApiOperation(value = "根据商机保存商机产品", tags = {"商机产品" },  notes = "根据商机保存商机产品")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/opportunityproducts/save")
    public ResponseEntity<Boolean> saveByOpportunity(@PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityProductDTO opportunityproductdto) {
        OpportunityProduct domain = opportunityproductMapping.toDomain(opportunityproductdto);
        domain.setOpportunityid(opportunity_id);
        return ResponseEntity.status(HttpStatus.OK).body(opportunityproductService.save(domain));
    }

    @PreAuthorize("hasPermission(this.opportunityproductMapping.toDomain(#opportunityproductdtos),'iBizBusinessCentral-OpportunityProduct-Save')")
    @ApiOperation(value = "根据商机批量保存商机产品", tags = {"商机产品" },  notes = "根据商机批量保存商机产品")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/opportunityproducts/savebatch")
    public ResponseEntity<Boolean> saveBatchByOpportunity(@PathVariable("opportunity_id") String opportunity_id, @RequestBody List<OpportunityProductDTO> opportunityproductdtos) {
        List<OpportunityProduct> domainlist=opportunityproductMapping.toDomain(opportunityproductdtos);
        for(OpportunityProduct domain:domainlist){
             domain.setOpportunityid(opportunity_id);
        }
        opportunityproductService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OpportunityProduct-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-OpportunityProduct-Get')")
	@ApiOperation(value = "根据商机获取DEFAULT", tags = {"商机产品" } ,notes = "根据商机获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/opportunities/{opportunity_id}/opportunityproducts/fetchdefault")
	public ResponseEntity<List<OpportunityProductDTO>> fetchOpportunityProductDefaultByOpportunity(@PathVariable("opportunity_id") String opportunity_id,OpportunityProductSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<OpportunityProduct> domains = opportunityproductService.searchDefault(context) ;
        List<OpportunityProductDTO> list = opportunityproductMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OpportunityProduct-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-OpportunityProduct-Get')")
	@ApiOperation(value = "根据商机查询DEFAULT", tags = {"商机产品" } ,notes = "根据商机查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/opportunities/{opportunity_id}/opportunityproducts/searchdefault")
	public ResponseEntity<Page<OpportunityProductDTO>> searchOpportunityProductDefaultByOpportunity(@PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityProductSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<OpportunityProduct> domains = opportunityproductService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(opportunityproductMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.opportunityproductMapping.toDomain(#opportunityproductdto),'iBizBusinessCentral-OpportunityProduct-Create')")
    @ApiOperation(value = "根据客户商机建立商机产品", tags = {"商机产品" },  notes = "根据客户商机建立商机产品")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/opportunityproducts")
    public ResponseEntity<OpportunityProductDTO> createByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityProductDTO opportunityproductdto) {
        OpportunityProduct domain = opportunityproductMapping.toDomain(opportunityproductdto);
        domain.setOpportunityid(opportunity_id);
		opportunityproductService.create(domain);
        OpportunityProductDTO dto = opportunityproductMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.opportunityproductMapping.toDomain(#opportunityproductdtos),'iBizBusinessCentral-OpportunityProduct-Create')")
    @ApiOperation(value = "根据客户商机批量建立商机产品", tags = {"商机产品" },  notes = "根据客户商机批量建立商机产品")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/opportunityproducts/batch")
    public ResponseEntity<Boolean> createBatchByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody List<OpportunityProductDTO> opportunityproductdtos) {
        List<OpportunityProduct> domainlist=opportunityproductMapping.toDomain(opportunityproductdtos);
        for(OpportunityProduct domain:domainlist){
            domain.setOpportunityid(opportunity_id);
        }
        opportunityproductService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "opportunityproduct" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.opportunityproductService.get(#opportunityproduct_id),'iBizBusinessCentral-OpportunityProduct-Update')")
    @ApiOperation(value = "根据客户商机更新商机产品", tags = {"商机产品" },  notes = "根据客户商机更新商机产品")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/opportunities/{opportunity_id}/opportunityproducts/{opportunityproduct_id}")
    public ResponseEntity<OpportunityProductDTO> updateByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("opportunityproduct_id") String opportunityproduct_id, @RequestBody OpportunityProductDTO opportunityproductdto) {
        OpportunityProduct domain = opportunityproductMapping.toDomain(opportunityproductdto);
        domain.setOpportunityid(opportunity_id);
        domain.setOpportunityproductid(opportunityproduct_id);
		opportunityproductService.update(domain);
        OpportunityProductDTO dto = opportunityproductMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.opportunityproductService.getOpportunityproductByEntities(this.opportunityproductMapping.toDomain(#opportunityproductdtos)),'iBizBusinessCentral-OpportunityProduct-Update')")
    @ApiOperation(value = "根据客户商机批量更新商机产品", tags = {"商机产品" },  notes = "根据客户商机批量更新商机产品")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/opportunities/{opportunity_id}/opportunityproducts/batch")
    public ResponseEntity<Boolean> updateBatchByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody List<OpportunityProductDTO> opportunityproductdtos) {
        List<OpportunityProduct> domainlist=opportunityproductMapping.toDomain(opportunityproductdtos);
        for(OpportunityProduct domain:domainlist){
            domain.setOpportunityid(opportunity_id);
        }
        opportunityproductService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.opportunityproductService.get(#opportunityproduct_id),'iBizBusinessCentral-OpportunityProduct-Remove')")
    @ApiOperation(value = "根据客户商机删除商机产品", tags = {"商机产品" },  notes = "根据客户商机删除商机产品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/opportunities/{opportunity_id}/opportunityproducts/{opportunityproduct_id}")
    public ResponseEntity<Boolean> removeByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("opportunityproduct_id") String opportunityproduct_id) {
		return ResponseEntity.status(HttpStatus.OK).body(opportunityproductService.remove(opportunityproduct_id));
    }

    @PreAuthorize("hasPermission(this.opportunityproductService.getOpportunityproductByIds(#ids),'iBizBusinessCentral-OpportunityProduct-Remove')")
    @ApiOperation(value = "根据客户商机批量删除商机产品", tags = {"商机产品" },  notes = "根据客户商机批量删除商机产品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/opportunities/{opportunity_id}/opportunityproducts/batch")
    public ResponseEntity<Boolean> removeBatchByAccountOpportunity(@RequestBody List<String> ids) {
        opportunityproductService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.opportunityproductMapping.toDomain(returnObject.body),'iBizBusinessCentral-OpportunityProduct-Get')")
    @ApiOperation(value = "根据客户商机获取商机产品", tags = {"商机产品" },  notes = "根据客户商机获取商机产品")
	@RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/opportunities/{opportunity_id}/opportunityproducts/{opportunityproduct_id}")
    public ResponseEntity<OpportunityProductDTO> getByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("opportunityproduct_id") String opportunityproduct_id) {
        OpportunityProduct domain = opportunityproductService.get(opportunityproduct_id);
        OpportunityProductDTO dto = opportunityproductMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据客户商机获取商机产品草稿", tags = {"商机产品" },  notes = "根据客户商机获取商机产品草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/opportunities/{opportunity_id}/opportunityproducts/getdraft")
    public ResponseEntity<OpportunityProductDTO> getDraftByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id) {
        OpportunityProduct domain = new OpportunityProduct();
        domain.setOpportunityid(opportunity_id);
        return ResponseEntity.status(HttpStatus.OK).body(opportunityproductMapping.toDto(opportunityproductService.getDraft(domain)));
    }

    @ApiOperation(value = "根据客户商机检查商机产品", tags = {"商机产品" },  notes = "根据客户商机检查商机产品")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/opportunityproducts/checkkey")
    public ResponseEntity<Boolean> checkKeyByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityProductDTO opportunityproductdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(opportunityproductService.checkKey(opportunityproductMapping.toDomain(opportunityproductdto)));
    }

    @PreAuthorize("hasPermission(this.opportunityproductMapping.toDomain(#opportunityproductdto),'iBizBusinessCentral-OpportunityProduct-Save')")
    @ApiOperation(value = "根据客户商机保存商机产品", tags = {"商机产品" },  notes = "根据客户商机保存商机产品")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/opportunityproducts/save")
    public ResponseEntity<Boolean> saveByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityProductDTO opportunityproductdto) {
        OpportunityProduct domain = opportunityproductMapping.toDomain(opportunityproductdto);
        domain.setOpportunityid(opportunity_id);
        return ResponseEntity.status(HttpStatus.OK).body(opportunityproductService.save(domain));
    }

    @PreAuthorize("hasPermission(this.opportunityproductMapping.toDomain(#opportunityproductdtos),'iBizBusinessCentral-OpportunityProduct-Save')")
    @ApiOperation(value = "根据客户商机批量保存商机产品", tags = {"商机产品" },  notes = "根据客户商机批量保存商机产品")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/opportunityproducts/savebatch")
    public ResponseEntity<Boolean> saveBatchByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody List<OpportunityProductDTO> opportunityproductdtos) {
        List<OpportunityProduct> domainlist=opportunityproductMapping.toDomain(opportunityproductdtos);
        for(OpportunityProduct domain:domainlist){
             domain.setOpportunityid(opportunity_id);
        }
        opportunityproductService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OpportunityProduct-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-OpportunityProduct-Get')")
	@ApiOperation(value = "根据客户商机获取DEFAULT", tags = {"商机产品" } ,notes = "根据客户商机获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/opportunities/{opportunity_id}/opportunityproducts/fetchdefault")
	public ResponseEntity<List<OpportunityProductDTO>> fetchOpportunityProductDefaultByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id,OpportunityProductSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<OpportunityProduct> domains = opportunityproductService.searchDefault(context) ;
        List<OpportunityProductDTO> list = opportunityproductMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OpportunityProduct-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-OpportunityProduct-Get')")
	@ApiOperation(value = "根据客户商机查询DEFAULT", tags = {"商机产品" } ,notes = "根据客户商机查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/opportunities/{opportunity_id}/opportunityproducts/searchdefault")
	public ResponseEntity<Page<OpportunityProductDTO>> searchOpportunityProductDefaultByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityProductSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<OpportunityProduct> domains = opportunityproductService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(opportunityproductMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.opportunityproductMapping.toDomain(#opportunityproductdto),'iBizBusinessCentral-OpportunityProduct-Create')")
    @ApiOperation(value = "根据联系人商机建立商机产品", tags = {"商机产品" },  notes = "根据联系人商机建立商机产品")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/opportunityproducts")
    public ResponseEntity<OpportunityProductDTO> createByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityProductDTO opportunityproductdto) {
        OpportunityProduct domain = opportunityproductMapping.toDomain(opportunityproductdto);
        domain.setOpportunityid(opportunity_id);
		opportunityproductService.create(domain);
        OpportunityProductDTO dto = opportunityproductMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.opportunityproductMapping.toDomain(#opportunityproductdtos),'iBizBusinessCentral-OpportunityProduct-Create')")
    @ApiOperation(value = "根据联系人商机批量建立商机产品", tags = {"商机产品" },  notes = "根据联系人商机批量建立商机产品")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/opportunityproducts/batch")
    public ResponseEntity<Boolean> createBatchByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody List<OpportunityProductDTO> opportunityproductdtos) {
        List<OpportunityProduct> domainlist=opportunityproductMapping.toDomain(opportunityproductdtos);
        for(OpportunityProduct domain:domainlist){
            domain.setOpportunityid(opportunity_id);
        }
        opportunityproductService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "opportunityproduct" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.opportunityproductService.get(#opportunityproduct_id),'iBizBusinessCentral-OpportunityProduct-Update')")
    @ApiOperation(value = "根据联系人商机更新商机产品", tags = {"商机产品" },  notes = "根据联系人商机更新商机产品")
	@RequestMapping(method = RequestMethod.PUT, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/opportunityproducts/{opportunityproduct_id}")
    public ResponseEntity<OpportunityProductDTO> updateByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("opportunityproduct_id") String opportunityproduct_id, @RequestBody OpportunityProductDTO opportunityproductdto) {
        OpportunityProduct domain = opportunityproductMapping.toDomain(opportunityproductdto);
        domain.setOpportunityid(opportunity_id);
        domain.setOpportunityproductid(opportunityproduct_id);
		opportunityproductService.update(domain);
        OpportunityProductDTO dto = opportunityproductMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.opportunityproductService.getOpportunityproductByEntities(this.opportunityproductMapping.toDomain(#opportunityproductdtos)),'iBizBusinessCentral-OpportunityProduct-Update')")
    @ApiOperation(value = "根据联系人商机批量更新商机产品", tags = {"商机产品" },  notes = "根据联系人商机批量更新商机产品")
	@RequestMapping(method = RequestMethod.PUT, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/opportunityproducts/batch")
    public ResponseEntity<Boolean> updateBatchByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody List<OpportunityProductDTO> opportunityproductdtos) {
        List<OpportunityProduct> domainlist=opportunityproductMapping.toDomain(opportunityproductdtos);
        for(OpportunityProduct domain:domainlist){
            domain.setOpportunityid(opportunity_id);
        }
        opportunityproductService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.opportunityproductService.get(#opportunityproduct_id),'iBizBusinessCentral-OpportunityProduct-Remove')")
    @ApiOperation(value = "根据联系人商机删除商机产品", tags = {"商机产品" },  notes = "根据联系人商机删除商机产品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/opportunityproducts/{opportunityproduct_id}")
    public ResponseEntity<Boolean> removeByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("opportunityproduct_id") String opportunityproduct_id) {
		return ResponseEntity.status(HttpStatus.OK).body(opportunityproductService.remove(opportunityproduct_id));
    }

    @PreAuthorize("hasPermission(this.opportunityproductService.getOpportunityproductByIds(#ids),'iBizBusinessCentral-OpportunityProduct-Remove')")
    @ApiOperation(value = "根据联系人商机批量删除商机产品", tags = {"商机产品" },  notes = "根据联系人商机批量删除商机产品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/opportunityproducts/batch")
    public ResponseEntity<Boolean> removeBatchByContactOpportunity(@RequestBody List<String> ids) {
        opportunityproductService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.opportunityproductMapping.toDomain(returnObject.body),'iBizBusinessCentral-OpportunityProduct-Get')")
    @ApiOperation(value = "根据联系人商机获取商机产品", tags = {"商机产品" },  notes = "根据联系人商机获取商机产品")
	@RequestMapping(method = RequestMethod.GET, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/opportunityproducts/{opportunityproduct_id}")
    public ResponseEntity<OpportunityProductDTO> getByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("opportunityproduct_id") String opportunityproduct_id) {
        OpportunityProduct domain = opportunityproductService.get(opportunityproduct_id);
        OpportunityProductDTO dto = opportunityproductMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据联系人商机获取商机产品草稿", tags = {"商机产品" },  notes = "根据联系人商机获取商机产品草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/opportunityproducts/getdraft")
    public ResponseEntity<OpportunityProductDTO> getDraftByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id) {
        OpportunityProduct domain = new OpportunityProduct();
        domain.setOpportunityid(opportunity_id);
        return ResponseEntity.status(HttpStatus.OK).body(opportunityproductMapping.toDto(opportunityproductService.getDraft(domain)));
    }

    @ApiOperation(value = "根据联系人商机检查商机产品", tags = {"商机产品" },  notes = "根据联系人商机检查商机产品")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/opportunityproducts/checkkey")
    public ResponseEntity<Boolean> checkKeyByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityProductDTO opportunityproductdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(opportunityproductService.checkKey(opportunityproductMapping.toDomain(opportunityproductdto)));
    }

    @PreAuthorize("hasPermission(this.opportunityproductMapping.toDomain(#opportunityproductdto),'iBizBusinessCentral-OpportunityProduct-Save')")
    @ApiOperation(value = "根据联系人商机保存商机产品", tags = {"商机产品" },  notes = "根据联系人商机保存商机产品")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/opportunityproducts/save")
    public ResponseEntity<Boolean> saveByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityProductDTO opportunityproductdto) {
        OpportunityProduct domain = opportunityproductMapping.toDomain(opportunityproductdto);
        domain.setOpportunityid(opportunity_id);
        return ResponseEntity.status(HttpStatus.OK).body(opportunityproductService.save(domain));
    }

    @PreAuthorize("hasPermission(this.opportunityproductMapping.toDomain(#opportunityproductdtos),'iBizBusinessCentral-OpportunityProduct-Save')")
    @ApiOperation(value = "根据联系人商机批量保存商机产品", tags = {"商机产品" },  notes = "根据联系人商机批量保存商机产品")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/opportunityproducts/savebatch")
    public ResponseEntity<Boolean> saveBatchByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody List<OpportunityProductDTO> opportunityproductdtos) {
        List<OpportunityProduct> domainlist=opportunityproductMapping.toDomain(opportunityproductdtos);
        for(OpportunityProduct domain:domainlist){
             domain.setOpportunityid(opportunity_id);
        }
        opportunityproductService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OpportunityProduct-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-OpportunityProduct-Get')")
	@ApiOperation(value = "根据联系人商机获取DEFAULT", tags = {"商机产品" } ,notes = "根据联系人商机获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/contacts/{contact_id}/opportunities/{opportunity_id}/opportunityproducts/fetchdefault")
	public ResponseEntity<List<OpportunityProductDTO>> fetchOpportunityProductDefaultByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id,OpportunityProductSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<OpportunityProduct> domains = opportunityproductService.searchDefault(context) ;
        List<OpportunityProductDTO> list = opportunityproductMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OpportunityProduct-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-OpportunityProduct-Get')")
	@ApiOperation(value = "根据联系人商机查询DEFAULT", tags = {"商机产品" } ,notes = "根据联系人商机查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/contacts/{contact_id}/opportunities/{opportunity_id}/opportunityproducts/searchdefault")
	public ResponseEntity<Page<OpportunityProductDTO>> searchOpportunityProductDefaultByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityProductSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<OpportunityProduct> domains = opportunityproductService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(opportunityproductMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.opportunityproductMapping.toDomain(#opportunityproductdto),'iBizBusinessCentral-OpportunityProduct-Create')")
    @ApiOperation(value = "根据客户联系人商机建立商机产品", tags = {"商机产品" },  notes = "根据客户联系人商机建立商机产品")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/opportunityproducts")
    public ResponseEntity<OpportunityProductDTO> createByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityProductDTO opportunityproductdto) {
        OpportunityProduct domain = opportunityproductMapping.toDomain(opportunityproductdto);
        domain.setOpportunityid(opportunity_id);
		opportunityproductService.create(domain);
        OpportunityProductDTO dto = opportunityproductMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.opportunityproductMapping.toDomain(#opportunityproductdtos),'iBizBusinessCentral-OpportunityProduct-Create')")
    @ApiOperation(value = "根据客户联系人商机批量建立商机产品", tags = {"商机产品" },  notes = "根据客户联系人商机批量建立商机产品")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/opportunityproducts/batch")
    public ResponseEntity<Boolean> createBatchByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody List<OpportunityProductDTO> opportunityproductdtos) {
        List<OpportunityProduct> domainlist=opportunityproductMapping.toDomain(opportunityproductdtos);
        for(OpportunityProduct domain:domainlist){
            domain.setOpportunityid(opportunity_id);
        }
        opportunityproductService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "opportunityproduct" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.opportunityproductService.get(#opportunityproduct_id),'iBizBusinessCentral-OpportunityProduct-Update')")
    @ApiOperation(value = "根据客户联系人商机更新商机产品", tags = {"商机产品" },  notes = "根据客户联系人商机更新商机产品")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/opportunityproducts/{opportunityproduct_id}")
    public ResponseEntity<OpportunityProductDTO> updateByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("opportunityproduct_id") String opportunityproduct_id, @RequestBody OpportunityProductDTO opportunityproductdto) {
        OpportunityProduct domain = opportunityproductMapping.toDomain(opportunityproductdto);
        domain.setOpportunityid(opportunity_id);
        domain.setOpportunityproductid(opportunityproduct_id);
		opportunityproductService.update(domain);
        OpportunityProductDTO dto = opportunityproductMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.opportunityproductService.getOpportunityproductByEntities(this.opportunityproductMapping.toDomain(#opportunityproductdtos)),'iBizBusinessCentral-OpportunityProduct-Update')")
    @ApiOperation(value = "根据客户联系人商机批量更新商机产品", tags = {"商机产品" },  notes = "根据客户联系人商机批量更新商机产品")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/opportunityproducts/batch")
    public ResponseEntity<Boolean> updateBatchByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody List<OpportunityProductDTO> opportunityproductdtos) {
        List<OpportunityProduct> domainlist=opportunityproductMapping.toDomain(opportunityproductdtos);
        for(OpportunityProduct domain:domainlist){
            domain.setOpportunityid(opportunity_id);
        }
        opportunityproductService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.opportunityproductService.get(#opportunityproduct_id),'iBizBusinessCentral-OpportunityProduct-Remove')")
    @ApiOperation(value = "根据客户联系人商机删除商机产品", tags = {"商机产品" },  notes = "根据客户联系人商机删除商机产品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/opportunityproducts/{opportunityproduct_id}")
    public ResponseEntity<Boolean> removeByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("opportunityproduct_id") String opportunityproduct_id) {
		return ResponseEntity.status(HttpStatus.OK).body(opportunityproductService.remove(opportunityproduct_id));
    }

    @PreAuthorize("hasPermission(this.opportunityproductService.getOpportunityproductByIds(#ids),'iBizBusinessCentral-OpportunityProduct-Remove')")
    @ApiOperation(value = "根据客户联系人商机批量删除商机产品", tags = {"商机产品" },  notes = "根据客户联系人商机批量删除商机产品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/opportunityproducts/batch")
    public ResponseEntity<Boolean> removeBatchByAccountContactOpportunity(@RequestBody List<String> ids) {
        opportunityproductService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.opportunityproductMapping.toDomain(returnObject.body),'iBizBusinessCentral-OpportunityProduct-Get')")
    @ApiOperation(value = "根据客户联系人商机获取商机产品", tags = {"商机产品" },  notes = "根据客户联系人商机获取商机产品")
	@RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/opportunityproducts/{opportunityproduct_id}")
    public ResponseEntity<OpportunityProductDTO> getByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("opportunityproduct_id") String opportunityproduct_id) {
        OpportunityProduct domain = opportunityproductService.get(opportunityproduct_id);
        OpportunityProductDTO dto = opportunityproductMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据客户联系人商机获取商机产品草稿", tags = {"商机产品" },  notes = "根据客户联系人商机获取商机产品草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/opportunityproducts/getdraft")
    public ResponseEntity<OpportunityProductDTO> getDraftByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id) {
        OpportunityProduct domain = new OpportunityProduct();
        domain.setOpportunityid(opportunity_id);
        return ResponseEntity.status(HttpStatus.OK).body(opportunityproductMapping.toDto(opportunityproductService.getDraft(domain)));
    }

    @ApiOperation(value = "根据客户联系人商机检查商机产品", tags = {"商机产品" },  notes = "根据客户联系人商机检查商机产品")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/opportunityproducts/checkkey")
    public ResponseEntity<Boolean> checkKeyByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityProductDTO opportunityproductdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(opportunityproductService.checkKey(opportunityproductMapping.toDomain(opportunityproductdto)));
    }

    @PreAuthorize("hasPermission(this.opportunityproductMapping.toDomain(#opportunityproductdto),'iBizBusinessCentral-OpportunityProduct-Save')")
    @ApiOperation(value = "根据客户联系人商机保存商机产品", tags = {"商机产品" },  notes = "根据客户联系人商机保存商机产品")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/opportunityproducts/save")
    public ResponseEntity<Boolean> saveByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityProductDTO opportunityproductdto) {
        OpportunityProduct domain = opportunityproductMapping.toDomain(opportunityproductdto);
        domain.setOpportunityid(opportunity_id);
        return ResponseEntity.status(HttpStatus.OK).body(opportunityproductService.save(domain));
    }

    @PreAuthorize("hasPermission(this.opportunityproductMapping.toDomain(#opportunityproductdtos),'iBizBusinessCentral-OpportunityProduct-Save')")
    @ApiOperation(value = "根据客户联系人商机批量保存商机产品", tags = {"商机产品" },  notes = "根据客户联系人商机批量保存商机产品")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/opportunityproducts/savebatch")
    public ResponseEntity<Boolean> saveBatchByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody List<OpportunityProductDTO> opportunityproductdtos) {
        List<OpportunityProduct> domainlist=opportunityproductMapping.toDomain(opportunityproductdtos);
        for(OpportunityProduct domain:domainlist){
             domain.setOpportunityid(opportunity_id);
        }
        opportunityproductService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OpportunityProduct-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-OpportunityProduct-Get')")
	@ApiOperation(value = "根据客户联系人商机获取DEFAULT", tags = {"商机产品" } ,notes = "根据客户联系人商机获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/opportunityproducts/fetchdefault")
	public ResponseEntity<List<OpportunityProductDTO>> fetchOpportunityProductDefaultByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id,OpportunityProductSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<OpportunityProduct> domains = opportunityproductService.searchDefault(context) ;
        List<OpportunityProductDTO> list = opportunityproductMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OpportunityProduct-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-OpportunityProduct-Get')")
	@ApiOperation(value = "根据客户联系人商机查询DEFAULT", tags = {"商机产品" } ,notes = "根据客户联系人商机查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/opportunityproducts/searchdefault")
	public ResponseEntity<Page<OpportunityProductDTO>> searchOpportunityProductDefaultByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityProductSearchContext context) {
        context.setN_opportunityid_eq(opportunity_id);
        Page<OpportunityProduct> domains = opportunityproductService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(opportunityproductMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

