package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.marketing.domain.CampaignActivity;
import cn.ibizlab.businesscentral.core.marketing.service.ICampaignActivityService;
import cn.ibizlab.businesscentral.core.marketing.filter.CampaignActivitySearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"市场活动项目" })
@RestController("CentralApi-campaignactivity")
@RequestMapping("")
public class CampaignActivityResource {

    @Autowired
    public ICampaignActivityService campaignactivityService;

    @Autowired
    @Lazy
    public CampaignActivityMapping campaignactivityMapping;

    @PreAuthorize("hasPermission(this.campaignactivityMapping.toDomain(#campaignactivitydto),'iBizBusinessCentral-CampaignActivity-Create')")
    @ApiOperation(value = "新建市场活动项目", tags = {"市场活动项目" },  notes = "新建市场活动项目")
	@RequestMapping(method = RequestMethod.POST, value = "/campaignactivities")
    public ResponseEntity<CampaignActivityDTO> create(@RequestBody CampaignActivityDTO campaignactivitydto) {
        CampaignActivity domain = campaignactivityMapping.toDomain(campaignactivitydto);
		campaignactivityService.create(domain);
        CampaignActivityDTO dto = campaignactivityMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.campaignactivityMapping.toDomain(#campaignactivitydtos),'iBizBusinessCentral-CampaignActivity-Create')")
    @ApiOperation(value = "批量新建市场活动项目", tags = {"市场活动项目" },  notes = "批量新建市场活动项目")
	@RequestMapping(method = RequestMethod.POST, value = "/campaignactivities/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<CampaignActivityDTO> campaignactivitydtos) {
        campaignactivityService.createBatch(campaignactivityMapping.toDomain(campaignactivitydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "campaignactivity" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.campaignactivityService.get(#campaignactivity_id),'iBizBusinessCentral-CampaignActivity-Update')")
    @ApiOperation(value = "更新市场活动项目", tags = {"市场活动项目" },  notes = "更新市场活动项目")
	@RequestMapping(method = RequestMethod.PUT, value = "/campaignactivities/{campaignactivity_id}")
    public ResponseEntity<CampaignActivityDTO> update(@PathVariable("campaignactivity_id") String campaignactivity_id, @RequestBody CampaignActivityDTO campaignactivitydto) {
		CampaignActivity domain  = campaignactivityMapping.toDomain(campaignactivitydto);
        domain .setActivityid(campaignactivity_id);
		campaignactivityService.update(domain );
		CampaignActivityDTO dto = campaignactivityMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.campaignactivityService.getCampaignactivityByEntities(this.campaignactivityMapping.toDomain(#campaignactivitydtos)),'iBizBusinessCentral-CampaignActivity-Update')")
    @ApiOperation(value = "批量更新市场活动项目", tags = {"市场活动项目" },  notes = "批量更新市场活动项目")
	@RequestMapping(method = RequestMethod.PUT, value = "/campaignactivities/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<CampaignActivityDTO> campaignactivitydtos) {
        campaignactivityService.updateBatch(campaignactivityMapping.toDomain(campaignactivitydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.campaignactivityService.get(#campaignactivity_id),'iBizBusinessCentral-CampaignActivity-Remove')")
    @ApiOperation(value = "删除市场活动项目", tags = {"市场活动项目" },  notes = "删除市场活动项目")
	@RequestMapping(method = RequestMethod.DELETE, value = "/campaignactivities/{campaignactivity_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("campaignactivity_id") String campaignactivity_id) {
         return ResponseEntity.status(HttpStatus.OK).body(campaignactivityService.remove(campaignactivity_id));
    }

    @PreAuthorize("hasPermission(this.campaignactivityService.getCampaignactivityByIds(#ids),'iBizBusinessCentral-CampaignActivity-Remove')")
    @ApiOperation(value = "批量删除市场活动项目", tags = {"市场活动项目" },  notes = "批量删除市场活动项目")
	@RequestMapping(method = RequestMethod.DELETE, value = "/campaignactivities/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        campaignactivityService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.campaignactivityMapping.toDomain(returnObject.body),'iBizBusinessCentral-CampaignActivity-Get')")
    @ApiOperation(value = "获取市场活动项目", tags = {"市场活动项目" },  notes = "获取市场活动项目")
	@RequestMapping(method = RequestMethod.GET, value = "/campaignactivities/{campaignactivity_id}")
    public ResponseEntity<CampaignActivityDTO> get(@PathVariable("campaignactivity_id") String campaignactivity_id) {
        CampaignActivity domain = campaignactivityService.get(campaignactivity_id);
        CampaignActivityDTO dto = campaignactivityMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取市场活动项目草稿", tags = {"市场活动项目" },  notes = "获取市场活动项目草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/campaignactivities/getdraft")
    public ResponseEntity<CampaignActivityDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(campaignactivityMapping.toDto(campaignactivityService.getDraft(new CampaignActivity())));
    }

    @ApiOperation(value = "检查市场活动项目", tags = {"市场活动项目" },  notes = "检查市场活动项目")
	@RequestMapping(method = RequestMethod.POST, value = "/campaignactivities/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody CampaignActivityDTO campaignactivitydto) {
        return  ResponseEntity.status(HttpStatus.OK).body(campaignactivityService.checkKey(campaignactivityMapping.toDomain(campaignactivitydto)));
    }

    @PreAuthorize("hasPermission(this.campaignactivityMapping.toDomain(#campaignactivitydto),'iBizBusinessCentral-CampaignActivity-Save')")
    @ApiOperation(value = "保存市场活动项目", tags = {"市场活动项目" },  notes = "保存市场活动项目")
	@RequestMapping(method = RequestMethod.POST, value = "/campaignactivities/save")
    public ResponseEntity<Boolean> save(@RequestBody CampaignActivityDTO campaignactivitydto) {
        return ResponseEntity.status(HttpStatus.OK).body(campaignactivityService.save(campaignactivityMapping.toDomain(campaignactivitydto)));
    }

    @PreAuthorize("hasPermission(this.campaignactivityMapping.toDomain(#campaignactivitydtos),'iBizBusinessCentral-CampaignActivity-Save')")
    @ApiOperation(value = "批量保存市场活动项目", tags = {"市场活动项目" },  notes = "批量保存市场活动项目")
	@RequestMapping(method = RequestMethod.POST, value = "/campaignactivities/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<CampaignActivityDTO> campaignactivitydtos) {
        campaignactivityService.saveBatch(campaignactivityMapping.toDomain(campaignactivitydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-CampaignActivity-searchByParentKey-all') and hasPermission(#context,'iBizBusinessCentral-CampaignActivity-Get')")
	@ApiOperation(value = "获取ByParentKey", tags = {"市场活动项目" } ,notes = "获取ByParentKey")
    @RequestMapping(method= RequestMethod.GET , value="/campaignactivities/fetchbyparentkey")
	public ResponseEntity<List<CampaignActivityDTO>> fetchByParentKey(CampaignActivitySearchContext context) {
        Page<CampaignActivity> domains = campaignactivityService.searchByParentKey(context) ;
        List<CampaignActivityDTO> list = campaignactivityMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-CampaignActivity-searchByParentKey-all') and hasPermission(#context,'iBizBusinessCentral-CampaignActivity-Get')")
	@ApiOperation(value = "查询ByParentKey", tags = {"市场活动项目" } ,notes = "查询ByParentKey")
    @RequestMapping(method= RequestMethod.POST , value="/campaignactivities/searchbyparentkey")
	public ResponseEntity<Page<CampaignActivityDTO>> searchByParentKey(@RequestBody CampaignActivitySearchContext context) {
        Page<CampaignActivity> domains = campaignactivityService.searchByParentKey(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(campaignactivityMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-CampaignActivity-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-CampaignActivity-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"市场活动项目" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/campaignactivities/fetchdefault")
	public ResponseEntity<List<CampaignActivityDTO>> fetchDefault(CampaignActivitySearchContext context) {
        Page<CampaignActivity> domains = campaignactivityService.searchDefault(context) ;
        List<CampaignActivityDTO> list = campaignactivityMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-CampaignActivity-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-CampaignActivity-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"市场活动项目" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/campaignactivities/searchdefault")
	public ResponseEntity<Page<CampaignActivityDTO>> searchDefault(@RequestBody CampaignActivitySearchContext context) {
        Page<CampaignActivity> domains = campaignactivityService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(campaignactivityMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

