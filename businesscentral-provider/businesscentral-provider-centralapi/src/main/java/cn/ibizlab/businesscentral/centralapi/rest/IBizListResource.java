package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.marketing.domain.IBizList;
import cn.ibizlab.businesscentral.core.marketing.service.IIBizListService;
import cn.ibizlab.businesscentral.core.marketing.filter.IBizListSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"市场营销列表" })
@RestController("CentralApi-ibizlist")
@RequestMapping("")
public class IBizListResource {

    @Autowired
    public IIBizListService ibizlistService;

    @Autowired
    @Lazy
    public IBizListMapping ibizlistMapping;

    @PreAuthorize("hasPermission(this.ibizlistMapping.toDomain(#ibizlistdto),'iBizBusinessCentral-IBizList-Create')")
    @ApiOperation(value = "新建市场营销列表", tags = {"市场营销列表" },  notes = "新建市场营销列表")
	@RequestMapping(method = RequestMethod.POST, value = "/ibizlists")
    public ResponseEntity<IBizListDTO> create(@RequestBody IBizListDTO ibizlistdto) {
        IBizList domain = ibizlistMapping.toDomain(ibizlistdto);
		ibizlistService.create(domain);
        IBizListDTO dto = ibizlistMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.ibizlistMapping.toDomain(#ibizlistdtos),'iBizBusinessCentral-IBizList-Create')")
    @ApiOperation(value = "批量新建市场营销列表", tags = {"市场营销列表" },  notes = "批量新建市场营销列表")
	@RequestMapping(method = RequestMethod.POST, value = "/ibizlists/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<IBizListDTO> ibizlistdtos) {
        ibizlistService.createBatch(ibizlistMapping.toDomain(ibizlistdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "ibizlist" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.ibizlistService.get(#ibizlist_id),'iBizBusinessCentral-IBizList-Update')")
    @ApiOperation(value = "更新市场营销列表", tags = {"市场营销列表" },  notes = "更新市场营销列表")
	@RequestMapping(method = RequestMethod.PUT, value = "/ibizlists/{ibizlist_id}")
    public ResponseEntity<IBizListDTO> update(@PathVariable("ibizlist_id") String ibizlist_id, @RequestBody IBizListDTO ibizlistdto) {
		IBizList domain  = ibizlistMapping.toDomain(ibizlistdto);
        domain .setListid(ibizlist_id);
		ibizlistService.update(domain );
		IBizListDTO dto = ibizlistMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.ibizlistService.getIbizlistByEntities(this.ibizlistMapping.toDomain(#ibizlistdtos)),'iBizBusinessCentral-IBizList-Update')")
    @ApiOperation(value = "批量更新市场营销列表", tags = {"市场营销列表" },  notes = "批量更新市场营销列表")
	@RequestMapping(method = RequestMethod.PUT, value = "/ibizlists/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<IBizListDTO> ibizlistdtos) {
        ibizlistService.updateBatch(ibizlistMapping.toDomain(ibizlistdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.ibizlistService.get(#ibizlist_id),'iBizBusinessCentral-IBizList-Remove')")
    @ApiOperation(value = "删除市场营销列表", tags = {"市场营销列表" },  notes = "删除市场营销列表")
	@RequestMapping(method = RequestMethod.DELETE, value = "/ibizlists/{ibizlist_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("ibizlist_id") String ibizlist_id) {
         return ResponseEntity.status(HttpStatus.OK).body(ibizlistService.remove(ibizlist_id));
    }

    @PreAuthorize("hasPermission(this.ibizlistService.getIbizlistByIds(#ids),'iBizBusinessCentral-IBizList-Remove')")
    @ApiOperation(value = "批量删除市场营销列表", tags = {"市场营销列表" },  notes = "批量删除市场营销列表")
	@RequestMapping(method = RequestMethod.DELETE, value = "/ibizlists/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        ibizlistService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.ibizlistMapping.toDomain(returnObject.body),'iBizBusinessCentral-IBizList-Get')")
    @ApiOperation(value = "获取市场营销列表", tags = {"市场营销列表" },  notes = "获取市场营销列表")
	@RequestMapping(method = RequestMethod.GET, value = "/ibizlists/{ibizlist_id}")
    public ResponseEntity<IBizListDTO> get(@PathVariable("ibizlist_id") String ibizlist_id) {
        IBizList domain = ibizlistService.get(ibizlist_id);
        IBizListDTO dto = ibizlistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取市场营销列表草稿", tags = {"市场营销列表" },  notes = "获取市场营销列表草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/ibizlists/getdraft")
    public ResponseEntity<IBizListDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(ibizlistMapping.toDto(ibizlistService.getDraft(new IBizList())));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-IBizList-Active-all')")
    @ApiOperation(value = "激活", tags = {"市场营销列表" },  notes = "激活")
	@RequestMapping(method = RequestMethod.POST, value = "/ibizlists/{ibizlist_id}/active")
    public ResponseEntity<IBizListDTO> active(@PathVariable("ibizlist_id") String ibizlist_id, @RequestBody IBizListDTO ibizlistdto) {
        IBizList domain = ibizlistMapping.toDomain(ibizlistdto);
domain.setListid(ibizlist_id);
        domain = ibizlistService.active(domain);
        ibizlistdto = ibizlistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(ibizlistdto);
    }

    @ApiOperation(value = "检查市场营销列表", tags = {"市场营销列表" },  notes = "检查市场营销列表")
	@RequestMapping(method = RequestMethod.POST, value = "/ibizlists/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody IBizListDTO ibizlistdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(ibizlistService.checkKey(ibizlistMapping.toDomain(ibizlistdto)));
    }

    @PreAuthorize("hasPermission(this.ibizlistMapping.toDomain(#ibizlistdto),'iBizBusinessCentral-IBizList-Save')")
    @ApiOperation(value = "保存市场营销列表", tags = {"市场营销列表" },  notes = "保存市场营销列表")
	@RequestMapping(method = RequestMethod.POST, value = "/ibizlists/save")
    public ResponseEntity<Boolean> save(@RequestBody IBizListDTO ibizlistdto) {
        return ResponseEntity.status(HttpStatus.OK).body(ibizlistService.save(ibizlistMapping.toDomain(ibizlistdto)));
    }

    @PreAuthorize("hasPermission(this.ibizlistMapping.toDomain(#ibizlistdtos),'iBizBusinessCentral-IBizList-Save')")
    @ApiOperation(value = "批量保存市场营销列表", tags = {"市场营销列表" },  notes = "批量保存市场营销列表")
	@RequestMapping(method = RequestMethod.POST, value = "/ibizlists/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<IBizListDTO> ibizlistdtos) {
        ibizlistService.saveBatch(ibizlistMapping.toDomain(ibizlistdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-IBizList-Stop-all')")
    @ApiOperation(value = "停用", tags = {"市场营销列表" },  notes = "停用")
	@RequestMapping(method = RequestMethod.POST, value = "/ibizlists/{ibizlist_id}/stop")
    public ResponseEntity<IBizListDTO> stop(@PathVariable("ibizlist_id") String ibizlist_id, @RequestBody IBizListDTO ibizlistdto) {
        IBizList domain = ibizlistMapping.toDomain(ibizlistdto);
domain.setListid(ibizlist_id);
        domain = ibizlistService.stop(domain);
        ibizlistdto = ibizlistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(ibizlistdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-IBizList-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-IBizList-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"市场营销列表" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/ibizlists/fetchdefault")
	public ResponseEntity<List<IBizListDTO>> fetchDefault(IBizListSearchContext context) {
        Page<IBizList> domains = ibizlistService.searchDefault(context) ;
        List<IBizListDTO> list = ibizlistMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-IBizList-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-IBizList-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"市场营销列表" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/ibizlists/searchdefault")
	public ResponseEntity<Page<IBizListDTO>> searchDefault(@RequestBody IBizListSearchContext context) {
        Page<IBizList> domains = ibizlistService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(ibizlistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-IBizList-searchEffective-all') and hasPermission(#context,'iBizBusinessCentral-IBizList-Get')")
	@ApiOperation(value = "获取有效", tags = {"市场营销列表" } ,notes = "获取有效")
    @RequestMapping(method= RequestMethod.GET , value="/ibizlists/fetcheffective")
	public ResponseEntity<List<IBizListDTO>> fetchEffective(IBizListSearchContext context) {
        Page<IBizList> domains = ibizlistService.searchEffective(context) ;
        List<IBizListDTO> list = ibizlistMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-IBizList-searchEffective-all') and hasPermission(#context,'iBizBusinessCentral-IBizList-Get')")
	@ApiOperation(value = "查询有效", tags = {"市场营销列表" } ,notes = "查询有效")
    @RequestMapping(method= RequestMethod.POST , value="/ibizlists/searcheffective")
	public ResponseEntity<Page<IBizListDTO>> searchEffective(@RequestBody IBizListSearchContext context) {
        Page<IBizList> domains = ibizlistService.searchEffective(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(ibizlistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-IBizList-searchStop-all') and hasPermission(#context,'iBizBusinessCentral-IBizList-Get')")
	@ApiOperation(value = "获取停用", tags = {"市场营销列表" } ,notes = "获取停用")
    @RequestMapping(method= RequestMethod.GET , value="/ibizlists/fetchstop")
	public ResponseEntity<List<IBizListDTO>> fetchStop(IBizListSearchContext context) {
        Page<IBizList> domains = ibizlistService.searchStop(context) ;
        List<IBizListDTO> list = ibizlistMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-IBizList-searchStop-all') and hasPermission(#context,'iBizBusinessCentral-IBizList-Get')")
	@ApiOperation(value = "查询停用", tags = {"市场营销列表" } ,notes = "查询停用")
    @RequestMapping(method= RequestMethod.POST , value="/ibizlists/searchstop")
	public ResponseEntity<Page<IBizListDTO>> searchStop(@RequestBody IBizListSearchContext context) {
        Page<IBizList> domains = ibizlistService.searchStop(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(ibizlistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

