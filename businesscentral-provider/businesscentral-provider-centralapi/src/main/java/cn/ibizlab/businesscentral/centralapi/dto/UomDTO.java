package cn.ibizlab.businesscentral.centralapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[UomDTO]
 */
@Data
public class UomDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [OVERRIDDENCREATEDON]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;

    /**
     * 属性 [BASEUOMNAME]
     *
     */
    @JSONField(name = "baseuomname")
    @JsonProperty("baseuomname")
    private String baseuomname;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;

    /**
     * 属性 [VERSIONNUMBER]
     *
     */
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;

    /**
     * 属性 [QUANTITY]
     *
     */
    @JSONField(name = "quantity")
    @JsonProperty("quantity")
    private BigDecimal quantity;

    /**
     * 属性 [UOMID]
     *
     */
    @JSONField(name = "uomid")
    @JsonProperty("uomid")
    private String uomid;

    /**
     * 属性 [SCHEDULEBASEUOM]
     *
     */
    @JSONField(name = "schedulebaseuom")
    @JsonProperty("schedulebaseuom")
    private Integer schedulebaseuom;

    /**
     * 属性 [ORGANIZATIONID]
     *
     */
    @JSONField(name = "organizationid")
    @JsonProperty("organizationid")
    private String organizationid;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [UOMNAME]
     *
     */
    @JSONField(name = "uomname")
    @JsonProperty("uomname")
    private String uomname;

    /**
     * 属性 [UTCCONVERSIONTIMEZONECODE]
     *
     */
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;

    /**
     * 属性 [TIMEZONERULEVERSIONNUMBER]
     *
     */
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;

    /**
     * 属性 [IMPORTSEQUENCENUMBER]
     *
     */
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;

    /**
     * 属性 [UOMSCHEDULEID]
     *
     */
    @JSONField(name = "uomscheduleid")
    @JsonProperty("uomscheduleid")
    private String uomscheduleid;

    /**
     * 属性 [BASEUOM]
     *
     */
    @JSONField(name = "baseuom")
    @JsonProperty("baseuom")
    private String baseuom;


    /**
     * 设置 [OVERRIDDENCREATEDON]
     */
    public void setOverriddencreatedon(Timestamp  overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 设置 [BASEUOMNAME]
     */
    public void setBaseuomname(String  baseuomname){
        this.baseuomname = baseuomname ;
        this.modify("baseuomname",baseuomname);
    }

    /**
     * 设置 [VERSIONNUMBER]
     */
    public void setVersionnumber(BigInteger  versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [QUANTITY]
     */
    public void setQuantity(BigDecimal  quantity){
        this.quantity = quantity ;
        this.modify("quantity",quantity);
    }

    /**
     * 设置 [SCHEDULEBASEUOM]
     */
    public void setSchedulebaseuom(Integer  schedulebaseuom){
        this.schedulebaseuom = schedulebaseuom ;
        this.modify("schedulebaseuom",schedulebaseuom);
    }

    /**
     * 设置 [ORGANIZATIONID]
     */
    public void setOrganizationid(String  organizationid){
        this.organizationid = organizationid ;
        this.modify("organizationid",organizationid);
    }

    /**
     * 设置 [UOMNAME]
     */
    public void setUomname(String  uomname){
        this.uomname = uomname ;
        this.modify("uomname",uomname);
    }

    /**
     * 设置 [UTCCONVERSIONTIMEZONECODE]
     */
    public void setUtcconversiontimezonecode(Integer  utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [TIMEZONERULEVERSIONNUMBER]
     */
    public void setTimezoneruleversionnumber(Integer  timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [IMPORTSEQUENCENUMBER]
     */
    public void setImportsequencenumber(Integer  importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [UOMSCHEDULEID]
     */
    public void setUomscheduleid(String  uomscheduleid){
        this.uomscheduleid = uomscheduleid ;
        this.modify("uomscheduleid",uomscheduleid);
    }

    /**
     * 设置 [BASEUOM]
     */
    public void setBaseuom(String  baseuom){
        this.baseuom = baseuom ;
        this.modify("baseuom",baseuom);
    }


}

