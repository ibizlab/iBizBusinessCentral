package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.marketing.domain.ListContact;
import cn.ibizlab.businesscentral.core.marketing.service.IListContactService;
import cn.ibizlab.businesscentral.core.marketing.filter.ListContactSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"营销列表-联系人" })
@RestController("CentralApi-listcontact")
@RequestMapping("")
public class ListContactResource {

    @Autowired
    public IListContactService listcontactService;

    @Autowired
    @Lazy
    public ListContactMapping listcontactMapping;

    @PreAuthorize("hasPermission(this.listcontactMapping.toDomain(#listcontactdto),'iBizBusinessCentral-ListContact-Create')")
    @ApiOperation(value = "新建营销列表-联系人", tags = {"营销列表-联系人" },  notes = "新建营销列表-联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/listcontacts")
    public ResponseEntity<ListContactDTO> create(@RequestBody ListContactDTO listcontactdto) {
        ListContact domain = listcontactMapping.toDomain(listcontactdto);
		listcontactService.create(domain);
        ListContactDTO dto = listcontactMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.listcontactMapping.toDomain(#listcontactdtos),'iBizBusinessCentral-ListContact-Create')")
    @ApiOperation(value = "批量新建营销列表-联系人", tags = {"营销列表-联系人" },  notes = "批量新建营销列表-联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/listcontacts/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<ListContactDTO> listcontactdtos) {
        listcontactService.createBatch(listcontactMapping.toDomain(listcontactdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "listcontact" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.listcontactService.get(#listcontact_id),'iBizBusinessCentral-ListContact-Update')")
    @ApiOperation(value = "更新营销列表-联系人", tags = {"营销列表-联系人" },  notes = "更新营销列表-联系人")
	@RequestMapping(method = RequestMethod.PUT, value = "/listcontacts/{listcontact_id}")
    public ResponseEntity<ListContactDTO> update(@PathVariable("listcontact_id") String listcontact_id, @RequestBody ListContactDTO listcontactdto) {
		ListContact domain  = listcontactMapping.toDomain(listcontactdto);
        domain .setRelationshipsid(listcontact_id);
		listcontactService.update(domain );
		ListContactDTO dto = listcontactMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.listcontactService.getListcontactByEntities(this.listcontactMapping.toDomain(#listcontactdtos)),'iBizBusinessCentral-ListContact-Update')")
    @ApiOperation(value = "批量更新营销列表-联系人", tags = {"营销列表-联系人" },  notes = "批量更新营销列表-联系人")
	@RequestMapping(method = RequestMethod.PUT, value = "/listcontacts/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<ListContactDTO> listcontactdtos) {
        listcontactService.updateBatch(listcontactMapping.toDomain(listcontactdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.listcontactService.get(#listcontact_id),'iBizBusinessCentral-ListContact-Remove')")
    @ApiOperation(value = "删除营销列表-联系人", tags = {"营销列表-联系人" },  notes = "删除营销列表-联系人")
	@RequestMapping(method = RequestMethod.DELETE, value = "/listcontacts/{listcontact_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("listcontact_id") String listcontact_id) {
         return ResponseEntity.status(HttpStatus.OK).body(listcontactService.remove(listcontact_id));
    }

    @PreAuthorize("hasPermission(this.listcontactService.getListcontactByIds(#ids),'iBizBusinessCentral-ListContact-Remove')")
    @ApiOperation(value = "批量删除营销列表-联系人", tags = {"营销列表-联系人" },  notes = "批量删除营销列表-联系人")
	@RequestMapping(method = RequestMethod.DELETE, value = "/listcontacts/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        listcontactService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.listcontactMapping.toDomain(returnObject.body),'iBizBusinessCentral-ListContact-Get')")
    @ApiOperation(value = "获取营销列表-联系人", tags = {"营销列表-联系人" },  notes = "获取营销列表-联系人")
	@RequestMapping(method = RequestMethod.GET, value = "/listcontacts/{listcontact_id}")
    public ResponseEntity<ListContactDTO> get(@PathVariable("listcontact_id") String listcontact_id) {
        ListContact domain = listcontactService.get(listcontact_id);
        ListContactDTO dto = listcontactMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取营销列表-联系人草稿", tags = {"营销列表-联系人" },  notes = "获取营销列表-联系人草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/listcontacts/getdraft")
    public ResponseEntity<ListContactDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(listcontactMapping.toDto(listcontactService.getDraft(new ListContact())));
    }

    @ApiOperation(value = "检查营销列表-联系人", tags = {"营销列表-联系人" },  notes = "检查营销列表-联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/listcontacts/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody ListContactDTO listcontactdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(listcontactService.checkKey(listcontactMapping.toDomain(listcontactdto)));
    }

    @PreAuthorize("hasPermission(this.listcontactMapping.toDomain(#listcontactdto),'iBizBusinessCentral-ListContact-Save')")
    @ApiOperation(value = "保存营销列表-联系人", tags = {"营销列表-联系人" },  notes = "保存营销列表-联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/listcontacts/save")
    public ResponseEntity<Boolean> save(@RequestBody ListContactDTO listcontactdto) {
        return ResponseEntity.status(HttpStatus.OK).body(listcontactService.save(listcontactMapping.toDomain(listcontactdto)));
    }

    @PreAuthorize("hasPermission(this.listcontactMapping.toDomain(#listcontactdtos),'iBizBusinessCentral-ListContact-Save')")
    @ApiOperation(value = "批量保存营销列表-联系人", tags = {"营销列表-联系人" },  notes = "批量保存营销列表-联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/listcontacts/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<ListContactDTO> listcontactdtos) {
        listcontactService.saveBatch(listcontactMapping.toDomain(listcontactdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ListContact-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-ListContact-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"营销列表-联系人" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/listcontacts/fetchdefault")
	public ResponseEntity<List<ListContactDTO>> fetchDefault(ListContactSearchContext context) {
        Page<ListContact> domains = listcontactService.searchDefault(context) ;
        List<ListContactDTO> list = listcontactMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ListContact-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-ListContact-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"营销列表-联系人" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/listcontacts/searchdefault")
	public ResponseEntity<Page<ListContactDTO>> searchDefault(@RequestBody ListContactSearchContext context) {
        Page<ListContact> domains = listcontactService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(listcontactMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.listcontactMapping.toDomain(#listcontactdto),'iBizBusinessCentral-ListContact-Create')")
    @ApiOperation(value = "根据联系人建立营销列表-联系人", tags = {"营销列表-联系人" },  notes = "根据联系人建立营销列表-联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/listcontacts")
    public ResponseEntity<ListContactDTO> createByContact(@PathVariable("contact_id") String contact_id, @RequestBody ListContactDTO listcontactdto) {
        ListContact domain = listcontactMapping.toDomain(listcontactdto);
        domain.setEntity2id(contact_id);
		listcontactService.create(domain);
        ListContactDTO dto = listcontactMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.listcontactMapping.toDomain(#listcontactdtos),'iBizBusinessCentral-ListContact-Create')")
    @ApiOperation(value = "根据联系人批量建立营销列表-联系人", tags = {"营销列表-联系人" },  notes = "根据联系人批量建立营销列表-联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/listcontacts/batch")
    public ResponseEntity<Boolean> createBatchByContact(@PathVariable("contact_id") String contact_id, @RequestBody List<ListContactDTO> listcontactdtos) {
        List<ListContact> domainlist=listcontactMapping.toDomain(listcontactdtos);
        for(ListContact domain:domainlist){
            domain.setEntity2id(contact_id);
        }
        listcontactService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "listcontact" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.listcontactService.get(#listcontact_id),'iBizBusinessCentral-ListContact-Update')")
    @ApiOperation(value = "根据联系人更新营销列表-联系人", tags = {"营销列表-联系人" },  notes = "根据联系人更新营销列表-联系人")
	@RequestMapping(method = RequestMethod.PUT, value = "/contacts/{contact_id}/listcontacts/{listcontact_id}")
    public ResponseEntity<ListContactDTO> updateByContact(@PathVariable("contact_id") String contact_id, @PathVariable("listcontact_id") String listcontact_id, @RequestBody ListContactDTO listcontactdto) {
        ListContact domain = listcontactMapping.toDomain(listcontactdto);
        domain.setEntity2id(contact_id);
        domain.setRelationshipsid(listcontact_id);
		listcontactService.update(domain);
        ListContactDTO dto = listcontactMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.listcontactService.getListcontactByEntities(this.listcontactMapping.toDomain(#listcontactdtos)),'iBizBusinessCentral-ListContact-Update')")
    @ApiOperation(value = "根据联系人批量更新营销列表-联系人", tags = {"营销列表-联系人" },  notes = "根据联系人批量更新营销列表-联系人")
	@RequestMapping(method = RequestMethod.PUT, value = "/contacts/{contact_id}/listcontacts/batch")
    public ResponseEntity<Boolean> updateBatchByContact(@PathVariable("contact_id") String contact_id, @RequestBody List<ListContactDTO> listcontactdtos) {
        List<ListContact> domainlist=listcontactMapping.toDomain(listcontactdtos);
        for(ListContact domain:domainlist){
            domain.setEntity2id(contact_id);
        }
        listcontactService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.listcontactService.get(#listcontact_id),'iBizBusinessCentral-ListContact-Remove')")
    @ApiOperation(value = "根据联系人删除营销列表-联系人", tags = {"营销列表-联系人" },  notes = "根据联系人删除营销列表-联系人")
	@RequestMapping(method = RequestMethod.DELETE, value = "/contacts/{contact_id}/listcontacts/{listcontact_id}")
    public ResponseEntity<Boolean> removeByContact(@PathVariable("contact_id") String contact_id, @PathVariable("listcontact_id") String listcontact_id) {
		return ResponseEntity.status(HttpStatus.OK).body(listcontactService.remove(listcontact_id));
    }

    @PreAuthorize("hasPermission(this.listcontactService.getListcontactByIds(#ids),'iBizBusinessCentral-ListContact-Remove')")
    @ApiOperation(value = "根据联系人批量删除营销列表-联系人", tags = {"营销列表-联系人" },  notes = "根据联系人批量删除营销列表-联系人")
	@RequestMapping(method = RequestMethod.DELETE, value = "/contacts/{contact_id}/listcontacts/batch")
    public ResponseEntity<Boolean> removeBatchByContact(@RequestBody List<String> ids) {
        listcontactService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.listcontactMapping.toDomain(returnObject.body),'iBizBusinessCentral-ListContact-Get')")
    @ApiOperation(value = "根据联系人获取营销列表-联系人", tags = {"营销列表-联系人" },  notes = "根据联系人获取营销列表-联系人")
	@RequestMapping(method = RequestMethod.GET, value = "/contacts/{contact_id}/listcontacts/{listcontact_id}")
    public ResponseEntity<ListContactDTO> getByContact(@PathVariable("contact_id") String contact_id, @PathVariable("listcontact_id") String listcontact_id) {
        ListContact domain = listcontactService.get(listcontact_id);
        ListContactDTO dto = listcontactMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据联系人获取营销列表-联系人草稿", tags = {"营销列表-联系人" },  notes = "根据联系人获取营销列表-联系人草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/contacts/{contact_id}/listcontacts/getdraft")
    public ResponseEntity<ListContactDTO> getDraftByContact(@PathVariable("contact_id") String contact_id) {
        ListContact domain = new ListContact();
        domain.setEntity2id(contact_id);
        return ResponseEntity.status(HttpStatus.OK).body(listcontactMapping.toDto(listcontactService.getDraft(domain)));
    }

    @ApiOperation(value = "根据联系人检查营销列表-联系人", tags = {"营销列表-联系人" },  notes = "根据联系人检查营销列表-联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/listcontacts/checkkey")
    public ResponseEntity<Boolean> checkKeyByContact(@PathVariable("contact_id") String contact_id, @RequestBody ListContactDTO listcontactdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(listcontactService.checkKey(listcontactMapping.toDomain(listcontactdto)));
    }

    @PreAuthorize("hasPermission(this.listcontactMapping.toDomain(#listcontactdto),'iBizBusinessCentral-ListContact-Save')")
    @ApiOperation(value = "根据联系人保存营销列表-联系人", tags = {"营销列表-联系人" },  notes = "根据联系人保存营销列表-联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/listcontacts/save")
    public ResponseEntity<Boolean> saveByContact(@PathVariable("contact_id") String contact_id, @RequestBody ListContactDTO listcontactdto) {
        ListContact domain = listcontactMapping.toDomain(listcontactdto);
        domain.setEntity2id(contact_id);
        return ResponseEntity.status(HttpStatus.OK).body(listcontactService.save(domain));
    }

    @PreAuthorize("hasPermission(this.listcontactMapping.toDomain(#listcontactdtos),'iBizBusinessCentral-ListContact-Save')")
    @ApiOperation(value = "根据联系人批量保存营销列表-联系人", tags = {"营销列表-联系人" },  notes = "根据联系人批量保存营销列表-联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/listcontacts/savebatch")
    public ResponseEntity<Boolean> saveBatchByContact(@PathVariable("contact_id") String contact_id, @RequestBody List<ListContactDTO> listcontactdtos) {
        List<ListContact> domainlist=listcontactMapping.toDomain(listcontactdtos);
        for(ListContact domain:domainlist){
             domain.setEntity2id(contact_id);
        }
        listcontactService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ListContact-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-ListContact-Get')")
	@ApiOperation(value = "根据联系人获取DEFAULT", tags = {"营销列表-联系人" } ,notes = "根据联系人获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/contacts/{contact_id}/listcontacts/fetchdefault")
	public ResponseEntity<List<ListContactDTO>> fetchListContactDefaultByContact(@PathVariable("contact_id") String contact_id,ListContactSearchContext context) {
        context.setN_entity2id_eq(contact_id);
        Page<ListContact> domains = listcontactService.searchDefault(context) ;
        List<ListContactDTO> list = listcontactMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ListContact-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-ListContact-Get')")
	@ApiOperation(value = "根据联系人查询DEFAULT", tags = {"营销列表-联系人" } ,notes = "根据联系人查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/contacts/{contact_id}/listcontacts/searchdefault")
	public ResponseEntity<Page<ListContactDTO>> searchListContactDefaultByContact(@PathVariable("contact_id") String contact_id, @RequestBody ListContactSearchContext context) {
        context.setN_entity2id_eq(contact_id);
        Page<ListContact> domains = listcontactService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(listcontactMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.listcontactMapping.toDomain(#listcontactdto),'iBizBusinessCentral-ListContact-Create')")
    @ApiOperation(value = "根据市场营销列表建立营销列表-联系人", tags = {"营销列表-联系人" },  notes = "根据市场营销列表建立营销列表-联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/ibizlists/{ibizlist_id}/listcontacts")
    public ResponseEntity<ListContactDTO> createByIBizList(@PathVariable("ibizlist_id") String ibizlist_id, @RequestBody ListContactDTO listcontactdto) {
        ListContact domain = listcontactMapping.toDomain(listcontactdto);
        domain.setEntityid(ibizlist_id);
		listcontactService.create(domain);
        ListContactDTO dto = listcontactMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.listcontactMapping.toDomain(#listcontactdtos),'iBizBusinessCentral-ListContact-Create')")
    @ApiOperation(value = "根据市场营销列表批量建立营销列表-联系人", tags = {"营销列表-联系人" },  notes = "根据市场营销列表批量建立营销列表-联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/ibizlists/{ibizlist_id}/listcontacts/batch")
    public ResponseEntity<Boolean> createBatchByIBizList(@PathVariable("ibizlist_id") String ibizlist_id, @RequestBody List<ListContactDTO> listcontactdtos) {
        List<ListContact> domainlist=listcontactMapping.toDomain(listcontactdtos);
        for(ListContact domain:domainlist){
            domain.setEntityid(ibizlist_id);
        }
        listcontactService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "listcontact" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.listcontactService.get(#listcontact_id),'iBizBusinessCentral-ListContact-Update')")
    @ApiOperation(value = "根据市场营销列表更新营销列表-联系人", tags = {"营销列表-联系人" },  notes = "根据市场营销列表更新营销列表-联系人")
	@RequestMapping(method = RequestMethod.PUT, value = "/ibizlists/{ibizlist_id}/listcontacts/{listcontact_id}")
    public ResponseEntity<ListContactDTO> updateByIBizList(@PathVariable("ibizlist_id") String ibizlist_id, @PathVariable("listcontact_id") String listcontact_id, @RequestBody ListContactDTO listcontactdto) {
        ListContact domain = listcontactMapping.toDomain(listcontactdto);
        domain.setEntityid(ibizlist_id);
        domain.setRelationshipsid(listcontact_id);
		listcontactService.update(domain);
        ListContactDTO dto = listcontactMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.listcontactService.getListcontactByEntities(this.listcontactMapping.toDomain(#listcontactdtos)),'iBizBusinessCentral-ListContact-Update')")
    @ApiOperation(value = "根据市场营销列表批量更新营销列表-联系人", tags = {"营销列表-联系人" },  notes = "根据市场营销列表批量更新营销列表-联系人")
	@RequestMapping(method = RequestMethod.PUT, value = "/ibizlists/{ibizlist_id}/listcontacts/batch")
    public ResponseEntity<Boolean> updateBatchByIBizList(@PathVariable("ibizlist_id") String ibizlist_id, @RequestBody List<ListContactDTO> listcontactdtos) {
        List<ListContact> domainlist=listcontactMapping.toDomain(listcontactdtos);
        for(ListContact domain:domainlist){
            domain.setEntityid(ibizlist_id);
        }
        listcontactService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.listcontactService.get(#listcontact_id),'iBizBusinessCentral-ListContact-Remove')")
    @ApiOperation(value = "根据市场营销列表删除营销列表-联系人", tags = {"营销列表-联系人" },  notes = "根据市场营销列表删除营销列表-联系人")
	@RequestMapping(method = RequestMethod.DELETE, value = "/ibizlists/{ibizlist_id}/listcontacts/{listcontact_id}")
    public ResponseEntity<Boolean> removeByIBizList(@PathVariable("ibizlist_id") String ibizlist_id, @PathVariable("listcontact_id") String listcontact_id) {
		return ResponseEntity.status(HttpStatus.OK).body(listcontactService.remove(listcontact_id));
    }

    @PreAuthorize("hasPermission(this.listcontactService.getListcontactByIds(#ids),'iBizBusinessCentral-ListContact-Remove')")
    @ApiOperation(value = "根据市场营销列表批量删除营销列表-联系人", tags = {"营销列表-联系人" },  notes = "根据市场营销列表批量删除营销列表-联系人")
	@RequestMapping(method = RequestMethod.DELETE, value = "/ibizlists/{ibizlist_id}/listcontacts/batch")
    public ResponseEntity<Boolean> removeBatchByIBizList(@RequestBody List<String> ids) {
        listcontactService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.listcontactMapping.toDomain(returnObject.body),'iBizBusinessCentral-ListContact-Get')")
    @ApiOperation(value = "根据市场营销列表获取营销列表-联系人", tags = {"营销列表-联系人" },  notes = "根据市场营销列表获取营销列表-联系人")
	@RequestMapping(method = RequestMethod.GET, value = "/ibizlists/{ibizlist_id}/listcontacts/{listcontact_id}")
    public ResponseEntity<ListContactDTO> getByIBizList(@PathVariable("ibizlist_id") String ibizlist_id, @PathVariable("listcontact_id") String listcontact_id) {
        ListContact domain = listcontactService.get(listcontact_id);
        ListContactDTO dto = listcontactMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据市场营销列表获取营销列表-联系人草稿", tags = {"营销列表-联系人" },  notes = "根据市场营销列表获取营销列表-联系人草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/ibizlists/{ibizlist_id}/listcontacts/getdraft")
    public ResponseEntity<ListContactDTO> getDraftByIBizList(@PathVariable("ibizlist_id") String ibizlist_id) {
        ListContact domain = new ListContact();
        domain.setEntityid(ibizlist_id);
        return ResponseEntity.status(HttpStatus.OK).body(listcontactMapping.toDto(listcontactService.getDraft(domain)));
    }

    @ApiOperation(value = "根据市场营销列表检查营销列表-联系人", tags = {"营销列表-联系人" },  notes = "根据市场营销列表检查营销列表-联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/ibizlists/{ibizlist_id}/listcontacts/checkkey")
    public ResponseEntity<Boolean> checkKeyByIBizList(@PathVariable("ibizlist_id") String ibizlist_id, @RequestBody ListContactDTO listcontactdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(listcontactService.checkKey(listcontactMapping.toDomain(listcontactdto)));
    }

    @PreAuthorize("hasPermission(this.listcontactMapping.toDomain(#listcontactdto),'iBizBusinessCentral-ListContact-Save')")
    @ApiOperation(value = "根据市场营销列表保存营销列表-联系人", tags = {"营销列表-联系人" },  notes = "根据市场营销列表保存营销列表-联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/ibizlists/{ibizlist_id}/listcontacts/save")
    public ResponseEntity<Boolean> saveByIBizList(@PathVariable("ibizlist_id") String ibizlist_id, @RequestBody ListContactDTO listcontactdto) {
        ListContact domain = listcontactMapping.toDomain(listcontactdto);
        domain.setEntityid(ibizlist_id);
        return ResponseEntity.status(HttpStatus.OK).body(listcontactService.save(domain));
    }

    @PreAuthorize("hasPermission(this.listcontactMapping.toDomain(#listcontactdtos),'iBizBusinessCentral-ListContact-Save')")
    @ApiOperation(value = "根据市场营销列表批量保存营销列表-联系人", tags = {"营销列表-联系人" },  notes = "根据市场营销列表批量保存营销列表-联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/ibizlists/{ibizlist_id}/listcontacts/savebatch")
    public ResponseEntity<Boolean> saveBatchByIBizList(@PathVariable("ibizlist_id") String ibizlist_id, @RequestBody List<ListContactDTO> listcontactdtos) {
        List<ListContact> domainlist=listcontactMapping.toDomain(listcontactdtos);
        for(ListContact domain:domainlist){
             domain.setEntityid(ibizlist_id);
        }
        listcontactService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ListContact-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-ListContact-Get')")
	@ApiOperation(value = "根据市场营销列表获取DEFAULT", tags = {"营销列表-联系人" } ,notes = "根据市场营销列表获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/ibizlists/{ibizlist_id}/listcontacts/fetchdefault")
	public ResponseEntity<List<ListContactDTO>> fetchListContactDefaultByIBizList(@PathVariable("ibizlist_id") String ibizlist_id,ListContactSearchContext context) {
        context.setN_entityid_eq(ibizlist_id);
        Page<ListContact> domains = listcontactService.searchDefault(context) ;
        List<ListContactDTO> list = listcontactMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ListContact-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-ListContact-Get')")
	@ApiOperation(value = "根据市场营销列表查询DEFAULT", tags = {"营销列表-联系人" } ,notes = "根据市场营销列表查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/ibizlists/{ibizlist_id}/listcontacts/searchdefault")
	public ResponseEntity<Page<ListContactDTO>> searchListContactDefaultByIBizList(@PathVariable("ibizlist_id") String ibizlist_id, @RequestBody ListContactSearchContext context) {
        context.setN_entityid_eq(ibizlist_id);
        Page<ListContact> domains = listcontactService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(listcontactMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.listcontactMapping.toDomain(#listcontactdto),'iBizBusinessCentral-ListContact-Create')")
    @ApiOperation(value = "根据客户联系人建立营销列表-联系人", tags = {"营销列表-联系人" },  notes = "根据客户联系人建立营销列表-联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/listcontacts")
    public ResponseEntity<ListContactDTO> createByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @RequestBody ListContactDTO listcontactdto) {
        ListContact domain = listcontactMapping.toDomain(listcontactdto);
        domain.setEntity2id(contact_id);
		listcontactService.create(domain);
        ListContactDTO dto = listcontactMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.listcontactMapping.toDomain(#listcontactdtos),'iBizBusinessCentral-ListContact-Create')")
    @ApiOperation(value = "根据客户联系人批量建立营销列表-联系人", tags = {"营销列表-联系人" },  notes = "根据客户联系人批量建立营销列表-联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/listcontacts/batch")
    public ResponseEntity<Boolean> createBatchByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @RequestBody List<ListContactDTO> listcontactdtos) {
        List<ListContact> domainlist=listcontactMapping.toDomain(listcontactdtos);
        for(ListContact domain:domainlist){
            domain.setEntity2id(contact_id);
        }
        listcontactService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "listcontact" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.listcontactService.get(#listcontact_id),'iBizBusinessCentral-ListContact-Update')")
    @ApiOperation(value = "根据客户联系人更新营销列表-联系人", tags = {"营销列表-联系人" },  notes = "根据客户联系人更新营销列表-联系人")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/contacts/{contact_id}/listcontacts/{listcontact_id}")
    public ResponseEntity<ListContactDTO> updateByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("listcontact_id") String listcontact_id, @RequestBody ListContactDTO listcontactdto) {
        ListContact domain = listcontactMapping.toDomain(listcontactdto);
        domain.setEntity2id(contact_id);
        domain.setRelationshipsid(listcontact_id);
		listcontactService.update(domain);
        ListContactDTO dto = listcontactMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.listcontactService.getListcontactByEntities(this.listcontactMapping.toDomain(#listcontactdtos)),'iBizBusinessCentral-ListContact-Update')")
    @ApiOperation(value = "根据客户联系人批量更新营销列表-联系人", tags = {"营销列表-联系人" },  notes = "根据客户联系人批量更新营销列表-联系人")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/contacts/{contact_id}/listcontacts/batch")
    public ResponseEntity<Boolean> updateBatchByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @RequestBody List<ListContactDTO> listcontactdtos) {
        List<ListContact> domainlist=listcontactMapping.toDomain(listcontactdtos);
        for(ListContact domain:domainlist){
            domain.setEntity2id(contact_id);
        }
        listcontactService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.listcontactService.get(#listcontact_id),'iBizBusinessCentral-ListContact-Remove')")
    @ApiOperation(value = "根据客户联系人删除营销列表-联系人", tags = {"营销列表-联系人" },  notes = "根据客户联系人删除营销列表-联系人")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/contacts/{contact_id}/listcontacts/{listcontact_id}")
    public ResponseEntity<Boolean> removeByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("listcontact_id") String listcontact_id) {
		return ResponseEntity.status(HttpStatus.OK).body(listcontactService.remove(listcontact_id));
    }

    @PreAuthorize("hasPermission(this.listcontactService.getListcontactByIds(#ids),'iBizBusinessCentral-ListContact-Remove')")
    @ApiOperation(value = "根据客户联系人批量删除营销列表-联系人", tags = {"营销列表-联系人" },  notes = "根据客户联系人批量删除营销列表-联系人")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/contacts/{contact_id}/listcontacts/batch")
    public ResponseEntity<Boolean> removeBatchByAccountContact(@RequestBody List<String> ids) {
        listcontactService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.listcontactMapping.toDomain(returnObject.body),'iBizBusinessCentral-ListContact-Get')")
    @ApiOperation(value = "根据客户联系人获取营销列表-联系人", tags = {"营销列表-联系人" },  notes = "根据客户联系人获取营销列表-联系人")
	@RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/contacts/{contact_id}/listcontacts/{listcontact_id}")
    public ResponseEntity<ListContactDTO> getByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("listcontact_id") String listcontact_id) {
        ListContact domain = listcontactService.get(listcontact_id);
        ListContactDTO dto = listcontactMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据客户联系人获取营销列表-联系人草稿", tags = {"营销列表-联系人" },  notes = "根据客户联系人获取营销列表-联系人草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/contacts/{contact_id}/listcontacts/getdraft")
    public ResponseEntity<ListContactDTO> getDraftByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id) {
        ListContact domain = new ListContact();
        domain.setEntity2id(contact_id);
        return ResponseEntity.status(HttpStatus.OK).body(listcontactMapping.toDto(listcontactService.getDraft(domain)));
    }

    @ApiOperation(value = "根据客户联系人检查营销列表-联系人", tags = {"营销列表-联系人" },  notes = "根据客户联系人检查营销列表-联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/listcontacts/checkkey")
    public ResponseEntity<Boolean> checkKeyByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @RequestBody ListContactDTO listcontactdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(listcontactService.checkKey(listcontactMapping.toDomain(listcontactdto)));
    }

    @PreAuthorize("hasPermission(this.listcontactMapping.toDomain(#listcontactdto),'iBizBusinessCentral-ListContact-Save')")
    @ApiOperation(value = "根据客户联系人保存营销列表-联系人", tags = {"营销列表-联系人" },  notes = "根据客户联系人保存营销列表-联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/listcontacts/save")
    public ResponseEntity<Boolean> saveByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @RequestBody ListContactDTO listcontactdto) {
        ListContact domain = listcontactMapping.toDomain(listcontactdto);
        domain.setEntity2id(contact_id);
        return ResponseEntity.status(HttpStatus.OK).body(listcontactService.save(domain));
    }

    @PreAuthorize("hasPermission(this.listcontactMapping.toDomain(#listcontactdtos),'iBizBusinessCentral-ListContact-Save')")
    @ApiOperation(value = "根据客户联系人批量保存营销列表-联系人", tags = {"营销列表-联系人" },  notes = "根据客户联系人批量保存营销列表-联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/listcontacts/savebatch")
    public ResponseEntity<Boolean> saveBatchByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @RequestBody List<ListContactDTO> listcontactdtos) {
        List<ListContact> domainlist=listcontactMapping.toDomain(listcontactdtos);
        for(ListContact domain:domainlist){
             domain.setEntity2id(contact_id);
        }
        listcontactService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ListContact-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-ListContact-Get')")
	@ApiOperation(value = "根据客户联系人获取DEFAULT", tags = {"营销列表-联系人" } ,notes = "根据客户联系人获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/contacts/{contact_id}/listcontacts/fetchdefault")
	public ResponseEntity<List<ListContactDTO>> fetchListContactDefaultByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id,ListContactSearchContext context) {
        context.setN_entity2id_eq(contact_id);
        Page<ListContact> domains = listcontactService.searchDefault(context) ;
        List<ListContactDTO> list = listcontactMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ListContact-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-ListContact-Get')")
	@ApiOperation(value = "根据客户联系人查询DEFAULT", tags = {"营销列表-联系人" } ,notes = "根据客户联系人查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/contacts/{contact_id}/listcontacts/searchdefault")
	public ResponseEntity<Page<ListContactDTO>> searchListContactDefaultByAccountContact(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @RequestBody ListContactSearchContext context) {
        context.setN_entity2id_eq(contact_id);
        Page<ListContact> domains = listcontactService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(listcontactMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

