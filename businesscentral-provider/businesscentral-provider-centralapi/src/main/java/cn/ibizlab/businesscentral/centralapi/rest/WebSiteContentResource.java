package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.website.domain.WebSiteContent;
import cn.ibizlab.businesscentral.core.website.service.IWebSiteContentService;
import cn.ibizlab.businesscentral.core.website.filter.WebSiteContentSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"站点内容" })
@RestController("CentralApi-websitecontent")
@RequestMapping("")
public class WebSiteContentResource {

    @Autowired
    public IWebSiteContentService websitecontentService;

    @Autowired
    @Lazy
    public WebSiteContentMapping websitecontentMapping;

    @PreAuthorize("hasPermission(this.websitecontentMapping.toDomain(#websitecontentdto),'iBizBusinessCentral-WebSiteContent-Create')")
    @ApiOperation(value = "新建站点内容", tags = {"站点内容" },  notes = "新建站点内容")
	@RequestMapping(method = RequestMethod.POST, value = "/websitecontents")
    public ResponseEntity<WebSiteContentDTO> create(@RequestBody WebSiteContentDTO websitecontentdto) {
        WebSiteContent domain = websitecontentMapping.toDomain(websitecontentdto);
		websitecontentService.create(domain);
        WebSiteContentDTO dto = websitecontentMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.websitecontentMapping.toDomain(#websitecontentdtos),'iBizBusinessCentral-WebSiteContent-Create')")
    @ApiOperation(value = "批量新建站点内容", tags = {"站点内容" },  notes = "批量新建站点内容")
	@RequestMapping(method = RequestMethod.POST, value = "/websitecontents/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<WebSiteContentDTO> websitecontentdtos) {
        websitecontentService.createBatch(websitecontentMapping.toDomain(websitecontentdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "websitecontent" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.websitecontentService.get(#websitecontent_id),'iBizBusinessCentral-WebSiteContent-Update')")
    @ApiOperation(value = "更新站点内容", tags = {"站点内容" },  notes = "更新站点内容")
	@RequestMapping(method = RequestMethod.PUT, value = "/websitecontents/{websitecontent_id}")
    public ResponseEntity<WebSiteContentDTO> update(@PathVariable("websitecontent_id") String websitecontent_id, @RequestBody WebSiteContentDTO websitecontentdto) {
		WebSiteContent domain  = websitecontentMapping.toDomain(websitecontentdto);
        domain .setWebsitecontentid(websitecontent_id);
		websitecontentService.update(domain );
		WebSiteContentDTO dto = websitecontentMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.websitecontentService.getWebsitecontentByEntities(this.websitecontentMapping.toDomain(#websitecontentdtos)),'iBizBusinessCentral-WebSiteContent-Update')")
    @ApiOperation(value = "批量更新站点内容", tags = {"站点内容" },  notes = "批量更新站点内容")
	@RequestMapping(method = RequestMethod.PUT, value = "/websitecontents/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<WebSiteContentDTO> websitecontentdtos) {
        websitecontentService.updateBatch(websitecontentMapping.toDomain(websitecontentdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.websitecontentService.get(#websitecontent_id),'iBizBusinessCentral-WebSiteContent-Remove')")
    @ApiOperation(value = "删除站点内容", tags = {"站点内容" },  notes = "删除站点内容")
	@RequestMapping(method = RequestMethod.DELETE, value = "/websitecontents/{websitecontent_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("websitecontent_id") String websitecontent_id) {
         return ResponseEntity.status(HttpStatus.OK).body(websitecontentService.remove(websitecontent_id));
    }

    @PreAuthorize("hasPermission(this.websitecontentService.getWebsitecontentByIds(#ids),'iBizBusinessCentral-WebSiteContent-Remove')")
    @ApiOperation(value = "批量删除站点内容", tags = {"站点内容" },  notes = "批量删除站点内容")
	@RequestMapping(method = RequestMethod.DELETE, value = "/websitecontents/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        websitecontentService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.websitecontentMapping.toDomain(returnObject.body),'iBizBusinessCentral-WebSiteContent-Get')")
    @ApiOperation(value = "获取站点内容", tags = {"站点内容" },  notes = "获取站点内容")
	@RequestMapping(method = RequestMethod.GET, value = "/websitecontents/{websitecontent_id}")
    public ResponseEntity<WebSiteContentDTO> get(@PathVariable("websitecontent_id") String websitecontent_id) {
        WebSiteContent domain = websitecontentService.get(websitecontent_id);
        WebSiteContentDTO dto = websitecontentMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取站点内容草稿", tags = {"站点内容" },  notes = "获取站点内容草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/websitecontents/getdraft")
    public ResponseEntity<WebSiteContentDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(websitecontentMapping.toDto(websitecontentService.getDraft(new WebSiteContent())));
    }

    @ApiOperation(value = "检查站点内容", tags = {"站点内容" },  notes = "检查站点内容")
	@RequestMapping(method = RequestMethod.POST, value = "/websitecontents/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody WebSiteContentDTO websitecontentdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(websitecontentService.checkKey(websitecontentMapping.toDomain(websitecontentdto)));
    }

    @PreAuthorize("hasPermission(this.websitecontentMapping.toDomain(#websitecontentdto),'iBizBusinessCentral-WebSiteContent-Save')")
    @ApiOperation(value = "保存站点内容", tags = {"站点内容" },  notes = "保存站点内容")
	@RequestMapping(method = RequestMethod.POST, value = "/websitecontents/save")
    public ResponseEntity<Boolean> save(@RequestBody WebSiteContentDTO websitecontentdto) {
        return ResponseEntity.status(HttpStatus.OK).body(websitecontentService.save(websitecontentMapping.toDomain(websitecontentdto)));
    }

    @PreAuthorize("hasPermission(this.websitecontentMapping.toDomain(#websitecontentdtos),'iBizBusinessCentral-WebSiteContent-Save')")
    @ApiOperation(value = "批量保存站点内容", tags = {"站点内容" },  notes = "批量保存站点内容")
	@RequestMapping(method = RequestMethod.POST, value = "/websitecontents/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<WebSiteContentDTO> websitecontentdtos) {
        websitecontentService.saveBatch(websitecontentMapping.toDomain(websitecontentdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-WebSiteContent-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-WebSiteContent-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"站点内容" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/websitecontents/fetchdefault")
	public ResponseEntity<List<WebSiteContentDTO>> fetchDefault(WebSiteContentSearchContext context) {
        Page<WebSiteContent> domains = websitecontentService.searchDefault(context) ;
        List<WebSiteContentDTO> list = websitecontentMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-WebSiteContent-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-WebSiteContent-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"站点内容" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/websitecontents/searchdefault")
	public ResponseEntity<Page<WebSiteContentDTO>> searchDefault(@RequestBody WebSiteContentSearchContext context) {
        Page<WebSiteContent> domains = websitecontentService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(websitecontentMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.websitecontentMapping.toDomain(#websitecontentdto),'iBizBusinessCentral-WebSiteContent-Create')")
    @ApiOperation(value = "根据站点频道建立站点内容", tags = {"站点内容" },  notes = "根据站点频道建立站点内容")
	@RequestMapping(method = RequestMethod.POST, value = "/websitechannels/{websitechannel_id}/websitecontents")
    public ResponseEntity<WebSiteContentDTO> createByWebSiteChannel(@PathVariable("websitechannel_id") String websitechannel_id, @RequestBody WebSiteContentDTO websitecontentdto) {
        WebSiteContent domain = websitecontentMapping.toDomain(websitecontentdto);
        domain.setWebsitechannelid(websitechannel_id);
		websitecontentService.create(domain);
        WebSiteContentDTO dto = websitecontentMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.websitecontentMapping.toDomain(#websitecontentdtos),'iBizBusinessCentral-WebSiteContent-Create')")
    @ApiOperation(value = "根据站点频道批量建立站点内容", tags = {"站点内容" },  notes = "根据站点频道批量建立站点内容")
	@RequestMapping(method = RequestMethod.POST, value = "/websitechannels/{websitechannel_id}/websitecontents/batch")
    public ResponseEntity<Boolean> createBatchByWebSiteChannel(@PathVariable("websitechannel_id") String websitechannel_id, @RequestBody List<WebSiteContentDTO> websitecontentdtos) {
        List<WebSiteContent> domainlist=websitecontentMapping.toDomain(websitecontentdtos);
        for(WebSiteContent domain:domainlist){
            domain.setWebsitechannelid(websitechannel_id);
        }
        websitecontentService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "websitecontent" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.websitecontentService.get(#websitecontent_id),'iBizBusinessCentral-WebSiteContent-Update')")
    @ApiOperation(value = "根据站点频道更新站点内容", tags = {"站点内容" },  notes = "根据站点频道更新站点内容")
	@RequestMapping(method = RequestMethod.PUT, value = "/websitechannels/{websitechannel_id}/websitecontents/{websitecontent_id}")
    public ResponseEntity<WebSiteContentDTO> updateByWebSiteChannel(@PathVariable("websitechannel_id") String websitechannel_id, @PathVariable("websitecontent_id") String websitecontent_id, @RequestBody WebSiteContentDTO websitecontentdto) {
        WebSiteContent domain = websitecontentMapping.toDomain(websitecontentdto);
        domain.setWebsitechannelid(websitechannel_id);
        domain.setWebsitecontentid(websitecontent_id);
		websitecontentService.update(domain);
        WebSiteContentDTO dto = websitecontentMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.websitecontentService.getWebsitecontentByEntities(this.websitecontentMapping.toDomain(#websitecontentdtos)),'iBizBusinessCentral-WebSiteContent-Update')")
    @ApiOperation(value = "根据站点频道批量更新站点内容", tags = {"站点内容" },  notes = "根据站点频道批量更新站点内容")
	@RequestMapping(method = RequestMethod.PUT, value = "/websitechannels/{websitechannel_id}/websitecontents/batch")
    public ResponseEntity<Boolean> updateBatchByWebSiteChannel(@PathVariable("websitechannel_id") String websitechannel_id, @RequestBody List<WebSiteContentDTO> websitecontentdtos) {
        List<WebSiteContent> domainlist=websitecontentMapping.toDomain(websitecontentdtos);
        for(WebSiteContent domain:domainlist){
            domain.setWebsitechannelid(websitechannel_id);
        }
        websitecontentService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.websitecontentService.get(#websitecontent_id),'iBizBusinessCentral-WebSiteContent-Remove')")
    @ApiOperation(value = "根据站点频道删除站点内容", tags = {"站点内容" },  notes = "根据站点频道删除站点内容")
	@RequestMapping(method = RequestMethod.DELETE, value = "/websitechannels/{websitechannel_id}/websitecontents/{websitecontent_id}")
    public ResponseEntity<Boolean> removeByWebSiteChannel(@PathVariable("websitechannel_id") String websitechannel_id, @PathVariable("websitecontent_id") String websitecontent_id) {
		return ResponseEntity.status(HttpStatus.OK).body(websitecontentService.remove(websitecontent_id));
    }

    @PreAuthorize("hasPermission(this.websitecontentService.getWebsitecontentByIds(#ids),'iBizBusinessCentral-WebSiteContent-Remove')")
    @ApiOperation(value = "根据站点频道批量删除站点内容", tags = {"站点内容" },  notes = "根据站点频道批量删除站点内容")
	@RequestMapping(method = RequestMethod.DELETE, value = "/websitechannels/{websitechannel_id}/websitecontents/batch")
    public ResponseEntity<Boolean> removeBatchByWebSiteChannel(@RequestBody List<String> ids) {
        websitecontentService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.websitecontentMapping.toDomain(returnObject.body),'iBizBusinessCentral-WebSiteContent-Get')")
    @ApiOperation(value = "根据站点频道获取站点内容", tags = {"站点内容" },  notes = "根据站点频道获取站点内容")
	@RequestMapping(method = RequestMethod.GET, value = "/websitechannels/{websitechannel_id}/websitecontents/{websitecontent_id}")
    public ResponseEntity<WebSiteContentDTO> getByWebSiteChannel(@PathVariable("websitechannel_id") String websitechannel_id, @PathVariable("websitecontent_id") String websitecontent_id) {
        WebSiteContent domain = websitecontentService.get(websitecontent_id);
        WebSiteContentDTO dto = websitecontentMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据站点频道获取站点内容草稿", tags = {"站点内容" },  notes = "根据站点频道获取站点内容草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/websitechannels/{websitechannel_id}/websitecontents/getdraft")
    public ResponseEntity<WebSiteContentDTO> getDraftByWebSiteChannel(@PathVariable("websitechannel_id") String websitechannel_id) {
        WebSiteContent domain = new WebSiteContent();
        domain.setWebsitechannelid(websitechannel_id);
        return ResponseEntity.status(HttpStatus.OK).body(websitecontentMapping.toDto(websitecontentService.getDraft(domain)));
    }

    @ApiOperation(value = "根据站点频道检查站点内容", tags = {"站点内容" },  notes = "根据站点频道检查站点内容")
	@RequestMapping(method = RequestMethod.POST, value = "/websitechannels/{websitechannel_id}/websitecontents/checkkey")
    public ResponseEntity<Boolean> checkKeyByWebSiteChannel(@PathVariable("websitechannel_id") String websitechannel_id, @RequestBody WebSiteContentDTO websitecontentdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(websitecontentService.checkKey(websitecontentMapping.toDomain(websitecontentdto)));
    }

    @PreAuthorize("hasPermission(this.websitecontentMapping.toDomain(#websitecontentdto),'iBizBusinessCentral-WebSiteContent-Save')")
    @ApiOperation(value = "根据站点频道保存站点内容", tags = {"站点内容" },  notes = "根据站点频道保存站点内容")
	@RequestMapping(method = RequestMethod.POST, value = "/websitechannels/{websitechannel_id}/websitecontents/save")
    public ResponseEntity<Boolean> saveByWebSiteChannel(@PathVariable("websitechannel_id") String websitechannel_id, @RequestBody WebSiteContentDTO websitecontentdto) {
        WebSiteContent domain = websitecontentMapping.toDomain(websitecontentdto);
        domain.setWebsitechannelid(websitechannel_id);
        return ResponseEntity.status(HttpStatus.OK).body(websitecontentService.save(domain));
    }

    @PreAuthorize("hasPermission(this.websitecontentMapping.toDomain(#websitecontentdtos),'iBizBusinessCentral-WebSiteContent-Save')")
    @ApiOperation(value = "根据站点频道批量保存站点内容", tags = {"站点内容" },  notes = "根据站点频道批量保存站点内容")
	@RequestMapping(method = RequestMethod.POST, value = "/websitechannels/{websitechannel_id}/websitecontents/savebatch")
    public ResponseEntity<Boolean> saveBatchByWebSiteChannel(@PathVariable("websitechannel_id") String websitechannel_id, @RequestBody List<WebSiteContentDTO> websitecontentdtos) {
        List<WebSiteContent> domainlist=websitecontentMapping.toDomain(websitecontentdtos);
        for(WebSiteContent domain:domainlist){
             domain.setWebsitechannelid(websitechannel_id);
        }
        websitecontentService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-WebSiteContent-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-WebSiteContent-Get')")
	@ApiOperation(value = "根据站点频道获取DEFAULT", tags = {"站点内容" } ,notes = "根据站点频道获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/websitechannels/{websitechannel_id}/websitecontents/fetchdefault")
	public ResponseEntity<List<WebSiteContentDTO>> fetchWebSiteContentDefaultByWebSiteChannel(@PathVariable("websitechannel_id") String websitechannel_id,WebSiteContentSearchContext context) {
        context.setN_websitechannelid_eq(websitechannel_id);
        Page<WebSiteContent> domains = websitecontentService.searchDefault(context) ;
        List<WebSiteContentDTO> list = websitecontentMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-WebSiteContent-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-WebSiteContent-Get')")
	@ApiOperation(value = "根据站点频道查询DEFAULT", tags = {"站点内容" } ,notes = "根据站点频道查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/websitechannels/{websitechannel_id}/websitecontents/searchdefault")
	public ResponseEntity<Page<WebSiteContentDTO>> searchWebSiteContentDefaultByWebSiteChannel(@PathVariable("websitechannel_id") String websitechannel_id, @RequestBody WebSiteContentSearchContext context) {
        context.setN_websitechannelid_eq(websitechannel_id);
        Page<WebSiteContent> domains = websitecontentService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(websitecontentMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.websitecontentMapping.toDomain(#websitecontentdto),'iBizBusinessCentral-WebSiteContent-Create')")
    @ApiOperation(value = "根据站点建立站点内容", tags = {"站点内容" },  notes = "根据站点建立站点内容")
	@RequestMapping(method = RequestMethod.POST, value = "/websites/{website_id}/websitecontents")
    public ResponseEntity<WebSiteContentDTO> createByWebSite(@PathVariable("website_id") String website_id, @RequestBody WebSiteContentDTO websitecontentdto) {
        WebSiteContent domain = websitecontentMapping.toDomain(websitecontentdto);
        domain.setWebsiteid(website_id);
		websitecontentService.create(domain);
        WebSiteContentDTO dto = websitecontentMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.websitecontentMapping.toDomain(#websitecontentdtos),'iBizBusinessCentral-WebSiteContent-Create')")
    @ApiOperation(value = "根据站点批量建立站点内容", tags = {"站点内容" },  notes = "根据站点批量建立站点内容")
	@RequestMapping(method = RequestMethod.POST, value = "/websites/{website_id}/websitecontents/batch")
    public ResponseEntity<Boolean> createBatchByWebSite(@PathVariable("website_id") String website_id, @RequestBody List<WebSiteContentDTO> websitecontentdtos) {
        List<WebSiteContent> domainlist=websitecontentMapping.toDomain(websitecontentdtos);
        for(WebSiteContent domain:domainlist){
            domain.setWebsiteid(website_id);
        }
        websitecontentService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "websitecontent" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.websitecontentService.get(#websitecontent_id),'iBizBusinessCentral-WebSiteContent-Update')")
    @ApiOperation(value = "根据站点更新站点内容", tags = {"站点内容" },  notes = "根据站点更新站点内容")
	@RequestMapping(method = RequestMethod.PUT, value = "/websites/{website_id}/websitecontents/{websitecontent_id}")
    public ResponseEntity<WebSiteContentDTO> updateByWebSite(@PathVariable("website_id") String website_id, @PathVariable("websitecontent_id") String websitecontent_id, @RequestBody WebSiteContentDTO websitecontentdto) {
        WebSiteContent domain = websitecontentMapping.toDomain(websitecontentdto);
        domain.setWebsiteid(website_id);
        domain.setWebsitecontentid(websitecontent_id);
		websitecontentService.update(domain);
        WebSiteContentDTO dto = websitecontentMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.websitecontentService.getWebsitecontentByEntities(this.websitecontentMapping.toDomain(#websitecontentdtos)),'iBizBusinessCentral-WebSiteContent-Update')")
    @ApiOperation(value = "根据站点批量更新站点内容", tags = {"站点内容" },  notes = "根据站点批量更新站点内容")
	@RequestMapping(method = RequestMethod.PUT, value = "/websites/{website_id}/websitecontents/batch")
    public ResponseEntity<Boolean> updateBatchByWebSite(@PathVariable("website_id") String website_id, @RequestBody List<WebSiteContentDTO> websitecontentdtos) {
        List<WebSiteContent> domainlist=websitecontentMapping.toDomain(websitecontentdtos);
        for(WebSiteContent domain:domainlist){
            domain.setWebsiteid(website_id);
        }
        websitecontentService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.websitecontentService.get(#websitecontent_id),'iBizBusinessCentral-WebSiteContent-Remove')")
    @ApiOperation(value = "根据站点删除站点内容", tags = {"站点内容" },  notes = "根据站点删除站点内容")
	@RequestMapping(method = RequestMethod.DELETE, value = "/websites/{website_id}/websitecontents/{websitecontent_id}")
    public ResponseEntity<Boolean> removeByWebSite(@PathVariable("website_id") String website_id, @PathVariable("websitecontent_id") String websitecontent_id) {
		return ResponseEntity.status(HttpStatus.OK).body(websitecontentService.remove(websitecontent_id));
    }

    @PreAuthorize("hasPermission(this.websitecontentService.getWebsitecontentByIds(#ids),'iBizBusinessCentral-WebSiteContent-Remove')")
    @ApiOperation(value = "根据站点批量删除站点内容", tags = {"站点内容" },  notes = "根据站点批量删除站点内容")
	@RequestMapping(method = RequestMethod.DELETE, value = "/websites/{website_id}/websitecontents/batch")
    public ResponseEntity<Boolean> removeBatchByWebSite(@RequestBody List<String> ids) {
        websitecontentService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.websitecontentMapping.toDomain(returnObject.body),'iBizBusinessCentral-WebSiteContent-Get')")
    @ApiOperation(value = "根据站点获取站点内容", tags = {"站点内容" },  notes = "根据站点获取站点内容")
	@RequestMapping(method = RequestMethod.GET, value = "/websites/{website_id}/websitecontents/{websitecontent_id}")
    public ResponseEntity<WebSiteContentDTO> getByWebSite(@PathVariable("website_id") String website_id, @PathVariable("websitecontent_id") String websitecontent_id) {
        WebSiteContent domain = websitecontentService.get(websitecontent_id);
        WebSiteContentDTO dto = websitecontentMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据站点获取站点内容草稿", tags = {"站点内容" },  notes = "根据站点获取站点内容草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/websites/{website_id}/websitecontents/getdraft")
    public ResponseEntity<WebSiteContentDTO> getDraftByWebSite(@PathVariable("website_id") String website_id) {
        WebSiteContent domain = new WebSiteContent();
        domain.setWebsiteid(website_id);
        return ResponseEntity.status(HttpStatus.OK).body(websitecontentMapping.toDto(websitecontentService.getDraft(domain)));
    }

    @ApiOperation(value = "根据站点检查站点内容", tags = {"站点内容" },  notes = "根据站点检查站点内容")
	@RequestMapping(method = RequestMethod.POST, value = "/websites/{website_id}/websitecontents/checkkey")
    public ResponseEntity<Boolean> checkKeyByWebSite(@PathVariable("website_id") String website_id, @RequestBody WebSiteContentDTO websitecontentdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(websitecontentService.checkKey(websitecontentMapping.toDomain(websitecontentdto)));
    }

    @PreAuthorize("hasPermission(this.websitecontentMapping.toDomain(#websitecontentdto),'iBizBusinessCentral-WebSiteContent-Save')")
    @ApiOperation(value = "根据站点保存站点内容", tags = {"站点内容" },  notes = "根据站点保存站点内容")
	@RequestMapping(method = RequestMethod.POST, value = "/websites/{website_id}/websitecontents/save")
    public ResponseEntity<Boolean> saveByWebSite(@PathVariable("website_id") String website_id, @RequestBody WebSiteContentDTO websitecontentdto) {
        WebSiteContent domain = websitecontentMapping.toDomain(websitecontentdto);
        domain.setWebsiteid(website_id);
        return ResponseEntity.status(HttpStatus.OK).body(websitecontentService.save(domain));
    }

    @PreAuthorize("hasPermission(this.websitecontentMapping.toDomain(#websitecontentdtos),'iBizBusinessCentral-WebSiteContent-Save')")
    @ApiOperation(value = "根据站点批量保存站点内容", tags = {"站点内容" },  notes = "根据站点批量保存站点内容")
	@RequestMapping(method = RequestMethod.POST, value = "/websites/{website_id}/websitecontents/savebatch")
    public ResponseEntity<Boolean> saveBatchByWebSite(@PathVariable("website_id") String website_id, @RequestBody List<WebSiteContentDTO> websitecontentdtos) {
        List<WebSiteContent> domainlist=websitecontentMapping.toDomain(websitecontentdtos);
        for(WebSiteContent domain:domainlist){
             domain.setWebsiteid(website_id);
        }
        websitecontentService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-WebSiteContent-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-WebSiteContent-Get')")
	@ApiOperation(value = "根据站点获取DEFAULT", tags = {"站点内容" } ,notes = "根据站点获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/websites/{website_id}/websitecontents/fetchdefault")
	public ResponseEntity<List<WebSiteContentDTO>> fetchWebSiteContentDefaultByWebSite(@PathVariable("website_id") String website_id,WebSiteContentSearchContext context) {
        context.setN_websiteid_eq(website_id);
        Page<WebSiteContent> domains = websitecontentService.searchDefault(context) ;
        List<WebSiteContentDTO> list = websitecontentMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-WebSiteContent-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-WebSiteContent-Get')")
	@ApiOperation(value = "根据站点查询DEFAULT", tags = {"站点内容" } ,notes = "根据站点查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/websites/{website_id}/websitecontents/searchdefault")
	public ResponseEntity<Page<WebSiteContentDTO>> searchWebSiteContentDefaultByWebSite(@PathVariable("website_id") String website_id, @RequestBody WebSiteContentSearchContext context) {
        context.setN_websiteid_eq(website_id);
        Page<WebSiteContent> domains = websitecontentService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(websitecontentMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.websitecontentMapping.toDomain(#websitecontentdto),'iBizBusinessCentral-WebSiteContent-Create')")
    @ApiOperation(value = "根据站点站点频道建立站点内容", tags = {"站点内容" },  notes = "根据站点站点频道建立站点内容")
	@RequestMapping(method = RequestMethod.POST, value = "/websites/{website_id}/websitechannels/{websitechannel_id}/websitecontents")
    public ResponseEntity<WebSiteContentDTO> createByWebSiteWebSiteChannel(@PathVariable("website_id") String website_id, @PathVariable("websitechannel_id") String websitechannel_id, @RequestBody WebSiteContentDTO websitecontentdto) {
        WebSiteContent domain = websitecontentMapping.toDomain(websitecontentdto);
        domain.setWebsitechannelid(websitechannel_id);
		websitecontentService.create(domain);
        WebSiteContentDTO dto = websitecontentMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.websitecontentMapping.toDomain(#websitecontentdtos),'iBizBusinessCentral-WebSiteContent-Create')")
    @ApiOperation(value = "根据站点站点频道批量建立站点内容", tags = {"站点内容" },  notes = "根据站点站点频道批量建立站点内容")
	@RequestMapping(method = RequestMethod.POST, value = "/websites/{website_id}/websitechannels/{websitechannel_id}/websitecontents/batch")
    public ResponseEntity<Boolean> createBatchByWebSiteWebSiteChannel(@PathVariable("website_id") String website_id, @PathVariable("websitechannel_id") String websitechannel_id, @RequestBody List<WebSiteContentDTO> websitecontentdtos) {
        List<WebSiteContent> domainlist=websitecontentMapping.toDomain(websitecontentdtos);
        for(WebSiteContent domain:domainlist){
            domain.setWebsitechannelid(websitechannel_id);
        }
        websitecontentService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "websitecontent" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.websitecontentService.get(#websitecontent_id),'iBizBusinessCentral-WebSiteContent-Update')")
    @ApiOperation(value = "根据站点站点频道更新站点内容", tags = {"站点内容" },  notes = "根据站点站点频道更新站点内容")
	@RequestMapping(method = RequestMethod.PUT, value = "/websites/{website_id}/websitechannels/{websitechannel_id}/websitecontents/{websitecontent_id}")
    public ResponseEntity<WebSiteContentDTO> updateByWebSiteWebSiteChannel(@PathVariable("website_id") String website_id, @PathVariable("websitechannel_id") String websitechannel_id, @PathVariable("websitecontent_id") String websitecontent_id, @RequestBody WebSiteContentDTO websitecontentdto) {
        WebSiteContent domain = websitecontentMapping.toDomain(websitecontentdto);
        domain.setWebsitechannelid(websitechannel_id);
        domain.setWebsitecontentid(websitecontent_id);
		websitecontentService.update(domain);
        WebSiteContentDTO dto = websitecontentMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.websitecontentService.getWebsitecontentByEntities(this.websitecontentMapping.toDomain(#websitecontentdtos)),'iBizBusinessCentral-WebSiteContent-Update')")
    @ApiOperation(value = "根据站点站点频道批量更新站点内容", tags = {"站点内容" },  notes = "根据站点站点频道批量更新站点内容")
	@RequestMapping(method = RequestMethod.PUT, value = "/websites/{website_id}/websitechannels/{websitechannel_id}/websitecontents/batch")
    public ResponseEntity<Boolean> updateBatchByWebSiteWebSiteChannel(@PathVariable("website_id") String website_id, @PathVariable("websitechannel_id") String websitechannel_id, @RequestBody List<WebSiteContentDTO> websitecontentdtos) {
        List<WebSiteContent> domainlist=websitecontentMapping.toDomain(websitecontentdtos);
        for(WebSiteContent domain:domainlist){
            domain.setWebsitechannelid(websitechannel_id);
        }
        websitecontentService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.websitecontentService.get(#websitecontent_id),'iBizBusinessCentral-WebSiteContent-Remove')")
    @ApiOperation(value = "根据站点站点频道删除站点内容", tags = {"站点内容" },  notes = "根据站点站点频道删除站点内容")
	@RequestMapping(method = RequestMethod.DELETE, value = "/websites/{website_id}/websitechannels/{websitechannel_id}/websitecontents/{websitecontent_id}")
    public ResponseEntity<Boolean> removeByWebSiteWebSiteChannel(@PathVariable("website_id") String website_id, @PathVariable("websitechannel_id") String websitechannel_id, @PathVariable("websitecontent_id") String websitecontent_id) {
		return ResponseEntity.status(HttpStatus.OK).body(websitecontentService.remove(websitecontent_id));
    }

    @PreAuthorize("hasPermission(this.websitecontentService.getWebsitecontentByIds(#ids),'iBizBusinessCentral-WebSiteContent-Remove')")
    @ApiOperation(value = "根据站点站点频道批量删除站点内容", tags = {"站点内容" },  notes = "根据站点站点频道批量删除站点内容")
	@RequestMapping(method = RequestMethod.DELETE, value = "/websites/{website_id}/websitechannels/{websitechannel_id}/websitecontents/batch")
    public ResponseEntity<Boolean> removeBatchByWebSiteWebSiteChannel(@RequestBody List<String> ids) {
        websitecontentService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.websitecontentMapping.toDomain(returnObject.body),'iBizBusinessCentral-WebSiteContent-Get')")
    @ApiOperation(value = "根据站点站点频道获取站点内容", tags = {"站点内容" },  notes = "根据站点站点频道获取站点内容")
	@RequestMapping(method = RequestMethod.GET, value = "/websites/{website_id}/websitechannels/{websitechannel_id}/websitecontents/{websitecontent_id}")
    public ResponseEntity<WebSiteContentDTO> getByWebSiteWebSiteChannel(@PathVariable("website_id") String website_id, @PathVariable("websitechannel_id") String websitechannel_id, @PathVariable("websitecontent_id") String websitecontent_id) {
        WebSiteContent domain = websitecontentService.get(websitecontent_id);
        WebSiteContentDTO dto = websitecontentMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据站点站点频道获取站点内容草稿", tags = {"站点内容" },  notes = "根据站点站点频道获取站点内容草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/websites/{website_id}/websitechannels/{websitechannel_id}/websitecontents/getdraft")
    public ResponseEntity<WebSiteContentDTO> getDraftByWebSiteWebSiteChannel(@PathVariable("website_id") String website_id, @PathVariable("websitechannel_id") String websitechannel_id) {
        WebSiteContent domain = new WebSiteContent();
        domain.setWebsitechannelid(websitechannel_id);
        return ResponseEntity.status(HttpStatus.OK).body(websitecontentMapping.toDto(websitecontentService.getDraft(domain)));
    }

    @ApiOperation(value = "根据站点站点频道检查站点内容", tags = {"站点内容" },  notes = "根据站点站点频道检查站点内容")
	@RequestMapping(method = RequestMethod.POST, value = "/websites/{website_id}/websitechannels/{websitechannel_id}/websitecontents/checkkey")
    public ResponseEntity<Boolean> checkKeyByWebSiteWebSiteChannel(@PathVariable("website_id") String website_id, @PathVariable("websitechannel_id") String websitechannel_id, @RequestBody WebSiteContentDTO websitecontentdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(websitecontentService.checkKey(websitecontentMapping.toDomain(websitecontentdto)));
    }

    @PreAuthorize("hasPermission(this.websitecontentMapping.toDomain(#websitecontentdto),'iBizBusinessCentral-WebSiteContent-Save')")
    @ApiOperation(value = "根据站点站点频道保存站点内容", tags = {"站点内容" },  notes = "根据站点站点频道保存站点内容")
	@RequestMapping(method = RequestMethod.POST, value = "/websites/{website_id}/websitechannels/{websitechannel_id}/websitecontents/save")
    public ResponseEntity<Boolean> saveByWebSiteWebSiteChannel(@PathVariable("website_id") String website_id, @PathVariable("websitechannel_id") String websitechannel_id, @RequestBody WebSiteContentDTO websitecontentdto) {
        WebSiteContent domain = websitecontentMapping.toDomain(websitecontentdto);
        domain.setWebsitechannelid(websitechannel_id);
        return ResponseEntity.status(HttpStatus.OK).body(websitecontentService.save(domain));
    }

    @PreAuthorize("hasPermission(this.websitecontentMapping.toDomain(#websitecontentdtos),'iBizBusinessCentral-WebSiteContent-Save')")
    @ApiOperation(value = "根据站点站点频道批量保存站点内容", tags = {"站点内容" },  notes = "根据站点站点频道批量保存站点内容")
	@RequestMapping(method = RequestMethod.POST, value = "/websites/{website_id}/websitechannels/{websitechannel_id}/websitecontents/savebatch")
    public ResponseEntity<Boolean> saveBatchByWebSiteWebSiteChannel(@PathVariable("website_id") String website_id, @PathVariable("websitechannel_id") String websitechannel_id, @RequestBody List<WebSiteContentDTO> websitecontentdtos) {
        List<WebSiteContent> domainlist=websitecontentMapping.toDomain(websitecontentdtos);
        for(WebSiteContent domain:domainlist){
             domain.setWebsitechannelid(websitechannel_id);
        }
        websitecontentService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-WebSiteContent-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-WebSiteContent-Get')")
	@ApiOperation(value = "根据站点站点频道获取DEFAULT", tags = {"站点内容" } ,notes = "根据站点站点频道获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/websites/{website_id}/websitechannels/{websitechannel_id}/websitecontents/fetchdefault")
	public ResponseEntity<List<WebSiteContentDTO>> fetchWebSiteContentDefaultByWebSiteWebSiteChannel(@PathVariable("website_id") String website_id, @PathVariable("websitechannel_id") String websitechannel_id,WebSiteContentSearchContext context) {
        context.setN_websitechannelid_eq(websitechannel_id);
        Page<WebSiteContent> domains = websitecontentService.searchDefault(context) ;
        List<WebSiteContentDTO> list = websitecontentMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-WebSiteContent-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-WebSiteContent-Get')")
	@ApiOperation(value = "根据站点站点频道查询DEFAULT", tags = {"站点内容" } ,notes = "根据站点站点频道查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/websites/{website_id}/websitechannels/{websitechannel_id}/websitecontents/searchdefault")
	public ResponseEntity<Page<WebSiteContentDTO>> searchWebSiteContentDefaultByWebSiteWebSiteChannel(@PathVariable("website_id") String website_id, @PathVariable("websitechannel_id") String websitechannel_id, @RequestBody WebSiteContentSearchContext context) {
        context.setN_websitechannelid_eq(websitechannel_id);
        Page<WebSiteContent> domains = websitecontentService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(websitecontentMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

