package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.marketing.domain.CampaignResponse;
import cn.ibizlab.businesscentral.core.marketing.service.ICampaignResponseService;
import cn.ibizlab.businesscentral.core.marketing.filter.CampaignResponseSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"市场活动响应" })
@RestController("CentralApi-campaignresponse")
@RequestMapping("")
public class CampaignResponseResource {

    @Autowired
    public ICampaignResponseService campaignresponseService;

    @Autowired
    @Lazy
    public CampaignResponseMapping campaignresponseMapping;

    @PreAuthorize("hasPermission(this.campaignresponseMapping.toDomain(#campaignresponsedto),'iBizBusinessCentral-CampaignResponse-Create')")
    @ApiOperation(value = "新建市场活动响应", tags = {"市场活动响应" },  notes = "新建市场活动响应")
	@RequestMapping(method = RequestMethod.POST, value = "/campaignresponses")
    public ResponseEntity<CampaignResponseDTO> create(@RequestBody CampaignResponseDTO campaignresponsedto) {
        CampaignResponse domain = campaignresponseMapping.toDomain(campaignresponsedto);
		campaignresponseService.create(domain);
        CampaignResponseDTO dto = campaignresponseMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.campaignresponseMapping.toDomain(#campaignresponsedtos),'iBizBusinessCentral-CampaignResponse-Create')")
    @ApiOperation(value = "批量新建市场活动响应", tags = {"市场活动响应" },  notes = "批量新建市场活动响应")
	@RequestMapping(method = RequestMethod.POST, value = "/campaignresponses/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<CampaignResponseDTO> campaignresponsedtos) {
        campaignresponseService.createBatch(campaignresponseMapping.toDomain(campaignresponsedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "campaignresponse" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.campaignresponseService.get(#campaignresponse_id),'iBizBusinessCentral-CampaignResponse-Update')")
    @ApiOperation(value = "更新市场活动响应", tags = {"市场活动响应" },  notes = "更新市场活动响应")
	@RequestMapping(method = RequestMethod.PUT, value = "/campaignresponses/{campaignresponse_id}")
    public ResponseEntity<CampaignResponseDTO> update(@PathVariable("campaignresponse_id") String campaignresponse_id, @RequestBody CampaignResponseDTO campaignresponsedto) {
		CampaignResponse domain  = campaignresponseMapping.toDomain(campaignresponsedto);
        domain .setActivityid(campaignresponse_id);
		campaignresponseService.update(domain );
		CampaignResponseDTO dto = campaignresponseMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.campaignresponseService.getCampaignresponseByEntities(this.campaignresponseMapping.toDomain(#campaignresponsedtos)),'iBizBusinessCentral-CampaignResponse-Update')")
    @ApiOperation(value = "批量更新市场活动响应", tags = {"市场活动响应" },  notes = "批量更新市场活动响应")
	@RequestMapping(method = RequestMethod.PUT, value = "/campaignresponses/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<CampaignResponseDTO> campaignresponsedtos) {
        campaignresponseService.updateBatch(campaignresponseMapping.toDomain(campaignresponsedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.campaignresponseService.get(#campaignresponse_id),'iBizBusinessCentral-CampaignResponse-Remove')")
    @ApiOperation(value = "删除市场活动响应", tags = {"市场活动响应" },  notes = "删除市场活动响应")
	@RequestMapping(method = RequestMethod.DELETE, value = "/campaignresponses/{campaignresponse_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("campaignresponse_id") String campaignresponse_id) {
         return ResponseEntity.status(HttpStatus.OK).body(campaignresponseService.remove(campaignresponse_id));
    }

    @PreAuthorize("hasPermission(this.campaignresponseService.getCampaignresponseByIds(#ids),'iBizBusinessCentral-CampaignResponse-Remove')")
    @ApiOperation(value = "批量删除市场活动响应", tags = {"市场活动响应" },  notes = "批量删除市场活动响应")
	@RequestMapping(method = RequestMethod.DELETE, value = "/campaignresponses/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        campaignresponseService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.campaignresponseMapping.toDomain(returnObject.body),'iBizBusinessCentral-CampaignResponse-Get')")
    @ApiOperation(value = "获取市场活动响应", tags = {"市场活动响应" },  notes = "获取市场活动响应")
	@RequestMapping(method = RequestMethod.GET, value = "/campaignresponses/{campaignresponse_id}")
    public ResponseEntity<CampaignResponseDTO> get(@PathVariable("campaignresponse_id") String campaignresponse_id) {
        CampaignResponse domain = campaignresponseService.get(campaignresponse_id);
        CampaignResponseDTO dto = campaignresponseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取市场活动响应草稿", tags = {"市场活动响应" },  notes = "获取市场活动响应草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/campaignresponses/getdraft")
    public ResponseEntity<CampaignResponseDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(campaignresponseMapping.toDto(campaignresponseService.getDraft(new CampaignResponse())));
    }

    @ApiOperation(value = "检查市场活动响应", tags = {"市场活动响应" },  notes = "检查市场活动响应")
	@RequestMapping(method = RequestMethod.POST, value = "/campaignresponses/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody CampaignResponseDTO campaignresponsedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(campaignresponseService.checkKey(campaignresponseMapping.toDomain(campaignresponsedto)));
    }

    @PreAuthorize("hasPermission(this.campaignresponseMapping.toDomain(#campaignresponsedto),'iBizBusinessCentral-CampaignResponse-Save')")
    @ApiOperation(value = "保存市场活动响应", tags = {"市场活动响应" },  notes = "保存市场活动响应")
	@RequestMapping(method = RequestMethod.POST, value = "/campaignresponses/save")
    public ResponseEntity<Boolean> save(@RequestBody CampaignResponseDTO campaignresponsedto) {
        return ResponseEntity.status(HttpStatus.OK).body(campaignresponseService.save(campaignresponseMapping.toDomain(campaignresponsedto)));
    }

    @PreAuthorize("hasPermission(this.campaignresponseMapping.toDomain(#campaignresponsedtos),'iBizBusinessCentral-CampaignResponse-Save')")
    @ApiOperation(value = "批量保存市场活动响应", tags = {"市场活动响应" },  notes = "批量保存市场活动响应")
	@RequestMapping(method = RequestMethod.POST, value = "/campaignresponses/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<CampaignResponseDTO> campaignresponsedtos) {
        campaignresponseService.saveBatch(campaignresponseMapping.toDomain(campaignresponsedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-CampaignResponse-searchByParentKey-all') and hasPermission(#context,'iBizBusinessCentral-CampaignResponse-Get')")
	@ApiOperation(value = "获取ByParentKey", tags = {"市场活动响应" } ,notes = "获取ByParentKey")
    @RequestMapping(method= RequestMethod.GET , value="/campaignresponses/fetchbyparentkey")
	public ResponseEntity<List<CampaignResponseDTO>> fetchByParentKey(CampaignResponseSearchContext context) {
        Page<CampaignResponse> domains = campaignresponseService.searchByParentKey(context) ;
        List<CampaignResponseDTO> list = campaignresponseMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-CampaignResponse-searchByParentKey-all') and hasPermission(#context,'iBizBusinessCentral-CampaignResponse-Get')")
	@ApiOperation(value = "查询ByParentKey", tags = {"市场活动响应" } ,notes = "查询ByParentKey")
    @RequestMapping(method= RequestMethod.POST , value="/campaignresponses/searchbyparentkey")
	public ResponseEntity<Page<CampaignResponseDTO>> searchByParentKey(@RequestBody CampaignResponseSearchContext context) {
        Page<CampaignResponse> domains = campaignresponseService.searchByParentKey(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(campaignresponseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-CampaignResponse-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-CampaignResponse-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"市场活动响应" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/campaignresponses/fetchdefault")
	public ResponseEntity<List<CampaignResponseDTO>> fetchDefault(CampaignResponseSearchContext context) {
        Page<CampaignResponse> domains = campaignresponseService.searchDefault(context) ;
        List<CampaignResponseDTO> list = campaignresponseMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-CampaignResponse-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-CampaignResponse-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"市场活动响应" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/campaignresponses/searchdefault")
	public ResponseEntity<Page<CampaignResponseDTO>> searchDefault(@RequestBody CampaignResponseSearchContext context) {
        Page<CampaignResponse> domains = campaignresponseService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(campaignresponseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

