package cn.ibizlab.businesscentral.centralapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.product.domain.ProductSalesLiterature;
import cn.ibizlab.businesscentral.centralapi.dto.ProductSalesLiteratureDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CentralApiProductSalesLiteratureMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface ProductSalesLiteratureMapping extends MappingBase<ProductSalesLiteratureDTO, ProductSalesLiterature> {


}

