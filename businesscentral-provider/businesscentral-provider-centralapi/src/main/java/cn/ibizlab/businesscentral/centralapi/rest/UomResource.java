package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.base.domain.Uom;
import cn.ibizlab.businesscentral.core.base.service.IUomService;
import cn.ibizlab.businesscentral.core.base.filter.UomSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"计价单位" })
@RestController("CentralApi-uom")
@RequestMapping("")
public class UomResource {

    @Autowired
    public IUomService uomService;

    @Autowired
    @Lazy
    public UomMapping uomMapping;

    @PreAuthorize("hasPermission(this.uomMapping.toDomain(#uomdto),'iBizBusinessCentral-Uom-Create')")
    @ApiOperation(value = "新建计价单位", tags = {"计价单位" },  notes = "新建计价单位")
	@RequestMapping(method = RequestMethod.POST, value = "/uoms")
    public ResponseEntity<UomDTO> create(@RequestBody UomDTO uomdto) {
        Uom domain = uomMapping.toDomain(uomdto);
		uomService.create(domain);
        UomDTO dto = uomMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.uomMapping.toDomain(#uomdtos),'iBizBusinessCentral-Uom-Create')")
    @ApiOperation(value = "批量新建计价单位", tags = {"计价单位" },  notes = "批量新建计价单位")
	@RequestMapping(method = RequestMethod.POST, value = "/uoms/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<UomDTO> uomdtos) {
        uomService.createBatch(uomMapping.toDomain(uomdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "uom" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.uomService.get(#uom_id),'iBizBusinessCentral-Uom-Update')")
    @ApiOperation(value = "更新计价单位", tags = {"计价单位" },  notes = "更新计价单位")
	@RequestMapping(method = RequestMethod.PUT, value = "/uoms/{uom_id}")
    public ResponseEntity<UomDTO> update(@PathVariable("uom_id") String uom_id, @RequestBody UomDTO uomdto) {
		Uom domain  = uomMapping.toDomain(uomdto);
        domain .setUomid(uom_id);
		uomService.update(domain );
		UomDTO dto = uomMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.uomService.getUomByEntities(this.uomMapping.toDomain(#uomdtos)),'iBizBusinessCentral-Uom-Update')")
    @ApiOperation(value = "批量更新计价单位", tags = {"计价单位" },  notes = "批量更新计价单位")
	@RequestMapping(method = RequestMethod.PUT, value = "/uoms/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<UomDTO> uomdtos) {
        uomService.updateBatch(uomMapping.toDomain(uomdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.uomService.get(#uom_id),'iBizBusinessCentral-Uom-Remove')")
    @ApiOperation(value = "删除计价单位", tags = {"计价单位" },  notes = "删除计价单位")
	@RequestMapping(method = RequestMethod.DELETE, value = "/uoms/{uom_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("uom_id") String uom_id) {
         return ResponseEntity.status(HttpStatus.OK).body(uomService.remove(uom_id));
    }

    @PreAuthorize("hasPermission(this.uomService.getUomByIds(#ids),'iBizBusinessCentral-Uom-Remove')")
    @ApiOperation(value = "批量删除计价单位", tags = {"计价单位" },  notes = "批量删除计价单位")
	@RequestMapping(method = RequestMethod.DELETE, value = "/uoms/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        uomService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.uomMapping.toDomain(returnObject.body),'iBizBusinessCentral-Uom-Get')")
    @ApiOperation(value = "获取计价单位", tags = {"计价单位" },  notes = "获取计价单位")
	@RequestMapping(method = RequestMethod.GET, value = "/uoms/{uom_id}")
    public ResponseEntity<UomDTO> get(@PathVariable("uom_id") String uom_id) {
        Uom domain = uomService.get(uom_id);
        UomDTO dto = uomMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取计价单位草稿", tags = {"计价单位" },  notes = "获取计价单位草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/uoms/getdraft")
    public ResponseEntity<UomDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(uomMapping.toDto(uomService.getDraft(new Uom())));
    }

    @ApiOperation(value = "检查计价单位", tags = {"计价单位" },  notes = "检查计价单位")
	@RequestMapping(method = RequestMethod.POST, value = "/uoms/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody UomDTO uomdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(uomService.checkKey(uomMapping.toDomain(uomdto)));
    }

    @PreAuthorize("hasPermission(this.uomMapping.toDomain(#uomdto),'iBizBusinessCentral-Uom-Save')")
    @ApiOperation(value = "保存计价单位", tags = {"计价单位" },  notes = "保存计价单位")
	@RequestMapping(method = RequestMethod.POST, value = "/uoms/save")
    public ResponseEntity<Boolean> save(@RequestBody UomDTO uomdto) {
        return ResponseEntity.status(HttpStatus.OK).body(uomService.save(uomMapping.toDomain(uomdto)));
    }

    @PreAuthorize("hasPermission(this.uomMapping.toDomain(#uomdtos),'iBizBusinessCentral-Uom-Save')")
    @ApiOperation(value = "批量保存计价单位", tags = {"计价单位" },  notes = "批量保存计价单位")
	@RequestMapping(method = RequestMethod.POST, value = "/uoms/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<UomDTO> uomdtos) {
        uomService.saveBatch(uomMapping.toDomain(uomdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Uom-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Uom-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"计价单位" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/uoms/fetchdefault")
	public ResponseEntity<List<UomDTO>> fetchDefault(UomSearchContext context) {
        Page<Uom> domains = uomService.searchDefault(context) ;
        List<UomDTO> list = uomMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Uom-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Uom-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"计价单位" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/uoms/searchdefault")
	public ResponseEntity<Page<UomDTO>> searchDefault(@RequestBody UomSearchContext context) {
        Page<Uom> domains = uomService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(uomMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

