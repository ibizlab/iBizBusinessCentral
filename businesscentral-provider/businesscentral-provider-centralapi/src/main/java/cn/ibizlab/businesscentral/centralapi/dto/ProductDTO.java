package cn.ibizlab.businesscentral.centralapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[ProductDTO]
 */
@Data
public class ProductDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [VENDORID]
     *
     */
    @JSONField(name = "vendorid")
    @JsonProperty("vendorid")
    private String vendorid;

    /**
     * 属性 [PRODUCTSTRUCTURE]
     *
     */
    @JSONField(name = "productstructure")
    @JsonProperty("productstructure")
    private String productstructure;

    /**
     * 属性 [VERSIONNUMBER]
     *
     */
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;

    /**
     * 属性 [STAGEID]
     *
     */
    @JSONField(name = "stageid")
    @JsonProperty("stageid")
    private String stageid;

    /**
     * 属性 [STATUSCODE]
     *
     */
    @JSONField(name = "statuscode")
    @JsonProperty("statuscode")
    private Integer statuscode;

    /**
     * 属性 [ENTITYIMAGE]
     *
     */
    @JSONField(name = "entityimage")
    @JsonProperty("entityimage")
    private String entityimage;

    /**
     * 属性 [OVERRIDDENCREATEDON]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;

    /**
     * 属性 [STOCKITEM]
     *
     */
    @JSONField(name = "stockitem")
    @JsonProperty("stockitem")
    private Integer stockitem;

    /**
     * 属性 [IMPORTSEQUENCENUMBER]
     *
     */
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;

    /**
     * 属性 [VALIDFROMDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "validfromdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("validfromdate")
    private Timestamp validfromdate;

    /**
     * 属性 [SUPPLIERNAME]
     *
     */
    @JSONField(name = "suppliername")
    @JsonProperty("suppliername")
    private String suppliername;

    /**
     * 属性 [PRODUCTTYPECODE]
     *
     */
    @JSONField(name = "producttypecode")
    @JsonProperty("producttypecode")
    private String producttypecode;

    /**
     * 属性 [ENTITYIMAGEID]
     *
     */
    @JSONField(name = "entityimageid")
    @JsonProperty("entityimageid")
    private String entityimageid;

    /**
     * 属性 [ENTITYIMAGE_TIMESTAMP]
     *
     */
    @JSONField(name = "entityimage_timestamp")
    @JsonProperty("entityimage_timestamp")
    private BigInteger entityimageTimestamp;

    /**
     * 属性 [PRODUCTURL]
     *
     */
    @JSONField(name = "producturl")
    @JsonProperty("producturl")
    private String producturl;

    /**
     * 属性 [STANDARDCOST]
     *
     */
    @JSONField(name = "standardcost")
    @JsonProperty("standardcost")
    private BigDecimal standardcost;

    /**
     * 属性 [QUANTITYONHAND]
     *
     */
    @JSONField(name = "quantityonhand")
    @JsonProperty("quantityonhand")
    private BigDecimal quantityonhand;

    /**
     * 属性 [CURRENTCOST]
     *
     */
    @JSONField(name = "currentcost")
    @JsonProperty("currentcost")
    private BigDecimal currentcost;

    /**
     * 属性 [EXCHANGERATE]
     *
     */
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;

    /**
     * 属性 [PRODUCTID]
     *
     */
    @JSONField(name = "productid")
    @JsonProperty("productid")
    private String productid;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 属性 [VENDORNAME]
     *
     */
    @JSONField(name = "vendorname")
    @JsonProperty("vendorname")
    private String vendorname;

    /**
     * 属性 [DMTIMPORTSTATE]
     *
     */
    @JSONField(name = "dmtimportstate")
    @JsonProperty("dmtimportstate")
    private Integer dmtimportstate;

    /**
     * 属性 [ENTITYIMAGE_URL]
     *
     */
    @JSONField(name = "entityimage_url")
    @JsonProperty("entityimage_url")
    private String entityimageUrl;

    /**
     * 属性 [STOCKVOLUME]
     *
     */
    @JSONField(name = "stockvolume")
    @JsonProperty("stockvolume")
    private BigDecimal stockvolume;

    /**
     * 属性 [QUANTITYDECIMAL]
     *
     */
    @JSONField(name = "quantitydecimal")
    @JsonProperty("quantitydecimal")
    private Integer quantitydecimal;

    /**
     * 属性 [VENDORPARTNUMBER]
     *
     */
    @JSONField(name = "vendorpartnumber")
    @JsonProperty("vendorpartnumber")
    private String vendorpartnumber;

    /**
     * 属性 [VALIDTODATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "validtodate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("validtodate")
    private Timestamp validtodate;

    /**
     * 属性 [REPARENTED]
     *
     */
    @JSONField(name = "reparented")
    @JsonProperty("reparented")
    private Integer reparented;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;

    /**
     * 属性 [PROCESSID]
     *
     */
    @JSONField(name = "processid")
    @JsonProperty("processid")
    private String processid;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [PRODUCTNUMBER]
     *
     */
    @JSONField(name = "productnumber")
    @JsonProperty("productnumber")
    private String productnumber;

    /**
     * 属性 [KIT]
     *
     */
    @JSONField(name = "kit")
    @JsonProperty("kit")
    private Integer kit;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [CURRENTCOST_BASE]
     *
     */
    @JSONField(name = "currentcost_base")
    @JsonProperty("currentcost_base")
    private BigDecimal currentcostBase;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;

    /**
     * 属性 [STOCKWEIGHT]
     *
     */
    @JSONField(name = "stockweight")
    @JsonProperty("stockweight")
    private BigDecimal stockweight;

    /**
     * 属性 [SIZE]
     *
     */
    @JSONField(name = "size")
    @JsonProperty("size")
    private String size;

    /**
     * 属性 [TIMEZONERULEVERSIONNUMBER]
     *
     */
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;

    /**
     * 属性 [UTCCONVERSIONTIMEZONECODE]
     *
     */
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;

    /**
     * 属性 [PRODUCTNAME]
     *
     */
    @JSONField(name = "productname")
    @JsonProperty("productname")
    private String productname;

    /**
     * 属性 [PRICE_BASE]
     *
     */
    @JSONField(name = "price_base")
    @JsonProperty("price_base")
    private BigDecimal priceBase;

    /**
     * 属性 [PRICE]
     *
     */
    @JSONField(name = "price")
    @JsonProperty("price")
    private BigDecimal price;

    /**
     * 属性 [TRAVERSEDPATH]
     *
     */
    @JSONField(name = "traversedpath")
    @JsonProperty("traversedpath")
    private String traversedpath;

    /**
     * 属性 [HIERARCHYPATH]
     *
     */
    @JSONField(name = "hierarchypath")
    @JsonProperty("hierarchypath")
    private String hierarchypath;

    /**
     * 属性 [STANDARDCOST_BASE]
     *
     */
    @JSONField(name = "standardcost_base")
    @JsonProperty("standardcost_base")
    private BigDecimal standardcostBase;

    /**
     * 属性 [STATECODE]
     *
     */
    @JSONField(name = "statecode")
    @JsonProperty("statecode")
    private Integer statecode;

    /**
     * 属性 [DEFAULTUOMSCHEDULENAME]
     *
     */
    @JSONField(name = "defaultuomschedulename")
    @JsonProperty("defaultuomschedulename")
    private String defaultuomschedulename;

    /**
     * 属性 [SUBJECTNAME]
     *
     */
    @JSONField(name = "subjectname")
    @JsonProperty("subjectname")
    private String subjectname;

    /**
     * 属性 [CURRENCYNAME]
     *
     */
    @JSONField(name = "currencyname")
    @JsonProperty("currencyname")
    private String currencyname;

    /**
     * 属性 [PRICELEVELNAME]
     *
     */
    @JSONField(name = "pricelevelname")
    @JsonProperty("pricelevelname")
    private String pricelevelname;

    /**
     * 属性 [DEFAULTUOMNAME]
     *
     */
    @JSONField(name = "defaultuomname")
    @JsonProperty("defaultuomname")
    private String defaultuomname;

    /**
     * 属性 [PARENTPRODUCTNAME]
     *
     */
    @JSONField(name = "parentproductname")
    @JsonProperty("parentproductname")
    private String parentproductname;

    /**
     * 属性 [PARENTPRODUCTID]
     *
     */
    @JSONField(name = "parentproductid")
    @JsonProperty("parentproductid")
    private String parentproductid;

    /**
     * 属性 [TRANSACTIONCURRENCYID]
     *
     */
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;

    /**
     * 属性 [PRICELEVELID]
     *
     */
    @JSONField(name = "pricelevelid")
    @JsonProperty("pricelevelid")
    private String pricelevelid;

    /**
     * 属性 [DEFAULTUOMID]
     *
     */
    @JSONField(name = "defaultuomid")
    @JsonProperty("defaultuomid")
    private String defaultuomid;

    /**
     * 属性 [DEFAULTUOMSCHEDULEID]
     *
     */
    @JSONField(name = "defaultuomscheduleid")
    @JsonProperty("defaultuomscheduleid")
    private String defaultuomscheduleid;

    /**
     * 属性 [SUBJECTID]
     *
     */
    @JSONField(name = "subjectid")
    @JsonProperty("subjectid")
    private String subjectid;


    /**
     * 设置 [VENDORID]
     */
    public void setVendorid(String  vendorid){
        this.vendorid = vendorid ;
        this.modify("vendorid",vendorid);
    }

    /**
     * 设置 [PRODUCTSTRUCTURE]
     */
    public void setProductstructure(String  productstructure){
        this.productstructure = productstructure ;
        this.modify("productstructure",productstructure);
    }

    /**
     * 设置 [VERSIONNUMBER]
     */
    public void setVersionnumber(BigInteger  versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [STAGEID]
     */
    public void setStageid(String  stageid){
        this.stageid = stageid ;
        this.modify("stageid",stageid);
    }

    /**
     * 设置 [STATUSCODE]
     */
    public void setStatuscode(Integer  statuscode){
        this.statuscode = statuscode ;
        this.modify("statuscode",statuscode);
    }

    /**
     * 设置 [ENTITYIMAGE]
     */
    public void setEntityimage(String  entityimage){
        this.entityimage = entityimage ;
        this.modify("entityimage",entityimage);
    }

    /**
     * 设置 [OVERRIDDENCREATEDON]
     */
    public void setOverriddencreatedon(Timestamp  overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 设置 [STOCKITEM]
     */
    public void setStockitem(Integer  stockitem){
        this.stockitem = stockitem ;
        this.modify("stockitem",stockitem);
    }

    /**
     * 设置 [IMPORTSEQUENCENUMBER]
     */
    public void setImportsequencenumber(Integer  importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [VALIDFROMDATE]
     */
    public void setValidfromdate(Timestamp  validfromdate){
        this.validfromdate = validfromdate ;
        this.modify("validfromdate",validfromdate);
    }

    /**
     * 设置 [SUPPLIERNAME]
     */
    public void setSuppliername(String  suppliername){
        this.suppliername = suppliername ;
        this.modify("suppliername",suppliername);
    }

    /**
     * 设置 [PRODUCTTYPECODE]
     */
    public void setProducttypecode(String  producttypecode){
        this.producttypecode = producttypecode ;
        this.modify("producttypecode",producttypecode);
    }

    /**
     * 设置 [ENTITYIMAGEID]
     */
    public void setEntityimageid(String  entityimageid){
        this.entityimageid = entityimageid ;
        this.modify("entityimageid",entityimageid);
    }

    /**
     * 设置 [ENTITYIMAGE_TIMESTAMP]
     */
    public void setEntityimageTimestamp(BigInteger  entityimageTimestamp){
        this.entityimageTimestamp = entityimageTimestamp ;
        this.modify("entityimage_timestamp",entityimageTimestamp);
    }

    /**
     * 设置 [PRODUCTURL]
     */
    public void setProducturl(String  producturl){
        this.producturl = producturl ;
        this.modify("producturl",producturl);
    }

    /**
     * 设置 [STANDARDCOST]
     */
    public void setStandardcost(BigDecimal  standardcost){
        this.standardcost = standardcost ;
        this.modify("standardcost",standardcost);
    }

    /**
     * 设置 [QUANTITYONHAND]
     */
    public void setQuantityonhand(BigDecimal  quantityonhand){
        this.quantityonhand = quantityonhand ;
        this.modify("quantityonhand",quantityonhand);
    }

    /**
     * 设置 [CURRENTCOST]
     */
    public void setCurrentcost(BigDecimal  currentcost){
        this.currentcost = currentcost ;
        this.modify("currentcost",currentcost);
    }

    /**
     * 设置 [EXCHANGERATE]
     */
    public void setExchangerate(BigDecimal  exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [VENDORNAME]
     */
    public void setVendorname(String  vendorname){
        this.vendorname = vendorname ;
        this.modify("vendorname",vendorname);
    }

    /**
     * 设置 [DMTIMPORTSTATE]
     */
    public void setDmtimportstate(Integer  dmtimportstate){
        this.dmtimportstate = dmtimportstate ;
        this.modify("dmtimportstate",dmtimportstate);
    }

    /**
     * 设置 [ENTITYIMAGE_URL]
     */
    public void setEntityimageUrl(String  entityimageUrl){
        this.entityimageUrl = entityimageUrl ;
        this.modify("entityimage_url",entityimageUrl);
    }

    /**
     * 设置 [STOCKVOLUME]
     */
    public void setStockvolume(BigDecimal  stockvolume){
        this.stockvolume = stockvolume ;
        this.modify("stockvolume",stockvolume);
    }

    /**
     * 设置 [QUANTITYDECIMAL]
     */
    public void setQuantitydecimal(Integer  quantitydecimal){
        this.quantitydecimal = quantitydecimal ;
        this.modify("quantitydecimal",quantitydecimal);
    }

    /**
     * 设置 [VENDORPARTNUMBER]
     */
    public void setVendorpartnumber(String  vendorpartnumber){
        this.vendorpartnumber = vendorpartnumber ;
        this.modify("vendorpartnumber",vendorpartnumber);
    }

    /**
     * 设置 [VALIDTODATE]
     */
    public void setValidtodate(Timestamp  validtodate){
        this.validtodate = validtodate ;
        this.modify("validtodate",validtodate);
    }

    /**
     * 设置 [REPARENTED]
     */
    public void setReparented(Integer  reparented){
        this.reparented = reparented ;
        this.modify("reparented",reparented);
    }

    /**
     * 设置 [PROCESSID]
     */
    public void setProcessid(String  processid){
        this.processid = processid ;
        this.modify("processid",processid);
    }

    /**
     * 设置 [PRODUCTNUMBER]
     */
    public void setProductnumber(String  productnumber){
        this.productnumber = productnumber ;
        this.modify("productnumber",productnumber);
    }

    /**
     * 设置 [KIT]
     */
    public void setKit(Integer  kit){
        this.kit = kit ;
        this.modify("kit",kit);
    }

    /**
     * 设置 [CURRENTCOST_BASE]
     */
    public void setCurrentcostBase(BigDecimal  currentcostBase){
        this.currentcostBase = currentcostBase ;
        this.modify("currentcost_base",currentcostBase);
    }

    /**
     * 设置 [STOCKWEIGHT]
     */
    public void setStockweight(BigDecimal  stockweight){
        this.stockweight = stockweight ;
        this.modify("stockweight",stockweight);
    }

    /**
     * 设置 [SIZE]
     */
    public void setSize(String  size){
        this.size = size ;
        this.modify("size",size);
    }

    /**
     * 设置 [TIMEZONERULEVERSIONNUMBER]
     */
    public void setTimezoneruleversionnumber(Integer  timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [UTCCONVERSIONTIMEZONECODE]
     */
    public void setUtcconversiontimezonecode(Integer  utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [PRODUCTNAME]
     */
    public void setProductname(String  productname){
        this.productname = productname ;
        this.modify("productname",productname);
    }

    /**
     * 设置 [PRICE_BASE]
     */
    public void setPriceBase(BigDecimal  priceBase){
        this.priceBase = priceBase ;
        this.modify("price_base",priceBase);
    }

    /**
     * 设置 [PRICE]
     */
    public void setPrice(BigDecimal  price){
        this.price = price ;
        this.modify("price",price);
    }

    /**
     * 设置 [TRAVERSEDPATH]
     */
    public void setTraversedpath(String  traversedpath){
        this.traversedpath = traversedpath ;
        this.modify("traversedpath",traversedpath);
    }

    /**
     * 设置 [HIERARCHYPATH]
     */
    public void setHierarchypath(String  hierarchypath){
        this.hierarchypath = hierarchypath ;
        this.modify("hierarchypath",hierarchypath);
    }

    /**
     * 设置 [STANDARDCOST_BASE]
     */
    public void setStandardcostBase(BigDecimal  standardcostBase){
        this.standardcostBase = standardcostBase ;
        this.modify("standardcost_base",standardcostBase);
    }

    /**
     * 设置 [STATECODE]
     */
    public void setStatecode(Integer  statecode){
        this.statecode = statecode ;
        this.modify("statecode",statecode);
    }

    /**
     * 设置 [DEFAULTUOMSCHEDULENAME]
     */
    public void setDefaultuomschedulename(String  defaultuomschedulename){
        this.defaultuomschedulename = defaultuomschedulename ;
        this.modify("defaultuomschedulename",defaultuomschedulename);
    }

    /**
     * 设置 [SUBJECTNAME]
     */
    public void setSubjectname(String  subjectname){
        this.subjectname = subjectname ;
        this.modify("subjectname",subjectname);
    }

    /**
     * 设置 [CURRENCYNAME]
     */
    public void setCurrencyname(String  currencyname){
        this.currencyname = currencyname ;
        this.modify("currencyname",currencyname);
    }

    /**
     * 设置 [PRICELEVELNAME]
     */
    public void setPricelevelname(String  pricelevelname){
        this.pricelevelname = pricelevelname ;
        this.modify("pricelevelname",pricelevelname);
    }

    /**
     * 设置 [DEFAULTUOMNAME]
     */
    public void setDefaultuomname(String  defaultuomname){
        this.defaultuomname = defaultuomname ;
        this.modify("defaultuomname",defaultuomname);
    }

    /**
     * 设置 [PARENTPRODUCTNAME]
     */
    public void setParentproductname(String  parentproductname){
        this.parentproductname = parentproductname ;
        this.modify("parentproductname",parentproductname);
    }

    /**
     * 设置 [PARENTPRODUCTID]
     */
    public void setParentproductid(String  parentproductid){
        this.parentproductid = parentproductid ;
        this.modify("parentproductid",parentproductid);
    }

    /**
     * 设置 [TRANSACTIONCURRENCYID]
     */
    public void setTransactioncurrencyid(String  transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }

    /**
     * 设置 [PRICELEVELID]
     */
    public void setPricelevelid(String  pricelevelid){
        this.pricelevelid = pricelevelid ;
        this.modify("pricelevelid",pricelevelid);
    }

    /**
     * 设置 [DEFAULTUOMID]
     */
    public void setDefaultuomid(String  defaultuomid){
        this.defaultuomid = defaultuomid ;
        this.modify("defaultuomid",defaultuomid);
    }

    /**
     * 设置 [DEFAULTUOMSCHEDULEID]
     */
    public void setDefaultuomscheduleid(String  defaultuomscheduleid){
        this.defaultuomscheduleid = defaultuomscheduleid ;
        this.modify("defaultuomscheduleid",defaultuomscheduleid);
    }

    /**
     * 设置 [SUBJECTID]
     */
    public void setSubjectid(String  subjectid){
        this.subjectid = subjectid ;
        this.modify("subjectid",subjectid);
    }


}

