package cn.ibizlab.businesscentral.centralapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.workflow.domain.WFProcessDefinition;
import cn.ibizlab.businesscentral.centralapi.dto.WFProcessDefinitionDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CentralApiWFProcessDefinitionMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface WFProcessDefinitionMapping extends MappingBase<WFProcessDefinitionDTO, WFProcessDefinition> {


}

