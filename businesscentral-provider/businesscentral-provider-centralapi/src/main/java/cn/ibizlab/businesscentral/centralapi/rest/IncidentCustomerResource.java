package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.service.domain.IncidentCustomer;
import cn.ibizlab.businesscentral.core.service.service.IIncidentCustomerService;
import cn.ibizlab.businesscentral.core.service.filter.IncidentCustomerSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"案例客户" })
@RestController("CentralApi-incidentcustomer")
@RequestMapping("")
public class IncidentCustomerResource {

    @Autowired
    public IIncidentCustomerService incidentcustomerService;

    @Autowired
    @Lazy
    public IncidentCustomerMapping incidentcustomerMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-IncidentCustomer-Create-all')")
    @ApiOperation(value = "新建案例客户", tags = {"案例客户" },  notes = "新建案例客户")
	@RequestMapping(method = RequestMethod.POST, value = "/incidentcustomers")
    public ResponseEntity<IncidentCustomerDTO> create(@RequestBody IncidentCustomerDTO incidentcustomerdto) {
        IncidentCustomer domain = incidentcustomerMapping.toDomain(incidentcustomerdto);
		incidentcustomerService.create(domain);
        IncidentCustomerDTO dto = incidentcustomerMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-IncidentCustomer-Create-all')")
    @ApiOperation(value = "批量新建案例客户", tags = {"案例客户" },  notes = "批量新建案例客户")
	@RequestMapping(method = RequestMethod.POST, value = "/incidentcustomers/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<IncidentCustomerDTO> incidentcustomerdtos) {
        incidentcustomerService.createBatch(incidentcustomerMapping.toDomain(incidentcustomerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-IncidentCustomer-Update-all')")
    @ApiOperation(value = "更新案例客户", tags = {"案例客户" },  notes = "更新案例客户")
	@RequestMapping(method = RequestMethod.PUT, value = "/incidentcustomers/{incidentcustomer_id}")
    public ResponseEntity<IncidentCustomerDTO> update(@PathVariable("incidentcustomer_id") String incidentcustomer_id, @RequestBody IncidentCustomerDTO incidentcustomerdto) {
		IncidentCustomer domain  = incidentcustomerMapping.toDomain(incidentcustomerdto);
        domain .setCustomerid(incidentcustomer_id);
		incidentcustomerService.update(domain );
		IncidentCustomerDTO dto = incidentcustomerMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-IncidentCustomer-Update-all')")
    @ApiOperation(value = "批量更新案例客户", tags = {"案例客户" },  notes = "批量更新案例客户")
	@RequestMapping(method = RequestMethod.PUT, value = "/incidentcustomers/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<IncidentCustomerDTO> incidentcustomerdtos) {
        incidentcustomerService.updateBatch(incidentcustomerMapping.toDomain(incidentcustomerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-IncidentCustomer-Remove-all')")
    @ApiOperation(value = "删除案例客户", tags = {"案例客户" },  notes = "删除案例客户")
	@RequestMapping(method = RequestMethod.DELETE, value = "/incidentcustomers/{incidentcustomer_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("incidentcustomer_id") String incidentcustomer_id) {
         return ResponseEntity.status(HttpStatus.OK).body(incidentcustomerService.remove(incidentcustomer_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-IncidentCustomer-Remove-all')")
    @ApiOperation(value = "批量删除案例客户", tags = {"案例客户" },  notes = "批量删除案例客户")
	@RequestMapping(method = RequestMethod.DELETE, value = "/incidentcustomers/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        incidentcustomerService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-IncidentCustomer-Get-all')")
    @ApiOperation(value = "获取案例客户", tags = {"案例客户" },  notes = "获取案例客户")
	@RequestMapping(method = RequestMethod.GET, value = "/incidentcustomers/{incidentcustomer_id}")
    public ResponseEntity<IncidentCustomerDTO> get(@PathVariable("incidentcustomer_id") String incidentcustomer_id) {
        IncidentCustomer domain = incidentcustomerService.get(incidentcustomer_id);
        IncidentCustomerDTO dto = incidentcustomerMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取案例客户草稿", tags = {"案例客户" },  notes = "获取案例客户草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/incidentcustomers/getdraft")
    public ResponseEntity<IncidentCustomerDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(incidentcustomerMapping.toDto(incidentcustomerService.getDraft(new IncidentCustomer())));
    }

    @ApiOperation(value = "检查案例客户", tags = {"案例客户" },  notes = "检查案例客户")
	@RequestMapping(method = RequestMethod.POST, value = "/incidentcustomers/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody IncidentCustomerDTO incidentcustomerdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(incidentcustomerService.checkKey(incidentcustomerMapping.toDomain(incidentcustomerdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-IncidentCustomer-Save-all')")
    @ApiOperation(value = "保存案例客户", tags = {"案例客户" },  notes = "保存案例客户")
	@RequestMapping(method = RequestMethod.POST, value = "/incidentcustomers/save")
    public ResponseEntity<Boolean> save(@RequestBody IncidentCustomerDTO incidentcustomerdto) {
        return ResponseEntity.status(HttpStatus.OK).body(incidentcustomerService.save(incidentcustomerMapping.toDomain(incidentcustomerdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-IncidentCustomer-Save-all')")
    @ApiOperation(value = "批量保存案例客户", tags = {"案例客户" },  notes = "批量保存案例客户")
	@RequestMapping(method = RequestMethod.POST, value = "/incidentcustomers/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<IncidentCustomerDTO> incidentcustomerdtos) {
        incidentcustomerService.saveBatch(incidentcustomerMapping.toDomain(incidentcustomerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-IncidentCustomer-searchDefault-all')")
	@ApiOperation(value = "获取DEFAULT", tags = {"案例客户" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/incidentcustomers/fetchdefault")
	public ResponseEntity<List<IncidentCustomerDTO>> fetchDefault(IncidentCustomerSearchContext context) {
        Page<IncidentCustomer> domains = incidentcustomerService.searchDefault(context) ;
        List<IncidentCustomerDTO> list = incidentcustomerMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-IncidentCustomer-searchDefault-all')")
	@ApiOperation(value = "查询DEFAULT", tags = {"案例客户" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/incidentcustomers/searchdefault")
	public ResponseEntity<Page<IncidentCustomerDTO>> searchDefault(@RequestBody IncidentCustomerSearchContext context) {
        Page<IncidentCustomer> domains = incidentcustomerService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(incidentcustomerMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-IncidentCustomer-searchIndexDER-all')")
	@ApiOperation(value = "获取IndexDER", tags = {"案例客户" } ,notes = "获取IndexDER")
    @RequestMapping(method= RequestMethod.GET , value="/incidentcustomers/fetchindexder")
	public ResponseEntity<List<IncidentCustomerDTO>> fetchIndexDER(IncidentCustomerSearchContext context) {
        Page<IncidentCustomer> domains = incidentcustomerService.searchIndexDER(context) ;
        List<IncidentCustomerDTO> list = incidentcustomerMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-IncidentCustomer-searchIndexDER-all')")
	@ApiOperation(value = "查询IndexDER", tags = {"案例客户" } ,notes = "查询IndexDER")
    @RequestMapping(method= RequestMethod.POST , value="/incidentcustomers/searchindexder")
	public ResponseEntity<Page<IncidentCustomerDTO>> searchIndexDER(@RequestBody IncidentCustomerSearchContext context) {
        Page<IncidentCustomer> domains = incidentcustomerService.searchIndexDER(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(incidentcustomerMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

