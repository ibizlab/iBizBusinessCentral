package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.website.domain.WebSiteChannel;
import cn.ibizlab.businesscentral.core.website.service.IWebSiteChannelService;
import cn.ibizlab.businesscentral.core.website.filter.WebSiteChannelSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"站点频道" })
@RestController("CentralApi-websitechannel")
@RequestMapping("")
public class WebSiteChannelResource {

    @Autowired
    public IWebSiteChannelService websitechannelService;

    @Autowired
    @Lazy
    public WebSiteChannelMapping websitechannelMapping;

    @PreAuthorize("hasPermission(this.websitechannelMapping.toDomain(#websitechanneldto),'iBizBusinessCentral-WebSiteChannel-Create')")
    @ApiOperation(value = "新建站点频道", tags = {"站点频道" },  notes = "新建站点频道")
	@RequestMapping(method = RequestMethod.POST, value = "/websitechannels")
    public ResponseEntity<WebSiteChannelDTO> create(@RequestBody WebSiteChannelDTO websitechanneldto) {
        WebSiteChannel domain = websitechannelMapping.toDomain(websitechanneldto);
		websitechannelService.create(domain);
        WebSiteChannelDTO dto = websitechannelMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.websitechannelMapping.toDomain(#websitechanneldtos),'iBizBusinessCentral-WebSiteChannel-Create')")
    @ApiOperation(value = "批量新建站点频道", tags = {"站点频道" },  notes = "批量新建站点频道")
	@RequestMapping(method = RequestMethod.POST, value = "/websitechannels/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<WebSiteChannelDTO> websitechanneldtos) {
        websitechannelService.createBatch(websitechannelMapping.toDomain(websitechanneldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "websitechannel" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.websitechannelService.get(#websitechannel_id),'iBizBusinessCentral-WebSiteChannel-Update')")
    @ApiOperation(value = "更新站点频道", tags = {"站点频道" },  notes = "更新站点频道")
	@RequestMapping(method = RequestMethod.PUT, value = "/websitechannels/{websitechannel_id}")
    public ResponseEntity<WebSiteChannelDTO> update(@PathVariable("websitechannel_id") String websitechannel_id, @RequestBody WebSiteChannelDTO websitechanneldto) {
		WebSiteChannel domain  = websitechannelMapping.toDomain(websitechanneldto);
        domain .setWebsitechannelid(websitechannel_id);
		websitechannelService.update(domain );
		WebSiteChannelDTO dto = websitechannelMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.websitechannelService.getWebsitechannelByEntities(this.websitechannelMapping.toDomain(#websitechanneldtos)),'iBizBusinessCentral-WebSiteChannel-Update')")
    @ApiOperation(value = "批量更新站点频道", tags = {"站点频道" },  notes = "批量更新站点频道")
	@RequestMapping(method = RequestMethod.PUT, value = "/websitechannels/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<WebSiteChannelDTO> websitechanneldtos) {
        websitechannelService.updateBatch(websitechannelMapping.toDomain(websitechanneldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.websitechannelService.get(#websitechannel_id),'iBizBusinessCentral-WebSiteChannel-Remove')")
    @ApiOperation(value = "删除站点频道", tags = {"站点频道" },  notes = "删除站点频道")
	@RequestMapping(method = RequestMethod.DELETE, value = "/websitechannels/{websitechannel_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("websitechannel_id") String websitechannel_id) {
         return ResponseEntity.status(HttpStatus.OK).body(websitechannelService.remove(websitechannel_id));
    }

    @PreAuthorize("hasPermission(this.websitechannelService.getWebsitechannelByIds(#ids),'iBizBusinessCentral-WebSiteChannel-Remove')")
    @ApiOperation(value = "批量删除站点频道", tags = {"站点频道" },  notes = "批量删除站点频道")
	@RequestMapping(method = RequestMethod.DELETE, value = "/websitechannels/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        websitechannelService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.websitechannelMapping.toDomain(returnObject.body),'iBizBusinessCentral-WebSiteChannel-Get')")
    @ApiOperation(value = "获取站点频道", tags = {"站点频道" },  notes = "获取站点频道")
	@RequestMapping(method = RequestMethod.GET, value = "/websitechannels/{websitechannel_id}")
    public ResponseEntity<WebSiteChannelDTO> get(@PathVariable("websitechannel_id") String websitechannel_id) {
        WebSiteChannel domain = websitechannelService.get(websitechannel_id);
        WebSiteChannelDTO dto = websitechannelMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取站点频道草稿", tags = {"站点频道" },  notes = "获取站点频道草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/websitechannels/getdraft")
    public ResponseEntity<WebSiteChannelDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(websitechannelMapping.toDto(websitechannelService.getDraft(new WebSiteChannel())));
    }

    @ApiOperation(value = "检查站点频道", tags = {"站点频道" },  notes = "检查站点频道")
	@RequestMapping(method = RequestMethod.POST, value = "/websitechannels/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody WebSiteChannelDTO websitechanneldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(websitechannelService.checkKey(websitechannelMapping.toDomain(websitechanneldto)));
    }

    @PreAuthorize("hasPermission(this.websitechannelMapping.toDomain(#websitechanneldto),'iBizBusinessCentral-WebSiteChannel-Save')")
    @ApiOperation(value = "保存站点频道", tags = {"站点频道" },  notes = "保存站点频道")
	@RequestMapping(method = RequestMethod.POST, value = "/websitechannels/save")
    public ResponseEntity<Boolean> save(@RequestBody WebSiteChannelDTO websitechanneldto) {
        return ResponseEntity.status(HttpStatus.OK).body(websitechannelService.save(websitechannelMapping.toDomain(websitechanneldto)));
    }

    @PreAuthorize("hasPermission(this.websitechannelMapping.toDomain(#websitechanneldtos),'iBizBusinessCentral-WebSiteChannel-Save')")
    @ApiOperation(value = "批量保存站点频道", tags = {"站点频道" },  notes = "批量保存站点频道")
	@RequestMapping(method = RequestMethod.POST, value = "/websitechannels/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<WebSiteChannelDTO> websitechanneldtos) {
        websitechannelService.saveBatch(websitechannelMapping.toDomain(websitechanneldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-WebSiteChannel-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-WebSiteChannel-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"站点频道" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/websitechannels/fetchdefault")
	public ResponseEntity<List<WebSiteChannelDTO>> fetchDefault(WebSiteChannelSearchContext context) {
        Page<WebSiteChannel> domains = websitechannelService.searchDefault(context) ;
        List<WebSiteChannelDTO> list = websitechannelMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-WebSiteChannel-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-WebSiteChannel-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"站点频道" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/websitechannels/searchdefault")
	public ResponseEntity<Page<WebSiteChannelDTO>> searchDefault(@RequestBody WebSiteChannelSearchContext context) {
        Page<WebSiteChannel> domains = websitechannelService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(websitechannelMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-WebSiteChannel-searchRoot-all') and hasPermission(#context,'iBizBusinessCentral-WebSiteChannel-Get')")
	@ApiOperation(value = "获取Root", tags = {"站点频道" } ,notes = "获取Root")
    @RequestMapping(method= RequestMethod.GET , value="/websitechannels/fetchroot")
	public ResponseEntity<List<WebSiteChannelDTO>> fetchRoot(WebSiteChannelSearchContext context) {
        Page<WebSiteChannel> domains = websitechannelService.searchRoot(context) ;
        List<WebSiteChannelDTO> list = websitechannelMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-WebSiteChannel-searchRoot-all') and hasPermission(#context,'iBizBusinessCentral-WebSiteChannel-Get')")
	@ApiOperation(value = "查询Root", tags = {"站点频道" } ,notes = "查询Root")
    @RequestMapping(method= RequestMethod.POST , value="/websitechannels/searchroot")
	public ResponseEntity<Page<WebSiteChannelDTO>> searchRoot(@RequestBody WebSiteChannelSearchContext context) {
        Page<WebSiteChannel> domains = websitechannelService.searchRoot(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(websitechannelMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.websitechannelMapping.toDomain(#websitechanneldto),'iBizBusinessCentral-WebSiteChannel-Create')")
    @ApiOperation(value = "根据站点建立站点频道", tags = {"站点频道" },  notes = "根据站点建立站点频道")
	@RequestMapping(method = RequestMethod.POST, value = "/websites/{website_id}/websitechannels")
    public ResponseEntity<WebSiteChannelDTO> createByWebSite(@PathVariable("website_id") String website_id, @RequestBody WebSiteChannelDTO websitechanneldto) {
        WebSiteChannel domain = websitechannelMapping.toDomain(websitechanneldto);
        domain.setWebsiteid(website_id);
		websitechannelService.create(domain);
        WebSiteChannelDTO dto = websitechannelMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.websitechannelMapping.toDomain(#websitechanneldtos),'iBizBusinessCentral-WebSiteChannel-Create')")
    @ApiOperation(value = "根据站点批量建立站点频道", tags = {"站点频道" },  notes = "根据站点批量建立站点频道")
	@RequestMapping(method = RequestMethod.POST, value = "/websites/{website_id}/websitechannels/batch")
    public ResponseEntity<Boolean> createBatchByWebSite(@PathVariable("website_id") String website_id, @RequestBody List<WebSiteChannelDTO> websitechanneldtos) {
        List<WebSiteChannel> domainlist=websitechannelMapping.toDomain(websitechanneldtos);
        for(WebSiteChannel domain:domainlist){
            domain.setWebsiteid(website_id);
        }
        websitechannelService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "websitechannel" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.websitechannelService.get(#websitechannel_id),'iBizBusinessCentral-WebSiteChannel-Update')")
    @ApiOperation(value = "根据站点更新站点频道", tags = {"站点频道" },  notes = "根据站点更新站点频道")
	@RequestMapping(method = RequestMethod.PUT, value = "/websites/{website_id}/websitechannels/{websitechannel_id}")
    public ResponseEntity<WebSiteChannelDTO> updateByWebSite(@PathVariable("website_id") String website_id, @PathVariable("websitechannel_id") String websitechannel_id, @RequestBody WebSiteChannelDTO websitechanneldto) {
        WebSiteChannel domain = websitechannelMapping.toDomain(websitechanneldto);
        domain.setWebsiteid(website_id);
        domain.setWebsitechannelid(websitechannel_id);
		websitechannelService.update(domain);
        WebSiteChannelDTO dto = websitechannelMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.websitechannelService.getWebsitechannelByEntities(this.websitechannelMapping.toDomain(#websitechanneldtos)),'iBizBusinessCentral-WebSiteChannel-Update')")
    @ApiOperation(value = "根据站点批量更新站点频道", tags = {"站点频道" },  notes = "根据站点批量更新站点频道")
	@RequestMapping(method = RequestMethod.PUT, value = "/websites/{website_id}/websitechannels/batch")
    public ResponseEntity<Boolean> updateBatchByWebSite(@PathVariable("website_id") String website_id, @RequestBody List<WebSiteChannelDTO> websitechanneldtos) {
        List<WebSiteChannel> domainlist=websitechannelMapping.toDomain(websitechanneldtos);
        for(WebSiteChannel domain:domainlist){
            domain.setWebsiteid(website_id);
        }
        websitechannelService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.websitechannelService.get(#websitechannel_id),'iBizBusinessCentral-WebSiteChannel-Remove')")
    @ApiOperation(value = "根据站点删除站点频道", tags = {"站点频道" },  notes = "根据站点删除站点频道")
	@RequestMapping(method = RequestMethod.DELETE, value = "/websites/{website_id}/websitechannels/{websitechannel_id}")
    public ResponseEntity<Boolean> removeByWebSite(@PathVariable("website_id") String website_id, @PathVariable("websitechannel_id") String websitechannel_id) {
		return ResponseEntity.status(HttpStatus.OK).body(websitechannelService.remove(websitechannel_id));
    }

    @PreAuthorize("hasPermission(this.websitechannelService.getWebsitechannelByIds(#ids),'iBizBusinessCentral-WebSiteChannel-Remove')")
    @ApiOperation(value = "根据站点批量删除站点频道", tags = {"站点频道" },  notes = "根据站点批量删除站点频道")
	@RequestMapping(method = RequestMethod.DELETE, value = "/websites/{website_id}/websitechannels/batch")
    public ResponseEntity<Boolean> removeBatchByWebSite(@RequestBody List<String> ids) {
        websitechannelService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.websitechannelMapping.toDomain(returnObject.body),'iBizBusinessCentral-WebSiteChannel-Get')")
    @ApiOperation(value = "根据站点获取站点频道", tags = {"站点频道" },  notes = "根据站点获取站点频道")
	@RequestMapping(method = RequestMethod.GET, value = "/websites/{website_id}/websitechannels/{websitechannel_id}")
    public ResponseEntity<WebSiteChannelDTO> getByWebSite(@PathVariable("website_id") String website_id, @PathVariable("websitechannel_id") String websitechannel_id) {
        WebSiteChannel domain = websitechannelService.get(websitechannel_id);
        WebSiteChannelDTO dto = websitechannelMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据站点获取站点频道草稿", tags = {"站点频道" },  notes = "根据站点获取站点频道草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/websites/{website_id}/websitechannels/getdraft")
    public ResponseEntity<WebSiteChannelDTO> getDraftByWebSite(@PathVariable("website_id") String website_id) {
        WebSiteChannel domain = new WebSiteChannel();
        domain.setWebsiteid(website_id);
        return ResponseEntity.status(HttpStatus.OK).body(websitechannelMapping.toDto(websitechannelService.getDraft(domain)));
    }

    @ApiOperation(value = "根据站点检查站点频道", tags = {"站点频道" },  notes = "根据站点检查站点频道")
	@RequestMapping(method = RequestMethod.POST, value = "/websites/{website_id}/websitechannels/checkkey")
    public ResponseEntity<Boolean> checkKeyByWebSite(@PathVariable("website_id") String website_id, @RequestBody WebSiteChannelDTO websitechanneldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(websitechannelService.checkKey(websitechannelMapping.toDomain(websitechanneldto)));
    }

    @PreAuthorize("hasPermission(this.websitechannelMapping.toDomain(#websitechanneldto),'iBizBusinessCentral-WebSiteChannel-Save')")
    @ApiOperation(value = "根据站点保存站点频道", tags = {"站点频道" },  notes = "根据站点保存站点频道")
	@RequestMapping(method = RequestMethod.POST, value = "/websites/{website_id}/websitechannels/save")
    public ResponseEntity<Boolean> saveByWebSite(@PathVariable("website_id") String website_id, @RequestBody WebSiteChannelDTO websitechanneldto) {
        WebSiteChannel domain = websitechannelMapping.toDomain(websitechanneldto);
        domain.setWebsiteid(website_id);
        return ResponseEntity.status(HttpStatus.OK).body(websitechannelService.save(domain));
    }

    @PreAuthorize("hasPermission(this.websitechannelMapping.toDomain(#websitechanneldtos),'iBizBusinessCentral-WebSiteChannel-Save')")
    @ApiOperation(value = "根据站点批量保存站点频道", tags = {"站点频道" },  notes = "根据站点批量保存站点频道")
	@RequestMapping(method = RequestMethod.POST, value = "/websites/{website_id}/websitechannels/savebatch")
    public ResponseEntity<Boolean> saveBatchByWebSite(@PathVariable("website_id") String website_id, @RequestBody List<WebSiteChannelDTO> websitechanneldtos) {
        List<WebSiteChannel> domainlist=websitechannelMapping.toDomain(websitechanneldtos);
        for(WebSiteChannel domain:domainlist){
             domain.setWebsiteid(website_id);
        }
        websitechannelService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-WebSiteChannel-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-WebSiteChannel-Get')")
	@ApiOperation(value = "根据站点获取DEFAULT", tags = {"站点频道" } ,notes = "根据站点获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/websites/{website_id}/websitechannels/fetchdefault")
	public ResponseEntity<List<WebSiteChannelDTO>> fetchWebSiteChannelDefaultByWebSite(@PathVariable("website_id") String website_id,WebSiteChannelSearchContext context) {
        context.setN_websiteid_eq(website_id);
        Page<WebSiteChannel> domains = websitechannelService.searchDefault(context) ;
        List<WebSiteChannelDTO> list = websitechannelMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-WebSiteChannel-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-WebSiteChannel-Get')")
	@ApiOperation(value = "根据站点查询DEFAULT", tags = {"站点频道" } ,notes = "根据站点查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/websites/{website_id}/websitechannels/searchdefault")
	public ResponseEntity<Page<WebSiteChannelDTO>> searchWebSiteChannelDefaultByWebSite(@PathVariable("website_id") String website_id, @RequestBody WebSiteChannelSearchContext context) {
        context.setN_websiteid_eq(website_id);
        Page<WebSiteChannel> domains = websitechannelService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(websitechannelMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-WebSiteChannel-searchRoot-all') and hasPermission(#context,'iBizBusinessCentral-WebSiteChannel-Get')")
	@ApiOperation(value = "根据站点获取Root", tags = {"站点频道" } ,notes = "根据站点获取Root")
    @RequestMapping(method= RequestMethod.GET , value="/websites/{website_id}/websitechannels/fetchroot")
	public ResponseEntity<List<WebSiteChannelDTO>> fetchWebSiteChannelRootByWebSite(@PathVariable("website_id") String website_id,WebSiteChannelSearchContext context) {
        context.setN_websiteid_eq(website_id);
        Page<WebSiteChannel> domains = websitechannelService.searchRoot(context) ;
        List<WebSiteChannelDTO> list = websitechannelMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-WebSiteChannel-searchRoot-all') and hasPermission(#context,'iBizBusinessCentral-WebSiteChannel-Get')")
	@ApiOperation(value = "根据站点查询Root", tags = {"站点频道" } ,notes = "根据站点查询Root")
    @RequestMapping(method= RequestMethod.POST , value="/websites/{website_id}/websitechannels/searchroot")
	public ResponseEntity<Page<WebSiteChannelDTO>> searchWebSiteChannelRootByWebSite(@PathVariable("website_id") String website_id, @RequestBody WebSiteChannelSearchContext context) {
        context.setN_websiteid_eq(website_id);
        Page<WebSiteChannel> domains = websitechannelService.searchRoot(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(websitechannelMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

