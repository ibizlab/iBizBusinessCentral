package cn.ibizlab.businesscentral.centralapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[CampaignDTO]
 */
@Data
public class CampaignDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [EXPECTEDRESPONSE]
     *
     */
    @JSONField(name = "expectedresponse")
    @JsonProperty("expectedresponse")
    private Integer expectedresponse;

    /**
     * 属性 [TYPECODE]
     *
     */
    @JSONField(name = "typecode")
    @JsonProperty("typecode")
    private String typecode;

    /**
     * 属性 [BUDGETEDCOST]
     *
     */
    @JSONField(name = "budgetedcost")
    @JsonProperty("budgetedcost")
    private BigDecimal budgetedcost;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [OWNERNAME]
     *
     */
    @JSONField(name = "ownername")
    @JsonProperty("ownername")
    private String ownername;

    /**
     * 属性 [TOTALACTUALCOST]
     *
     */
    @JSONField(name = "totalactualcost")
    @JsonProperty("totalactualcost")
    private BigDecimal totalactualcost;

    /**
     * 属性 [UTCCONVERSIONTIMEZONECODE]
     *
     */
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;

    /**
     * 属性 [IMPORTSEQUENCENUMBER]
     *
     */
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;

    /**
     * 属性 [OTHERCOST]
     *
     */
    @JSONField(name = "othercost")
    @JsonProperty("othercost")
    private BigDecimal othercost;

    /**
     * 属性 [TIMEZONERULEVERSIONNUMBER]
     *
     */
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;

    /**
     * 属性 [CAMPAIGNNAME]
     *
     */
    @JSONField(name = "campaignname")
    @JsonProperty("campaignname")
    private String campaignname;

    /**
     * 属性 [ENTITYIMAGE_URL]
     *
     */
    @JSONField(name = "entityimage_url")
    @JsonProperty("entityimage_url")
    private String entityimageUrl;

    /**
     * 属性 [STATECODE]
     *
     */
    @JSONField(name = "statecode")
    @JsonProperty("statecode")
    private Integer statecode;

    /**
     * 属性 [TEMPLATE]
     *
     */
    @JSONField(name = "template")
    @JsonProperty("template")
    private Integer template;

    /**
     * 属性 [OWNERTYPE]
     *
     */
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;

    /**
     * 属性 [EXCHANGERATE]
     *
     */
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;

    /**
     * 属性 [CAMPAIGNID]
     *
     */
    @JSONField(name = "campaignid")
    @JsonProperty("campaignid")
    private String campaignid;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;

    /**
     * 属性 [OBJECTIVE]
     *
     */
    @JSONField(name = "objective")
    @JsonProperty("objective")
    private String objective;

    /**
     * 属性 [BUDGETEDCOST_BASE]
     *
     */
    @JSONField(name = "budgetedcost_base")
    @JsonProperty("budgetedcost_base")
    private BigDecimal budgetedcostBase;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;

    /**
     * 属性 [EXPECTEDREVENUE_BASE]
     *
     */
    @JSONField(name = "expectedrevenue_base")
    @JsonProperty("expectedrevenue_base")
    private BigDecimal expectedrevenueBase;

    /**
     * 属性 [ENTITYIMAGE_TIMESTAMP]
     *
     */
    @JSONField(name = "entityimage_timestamp")
    @JsonProperty("entityimage_timestamp")
    private BigInteger entityimageTimestamp;

    /**
     * 属性 [TRAVERSEDPATH]
     *
     */
    @JSONField(name = "traversedpath")
    @JsonProperty("traversedpath")
    private String traversedpath;

    /**
     * 属性 [OTHERCOST_BASE]
     *
     */
    @JSONField(name = "othercost_base")
    @JsonProperty("othercost_base")
    private BigDecimal othercostBase;

    /**
     * 属性 [PROPOSEDEND]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "proposedend" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("proposedend")
    private Timestamp proposedend;

    /**
     * 属性 [VERSIONNUMBER]
     *
     */
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;

    /**
     * 属性 [ENTITYIMAGE]
     *
     */
    @JSONField(name = "entityimage")
    @JsonProperty("entityimage")
    private String entityimage;

    /**
     * 属性 [CODENAME]
     *
     */
    @JSONField(name = "codename")
    @JsonProperty("codename")
    private String codename;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 属性 [ACTUALEND]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "actualend" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("actualend")
    private Timestamp actualend;

    /**
     * 属性 [EXPECTEDREVENUE]
     *
     */
    @JSONField(name = "expectedrevenue")
    @JsonProperty("expectedrevenue")
    private BigDecimal expectedrevenue;

    /**
     * 属性 [PRICELISTNAME]
     *
     */
    @JSONField(name = "pricelistname")
    @JsonProperty("pricelistname")
    private String pricelistname;

    /**
     * 属性 [ENTITYIMAGEID]
     *
     */
    @JSONField(name = "entityimageid")
    @JsonProperty("entityimageid")
    private String entityimageid;

    /**
     * 属性 [PROCESSID]
     *
     */
    @JSONField(name = "processid")
    @JsonProperty("processid")
    private String processid;

    /**
     * 属性 [ACTUALSTART]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "actualstart" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("actualstart")
    private Timestamp actualstart;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [OWNERID]
     *
     */
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;

    /**
     * 属性 [MESSAGE]
     *
     */
    @JSONField(name = "message")
    @JsonProperty("message")
    private String message;

    /**
     * 属性 [STAGEID]
     *
     */
    @JSONField(name = "stageid")
    @JsonProperty("stageid")
    private String stageid;

    /**
     * 属性 [STATUSCODE]
     *
     */
    @JSONField(name = "statuscode")
    @JsonProperty("statuscode")
    private Integer statuscode;

    /**
     * 属性 [OVERRIDDENCREATEDON]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;

    /**
     * 属性 [PROPOSEDSTART]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "proposedstart" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("proposedstart")
    private Timestamp proposedstart;

    /**
     * 属性 [TOTALACTUALCOST_BASE]
     *
     */
    @JSONField(name = "totalactualcost_base")
    @JsonProperty("totalactualcost_base")
    private BigDecimal totalactualcostBase;

    /**
     * 属性 [EMAILADDRESS]
     *
     */
    @JSONField(name = "emailaddress")
    @JsonProperty("emailaddress")
    private String emailaddress;

    /**
     * 属性 [PROMOTIONCODENAME]
     *
     */
    @JSONField(name = "promotioncodename")
    @JsonProperty("promotioncodename")
    private String promotioncodename;

    /**
     * 属性 [CURRENCYNAME]
     *
     */
    @JSONField(name = "currencyname")
    @JsonProperty("currencyname")
    private String currencyname;

    /**
     * 属性 [PRICELISTID]
     *
     */
    @JSONField(name = "pricelistid")
    @JsonProperty("pricelistid")
    private String pricelistid;

    /**
     * 属性 [TRANSACTIONCURRENCYID]
     *
     */
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;


    /**
     * 设置 [EXPECTEDRESPONSE]
     */
    public void setExpectedresponse(Integer  expectedresponse){
        this.expectedresponse = expectedresponse ;
        this.modify("expectedresponse",expectedresponse);
    }

    /**
     * 设置 [TYPECODE]
     */
    public void setTypecode(String  typecode){
        this.typecode = typecode ;
        this.modify("typecode",typecode);
    }

    /**
     * 设置 [BUDGETEDCOST]
     */
    public void setBudgetedcost(BigDecimal  budgetedcost){
        this.budgetedcost = budgetedcost ;
        this.modify("budgetedcost",budgetedcost);
    }

    /**
     * 设置 [OWNERNAME]
     */
    public void setOwnername(String  ownername){
        this.ownername = ownername ;
        this.modify("ownername",ownername);
    }

    /**
     * 设置 [TOTALACTUALCOST]
     */
    public void setTotalactualcost(BigDecimal  totalactualcost){
        this.totalactualcost = totalactualcost ;
        this.modify("totalactualcost",totalactualcost);
    }

    /**
     * 设置 [UTCCONVERSIONTIMEZONECODE]
     */
    public void setUtcconversiontimezonecode(Integer  utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [IMPORTSEQUENCENUMBER]
     */
    public void setImportsequencenumber(Integer  importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [OTHERCOST]
     */
    public void setOthercost(BigDecimal  othercost){
        this.othercost = othercost ;
        this.modify("othercost",othercost);
    }

    /**
     * 设置 [TIMEZONERULEVERSIONNUMBER]
     */
    public void setTimezoneruleversionnumber(Integer  timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [CAMPAIGNNAME]
     */
    public void setCampaignname(String  campaignname){
        this.campaignname = campaignname ;
        this.modify("campaignname",campaignname);
    }

    /**
     * 设置 [ENTITYIMAGE_URL]
     */
    public void setEntityimageUrl(String  entityimageUrl){
        this.entityimageUrl = entityimageUrl ;
        this.modify("entityimage_url",entityimageUrl);
    }

    /**
     * 设置 [STATECODE]
     */
    public void setStatecode(Integer  statecode){
        this.statecode = statecode ;
        this.modify("statecode",statecode);
    }

    /**
     * 设置 [TEMPLATE]
     */
    public void setTemplate(Integer  template){
        this.template = template ;
        this.modify("template",template);
    }

    /**
     * 设置 [OWNERTYPE]
     */
    public void setOwnertype(String  ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [EXCHANGERATE]
     */
    public void setExchangerate(BigDecimal  exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [OBJECTIVE]
     */
    public void setObjective(String  objective){
        this.objective = objective ;
        this.modify("objective",objective);
    }

    /**
     * 设置 [BUDGETEDCOST_BASE]
     */
    public void setBudgetedcostBase(BigDecimal  budgetedcostBase){
        this.budgetedcostBase = budgetedcostBase ;
        this.modify("budgetedcost_base",budgetedcostBase);
    }

    /**
     * 设置 [EXPECTEDREVENUE_BASE]
     */
    public void setExpectedrevenueBase(BigDecimal  expectedrevenueBase){
        this.expectedrevenueBase = expectedrevenueBase ;
        this.modify("expectedrevenue_base",expectedrevenueBase);
    }

    /**
     * 设置 [ENTITYIMAGE_TIMESTAMP]
     */
    public void setEntityimageTimestamp(BigInteger  entityimageTimestamp){
        this.entityimageTimestamp = entityimageTimestamp ;
        this.modify("entityimage_timestamp",entityimageTimestamp);
    }

    /**
     * 设置 [TRAVERSEDPATH]
     */
    public void setTraversedpath(String  traversedpath){
        this.traversedpath = traversedpath ;
        this.modify("traversedpath",traversedpath);
    }

    /**
     * 设置 [OTHERCOST_BASE]
     */
    public void setOthercostBase(BigDecimal  othercostBase){
        this.othercostBase = othercostBase ;
        this.modify("othercost_base",othercostBase);
    }

    /**
     * 设置 [PROPOSEDEND]
     */
    public void setProposedend(Timestamp  proposedend){
        this.proposedend = proposedend ;
        this.modify("proposedend",proposedend);
    }

    /**
     * 设置 [VERSIONNUMBER]
     */
    public void setVersionnumber(BigInteger  versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [ENTITYIMAGE]
     */
    public void setEntityimage(String  entityimage){
        this.entityimage = entityimage ;
        this.modify("entityimage",entityimage);
    }

    /**
     * 设置 [CODENAME]
     */
    public void setCodename(String  codename){
        this.codename = codename ;
        this.modify("codename",codename);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [ACTUALEND]
     */
    public void setActualend(Timestamp  actualend){
        this.actualend = actualend ;
        this.modify("actualend",actualend);
    }

    /**
     * 设置 [EXPECTEDREVENUE]
     */
    public void setExpectedrevenue(BigDecimal  expectedrevenue){
        this.expectedrevenue = expectedrevenue ;
        this.modify("expectedrevenue",expectedrevenue);
    }

    /**
     * 设置 [PRICELISTNAME]
     */
    public void setPricelistname(String  pricelistname){
        this.pricelistname = pricelistname ;
        this.modify("pricelistname",pricelistname);
    }

    /**
     * 设置 [ENTITYIMAGEID]
     */
    public void setEntityimageid(String  entityimageid){
        this.entityimageid = entityimageid ;
        this.modify("entityimageid",entityimageid);
    }

    /**
     * 设置 [PROCESSID]
     */
    public void setProcessid(String  processid){
        this.processid = processid ;
        this.modify("processid",processid);
    }

    /**
     * 设置 [ACTUALSTART]
     */
    public void setActualstart(Timestamp  actualstart){
        this.actualstart = actualstart ;
        this.modify("actualstart",actualstart);
    }

    /**
     * 设置 [OWNERID]
     */
    public void setOwnerid(String  ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [MESSAGE]
     */
    public void setMessage(String  message){
        this.message = message ;
        this.modify("message",message);
    }

    /**
     * 设置 [STAGEID]
     */
    public void setStageid(String  stageid){
        this.stageid = stageid ;
        this.modify("stageid",stageid);
    }

    /**
     * 设置 [STATUSCODE]
     */
    public void setStatuscode(Integer  statuscode){
        this.statuscode = statuscode ;
        this.modify("statuscode",statuscode);
    }

    /**
     * 设置 [OVERRIDDENCREATEDON]
     */
    public void setOverriddencreatedon(Timestamp  overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 设置 [PROPOSEDSTART]
     */
    public void setProposedstart(Timestamp  proposedstart){
        this.proposedstart = proposedstart ;
        this.modify("proposedstart",proposedstart);
    }

    /**
     * 设置 [TOTALACTUALCOST_BASE]
     */
    public void setTotalactualcostBase(BigDecimal  totalactualcostBase){
        this.totalactualcostBase = totalactualcostBase ;
        this.modify("totalactualcost_base",totalactualcostBase);
    }

    /**
     * 设置 [EMAILADDRESS]
     */
    public void setEmailaddress(String  emailaddress){
        this.emailaddress = emailaddress ;
        this.modify("emailaddress",emailaddress);
    }

    /**
     * 设置 [PROMOTIONCODENAME]
     */
    public void setPromotioncodename(String  promotioncodename){
        this.promotioncodename = promotioncodename ;
        this.modify("promotioncodename",promotioncodename);
    }

    /**
     * 设置 [CURRENCYNAME]
     */
    public void setCurrencyname(String  currencyname){
        this.currencyname = currencyname ;
        this.modify("currencyname",currencyname);
    }

    /**
     * 设置 [PRICELISTID]
     */
    public void setPricelistid(String  pricelistid){
        this.pricelistid = pricelistid ;
        this.modify("pricelistid",pricelistid);
    }

    /**
     * 设置 [TRANSACTIONCURRENCYID]
     */
    public void setTransactioncurrencyid(String  transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }


}

