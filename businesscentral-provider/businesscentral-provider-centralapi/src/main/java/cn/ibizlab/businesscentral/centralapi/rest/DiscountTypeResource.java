package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.sales.domain.DiscountType;
import cn.ibizlab.businesscentral.core.sales.service.IDiscountTypeService;
import cn.ibizlab.businesscentral.core.sales.filter.DiscountTypeSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"折扣表" })
@RestController("CentralApi-discounttype")
@RequestMapping("")
public class DiscountTypeResource {

    @Autowired
    public IDiscountTypeService discounttypeService;

    @Autowired
    @Lazy
    public DiscountTypeMapping discounttypeMapping;

    @PreAuthorize("hasPermission(this.discounttypeMapping.toDomain(#discounttypedto),'iBizBusinessCentral-DiscountType-Create')")
    @ApiOperation(value = "新建折扣表", tags = {"折扣表" },  notes = "新建折扣表")
	@RequestMapping(method = RequestMethod.POST, value = "/discounttypes")
    public ResponseEntity<DiscountTypeDTO> create(@RequestBody DiscountTypeDTO discounttypedto) {
        DiscountType domain = discounttypeMapping.toDomain(discounttypedto);
		discounttypeService.create(domain);
        DiscountTypeDTO dto = discounttypeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.discounttypeMapping.toDomain(#discounttypedtos),'iBizBusinessCentral-DiscountType-Create')")
    @ApiOperation(value = "批量新建折扣表", tags = {"折扣表" },  notes = "批量新建折扣表")
	@RequestMapping(method = RequestMethod.POST, value = "/discounttypes/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<DiscountTypeDTO> discounttypedtos) {
        discounttypeService.createBatch(discounttypeMapping.toDomain(discounttypedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "discounttype" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.discounttypeService.get(#discounttype_id),'iBizBusinessCentral-DiscountType-Update')")
    @ApiOperation(value = "更新折扣表", tags = {"折扣表" },  notes = "更新折扣表")
	@RequestMapping(method = RequestMethod.PUT, value = "/discounttypes/{discounttype_id}")
    public ResponseEntity<DiscountTypeDTO> update(@PathVariable("discounttype_id") String discounttype_id, @RequestBody DiscountTypeDTO discounttypedto) {
		DiscountType domain  = discounttypeMapping.toDomain(discounttypedto);
        domain .setDiscounttypeid(discounttype_id);
		discounttypeService.update(domain );
		DiscountTypeDTO dto = discounttypeMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.discounttypeService.getDiscounttypeByEntities(this.discounttypeMapping.toDomain(#discounttypedtos)),'iBizBusinessCentral-DiscountType-Update')")
    @ApiOperation(value = "批量更新折扣表", tags = {"折扣表" },  notes = "批量更新折扣表")
	@RequestMapping(method = RequestMethod.PUT, value = "/discounttypes/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<DiscountTypeDTO> discounttypedtos) {
        discounttypeService.updateBatch(discounttypeMapping.toDomain(discounttypedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.discounttypeService.get(#discounttype_id),'iBizBusinessCentral-DiscountType-Remove')")
    @ApiOperation(value = "删除折扣表", tags = {"折扣表" },  notes = "删除折扣表")
	@RequestMapping(method = RequestMethod.DELETE, value = "/discounttypes/{discounttype_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("discounttype_id") String discounttype_id) {
         return ResponseEntity.status(HttpStatus.OK).body(discounttypeService.remove(discounttype_id));
    }

    @PreAuthorize("hasPermission(this.discounttypeService.getDiscounttypeByIds(#ids),'iBizBusinessCentral-DiscountType-Remove')")
    @ApiOperation(value = "批量删除折扣表", tags = {"折扣表" },  notes = "批量删除折扣表")
	@RequestMapping(method = RequestMethod.DELETE, value = "/discounttypes/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        discounttypeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.discounttypeMapping.toDomain(returnObject.body),'iBizBusinessCentral-DiscountType-Get')")
    @ApiOperation(value = "获取折扣表", tags = {"折扣表" },  notes = "获取折扣表")
	@RequestMapping(method = RequestMethod.GET, value = "/discounttypes/{discounttype_id}")
    public ResponseEntity<DiscountTypeDTO> get(@PathVariable("discounttype_id") String discounttype_id) {
        DiscountType domain = discounttypeService.get(discounttype_id);
        DiscountTypeDTO dto = discounttypeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取折扣表草稿", tags = {"折扣表" },  notes = "获取折扣表草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/discounttypes/getdraft")
    public ResponseEntity<DiscountTypeDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(discounttypeMapping.toDto(discounttypeService.getDraft(new DiscountType())));
    }

    @ApiOperation(value = "检查折扣表", tags = {"折扣表" },  notes = "检查折扣表")
	@RequestMapping(method = RequestMethod.POST, value = "/discounttypes/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody DiscountTypeDTO discounttypedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(discounttypeService.checkKey(discounttypeMapping.toDomain(discounttypedto)));
    }

    @PreAuthorize("hasPermission(this.discounttypeMapping.toDomain(#discounttypedto),'iBizBusinessCentral-DiscountType-Save')")
    @ApiOperation(value = "保存折扣表", tags = {"折扣表" },  notes = "保存折扣表")
	@RequestMapping(method = RequestMethod.POST, value = "/discounttypes/save")
    public ResponseEntity<Boolean> save(@RequestBody DiscountTypeDTO discounttypedto) {
        return ResponseEntity.status(HttpStatus.OK).body(discounttypeService.save(discounttypeMapping.toDomain(discounttypedto)));
    }

    @PreAuthorize("hasPermission(this.discounttypeMapping.toDomain(#discounttypedtos),'iBizBusinessCentral-DiscountType-Save')")
    @ApiOperation(value = "批量保存折扣表", tags = {"折扣表" },  notes = "批量保存折扣表")
	@RequestMapping(method = RequestMethod.POST, value = "/discounttypes/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<DiscountTypeDTO> discounttypedtos) {
        discounttypeService.saveBatch(discounttypeMapping.toDomain(discounttypedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-DiscountType-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-DiscountType-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"折扣表" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/discounttypes/fetchdefault")
	public ResponseEntity<List<DiscountTypeDTO>> fetchDefault(DiscountTypeSearchContext context) {
        Page<DiscountType> domains = discounttypeService.searchDefault(context) ;
        List<DiscountTypeDTO> list = discounttypeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-DiscountType-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-DiscountType-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"折扣表" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/discounttypes/searchdefault")
	public ResponseEntity<Page<DiscountTypeDTO>> searchDefault(@RequestBody DiscountTypeSearchContext context) {
        Page<DiscountType> domains = discounttypeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(discounttypeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

