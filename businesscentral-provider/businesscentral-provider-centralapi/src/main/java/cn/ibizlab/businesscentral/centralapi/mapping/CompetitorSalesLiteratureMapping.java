package cn.ibizlab.businesscentral.centralapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.sales.domain.CompetitorSalesLiterature;
import cn.ibizlab.businesscentral.centralapi.dto.CompetitorSalesLiteratureDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CentralApiCompetitorSalesLiteratureMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface CompetitorSalesLiteratureMapping extends MappingBase<CompetitorSalesLiteratureDTO, CompetitorSalesLiterature> {


}

