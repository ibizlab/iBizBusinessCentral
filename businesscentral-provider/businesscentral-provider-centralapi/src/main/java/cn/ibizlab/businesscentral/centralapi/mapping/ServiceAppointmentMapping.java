package cn.ibizlab.businesscentral.centralapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.service.domain.ServiceAppointment;
import cn.ibizlab.businesscentral.centralapi.dto.ServiceAppointmentDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CentralApiServiceAppointmentMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface ServiceAppointmentMapping extends MappingBase<ServiceAppointmentDTO, ServiceAppointment> {


}

