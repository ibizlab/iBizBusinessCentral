package cn.ibizlab.businesscentral.centralapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[AccountDTO]
 */
@Data
public class AccountDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ADDRESS1_PRIMARYCONTACTNAME]
     *
     */
    @JSONField(name = "address1_primarycontactname")
    @JsonProperty("address1_primarycontactname")
    private String address1Primarycontactname;

    /**
     * 属性 [OPENREVENUE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "openrevenue_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("openrevenue_date")
    private Timestamp openrevenueDate;

    /**
     * 属性 [ADDRESS2_UTCOFFSET]
     *
     */
    @JSONField(name = "address2_utcoffset")
    @JsonProperty("address2_utcoffset")
    private Integer address2Utcoffset;

    /**
     * 属性 [FAX]
     *
     */
    @JSONField(name = "fax")
    @JsonProperty("fax")
    private String fax;

    /**
     * 属性 [ADDRESS1_COMPOSITE]
     *
     */
    @JSONField(name = "address1_composite")
    @JsonProperty("address1_composite")
    private String address1Composite;

    /**
     * 属性 [ENTITYIMAGE]
     *
     */
    @JSONField(name = "entityimage")
    @JsonProperty("entityimage")
    private String entityimage;

    /**
     * 属性 [EMAILADDRESS1]
     *
     */
    @JSONField(name = "emailaddress1")
    @JsonProperty("emailaddress1")
    private String emailaddress1;

    /**
     * 属性 [OVERRIDDENCREATEDON]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overriddencreatedon" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overriddencreatedon")
    private Timestamp overriddencreatedon;

    /**
     * 属性 [ADDRESS1_STATEORPROVINCE]
     *
     */
    @JSONField(name = "address1_stateorprovince")
    @JsonProperty("address1_stateorprovince")
    private String address1Stateorprovince;

    /**
     * 属性 [OPENREVENUE]
     *
     */
    @JSONField(name = "openrevenue")
    @JsonProperty("openrevenue")
    private BigDecimal openrevenue;

    /**
     * 属性 [ADDRESS1_UPSZONE]
     *
     */
    @JSONField(name = "address1_upszone")
    @JsonProperty("address1_upszone")
    private String address1Upszone;

    /**
     * 属性 [FOLLOWEMAIL]
     *
     */
    @JSONField(name = "followemail")
    @JsonProperty("followemail")
    private Integer followemail;

    /**
     * 属性 [MARKETINGONLY]
     *
     */
    @JSONField(name = "marketingonly")
    @JsonProperty("marketingonly")
    private Integer marketingonly;

    /**
     * 属性 [NUMBEROFEMPLOYEES]
     *
     */
    @JSONField(name = "numberofemployees")
    @JsonProperty("numberofemployees")
    private Integer numberofemployees;

    /**
     * 属性 [TELEPHONE1]
     *
     */
    @JSONField(name = "telephone1")
    @JsonProperty("telephone1")
    private String telephone1;

    /**
     * 属性 [AGING60]
     *
     */
    @JSONField(name = "aging60")
    @JsonProperty("aging60")
    private BigDecimal aging60;

    /**
     * 属性 [SHARESOUTSTANDING]
     *
     */
    @JSONField(name = "sharesoutstanding")
    @JsonProperty("sharesoutstanding")
    private Integer sharesoutstanding;

    /**
     * 属性 [ADDRESS2_FREIGHTTERMSCODE]
     *
     */
    @JSONField(name = "address2_freighttermscode")
    @JsonProperty("address2_freighttermscode")
    private String address2Freighttermscode;

    /**
     * 属性 [ENTITYIMAGE_TIMESTAMP]
     *
     */
    @JSONField(name = "entityimage_timestamp")
    @JsonProperty("entityimage_timestamp")
    private BigInteger entityimageTimestamp;

    /**
     * 属性 [REVENUE_BASE]
     *
     */
    @JSONField(name = "revenue_base")
    @JsonProperty("revenue_base")
    private BigDecimal revenueBase;

    /**
     * 属性 [ACCOUNTRATINGCODE]
     *
     */
    @JSONField(name = "accountratingcode")
    @JsonProperty("accountratingcode")
    private String accountratingcode;

    /**
     * 属性 [DONOTBULKEMAIL]
     *
     */
    @JSONField(name = "donotbulkemail")
    @JsonProperty("donotbulkemail")
    private Integer donotbulkemail;

    /**
     * 属性 [AGING30]
     *
     */
    @JSONField(name = "aging30")
    @JsonProperty("aging30")
    private BigDecimal aging30;

    /**
     * 属性 [DONOTBULKPOSTALMAIL]
     *
     */
    @JSONField(name = "donotbulkpostalmail")
    @JsonProperty("donotbulkpostalmail")
    private Integer donotbulkpostalmail;

    /**
     * 属性 [CREDITLIMIT_BASE]
     *
     */
    @JSONField(name = "creditlimit_base")
    @JsonProperty("creditlimit_base")
    private BigDecimal creditlimitBase;

    /**
     * 属性 [TIMEZONERULEVERSIONNUMBER]
     *
     */
    @JSONField(name = "timezoneruleversionnumber")
    @JsonProperty("timezoneruleversionnumber")
    private Integer timezoneruleversionnumber;

    /**
     * 属性 [CUSTOMERTYPECODE]
     *
     */
    @JSONField(name = "customertypecode")
    @JsonProperty("customertypecode")
    private String customertypecode;

    /**
     * 属性 [ADDRESS2_LONGITUDE]
     *
     */
    @JSONField(name = "address2_longitude")
    @JsonProperty("address2_longitude")
    private Double address2Longitude;

    /**
     * 属性 [ACCOUNTNAME]
     *
     */
    @JSONField(name = "accountname")
    @JsonProperty("accountname")
    private String accountname;

    /**
     * 属性 [PARTICIPATESINWORKFLOW]
     *
     */
    @JSONField(name = "participatesinworkflow")
    @JsonProperty("participatesinworkflow")
    private Integer participatesinworkflow;

    /**
     * 属性 [PREFERREDCONTACTMETHODCODE]
     *
     */
    @JSONField(name = "preferredcontactmethodcode")
    @JsonProperty("preferredcontactmethodcode")
    private String preferredcontactmethodcode;

    /**
     * 属性 [PREFERREDAPPOINTMENTDAYCODE]
     *
     */
    @JSONField(name = "preferredappointmentdaycode")
    @JsonProperty("preferredappointmentdaycode")
    private String preferredappointmentdaycode;

    /**
     * 属性 [MARKETCAP]
     *
     */
    @JSONField(name = "marketcap")
    @JsonProperty("marketcap")
    private BigDecimal marketcap;

    /**
     * 属性 [ADDRESS2_TELEPHONE1]
     *
     */
    @JSONField(name = "address2_telephone1")
    @JsonProperty("address2_telephone1")
    private String address2Telephone1;

    /**
     * 属性 [BUSINESSTYPECODE]
     *
     */
    @JSONField(name = "businesstypecode")
    @JsonProperty("businesstypecode")
    private String businesstypecode;

    /**
     * 属性 [ENTITYIMAGEID]
     *
     */
    @JSONField(name = "entityimageid")
    @JsonProperty("entityimageid")
    private String entityimageid;

    /**
     * 属性 [ADDRESS2_CITY]
     *
     */
    @JSONField(name = "address2_city")
    @JsonProperty("address2_city")
    private String address2City;

    /**
     * 属性 [OWNERSHIPCODE]
     *
     */
    @JSONField(name = "ownershipcode")
    @JsonProperty("ownershipcode")
    private String ownershipcode;

    /**
     * 属性 [UTCCONVERSIONTIMEZONECODE]
     *
     */
    @JSONField(name = "utcconversiontimezonecode")
    @JsonProperty("utcconversiontimezonecode")
    private Integer utcconversiontimezonecode;

    /**
     * 属性 [ACCOUNTNUMBER]
     *
     */
    @JSONField(name = "accountnumber")
    @JsonProperty("accountnumber")
    private String accountnumber;

    /**
     * 属性 [ADDRESS1_TELEPHONE2]
     *
     */
    @JSONField(name = "address1_telephone2")
    @JsonProperty("address1_telephone2")
    private String address1Telephone2;

    /**
     * 属性 [ADDRESS1_LINE1]
     *
     */
    @JSONField(name = "address1_line1")
    @JsonProperty("address1_line1")
    private String address1Line1;

    /**
     * 属性 [DONOTPHONE]
     *
     */
    @JSONField(name = "donotphone")
    @JsonProperty("donotphone")
    private Integer donotphone;

    /**
     * 属性 [ADDRESS1_TELEPHONE3]
     *
     */
    @JSONField(name = "address1_telephone3")
    @JsonProperty("address1_telephone3")
    private String address1Telephone3;

    /**
     * 属性 [EXCHANGERATE]
     *
     */
    @JSONField(name = "exchangerate")
    @JsonProperty("exchangerate")
    private BigDecimal exchangerate;

    /**
     * 属性 [ADDRESS2_LINE2]
     *
     */
    @JSONField(name = "address2_line2")
    @JsonProperty("address2_line2")
    private String address2Line2;

    /**
     * 属性 [ADDRESS2_TELEPHONE3]
     *
     */
    @JSONField(name = "address2_telephone3")
    @JsonProperty("address2_telephone3")
    private String address2Telephone3;

    /**
     * 属性 [ADDRESS1_TELEPHONE1]
     *
     */
    @JSONField(name = "address1_telephone1")
    @JsonProperty("address1_telephone1")
    private String address1Telephone1;

    /**
     * 属性 [ADDRESS1_FAX]
     *
     */
    @JSONField(name = "address1_fax")
    @JsonProperty("address1_fax")
    private String address1Fax;

    /**
     * 属性 [ADDRESS1_FREIGHTTERMSCODE]
     *
     */
    @JSONField(name = "address1_freighttermscode")
    @JsonProperty("address1_freighttermscode")
    private String address1Freighttermscode;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 属性 [MERGED]
     *
     */
    @JSONField(name = "merged")
    @JsonProperty("merged")
    private Integer merged;

    /**
     * 属性 [ADDRESS1_SHIPPINGMETHODCODE]
     *
     */
    @JSONField(name = "address1_shippingmethodcode")
    @JsonProperty("address1_shippingmethodcode")
    private String address1Shippingmethodcode;

    /**
     * 属性 [ADDRESS1_NAME]
     *
     */
    @JSONField(name = "address1_name")
    @JsonProperty("address1_name")
    private String address1Name;

    /**
     * 属性 [AGING30_BASE]
     *
     */
    @JSONField(name = "aging30_base")
    @JsonProperty("aging30_base")
    private BigDecimal aging30Base;

    /**
     * 属性 [ADDRESS2_TELEPHONE2]
     *
     */
    @JSONField(name = "address2_telephone2")
    @JsonProperty("address2_telephone2")
    private String address2Telephone2;

    /**
     * 属性 [CHILDACCOUNTCOUNT]
     *
     */
    @JSONField(name = "childaccountcount")
    @JsonProperty("childaccountcount")
    private Integer childaccountcount;

    /**
     * 属性 [ACCOUNTID]
     *
     */
    @JSONField(name = "accountid")
    @JsonProperty("accountid")
    private String accountid;

    /**
     * 属性 [PRIMARYTWITTERID]
     *
     */
    @JSONField(name = "primarytwitterid")
    @JsonProperty("primarytwitterid")
    private String primarytwitterid;

    /**
     * 属性 [TICKERSYMBOL]
     *
     */
    @JSONField(name = "tickersymbol")
    @JsonProperty("tickersymbol")
    private String tickersymbol;

    /**
     * 属性 [STOCKEXCHANGE]
     *
     */
    @JSONField(name = "stockexchange")
    @JsonProperty("stockexchange")
    private String stockexchange;

    /**
     * 属性 [ADDRESS2_STATEORPROVINCE]
     *
     */
    @JSONField(name = "address2_stateorprovince")
    @JsonProperty("address2_stateorprovince")
    private String address2Stateorprovince;

    /**
     * 属性 [FTPSITEURL]
     *
     */
    @JSONField(name = "ftpsiteurl")
    @JsonProperty("ftpsiteurl")
    private String ftpsiteurl;

    /**
     * 属性 [STATUSCODE]
     *
     */
    @JSONField(name = "statuscode")
    @JsonProperty("statuscode")
    private Integer statuscode;

    /**
     * 属性 [WEBSITEURL]
     *
     */
    @JSONField(name = "websiteurl")
    @JsonProperty("websiteurl")
    private String websiteurl;

    /**
     * 属性 [DONOTPOSTALMAIL]
     *
     */
    @JSONField(name = "donotpostalmail")
    @JsonProperty("donotpostalmail")
    private Integer donotpostalmail;

    /**
     * 属性 [ADDRESS1_CITY]
     *
     */
    @JSONField(name = "address1_city")
    @JsonProperty("address1_city")
    private String address1City;

    /**
     * 属性 [ADDRESS2_FAX]
     *
     */
    @JSONField(name = "address2_fax")
    @JsonProperty("address2_fax")
    private String address2Fax;

    /**
     * 属性 [AGING90_BASE]
     *
     */
    @JSONField(name = "aging90_base")
    @JsonProperty("aging90_base")
    private BigDecimal aging90Base;

    /**
     * 属性 [OPENDEALS_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "opendeals_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("opendeals_date")
    private Timestamp opendealsDate;

    /**
     * 属性 [TRAVERSEDPATH]
     *
     */
    @JSONField(name = "traversedpath")
    @JsonProperty("traversedpath")
    private String traversedpath;

    /**
     * 属性 [SHIPPINGMETHODCODE]
     *
     */
    @JSONField(name = "shippingmethodcode")
    @JsonProperty("shippingmethodcode")
    private String shippingmethodcode;

    /**
     * 属性 [ADDRESS1_LINE2]
     *
     */
    @JSONField(name = "address1_line2")
    @JsonProperty("address1_line2")
    private String address1Line2;

    /**
     * 属性 [OPENREVENUE_BASE]
     *
     */
    @JSONField(name = "openrevenue_base")
    @JsonProperty("openrevenue_base")
    private BigDecimal openrevenueBase;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [ADDRESS2_LINE3]
     *
     */
    @JSONField(name = "address2_line3")
    @JsonProperty("address2_line3")
    private String address2Line3;

    /**
     * 属性 [ADDRESS1_POSTALCODE]
     *
     */
    @JSONField(name = "address1_postalcode")
    @JsonProperty("address1_postalcode")
    private String address1Postalcode;

    /**
     * 属性 [ADDRESS2_LATITUDE]
     *
     */
    @JSONField(name = "address2_latitude")
    @JsonProperty("address2_latitude")
    private Double address2Latitude;

    /**
     * 属性 [PRIVATE]
     *
     */
    @JSONField(name = "ibizprivate")
    @JsonProperty("ibizprivate")
    private Integer ibizprivate;

    /**
     * 属性 [TERRITORYCODE]
     *
     */
    @JSONField(name = "territorycode")
    @JsonProperty("territorycode")
    private String territorycode;

    /**
     * 属性 [TEAMSFOLLOWED]
     *
     */
    @JSONField(name = "teamsfollowed")
    @JsonProperty("teamsfollowed")
    private Integer teamsfollowed;

    /**
     * 属性 [ADDRESS1_LATITUDE]
     *
     */
    @JSONField(name = "address1_latitude")
    @JsonProperty("address1_latitude")
    private Double address1Latitude;

    /**
     * 属性 [TELEPHONE3]
     *
     */
    @JSONField(name = "telephone3")
    @JsonProperty("telephone3")
    private String telephone3;

    /**
     * 属性 [OWNERID]
     *
     */
    @JSONField(name = "ownerid")
    @JsonProperty("ownerid")
    private String ownerid;

    /**
     * 属性 [ADDRESS2_COUNTRY]
     *
     */
    @JSONField(name = "address2_country")
    @JsonProperty("address2_country")
    private String address2Country;

    /**
     * 属性 [MASTERACCOUNTNAME]
     *
     */
    @JSONField(name = "masteraccountname")
    @JsonProperty("masteraccountname")
    private String masteraccountname;

    /**
     * 属性 [OWNERNAME]
     *
     */
    @JSONField(name = "ownername")
    @JsonProperty("ownername")
    private String ownername;

    /**
     * 属性 [INDUSTRYCODE]
     *
     */
    @JSONField(name = "industrycode")
    @JsonProperty("industrycode")
    private String industrycode;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [LASTONHOLDTIME]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "lastonholdtime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("lastonholdtime")
    private Timestamp lastonholdtime;

    /**
     * 属性 [ADDRESS2_ADDRESSID]
     *
     */
    @JSONField(name = "address2_addressid")
    @JsonProperty("address2_addressid")
    private String address2Addressid;

    /**
     * 属性 [ONHOLDTIME]
     *
     */
    @JSONField(name = "onholdtime")
    @JsonProperty("onholdtime")
    private Integer onholdtime;

    /**
     * 属性 [ACCOUNTCLASSIFICATIONCODE]
     *
     */
    @JSONField(name = "accountclassificationcode")
    @JsonProperty("accountclassificationcode")
    private String accountclassificationcode;

    /**
     * 属性 [ADDRESS1_COUNTRY]
     *
     */
    @JSONField(name = "address1_country")
    @JsonProperty("address1_country")
    private String address1Country;

    /**
     * 属性 [ADDRESS1_ADDRESSTYPECODE]
     *
     */
    @JSONField(name = "address1_addresstypecode")
    @JsonProperty("address1_addresstypecode")
    private String address1Addresstypecode;

    /**
     * 属性 [STATECODE]
     *
     */
    @JSONField(name = "statecode")
    @JsonProperty("statecode")
    private Integer statecode;

    /**
     * 属性 [ADDRESS2_ADDRESSTYPECODE]
     *
     */
    @JSONField(name = "address2_addresstypecode")
    @JsonProperty("address2_addresstypecode")
    private String address2Addresstypecode;

    /**
     * 属性 [ACCOUNTCATEGORYCODE]
     *
     */
    @JSONField(name = "accountcategorycode")
    @JsonProperty("accountcategorycode")
    private String accountcategorycode;

    /**
     * 属性 [EMAILADDRESS2]
     *
     */
    @JSONField(name = "emailaddress2")
    @JsonProperty("emailaddress2")
    private String emailaddress2;

    /**
     * 属性 [ADDRESS2_SHIPPINGMETHODCODE]
     *
     */
    @JSONField(name = "address2_shippingmethodcode")
    @JsonProperty("address2_shippingmethodcode")
    private String address2Shippingmethodcode;

    /**
     * 属性 [IMPORTSEQUENCENUMBER]
     *
     */
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;

    /**
     * 属性 [PRIMARYSATORIID]
     *
     */
    @JSONField(name = "primarysatoriid")
    @JsonProperty("primarysatoriid")
    private String primarysatoriid;

    /**
     * 属性 [CUSTOMERSIZECODE]
     *
     */
    @JSONField(name = "customersizecode")
    @JsonProperty("customersizecode")
    private String customersizecode;

    /**
     * 属性 [OPENDEALS]
     *
     */
    @JSONField(name = "opendeals")
    @JsonProperty("opendeals")
    private Integer opendeals;

    /**
     * 属性 [DONOTSENDMM]
     *
     */
    @JSONField(name = "donotsendmm")
    @JsonProperty("donotsendmm")
    private Integer donotsendmm;

    /**
     * 属性 [PROCESSID]
     *
     */
    @JSONField(name = "processid")
    @JsonProperty("processid")
    private String processid;

    /**
     * 属性 [PAYMENTTERMSCODE]
     *
     */
    @JSONField(name = "paymenttermscode")
    @JsonProperty("paymenttermscode")
    private String paymenttermscode;

    /**
     * 属性 [SIC]
     *
     */
    @JSONField(name = "sic")
    @JsonProperty("sic")
    private String sic;

    /**
     * 属性 [ADDRESS2_PRIMARYCONTACTNAME]
     *
     */
    @JSONField(name = "address2_primarycontactname")
    @JsonProperty("address2_primarycontactname")
    private String address2Primarycontactname;

    /**
     * 属性 [ADDRESS1_UTCOFFSET]
     *
     */
    @JSONField(name = "address1_utcoffset")
    @JsonProperty("address1_utcoffset")
    private Integer address1Utcoffset;

    /**
     * 属性 [LASTUSEDINCAMPAIGN]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "lastusedincampaign" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("lastusedincampaign")
    private Timestamp lastusedincampaign;

    /**
     * 属性 [AGING60_BASE]
     *
     */
    @JSONField(name = "aging60_base")
    @JsonProperty("aging60_base")
    private BigDecimal aging60Base;

    /**
     * 属性 [DONOTFAX]
     *
     */
    @JSONField(name = "donotfax")
    @JsonProperty("donotfax")
    private Integer donotfax;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;

    /**
     * 属性 [OPENREVENUE_STATE]
     *
     */
    @JSONField(name = "openrevenue_state")
    @JsonProperty("openrevenue_state")
    private Integer openrevenueState;

    /**
     * 属性 [OPENDEALS_STATE]
     *
     */
    @JSONField(name = "opendeals_state")
    @JsonProperty("opendeals_state")
    private Integer opendealsState;

    /**
     * 属性 [VERSIONNUMBER]
     *
     */
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;

    /**
     * 属性 [ADDRESS1_LONGITUDE]
     *
     */
    @JSONField(name = "address1_longitude")
    @JsonProperty("address1_longitude")
    private Double address1Longitude;

    /**
     * 属性 [AGING90]
     *
     */
    @JSONField(name = "aging90")
    @JsonProperty("aging90")
    private BigDecimal aging90;

    /**
     * 属性 [ENTITYIMAGE_URL]
     *
     */
    @JSONField(name = "entityimage_url")
    @JsonProperty("entityimage_url")
    private String entityimageUrl;

    /**
     * 属性 [CREDITLIMIT]
     *
     */
    @JSONField(name = "creditlimit")
    @JsonProperty("creditlimit")
    private BigDecimal creditlimit;

    /**
     * 属性 [PREFERREDSYSTEMUSERID]
     *
     */
    @JSONField(name = "preferredsystemuserid")
    @JsonProperty("preferredsystemuserid")
    private String preferredsystemuserid;

    /**
     * 属性 [REVENUE]
     *
     */
    @JSONField(name = "revenue")
    @JsonProperty("revenue")
    private BigDecimal revenue;

    /**
     * 属性 [ADDRESS1_COUNTY]
     *
     */
    @JSONField(name = "address1_county")
    @JsonProperty("address1_county")
    private String address1County;

    /**
     * 属性 [ADDRESS1_LINE3]
     *
     */
    @JSONField(name = "address1_line3")
    @JsonProperty("address1_line3")
    private String address1Line3;

    /**
     * 属性 [PREFERREDSYSTEMUSERNAME]
     *
     */
    @JSONField(name = "preferredsystemusername")
    @JsonProperty("preferredsystemusername")
    private String preferredsystemusername;

    /**
     * 属性 [STAGEID]
     *
     */
    @JSONField(name = "stageid")
    @JsonProperty("stageid")
    private String stageid;

    /**
     * 属性 [DONOTEMAIL]
     *
     */
    @JSONField(name = "donotemail")
    @JsonProperty("donotemail")
    private Integer donotemail;

    /**
     * 属性 [ADDRESS2_POSTALCODE]
     *
     */
    @JSONField(name = "address2_postalcode")
    @JsonProperty("address2_postalcode")
    private String address2Postalcode;

    /**
     * 属性 [PREFERREDAPPOINTMENTTIMECODE]
     *
     */
    @JSONField(name = "preferredappointmenttimecode")
    @JsonProperty("preferredappointmenttimecode")
    private String preferredappointmenttimecode;

    /**
     * 属性 [ADDRESS2_UPSZONE]
     *
     */
    @JSONField(name = "address2_upszone")
    @JsonProperty("address2_upszone")
    private String address2Upszone;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [ADDRESS2_COUNTY]
     *
     */
    @JSONField(name = "address2_county")
    @JsonProperty("address2_county")
    private String address2County;

    /**
     * 属性 [ADDRESS2_COMPOSITE]
     *
     */
    @JSONField(name = "address2_composite")
    @JsonProperty("address2_composite")
    private String address2Composite;

    /**
     * 属性 [ADDRESS2_NAME]
     *
     */
    @JSONField(name = "address2_name")
    @JsonProperty("address2_name")
    private String address2Name;

    /**
     * 属性 [ADDRESS2_LINE1]
     *
     */
    @JSONField(name = "address2_line1")
    @JsonProperty("address2_line1")
    private String address2Line1;

    /**
     * 属性 [MARKETCAP_BASE]
     *
     */
    @JSONField(name = "marketcap_base")
    @JsonProperty("marketcap_base")
    private BigDecimal marketcapBase;

    /**
     * 属性 [EMAILADDRESS3]
     *
     */
    @JSONField(name = "emailaddress3")
    @JsonProperty("emailaddress3")
    private String emailaddress3;

    /**
     * 属性 [OWNERTYPE]
     *
     */
    @JSONField(name = "ownertype")
    @JsonProperty("ownertype")
    private String ownertype;

    /**
     * 属性 [TELEPHONE2]
     *
     */
    @JSONField(name = "telephone2")
    @JsonProperty("telephone2")
    private String telephone2;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;

    /**
     * 属性 [ADDRESS1_POSTOFFICEBOX]
     *
     */
    @JSONField(name = "address1_postofficebox")
    @JsonProperty("address1_postofficebox")
    private String address1Postofficebox;

    /**
     * 属性 [CREDITONHOLD]
     *
     */
    @JSONField(name = "creditonhold")
    @JsonProperty("creditonhold")
    private Integer creditonhold;

    /**
     * 属性 [ADDRESS1_ADDRESSID]
     *
     */
    @JSONField(name = "address1_addressid")
    @JsonProperty("address1_addressid")
    private String address1Addressid;

    /**
     * 属性 [ADDRESS2_POSTOFFICEBOX]
     *
     */
    @JSONField(name = "address2_postofficebox")
    @JsonProperty("address2_postofficebox")
    private String address2Postofficebox;

    /**
     * 属性 [CURRENCYNAME]
     *
     */
    @JSONField(name = "currencyname")
    @JsonProperty("currencyname")
    private String currencyname;

    /**
     * 属性 [DEFAULTPRICELEVELNAME]
     *
     */
    @JSONField(name = "defaultpricelevelname")
    @JsonProperty("defaultpricelevelname")
    private String defaultpricelevelname;

    /**
     * 属性 [SLANAME]
     *
     */
    @JSONField(name = "slaname")
    @JsonProperty("slaname")
    private String slaname;

    /**
     * 属性 [PREFERREDEQUIPMENTNAME]
     *
     */
    @JSONField(name = "preferredequipmentname")
    @JsonProperty("preferredequipmentname")
    private String preferredequipmentname;

    /**
     * 属性 [PREFERREDSERVICENAME]
     *
     */
    @JSONField(name = "preferredservicename")
    @JsonProperty("preferredservicename")
    private String preferredservicename;

    /**
     * 属性 [TERRITORYNAME]
     *
     */
    @JSONField(name = "territoryname")
    @JsonProperty("territoryname")
    private String territoryname;

    /**
     * 属性 [PRIMARYCONTACTNAME]
     *
     */
    @JSONField(name = "primarycontactname")
    @JsonProperty("primarycontactname")
    private String primarycontactname;

    /**
     * 属性 [PARENTACCOUNTNAME]
     *
     */
    @JSONField(name = "parentaccountname")
    @JsonProperty("parentaccountname")
    private String parentaccountname;

    /**
     * 属性 [ORIGINATINGLEADNAME]
     *
     */
    @JSONField(name = "originatingleadname")
    @JsonProperty("originatingleadname")
    private String originatingleadname;

    /**
     * 属性 [ORIGINATINGLEADID]
     *
     */
    @JSONField(name = "originatingleadid")
    @JsonProperty("originatingleadid")
    private String originatingleadid;

    /**
     * 属性 [TRANSACTIONCURRENCYID]
     *
     */
    @JSONField(name = "transactioncurrencyid")
    @JsonProperty("transactioncurrencyid")
    private String transactioncurrencyid;

    /**
     * 属性 [DEFAULTPRICELEVELID]
     *
     */
    @JSONField(name = "defaultpricelevelid")
    @JsonProperty("defaultpricelevelid")
    private String defaultpricelevelid;

    /**
     * 属性 [PARENTACCOUNTID]
     *
     */
    @JSONField(name = "parentaccountid")
    @JsonProperty("parentaccountid")
    private String parentaccountid;

    /**
     * 属性 [PREFERREDEQUIPMENTID]
     *
     */
    @JSONField(name = "preferredequipmentid")
    @JsonProperty("preferredequipmentid")
    private String preferredequipmentid;

    /**
     * 属性 [TERRITORYID]
     *
     */
    @JSONField(name = "territoryid")
    @JsonProperty("territoryid")
    private String territoryid;

    /**
     * 属性 [PRIMARYCONTACTID]
     *
     */
    @JSONField(name = "primarycontactid")
    @JsonProperty("primarycontactid")
    private String primarycontactid;

    /**
     * 属性 [SLAID]
     *
     */
    @JSONField(name = "slaid")
    @JsonProperty("slaid")
    private String slaid;

    /**
     * 属性 [PREFERREDSERVICEID]
     *
     */
    @JSONField(name = "preferredserviceid")
    @JsonProperty("preferredserviceid")
    private String preferredserviceid;


    /**
     * 设置 [ADDRESS1_PRIMARYCONTACTNAME]
     */
    public void setAddress1Primarycontactname(String  address1Primarycontactname){
        this.address1Primarycontactname = address1Primarycontactname ;
        this.modify("address1_primarycontactname",address1Primarycontactname);
    }

    /**
     * 设置 [OPENREVENUE_DATE]
     */
    public void setOpenrevenueDate(Timestamp  openrevenueDate){
        this.openrevenueDate = openrevenueDate ;
        this.modify("openrevenue_date",openrevenueDate);
    }

    /**
     * 设置 [ADDRESS2_UTCOFFSET]
     */
    public void setAddress2Utcoffset(Integer  address2Utcoffset){
        this.address2Utcoffset = address2Utcoffset ;
        this.modify("address2_utcoffset",address2Utcoffset);
    }

    /**
     * 设置 [FAX]
     */
    public void setFax(String  fax){
        this.fax = fax ;
        this.modify("fax",fax);
    }

    /**
     * 设置 [ADDRESS1_COMPOSITE]
     */
    public void setAddress1Composite(String  address1Composite){
        this.address1Composite = address1Composite ;
        this.modify("address1_composite",address1Composite);
    }

    /**
     * 设置 [ENTITYIMAGE]
     */
    public void setEntityimage(String  entityimage){
        this.entityimage = entityimage ;
        this.modify("entityimage",entityimage);
    }

    /**
     * 设置 [EMAILADDRESS1]
     */
    public void setEmailaddress1(String  emailaddress1){
        this.emailaddress1 = emailaddress1 ;
        this.modify("emailaddress1",emailaddress1);
    }

    /**
     * 设置 [OVERRIDDENCREATEDON]
     */
    public void setOverriddencreatedon(Timestamp  overriddencreatedon){
        this.overriddencreatedon = overriddencreatedon ;
        this.modify("overriddencreatedon",overriddencreatedon);
    }

    /**
     * 设置 [ADDRESS1_STATEORPROVINCE]
     */
    public void setAddress1Stateorprovince(String  address1Stateorprovince){
        this.address1Stateorprovince = address1Stateorprovince ;
        this.modify("address1_stateorprovince",address1Stateorprovince);
    }

    /**
     * 设置 [OPENREVENUE]
     */
    public void setOpenrevenue(BigDecimal  openrevenue){
        this.openrevenue = openrevenue ;
        this.modify("openrevenue",openrevenue);
    }

    /**
     * 设置 [ADDRESS1_UPSZONE]
     */
    public void setAddress1Upszone(String  address1Upszone){
        this.address1Upszone = address1Upszone ;
        this.modify("address1_upszone",address1Upszone);
    }

    /**
     * 设置 [FOLLOWEMAIL]
     */
    public void setFollowemail(Integer  followemail){
        this.followemail = followemail ;
        this.modify("followemail",followemail);
    }

    /**
     * 设置 [MARKETINGONLY]
     */
    public void setMarketingonly(Integer  marketingonly){
        this.marketingonly = marketingonly ;
        this.modify("marketingonly",marketingonly);
    }

    /**
     * 设置 [NUMBEROFEMPLOYEES]
     */
    public void setNumberofemployees(Integer  numberofemployees){
        this.numberofemployees = numberofemployees ;
        this.modify("numberofemployees",numberofemployees);
    }

    /**
     * 设置 [TELEPHONE1]
     */
    public void setTelephone1(String  telephone1){
        this.telephone1 = telephone1 ;
        this.modify("telephone1",telephone1);
    }

    /**
     * 设置 [AGING60]
     */
    public void setAging60(BigDecimal  aging60){
        this.aging60 = aging60 ;
        this.modify("aging60",aging60);
    }

    /**
     * 设置 [SHARESOUTSTANDING]
     */
    public void setSharesoutstanding(Integer  sharesoutstanding){
        this.sharesoutstanding = sharesoutstanding ;
        this.modify("sharesoutstanding",sharesoutstanding);
    }

    /**
     * 设置 [ADDRESS2_FREIGHTTERMSCODE]
     */
    public void setAddress2Freighttermscode(String  address2Freighttermscode){
        this.address2Freighttermscode = address2Freighttermscode ;
        this.modify("address2_freighttermscode",address2Freighttermscode);
    }

    /**
     * 设置 [ENTITYIMAGE_TIMESTAMP]
     */
    public void setEntityimageTimestamp(BigInteger  entityimageTimestamp){
        this.entityimageTimestamp = entityimageTimestamp ;
        this.modify("entityimage_timestamp",entityimageTimestamp);
    }

    /**
     * 设置 [REVENUE_BASE]
     */
    public void setRevenueBase(BigDecimal  revenueBase){
        this.revenueBase = revenueBase ;
        this.modify("revenue_base",revenueBase);
    }

    /**
     * 设置 [ACCOUNTRATINGCODE]
     */
    public void setAccountratingcode(String  accountratingcode){
        this.accountratingcode = accountratingcode ;
        this.modify("accountratingcode",accountratingcode);
    }

    /**
     * 设置 [DONOTBULKEMAIL]
     */
    public void setDonotbulkemail(Integer  donotbulkemail){
        this.donotbulkemail = donotbulkemail ;
        this.modify("donotbulkemail",donotbulkemail);
    }

    /**
     * 设置 [AGING30]
     */
    public void setAging30(BigDecimal  aging30){
        this.aging30 = aging30 ;
        this.modify("aging30",aging30);
    }

    /**
     * 设置 [DONOTBULKPOSTALMAIL]
     */
    public void setDonotbulkpostalmail(Integer  donotbulkpostalmail){
        this.donotbulkpostalmail = donotbulkpostalmail ;
        this.modify("donotbulkpostalmail",donotbulkpostalmail);
    }

    /**
     * 设置 [CREDITLIMIT_BASE]
     */
    public void setCreditlimitBase(BigDecimal  creditlimitBase){
        this.creditlimitBase = creditlimitBase ;
        this.modify("creditlimit_base",creditlimitBase);
    }

    /**
     * 设置 [TIMEZONERULEVERSIONNUMBER]
     */
    public void setTimezoneruleversionnumber(Integer  timezoneruleversionnumber){
        this.timezoneruleversionnumber = timezoneruleversionnumber ;
        this.modify("timezoneruleversionnumber",timezoneruleversionnumber);
    }

    /**
     * 设置 [CUSTOMERTYPECODE]
     */
    public void setCustomertypecode(String  customertypecode){
        this.customertypecode = customertypecode ;
        this.modify("customertypecode",customertypecode);
    }

    /**
     * 设置 [ADDRESS2_LONGITUDE]
     */
    public void setAddress2Longitude(Double  address2Longitude){
        this.address2Longitude = address2Longitude ;
        this.modify("address2_longitude",address2Longitude);
    }

    /**
     * 设置 [ACCOUNTNAME]
     */
    public void setAccountname(String  accountname){
        this.accountname = accountname ;
        this.modify("accountname",accountname);
    }

    /**
     * 设置 [PARTICIPATESINWORKFLOW]
     */
    public void setParticipatesinworkflow(Integer  participatesinworkflow){
        this.participatesinworkflow = participatesinworkflow ;
        this.modify("participatesinworkflow",participatesinworkflow);
    }

    /**
     * 设置 [PREFERREDCONTACTMETHODCODE]
     */
    public void setPreferredcontactmethodcode(String  preferredcontactmethodcode){
        this.preferredcontactmethodcode = preferredcontactmethodcode ;
        this.modify("preferredcontactmethodcode",preferredcontactmethodcode);
    }

    /**
     * 设置 [PREFERREDAPPOINTMENTDAYCODE]
     */
    public void setPreferredappointmentdaycode(String  preferredappointmentdaycode){
        this.preferredappointmentdaycode = preferredappointmentdaycode ;
        this.modify("preferredappointmentdaycode",preferredappointmentdaycode);
    }

    /**
     * 设置 [MARKETCAP]
     */
    public void setMarketcap(BigDecimal  marketcap){
        this.marketcap = marketcap ;
        this.modify("marketcap",marketcap);
    }

    /**
     * 设置 [ADDRESS2_TELEPHONE1]
     */
    public void setAddress2Telephone1(String  address2Telephone1){
        this.address2Telephone1 = address2Telephone1 ;
        this.modify("address2_telephone1",address2Telephone1);
    }

    /**
     * 设置 [BUSINESSTYPECODE]
     */
    public void setBusinesstypecode(String  businesstypecode){
        this.businesstypecode = businesstypecode ;
        this.modify("businesstypecode",businesstypecode);
    }

    /**
     * 设置 [ENTITYIMAGEID]
     */
    public void setEntityimageid(String  entityimageid){
        this.entityimageid = entityimageid ;
        this.modify("entityimageid",entityimageid);
    }

    /**
     * 设置 [ADDRESS2_CITY]
     */
    public void setAddress2City(String  address2City){
        this.address2City = address2City ;
        this.modify("address2_city",address2City);
    }

    /**
     * 设置 [OWNERSHIPCODE]
     */
    public void setOwnershipcode(String  ownershipcode){
        this.ownershipcode = ownershipcode ;
        this.modify("ownershipcode",ownershipcode);
    }

    /**
     * 设置 [UTCCONVERSIONTIMEZONECODE]
     */
    public void setUtcconversiontimezonecode(Integer  utcconversiontimezonecode){
        this.utcconversiontimezonecode = utcconversiontimezonecode ;
        this.modify("utcconversiontimezonecode",utcconversiontimezonecode);
    }

    /**
     * 设置 [ACCOUNTNUMBER]
     */
    public void setAccountnumber(String  accountnumber){
        this.accountnumber = accountnumber ;
        this.modify("accountnumber",accountnumber);
    }

    /**
     * 设置 [ADDRESS1_TELEPHONE2]
     */
    public void setAddress1Telephone2(String  address1Telephone2){
        this.address1Telephone2 = address1Telephone2 ;
        this.modify("address1_telephone2",address1Telephone2);
    }

    /**
     * 设置 [ADDRESS1_LINE1]
     */
    public void setAddress1Line1(String  address1Line1){
        this.address1Line1 = address1Line1 ;
        this.modify("address1_line1",address1Line1);
    }

    /**
     * 设置 [DONOTPHONE]
     */
    public void setDonotphone(Integer  donotphone){
        this.donotphone = donotphone ;
        this.modify("donotphone",donotphone);
    }

    /**
     * 设置 [ADDRESS1_TELEPHONE3]
     */
    public void setAddress1Telephone3(String  address1Telephone3){
        this.address1Telephone3 = address1Telephone3 ;
        this.modify("address1_telephone3",address1Telephone3);
    }

    /**
     * 设置 [EXCHANGERATE]
     */
    public void setExchangerate(BigDecimal  exchangerate){
        this.exchangerate = exchangerate ;
        this.modify("exchangerate",exchangerate);
    }

    /**
     * 设置 [ADDRESS2_LINE2]
     */
    public void setAddress2Line2(String  address2Line2){
        this.address2Line2 = address2Line2 ;
        this.modify("address2_line2",address2Line2);
    }

    /**
     * 设置 [ADDRESS2_TELEPHONE3]
     */
    public void setAddress2Telephone3(String  address2Telephone3){
        this.address2Telephone3 = address2Telephone3 ;
        this.modify("address2_telephone3",address2Telephone3);
    }

    /**
     * 设置 [ADDRESS1_TELEPHONE1]
     */
    public void setAddress1Telephone1(String  address1Telephone1){
        this.address1Telephone1 = address1Telephone1 ;
        this.modify("address1_telephone1",address1Telephone1);
    }

    /**
     * 设置 [ADDRESS1_FAX]
     */
    public void setAddress1Fax(String  address1Fax){
        this.address1Fax = address1Fax ;
        this.modify("address1_fax",address1Fax);
    }

    /**
     * 设置 [ADDRESS1_FREIGHTTERMSCODE]
     */
    public void setAddress1Freighttermscode(String  address1Freighttermscode){
        this.address1Freighttermscode = address1Freighttermscode ;
        this.modify("address1_freighttermscode",address1Freighttermscode);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [MERGED]
     */
    public void setMerged(Integer  merged){
        this.merged = merged ;
        this.modify("merged",merged);
    }

    /**
     * 设置 [ADDRESS1_SHIPPINGMETHODCODE]
     */
    public void setAddress1Shippingmethodcode(String  address1Shippingmethodcode){
        this.address1Shippingmethodcode = address1Shippingmethodcode ;
        this.modify("address1_shippingmethodcode",address1Shippingmethodcode);
    }

    /**
     * 设置 [ADDRESS1_NAME]
     */
    public void setAddress1Name(String  address1Name){
        this.address1Name = address1Name ;
        this.modify("address1_name",address1Name);
    }

    /**
     * 设置 [AGING30_BASE]
     */
    public void setAging30Base(BigDecimal  aging30Base){
        this.aging30Base = aging30Base ;
        this.modify("aging30_base",aging30Base);
    }

    /**
     * 设置 [ADDRESS2_TELEPHONE2]
     */
    public void setAddress2Telephone2(String  address2Telephone2){
        this.address2Telephone2 = address2Telephone2 ;
        this.modify("address2_telephone2",address2Telephone2);
    }

    /**
     * 设置 [CHILDACCOUNTCOUNT]
     */
    public void setChildaccountcount(Integer  childaccountcount){
        this.childaccountcount = childaccountcount ;
        this.modify("childaccountcount",childaccountcount);
    }

    /**
     * 设置 [PRIMARYTWITTERID]
     */
    public void setPrimarytwitterid(String  primarytwitterid){
        this.primarytwitterid = primarytwitterid ;
        this.modify("primarytwitterid",primarytwitterid);
    }

    /**
     * 设置 [TICKERSYMBOL]
     */
    public void setTickersymbol(String  tickersymbol){
        this.tickersymbol = tickersymbol ;
        this.modify("tickersymbol",tickersymbol);
    }

    /**
     * 设置 [STOCKEXCHANGE]
     */
    public void setStockexchange(String  stockexchange){
        this.stockexchange = stockexchange ;
        this.modify("stockexchange",stockexchange);
    }

    /**
     * 设置 [ADDRESS2_STATEORPROVINCE]
     */
    public void setAddress2Stateorprovince(String  address2Stateorprovince){
        this.address2Stateorprovince = address2Stateorprovince ;
        this.modify("address2_stateorprovince",address2Stateorprovince);
    }

    /**
     * 设置 [FTPSITEURL]
     */
    public void setFtpsiteurl(String  ftpsiteurl){
        this.ftpsiteurl = ftpsiteurl ;
        this.modify("ftpsiteurl",ftpsiteurl);
    }

    /**
     * 设置 [STATUSCODE]
     */
    public void setStatuscode(Integer  statuscode){
        this.statuscode = statuscode ;
        this.modify("statuscode",statuscode);
    }

    /**
     * 设置 [WEBSITEURL]
     */
    public void setWebsiteurl(String  websiteurl){
        this.websiteurl = websiteurl ;
        this.modify("websiteurl",websiteurl);
    }

    /**
     * 设置 [DONOTPOSTALMAIL]
     */
    public void setDonotpostalmail(Integer  donotpostalmail){
        this.donotpostalmail = donotpostalmail ;
        this.modify("donotpostalmail",donotpostalmail);
    }

    /**
     * 设置 [ADDRESS1_CITY]
     */
    public void setAddress1City(String  address1City){
        this.address1City = address1City ;
        this.modify("address1_city",address1City);
    }

    /**
     * 设置 [ADDRESS2_FAX]
     */
    public void setAddress2Fax(String  address2Fax){
        this.address2Fax = address2Fax ;
        this.modify("address2_fax",address2Fax);
    }

    /**
     * 设置 [AGING90_BASE]
     */
    public void setAging90Base(BigDecimal  aging90Base){
        this.aging90Base = aging90Base ;
        this.modify("aging90_base",aging90Base);
    }

    /**
     * 设置 [OPENDEALS_DATE]
     */
    public void setOpendealsDate(Timestamp  opendealsDate){
        this.opendealsDate = opendealsDate ;
        this.modify("opendeals_date",opendealsDate);
    }

    /**
     * 设置 [TRAVERSEDPATH]
     */
    public void setTraversedpath(String  traversedpath){
        this.traversedpath = traversedpath ;
        this.modify("traversedpath",traversedpath);
    }

    /**
     * 设置 [SHIPPINGMETHODCODE]
     */
    public void setShippingmethodcode(String  shippingmethodcode){
        this.shippingmethodcode = shippingmethodcode ;
        this.modify("shippingmethodcode",shippingmethodcode);
    }

    /**
     * 设置 [ADDRESS1_LINE2]
     */
    public void setAddress1Line2(String  address1Line2){
        this.address1Line2 = address1Line2 ;
        this.modify("address1_line2",address1Line2);
    }

    /**
     * 设置 [OPENREVENUE_BASE]
     */
    public void setOpenrevenueBase(BigDecimal  openrevenueBase){
        this.openrevenueBase = openrevenueBase ;
        this.modify("openrevenue_base",openrevenueBase);
    }

    /**
     * 设置 [ADDRESS2_LINE3]
     */
    public void setAddress2Line3(String  address2Line3){
        this.address2Line3 = address2Line3 ;
        this.modify("address2_line3",address2Line3);
    }

    /**
     * 设置 [ADDRESS1_POSTALCODE]
     */
    public void setAddress1Postalcode(String  address1Postalcode){
        this.address1Postalcode = address1Postalcode ;
        this.modify("address1_postalcode",address1Postalcode);
    }

    /**
     * 设置 [ADDRESS2_LATITUDE]
     */
    public void setAddress2Latitude(Double  address2Latitude){
        this.address2Latitude = address2Latitude ;
        this.modify("address2_latitude",address2Latitude);
    }

    /**
     * 设置 [PRIVATE]
     */
    public void setIbizprivate(Integer  ibizprivate){
        this.ibizprivate = ibizprivate ;
        this.modify("private",ibizprivate);
    }

    /**
     * 设置 [TERRITORYCODE]
     */
    public void setTerritorycode(String  territorycode){
        this.territorycode = territorycode ;
        this.modify("territorycode",territorycode);
    }

    /**
     * 设置 [TEAMSFOLLOWED]
     */
    public void setTeamsfollowed(Integer  teamsfollowed){
        this.teamsfollowed = teamsfollowed ;
        this.modify("teamsfollowed",teamsfollowed);
    }

    /**
     * 设置 [ADDRESS1_LATITUDE]
     */
    public void setAddress1Latitude(Double  address1Latitude){
        this.address1Latitude = address1Latitude ;
        this.modify("address1_latitude",address1Latitude);
    }

    /**
     * 设置 [TELEPHONE3]
     */
    public void setTelephone3(String  telephone3){
        this.telephone3 = telephone3 ;
        this.modify("telephone3",telephone3);
    }

    /**
     * 设置 [OWNERID]
     */
    public void setOwnerid(String  ownerid){
        this.ownerid = ownerid ;
        this.modify("ownerid",ownerid);
    }

    /**
     * 设置 [ADDRESS2_COUNTRY]
     */
    public void setAddress2Country(String  address2Country){
        this.address2Country = address2Country ;
        this.modify("address2_country",address2Country);
    }

    /**
     * 设置 [MASTERACCOUNTNAME]
     */
    public void setMasteraccountname(String  masteraccountname){
        this.masteraccountname = masteraccountname ;
        this.modify("masteraccountname",masteraccountname);
    }

    /**
     * 设置 [OWNERNAME]
     */
    public void setOwnername(String  ownername){
        this.ownername = ownername ;
        this.modify("ownername",ownername);
    }

    /**
     * 设置 [INDUSTRYCODE]
     */
    public void setIndustrycode(String  industrycode){
        this.industrycode = industrycode ;
        this.modify("industrycode",industrycode);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [LASTONHOLDTIME]
     */
    public void setLastonholdtime(Timestamp  lastonholdtime){
        this.lastonholdtime = lastonholdtime ;
        this.modify("lastonholdtime",lastonholdtime);
    }

    /**
     * 设置 [ADDRESS2_ADDRESSID]
     */
    public void setAddress2Addressid(String  address2Addressid){
        this.address2Addressid = address2Addressid ;
        this.modify("address2_addressid",address2Addressid);
    }

    /**
     * 设置 [ONHOLDTIME]
     */
    public void setOnholdtime(Integer  onholdtime){
        this.onholdtime = onholdtime ;
        this.modify("onholdtime",onholdtime);
    }

    /**
     * 设置 [ACCOUNTCLASSIFICATIONCODE]
     */
    public void setAccountclassificationcode(String  accountclassificationcode){
        this.accountclassificationcode = accountclassificationcode ;
        this.modify("accountclassificationcode",accountclassificationcode);
    }

    /**
     * 设置 [ADDRESS1_COUNTRY]
     */
    public void setAddress1Country(String  address1Country){
        this.address1Country = address1Country ;
        this.modify("address1_country",address1Country);
    }

    /**
     * 设置 [ADDRESS1_ADDRESSTYPECODE]
     */
    public void setAddress1Addresstypecode(String  address1Addresstypecode){
        this.address1Addresstypecode = address1Addresstypecode ;
        this.modify("address1_addresstypecode",address1Addresstypecode);
    }

    /**
     * 设置 [STATECODE]
     */
    public void setStatecode(Integer  statecode){
        this.statecode = statecode ;
        this.modify("statecode",statecode);
    }

    /**
     * 设置 [ADDRESS2_ADDRESSTYPECODE]
     */
    public void setAddress2Addresstypecode(String  address2Addresstypecode){
        this.address2Addresstypecode = address2Addresstypecode ;
        this.modify("address2_addresstypecode",address2Addresstypecode);
    }

    /**
     * 设置 [ACCOUNTCATEGORYCODE]
     */
    public void setAccountcategorycode(String  accountcategorycode){
        this.accountcategorycode = accountcategorycode ;
        this.modify("accountcategorycode",accountcategorycode);
    }

    /**
     * 设置 [EMAILADDRESS2]
     */
    public void setEmailaddress2(String  emailaddress2){
        this.emailaddress2 = emailaddress2 ;
        this.modify("emailaddress2",emailaddress2);
    }

    /**
     * 设置 [ADDRESS2_SHIPPINGMETHODCODE]
     */
    public void setAddress2Shippingmethodcode(String  address2Shippingmethodcode){
        this.address2Shippingmethodcode = address2Shippingmethodcode ;
        this.modify("address2_shippingmethodcode",address2Shippingmethodcode);
    }

    /**
     * 设置 [IMPORTSEQUENCENUMBER]
     */
    public void setImportsequencenumber(Integer  importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [PRIMARYSATORIID]
     */
    public void setPrimarysatoriid(String  primarysatoriid){
        this.primarysatoriid = primarysatoriid ;
        this.modify("primarysatoriid",primarysatoriid);
    }

    /**
     * 设置 [CUSTOMERSIZECODE]
     */
    public void setCustomersizecode(String  customersizecode){
        this.customersizecode = customersizecode ;
        this.modify("customersizecode",customersizecode);
    }

    /**
     * 设置 [OPENDEALS]
     */
    public void setOpendeals(Integer  opendeals){
        this.opendeals = opendeals ;
        this.modify("opendeals",opendeals);
    }

    /**
     * 设置 [DONOTSENDMM]
     */
    public void setDonotsendmm(Integer  donotsendmm){
        this.donotsendmm = donotsendmm ;
        this.modify("donotsendmm",donotsendmm);
    }

    /**
     * 设置 [PROCESSID]
     */
    public void setProcessid(String  processid){
        this.processid = processid ;
        this.modify("processid",processid);
    }

    /**
     * 设置 [PAYMENTTERMSCODE]
     */
    public void setPaymenttermscode(String  paymenttermscode){
        this.paymenttermscode = paymenttermscode ;
        this.modify("paymenttermscode",paymenttermscode);
    }

    /**
     * 设置 [SIC]
     */
    public void setSic(String  sic){
        this.sic = sic ;
        this.modify("sic",sic);
    }

    /**
     * 设置 [ADDRESS2_PRIMARYCONTACTNAME]
     */
    public void setAddress2Primarycontactname(String  address2Primarycontactname){
        this.address2Primarycontactname = address2Primarycontactname ;
        this.modify("address2_primarycontactname",address2Primarycontactname);
    }

    /**
     * 设置 [ADDRESS1_UTCOFFSET]
     */
    public void setAddress1Utcoffset(Integer  address1Utcoffset){
        this.address1Utcoffset = address1Utcoffset ;
        this.modify("address1_utcoffset",address1Utcoffset);
    }

    /**
     * 设置 [LASTUSEDINCAMPAIGN]
     */
    public void setLastusedincampaign(Timestamp  lastusedincampaign){
        this.lastusedincampaign = lastusedincampaign ;
        this.modify("lastusedincampaign",lastusedincampaign);
    }

    /**
     * 设置 [AGING60_BASE]
     */
    public void setAging60Base(BigDecimal  aging60Base){
        this.aging60Base = aging60Base ;
        this.modify("aging60_base",aging60Base);
    }

    /**
     * 设置 [DONOTFAX]
     */
    public void setDonotfax(Integer  donotfax){
        this.donotfax = donotfax ;
        this.modify("donotfax",donotfax);
    }

    /**
     * 设置 [OPENREVENUE_STATE]
     */
    public void setOpenrevenueState(Integer  openrevenueState){
        this.openrevenueState = openrevenueState ;
        this.modify("openrevenue_state",openrevenueState);
    }

    /**
     * 设置 [OPENDEALS_STATE]
     */
    public void setOpendealsState(Integer  opendealsState){
        this.opendealsState = opendealsState ;
        this.modify("opendeals_state",opendealsState);
    }

    /**
     * 设置 [VERSIONNUMBER]
     */
    public void setVersionnumber(BigInteger  versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [ADDRESS1_LONGITUDE]
     */
    public void setAddress1Longitude(Double  address1Longitude){
        this.address1Longitude = address1Longitude ;
        this.modify("address1_longitude",address1Longitude);
    }

    /**
     * 设置 [AGING90]
     */
    public void setAging90(BigDecimal  aging90){
        this.aging90 = aging90 ;
        this.modify("aging90",aging90);
    }

    /**
     * 设置 [ENTITYIMAGE_URL]
     */
    public void setEntityimageUrl(String  entityimageUrl){
        this.entityimageUrl = entityimageUrl ;
        this.modify("entityimage_url",entityimageUrl);
    }

    /**
     * 设置 [CREDITLIMIT]
     */
    public void setCreditlimit(BigDecimal  creditlimit){
        this.creditlimit = creditlimit ;
        this.modify("creditlimit",creditlimit);
    }

    /**
     * 设置 [PREFERREDSYSTEMUSERID]
     */
    public void setPreferredsystemuserid(String  preferredsystemuserid){
        this.preferredsystemuserid = preferredsystemuserid ;
        this.modify("preferredsystemuserid",preferredsystemuserid);
    }

    /**
     * 设置 [REVENUE]
     */
    public void setRevenue(BigDecimal  revenue){
        this.revenue = revenue ;
        this.modify("revenue",revenue);
    }

    /**
     * 设置 [ADDRESS1_COUNTY]
     */
    public void setAddress1County(String  address1County){
        this.address1County = address1County ;
        this.modify("address1_county",address1County);
    }

    /**
     * 设置 [ADDRESS1_LINE3]
     */
    public void setAddress1Line3(String  address1Line3){
        this.address1Line3 = address1Line3 ;
        this.modify("address1_line3",address1Line3);
    }

    /**
     * 设置 [PREFERREDSYSTEMUSERNAME]
     */
    public void setPreferredsystemusername(String  preferredsystemusername){
        this.preferredsystemusername = preferredsystemusername ;
        this.modify("preferredsystemusername",preferredsystemusername);
    }

    /**
     * 设置 [STAGEID]
     */
    public void setStageid(String  stageid){
        this.stageid = stageid ;
        this.modify("stageid",stageid);
    }

    /**
     * 设置 [DONOTEMAIL]
     */
    public void setDonotemail(Integer  donotemail){
        this.donotemail = donotemail ;
        this.modify("donotemail",donotemail);
    }

    /**
     * 设置 [ADDRESS2_POSTALCODE]
     */
    public void setAddress2Postalcode(String  address2Postalcode){
        this.address2Postalcode = address2Postalcode ;
        this.modify("address2_postalcode",address2Postalcode);
    }

    /**
     * 设置 [PREFERREDAPPOINTMENTTIMECODE]
     */
    public void setPreferredappointmenttimecode(String  preferredappointmenttimecode){
        this.preferredappointmenttimecode = preferredappointmenttimecode ;
        this.modify("preferredappointmenttimecode",preferredappointmenttimecode);
    }

    /**
     * 设置 [ADDRESS2_UPSZONE]
     */
    public void setAddress2Upszone(String  address2Upszone){
        this.address2Upszone = address2Upszone ;
        this.modify("address2_upszone",address2Upszone);
    }

    /**
     * 设置 [ADDRESS2_COUNTY]
     */
    public void setAddress2County(String  address2County){
        this.address2County = address2County ;
        this.modify("address2_county",address2County);
    }

    /**
     * 设置 [ADDRESS2_COMPOSITE]
     */
    public void setAddress2Composite(String  address2Composite){
        this.address2Composite = address2Composite ;
        this.modify("address2_composite",address2Composite);
    }

    /**
     * 设置 [ADDRESS2_NAME]
     */
    public void setAddress2Name(String  address2Name){
        this.address2Name = address2Name ;
        this.modify("address2_name",address2Name);
    }

    /**
     * 设置 [ADDRESS2_LINE1]
     */
    public void setAddress2Line1(String  address2Line1){
        this.address2Line1 = address2Line1 ;
        this.modify("address2_line1",address2Line1);
    }

    /**
     * 设置 [MARKETCAP_BASE]
     */
    public void setMarketcapBase(BigDecimal  marketcapBase){
        this.marketcapBase = marketcapBase ;
        this.modify("marketcap_base",marketcapBase);
    }

    /**
     * 设置 [EMAILADDRESS3]
     */
    public void setEmailaddress3(String  emailaddress3){
        this.emailaddress3 = emailaddress3 ;
        this.modify("emailaddress3",emailaddress3);
    }

    /**
     * 设置 [OWNERTYPE]
     */
    public void setOwnertype(String  ownertype){
        this.ownertype = ownertype ;
        this.modify("ownertype",ownertype);
    }

    /**
     * 设置 [TELEPHONE2]
     */
    public void setTelephone2(String  telephone2){
        this.telephone2 = telephone2 ;
        this.modify("telephone2",telephone2);
    }

    /**
     * 设置 [ADDRESS1_POSTOFFICEBOX]
     */
    public void setAddress1Postofficebox(String  address1Postofficebox){
        this.address1Postofficebox = address1Postofficebox ;
        this.modify("address1_postofficebox",address1Postofficebox);
    }

    /**
     * 设置 [CREDITONHOLD]
     */
    public void setCreditonhold(Integer  creditonhold){
        this.creditonhold = creditonhold ;
        this.modify("creditonhold",creditonhold);
    }

    /**
     * 设置 [ADDRESS1_ADDRESSID]
     */
    public void setAddress1Addressid(String  address1Addressid){
        this.address1Addressid = address1Addressid ;
        this.modify("address1_addressid",address1Addressid);
    }

    /**
     * 设置 [ADDRESS2_POSTOFFICEBOX]
     */
    public void setAddress2Postofficebox(String  address2Postofficebox){
        this.address2Postofficebox = address2Postofficebox ;
        this.modify("address2_postofficebox",address2Postofficebox);
    }

    /**
     * 设置 [CURRENCYNAME]
     */
    public void setCurrencyname(String  currencyname){
        this.currencyname = currencyname ;
        this.modify("currencyname",currencyname);
    }

    /**
     * 设置 [DEFAULTPRICELEVELNAME]
     */
    public void setDefaultpricelevelname(String  defaultpricelevelname){
        this.defaultpricelevelname = defaultpricelevelname ;
        this.modify("defaultpricelevelname",defaultpricelevelname);
    }

    /**
     * 设置 [SLANAME]
     */
    public void setSlaname(String  slaname){
        this.slaname = slaname ;
        this.modify("slaname",slaname);
    }

    /**
     * 设置 [PREFERREDEQUIPMENTNAME]
     */
    public void setPreferredequipmentname(String  preferredequipmentname){
        this.preferredequipmentname = preferredequipmentname ;
        this.modify("preferredequipmentname",preferredequipmentname);
    }

    /**
     * 设置 [PREFERREDSERVICENAME]
     */
    public void setPreferredservicename(String  preferredservicename){
        this.preferredservicename = preferredservicename ;
        this.modify("preferredservicename",preferredservicename);
    }

    /**
     * 设置 [TERRITORYNAME]
     */
    public void setTerritoryname(String  territoryname){
        this.territoryname = territoryname ;
        this.modify("territoryname",territoryname);
    }

    /**
     * 设置 [PRIMARYCONTACTNAME]
     */
    public void setPrimarycontactname(String  primarycontactname){
        this.primarycontactname = primarycontactname ;
        this.modify("primarycontactname",primarycontactname);
    }

    /**
     * 设置 [PARENTACCOUNTNAME]
     */
    public void setParentaccountname(String  parentaccountname){
        this.parentaccountname = parentaccountname ;
        this.modify("parentaccountname",parentaccountname);
    }

    /**
     * 设置 [ORIGINATINGLEADNAME]
     */
    public void setOriginatingleadname(String  originatingleadname){
        this.originatingleadname = originatingleadname ;
        this.modify("originatingleadname",originatingleadname);
    }

    /**
     * 设置 [ORIGINATINGLEADID]
     */
    public void setOriginatingleadid(String  originatingleadid){
        this.originatingleadid = originatingleadid ;
        this.modify("originatingleadid",originatingleadid);
    }

    /**
     * 设置 [TRANSACTIONCURRENCYID]
     */
    public void setTransactioncurrencyid(String  transactioncurrencyid){
        this.transactioncurrencyid = transactioncurrencyid ;
        this.modify("transactioncurrencyid",transactioncurrencyid);
    }

    /**
     * 设置 [DEFAULTPRICELEVELID]
     */
    public void setDefaultpricelevelid(String  defaultpricelevelid){
        this.defaultpricelevelid = defaultpricelevelid ;
        this.modify("defaultpricelevelid",defaultpricelevelid);
    }

    /**
     * 设置 [PARENTACCOUNTID]
     */
    public void setParentaccountid(String  parentaccountid){
        this.parentaccountid = parentaccountid ;
        this.modify("parentaccountid",parentaccountid);
    }

    /**
     * 设置 [PREFERREDEQUIPMENTID]
     */
    public void setPreferredequipmentid(String  preferredequipmentid){
        this.preferredequipmentid = preferredequipmentid ;
        this.modify("preferredequipmentid",preferredequipmentid);
    }

    /**
     * 设置 [TERRITORYID]
     */
    public void setTerritoryid(String  territoryid){
        this.territoryid = territoryid ;
        this.modify("territoryid",territoryid);
    }

    /**
     * 设置 [PRIMARYCONTACTID]
     */
    public void setPrimarycontactid(String  primarycontactid){
        this.primarycontactid = primarycontactid ;
        this.modify("primarycontactid",primarycontactid);
    }

    /**
     * 设置 [SLAID]
     */
    public void setSlaid(String  slaid){
        this.slaid = slaid ;
        this.modify("slaid",slaid);
    }

    /**
     * 设置 [PREFERREDSERVICEID]
     */
    public void setPreferredserviceid(String  preferredserviceid){
        this.preferredserviceid = preferredserviceid ;
        this.modify("preferredserviceid",preferredserviceid);
    }


}

