package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.base.domain.UomSchedule;
import cn.ibizlab.businesscentral.core.base.service.IUomScheduleService;
import cn.ibizlab.businesscentral.core.base.filter.UomScheduleSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"计价单位组" })
@RestController("CentralApi-uomschedule")
@RequestMapping("")
public class UomScheduleResource {

    @Autowired
    public IUomScheduleService uomscheduleService;

    @Autowired
    @Lazy
    public UomScheduleMapping uomscheduleMapping;

    @PreAuthorize("hasPermission(this.uomscheduleMapping.toDomain(#uomscheduledto),'iBizBusinessCentral-UomSchedule-Create')")
    @ApiOperation(value = "新建计价单位组", tags = {"计价单位组" },  notes = "新建计价单位组")
	@RequestMapping(method = RequestMethod.POST, value = "/uomschedules")
    public ResponseEntity<UomScheduleDTO> create(@RequestBody UomScheduleDTO uomscheduledto) {
        UomSchedule domain = uomscheduleMapping.toDomain(uomscheduledto);
		uomscheduleService.create(domain);
        UomScheduleDTO dto = uomscheduleMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.uomscheduleMapping.toDomain(#uomscheduledtos),'iBizBusinessCentral-UomSchedule-Create')")
    @ApiOperation(value = "批量新建计价单位组", tags = {"计价单位组" },  notes = "批量新建计价单位组")
	@RequestMapping(method = RequestMethod.POST, value = "/uomschedules/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<UomScheduleDTO> uomscheduledtos) {
        uomscheduleService.createBatch(uomscheduleMapping.toDomain(uomscheduledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "uomschedule" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.uomscheduleService.get(#uomschedule_id),'iBizBusinessCentral-UomSchedule-Update')")
    @ApiOperation(value = "更新计价单位组", tags = {"计价单位组" },  notes = "更新计价单位组")
	@RequestMapping(method = RequestMethod.PUT, value = "/uomschedules/{uomschedule_id}")
    public ResponseEntity<UomScheduleDTO> update(@PathVariable("uomschedule_id") String uomschedule_id, @RequestBody UomScheduleDTO uomscheduledto) {
		UomSchedule domain  = uomscheduleMapping.toDomain(uomscheduledto);
        domain .setUomscheduleid(uomschedule_id);
		uomscheduleService.update(domain );
		UomScheduleDTO dto = uomscheduleMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.uomscheduleService.getUomscheduleByEntities(this.uomscheduleMapping.toDomain(#uomscheduledtos)),'iBizBusinessCentral-UomSchedule-Update')")
    @ApiOperation(value = "批量更新计价单位组", tags = {"计价单位组" },  notes = "批量更新计价单位组")
	@RequestMapping(method = RequestMethod.PUT, value = "/uomschedules/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<UomScheduleDTO> uomscheduledtos) {
        uomscheduleService.updateBatch(uomscheduleMapping.toDomain(uomscheduledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.uomscheduleService.get(#uomschedule_id),'iBizBusinessCentral-UomSchedule-Remove')")
    @ApiOperation(value = "删除计价单位组", tags = {"计价单位组" },  notes = "删除计价单位组")
	@RequestMapping(method = RequestMethod.DELETE, value = "/uomschedules/{uomschedule_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("uomschedule_id") String uomschedule_id) {
         return ResponseEntity.status(HttpStatus.OK).body(uomscheduleService.remove(uomschedule_id));
    }

    @PreAuthorize("hasPermission(this.uomscheduleService.getUomscheduleByIds(#ids),'iBizBusinessCentral-UomSchedule-Remove')")
    @ApiOperation(value = "批量删除计价单位组", tags = {"计价单位组" },  notes = "批量删除计价单位组")
	@RequestMapping(method = RequestMethod.DELETE, value = "/uomschedules/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        uomscheduleService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.uomscheduleMapping.toDomain(returnObject.body),'iBizBusinessCentral-UomSchedule-Get')")
    @ApiOperation(value = "获取计价单位组", tags = {"计价单位组" },  notes = "获取计价单位组")
	@RequestMapping(method = RequestMethod.GET, value = "/uomschedules/{uomschedule_id}")
    public ResponseEntity<UomScheduleDTO> get(@PathVariable("uomschedule_id") String uomschedule_id) {
        UomSchedule domain = uomscheduleService.get(uomschedule_id);
        UomScheduleDTO dto = uomscheduleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取计价单位组草稿", tags = {"计价单位组" },  notes = "获取计价单位组草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/uomschedules/getdraft")
    public ResponseEntity<UomScheduleDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(uomscheduleMapping.toDto(uomscheduleService.getDraft(new UomSchedule())));
    }

    @ApiOperation(value = "检查计价单位组", tags = {"计价单位组" },  notes = "检查计价单位组")
	@RequestMapping(method = RequestMethod.POST, value = "/uomschedules/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody UomScheduleDTO uomscheduledto) {
        return  ResponseEntity.status(HttpStatus.OK).body(uomscheduleService.checkKey(uomscheduleMapping.toDomain(uomscheduledto)));
    }

    @PreAuthorize("hasPermission(this.uomscheduleMapping.toDomain(#uomscheduledto),'iBizBusinessCentral-UomSchedule-Save')")
    @ApiOperation(value = "保存计价单位组", tags = {"计价单位组" },  notes = "保存计价单位组")
	@RequestMapping(method = RequestMethod.POST, value = "/uomschedules/save")
    public ResponseEntity<Boolean> save(@RequestBody UomScheduleDTO uomscheduledto) {
        return ResponseEntity.status(HttpStatus.OK).body(uomscheduleService.save(uomscheduleMapping.toDomain(uomscheduledto)));
    }

    @PreAuthorize("hasPermission(this.uomscheduleMapping.toDomain(#uomscheduledtos),'iBizBusinessCentral-UomSchedule-Save')")
    @ApiOperation(value = "批量保存计价单位组", tags = {"计价单位组" },  notes = "批量保存计价单位组")
	@RequestMapping(method = RequestMethod.POST, value = "/uomschedules/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<UomScheduleDTO> uomscheduledtos) {
        uomscheduleService.saveBatch(uomscheduleMapping.toDomain(uomscheduledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-UomSchedule-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-UomSchedule-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"计价单位组" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/uomschedules/fetchdefault")
	public ResponseEntity<List<UomScheduleDTO>> fetchDefault(UomScheduleSearchContext context) {
        Page<UomSchedule> domains = uomscheduleService.searchDefault(context) ;
        List<UomScheduleDTO> list = uomscheduleMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-UomSchedule-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-UomSchedule-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"计价单位组" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/uomschedules/searchdefault")
	public ResponseEntity<Page<UomScheduleDTO>> searchDefault(@RequestBody UomScheduleSearchContext context) {
        Page<UomSchedule> domains = uomscheduleService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(uomscheduleMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

