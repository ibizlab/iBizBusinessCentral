package cn.ibizlab.businesscentral.centralapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[OMHierarchyDTO]
 */
@Data
public class OMHierarchyDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [OMHIERARCHYID]
     *
     */
    @JSONField(name = "omhierarchyid")
    @JsonProperty("omhierarchyid")
    private String omhierarchyid;

    /**
     * 属性 [OMHIERARCHYNAME]
     *
     */
    @JSONField(name = "omhierarchyname")
    @JsonProperty("omhierarchyname")
    private String omhierarchyname;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;

    /**
     * 属性 [ORGANIZATIONID]
     *
     */
    @JSONField(name = "organizationid")
    @JsonProperty("organizationid")
    private String organizationid;

    /**
     * 属性 [POMHIERARCHYID]
     *
     */
    @JSONField(name = "pomhierarchyid")
    @JsonProperty("pomhierarchyid")
    private String pomhierarchyid;

    /**
     * 属性 [OMHIERARCHYCATID]
     *
     */
    @JSONField(name = "omhierarchycatid")
    @JsonProperty("omhierarchycatid")
    private String omhierarchycatid;

    /**
     * 属性 [VALID]
     *
     */
    @JSONField(name = "valid")
    @JsonProperty("valid")
    private Integer valid;

    /**
     * 属性 [DISPLAYNAME]
     *
     */
    @JSONField(name = "displayname")
    @JsonProperty("displayname")
    private String displayname;

    /**
     * 属性 [SN]
     *
     */
    @JSONField(name = "sn")
    @JsonProperty("sn")
    private Integer sn;

    /**
     * 属性 [CODEVALUE]
     *
     */
    @JSONField(name = "codevalue")
    @JsonProperty("codevalue")
    private String codevalue;

    /**
     * 属性 [SHOWORDER]
     *
     */
    @JSONField(name = "showorder")
    @JsonProperty("showorder")
    private BigInteger showorder;

    /**
     * 属性 [SHORTNAME]
     *
     */
    @JSONField(name = "shortname")
    @JsonProperty("shortname")
    private String shortname;

    /**
     * 属性 [ORGANIZATIONNAME]
     *
     */
    @JSONField(name = "organizationname")
    @JsonProperty("organizationname")
    private String organizationname;

    /**
     * 属性 [OMHIERARCHYCATNAME]
     *
     */
    @JSONField(name = "omhierarchycatname")
    @JsonProperty("omhierarchycatname")
    private String omhierarchycatname;

    /**
     * 属性 [POMHIERARCHYNAME]
     *
     */
    @JSONField(name = "pomhierarchyname")
    @JsonProperty("pomhierarchyname")
    private String pomhierarchyname;


    /**
     * 设置 [OMHIERARCHYNAME]
     */
    public void setOmhierarchyname(String  omhierarchyname){
        this.omhierarchyname = omhierarchyname ;
        this.modify("omhierarchyname",omhierarchyname);
    }

    /**
     * 设置 [ORGANIZATIONID]
     */
    public void setOrganizationid(String  organizationid){
        this.organizationid = organizationid ;
        this.modify("organizationid",organizationid);
    }

    /**
     * 设置 [POMHIERARCHYID]
     */
    public void setPomhierarchyid(String  pomhierarchyid){
        this.pomhierarchyid = pomhierarchyid ;
        this.modify("pomhierarchyid",pomhierarchyid);
    }

    /**
     * 设置 [OMHIERARCHYCATID]
     */
    public void setOmhierarchycatid(String  omhierarchycatid){
        this.omhierarchycatid = omhierarchycatid ;
        this.modify("omhierarchycatid",omhierarchycatid);
    }

    /**
     * 设置 [VALID]
     */
    public void setValid(Integer  valid){
        this.valid = valid ;
        this.modify("valid",valid);
    }

    /**
     * 设置 [DISPLAYNAME]
     */
    public void setDisplayname(String  displayname){
        this.displayname = displayname ;
        this.modify("displayname",displayname);
    }

    /**
     * 设置 [SN]
     */
    public void setSn(Integer  sn){
        this.sn = sn ;
        this.modify("sn",sn);
    }

    /**
     * 设置 [CODEVALUE]
     */
    public void setCodevalue(String  codevalue){
        this.codevalue = codevalue ;
        this.modify("codevalue",codevalue);
    }

    /**
     * 设置 [SHOWORDER]
     */
    public void setShoworder(BigInteger  showorder){
        this.showorder = showorder ;
        this.modify("showorder",showorder);
    }


}

