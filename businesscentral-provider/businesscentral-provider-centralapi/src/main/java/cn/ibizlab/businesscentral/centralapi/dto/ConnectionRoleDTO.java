package cn.ibizlab.businesscentral.centralapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[ConnectionRoleDTO]
 */
@Data
public class ConnectionRoleDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [OVERWRITETIME]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "overwritetime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("overwritetime")
    private Timestamp overwritetime;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [MANAGED]
     *
     */
    @JSONField(name = "managed")
    @JsonProperty("managed")
    private Integer managed;

    /**
     * 属性 [SUPPORTINGSOLUTIONID]
     *
     */
    @JSONField(name = "supportingsolutionid")
    @JsonProperty("supportingsolutionid")
    private String supportingsolutionid;

    /**
     * 属性 [COMPONENTSTATE]
     *
     */
    @JSONField(name = "componentstate")
    @JsonProperty("componentstate")
    private String componentstate;

    /**
     * 属性 [CATEGORY]
     *
     */
    @JSONField(name = "category")
    @JsonProperty("category")
    private String category;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [IMPORTSEQUENCENUMBER]
     *
     */
    @JSONField(name = "importsequencenumber")
    @JsonProperty("importsequencenumber")
    private Integer importsequencenumber;

    /**
     * 属性 [CONNECTIONROLENAME]
     *
     */
    @JSONField(name = "connectionrolename")
    @JsonProperty("connectionrolename")
    private String connectionrolename;

    /**
     * 属性 [STATUSCODE]
     *
     */
    @JSONField(name = "statuscode")
    @JsonProperty("statuscode")
    private Integer statuscode;

    /**
     * 属性 [CONNECTIONROLEID]
     *
     */
    @JSONField(name = "connectionroleid")
    @JsonProperty("connectionroleid")
    private String connectionroleid;

    /**
     * 属性 [CONNECTIONROLEIDUNIQUE]
     *
     */
    @JSONField(name = "connectionroleidunique")
    @JsonProperty("connectionroleidunique")
    private String connectionroleidunique;

    /**
     * 属性 [VERSIONNUMBER]
     *
     */
    @JSONField(name = "versionnumber")
    @JsonProperty("versionnumber")
    private BigInteger versionnumber;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;

    /**
     * 属性 [SOLUTIONID]
     *
     */
    @JSONField(name = "solutionid")
    @JsonProperty("solutionid")
    private String solutionid;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 属性 [CUSTOMIZABLE]
     *
     */
    @JSONField(name = "customizable")
    @JsonProperty("customizable")
    private String customizable;

    /**
     * 属性 [INTRODUCEDVERSION]
     *
     */
    @JSONField(name = "introducedversion")
    @JsonProperty("introducedversion")
    private String introducedversion;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;

    /**
     * 属性 [STATECODE]
     *
     */
    @JSONField(name = "statecode")
    @JsonProperty("statecode")
    private Integer statecode;


    /**
     * 设置 [OVERWRITETIME]
     */
    public void setOverwritetime(Timestamp  overwritetime){
        this.overwritetime = overwritetime ;
        this.modify("overwritetime",overwritetime);
    }

    /**
     * 设置 [MANAGED]
     */
    public void setManaged(Integer  managed){
        this.managed = managed ;
        this.modify("managed",managed);
    }

    /**
     * 设置 [SUPPORTINGSOLUTIONID]
     */
    public void setSupportingsolutionid(String  supportingsolutionid){
        this.supportingsolutionid = supportingsolutionid ;
        this.modify("supportingsolutionid",supportingsolutionid);
    }

    /**
     * 设置 [COMPONENTSTATE]
     */
    public void setComponentstate(String  componentstate){
        this.componentstate = componentstate ;
        this.modify("componentstate",componentstate);
    }

    /**
     * 设置 [CATEGORY]
     */
    public void setCategory(String  category){
        this.category = category ;
        this.modify("category",category);
    }

    /**
     * 设置 [IMPORTSEQUENCENUMBER]
     */
    public void setImportsequencenumber(Integer  importsequencenumber){
        this.importsequencenumber = importsequencenumber ;
        this.modify("importsequencenumber",importsequencenumber);
    }

    /**
     * 设置 [CONNECTIONROLENAME]
     */
    public void setConnectionrolename(String  connectionrolename){
        this.connectionrolename = connectionrolename ;
        this.modify("connectionrolename",connectionrolename);
    }

    /**
     * 设置 [STATUSCODE]
     */
    public void setStatuscode(Integer  statuscode){
        this.statuscode = statuscode ;
        this.modify("statuscode",statuscode);
    }

    /**
     * 设置 [CONNECTIONROLEIDUNIQUE]
     */
    public void setConnectionroleidunique(String  connectionroleidunique){
        this.connectionroleidunique = connectionroleidunique ;
        this.modify("connectionroleidunique",connectionroleidunique);
    }

    /**
     * 设置 [VERSIONNUMBER]
     */
    public void setVersionnumber(BigInteger  versionnumber){
        this.versionnumber = versionnumber ;
        this.modify("versionnumber",versionnumber);
    }

    /**
     * 设置 [SOLUTIONID]
     */
    public void setSolutionid(String  solutionid){
        this.solutionid = solutionid ;
        this.modify("solutionid",solutionid);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [CUSTOMIZABLE]
     */
    public void setCustomizable(String  customizable){
        this.customizable = customizable ;
        this.modify("customizable",customizable);
    }

    /**
     * 设置 [INTRODUCEDVERSION]
     */
    public void setIntroducedversion(String  introducedversion){
        this.introducedversion = introducedversion ;
        this.modify("introducedversion",introducedversion);
    }

    /**
     * 设置 [STATECODE]
     */
    public void setStatecode(Integer  statecode){
        this.statecode = statecode ;
        this.modify("statecode",statecode);
    }


}

