package cn.ibizlab.businesscentral.centralapi.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "service.centralapi")
@Data
public class CentralApiServiceProperties {

	private boolean enabled;

	private boolean auth;


}