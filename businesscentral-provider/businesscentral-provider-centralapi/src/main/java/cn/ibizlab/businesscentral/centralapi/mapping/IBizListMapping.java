package cn.ibizlab.businesscentral.centralapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.marketing.domain.IBizList;
import cn.ibizlab.businesscentral.centralapi.dto.IBizListDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CentralApiIBizListMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface IBizListMapping extends MappingBase<IBizListDTO, IBizList> {


}

