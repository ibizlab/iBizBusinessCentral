package cn.ibizlab.businesscentral.centralapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.finance.domain.InvoiceDetail;
import cn.ibizlab.businesscentral.centralapi.dto.InvoiceDetailDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CentralApiInvoiceDetailMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface InvoiceDetailMapping extends MappingBase<InvoiceDetailDTO, InvoiceDetail> {


}

