package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.product.domain.ProductSubstitute;
import cn.ibizlab.businesscentral.core.product.service.IProductSubstituteService;
import cn.ibizlab.businesscentral.core.product.filter.ProductSubstituteSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"产品替换" })
@RestController("CentralApi-productsubstitute")
@RequestMapping("")
public class ProductSubstituteResource {

    @Autowired
    public IProductSubstituteService productsubstituteService;

    @Autowired
    @Lazy
    public ProductSubstituteMapping productsubstituteMapping;

    @PreAuthorize("hasPermission(this.productsubstituteMapping.toDomain(#productsubstitutedto),'iBizBusinessCentral-ProductSubstitute-Create')")
    @ApiOperation(value = "新建产品替换", tags = {"产品替换" },  notes = "新建产品替换")
	@RequestMapping(method = RequestMethod.POST, value = "/productsubstitutes")
    public ResponseEntity<ProductSubstituteDTO> create(@RequestBody ProductSubstituteDTO productsubstitutedto) {
        ProductSubstitute domain = productsubstituteMapping.toDomain(productsubstitutedto);
		productsubstituteService.create(domain);
        ProductSubstituteDTO dto = productsubstituteMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.productsubstituteMapping.toDomain(#productsubstitutedtos),'iBizBusinessCentral-ProductSubstitute-Create')")
    @ApiOperation(value = "批量新建产品替换", tags = {"产品替换" },  notes = "批量新建产品替换")
	@RequestMapping(method = RequestMethod.POST, value = "/productsubstitutes/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<ProductSubstituteDTO> productsubstitutedtos) {
        productsubstituteService.createBatch(productsubstituteMapping.toDomain(productsubstitutedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "productsubstitute" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.productsubstituteService.get(#productsubstitute_id),'iBizBusinessCentral-ProductSubstitute-Update')")
    @ApiOperation(value = "更新产品替换", tags = {"产品替换" },  notes = "更新产品替换")
	@RequestMapping(method = RequestMethod.PUT, value = "/productsubstitutes/{productsubstitute_id}")
    public ResponseEntity<ProductSubstituteDTO> update(@PathVariable("productsubstitute_id") String productsubstitute_id, @RequestBody ProductSubstituteDTO productsubstitutedto) {
		ProductSubstitute domain  = productsubstituteMapping.toDomain(productsubstitutedto);
        domain .setProductsubstituteid(productsubstitute_id);
		productsubstituteService.update(domain );
		ProductSubstituteDTO dto = productsubstituteMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.productsubstituteService.getProductsubstituteByEntities(this.productsubstituteMapping.toDomain(#productsubstitutedtos)),'iBizBusinessCentral-ProductSubstitute-Update')")
    @ApiOperation(value = "批量更新产品替换", tags = {"产品替换" },  notes = "批量更新产品替换")
	@RequestMapping(method = RequestMethod.PUT, value = "/productsubstitutes/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<ProductSubstituteDTO> productsubstitutedtos) {
        productsubstituteService.updateBatch(productsubstituteMapping.toDomain(productsubstitutedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.productsubstituteService.get(#productsubstitute_id),'iBizBusinessCentral-ProductSubstitute-Remove')")
    @ApiOperation(value = "删除产品替换", tags = {"产品替换" },  notes = "删除产品替换")
	@RequestMapping(method = RequestMethod.DELETE, value = "/productsubstitutes/{productsubstitute_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("productsubstitute_id") String productsubstitute_id) {
         return ResponseEntity.status(HttpStatus.OK).body(productsubstituteService.remove(productsubstitute_id));
    }

    @PreAuthorize("hasPermission(this.productsubstituteService.getProductsubstituteByIds(#ids),'iBizBusinessCentral-ProductSubstitute-Remove')")
    @ApiOperation(value = "批量删除产品替换", tags = {"产品替换" },  notes = "批量删除产品替换")
	@RequestMapping(method = RequestMethod.DELETE, value = "/productsubstitutes/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        productsubstituteService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.productsubstituteMapping.toDomain(returnObject.body),'iBizBusinessCentral-ProductSubstitute-Get')")
    @ApiOperation(value = "获取产品替换", tags = {"产品替换" },  notes = "获取产品替换")
	@RequestMapping(method = RequestMethod.GET, value = "/productsubstitutes/{productsubstitute_id}")
    public ResponseEntity<ProductSubstituteDTO> get(@PathVariable("productsubstitute_id") String productsubstitute_id) {
        ProductSubstitute domain = productsubstituteService.get(productsubstitute_id);
        ProductSubstituteDTO dto = productsubstituteMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取产品替换草稿", tags = {"产品替换" },  notes = "获取产品替换草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/productsubstitutes/getdraft")
    public ResponseEntity<ProductSubstituteDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(productsubstituteMapping.toDto(productsubstituteService.getDraft(new ProductSubstitute())));
    }

    @ApiOperation(value = "检查产品替换", tags = {"产品替换" },  notes = "检查产品替换")
	@RequestMapping(method = RequestMethod.POST, value = "/productsubstitutes/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody ProductSubstituteDTO productsubstitutedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(productsubstituteService.checkKey(productsubstituteMapping.toDomain(productsubstitutedto)));
    }

    @PreAuthorize("hasPermission(this.productsubstituteMapping.toDomain(#productsubstitutedto),'iBizBusinessCentral-ProductSubstitute-Save')")
    @ApiOperation(value = "保存产品替换", tags = {"产品替换" },  notes = "保存产品替换")
	@RequestMapping(method = RequestMethod.POST, value = "/productsubstitutes/save")
    public ResponseEntity<Boolean> save(@RequestBody ProductSubstituteDTO productsubstitutedto) {
        return ResponseEntity.status(HttpStatus.OK).body(productsubstituteService.save(productsubstituteMapping.toDomain(productsubstitutedto)));
    }

    @PreAuthorize("hasPermission(this.productsubstituteMapping.toDomain(#productsubstitutedtos),'iBizBusinessCentral-ProductSubstitute-Save')")
    @ApiOperation(value = "批量保存产品替换", tags = {"产品替换" },  notes = "批量保存产品替换")
	@RequestMapping(method = RequestMethod.POST, value = "/productsubstitutes/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<ProductSubstituteDTO> productsubstitutedtos) {
        productsubstituteService.saveBatch(productsubstituteMapping.toDomain(productsubstitutedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ProductSubstitute-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-ProductSubstitute-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"产品替换" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/productsubstitutes/fetchdefault")
	public ResponseEntity<List<ProductSubstituteDTO>> fetchDefault(ProductSubstituteSearchContext context) {
        Page<ProductSubstitute> domains = productsubstituteService.searchDefault(context) ;
        List<ProductSubstituteDTO> list = productsubstituteMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ProductSubstitute-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-ProductSubstitute-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"产品替换" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/productsubstitutes/searchdefault")
	public ResponseEntity<Page<ProductSubstituteDTO>> searchDefault(@RequestBody ProductSubstituteSearchContext context) {
        Page<ProductSubstitute> domains = productsubstituteService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(productsubstituteMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.productsubstituteMapping.toDomain(#productsubstitutedto),'iBizBusinessCentral-ProductSubstitute-Create')")
    @ApiOperation(value = "根据产品建立产品替换", tags = {"产品替换" },  notes = "根据产品建立产品替换")
	@RequestMapping(method = RequestMethod.POST, value = "/products/{product_id}/productsubstitutes")
    public ResponseEntity<ProductSubstituteDTO> createByProduct(@PathVariable("product_id") String product_id, @RequestBody ProductSubstituteDTO productsubstitutedto) {
        ProductSubstitute domain = productsubstituteMapping.toDomain(productsubstitutedto);
        domain.setProductid(product_id);
		productsubstituteService.create(domain);
        ProductSubstituteDTO dto = productsubstituteMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.productsubstituteMapping.toDomain(#productsubstitutedtos),'iBizBusinessCentral-ProductSubstitute-Create')")
    @ApiOperation(value = "根据产品批量建立产品替换", tags = {"产品替换" },  notes = "根据产品批量建立产品替换")
	@RequestMapping(method = RequestMethod.POST, value = "/products/{product_id}/productsubstitutes/batch")
    public ResponseEntity<Boolean> createBatchByProduct(@PathVariable("product_id") String product_id, @RequestBody List<ProductSubstituteDTO> productsubstitutedtos) {
        List<ProductSubstitute> domainlist=productsubstituteMapping.toDomain(productsubstitutedtos);
        for(ProductSubstitute domain:domainlist){
            domain.setProductid(product_id);
        }
        productsubstituteService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "productsubstitute" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.productsubstituteService.get(#productsubstitute_id),'iBizBusinessCentral-ProductSubstitute-Update')")
    @ApiOperation(value = "根据产品更新产品替换", tags = {"产品替换" },  notes = "根据产品更新产品替换")
	@RequestMapping(method = RequestMethod.PUT, value = "/products/{product_id}/productsubstitutes/{productsubstitute_id}")
    public ResponseEntity<ProductSubstituteDTO> updateByProduct(@PathVariable("product_id") String product_id, @PathVariable("productsubstitute_id") String productsubstitute_id, @RequestBody ProductSubstituteDTO productsubstitutedto) {
        ProductSubstitute domain = productsubstituteMapping.toDomain(productsubstitutedto);
        domain.setProductid(product_id);
        domain.setProductsubstituteid(productsubstitute_id);
		productsubstituteService.update(domain);
        ProductSubstituteDTO dto = productsubstituteMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.productsubstituteService.getProductsubstituteByEntities(this.productsubstituteMapping.toDomain(#productsubstitutedtos)),'iBizBusinessCentral-ProductSubstitute-Update')")
    @ApiOperation(value = "根据产品批量更新产品替换", tags = {"产品替换" },  notes = "根据产品批量更新产品替换")
	@RequestMapping(method = RequestMethod.PUT, value = "/products/{product_id}/productsubstitutes/batch")
    public ResponseEntity<Boolean> updateBatchByProduct(@PathVariable("product_id") String product_id, @RequestBody List<ProductSubstituteDTO> productsubstitutedtos) {
        List<ProductSubstitute> domainlist=productsubstituteMapping.toDomain(productsubstitutedtos);
        for(ProductSubstitute domain:domainlist){
            domain.setProductid(product_id);
        }
        productsubstituteService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.productsubstituteService.get(#productsubstitute_id),'iBizBusinessCentral-ProductSubstitute-Remove')")
    @ApiOperation(value = "根据产品删除产品替换", tags = {"产品替换" },  notes = "根据产品删除产品替换")
	@RequestMapping(method = RequestMethod.DELETE, value = "/products/{product_id}/productsubstitutes/{productsubstitute_id}")
    public ResponseEntity<Boolean> removeByProduct(@PathVariable("product_id") String product_id, @PathVariable("productsubstitute_id") String productsubstitute_id) {
		return ResponseEntity.status(HttpStatus.OK).body(productsubstituteService.remove(productsubstitute_id));
    }

    @PreAuthorize("hasPermission(this.productsubstituteService.getProductsubstituteByIds(#ids),'iBizBusinessCentral-ProductSubstitute-Remove')")
    @ApiOperation(value = "根据产品批量删除产品替换", tags = {"产品替换" },  notes = "根据产品批量删除产品替换")
	@RequestMapping(method = RequestMethod.DELETE, value = "/products/{product_id}/productsubstitutes/batch")
    public ResponseEntity<Boolean> removeBatchByProduct(@RequestBody List<String> ids) {
        productsubstituteService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.productsubstituteMapping.toDomain(returnObject.body),'iBizBusinessCentral-ProductSubstitute-Get')")
    @ApiOperation(value = "根据产品获取产品替换", tags = {"产品替换" },  notes = "根据产品获取产品替换")
	@RequestMapping(method = RequestMethod.GET, value = "/products/{product_id}/productsubstitutes/{productsubstitute_id}")
    public ResponseEntity<ProductSubstituteDTO> getByProduct(@PathVariable("product_id") String product_id, @PathVariable("productsubstitute_id") String productsubstitute_id) {
        ProductSubstitute domain = productsubstituteService.get(productsubstitute_id);
        ProductSubstituteDTO dto = productsubstituteMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据产品获取产品替换草稿", tags = {"产品替换" },  notes = "根据产品获取产品替换草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/products/{product_id}/productsubstitutes/getdraft")
    public ResponseEntity<ProductSubstituteDTO> getDraftByProduct(@PathVariable("product_id") String product_id) {
        ProductSubstitute domain = new ProductSubstitute();
        domain.setProductid(product_id);
        return ResponseEntity.status(HttpStatus.OK).body(productsubstituteMapping.toDto(productsubstituteService.getDraft(domain)));
    }

    @ApiOperation(value = "根据产品检查产品替换", tags = {"产品替换" },  notes = "根据产品检查产品替换")
	@RequestMapping(method = RequestMethod.POST, value = "/products/{product_id}/productsubstitutes/checkkey")
    public ResponseEntity<Boolean> checkKeyByProduct(@PathVariable("product_id") String product_id, @RequestBody ProductSubstituteDTO productsubstitutedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(productsubstituteService.checkKey(productsubstituteMapping.toDomain(productsubstitutedto)));
    }

    @PreAuthorize("hasPermission(this.productsubstituteMapping.toDomain(#productsubstitutedto),'iBizBusinessCentral-ProductSubstitute-Save')")
    @ApiOperation(value = "根据产品保存产品替换", tags = {"产品替换" },  notes = "根据产品保存产品替换")
	@RequestMapping(method = RequestMethod.POST, value = "/products/{product_id}/productsubstitutes/save")
    public ResponseEntity<Boolean> saveByProduct(@PathVariable("product_id") String product_id, @RequestBody ProductSubstituteDTO productsubstitutedto) {
        ProductSubstitute domain = productsubstituteMapping.toDomain(productsubstitutedto);
        domain.setProductid(product_id);
        return ResponseEntity.status(HttpStatus.OK).body(productsubstituteService.save(domain));
    }

    @PreAuthorize("hasPermission(this.productsubstituteMapping.toDomain(#productsubstitutedtos),'iBizBusinessCentral-ProductSubstitute-Save')")
    @ApiOperation(value = "根据产品批量保存产品替换", tags = {"产品替换" },  notes = "根据产品批量保存产品替换")
	@RequestMapping(method = RequestMethod.POST, value = "/products/{product_id}/productsubstitutes/savebatch")
    public ResponseEntity<Boolean> saveBatchByProduct(@PathVariable("product_id") String product_id, @RequestBody List<ProductSubstituteDTO> productsubstitutedtos) {
        List<ProductSubstitute> domainlist=productsubstituteMapping.toDomain(productsubstitutedtos);
        for(ProductSubstitute domain:domainlist){
             domain.setProductid(product_id);
        }
        productsubstituteService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ProductSubstitute-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-ProductSubstitute-Get')")
	@ApiOperation(value = "根据产品获取DEFAULT", tags = {"产品替换" } ,notes = "根据产品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/products/{product_id}/productsubstitutes/fetchdefault")
	public ResponseEntity<List<ProductSubstituteDTO>> fetchProductSubstituteDefaultByProduct(@PathVariable("product_id") String product_id,ProductSubstituteSearchContext context) {
        context.setN_productid_eq(product_id);
        Page<ProductSubstitute> domains = productsubstituteService.searchDefault(context) ;
        List<ProductSubstituteDTO> list = productsubstituteMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ProductSubstitute-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-ProductSubstitute-Get')")
	@ApiOperation(value = "根据产品查询DEFAULT", tags = {"产品替换" } ,notes = "根据产品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/products/{product_id}/productsubstitutes/searchdefault")
	public ResponseEntity<Page<ProductSubstituteDTO>> searchProductSubstituteDefaultByProduct(@PathVariable("product_id") String product_id, @RequestBody ProductSubstituteSearchContext context) {
        context.setN_productid_eq(product_id);
        Page<ProductSubstitute> domains = productsubstituteService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(productsubstituteMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

