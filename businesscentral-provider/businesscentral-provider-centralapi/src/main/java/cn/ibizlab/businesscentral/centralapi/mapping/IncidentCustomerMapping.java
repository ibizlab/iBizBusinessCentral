package cn.ibizlab.businesscentral.centralapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.service.domain.IncidentCustomer;
import cn.ibizlab.businesscentral.centralapi.dto.IncidentCustomerDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CentralApiIncidentCustomerMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface IncidentCustomerMapping extends MappingBase<IncidentCustomerDTO, IncidentCustomer> {


}

