package cn.ibizlab.businesscentral.centralapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.sales.domain.SalesLiteratureItem;
import cn.ibizlab.businesscentral.centralapi.dto.SalesLiteratureItemDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CentralApiSalesLiteratureItemMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface SalesLiteratureItemMapping extends MappingBase<SalesLiteratureItemDTO, SalesLiteratureItem> {


}

