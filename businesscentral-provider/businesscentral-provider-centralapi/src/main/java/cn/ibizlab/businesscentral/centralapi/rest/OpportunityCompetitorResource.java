package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.sales.domain.OpportunityCompetitor;
import cn.ibizlab.businesscentral.core.sales.service.IOpportunityCompetitorService;
import cn.ibizlab.businesscentral.core.sales.filter.OpportunityCompetitorSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"商机对手" })
@RestController("CentralApi-opportunitycompetitor")
@RequestMapping("")
public class OpportunityCompetitorResource {

    @Autowired
    public IOpportunityCompetitorService opportunitycompetitorService;

    @Autowired
    @Lazy
    public OpportunityCompetitorMapping opportunitycompetitorMapping;

    @PreAuthorize("hasPermission(this.opportunitycompetitorMapping.toDomain(#opportunitycompetitordto),'iBizBusinessCentral-OpportunityCompetitor-Create')")
    @ApiOperation(value = "新建商机对手", tags = {"商机对手" },  notes = "新建商机对手")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunitycompetitors")
    public ResponseEntity<OpportunityCompetitorDTO> create(@RequestBody OpportunityCompetitorDTO opportunitycompetitordto) {
        OpportunityCompetitor domain = opportunitycompetitorMapping.toDomain(opportunitycompetitordto);
		opportunitycompetitorService.create(domain);
        OpportunityCompetitorDTO dto = opportunitycompetitorMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.opportunitycompetitorMapping.toDomain(#opportunitycompetitordtos),'iBizBusinessCentral-OpportunityCompetitor-Create')")
    @ApiOperation(value = "批量新建商机对手", tags = {"商机对手" },  notes = "批量新建商机对手")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunitycompetitors/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<OpportunityCompetitorDTO> opportunitycompetitordtos) {
        opportunitycompetitorService.createBatch(opportunitycompetitorMapping.toDomain(opportunitycompetitordtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "opportunitycompetitor" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.opportunitycompetitorService.get(#opportunitycompetitor_id),'iBizBusinessCentral-OpportunityCompetitor-Update')")
    @ApiOperation(value = "更新商机对手", tags = {"商机对手" },  notes = "更新商机对手")
	@RequestMapping(method = RequestMethod.PUT, value = "/opportunitycompetitors/{opportunitycompetitor_id}")
    public ResponseEntity<OpportunityCompetitorDTO> update(@PathVariable("opportunitycompetitor_id") String opportunitycompetitor_id, @RequestBody OpportunityCompetitorDTO opportunitycompetitordto) {
		OpportunityCompetitor domain  = opportunitycompetitorMapping.toDomain(opportunitycompetitordto);
        domain .setRelationshipsid(opportunitycompetitor_id);
		opportunitycompetitorService.update(domain );
		OpportunityCompetitorDTO dto = opportunitycompetitorMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.opportunitycompetitorService.getOpportunitycompetitorByEntities(this.opportunitycompetitorMapping.toDomain(#opportunitycompetitordtos)),'iBizBusinessCentral-OpportunityCompetitor-Update')")
    @ApiOperation(value = "批量更新商机对手", tags = {"商机对手" },  notes = "批量更新商机对手")
	@RequestMapping(method = RequestMethod.PUT, value = "/opportunitycompetitors/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<OpportunityCompetitorDTO> opportunitycompetitordtos) {
        opportunitycompetitorService.updateBatch(opportunitycompetitorMapping.toDomain(opportunitycompetitordtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.opportunitycompetitorService.get(#opportunitycompetitor_id),'iBizBusinessCentral-OpportunityCompetitor-Remove')")
    @ApiOperation(value = "删除商机对手", tags = {"商机对手" },  notes = "删除商机对手")
	@RequestMapping(method = RequestMethod.DELETE, value = "/opportunitycompetitors/{opportunitycompetitor_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("opportunitycompetitor_id") String opportunitycompetitor_id) {
         return ResponseEntity.status(HttpStatus.OK).body(opportunitycompetitorService.remove(opportunitycompetitor_id));
    }

    @PreAuthorize("hasPermission(this.opportunitycompetitorService.getOpportunitycompetitorByIds(#ids),'iBizBusinessCentral-OpportunityCompetitor-Remove')")
    @ApiOperation(value = "批量删除商机对手", tags = {"商机对手" },  notes = "批量删除商机对手")
	@RequestMapping(method = RequestMethod.DELETE, value = "/opportunitycompetitors/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        opportunitycompetitorService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.opportunitycompetitorMapping.toDomain(returnObject.body),'iBizBusinessCentral-OpportunityCompetitor-Get')")
    @ApiOperation(value = "获取商机对手", tags = {"商机对手" },  notes = "获取商机对手")
	@RequestMapping(method = RequestMethod.GET, value = "/opportunitycompetitors/{opportunitycompetitor_id}")
    public ResponseEntity<OpportunityCompetitorDTO> get(@PathVariable("opportunitycompetitor_id") String opportunitycompetitor_id) {
        OpportunityCompetitor domain = opportunitycompetitorService.get(opportunitycompetitor_id);
        OpportunityCompetitorDTO dto = opportunitycompetitorMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取商机对手草稿", tags = {"商机对手" },  notes = "获取商机对手草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/opportunitycompetitors/getdraft")
    public ResponseEntity<OpportunityCompetitorDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(opportunitycompetitorMapping.toDto(opportunitycompetitorService.getDraft(new OpportunityCompetitor())));
    }

    @ApiOperation(value = "检查商机对手", tags = {"商机对手" },  notes = "检查商机对手")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunitycompetitors/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody OpportunityCompetitorDTO opportunitycompetitordto) {
        return  ResponseEntity.status(HttpStatus.OK).body(opportunitycompetitorService.checkKey(opportunitycompetitorMapping.toDomain(opportunitycompetitordto)));
    }

    @PreAuthorize("hasPermission(this.opportunitycompetitorMapping.toDomain(#opportunitycompetitordto),'iBizBusinessCentral-OpportunityCompetitor-Save')")
    @ApiOperation(value = "保存商机对手", tags = {"商机对手" },  notes = "保存商机对手")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunitycompetitors/save")
    public ResponseEntity<Boolean> save(@RequestBody OpportunityCompetitorDTO opportunitycompetitordto) {
        return ResponseEntity.status(HttpStatus.OK).body(opportunitycompetitorService.save(opportunitycompetitorMapping.toDomain(opportunitycompetitordto)));
    }

    @PreAuthorize("hasPermission(this.opportunitycompetitorMapping.toDomain(#opportunitycompetitordtos),'iBizBusinessCentral-OpportunityCompetitor-Save')")
    @ApiOperation(value = "批量保存商机对手", tags = {"商机对手" },  notes = "批量保存商机对手")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunitycompetitors/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<OpportunityCompetitorDTO> opportunitycompetitordtos) {
        opportunitycompetitorService.saveBatch(opportunitycompetitorMapping.toDomain(opportunitycompetitordtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OpportunityCompetitor-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-OpportunityCompetitor-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"商机对手" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/opportunitycompetitors/fetchdefault")
	public ResponseEntity<List<OpportunityCompetitorDTO>> fetchDefault(OpportunityCompetitorSearchContext context) {
        Page<OpportunityCompetitor> domains = opportunitycompetitorService.searchDefault(context) ;
        List<OpportunityCompetitorDTO> list = opportunitycompetitorMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OpportunityCompetitor-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-OpportunityCompetitor-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"商机对手" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/opportunitycompetitors/searchdefault")
	public ResponseEntity<Page<OpportunityCompetitorDTO>> searchDefault(@RequestBody OpportunityCompetitorSearchContext context) {
        Page<OpportunityCompetitor> domains = opportunitycompetitorService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(opportunitycompetitorMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.opportunitycompetitorMapping.toDomain(#opportunitycompetitordto),'iBizBusinessCentral-OpportunityCompetitor-Create')")
    @ApiOperation(value = "根据竞争对手建立商机对手", tags = {"商机对手" },  notes = "根据竞争对手建立商机对手")
	@RequestMapping(method = RequestMethod.POST, value = "/competitors/{competitor_id}/opportunitycompetitors")
    public ResponseEntity<OpportunityCompetitorDTO> createByCompetitor(@PathVariable("competitor_id") String competitor_id, @RequestBody OpportunityCompetitorDTO opportunitycompetitordto) {
        OpportunityCompetitor domain = opportunitycompetitorMapping.toDomain(opportunitycompetitordto);
        domain.setEntity2id(competitor_id);
		opportunitycompetitorService.create(domain);
        OpportunityCompetitorDTO dto = opportunitycompetitorMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.opportunitycompetitorMapping.toDomain(#opportunitycompetitordtos),'iBizBusinessCentral-OpportunityCompetitor-Create')")
    @ApiOperation(value = "根据竞争对手批量建立商机对手", tags = {"商机对手" },  notes = "根据竞争对手批量建立商机对手")
	@RequestMapping(method = RequestMethod.POST, value = "/competitors/{competitor_id}/opportunitycompetitors/batch")
    public ResponseEntity<Boolean> createBatchByCompetitor(@PathVariable("competitor_id") String competitor_id, @RequestBody List<OpportunityCompetitorDTO> opportunitycompetitordtos) {
        List<OpportunityCompetitor> domainlist=opportunitycompetitorMapping.toDomain(opportunitycompetitordtos);
        for(OpportunityCompetitor domain:domainlist){
            domain.setEntity2id(competitor_id);
        }
        opportunitycompetitorService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "opportunitycompetitor" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.opportunitycompetitorService.get(#opportunitycompetitor_id),'iBizBusinessCentral-OpportunityCompetitor-Update')")
    @ApiOperation(value = "根据竞争对手更新商机对手", tags = {"商机对手" },  notes = "根据竞争对手更新商机对手")
	@RequestMapping(method = RequestMethod.PUT, value = "/competitors/{competitor_id}/opportunitycompetitors/{opportunitycompetitor_id}")
    public ResponseEntity<OpportunityCompetitorDTO> updateByCompetitor(@PathVariable("competitor_id") String competitor_id, @PathVariable("opportunitycompetitor_id") String opportunitycompetitor_id, @RequestBody OpportunityCompetitorDTO opportunitycompetitordto) {
        OpportunityCompetitor domain = opportunitycompetitorMapping.toDomain(opportunitycompetitordto);
        domain.setEntity2id(competitor_id);
        domain.setRelationshipsid(opportunitycompetitor_id);
		opportunitycompetitorService.update(domain);
        OpportunityCompetitorDTO dto = opportunitycompetitorMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.opportunitycompetitorService.getOpportunitycompetitorByEntities(this.opportunitycompetitorMapping.toDomain(#opportunitycompetitordtos)),'iBizBusinessCentral-OpportunityCompetitor-Update')")
    @ApiOperation(value = "根据竞争对手批量更新商机对手", tags = {"商机对手" },  notes = "根据竞争对手批量更新商机对手")
	@RequestMapping(method = RequestMethod.PUT, value = "/competitors/{competitor_id}/opportunitycompetitors/batch")
    public ResponseEntity<Boolean> updateBatchByCompetitor(@PathVariable("competitor_id") String competitor_id, @RequestBody List<OpportunityCompetitorDTO> opportunitycompetitordtos) {
        List<OpportunityCompetitor> domainlist=opportunitycompetitorMapping.toDomain(opportunitycompetitordtos);
        for(OpportunityCompetitor domain:domainlist){
            domain.setEntity2id(competitor_id);
        }
        opportunitycompetitorService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.opportunitycompetitorService.get(#opportunitycompetitor_id),'iBizBusinessCentral-OpportunityCompetitor-Remove')")
    @ApiOperation(value = "根据竞争对手删除商机对手", tags = {"商机对手" },  notes = "根据竞争对手删除商机对手")
	@RequestMapping(method = RequestMethod.DELETE, value = "/competitors/{competitor_id}/opportunitycompetitors/{opportunitycompetitor_id}")
    public ResponseEntity<Boolean> removeByCompetitor(@PathVariable("competitor_id") String competitor_id, @PathVariable("opportunitycompetitor_id") String opportunitycompetitor_id) {
		return ResponseEntity.status(HttpStatus.OK).body(opportunitycompetitorService.remove(opportunitycompetitor_id));
    }

    @PreAuthorize("hasPermission(this.opportunitycompetitorService.getOpportunitycompetitorByIds(#ids),'iBizBusinessCentral-OpportunityCompetitor-Remove')")
    @ApiOperation(value = "根据竞争对手批量删除商机对手", tags = {"商机对手" },  notes = "根据竞争对手批量删除商机对手")
	@RequestMapping(method = RequestMethod.DELETE, value = "/competitors/{competitor_id}/opportunitycompetitors/batch")
    public ResponseEntity<Boolean> removeBatchByCompetitor(@RequestBody List<String> ids) {
        opportunitycompetitorService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.opportunitycompetitorMapping.toDomain(returnObject.body),'iBizBusinessCentral-OpportunityCompetitor-Get')")
    @ApiOperation(value = "根据竞争对手获取商机对手", tags = {"商机对手" },  notes = "根据竞争对手获取商机对手")
	@RequestMapping(method = RequestMethod.GET, value = "/competitors/{competitor_id}/opportunitycompetitors/{opportunitycompetitor_id}")
    public ResponseEntity<OpportunityCompetitorDTO> getByCompetitor(@PathVariable("competitor_id") String competitor_id, @PathVariable("opportunitycompetitor_id") String opportunitycompetitor_id) {
        OpportunityCompetitor domain = opportunitycompetitorService.get(opportunitycompetitor_id);
        OpportunityCompetitorDTO dto = opportunitycompetitorMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据竞争对手获取商机对手草稿", tags = {"商机对手" },  notes = "根据竞争对手获取商机对手草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/competitors/{competitor_id}/opportunitycompetitors/getdraft")
    public ResponseEntity<OpportunityCompetitorDTO> getDraftByCompetitor(@PathVariable("competitor_id") String competitor_id) {
        OpportunityCompetitor domain = new OpportunityCompetitor();
        domain.setEntity2id(competitor_id);
        return ResponseEntity.status(HttpStatus.OK).body(opportunitycompetitorMapping.toDto(opportunitycompetitorService.getDraft(domain)));
    }

    @ApiOperation(value = "根据竞争对手检查商机对手", tags = {"商机对手" },  notes = "根据竞争对手检查商机对手")
	@RequestMapping(method = RequestMethod.POST, value = "/competitors/{competitor_id}/opportunitycompetitors/checkkey")
    public ResponseEntity<Boolean> checkKeyByCompetitor(@PathVariable("competitor_id") String competitor_id, @RequestBody OpportunityCompetitorDTO opportunitycompetitordto) {
        return  ResponseEntity.status(HttpStatus.OK).body(opportunitycompetitorService.checkKey(opportunitycompetitorMapping.toDomain(opportunitycompetitordto)));
    }

    @PreAuthorize("hasPermission(this.opportunitycompetitorMapping.toDomain(#opportunitycompetitordto),'iBizBusinessCentral-OpportunityCompetitor-Save')")
    @ApiOperation(value = "根据竞争对手保存商机对手", tags = {"商机对手" },  notes = "根据竞争对手保存商机对手")
	@RequestMapping(method = RequestMethod.POST, value = "/competitors/{competitor_id}/opportunitycompetitors/save")
    public ResponseEntity<Boolean> saveByCompetitor(@PathVariable("competitor_id") String competitor_id, @RequestBody OpportunityCompetitorDTO opportunitycompetitordto) {
        OpportunityCompetitor domain = opportunitycompetitorMapping.toDomain(opportunitycompetitordto);
        domain.setEntity2id(competitor_id);
        return ResponseEntity.status(HttpStatus.OK).body(opportunitycompetitorService.save(domain));
    }

    @PreAuthorize("hasPermission(this.opportunitycompetitorMapping.toDomain(#opportunitycompetitordtos),'iBizBusinessCentral-OpportunityCompetitor-Save')")
    @ApiOperation(value = "根据竞争对手批量保存商机对手", tags = {"商机对手" },  notes = "根据竞争对手批量保存商机对手")
	@RequestMapping(method = RequestMethod.POST, value = "/competitors/{competitor_id}/opportunitycompetitors/savebatch")
    public ResponseEntity<Boolean> saveBatchByCompetitor(@PathVariable("competitor_id") String competitor_id, @RequestBody List<OpportunityCompetitorDTO> opportunitycompetitordtos) {
        List<OpportunityCompetitor> domainlist=opportunitycompetitorMapping.toDomain(opportunitycompetitordtos);
        for(OpportunityCompetitor domain:domainlist){
             domain.setEntity2id(competitor_id);
        }
        opportunitycompetitorService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OpportunityCompetitor-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-OpportunityCompetitor-Get')")
	@ApiOperation(value = "根据竞争对手获取DEFAULT", tags = {"商机对手" } ,notes = "根据竞争对手获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/competitors/{competitor_id}/opportunitycompetitors/fetchdefault")
	public ResponseEntity<List<OpportunityCompetitorDTO>> fetchOpportunityCompetitorDefaultByCompetitor(@PathVariable("competitor_id") String competitor_id,OpportunityCompetitorSearchContext context) {
        context.setN_entity2id_eq(competitor_id);
        Page<OpportunityCompetitor> domains = opportunitycompetitorService.searchDefault(context) ;
        List<OpportunityCompetitorDTO> list = opportunitycompetitorMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OpportunityCompetitor-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-OpportunityCompetitor-Get')")
	@ApiOperation(value = "根据竞争对手查询DEFAULT", tags = {"商机对手" } ,notes = "根据竞争对手查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/competitors/{competitor_id}/opportunitycompetitors/searchdefault")
	public ResponseEntity<Page<OpportunityCompetitorDTO>> searchOpportunityCompetitorDefaultByCompetitor(@PathVariable("competitor_id") String competitor_id, @RequestBody OpportunityCompetitorSearchContext context) {
        context.setN_entity2id_eq(competitor_id);
        Page<OpportunityCompetitor> domains = opportunitycompetitorService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(opportunitycompetitorMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.opportunitycompetitorMapping.toDomain(#opportunitycompetitordto),'iBizBusinessCentral-OpportunityCompetitor-Create')")
    @ApiOperation(value = "根据商机建立商机对手", tags = {"商机对手" },  notes = "根据商机建立商机对手")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/opportunitycompetitors")
    public ResponseEntity<OpportunityCompetitorDTO> createByOpportunity(@PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityCompetitorDTO opportunitycompetitordto) {
        OpportunityCompetitor domain = opportunitycompetitorMapping.toDomain(opportunitycompetitordto);
        domain.setEntityid(opportunity_id);
		opportunitycompetitorService.create(domain);
        OpportunityCompetitorDTO dto = opportunitycompetitorMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.opportunitycompetitorMapping.toDomain(#opportunitycompetitordtos),'iBizBusinessCentral-OpportunityCompetitor-Create')")
    @ApiOperation(value = "根据商机批量建立商机对手", tags = {"商机对手" },  notes = "根据商机批量建立商机对手")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/opportunitycompetitors/batch")
    public ResponseEntity<Boolean> createBatchByOpportunity(@PathVariable("opportunity_id") String opportunity_id, @RequestBody List<OpportunityCompetitorDTO> opportunitycompetitordtos) {
        List<OpportunityCompetitor> domainlist=opportunitycompetitorMapping.toDomain(opportunitycompetitordtos);
        for(OpportunityCompetitor domain:domainlist){
            domain.setEntityid(opportunity_id);
        }
        opportunitycompetitorService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "opportunitycompetitor" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.opportunitycompetitorService.get(#opportunitycompetitor_id),'iBizBusinessCentral-OpportunityCompetitor-Update')")
    @ApiOperation(value = "根据商机更新商机对手", tags = {"商机对手" },  notes = "根据商机更新商机对手")
	@RequestMapping(method = RequestMethod.PUT, value = "/opportunities/{opportunity_id}/opportunitycompetitors/{opportunitycompetitor_id}")
    public ResponseEntity<OpportunityCompetitorDTO> updateByOpportunity(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("opportunitycompetitor_id") String opportunitycompetitor_id, @RequestBody OpportunityCompetitorDTO opportunitycompetitordto) {
        OpportunityCompetitor domain = opportunitycompetitorMapping.toDomain(opportunitycompetitordto);
        domain.setEntityid(opportunity_id);
        domain.setRelationshipsid(opportunitycompetitor_id);
		opportunitycompetitorService.update(domain);
        OpportunityCompetitorDTO dto = opportunitycompetitorMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.opportunitycompetitorService.getOpportunitycompetitorByEntities(this.opportunitycompetitorMapping.toDomain(#opportunitycompetitordtos)),'iBizBusinessCentral-OpportunityCompetitor-Update')")
    @ApiOperation(value = "根据商机批量更新商机对手", tags = {"商机对手" },  notes = "根据商机批量更新商机对手")
	@RequestMapping(method = RequestMethod.PUT, value = "/opportunities/{opportunity_id}/opportunitycompetitors/batch")
    public ResponseEntity<Boolean> updateBatchByOpportunity(@PathVariable("opportunity_id") String opportunity_id, @RequestBody List<OpportunityCompetitorDTO> opportunitycompetitordtos) {
        List<OpportunityCompetitor> domainlist=opportunitycompetitorMapping.toDomain(opportunitycompetitordtos);
        for(OpportunityCompetitor domain:domainlist){
            domain.setEntityid(opportunity_id);
        }
        opportunitycompetitorService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.opportunitycompetitorService.get(#opportunitycompetitor_id),'iBizBusinessCentral-OpportunityCompetitor-Remove')")
    @ApiOperation(value = "根据商机删除商机对手", tags = {"商机对手" },  notes = "根据商机删除商机对手")
	@RequestMapping(method = RequestMethod.DELETE, value = "/opportunities/{opportunity_id}/opportunitycompetitors/{opportunitycompetitor_id}")
    public ResponseEntity<Boolean> removeByOpportunity(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("opportunitycompetitor_id") String opportunitycompetitor_id) {
		return ResponseEntity.status(HttpStatus.OK).body(opportunitycompetitorService.remove(opportunitycompetitor_id));
    }

    @PreAuthorize("hasPermission(this.opportunitycompetitorService.getOpportunitycompetitorByIds(#ids),'iBizBusinessCentral-OpportunityCompetitor-Remove')")
    @ApiOperation(value = "根据商机批量删除商机对手", tags = {"商机对手" },  notes = "根据商机批量删除商机对手")
	@RequestMapping(method = RequestMethod.DELETE, value = "/opportunities/{opportunity_id}/opportunitycompetitors/batch")
    public ResponseEntity<Boolean> removeBatchByOpportunity(@RequestBody List<String> ids) {
        opportunitycompetitorService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.opportunitycompetitorMapping.toDomain(returnObject.body),'iBizBusinessCentral-OpportunityCompetitor-Get')")
    @ApiOperation(value = "根据商机获取商机对手", tags = {"商机对手" },  notes = "根据商机获取商机对手")
	@RequestMapping(method = RequestMethod.GET, value = "/opportunities/{opportunity_id}/opportunitycompetitors/{opportunitycompetitor_id}")
    public ResponseEntity<OpportunityCompetitorDTO> getByOpportunity(@PathVariable("opportunity_id") String opportunity_id, @PathVariable("opportunitycompetitor_id") String opportunitycompetitor_id) {
        OpportunityCompetitor domain = opportunitycompetitorService.get(opportunitycompetitor_id);
        OpportunityCompetitorDTO dto = opportunitycompetitorMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据商机获取商机对手草稿", tags = {"商机对手" },  notes = "根据商机获取商机对手草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/opportunities/{opportunity_id}/opportunitycompetitors/getdraft")
    public ResponseEntity<OpportunityCompetitorDTO> getDraftByOpportunity(@PathVariable("opportunity_id") String opportunity_id) {
        OpportunityCompetitor domain = new OpportunityCompetitor();
        domain.setEntityid(opportunity_id);
        return ResponseEntity.status(HttpStatus.OK).body(opportunitycompetitorMapping.toDto(opportunitycompetitorService.getDraft(domain)));
    }

    @ApiOperation(value = "根据商机检查商机对手", tags = {"商机对手" },  notes = "根据商机检查商机对手")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/opportunitycompetitors/checkkey")
    public ResponseEntity<Boolean> checkKeyByOpportunity(@PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityCompetitorDTO opportunitycompetitordto) {
        return  ResponseEntity.status(HttpStatus.OK).body(opportunitycompetitorService.checkKey(opportunitycompetitorMapping.toDomain(opportunitycompetitordto)));
    }

    @PreAuthorize("hasPermission(this.opportunitycompetitorMapping.toDomain(#opportunitycompetitordto),'iBizBusinessCentral-OpportunityCompetitor-Save')")
    @ApiOperation(value = "根据商机保存商机对手", tags = {"商机对手" },  notes = "根据商机保存商机对手")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/opportunitycompetitors/save")
    public ResponseEntity<Boolean> saveByOpportunity(@PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityCompetitorDTO opportunitycompetitordto) {
        OpportunityCompetitor domain = opportunitycompetitorMapping.toDomain(opportunitycompetitordto);
        domain.setEntityid(opportunity_id);
        return ResponseEntity.status(HttpStatus.OK).body(opportunitycompetitorService.save(domain));
    }

    @PreAuthorize("hasPermission(this.opportunitycompetitorMapping.toDomain(#opportunitycompetitordtos),'iBizBusinessCentral-OpportunityCompetitor-Save')")
    @ApiOperation(value = "根据商机批量保存商机对手", tags = {"商机对手" },  notes = "根据商机批量保存商机对手")
	@RequestMapping(method = RequestMethod.POST, value = "/opportunities/{opportunity_id}/opportunitycompetitors/savebatch")
    public ResponseEntity<Boolean> saveBatchByOpportunity(@PathVariable("opportunity_id") String opportunity_id, @RequestBody List<OpportunityCompetitorDTO> opportunitycompetitordtos) {
        List<OpportunityCompetitor> domainlist=opportunitycompetitorMapping.toDomain(opportunitycompetitordtos);
        for(OpportunityCompetitor domain:domainlist){
             domain.setEntityid(opportunity_id);
        }
        opportunitycompetitorService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OpportunityCompetitor-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-OpportunityCompetitor-Get')")
	@ApiOperation(value = "根据商机获取DEFAULT", tags = {"商机对手" } ,notes = "根据商机获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/opportunities/{opportunity_id}/opportunitycompetitors/fetchdefault")
	public ResponseEntity<List<OpportunityCompetitorDTO>> fetchOpportunityCompetitorDefaultByOpportunity(@PathVariable("opportunity_id") String opportunity_id,OpportunityCompetitorSearchContext context) {
        context.setN_entityid_eq(opportunity_id);
        Page<OpportunityCompetitor> domains = opportunitycompetitorService.searchDefault(context) ;
        List<OpportunityCompetitorDTO> list = opportunitycompetitorMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OpportunityCompetitor-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-OpportunityCompetitor-Get')")
	@ApiOperation(value = "根据商机查询DEFAULT", tags = {"商机对手" } ,notes = "根据商机查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/opportunities/{opportunity_id}/opportunitycompetitors/searchdefault")
	public ResponseEntity<Page<OpportunityCompetitorDTO>> searchOpportunityCompetitorDefaultByOpportunity(@PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityCompetitorSearchContext context) {
        context.setN_entityid_eq(opportunity_id);
        Page<OpportunityCompetitor> domains = opportunitycompetitorService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(opportunitycompetitorMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.opportunitycompetitorMapping.toDomain(#opportunitycompetitordto),'iBizBusinessCentral-OpportunityCompetitor-Create')")
    @ApiOperation(value = "根据客户商机建立商机对手", tags = {"商机对手" },  notes = "根据客户商机建立商机对手")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/opportunitycompetitors")
    public ResponseEntity<OpportunityCompetitorDTO> createByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityCompetitorDTO opportunitycompetitordto) {
        OpportunityCompetitor domain = opportunitycompetitorMapping.toDomain(opportunitycompetitordto);
        domain.setEntityid(opportunity_id);
		opportunitycompetitorService.create(domain);
        OpportunityCompetitorDTO dto = opportunitycompetitorMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.opportunitycompetitorMapping.toDomain(#opportunitycompetitordtos),'iBizBusinessCentral-OpportunityCompetitor-Create')")
    @ApiOperation(value = "根据客户商机批量建立商机对手", tags = {"商机对手" },  notes = "根据客户商机批量建立商机对手")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/opportunitycompetitors/batch")
    public ResponseEntity<Boolean> createBatchByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody List<OpportunityCompetitorDTO> opportunitycompetitordtos) {
        List<OpportunityCompetitor> domainlist=opportunitycompetitorMapping.toDomain(opportunitycompetitordtos);
        for(OpportunityCompetitor domain:domainlist){
            domain.setEntityid(opportunity_id);
        }
        opportunitycompetitorService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "opportunitycompetitor" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.opportunitycompetitorService.get(#opportunitycompetitor_id),'iBizBusinessCentral-OpportunityCompetitor-Update')")
    @ApiOperation(value = "根据客户商机更新商机对手", tags = {"商机对手" },  notes = "根据客户商机更新商机对手")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/opportunities/{opportunity_id}/opportunitycompetitors/{opportunitycompetitor_id}")
    public ResponseEntity<OpportunityCompetitorDTO> updateByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("opportunitycompetitor_id") String opportunitycompetitor_id, @RequestBody OpportunityCompetitorDTO opportunitycompetitordto) {
        OpportunityCompetitor domain = opportunitycompetitorMapping.toDomain(opportunitycompetitordto);
        domain.setEntityid(opportunity_id);
        domain.setRelationshipsid(opportunitycompetitor_id);
		opportunitycompetitorService.update(domain);
        OpportunityCompetitorDTO dto = opportunitycompetitorMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.opportunitycompetitorService.getOpportunitycompetitorByEntities(this.opportunitycompetitorMapping.toDomain(#opportunitycompetitordtos)),'iBizBusinessCentral-OpportunityCompetitor-Update')")
    @ApiOperation(value = "根据客户商机批量更新商机对手", tags = {"商机对手" },  notes = "根据客户商机批量更新商机对手")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/opportunities/{opportunity_id}/opportunitycompetitors/batch")
    public ResponseEntity<Boolean> updateBatchByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody List<OpportunityCompetitorDTO> opportunitycompetitordtos) {
        List<OpportunityCompetitor> domainlist=opportunitycompetitorMapping.toDomain(opportunitycompetitordtos);
        for(OpportunityCompetitor domain:domainlist){
            domain.setEntityid(opportunity_id);
        }
        opportunitycompetitorService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.opportunitycompetitorService.get(#opportunitycompetitor_id),'iBizBusinessCentral-OpportunityCompetitor-Remove')")
    @ApiOperation(value = "根据客户商机删除商机对手", tags = {"商机对手" },  notes = "根据客户商机删除商机对手")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/opportunities/{opportunity_id}/opportunitycompetitors/{opportunitycompetitor_id}")
    public ResponseEntity<Boolean> removeByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("opportunitycompetitor_id") String opportunitycompetitor_id) {
		return ResponseEntity.status(HttpStatus.OK).body(opportunitycompetitorService.remove(opportunitycompetitor_id));
    }

    @PreAuthorize("hasPermission(this.opportunitycompetitorService.getOpportunitycompetitorByIds(#ids),'iBizBusinessCentral-OpportunityCompetitor-Remove')")
    @ApiOperation(value = "根据客户商机批量删除商机对手", tags = {"商机对手" },  notes = "根据客户商机批量删除商机对手")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/opportunities/{opportunity_id}/opportunitycompetitors/batch")
    public ResponseEntity<Boolean> removeBatchByAccountOpportunity(@RequestBody List<String> ids) {
        opportunitycompetitorService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.opportunitycompetitorMapping.toDomain(returnObject.body),'iBizBusinessCentral-OpportunityCompetitor-Get')")
    @ApiOperation(value = "根据客户商机获取商机对手", tags = {"商机对手" },  notes = "根据客户商机获取商机对手")
	@RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/opportunities/{opportunity_id}/opportunitycompetitors/{opportunitycompetitor_id}")
    public ResponseEntity<OpportunityCompetitorDTO> getByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("opportunitycompetitor_id") String opportunitycompetitor_id) {
        OpportunityCompetitor domain = opportunitycompetitorService.get(opportunitycompetitor_id);
        OpportunityCompetitorDTO dto = opportunitycompetitorMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据客户商机获取商机对手草稿", tags = {"商机对手" },  notes = "根据客户商机获取商机对手草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/opportunities/{opportunity_id}/opportunitycompetitors/getdraft")
    public ResponseEntity<OpportunityCompetitorDTO> getDraftByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id) {
        OpportunityCompetitor domain = new OpportunityCompetitor();
        domain.setEntityid(opportunity_id);
        return ResponseEntity.status(HttpStatus.OK).body(opportunitycompetitorMapping.toDto(opportunitycompetitorService.getDraft(domain)));
    }

    @ApiOperation(value = "根据客户商机检查商机对手", tags = {"商机对手" },  notes = "根据客户商机检查商机对手")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/opportunitycompetitors/checkkey")
    public ResponseEntity<Boolean> checkKeyByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityCompetitorDTO opportunitycompetitordto) {
        return  ResponseEntity.status(HttpStatus.OK).body(opportunitycompetitorService.checkKey(opportunitycompetitorMapping.toDomain(opportunitycompetitordto)));
    }

    @PreAuthorize("hasPermission(this.opportunitycompetitorMapping.toDomain(#opportunitycompetitordto),'iBizBusinessCentral-OpportunityCompetitor-Save')")
    @ApiOperation(value = "根据客户商机保存商机对手", tags = {"商机对手" },  notes = "根据客户商机保存商机对手")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/opportunitycompetitors/save")
    public ResponseEntity<Boolean> saveByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityCompetitorDTO opportunitycompetitordto) {
        OpportunityCompetitor domain = opportunitycompetitorMapping.toDomain(opportunitycompetitordto);
        domain.setEntityid(opportunity_id);
        return ResponseEntity.status(HttpStatus.OK).body(opportunitycompetitorService.save(domain));
    }

    @PreAuthorize("hasPermission(this.opportunitycompetitorMapping.toDomain(#opportunitycompetitordtos),'iBizBusinessCentral-OpportunityCompetitor-Save')")
    @ApiOperation(value = "根据客户商机批量保存商机对手", tags = {"商机对手" },  notes = "根据客户商机批量保存商机对手")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/opportunities/{opportunity_id}/opportunitycompetitors/savebatch")
    public ResponseEntity<Boolean> saveBatchByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody List<OpportunityCompetitorDTO> opportunitycompetitordtos) {
        List<OpportunityCompetitor> domainlist=opportunitycompetitorMapping.toDomain(opportunitycompetitordtos);
        for(OpportunityCompetitor domain:domainlist){
             domain.setEntityid(opportunity_id);
        }
        opportunitycompetitorService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OpportunityCompetitor-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-OpportunityCompetitor-Get')")
	@ApiOperation(value = "根据客户商机获取DEFAULT", tags = {"商机对手" } ,notes = "根据客户商机获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/opportunities/{opportunity_id}/opportunitycompetitors/fetchdefault")
	public ResponseEntity<List<OpportunityCompetitorDTO>> fetchOpportunityCompetitorDefaultByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id,OpportunityCompetitorSearchContext context) {
        context.setN_entityid_eq(opportunity_id);
        Page<OpportunityCompetitor> domains = opportunitycompetitorService.searchDefault(context) ;
        List<OpportunityCompetitorDTO> list = opportunitycompetitorMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OpportunityCompetitor-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-OpportunityCompetitor-Get')")
	@ApiOperation(value = "根据客户商机查询DEFAULT", tags = {"商机对手" } ,notes = "根据客户商机查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/opportunities/{opportunity_id}/opportunitycompetitors/searchdefault")
	public ResponseEntity<Page<OpportunityCompetitorDTO>> searchOpportunityCompetitorDefaultByAccountOpportunity(@PathVariable("account_id") String account_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityCompetitorSearchContext context) {
        context.setN_entityid_eq(opportunity_id);
        Page<OpportunityCompetitor> domains = opportunitycompetitorService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(opportunitycompetitorMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.opportunitycompetitorMapping.toDomain(#opportunitycompetitordto),'iBizBusinessCentral-OpportunityCompetitor-Create')")
    @ApiOperation(value = "根据联系人商机建立商机对手", tags = {"商机对手" },  notes = "根据联系人商机建立商机对手")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/opportunitycompetitors")
    public ResponseEntity<OpportunityCompetitorDTO> createByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityCompetitorDTO opportunitycompetitordto) {
        OpportunityCompetitor domain = opportunitycompetitorMapping.toDomain(opportunitycompetitordto);
        domain.setEntityid(opportunity_id);
		opportunitycompetitorService.create(domain);
        OpportunityCompetitorDTO dto = opportunitycompetitorMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.opportunitycompetitorMapping.toDomain(#opportunitycompetitordtos),'iBizBusinessCentral-OpportunityCompetitor-Create')")
    @ApiOperation(value = "根据联系人商机批量建立商机对手", tags = {"商机对手" },  notes = "根据联系人商机批量建立商机对手")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/opportunitycompetitors/batch")
    public ResponseEntity<Boolean> createBatchByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody List<OpportunityCompetitorDTO> opportunitycompetitordtos) {
        List<OpportunityCompetitor> domainlist=opportunitycompetitorMapping.toDomain(opportunitycompetitordtos);
        for(OpportunityCompetitor domain:domainlist){
            domain.setEntityid(opportunity_id);
        }
        opportunitycompetitorService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "opportunitycompetitor" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.opportunitycompetitorService.get(#opportunitycompetitor_id),'iBizBusinessCentral-OpportunityCompetitor-Update')")
    @ApiOperation(value = "根据联系人商机更新商机对手", tags = {"商机对手" },  notes = "根据联系人商机更新商机对手")
	@RequestMapping(method = RequestMethod.PUT, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/opportunitycompetitors/{opportunitycompetitor_id}")
    public ResponseEntity<OpportunityCompetitorDTO> updateByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("opportunitycompetitor_id") String opportunitycompetitor_id, @RequestBody OpportunityCompetitorDTO opportunitycompetitordto) {
        OpportunityCompetitor domain = opportunitycompetitorMapping.toDomain(opportunitycompetitordto);
        domain.setEntityid(opportunity_id);
        domain.setRelationshipsid(opportunitycompetitor_id);
		opportunitycompetitorService.update(domain);
        OpportunityCompetitorDTO dto = opportunitycompetitorMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.opportunitycompetitorService.getOpportunitycompetitorByEntities(this.opportunitycompetitorMapping.toDomain(#opportunitycompetitordtos)),'iBizBusinessCentral-OpportunityCompetitor-Update')")
    @ApiOperation(value = "根据联系人商机批量更新商机对手", tags = {"商机对手" },  notes = "根据联系人商机批量更新商机对手")
	@RequestMapping(method = RequestMethod.PUT, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/opportunitycompetitors/batch")
    public ResponseEntity<Boolean> updateBatchByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody List<OpportunityCompetitorDTO> opportunitycompetitordtos) {
        List<OpportunityCompetitor> domainlist=opportunitycompetitorMapping.toDomain(opportunitycompetitordtos);
        for(OpportunityCompetitor domain:domainlist){
            domain.setEntityid(opportunity_id);
        }
        opportunitycompetitorService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.opportunitycompetitorService.get(#opportunitycompetitor_id),'iBizBusinessCentral-OpportunityCompetitor-Remove')")
    @ApiOperation(value = "根据联系人商机删除商机对手", tags = {"商机对手" },  notes = "根据联系人商机删除商机对手")
	@RequestMapping(method = RequestMethod.DELETE, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/opportunitycompetitors/{opportunitycompetitor_id}")
    public ResponseEntity<Boolean> removeByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("opportunitycompetitor_id") String opportunitycompetitor_id) {
		return ResponseEntity.status(HttpStatus.OK).body(opportunitycompetitorService.remove(opportunitycompetitor_id));
    }

    @PreAuthorize("hasPermission(this.opportunitycompetitorService.getOpportunitycompetitorByIds(#ids),'iBizBusinessCentral-OpportunityCompetitor-Remove')")
    @ApiOperation(value = "根据联系人商机批量删除商机对手", tags = {"商机对手" },  notes = "根据联系人商机批量删除商机对手")
	@RequestMapping(method = RequestMethod.DELETE, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/opportunitycompetitors/batch")
    public ResponseEntity<Boolean> removeBatchByContactOpportunity(@RequestBody List<String> ids) {
        opportunitycompetitorService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.opportunitycompetitorMapping.toDomain(returnObject.body),'iBizBusinessCentral-OpportunityCompetitor-Get')")
    @ApiOperation(value = "根据联系人商机获取商机对手", tags = {"商机对手" },  notes = "根据联系人商机获取商机对手")
	@RequestMapping(method = RequestMethod.GET, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/opportunitycompetitors/{opportunitycompetitor_id}")
    public ResponseEntity<OpportunityCompetitorDTO> getByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("opportunitycompetitor_id") String opportunitycompetitor_id) {
        OpportunityCompetitor domain = opportunitycompetitorService.get(opportunitycompetitor_id);
        OpportunityCompetitorDTO dto = opportunitycompetitorMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据联系人商机获取商机对手草稿", tags = {"商机对手" },  notes = "根据联系人商机获取商机对手草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/opportunitycompetitors/getdraft")
    public ResponseEntity<OpportunityCompetitorDTO> getDraftByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id) {
        OpportunityCompetitor domain = new OpportunityCompetitor();
        domain.setEntityid(opportunity_id);
        return ResponseEntity.status(HttpStatus.OK).body(opportunitycompetitorMapping.toDto(opportunitycompetitorService.getDraft(domain)));
    }

    @ApiOperation(value = "根据联系人商机检查商机对手", tags = {"商机对手" },  notes = "根据联系人商机检查商机对手")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/opportunitycompetitors/checkkey")
    public ResponseEntity<Boolean> checkKeyByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityCompetitorDTO opportunitycompetitordto) {
        return  ResponseEntity.status(HttpStatus.OK).body(opportunitycompetitorService.checkKey(opportunitycompetitorMapping.toDomain(opportunitycompetitordto)));
    }

    @PreAuthorize("hasPermission(this.opportunitycompetitorMapping.toDomain(#opportunitycompetitordto),'iBizBusinessCentral-OpportunityCompetitor-Save')")
    @ApiOperation(value = "根据联系人商机保存商机对手", tags = {"商机对手" },  notes = "根据联系人商机保存商机对手")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/opportunitycompetitors/save")
    public ResponseEntity<Boolean> saveByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityCompetitorDTO opportunitycompetitordto) {
        OpportunityCompetitor domain = opportunitycompetitorMapping.toDomain(opportunitycompetitordto);
        domain.setEntityid(opportunity_id);
        return ResponseEntity.status(HttpStatus.OK).body(opportunitycompetitorService.save(domain));
    }

    @PreAuthorize("hasPermission(this.opportunitycompetitorMapping.toDomain(#opportunitycompetitordtos),'iBizBusinessCentral-OpportunityCompetitor-Save')")
    @ApiOperation(value = "根据联系人商机批量保存商机对手", tags = {"商机对手" },  notes = "根据联系人商机批量保存商机对手")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/opportunities/{opportunity_id}/opportunitycompetitors/savebatch")
    public ResponseEntity<Boolean> saveBatchByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody List<OpportunityCompetitorDTO> opportunitycompetitordtos) {
        List<OpportunityCompetitor> domainlist=opportunitycompetitorMapping.toDomain(opportunitycompetitordtos);
        for(OpportunityCompetitor domain:domainlist){
             domain.setEntityid(opportunity_id);
        }
        opportunitycompetitorService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OpportunityCompetitor-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-OpportunityCompetitor-Get')")
	@ApiOperation(value = "根据联系人商机获取DEFAULT", tags = {"商机对手" } ,notes = "根据联系人商机获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/contacts/{contact_id}/opportunities/{opportunity_id}/opportunitycompetitors/fetchdefault")
	public ResponseEntity<List<OpportunityCompetitorDTO>> fetchOpportunityCompetitorDefaultByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id,OpportunityCompetitorSearchContext context) {
        context.setN_entityid_eq(opportunity_id);
        Page<OpportunityCompetitor> domains = opportunitycompetitorService.searchDefault(context) ;
        List<OpportunityCompetitorDTO> list = opportunitycompetitorMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OpportunityCompetitor-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-OpportunityCompetitor-Get')")
	@ApiOperation(value = "根据联系人商机查询DEFAULT", tags = {"商机对手" } ,notes = "根据联系人商机查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/contacts/{contact_id}/opportunities/{opportunity_id}/opportunitycompetitors/searchdefault")
	public ResponseEntity<Page<OpportunityCompetitorDTO>> searchOpportunityCompetitorDefaultByContactOpportunity(@PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityCompetitorSearchContext context) {
        context.setN_entityid_eq(opportunity_id);
        Page<OpportunityCompetitor> domains = opportunitycompetitorService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(opportunitycompetitorMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.opportunitycompetitorMapping.toDomain(#opportunitycompetitordto),'iBizBusinessCentral-OpportunityCompetitor-Create')")
    @ApiOperation(value = "根据客户联系人商机建立商机对手", tags = {"商机对手" },  notes = "根据客户联系人商机建立商机对手")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/opportunitycompetitors")
    public ResponseEntity<OpportunityCompetitorDTO> createByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityCompetitorDTO opportunitycompetitordto) {
        OpportunityCompetitor domain = opportunitycompetitorMapping.toDomain(opportunitycompetitordto);
        domain.setEntityid(opportunity_id);
		opportunitycompetitorService.create(domain);
        OpportunityCompetitorDTO dto = opportunitycompetitorMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.opportunitycompetitorMapping.toDomain(#opportunitycompetitordtos),'iBizBusinessCentral-OpportunityCompetitor-Create')")
    @ApiOperation(value = "根据客户联系人商机批量建立商机对手", tags = {"商机对手" },  notes = "根据客户联系人商机批量建立商机对手")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/opportunitycompetitors/batch")
    public ResponseEntity<Boolean> createBatchByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody List<OpportunityCompetitorDTO> opportunitycompetitordtos) {
        List<OpportunityCompetitor> domainlist=opportunitycompetitorMapping.toDomain(opportunitycompetitordtos);
        for(OpportunityCompetitor domain:domainlist){
            domain.setEntityid(opportunity_id);
        }
        opportunitycompetitorService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "opportunitycompetitor" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.opportunitycompetitorService.get(#opportunitycompetitor_id),'iBizBusinessCentral-OpportunityCompetitor-Update')")
    @ApiOperation(value = "根据客户联系人商机更新商机对手", tags = {"商机对手" },  notes = "根据客户联系人商机更新商机对手")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/opportunitycompetitors/{opportunitycompetitor_id}")
    public ResponseEntity<OpportunityCompetitorDTO> updateByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("opportunitycompetitor_id") String opportunitycompetitor_id, @RequestBody OpportunityCompetitorDTO opportunitycompetitordto) {
        OpportunityCompetitor domain = opportunitycompetitorMapping.toDomain(opportunitycompetitordto);
        domain.setEntityid(opportunity_id);
        domain.setRelationshipsid(opportunitycompetitor_id);
		opportunitycompetitorService.update(domain);
        OpportunityCompetitorDTO dto = opportunitycompetitorMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.opportunitycompetitorService.getOpportunitycompetitorByEntities(this.opportunitycompetitorMapping.toDomain(#opportunitycompetitordtos)),'iBizBusinessCentral-OpportunityCompetitor-Update')")
    @ApiOperation(value = "根据客户联系人商机批量更新商机对手", tags = {"商机对手" },  notes = "根据客户联系人商机批量更新商机对手")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/opportunitycompetitors/batch")
    public ResponseEntity<Boolean> updateBatchByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody List<OpportunityCompetitorDTO> opportunitycompetitordtos) {
        List<OpportunityCompetitor> domainlist=opportunitycompetitorMapping.toDomain(opportunitycompetitordtos);
        for(OpportunityCompetitor domain:domainlist){
            domain.setEntityid(opportunity_id);
        }
        opportunitycompetitorService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.opportunitycompetitorService.get(#opportunitycompetitor_id),'iBizBusinessCentral-OpportunityCompetitor-Remove')")
    @ApiOperation(value = "根据客户联系人商机删除商机对手", tags = {"商机对手" },  notes = "根据客户联系人商机删除商机对手")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/opportunitycompetitors/{opportunitycompetitor_id}")
    public ResponseEntity<Boolean> removeByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("opportunitycompetitor_id") String opportunitycompetitor_id) {
		return ResponseEntity.status(HttpStatus.OK).body(opportunitycompetitorService.remove(opportunitycompetitor_id));
    }

    @PreAuthorize("hasPermission(this.opportunitycompetitorService.getOpportunitycompetitorByIds(#ids),'iBizBusinessCentral-OpportunityCompetitor-Remove')")
    @ApiOperation(value = "根据客户联系人商机批量删除商机对手", tags = {"商机对手" },  notes = "根据客户联系人商机批量删除商机对手")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/opportunitycompetitors/batch")
    public ResponseEntity<Boolean> removeBatchByAccountContactOpportunity(@RequestBody List<String> ids) {
        opportunitycompetitorService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.opportunitycompetitorMapping.toDomain(returnObject.body),'iBizBusinessCentral-OpportunityCompetitor-Get')")
    @ApiOperation(value = "根据客户联系人商机获取商机对手", tags = {"商机对手" },  notes = "根据客户联系人商机获取商机对手")
	@RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/opportunitycompetitors/{opportunitycompetitor_id}")
    public ResponseEntity<OpportunityCompetitorDTO> getByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @PathVariable("opportunitycompetitor_id") String opportunitycompetitor_id) {
        OpportunityCompetitor domain = opportunitycompetitorService.get(opportunitycompetitor_id);
        OpportunityCompetitorDTO dto = opportunitycompetitorMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据客户联系人商机获取商机对手草稿", tags = {"商机对手" },  notes = "根据客户联系人商机获取商机对手草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/opportunitycompetitors/getdraft")
    public ResponseEntity<OpportunityCompetitorDTO> getDraftByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id) {
        OpportunityCompetitor domain = new OpportunityCompetitor();
        domain.setEntityid(opportunity_id);
        return ResponseEntity.status(HttpStatus.OK).body(opportunitycompetitorMapping.toDto(opportunitycompetitorService.getDraft(domain)));
    }

    @ApiOperation(value = "根据客户联系人商机检查商机对手", tags = {"商机对手" },  notes = "根据客户联系人商机检查商机对手")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/opportunitycompetitors/checkkey")
    public ResponseEntity<Boolean> checkKeyByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityCompetitorDTO opportunitycompetitordto) {
        return  ResponseEntity.status(HttpStatus.OK).body(opportunitycompetitorService.checkKey(opportunitycompetitorMapping.toDomain(opportunitycompetitordto)));
    }

    @PreAuthorize("hasPermission(this.opportunitycompetitorMapping.toDomain(#opportunitycompetitordto),'iBizBusinessCentral-OpportunityCompetitor-Save')")
    @ApiOperation(value = "根据客户联系人商机保存商机对手", tags = {"商机对手" },  notes = "根据客户联系人商机保存商机对手")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/opportunitycompetitors/save")
    public ResponseEntity<Boolean> saveByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityCompetitorDTO opportunitycompetitordto) {
        OpportunityCompetitor domain = opportunitycompetitorMapping.toDomain(opportunitycompetitordto);
        domain.setEntityid(opportunity_id);
        return ResponseEntity.status(HttpStatus.OK).body(opportunitycompetitorService.save(domain));
    }

    @PreAuthorize("hasPermission(this.opportunitycompetitorMapping.toDomain(#opportunitycompetitordtos),'iBizBusinessCentral-OpportunityCompetitor-Save')")
    @ApiOperation(value = "根据客户联系人商机批量保存商机对手", tags = {"商机对手" },  notes = "根据客户联系人商机批量保存商机对手")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/opportunitycompetitors/savebatch")
    public ResponseEntity<Boolean> saveBatchByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody List<OpportunityCompetitorDTO> opportunitycompetitordtos) {
        List<OpportunityCompetitor> domainlist=opportunitycompetitorMapping.toDomain(opportunitycompetitordtos);
        for(OpportunityCompetitor domain:domainlist){
             domain.setEntityid(opportunity_id);
        }
        opportunitycompetitorService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OpportunityCompetitor-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-OpportunityCompetitor-Get')")
	@ApiOperation(value = "根据客户联系人商机获取DEFAULT", tags = {"商机对手" } ,notes = "根据客户联系人商机获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/opportunitycompetitors/fetchdefault")
	public ResponseEntity<List<OpportunityCompetitorDTO>> fetchOpportunityCompetitorDefaultByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id,OpportunityCompetitorSearchContext context) {
        context.setN_entityid_eq(opportunity_id);
        Page<OpportunityCompetitor> domains = opportunitycompetitorService.searchDefault(context) ;
        List<OpportunityCompetitorDTO> list = opportunitycompetitorMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-OpportunityCompetitor-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-OpportunityCompetitor-Get')")
	@ApiOperation(value = "根据客户联系人商机查询DEFAULT", tags = {"商机对手" } ,notes = "根据客户联系人商机查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/contacts/{contact_id}/opportunities/{opportunity_id}/opportunitycompetitors/searchdefault")
	public ResponseEntity<Page<OpportunityCompetitorDTO>> searchOpportunityCompetitorDefaultByAccountContactOpportunity(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("opportunity_id") String opportunity_id, @RequestBody OpportunityCompetitorSearchContext context) {
        context.setN_entityid_eq(opportunity_id);
        Page<OpportunityCompetitor> domains = opportunitycompetitorService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(opportunitycompetitorMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

