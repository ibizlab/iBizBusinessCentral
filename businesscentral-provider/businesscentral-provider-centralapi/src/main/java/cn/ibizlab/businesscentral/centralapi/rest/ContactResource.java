package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.base.domain.Contact;
import cn.ibizlab.businesscentral.core.base.service.IContactService;
import cn.ibizlab.businesscentral.core.base.filter.ContactSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"联系人" })
@RestController("CentralApi-contact")
@RequestMapping("")
public class ContactResource {

    @Autowired
    public IContactService contactService;

    @Autowired
    @Lazy
    public ContactMapping contactMapping;

    @PreAuthorize("hasPermission(this.contactMapping.toDomain(#contactdto),'iBizBusinessCentral-Contact-Create')")
    @ApiOperation(value = "新建联系人", tags = {"联系人" },  notes = "新建联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts")
    public ResponseEntity<ContactDTO> create(@RequestBody ContactDTO contactdto) {
        Contact domain = contactMapping.toDomain(contactdto);
		contactService.create(domain);
        ContactDTO dto = contactMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.contactMapping.toDomain(#contactdtos),'iBizBusinessCentral-Contact-Create')")
    @ApiOperation(value = "批量新建联系人", tags = {"联系人" },  notes = "批量新建联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<ContactDTO> contactdtos) {
        contactService.createBatch(contactMapping.toDomain(contactdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "contact" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.contactService.get(#contact_id),'iBizBusinessCentral-Contact-Update')")
    @ApiOperation(value = "更新联系人", tags = {"联系人" },  notes = "更新联系人")
	@RequestMapping(method = RequestMethod.PUT, value = "/contacts/{contact_id}")
    public ResponseEntity<ContactDTO> update(@PathVariable("contact_id") String contact_id, @RequestBody ContactDTO contactdto) {
		Contact domain  = contactMapping.toDomain(contactdto);
        domain .setContactid(contact_id);
		contactService.update(domain );
		ContactDTO dto = contactMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.contactService.getContactByEntities(this.contactMapping.toDomain(#contactdtos)),'iBizBusinessCentral-Contact-Update')")
    @ApiOperation(value = "批量更新联系人", tags = {"联系人" },  notes = "批量更新联系人")
	@RequestMapping(method = RequestMethod.PUT, value = "/contacts/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<ContactDTO> contactdtos) {
        contactService.updateBatch(contactMapping.toDomain(contactdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.contactService.get(#contact_id),'iBizBusinessCentral-Contact-Remove')")
    @ApiOperation(value = "删除联系人", tags = {"联系人" },  notes = "删除联系人")
	@RequestMapping(method = RequestMethod.DELETE, value = "/contacts/{contact_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("contact_id") String contact_id) {
         return ResponseEntity.status(HttpStatus.OK).body(contactService.remove(contact_id));
    }

    @PreAuthorize("hasPermission(this.contactService.getContactByIds(#ids),'iBizBusinessCentral-Contact-Remove')")
    @ApiOperation(value = "批量删除联系人", tags = {"联系人" },  notes = "批量删除联系人")
	@RequestMapping(method = RequestMethod.DELETE, value = "/contacts/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        contactService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.contactMapping.toDomain(returnObject.body),'iBizBusinessCentral-Contact-Get')")
    @ApiOperation(value = "获取联系人", tags = {"联系人" },  notes = "获取联系人")
	@RequestMapping(method = RequestMethod.GET, value = "/contacts/{contact_id}")
    public ResponseEntity<ContactDTO> get(@PathVariable("contact_id") String contact_id) {
        Contact domain = contactService.get(contact_id);
        ContactDTO dto = contactMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取联系人草稿", tags = {"联系人" },  notes = "获取联系人草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/contacts/getdraft")
    public ResponseEntity<ContactDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(contactMapping.toDto(contactService.getDraft(new Contact())));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Contact-Active-all')")
    @ApiOperation(value = "激活", tags = {"联系人" },  notes = "激活")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/active")
    public ResponseEntity<ContactDTO> active(@PathVariable("contact_id") String contact_id, @RequestBody ContactDTO contactdto) {
        Contact domain = contactMapping.toDomain(contactdto);
domain.setContactid(contact_id);
        domain = contactService.active(domain);
        contactdto = contactMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(contactdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Contact-AddList-all')")
    @ApiOperation(value = "添加到市场营销列表", tags = {"联系人" },  notes = "添加到市场营销列表")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/addlist")
    public ResponseEntity<ContactDTO> addList(@PathVariable("contact_id") String contact_id, @RequestBody ContactDTO contactdto) {
        Contact domain = contactMapping.toDomain(contactdto);
domain.setContactid(contact_id);
        domain = contactService.addList(domain);
        contactdto = contactMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(contactdto);
    }

    @ApiOperation(value = "检查联系人", tags = {"联系人" },  notes = "检查联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody ContactDTO contactdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(contactService.checkKey(contactMapping.toDomain(contactdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Contact-Inactive-all')")
    @ApiOperation(value = "停用", tags = {"联系人" },  notes = "停用")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/inactive")
    public ResponseEntity<ContactDTO> inactive(@PathVariable("contact_id") String contact_id, @RequestBody ContactDTO contactdto) {
        Contact domain = contactMapping.toDomain(contactdto);
domain.setContactid(contact_id);
        domain = contactService.inactive(domain);
        contactdto = contactMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(contactdto);
    }

    @PreAuthorize("hasPermission(this.contactMapping.toDomain(#contactdto),'iBizBusinessCentral-Contact-Save')")
    @ApiOperation(value = "保存联系人", tags = {"联系人" },  notes = "保存联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/save")
    public ResponseEntity<Boolean> save(@RequestBody ContactDTO contactdto) {
        return ResponseEntity.status(HttpStatus.OK).body(contactService.save(contactMapping.toDomain(contactdto)));
    }

    @PreAuthorize("hasPermission(this.contactMapping.toDomain(#contactdtos),'iBizBusinessCentral-Contact-Save')")
    @ApiOperation(value = "批量保存联系人", tags = {"联系人" },  notes = "批量保存联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<ContactDTO> contactdtos) {
        contactService.saveBatch(contactMapping.toDomain(contactdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Contact-SetPrimary-all')")
    @ApiOperation(value = "设置主联系人", tags = {"联系人" },  notes = "设置主联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/setprimary")
    public ResponseEntity<ContactDTO> setPrimary(@PathVariable("contact_id") String contact_id, @RequestBody ContactDTO contactdto) {
        Contact domain = contactMapping.toDomain(contactdto);
domain.setContactid(contact_id);
        domain = contactService.setPrimary(domain);
        contactdto = contactMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(contactdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Contact-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Contact-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"联系人" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/contacts/fetchdefault")
	public ResponseEntity<List<ContactDTO>> fetchDefault(ContactSearchContext context) {
        Page<Contact> domains = contactService.searchDefault(context) ;
        List<ContactDTO> list = contactMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Contact-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Contact-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"联系人" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/contacts/searchdefault")
	public ResponseEntity<Page<ContactDTO>> searchDefault(@RequestBody ContactSearchContext context) {
        Page<Contact> domains = contactService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(contactMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Contact-searchStop-all') and hasPermission(#context,'iBizBusinessCentral-Contact-Get')")
	@ApiOperation(value = "获取停用联系人", tags = {"联系人" } ,notes = "获取停用联系人")
    @RequestMapping(method= RequestMethod.GET , value="/contacts/fetchstop")
	public ResponseEntity<List<ContactDTO>> fetchStop(ContactSearchContext context) {
        Page<Contact> domains = contactService.searchStop(context) ;
        List<ContactDTO> list = contactMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Contact-searchStop-all') and hasPermission(#context,'iBizBusinessCentral-Contact-Get')")
	@ApiOperation(value = "查询停用联系人", tags = {"联系人" } ,notes = "查询停用联系人")
    @RequestMapping(method= RequestMethod.POST , value="/contacts/searchstop")
	public ResponseEntity<Page<ContactDTO>> searchStop(@RequestBody ContactSearchContext context) {
        Page<Contact> domains = contactService.searchStop(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(contactMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Contact-searchUsable-all') and hasPermission(#context,'iBizBusinessCentral-Contact-Get')")
	@ApiOperation(value = "获取可用联系人", tags = {"联系人" } ,notes = "获取可用联系人")
    @RequestMapping(method= RequestMethod.GET , value="/contacts/fetchusable")
	public ResponseEntity<List<ContactDTO>> fetchUsable(ContactSearchContext context) {
        Page<Contact> domains = contactService.searchUsable(context) ;
        List<ContactDTO> list = contactMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Contact-searchUsable-all') and hasPermission(#context,'iBizBusinessCentral-Contact-Get')")
	@ApiOperation(value = "查询可用联系人", tags = {"联系人" } ,notes = "查询可用联系人")
    @RequestMapping(method= RequestMethod.POST , value="/contacts/searchusable")
	public ResponseEntity<Page<ContactDTO>> searchUsable(@RequestBody ContactSearchContext context) {
        Page<Contact> domains = contactService.searchUsable(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(contactMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.contactMapping.toDomain(#contactdto),'iBizBusinessCentral-Contact-Create')")
    @ApiOperation(value = "根据客户建立联系人", tags = {"联系人" },  notes = "根据客户建立联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts")
    public ResponseEntity<ContactDTO> createByAccount(@PathVariable("account_id") String account_id, @RequestBody ContactDTO contactdto) {
        Contact domain = contactMapping.toDomain(contactdto);
        domain.setCustomerid(account_id);
		contactService.create(domain);
        ContactDTO dto = contactMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.contactMapping.toDomain(#contactdtos),'iBizBusinessCentral-Contact-Create')")
    @ApiOperation(value = "根据客户批量建立联系人", tags = {"联系人" },  notes = "根据客户批量建立联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/batch")
    public ResponseEntity<Boolean> createBatchByAccount(@PathVariable("account_id") String account_id, @RequestBody List<ContactDTO> contactdtos) {
        List<Contact> domainlist=contactMapping.toDomain(contactdtos);
        for(Contact domain:domainlist){
            domain.setCustomerid(account_id);
        }
        contactService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "contact" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.contactService.get(#contact_id),'iBizBusinessCentral-Contact-Update')")
    @ApiOperation(value = "根据客户更新联系人", tags = {"联系人" },  notes = "根据客户更新联系人")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/contacts/{contact_id}")
    public ResponseEntity<ContactDTO> updateByAccount(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @RequestBody ContactDTO contactdto) {
        Contact domain = contactMapping.toDomain(contactdto);
        domain.setCustomerid(account_id);
        domain.setContactid(contact_id);
		contactService.update(domain);
        ContactDTO dto = contactMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.contactService.getContactByEntities(this.contactMapping.toDomain(#contactdtos)),'iBizBusinessCentral-Contact-Update')")
    @ApiOperation(value = "根据客户批量更新联系人", tags = {"联系人" },  notes = "根据客户批量更新联系人")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/contacts/batch")
    public ResponseEntity<Boolean> updateBatchByAccount(@PathVariable("account_id") String account_id, @RequestBody List<ContactDTO> contactdtos) {
        List<Contact> domainlist=contactMapping.toDomain(contactdtos);
        for(Contact domain:domainlist){
            domain.setCustomerid(account_id);
        }
        contactService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.contactService.get(#contact_id),'iBizBusinessCentral-Contact-Remove')")
    @ApiOperation(value = "根据客户删除联系人", tags = {"联系人" },  notes = "根据客户删除联系人")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/contacts/{contact_id}")
    public ResponseEntity<Boolean> removeByAccount(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id) {
		return ResponseEntity.status(HttpStatus.OK).body(contactService.remove(contact_id));
    }

    @PreAuthorize("hasPermission(this.contactService.getContactByIds(#ids),'iBizBusinessCentral-Contact-Remove')")
    @ApiOperation(value = "根据客户批量删除联系人", tags = {"联系人" },  notes = "根据客户批量删除联系人")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/contacts/batch")
    public ResponseEntity<Boolean> removeBatchByAccount(@RequestBody List<String> ids) {
        contactService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.contactMapping.toDomain(returnObject.body),'iBizBusinessCentral-Contact-Get')")
    @ApiOperation(value = "根据客户获取联系人", tags = {"联系人" },  notes = "根据客户获取联系人")
	@RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/contacts/{contact_id}")
    public ResponseEntity<ContactDTO> getByAccount(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id) {
        Contact domain = contactService.get(contact_id);
        ContactDTO dto = contactMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据客户获取联系人草稿", tags = {"联系人" },  notes = "根据客户获取联系人草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/contacts/getdraft")
    public ResponseEntity<ContactDTO> getDraftByAccount(@PathVariable("account_id") String account_id) {
        Contact domain = new Contact();
        domain.setCustomerid(account_id);
        return ResponseEntity.status(HttpStatus.OK).body(contactMapping.toDto(contactService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Contact-Active-all')")
    @ApiOperation(value = "根据客户联系人", tags = {"联系人" },  notes = "根据客户联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/active")
    public ResponseEntity<ContactDTO> activeByAccount(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @RequestBody ContactDTO contactdto) {
        Contact domain = contactMapping.toDomain(contactdto);
        domain.setCustomerid(account_id);
        domain = contactService.active(domain) ;
        contactdto = contactMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(contactdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Contact-AddList-all')")
    @ApiOperation(value = "根据客户联系人", tags = {"联系人" },  notes = "根据客户联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/addlist")
    public ResponseEntity<ContactDTO> addListByAccount(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @RequestBody ContactDTO contactdto) {
        Contact domain = contactMapping.toDomain(contactdto);
        domain.setCustomerid(account_id);
        domain = contactService.addList(domain) ;
        contactdto = contactMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(contactdto);
    }

    @ApiOperation(value = "根据客户检查联系人", tags = {"联系人" },  notes = "根据客户检查联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/checkkey")
    public ResponseEntity<Boolean> checkKeyByAccount(@PathVariable("account_id") String account_id, @RequestBody ContactDTO contactdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(contactService.checkKey(contactMapping.toDomain(contactdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Contact-Inactive-all')")
    @ApiOperation(value = "根据客户联系人", tags = {"联系人" },  notes = "根据客户联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/inactive")
    public ResponseEntity<ContactDTO> inactiveByAccount(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @RequestBody ContactDTO contactdto) {
        Contact domain = contactMapping.toDomain(contactdto);
        domain.setCustomerid(account_id);
        domain = contactService.inactive(domain) ;
        contactdto = contactMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(contactdto);
    }

    @PreAuthorize("hasPermission(this.contactMapping.toDomain(#contactdto),'iBizBusinessCentral-Contact-Save')")
    @ApiOperation(value = "根据客户保存联系人", tags = {"联系人" },  notes = "根据客户保存联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/save")
    public ResponseEntity<Boolean> saveByAccount(@PathVariable("account_id") String account_id, @RequestBody ContactDTO contactdto) {
        Contact domain = contactMapping.toDomain(contactdto);
        domain.setCustomerid(account_id);
        return ResponseEntity.status(HttpStatus.OK).body(contactService.save(domain));
    }

    @PreAuthorize("hasPermission(this.contactMapping.toDomain(#contactdtos),'iBizBusinessCentral-Contact-Save')")
    @ApiOperation(value = "根据客户批量保存联系人", tags = {"联系人" },  notes = "根据客户批量保存联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/savebatch")
    public ResponseEntity<Boolean> saveBatchByAccount(@PathVariable("account_id") String account_id, @RequestBody List<ContactDTO> contactdtos) {
        List<Contact> domainlist=contactMapping.toDomain(contactdtos);
        for(Contact domain:domainlist){
             domain.setCustomerid(account_id);
        }
        contactService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Contact-SetPrimary-all')")
    @ApiOperation(value = "根据客户联系人", tags = {"联系人" },  notes = "根据客户联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/setprimary")
    public ResponseEntity<ContactDTO> setPrimaryByAccount(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @RequestBody ContactDTO contactdto) {
        Contact domain = contactMapping.toDomain(contactdto);
        domain.setCustomerid(account_id);
        domain = contactService.setPrimary(domain) ;
        contactdto = contactMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(contactdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Contact-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Contact-Get')")
	@ApiOperation(value = "根据客户获取DEFAULT", tags = {"联系人" } ,notes = "根据客户获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/contacts/fetchdefault")
	public ResponseEntity<List<ContactDTO>> fetchContactDefaultByAccount(@PathVariable("account_id") String account_id,ContactSearchContext context) {
        context.setN_customerid_eq(account_id);
        Page<Contact> domains = contactService.searchDefault(context) ;
        List<ContactDTO> list = contactMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Contact-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Contact-Get')")
	@ApiOperation(value = "根据客户查询DEFAULT", tags = {"联系人" } ,notes = "根据客户查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/contacts/searchdefault")
	public ResponseEntity<Page<ContactDTO>> searchContactDefaultByAccount(@PathVariable("account_id") String account_id, @RequestBody ContactSearchContext context) {
        context.setN_customerid_eq(account_id);
        Page<Contact> domains = contactService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(contactMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Contact-searchStop-all') and hasPermission(#context,'iBizBusinessCentral-Contact-Get')")
	@ApiOperation(value = "根据客户获取停用联系人", tags = {"联系人" } ,notes = "根据客户获取停用联系人")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/contacts/fetchstop")
	public ResponseEntity<List<ContactDTO>> fetchContactStopByAccount(@PathVariable("account_id") String account_id,ContactSearchContext context) {
        context.setN_customerid_eq(account_id);
        Page<Contact> domains = contactService.searchStop(context) ;
        List<ContactDTO> list = contactMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Contact-searchStop-all') and hasPermission(#context,'iBizBusinessCentral-Contact-Get')")
	@ApiOperation(value = "根据客户查询停用联系人", tags = {"联系人" } ,notes = "根据客户查询停用联系人")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/contacts/searchstop")
	public ResponseEntity<Page<ContactDTO>> searchContactStopByAccount(@PathVariable("account_id") String account_id, @RequestBody ContactSearchContext context) {
        context.setN_customerid_eq(account_id);
        Page<Contact> domains = contactService.searchStop(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(contactMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Contact-searchUsable-all') and hasPermission(#context,'iBizBusinessCentral-Contact-Get')")
	@ApiOperation(value = "根据客户获取可用联系人", tags = {"联系人" } ,notes = "根据客户获取可用联系人")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/contacts/fetchusable")
	public ResponseEntity<List<ContactDTO>> fetchContactUsableByAccount(@PathVariable("account_id") String account_id,ContactSearchContext context) {
        context.setN_customerid_eq(account_id);
        Page<Contact> domains = contactService.searchUsable(context) ;
        List<ContactDTO> list = contactMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Contact-searchUsable-all') and hasPermission(#context,'iBizBusinessCentral-Contact-Get')")
	@ApiOperation(value = "根据客户查询可用联系人", tags = {"联系人" } ,notes = "根据客户查询可用联系人")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/contacts/searchusable")
	public ResponseEntity<Page<ContactDTO>> searchContactUsableByAccount(@PathVariable("account_id") String account_id, @RequestBody ContactSearchContext context) {
        context.setN_customerid_eq(account_id);
        Page<Contact> domains = contactService.searchUsable(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(contactMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

