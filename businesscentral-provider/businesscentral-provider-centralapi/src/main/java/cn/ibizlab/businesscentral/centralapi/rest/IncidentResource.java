package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.service.domain.Incident;
import cn.ibizlab.businesscentral.core.service.service.IIncidentService;
import cn.ibizlab.businesscentral.core.service.filter.IncidentSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"案例" })
@RestController("CentralApi-incident")
@RequestMapping("")
public class IncidentResource {

    @Autowired
    public IIncidentService incidentService;

    @Autowired
    @Lazy
    public IncidentMapping incidentMapping;

    @PreAuthorize("hasPermission(this.incidentMapping.toDomain(#incidentdto),'iBizBusinessCentral-Incident-Create')")
    @ApiOperation(value = "新建案例", tags = {"案例" },  notes = "新建案例")
	@RequestMapping(method = RequestMethod.POST, value = "/incidents")
    public ResponseEntity<IncidentDTO> create(@RequestBody IncidentDTO incidentdto) {
        Incident domain = incidentMapping.toDomain(incidentdto);
		incidentService.create(domain);
        IncidentDTO dto = incidentMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.incidentMapping.toDomain(#incidentdtos),'iBizBusinessCentral-Incident-Create')")
    @ApiOperation(value = "批量新建案例", tags = {"案例" },  notes = "批量新建案例")
	@RequestMapping(method = RequestMethod.POST, value = "/incidents/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<IncidentDTO> incidentdtos) {
        incidentService.createBatch(incidentMapping.toDomain(incidentdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "incident" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.incidentService.get(#incident_id),'iBizBusinessCentral-Incident-Update')")
    @ApiOperation(value = "更新案例", tags = {"案例" },  notes = "更新案例")
	@RequestMapping(method = RequestMethod.PUT, value = "/incidents/{incident_id}")
    public ResponseEntity<IncidentDTO> update(@PathVariable("incident_id") String incident_id, @RequestBody IncidentDTO incidentdto) {
		Incident domain  = incidentMapping.toDomain(incidentdto);
        domain .setIncidentid(incident_id);
		incidentService.update(domain );
		IncidentDTO dto = incidentMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.incidentService.getIncidentByEntities(this.incidentMapping.toDomain(#incidentdtos)),'iBizBusinessCentral-Incident-Update')")
    @ApiOperation(value = "批量更新案例", tags = {"案例" },  notes = "批量更新案例")
	@RequestMapping(method = RequestMethod.PUT, value = "/incidents/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<IncidentDTO> incidentdtos) {
        incidentService.updateBatch(incidentMapping.toDomain(incidentdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.incidentService.get(#incident_id),'iBizBusinessCentral-Incident-Remove')")
    @ApiOperation(value = "删除案例", tags = {"案例" },  notes = "删除案例")
	@RequestMapping(method = RequestMethod.DELETE, value = "/incidents/{incident_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("incident_id") String incident_id) {
         return ResponseEntity.status(HttpStatus.OK).body(incidentService.remove(incident_id));
    }

    @PreAuthorize("hasPermission(this.incidentService.getIncidentByIds(#ids),'iBizBusinessCentral-Incident-Remove')")
    @ApiOperation(value = "批量删除案例", tags = {"案例" },  notes = "批量删除案例")
	@RequestMapping(method = RequestMethod.DELETE, value = "/incidents/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        incidentService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.incidentMapping.toDomain(returnObject.body),'iBizBusinessCentral-Incident-Get')")
    @ApiOperation(value = "获取案例", tags = {"案例" },  notes = "获取案例")
	@RequestMapping(method = RequestMethod.GET, value = "/incidents/{incident_id}")
    public ResponseEntity<IncidentDTO> get(@PathVariable("incident_id") String incident_id) {
        Incident domain = incidentService.get(incident_id);
        IncidentDTO dto = incidentMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取案例草稿", tags = {"案例" },  notes = "获取案例草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/incidents/getdraft")
    public ResponseEntity<IncidentDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(incidentMapping.toDto(incidentService.getDraft(new Incident())));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Incident-Active-all')")
    @ApiOperation(value = "重新激活", tags = {"案例" },  notes = "重新激活")
	@RequestMapping(method = RequestMethod.POST, value = "/incidents/{incident_id}/active")
    public ResponseEntity<IncidentDTO> active(@PathVariable("incident_id") String incident_id, @RequestBody IncidentDTO incidentdto) {
        Incident domain = incidentMapping.toDomain(incidentdto);
domain.setIncidentid(incident_id);
        domain = incidentService.active(domain);
        incidentdto = incidentMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(incidentdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Incident-Cancel-all')")
    @ApiOperation(value = "取消案例", tags = {"案例" },  notes = "取消案例")
	@RequestMapping(method = RequestMethod.POST, value = "/incidents/{incident_id}/cancel")
    public ResponseEntity<IncidentDTO> cancel(@PathVariable("incident_id") String incident_id, @RequestBody IncidentDTO incidentdto) {
        Incident domain = incidentMapping.toDomain(incidentdto);
domain.setIncidentid(incident_id);
        domain = incidentService.cancel(domain);
        incidentdto = incidentMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(incidentdto);
    }

    @ApiOperation(value = "检查案例", tags = {"案例" },  notes = "检查案例")
	@RequestMapping(method = RequestMethod.POST, value = "/incidents/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody IncidentDTO incidentdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(incidentService.checkKey(incidentMapping.toDomain(incidentdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Incident-Resolve-all')")
    @ApiOperation(value = "解决案例", tags = {"案例" },  notes = "解决案例")
	@RequestMapping(method = RequestMethod.POST, value = "/incidents/{incident_id}/resolve")
    public ResponseEntity<IncidentDTO> resolve(@PathVariable("incident_id") String incident_id, @RequestBody IncidentDTO incidentdto) {
        Incident domain = incidentMapping.toDomain(incidentdto);
domain.setIncidentid(incident_id);
        domain = incidentService.resolve(domain);
        incidentdto = incidentMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(incidentdto);
    }

    @PreAuthorize("hasPermission(this.incidentMapping.toDomain(#incidentdto),'iBizBusinessCentral-Incident-Save')")
    @ApiOperation(value = "保存案例", tags = {"案例" },  notes = "保存案例")
	@RequestMapping(method = RequestMethod.POST, value = "/incidents/save")
    public ResponseEntity<Boolean> save(@RequestBody IncidentDTO incidentdto) {
        return ResponseEntity.status(HttpStatus.OK).body(incidentService.save(incidentMapping.toDomain(incidentdto)));
    }

    @PreAuthorize("hasPermission(this.incidentMapping.toDomain(#incidentdtos),'iBizBusinessCentral-Incident-Save')")
    @ApiOperation(value = "批量保存案例", tags = {"案例" },  notes = "批量保存案例")
	@RequestMapping(method = RequestMethod.POST, value = "/incidents/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<IncidentDTO> incidentdtos) {
        incidentService.saveBatch(incidentMapping.toDomain(incidentdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Incident-searchByParentKey-all') and hasPermission(#context,'iBizBusinessCentral-Incident-Get')")
	@ApiOperation(value = "获取ByParentKey", tags = {"案例" } ,notes = "获取ByParentKey")
    @RequestMapping(method= RequestMethod.GET , value="/incidents/fetchbyparentkey")
	public ResponseEntity<List<IncidentDTO>> fetchByParentKey(IncidentSearchContext context) {
        Page<Incident> domains = incidentService.searchByParentKey(context) ;
        List<IncidentDTO> list = incidentMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Incident-searchByParentKey-all') and hasPermission(#context,'iBizBusinessCentral-Incident-Get')")
	@ApiOperation(value = "查询ByParentKey", tags = {"案例" } ,notes = "查询ByParentKey")
    @RequestMapping(method= RequestMethod.POST , value="/incidents/searchbyparentkey")
	public ResponseEntity<Page<IncidentDTO>> searchByParentKey(@RequestBody IncidentSearchContext context) {
        Page<Incident> domains = incidentService.searchByParentKey(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(incidentMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Incident-searchCancel-all') and hasPermission(#context,'iBizBusinessCentral-Incident-Get')")
	@ApiOperation(value = "获取已取消", tags = {"案例" } ,notes = "获取已取消")
    @RequestMapping(method= RequestMethod.GET , value="/incidents/fetchcancel")
	public ResponseEntity<List<IncidentDTO>> fetchCancel(IncidentSearchContext context) {
        Page<Incident> domains = incidentService.searchCancel(context) ;
        List<IncidentDTO> list = incidentMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Incident-searchCancel-all') and hasPermission(#context,'iBizBusinessCentral-Incident-Get')")
	@ApiOperation(value = "查询已取消", tags = {"案例" } ,notes = "查询已取消")
    @RequestMapping(method= RequestMethod.POST , value="/incidents/searchcancel")
	public ResponseEntity<Page<IncidentDTO>> searchCancel(@RequestBody IncidentSearchContext context) {
        Page<Incident> domains = incidentService.searchCancel(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(incidentMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Incident-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Incident-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"案例" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/incidents/fetchdefault")
	public ResponseEntity<List<IncidentDTO>> fetchDefault(IncidentSearchContext context) {
        Page<Incident> domains = incidentService.searchDefault(context) ;
        List<IncidentDTO> list = incidentMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Incident-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Incident-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"案例" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/incidents/searchdefault")
	public ResponseEntity<Page<IncidentDTO>> searchDefault(@RequestBody IncidentSearchContext context) {
        Page<Incident> domains = incidentService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(incidentMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Incident-searchEffective-all') and hasPermission(#context,'iBizBusinessCentral-Incident-Get')")
	@ApiOperation(value = "获取有效", tags = {"案例" } ,notes = "获取有效")
    @RequestMapping(method= RequestMethod.GET , value="/incidents/fetcheffective")
	public ResponseEntity<List<IncidentDTO>> fetchEffective(IncidentSearchContext context) {
        Page<Incident> domains = incidentService.searchEffective(context) ;
        List<IncidentDTO> list = incidentMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Incident-searchEffective-all') and hasPermission(#context,'iBizBusinessCentral-Incident-Get')")
	@ApiOperation(value = "查询有效", tags = {"案例" } ,notes = "查询有效")
    @RequestMapping(method= RequestMethod.POST , value="/incidents/searcheffective")
	public ResponseEntity<Page<IncidentDTO>> searchEffective(@RequestBody IncidentSearchContext context) {
        Page<Incident> domains = incidentService.searchEffective(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(incidentMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Incident-searchResolved-all') and hasPermission(#context,'iBizBusinessCentral-Incident-Get')")
	@ApiOperation(value = "获取已解决", tags = {"案例" } ,notes = "获取已解决")
    @RequestMapping(method= RequestMethod.GET , value="/incidents/fetchresolved")
	public ResponseEntity<List<IncidentDTO>> fetchResolved(IncidentSearchContext context) {
        Page<Incident> domains = incidentService.searchResolved(context) ;
        List<IncidentDTO> list = incidentMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Incident-searchResolved-all') and hasPermission(#context,'iBizBusinessCentral-Incident-Get')")
	@ApiOperation(value = "查询已解决", tags = {"案例" } ,notes = "查询已解决")
    @RequestMapping(method= RequestMethod.POST , value="/incidents/searchresolved")
	public ResponseEntity<Page<IncidentDTO>> searchResolved(@RequestBody IncidentSearchContext context) {
        Page<Incident> domains = incidentService.searchResolved(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(incidentMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

