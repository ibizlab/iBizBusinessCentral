package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.runtime.domain.BulkOperation;
import cn.ibizlab.businesscentral.core.runtime.service.IBulkOperationService;
import cn.ibizlab.businesscentral.core.runtime.filter.BulkOperationSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"快速市场活动" })
@RestController("CentralApi-bulkoperation")
@RequestMapping("")
public class BulkOperationResource {

    @Autowired
    public IBulkOperationService bulkoperationService;

    @Autowired
    @Lazy
    public BulkOperationMapping bulkoperationMapping;

    @PreAuthorize("hasPermission(this.bulkoperationMapping.toDomain(#bulkoperationdto),'iBizBusinessCentral-BulkOperation-Create')")
    @ApiOperation(value = "新建快速市场活动", tags = {"快速市场活动" },  notes = "新建快速市场活动")
	@RequestMapping(method = RequestMethod.POST, value = "/bulkoperations")
    public ResponseEntity<BulkOperationDTO> create(@RequestBody BulkOperationDTO bulkoperationdto) {
        BulkOperation domain = bulkoperationMapping.toDomain(bulkoperationdto);
		bulkoperationService.create(domain);
        BulkOperationDTO dto = bulkoperationMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.bulkoperationMapping.toDomain(#bulkoperationdtos),'iBizBusinessCentral-BulkOperation-Create')")
    @ApiOperation(value = "批量新建快速市场活动", tags = {"快速市场活动" },  notes = "批量新建快速市场活动")
	@RequestMapping(method = RequestMethod.POST, value = "/bulkoperations/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<BulkOperationDTO> bulkoperationdtos) {
        bulkoperationService.createBatch(bulkoperationMapping.toDomain(bulkoperationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "bulkoperation" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.bulkoperationService.get(#bulkoperation_id),'iBizBusinessCentral-BulkOperation-Update')")
    @ApiOperation(value = "更新快速市场活动", tags = {"快速市场活动" },  notes = "更新快速市场活动")
	@RequestMapping(method = RequestMethod.PUT, value = "/bulkoperations/{bulkoperation_id}")
    public ResponseEntity<BulkOperationDTO> update(@PathVariable("bulkoperation_id") String bulkoperation_id, @RequestBody BulkOperationDTO bulkoperationdto) {
		BulkOperation domain  = bulkoperationMapping.toDomain(bulkoperationdto);
        domain .setActivityid(bulkoperation_id);
		bulkoperationService.update(domain );
		BulkOperationDTO dto = bulkoperationMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.bulkoperationService.getBulkoperationByEntities(this.bulkoperationMapping.toDomain(#bulkoperationdtos)),'iBizBusinessCentral-BulkOperation-Update')")
    @ApiOperation(value = "批量更新快速市场活动", tags = {"快速市场活动" },  notes = "批量更新快速市场活动")
	@RequestMapping(method = RequestMethod.PUT, value = "/bulkoperations/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<BulkOperationDTO> bulkoperationdtos) {
        bulkoperationService.updateBatch(bulkoperationMapping.toDomain(bulkoperationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.bulkoperationService.get(#bulkoperation_id),'iBizBusinessCentral-BulkOperation-Remove')")
    @ApiOperation(value = "删除快速市场活动", tags = {"快速市场活动" },  notes = "删除快速市场活动")
	@RequestMapping(method = RequestMethod.DELETE, value = "/bulkoperations/{bulkoperation_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("bulkoperation_id") String bulkoperation_id) {
         return ResponseEntity.status(HttpStatus.OK).body(bulkoperationService.remove(bulkoperation_id));
    }

    @PreAuthorize("hasPermission(this.bulkoperationService.getBulkoperationByIds(#ids),'iBizBusinessCentral-BulkOperation-Remove')")
    @ApiOperation(value = "批量删除快速市场活动", tags = {"快速市场活动" },  notes = "批量删除快速市场活动")
	@RequestMapping(method = RequestMethod.DELETE, value = "/bulkoperations/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        bulkoperationService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.bulkoperationMapping.toDomain(returnObject.body),'iBizBusinessCentral-BulkOperation-Get')")
    @ApiOperation(value = "获取快速市场活动", tags = {"快速市场活动" },  notes = "获取快速市场活动")
	@RequestMapping(method = RequestMethod.GET, value = "/bulkoperations/{bulkoperation_id}")
    public ResponseEntity<BulkOperationDTO> get(@PathVariable("bulkoperation_id") String bulkoperation_id) {
        BulkOperation domain = bulkoperationService.get(bulkoperation_id);
        BulkOperationDTO dto = bulkoperationMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取快速市场活动草稿", tags = {"快速市场活动" },  notes = "获取快速市场活动草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/bulkoperations/getdraft")
    public ResponseEntity<BulkOperationDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(bulkoperationMapping.toDto(bulkoperationService.getDraft(new BulkOperation())));
    }

    @ApiOperation(value = "检查快速市场活动", tags = {"快速市场活动" },  notes = "检查快速市场活动")
	@RequestMapping(method = RequestMethod.POST, value = "/bulkoperations/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody BulkOperationDTO bulkoperationdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(bulkoperationService.checkKey(bulkoperationMapping.toDomain(bulkoperationdto)));
    }

    @PreAuthorize("hasPermission(this.bulkoperationMapping.toDomain(#bulkoperationdto),'iBizBusinessCentral-BulkOperation-Save')")
    @ApiOperation(value = "保存快速市场活动", tags = {"快速市场活动" },  notes = "保存快速市场活动")
	@RequestMapping(method = RequestMethod.POST, value = "/bulkoperations/save")
    public ResponseEntity<Boolean> save(@RequestBody BulkOperationDTO bulkoperationdto) {
        return ResponseEntity.status(HttpStatus.OK).body(bulkoperationService.save(bulkoperationMapping.toDomain(bulkoperationdto)));
    }

    @PreAuthorize("hasPermission(this.bulkoperationMapping.toDomain(#bulkoperationdtos),'iBizBusinessCentral-BulkOperation-Save')")
    @ApiOperation(value = "批量保存快速市场活动", tags = {"快速市场活动" },  notes = "批量保存快速市场活动")
	@RequestMapping(method = RequestMethod.POST, value = "/bulkoperations/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<BulkOperationDTO> bulkoperationdtos) {
        bulkoperationService.saveBatch(bulkoperationMapping.toDomain(bulkoperationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-BulkOperation-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-BulkOperation-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"快速市场活动" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/bulkoperations/fetchdefault")
	public ResponseEntity<List<BulkOperationDTO>> fetchDefault(BulkOperationSearchContext context) {
        Page<BulkOperation> domains = bulkoperationService.searchDefault(context) ;
        List<BulkOperationDTO> list = bulkoperationMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-BulkOperation-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-BulkOperation-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"快速市场活动" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/bulkoperations/searchdefault")
	public ResponseEntity<Page<BulkOperationDTO>> searchDefault(@RequestBody BulkOperationSearchContext context) {
        Page<BulkOperation> domains = bulkoperationService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(bulkoperationMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

