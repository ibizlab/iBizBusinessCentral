package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.marketing.domain.ListLead;
import cn.ibizlab.businesscentral.core.marketing.service.IListLeadService;
import cn.ibizlab.businesscentral.core.marketing.filter.ListLeadSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"营销列表-潜在客户" })
@RestController("CentralApi-listlead")
@RequestMapping("")
public class ListLeadResource {

    @Autowired
    public IListLeadService listleadService;

    @Autowired
    @Lazy
    public ListLeadMapping listleadMapping;

    @PreAuthorize("hasPermission(this.listleadMapping.toDomain(#listleaddto),'iBizBusinessCentral-ListLead-Create')")
    @ApiOperation(value = "新建营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "新建营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.POST, value = "/listleads")
    public ResponseEntity<ListLeadDTO> create(@RequestBody ListLeadDTO listleaddto) {
        ListLead domain = listleadMapping.toDomain(listleaddto);
		listleadService.create(domain);
        ListLeadDTO dto = listleadMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.listleadMapping.toDomain(#listleaddtos),'iBizBusinessCentral-ListLead-Create')")
    @ApiOperation(value = "批量新建营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "批量新建营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.POST, value = "/listleads/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<ListLeadDTO> listleaddtos) {
        listleadService.createBatch(listleadMapping.toDomain(listleaddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "listlead" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.listleadService.get(#listlead_id),'iBizBusinessCentral-ListLead-Update')")
    @ApiOperation(value = "更新营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "更新营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.PUT, value = "/listleads/{listlead_id}")
    public ResponseEntity<ListLeadDTO> update(@PathVariable("listlead_id") String listlead_id, @RequestBody ListLeadDTO listleaddto) {
		ListLead domain  = listleadMapping.toDomain(listleaddto);
        domain .setRelationshipsid(listlead_id);
		listleadService.update(domain );
		ListLeadDTO dto = listleadMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.listleadService.getListleadByEntities(this.listleadMapping.toDomain(#listleaddtos)),'iBizBusinessCentral-ListLead-Update')")
    @ApiOperation(value = "批量更新营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "批量更新营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.PUT, value = "/listleads/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<ListLeadDTO> listleaddtos) {
        listleadService.updateBatch(listleadMapping.toDomain(listleaddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.listleadService.get(#listlead_id),'iBizBusinessCentral-ListLead-Remove')")
    @ApiOperation(value = "删除营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "删除营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.DELETE, value = "/listleads/{listlead_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("listlead_id") String listlead_id) {
         return ResponseEntity.status(HttpStatus.OK).body(listleadService.remove(listlead_id));
    }

    @PreAuthorize("hasPermission(this.listleadService.getListleadByIds(#ids),'iBizBusinessCentral-ListLead-Remove')")
    @ApiOperation(value = "批量删除营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "批量删除营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.DELETE, value = "/listleads/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        listleadService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.listleadMapping.toDomain(returnObject.body),'iBizBusinessCentral-ListLead-Get')")
    @ApiOperation(value = "获取营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "获取营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.GET, value = "/listleads/{listlead_id}")
    public ResponseEntity<ListLeadDTO> get(@PathVariable("listlead_id") String listlead_id) {
        ListLead domain = listleadService.get(listlead_id);
        ListLeadDTO dto = listleadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取营销列表-潜在客户草稿", tags = {"营销列表-潜在客户" },  notes = "获取营销列表-潜在客户草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/listleads/getdraft")
    public ResponseEntity<ListLeadDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(listleadMapping.toDto(listleadService.getDraft(new ListLead())));
    }

    @ApiOperation(value = "检查营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "检查营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.POST, value = "/listleads/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody ListLeadDTO listleaddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(listleadService.checkKey(listleadMapping.toDomain(listleaddto)));
    }

    @PreAuthorize("hasPermission(this.listleadMapping.toDomain(#listleaddto),'iBizBusinessCentral-ListLead-Save')")
    @ApiOperation(value = "保存营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "保存营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.POST, value = "/listleads/save")
    public ResponseEntity<Boolean> save(@RequestBody ListLeadDTO listleaddto) {
        return ResponseEntity.status(HttpStatus.OK).body(listleadService.save(listleadMapping.toDomain(listleaddto)));
    }

    @PreAuthorize("hasPermission(this.listleadMapping.toDomain(#listleaddtos),'iBizBusinessCentral-ListLead-Save')")
    @ApiOperation(value = "批量保存营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "批量保存营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.POST, value = "/listleads/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<ListLeadDTO> listleaddtos) {
        listleadService.saveBatch(listleadMapping.toDomain(listleaddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ListLead-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-ListLead-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"营销列表-潜在客户" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/listleads/fetchdefault")
	public ResponseEntity<List<ListLeadDTO>> fetchDefault(ListLeadSearchContext context) {
        Page<ListLead> domains = listleadService.searchDefault(context) ;
        List<ListLeadDTO> list = listleadMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ListLead-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-ListLead-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"营销列表-潜在客户" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/listleads/searchdefault")
	public ResponseEntity<Page<ListLeadDTO>> searchDefault(@RequestBody ListLeadSearchContext context) {
        Page<ListLead> domains = listleadService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(listleadMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.listleadMapping.toDomain(#listleaddto),'iBizBusinessCentral-ListLead-Create')")
    @ApiOperation(value = "根据潜在顾客建立营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据潜在顾客建立营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.POST, value = "/leads/{lead_id}/listleads")
    public ResponseEntity<ListLeadDTO> createByLead(@PathVariable("lead_id") String lead_id, @RequestBody ListLeadDTO listleaddto) {
        ListLead domain = listleadMapping.toDomain(listleaddto);
        domain.setEntity2id(lead_id);
		listleadService.create(domain);
        ListLeadDTO dto = listleadMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.listleadMapping.toDomain(#listleaddtos),'iBizBusinessCentral-ListLead-Create')")
    @ApiOperation(value = "根据潜在顾客批量建立营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据潜在顾客批量建立营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.POST, value = "/leads/{lead_id}/listleads/batch")
    public ResponseEntity<Boolean> createBatchByLead(@PathVariable("lead_id") String lead_id, @RequestBody List<ListLeadDTO> listleaddtos) {
        List<ListLead> domainlist=listleadMapping.toDomain(listleaddtos);
        for(ListLead domain:domainlist){
            domain.setEntity2id(lead_id);
        }
        listleadService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "listlead" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.listleadService.get(#listlead_id),'iBizBusinessCentral-ListLead-Update')")
    @ApiOperation(value = "根据潜在顾客更新营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据潜在顾客更新营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.PUT, value = "/leads/{lead_id}/listleads/{listlead_id}")
    public ResponseEntity<ListLeadDTO> updateByLead(@PathVariable("lead_id") String lead_id, @PathVariable("listlead_id") String listlead_id, @RequestBody ListLeadDTO listleaddto) {
        ListLead domain = listleadMapping.toDomain(listleaddto);
        domain.setEntity2id(lead_id);
        domain.setRelationshipsid(listlead_id);
		listleadService.update(domain);
        ListLeadDTO dto = listleadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.listleadService.getListleadByEntities(this.listleadMapping.toDomain(#listleaddtos)),'iBizBusinessCentral-ListLead-Update')")
    @ApiOperation(value = "根据潜在顾客批量更新营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据潜在顾客批量更新营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.PUT, value = "/leads/{lead_id}/listleads/batch")
    public ResponseEntity<Boolean> updateBatchByLead(@PathVariable("lead_id") String lead_id, @RequestBody List<ListLeadDTO> listleaddtos) {
        List<ListLead> domainlist=listleadMapping.toDomain(listleaddtos);
        for(ListLead domain:domainlist){
            domain.setEntity2id(lead_id);
        }
        listleadService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.listleadService.get(#listlead_id),'iBizBusinessCentral-ListLead-Remove')")
    @ApiOperation(value = "根据潜在顾客删除营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据潜在顾客删除营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.DELETE, value = "/leads/{lead_id}/listleads/{listlead_id}")
    public ResponseEntity<Boolean> removeByLead(@PathVariable("lead_id") String lead_id, @PathVariable("listlead_id") String listlead_id) {
		return ResponseEntity.status(HttpStatus.OK).body(listleadService.remove(listlead_id));
    }

    @PreAuthorize("hasPermission(this.listleadService.getListleadByIds(#ids),'iBizBusinessCentral-ListLead-Remove')")
    @ApiOperation(value = "根据潜在顾客批量删除营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据潜在顾客批量删除营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.DELETE, value = "/leads/{lead_id}/listleads/batch")
    public ResponseEntity<Boolean> removeBatchByLead(@RequestBody List<String> ids) {
        listleadService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.listleadMapping.toDomain(returnObject.body),'iBizBusinessCentral-ListLead-Get')")
    @ApiOperation(value = "根据潜在顾客获取营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据潜在顾客获取营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.GET, value = "/leads/{lead_id}/listleads/{listlead_id}")
    public ResponseEntity<ListLeadDTO> getByLead(@PathVariable("lead_id") String lead_id, @PathVariable("listlead_id") String listlead_id) {
        ListLead domain = listleadService.get(listlead_id);
        ListLeadDTO dto = listleadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据潜在顾客获取营销列表-潜在客户草稿", tags = {"营销列表-潜在客户" },  notes = "根据潜在顾客获取营销列表-潜在客户草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/leads/{lead_id}/listleads/getdraft")
    public ResponseEntity<ListLeadDTO> getDraftByLead(@PathVariable("lead_id") String lead_id) {
        ListLead domain = new ListLead();
        domain.setEntity2id(lead_id);
        return ResponseEntity.status(HttpStatus.OK).body(listleadMapping.toDto(listleadService.getDraft(domain)));
    }

    @ApiOperation(value = "根据潜在顾客检查营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据潜在顾客检查营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.POST, value = "/leads/{lead_id}/listleads/checkkey")
    public ResponseEntity<Boolean> checkKeyByLead(@PathVariable("lead_id") String lead_id, @RequestBody ListLeadDTO listleaddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(listleadService.checkKey(listleadMapping.toDomain(listleaddto)));
    }

    @PreAuthorize("hasPermission(this.listleadMapping.toDomain(#listleaddto),'iBizBusinessCentral-ListLead-Save')")
    @ApiOperation(value = "根据潜在顾客保存营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据潜在顾客保存营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.POST, value = "/leads/{lead_id}/listleads/save")
    public ResponseEntity<Boolean> saveByLead(@PathVariable("lead_id") String lead_id, @RequestBody ListLeadDTO listleaddto) {
        ListLead domain = listleadMapping.toDomain(listleaddto);
        domain.setEntity2id(lead_id);
        return ResponseEntity.status(HttpStatus.OK).body(listleadService.save(domain));
    }

    @PreAuthorize("hasPermission(this.listleadMapping.toDomain(#listleaddtos),'iBizBusinessCentral-ListLead-Save')")
    @ApiOperation(value = "根据潜在顾客批量保存营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据潜在顾客批量保存营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.POST, value = "/leads/{lead_id}/listleads/savebatch")
    public ResponseEntity<Boolean> saveBatchByLead(@PathVariable("lead_id") String lead_id, @RequestBody List<ListLeadDTO> listleaddtos) {
        List<ListLead> domainlist=listleadMapping.toDomain(listleaddtos);
        for(ListLead domain:domainlist){
             domain.setEntity2id(lead_id);
        }
        listleadService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ListLead-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-ListLead-Get')")
	@ApiOperation(value = "根据潜在顾客获取DEFAULT", tags = {"营销列表-潜在客户" } ,notes = "根据潜在顾客获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/leads/{lead_id}/listleads/fetchdefault")
	public ResponseEntity<List<ListLeadDTO>> fetchListLeadDefaultByLead(@PathVariable("lead_id") String lead_id,ListLeadSearchContext context) {
        context.setN_entity2id_eq(lead_id);
        Page<ListLead> domains = listleadService.searchDefault(context) ;
        List<ListLeadDTO> list = listleadMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ListLead-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-ListLead-Get')")
	@ApiOperation(value = "根据潜在顾客查询DEFAULT", tags = {"营销列表-潜在客户" } ,notes = "根据潜在顾客查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/leads/{lead_id}/listleads/searchdefault")
	public ResponseEntity<Page<ListLeadDTO>> searchListLeadDefaultByLead(@PathVariable("lead_id") String lead_id, @RequestBody ListLeadSearchContext context) {
        context.setN_entity2id_eq(lead_id);
        Page<ListLead> domains = listleadService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(listleadMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.listleadMapping.toDomain(#listleaddto),'iBizBusinessCentral-ListLead-Create')")
    @ApiOperation(value = "根据市场营销列表建立营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据市场营销列表建立营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.POST, value = "/ibizlists/{ibizlist_id}/listleads")
    public ResponseEntity<ListLeadDTO> createByIBizList(@PathVariable("ibizlist_id") String ibizlist_id, @RequestBody ListLeadDTO listleaddto) {
        ListLead domain = listleadMapping.toDomain(listleaddto);
        domain.setEntityid(ibizlist_id);
		listleadService.create(domain);
        ListLeadDTO dto = listleadMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.listleadMapping.toDomain(#listleaddtos),'iBizBusinessCentral-ListLead-Create')")
    @ApiOperation(value = "根据市场营销列表批量建立营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据市场营销列表批量建立营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.POST, value = "/ibizlists/{ibizlist_id}/listleads/batch")
    public ResponseEntity<Boolean> createBatchByIBizList(@PathVariable("ibizlist_id") String ibizlist_id, @RequestBody List<ListLeadDTO> listleaddtos) {
        List<ListLead> domainlist=listleadMapping.toDomain(listleaddtos);
        for(ListLead domain:domainlist){
            domain.setEntityid(ibizlist_id);
        }
        listleadService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "listlead" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.listleadService.get(#listlead_id),'iBizBusinessCentral-ListLead-Update')")
    @ApiOperation(value = "根据市场营销列表更新营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据市场营销列表更新营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.PUT, value = "/ibizlists/{ibizlist_id}/listleads/{listlead_id}")
    public ResponseEntity<ListLeadDTO> updateByIBizList(@PathVariable("ibizlist_id") String ibizlist_id, @PathVariable("listlead_id") String listlead_id, @RequestBody ListLeadDTO listleaddto) {
        ListLead domain = listleadMapping.toDomain(listleaddto);
        domain.setEntityid(ibizlist_id);
        domain.setRelationshipsid(listlead_id);
		listleadService.update(domain);
        ListLeadDTO dto = listleadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.listleadService.getListleadByEntities(this.listleadMapping.toDomain(#listleaddtos)),'iBizBusinessCentral-ListLead-Update')")
    @ApiOperation(value = "根据市场营销列表批量更新营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据市场营销列表批量更新营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.PUT, value = "/ibizlists/{ibizlist_id}/listleads/batch")
    public ResponseEntity<Boolean> updateBatchByIBizList(@PathVariable("ibizlist_id") String ibizlist_id, @RequestBody List<ListLeadDTO> listleaddtos) {
        List<ListLead> domainlist=listleadMapping.toDomain(listleaddtos);
        for(ListLead domain:domainlist){
            domain.setEntityid(ibizlist_id);
        }
        listleadService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.listleadService.get(#listlead_id),'iBizBusinessCentral-ListLead-Remove')")
    @ApiOperation(value = "根据市场营销列表删除营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据市场营销列表删除营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.DELETE, value = "/ibizlists/{ibizlist_id}/listleads/{listlead_id}")
    public ResponseEntity<Boolean> removeByIBizList(@PathVariable("ibizlist_id") String ibizlist_id, @PathVariable("listlead_id") String listlead_id) {
		return ResponseEntity.status(HttpStatus.OK).body(listleadService.remove(listlead_id));
    }

    @PreAuthorize("hasPermission(this.listleadService.getListleadByIds(#ids),'iBizBusinessCentral-ListLead-Remove')")
    @ApiOperation(value = "根据市场营销列表批量删除营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据市场营销列表批量删除营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.DELETE, value = "/ibizlists/{ibizlist_id}/listleads/batch")
    public ResponseEntity<Boolean> removeBatchByIBizList(@RequestBody List<String> ids) {
        listleadService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.listleadMapping.toDomain(returnObject.body),'iBizBusinessCentral-ListLead-Get')")
    @ApiOperation(value = "根据市场营销列表获取营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据市场营销列表获取营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.GET, value = "/ibizlists/{ibizlist_id}/listleads/{listlead_id}")
    public ResponseEntity<ListLeadDTO> getByIBizList(@PathVariable("ibizlist_id") String ibizlist_id, @PathVariable("listlead_id") String listlead_id) {
        ListLead domain = listleadService.get(listlead_id);
        ListLeadDTO dto = listleadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据市场营销列表获取营销列表-潜在客户草稿", tags = {"营销列表-潜在客户" },  notes = "根据市场营销列表获取营销列表-潜在客户草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/ibizlists/{ibizlist_id}/listleads/getdraft")
    public ResponseEntity<ListLeadDTO> getDraftByIBizList(@PathVariable("ibizlist_id") String ibizlist_id) {
        ListLead domain = new ListLead();
        domain.setEntityid(ibizlist_id);
        return ResponseEntity.status(HttpStatus.OK).body(listleadMapping.toDto(listleadService.getDraft(domain)));
    }

    @ApiOperation(value = "根据市场营销列表检查营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据市场营销列表检查营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.POST, value = "/ibizlists/{ibizlist_id}/listleads/checkkey")
    public ResponseEntity<Boolean> checkKeyByIBizList(@PathVariable("ibizlist_id") String ibizlist_id, @RequestBody ListLeadDTO listleaddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(listleadService.checkKey(listleadMapping.toDomain(listleaddto)));
    }

    @PreAuthorize("hasPermission(this.listleadMapping.toDomain(#listleaddto),'iBizBusinessCentral-ListLead-Save')")
    @ApiOperation(value = "根据市场营销列表保存营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据市场营销列表保存营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.POST, value = "/ibizlists/{ibizlist_id}/listleads/save")
    public ResponseEntity<Boolean> saveByIBizList(@PathVariable("ibizlist_id") String ibizlist_id, @RequestBody ListLeadDTO listleaddto) {
        ListLead domain = listleadMapping.toDomain(listleaddto);
        domain.setEntityid(ibizlist_id);
        return ResponseEntity.status(HttpStatus.OK).body(listleadService.save(domain));
    }

    @PreAuthorize("hasPermission(this.listleadMapping.toDomain(#listleaddtos),'iBizBusinessCentral-ListLead-Save')")
    @ApiOperation(value = "根据市场营销列表批量保存营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据市场营销列表批量保存营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.POST, value = "/ibizlists/{ibizlist_id}/listleads/savebatch")
    public ResponseEntity<Boolean> saveBatchByIBizList(@PathVariable("ibizlist_id") String ibizlist_id, @RequestBody List<ListLeadDTO> listleaddtos) {
        List<ListLead> domainlist=listleadMapping.toDomain(listleaddtos);
        for(ListLead domain:domainlist){
             domain.setEntityid(ibizlist_id);
        }
        listleadService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ListLead-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-ListLead-Get')")
	@ApiOperation(value = "根据市场营销列表获取DEFAULT", tags = {"营销列表-潜在客户" } ,notes = "根据市场营销列表获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/ibizlists/{ibizlist_id}/listleads/fetchdefault")
	public ResponseEntity<List<ListLeadDTO>> fetchListLeadDefaultByIBizList(@PathVariable("ibizlist_id") String ibizlist_id,ListLeadSearchContext context) {
        context.setN_entityid_eq(ibizlist_id);
        Page<ListLead> domains = listleadService.searchDefault(context) ;
        List<ListLeadDTO> list = listleadMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ListLead-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-ListLead-Get')")
	@ApiOperation(value = "根据市场营销列表查询DEFAULT", tags = {"营销列表-潜在客户" } ,notes = "根据市场营销列表查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/ibizlists/{ibizlist_id}/listleads/searchdefault")
	public ResponseEntity<Page<ListLeadDTO>> searchListLeadDefaultByIBizList(@PathVariable("ibizlist_id") String ibizlist_id, @RequestBody ListLeadSearchContext context) {
        context.setN_entityid_eq(ibizlist_id);
        Page<ListLead> domains = listleadService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(listleadMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.listleadMapping.toDomain(#listleaddto),'iBizBusinessCentral-ListLead-Create')")
    @ApiOperation(value = "根据客户潜在顾客建立营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据客户潜在顾客建立营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/leads/{lead_id}/listleads")
    public ResponseEntity<ListLeadDTO> createByAccountLead(@PathVariable("account_id") String account_id, @PathVariable("lead_id") String lead_id, @RequestBody ListLeadDTO listleaddto) {
        ListLead domain = listleadMapping.toDomain(listleaddto);
        domain.setEntity2id(lead_id);
		listleadService.create(domain);
        ListLeadDTO dto = listleadMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.listleadMapping.toDomain(#listleaddtos),'iBizBusinessCentral-ListLead-Create')")
    @ApiOperation(value = "根据客户潜在顾客批量建立营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据客户潜在顾客批量建立营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/leads/{lead_id}/listleads/batch")
    public ResponseEntity<Boolean> createBatchByAccountLead(@PathVariable("account_id") String account_id, @PathVariable("lead_id") String lead_id, @RequestBody List<ListLeadDTO> listleaddtos) {
        List<ListLead> domainlist=listleadMapping.toDomain(listleaddtos);
        for(ListLead domain:domainlist){
            domain.setEntity2id(lead_id);
        }
        listleadService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "listlead" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.listleadService.get(#listlead_id),'iBizBusinessCentral-ListLead-Update')")
    @ApiOperation(value = "根据客户潜在顾客更新营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据客户潜在顾客更新营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/leads/{lead_id}/listleads/{listlead_id}")
    public ResponseEntity<ListLeadDTO> updateByAccountLead(@PathVariable("account_id") String account_id, @PathVariable("lead_id") String lead_id, @PathVariable("listlead_id") String listlead_id, @RequestBody ListLeadDTO listleaddto) {
        ListLead domain = listleadMapping.toDomain(listleaddto);
        domain.setEntity2id(lead_id);
        domain.setRelationshipsid(listlead_id);
		listleadService.update(domain);
        ListLeadDTO dto = listleadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.listleadService.getListleadByEntities(this.listleadMapping.toDomain(#listleaddtos)),'iBizBusinessCentral-ListLead-Update')")
    @ApiOperation(value = "根据客户潜在顾客批量更新营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据客户潜在顾客批量更新营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/leads/{lead_id}/listleads/batch")
    public ResponseEntity<Boolean> updateBatchByAccountLead(@PathVariable("account_id") String account_id, @PathVariable("lead_id") String lead_id, @RequestBody List<ListLeadDTO> listleaddtos) {
        List<ListLead> domainlist=listleadMapping.toDomain(listleaddtos);
        for(ListLead domain:domainlist){
            domain.setEntity2id(lead_id);
        }
        listleadService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.listleadService.get(#listlead_id),'iBizBusinessCentral-ListLead-Remove')")
    @ApiOperation(value = "根据客户潜在顾客删除营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据客户潜在顾客删除营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/leads/{lead_id}/listleads/{listlead_id}")
    public ResponseEntity<Boolean> removeByAccountLead(@PathVariable("account_id") String account_id, @PathVariable("lead_id") String lead_id, @PathVariable("listlead_id") String listlead_id) {
		return ResponseEntity.status(HttpStatus.OK).body(listleadService.remove(listlead_id));
    }

    @PreAuthorize("hasPermission(this.listleadService.getListleadByIds(#ids),'iBizBusinessCentral-ListLead-Remove')")
    @ApiOperation(value = "根据客户潜在顾客批量删除营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据客户潜在顾客批量删除营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/leads/{lead_id}/listleads/batch")
    public ResponseEntity<Boolean> removeBatchByAccountLead(@RequestBody List<String> ids) {
        listleadService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.listleadMapping.toDomain(returnObject.body),'iBizBusinessCentral-ListLead-Get')")
    @ApiOperation(value = "根据客户潜在顾客获取营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据客户潜在顾客获取营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/leads/{lead_id}/listleads/{listlead_id}")
    public ResponseEntity<ListLeadDTO> getByAccountLead(@PathVariable("account_id") String account_id, @PathVariable("lead_id") String lead_id, @PathVariable("listlead_id") String listlead_id) {
        ListLead domain = listleadService.get(listlead_id);
        ListLeadDTO dto = listleadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据客户潜在顾客获取营销列表-潜在客户草稿", tags = {"营销列表-潜在客户" },  notes = "根据客户潜在顾客获取营销列表-潜在客户草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/leads/{lead_id}/listleads/getdraft")
    public ResponseEntity<ListLeadDTO> getDraftByAccountLead(@PathVariable("account_id") String account_id, @PathVariable("lead_id") String lead_id) {
        ListLead domain = new ListLead();
        domain.setEntity2id(lead_id);
        return ResponseEntity.status(HttpStatus.OK).body(listleadMapping.toDto(listleadService.getDraft(domain)));
    }

    @ApiOperation(value = "根据客户潜在顾客检查营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据客户潜在顾客检查营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/leads/{lead_id}/listleads/checkkey")
    public ResponseEntity<Boolean> checkKeyByAccountLead(@PathVariable("account_id") String account_id, @PathVariable("lead_id") String lead_id, @RequestBody ListLeadDTO listleaddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(listleadService.checkKey(listleadMapping.toDomain(listleaddto)));
    }

    @PreAuthorize("hasPermission(this.listleadMapping.toDomain(#listleaddto),'iBizBusinessCentral-ListLead-Save')")
    @ApiOperation(value = "根据客户潜在顾客保存营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据客户潜在顾客保存营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/leads/{lead_id}/listleads/save")
    public ResponseEntity<Boolean> saveByAccountLead(@PathVariable("account_id") String account_id, @PathVariable("lead_id") String lead_id, @RequestBody ListLeadDTO listleaddto) {
        ListLead domain = listleadMapping.toDomain(listleaddto);
        domain.setEntity2id(lead_id);
        return ResponseEntity.status(HttpStatus.OK).body(listleadService.save(domain));
    }

    @PreAuthorize("hasPermission(this.listleadMapping.toDomain(#listleaddtos),'iBizBusinessCentral-ListLead-Save')")
    @ApiOperation(value = "根据客户潜在顾客批量保存营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据客户潜在顾客批量保存营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/leads/{lead_id}/listleads/savebatch")
    public ResponseEntity<Boolean> saveBatchByAccountLead(@PathVariable("account_id") String account_id, @PathVariable("lead_id") String lead_id, @RequestBody List<ListLeadDTO> listleaddtos) {
        List<ListLead> domainlist=listleadMapping.toDomain(listleaddtos);
        for(ListLead domain:domainlist){
             domain.setEntity2id(lead_id);
        }
        listleadService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ListLead-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-ListLead-Get')")
	@ApiOperation(value = "根据客户潜在顾客获取DEFAULT", tags = {"营销列表-潜在客户" } ,notes = "根据客户潜在顾客获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/leads/{lead_id}/listleads/fetchdefault")
	public ResponseEntity<List<ListLeadDTO>> fetchListLeadDefaultByAccountLead(@PathVariable("account_id") String account_id, @PathVariable("lead_id") String lead_id,ListLeadSearchContext context) {
        context.setN_entity2id_eq(lead_id);
        Page<ListLead> domains = listleadService.searchDefault(context) ;
        List<ListLeadDTO> list = listleadMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ListLead-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-ListLead-Get')")
	@ApiOperation(value = "根据客户潜在顾客查询DEFAULT", tags = {"营销列表-潜在客户" } ,notes = "根据客户潜在顾客查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/leads/{lead_id}/listleads/searchdefault")
	public ResponseEntity<Page<ListLeadDTO>> searchListLeadDefaultByAccountLead(@PathVariable("account_id") String account_id, @PathVariable("lead_id") String lead_id, @RequestBody ListLeadSearchContext context) {
        context.setN_entity2id_eq(lead_id);
        Page<ListLead> domains = listleadService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(listleadMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.listleadMapping.toDomain(#listleaddto),'iBizBusinessCentral-ListLead-Create')")
    @ApiOperation(value = "根据市场活动潜在顾客建立营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据市场活动潜在顾客建立营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.POST, value = "/campaigns/{campaign_id}/leads/{lead_id}/listleads")
    public ResponseEntity<ListLeadDTO> createByCampaignLead(@PathVariable("campaign_id") String campaign_id, @PathVariable("lead_id") String lead_id, @RequestBody ListLeadDTO listleaddto) {
        ListLead domain = listleadMapping.toDomain(listleaddto);
        domain.setEntity2id(lead_id);
		listleadService.create(domain);
        ListLeadDTO dto = listleadMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.listleadMapping.toDomain(#listleaddtos),'iBizBusinessCentral-ListLead-Create')")
    @ApiOperation(value = "根据市场活动潜在顾客批量建立营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据市场活动潜在顾客批量建立营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.POST, value = "/campaigns/{campaign_id}/leads/{lead_id}/listleads/batch")
    public ResponseEntity<Boolean> createBatchByCampaignLead(@PathVariable("campaign_id") String campaign_id, @PathVariable("lead_id") String lead_id, @RequestBody List<ListLeadDTO> listleaddtos) {
        List<ListLead> domainlist=listleadMapping.toDomain(listleaddtos);
        for(ListLead domain:domainlist){
            domain.setEntity2id(lead_id);
        }
        listleadService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "listlead" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.listleadService.get(#listlead_id),'iBizBusinessCentral-ListLead-Update')")
    @ApiOperation(value = "根据市场活动潜在顾客更新营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据市场活动潜在顾客更新营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.PUT, value = "/campaigns/{campaign_id}/leads/{lead_id}/listleads/{listlead_id}")
    public ResponseEntity<ListLeadDTO> updateByCampaignLead(@PathVariable("campaign_id") String campaign_id, @PathVariable("lead_id") String lead_id, @PathVariable("listlead_id") String listlead_id, @RequestBody ListLeadDTO listleaddto) {
        ListLead domain = listleadMapping.toDomain(listleaddto);
        domain.setEntity2id(lead_id);
        domain.setRelationshipsid(listlead_id);
		listleadService.update(domain);
        ListLeadDTO dto = listleadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.listleadService.getListleadByEntities(this.listleadMapping.toDomain(#listleaddtos)),'iBizBusinessCentral-ListLead-Update')")
    @ApiOperation(value = "根据市场活动潜在顾客批量更新营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据市场活动潜在顾客批量更新营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.PUT, value = "/campaigns/{campaign_id}/leads/{lead_id}/listleads/batch")
    public ResponseEntity<Boolean> updateBatchByCampaignLead(@PathVariable("campaign_id") String campaign_id, @PathVariable("lead_id") String lead_id, @RequestBody List<ListLeadDTO> listleaddtos) {
        List<ListLead> domainlist=listleadMapping.toDomain(listleaddtos);
        for(ListLead domain:domainlist){
            domain.setEntity2id(lead_id);
        }
        listleadService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.listleadService.get(#listlead_id),'iBizBusinessCentral-ListLead-Remove')")
    @ApiOperation(value = "根据市场活动潜在顾客删除营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据市场活动潜在顾客删除营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.DELETE, value = "/campaigns/{campaign_id}/leads/{lead_id}/listleads/{listlead_id}")
    public ResponseEntity<Boolean> removeByCampaignLead(@PathVariable("campaign_id") String campaign_id, @PathVariable("lead_id") String lead_id, @PathVariable("listlead_id") String listlead_id) {
		return ResponseEntity.status(HttpStatus.OK).body(listleadService.remove(listlead_id));
    }

    @PreAuthorize("hasPermission(this.listleadService.getListleadByIds(#ids),'iBizBusinessCentral-ListLead-Remove')")
    @ApiOperation(value = "根据市场活动潜在顾客批量删除营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据市场活动潜在顾客批量删除营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.DELETE, value = "/campaigns/{campaign_id}/leads/{lead_id}/listleads/batch")
    public ResponseEntity<Boolean> removeBatchByCampaignLead(@RequestBody List<String> ids) {
        listleadService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.listleadMapping.toDomain(returnObject.body),'iBizBusinessCentral-ListLead-Get')")
    @ApiOperation(value = "根据市场活动潜在顾客获取营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据市场活动潜在顾客获取营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.GET, value = "/campaigns/{campaign_id}/leads/{lead_id}/listleads/{listlead_id}")
    public ResponseEntity<ListLeadDTO> getByCampaignLead(@PathVariable("campaign_id") String campaign_id, @PathVariable("lead_id") String lead_id, @PathVariable("listlead_id") String listlead_id) {
        ListLead domain = listleadService.get(listlead_id);
        ListLeadDTO dto = listleadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据市场活动潜在顾客获取营销列表-潜在客户草稿", tags = {"营销列表-潜在客户" },  notes = "根据市场活动潜在顾客获取营销列表-潜在客户草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/campaigns/{campaign_id}/leads/{lead_id}/listleads/getdraft")
    public ResponseEntity<ListLeadDTO> getDraftByCampaignLead(@PathVariable("campaign_id") String campaign_id, @PathVariable("lead_id") String lead_id) {
        ListLead domain = new ListLead();
        domain.setEntity2id(lead_id);
        return ResponseEntity.status(HttpStatus.OK).body(listleadMapping.toDto(listleadService.getDraft(domain)));
    }

    @ApiOperation(value = "根据市场活动潜在顾客检查营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据市场活动潜在顾客检查营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.POST, value = "/campaigns/{campaign_id}/leads/{lead_id}/listleads/checkkey")
    public ResponseEntity<Boolean> checkKeyByCampaignLead(@PathVariable("campaign_id") String campaign_id, @PathVariable("lead_id") String lead_id, @RequestBody ListLeadDTO listleaddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(listleadService.checkKey(listleadMapping.toDomain(listleaddto)));
    }

    @PreAuthorize("hasPermission(this.listleadMapping.toDomain(#listleaddto),'iBizBusinessCentral-ListLead-Save')")
    @ApiOperation(value = "根据市场活动潜在顾客保存营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据市场活动潜在顾客保存营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.POST, value = "/campaigns/{campaign_id}/leads/{lead_id}/listleads/save")
    public ResponseEntity<Boolean> saveByCampaignLead(@PathVariable("campaign_id") String campaign_id, @PathVariable("lead_id") String lead_id, @RequestBody ListLeadDTO listleaddto) {
        ListLead domain = listleadMapping.toDomain(listleaddto);
        domain.setEntity2id(lead_id);
        return ResponseEntity.status(HttpStatus.OK).body(listleadService.save(domain));
    }

    @PreAuthorize("hasPermission(this.listleadMapping.toDomain(#listleaddtos),'iBizBusinessCentral-ListLead-Save')")
    @ApiOperation(value = "根据市场活动潜在顾客批量保存营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据市场活动潜在顾客批量保存营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.POST, value = "/campaigns/{campaign_id}/leads/{lead_id}/listleads/savebatch")
    public ResponseEntity<Boolean> saveBatchByCampaignLead(@PathVariable("campaign_id") String campaign_id, @PathVariable("lead_id") String lead_id, @RequestBody List<ListLeadDTO> listleaddtos) {
        List<ListLead> domainlist=listleadMapping.toDomain(listleaddtos);
        for(ListLead domain:domainlist){
             domain.setEntity2id(lead_id);
        }
        listleadService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ListLead-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-ListLead-Get')")
	@ApiOperation(value = "根据市场活动潜在顾客获取DEFAULT", tags = {"营销列表-潜在客户" } ,notes = "根据市场活动潜在顾客获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/campaigns/{campaign_id}/leads/{lead_id}/listleads/fetchdefault")
	public ResponseEntity<List<ListLeadDTO>> fetchListLeadDefaultByCampaignLead(@PathVariable("campaign_id") String campaign_id, @PathVariable("lead_id") String lead_id,ListLeadSearchContext context) {
        context.setN_entity2id_eq(lead_id);
        Page<ListLead> domains = listleadService.searchDefault(context) ;
        List<ListLeadDTO> list = listleadMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ListLead-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-ListLead-Get')")
	@ApiOperation(value = "根据市场活动潜在顾客查询DEFAULT", tags = {"营销列表-潜在客户" } ,notes = "根据市场活动潜在顾客查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/campaigns/{campaign_id}/leads/{lead_id}/listleads/searchdefault")
	public ResponseEntity<Page<ListLeadDTO>> searchListLeadDefaultByCampaignLead(@PathVariable("campaign_id") String campaign_id, @PathVariable("lead_id") String lead_id, @RequestBody ListLeadSearchContext context) {
        context.setN_entity2id_eq(lead_id);
        Page<ListLead> domains = listleadService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(listleadMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.listleadMapping.toDomain(#listleaddto),'iBizBusinessCentral-ListLead-Create')")
    @ApiOperation(value = "根据联系人潜在顾客建立营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据联系人潜在顾客建立营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/leads/{lead_id}/listleads")
    public ResponseEntity<ListLeadDTO> createByContactLead(@PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @RequestBody ListLeadDTO listleaddto) {
        ListLead domain = listleadMapping.toDomain(listleaddto);
        domain.setEntity2id(lead_id);
		listleadService.create(domain);
        ListLeadDTO dto = listleadMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.listleadMapping.toDomain(#listleaddtos),'iBizBusinessCentral-ListLead-Create')")
    @ApiOperation(value = "根据联系人潜在顾客批量建立营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据联系人潜在顾客批量建立营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/leads/{lead_id}/listleads/batch")
    public ResponseEntity<Boolean> createBatchByContactLead(@PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @RequestBody List<ListLeadDTO> listleaddtos) {
        List<ListLead> domainlist=listleadMapping.toDomain(listleaddtos);
        for(ListLead domain:domainlist){
            domain.setEntity2id(lead_id);
        }
        listleadService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "listlead" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.listleadService.get(#listlead_id),'iBizBusinessCentral-ListLead-Update')")
    @ApiOperation(value = "根据联系人潜在顾客更新营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据联系人潜在顾客更新营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.PUT, value = "/contacts/{contact_id}/leads/{lead_id}/listleads/{listlead_id}")
    public ResponseEntity<ListLeadDTO> updateByContactLead(@PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @PathVariable("listlead_id") String listlead_id, @RequestBody ListLeadDTO listleaddto) {
        ListLead domain = listleadMapping.toDomain(listleaddto);
        domain.setEntity2id(lead_id);
        domain.setRelationshipsid(listlead_id);
		listleadService.update(domain);
        ListLeadDTO dto = listleadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.listleadService.getListleadByEntities(this.listleadMapping.toDomain(#listleaddtos)),'iBizBusinessCentral-ListLead-Update')")
    @ApiOperation(value = "根据联系人潜在顾客批量更新营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据联系人潜在顾客批量更新营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.PUT, value = "/contacts/{contact_id}/leads/{lead_id}/listleads/batch")
    public ResponseEntity<Boolean> updateBatchByContactLead(@PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @RequestBody List<ListLeadDTO> listleaddtos) {
        List<ListLead> domainlist=listleadMapping.toDomain(listleaddtos);
        for(ListLead domain:domainlist){
            domain.setEntity2id(lead_id);
        }
        listleadService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.listleadService.get(#listlead_id),'iBizBusinessCentral-ListLead-Remove')")
    @ApiOperation(value = "根据联系人潜在顾客删除营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据联系人潜在顾客删除营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.DELETE, value = "/contacts/{contact_id}/leads/{lead_id}/listleads/{listlead_id}")
    public ResponseEntity<Boolean> removeByContactLead(@PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @PathVariable("listlead_id") String listlead_id) {
		return ResponseEntity.status(HttpStatus.OK).body(listleadService.remove(listlead_id));
    }

    @PreAuthorize("hasPermission(this.listleadService.getListleadByIds(#ids),'iBizBusinessCentral-ListLead-Remove')")
    @ApiOperation(value = "根据联系人潜在顾客批量删除营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据联系人潜在顾客批量删除营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.DELETE, value = "/contacts/{contact_id}/leads/{lead_id}/listleads/batch")
    public ResponseEntity<Boolean> removeBatchByContactLead(@RequestBody List<String> ids) {
        listleadService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.listleadMapping.toDomain(returnObject.body),'iBizBusinessCentral-ListLead-Get')")
    @ApiOperation(value = "根据联系人潜在顾客获取营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据联系人潜在顾客获取营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.GET, value = "/contacts/{contact_id}/leads/{lead_id}/listleads/{listlead_id}")
    public ResponseEntity<ListLeadDTO> getByContactLead(@PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @PathVariable("listlead_id") String listlead_id) {
        ListLead domain = listleadService.get(listlead_id);
        ListLeadDTO dto = listleadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据联系人潜在顾客获取营销列表-潜在客户草稿", tags = {"营销列表-潜在客户" },  notes = "根据联系人潜在顾客获取营销列表-潜在客户草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/contacts/{contact_id}/leads/{lead_id}/listleads/getdraft")
    public ResponseEntity<ListLeadDTO> getDraftByContactLead(@PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id) {
        ListLead domain = new ListLead();
        domain.setEntity2id(lead_id);
        return ResponseEntity.status(HttpStatus.OK).body(listleadMapping.toDto(listleadService.getDraft(domain)));
    }

    @ApiOperation(value = "根据联系人潜在顾客检查营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据联系人潜在顾客检查营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/leads/{lead_id}/listleads/checkkey")
    public ResponseEntity<Boolean> checkKeyByContactLead(@PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @RequestBody ListLeadDTO listleaddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(listleadService.checkKey(listleadMapping.toDomain(listleaddto)));
    }

    @PreAuthorize("hasPermission(this.listleadMapping.toDomain(#listleaddto),'iBizBusinessCentral-ListLead-Save')")
    @ApiOperation(value = "根据联系人潜在顾客保存营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据联系人潜在顾客保存营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/leads/{lead_id}/listleads/save")
    public ResponseEntity<Boolean> saveByContactLead(@PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @RequestBody ListLeadDTO listleaddto) {
        ListLead domain = listleadMapping.toDomain(listleaddto);
        domain.setEntity2id(lead_id);
        return ResponseEntity.status(HttpStatus.OK).body(listleadService.save(domain));
    }

    @PreAuthorize("hasPermission(this.listleadMapping.toDomain(#listleaddtos),'iBizBusinessCentral-ListLead-Save')")
    @ApiOperation(value = "根据联系人潜在顾客批量保存营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据联系人潜在顾客批量保存营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.POST, value = "/contacts/{contact_id}/leads/{lead_id}/listleads/savebatch")
    public ResponseEntity<Boolean> saveBatchByContactLead(@PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @RequestBody List<ListLeadDTO> listleaddtos) {
        List<ListLead> domainlist=listleadMapping.toDomain(listleaddtos);
        for(ListLead domain:domainlist){
             domain.setEntity2id(lead_id);
        }
        listleadService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ListLead-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-ListLead-Get')")
	@ApiOperation(value = "根据联系人潜在顾客获取DEFAULT", tags = {"营销列表-潜在客户" } ,notes = "根据联系人潜在顾客获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/contacts/{contact_id}/leads/{lead_id}/listleads/fetchdefault")
	public ResponseEntity<List<ListLeadDTO>> fetchListLeadDefaultByContactLead(@PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id,ListLeadSearchContext context) {
        context.setN_entity2id_eq(lead_id);
        Page<ListLead> domains = listleadService.searchDefault(context) ;
        List<ListLeadDTO> list = listleadMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ListLead-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-ListLead-Get')")
	@ApiOperation(value = "根据联系人潜在顾客查询DEFAULT", tags = {"营销列表-潜在客户" } ,notes = "根据联系人潜在顾客查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/contacts/{contact_id}/leads/{lead_id}/listleads/searchdefault")
	public ResponseEntity<Page<ListLeadDTO>> searchListLeadDefaultByContactLead(@PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @RequestBody ListLeadSearchContext context) {
        context.setN_entity2id_eq(lead_id);
        Page<ListLead> domains = listleadService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(listleadMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.listleadMapping.toDomain(#listleaddto),'iBizBusinessCentral-ListLead-Create')")
    @ApiOperation(value = "根据客户联系人潜在顾客建立营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据客户联系人潜在顾客建立营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/leads/{lead_id}/listleads")
    public ResponseEntity<ListLeadDTO> createByAccountContactLead(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @RequestBody ListLeadDTO listleaddto) {
        ListLead domain = listleadMapping.toDomain(listleaddto);
        domain.setEntity2id(lead_id);
		listleadService.create(domain);
        ListLeadDTO dto = listleadMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.listleadMapping.toDomain(#listleaddtos),'iBizBusinessCentral-ListLead-Create')")
    @ApiOperation(value = "根据客户联系人潜在顾客批量建立营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据客户联系人潜在顾客批量建立营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/leads/{lead_id}/listleads/batch")
    public ResponseEntity<Boolean> createBatchByAccountContactLead(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @RequestBody List<ListLeadDTO> listleaddtos) {
        List<ListLead> domainlist=listleadMapping.toDomain(listleaddtos);
        for(ListLead domain:domainlist){
            domain.setEntity2id(lead_id);
        }
        listleadService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "listlead" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.listleadService.get(#listlead_id),'iBizBusinessCentral-ListLead-Update')")
    @ApiOperation(value = "根据客户联系人潜在顾客更新营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据客户联系人潜在顾客更新营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/contacts/{contact_id}/leads/{lead_id}/listleads/{listlead_id}")
    public ResponseEntity<ListLeadDTO> updateByAccountContactLead(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @PathVariable("listlead_id") String listlead_id, @RequestBody ListLeadDTO listleaddto) {
        ListLead domain = listleadMapping.toDomain(listleaddto);
        domain.setEntity2id(lead_id);
        domain.setRelationshipsid(listlead_id);
		listleadService.update(domain);
        ListLeadDTO dto = listleadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.listleadService.getListleadByEntities(this.listleadMapping.toDomain(#listleaddtos)),'iBizBusinessCentral-ListLead-Update')")
    @ApiOperation(value = "根据客户联系人潜在顾客批量更新营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据客户联系人潜在顾客批量更新营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.PUT, value = "/accounts/{account_id}/contacts/{contact_id}/leads/{lead_id}/listleads/batch")
    public ResponseEntity<Boolean> updateBatchByAccountContactLead(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @RequestBody List<ListLeadDTO> listleaddtos) {
        List<ListLead> domainlist=listleadMapping.toDomain(listleaddtos);
        for(ListLead domain:domainlist){
            domain.setEntity2id(lead_id);
        }
        listleadService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.listleadService.get(#listlead_id),'iBizBusinessCentral-ListLead-Remove')")
    @ApiOperation(value = "根据客户联系人潜在顾客删除营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据客户联系人潜在顾客删除营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/contacts/{contact_id}/leads/{lead_id}/listleads/{listlead_id}")
    public ResponseEntity<Boolean> removeByAccountContactLead(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @PathVariable("listlead_id") String listlead_id) {
		return ResponseEntity.status(HttpStatus.OK).body(listleadService.remove(listlead_id));
    }

    @PreAuthorize("hasPermission(this.listleadService.getListleadByIds(#ids),'iBizBusinessCentral-ListLead-Remove')")
    @ApiOperation(value = "根据客户联系人潜在顾客批量删除营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据客户联系人潜在顾客批量删除营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{account_id}/contacts/{contact_id}/leads/{lead_id}/listleads/batch")
    public ResponseEntity<Boolean> removeBatchByAccountContactLead(@RequestBody List<String> ids) {
        listleadService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.listleadMapping.toDomain(returnObject.body),'iBizBusinessCentral-ListLead-Get')")
    @ApiOperation(value = "根据客户联系人潜在顾客获取营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据客户联系人潜在顾客获取营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/contacts/{contact_id}/leads/{lead_id}/listleads/{listlead_id}")
    public ResponseEntity<ListLeadDTO> getByAccountContactLead(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @PathVariable("listlead_id") String listlead_id) {
        ListLead domain = listleadService.get(listlead_id);
        ListLeadDTO dto = listleadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据客户联系人潜在顾客获取营销列表-潜在客户草稿", tags = {"营销列表-潜在客户" },  notes = "根据客户联系人潜在顾客获取营销列表-潜在客户草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/accounts/{account_id}/contacts/{contact_id}/leads/{lead_id}/listleads/getdraft")
    public ResponseEntity<ListLeadDTO> getDraftByAccountContactLead(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id) {
        ListLead domain = new ListLead();
        domain.setEntity2id(lead_id);
        return ResponseEntity.status(HttpStatus.OK).body(listleadMapping.toDto(listleadService.getDraft(domain)));
    }

    @ApiOperation(value = "根据客户联系人潜在顾客检查营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据客户联系人潜在顾客检查营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/leads/{lead_id}/listleads/checkkey")
    public ResponseEntity<Boolean> checkKeyByAccountContactLead(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @RequestBody ListLeadDTO listleaddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(listleadService.checkKey(listleadMapping.toDomain(listleaddto)));
    }

    @PreAuthorize("hasPermission(this.listleadMapping.toDomain(#listleaddto),'iBizBusinessCentral-ListLead-Save')")
    @ApiOperation(value = "根据客户联系人潜在顾客保存营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据客户联系人潜在顾客保存营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/leads/{lead_id}/listleads/save")
    public ResponseEntity<Boolean> saveByAccountContactLead(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @RequestBody ListLeadDTO listleaddto) {
        ListLead domain = listleadMapping.toDomain(listleaddto);
        domain.setEntity2id(lead_id);
        return ResponseEntity.status(HttpStatus.OK).body(listleadService.save(domain));
    }

    @PreAuthorize("hasPermission(this.listleadMapping.toDomain(#listleaddtos),'iBizBusinessCentral-ListLead-Save')")
    @ApiOperation(value = "根据客户联系人潜在顾客批量保存营销列表-潜在客户", tags = {"营销列表-潜在客户" },  notes = "根据客户联系人潜在顾客批量保存营销列表-潜在客户")
	@RequestMapping(method = RequestMethod.POST, value = "/accounts/{account_id}/contacts/{contact_id}/leads/{lead_id}/listleads/savebatch")
    public ResponseEntity<Boolean> saveBatchByAccountContactLead(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @RequestBody List<ListLeadDTO> listleaddtos) {
        List<ListLead> domainlist=listleadMapping.toDomain(listleaddtos);
        for(ListLead domain:domainlist){
             domain.setEntity2id(lead_id);
        }
        listleadService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ListLead-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-ListLead-Get')")
	@ApiOperation(value = "根据客户联系人潜在顾客获取DEFAULT", tags = {"营销列表-潜在客户" } ,notes = "根据客户联系人潜在顾客获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/accounts/{account_id}/contacts/{contact_id}/leads/{lead_id}/listleads/fetchdefault")
	public ResponseEntity<List<ListLeadDTO>> fetchListLeadDefaultByAccountContactLead(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id,ListLeadSearchContext context) {
        context.setN_entity2id_eq(lead_id);
        Page<ListLead> domains = listleadService.searchDefault(context) ;
        List<ListLeadDTO> list = listleadMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-ListLead-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-ListLead-Get')")
	@ApiOperation(value = "根据客户联系人潜在顾客查询DEFAULT", tags = {"营销列表-潜在客户" } ,notes = "根据客户联系人潜在顾客查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/accounts/{account_id}/contacts/{contact_id}/leads/{lead_id}/listleads/searchdefault")
	public ResponseEntity<Page<ListLeadDTO>> searchListLeadDefaultByAccountContactLead(@PathVariable("account_id") String account_id, @PathVariable("contact_id") String contact_id, @PathVariable("lead_id") String lead_id, @RequestBody ListLeadSearchContext context) {
        context.setN_entity2id_eq(lead_id);
        Page<ListLead> domains = listleadService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(listleadMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

