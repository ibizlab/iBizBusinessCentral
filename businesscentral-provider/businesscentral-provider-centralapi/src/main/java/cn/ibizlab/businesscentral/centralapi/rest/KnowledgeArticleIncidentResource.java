package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.service.domain.KnowledgeArticleIncident;
import cn.ibizlab.businesscentral.core.service.service.IKnowledgeArticleIncidentService;
import cn.ibizlab.businesscentral.core.service.filter.KnowledgeArticleIncidentSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"知识文章事件" })
@RestController("CentralApi-knowledgearticleincident")
@RequestMapping("")
public class KnowledgeArticleIncidentResource {

    @Autowired
    public IKnowledgeArticleIncidentService knowledgearticleincidentService;

    @Autowired
    @Lazy
    public KnowledgeArticleIncidentMapping knowledgearticleincidentMapping;

    @PreAuthorize("hasPermission(this.knowledgearticleincidentMapping.toDomain(#knowledgearticleincidentdto),'iBizBusinessCentral-KnowledgeArticleIncident-Create')")
    @ApiOperation(value = "新建知识文章事件", tags = {"知识文章事件" },  notes = "新建知识文章事件")
	@RequestMapping(method = RequestMethod.POST, value = "/knowledgearticleincidents")
    public ResponseEntity<KnowledgeArticleIncidentDTO> create(@RequestBody KnowledgeArticleIncidentDTO knowledgearticleincidentdto) {
        KnowledgeArticleIncident domain = knowledgearticleincidentMapping.toDomain(knowledgearticleincidentdto);
		knowledgearticleincidentService.create(domain);
        KnowledgeArticleIncidentDTO dto = knowledgearticleincidentMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.knowledgearticleincidentMapping.toDomain(#knowledgearticleincidentdtos),'iBizBusinessCentral-KnowledgeArticleIncident-Create')")
    @ApiOperation(value = "批量新建知识文章事件", tags = {"知识文章事件" },  notes = "批量新建知识文章事件")
	@RequestMapping(method = RequestMethod.POST, value = "/knowledgearticleincidents/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<KnowledgeArticleIncidentDTO> knowledgearticleincidentdtos) {
        knowledgearticleincidentService.createBatch(knowledgearticleincidentMapping.toDomain(knowledgearticleincidentdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "knowledgearticleincident" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.knowledgearticleincidentService.get(#knowledgearticleincident_id),'iBizBusinessCentral-KnowledgeArticleIncident-Update')")
    @ApiOperation(value = "更新知识文章事件", tags = {"知识文章事件" },  notes = "更新知识文章事件")
	@RequestMapping(method = RequestMethod.PUT, value = "/knowledgearticleincidents/{knowledgearticleincident_id}")
    public ResponseEntity<KnowledgeArticleIncidentDTO> update(@PathVariable("knowledgearticleincident_id") String knowledgearticleincident_id, @RequestBody KnowledgeArticleIncidentDTO knowledgearticleincidentdto) {
		KnowledgeArticleIncident domain  = knowledgearticleincidentMapping.toDomain(knowledgearticleincidentdto);
        domain .setKnowledgearticleincidentid(knowledgearticleincident_id);
		knowledgearticleincidentService.update(domain );
		KnowledgeArticleIncidentDTO dto = knowledgearticleincidentMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.knowledgearticleincidentService.getKnowledgearticleincidentByEntities(this.knowledgearticleincidentMapping.toDomain(#knowledgearticleincidentdtos)),'iBizBusinessCentral-KnowledgeArticleIncident-Update')")
    @ApiOperation(value = "批量更新知识文章事件", tags = {"知识文章事件" },  notes = "批量更新知识文章事件")
	@RequestMapping(method = RequestMethod.PUT, value = "/knowledgearticleincidents/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<KnowledgeArticleIncidentDTO> knowledgearticleincidentdtos) {
        knowledgearticleincidentService.updateBatch(knowledgearticleincidentMapping.toDomain(knowledgearticleincidentdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.knowledgearticleincidentService.get(#knowledgearticleincident_id),'iBizBusinessCentral-KnowledgeArticleIncident-Remove')")
    @ApiOperation(value = "删除知识文章事件", tags = {"知识文章事件" },  notes = "删除知识文章事件")
	@RequestMapping(method = RequestMethod.DELETE, value = "/knowledgearticleincidents/{knowledgearticleincident_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("knowledgearticleincident_id") String knowledgearticleincident_id) {
         return ResponseEntity.status(HttpStatus.OK).body(knowledgearticleincidentService.remove(knowledgearticleincident_id));
    }

    @PreAuthorize("hasPermission(this.knowledgearticleincidentService.getKnowledgearticleincidentByIds(#ids),'iBizBusinessCentral-KnowledgeArticleIncident-Remove')")
    @ApiOperation(value = "批量删除知识文章事件", tags = {"知识文章事件" },  notes = "批量删除知识文章事件")
	@RequestMapping(method = RequestMethod.DELETE, value = "/knowledgearticleincidents/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        knowledgearticleincidentService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.knowledgearticleincidentMapping.toDomain(returnObject.body),'iBizBusinessCentral-KnowledgeArticleIncident-Get')")
    @ApiOperation(value = "获取知识文章事件", tags = {"知识文章事件" },  notes = "获取知识文章事件")
	@RequestMapping(method = RequestMethod.GET, value = "/knowledgearticleincidents/{knowledgearticleincident_id}")
    public ResponseEntity<KnowledgeArticleIncidentDTO> get(@PathVariable("knowledgearticleincident_id") String knowledgearticleincident_id) {
        KnowledgeArticleIncident domain = knowledgearticleincidentService.get(knowledgearticleincident_id);
        KnowledgeArticleIncidentDTO dto = knowledgearticleincidentMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取知识文章事件草稿", tags = {"知识文章事件" },  notes = "获取知识文章事件草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/knowledgearticleincidents/getdraft")
    public ResponseEntity<KnowledgeArticleIncidentDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(knowledgearticleincidentMapping.toDto(knowledgearticleincidentService.getDraft(new KnowledgeArticleIncident())));
    }

    @ApiOperation(value = "检查知识文章事件", tags = {"知识文章事件" },  notes = "检查知识文章事件")
	@RequestMapping(method = RequestMethod.POST, value = "/knowledgearticleincidents/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody KnowledgeArticleIncidentDTO knowledgearticleincidentdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(knowledgearticleincidentService.checkKey(knowledgearticleincidentMapping.toDomain(knowledgearticleincidentdto)));
    }

    @PreAuthorize("hasPermission(this.knowledgearticleincidentMapping.toDomain(#knowledgearticleincidentdto),'iBizBusinessCentral-KnowledgeArticleIncident-Save')")
    @ApiOperation(value = "保存知识文章事件", tags = {"知识文章事件" },  notes = "保存知识文章事件")
	@RequestMapping(method = RequestMethod.POST, value = "/knowledgearticleincidents/save")
    public ResponseEntity<Boolean> save(@RequestBody KnowledgeArticleIncidentDTO knowledgearticleincidentdto) {
        return ResponseEntity.status(HttpStatus.OK).body(knowledgearticleincidentService.save(knowledgearticleincidentMapping.toDomain(knowledgearticleincidentdto)));
    }

    @PreAuthorize("hasPermission(this.knowledgearticleincidentMapping.toDomain(#knowledgearticleincidentdtos),'iBizBusinessCentral-KnowledgeArticleIncident-Save')")
    @ApiOperation(value = "批量保存知识文章事件", tags = {"知识文章事件" },  notes = "批量保存知识文章事件")
	@RequestMapping(method = RequestMethod.POST, value = "/knowledgearticleincidents/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<KnowledgeArticleIncidentDTO> knowledgearticleincidentdtos) {
        knowledgearticleincidentService.saveBatch(knowledgearticleincidentMapping.toDomain(knowledgearticleincidentdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-KnowledgeArticleIncident-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-KnowledgeArticleIncident-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"知识文章事件" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/knowledgearticleincidents/fetchdefault")
	public ResponseEntity<List<KnowledgeArticleIncidentDTO>> fetchDefault(KnowledgeArticleIncidentSearchContext context) {
        Page<KnowledgeArticleIncident> domains = knowledgearticleincidentService.searchDefault(context) ;
        List<KnowledgeArticleIncidentDTO> list = knowledgearticleincidentMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-KnowledgeArticleIncident-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-KnowledgeArticleIncident-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"知识文章事件" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/knowledgearticleincidents/searchdefault")
	public ResponseEntity<Page<KnowledgeArticleIncidentDTO>> searchDefault(@RequestBody KnowledgeArticleIncidentSearchContext context) {
        Page<KnowledgeArticleIncident> domains = knowledgearticleincidentService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(knowledgearticleincidentMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.knowledgearticleincidentMapping.toDomain(#knowledgearticleincidentdto),'iBizBusinessCentral-KnowledgeArticleIncident-Create')")
    @ApiOperation(value = "根据知识文章建立知识文章事件", tags = {"知识文章事件" },  notes = "根据知识文章建立知识文章事件")
	@RequestMapping(method = RequestMethod.POST, value = "/knowledgearticles/{knowledgearticle_id}/knowledgearticleincidents")
    public ResponseEntity<KnowledgeArticleIncidentDTO> createByKnowledgeArticle(@PathVariable("knowledgearticle_id") String knowledgearticle_id, @RequestBody KnowledgeArticleIncidentDTO knowledgearticleincidentdto) {
        KnowledgeArticleIncident domain = knowledgearticleincidentMapping.toDomain(knowledgearticleincidentdto);
        domain.setKnowledgearticleid(knowledgearticle_id);
		knowledgearticleincidentService.create(domain);
        KnowledgeArticleIncidentDTO dto = knowledgearticleincidentMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.knowledgearticleincidentMapping.toDomain(#knowledgearticleincidentdtos),'iBizBusinessCentral-KnowledgeArticleIncident-Create')")
    @ApiOperation(value = "根据知识文章批量建立知识文章事件", tags = {"知识文章事件" },  notes = "根据知识文章批量建立知识文章事件")
	@RequestMapping(method = RequestMethod.POST, value = "/knowledgearticles/{knowledgearticle_id}/knowledgearticleincidents/batch")
    public ResponseEntity<Boolean> createBatchByKnowledgeArticle(@PathVariable("knowledgearticle_id") String knowledgearticle_id, @RequestBody List<KnowledgeArticleIncidentDTO> knowledgearticleincidentdtos) {
        List<KnowledgeArticleIncident> domainlist=knowledgearticleincidentMapping.toDomain(knowledgearticleincidentdtos);
        for(KnowledgeArticleIncident domain:domainlist){
            domain.setKnowledgearticleid(knowledgearticle_id);
        }
        knowledgearticleincidentService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "knowledgearticleincident" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.knowledgearticleincidentService.get(#knowledgearticleincident_id),'iBizBusinessCentral-KnowledgeArticleIncident-Update')")
    @ApiOperation(value = "根据知识文章更新知识文章事件", tags = {"知识文章事件" },  notes = "根据知识文章更新知识文章事件")
	@RequestMapping(method = RequestMethod.PUT, value = "/knowledgearticles/{knowledgearticle_id}/knowledgearticleincidents/{knowledgearticleincident_id}")
    public ResponseEntity<KnowledgeArticleIncidentDTO> updateByKnowledgeArticle(@PathVariable("knowledgearticle_id") String knowledgearticle_id, @PathVariable("knowledgearticleincident_id") String knowledgearticleincident_id, @RequestBody KnowledgeArticleIncidentDTO knowledgearticleincidentdto) {
        KnowledgeArticleIncident domain = knowledgearticleincidentMapping.toDomain(knowledgearticleincidentdto);
        domain.setKnowledgearticleid(knowledgearticle_id);
        domain.setKnowledgearticleincidentid(knowledgearticleincident_id);
		knowledgearticleincidentService.update(domain);
        KnowledgeArticleIncidentDTO dto = knowledgearticleincidentMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.knowledgearticleincidentService.getKnowledgearticleincidentByEntities(this.knowledgearticleincidentMapping.toDomain(#knowledgearticleincidentdtos)),'iBizBusinessCentral-KnowledgeArticleIncident-Update')")
    @ApiOperation(value = "根据知识文章批量更新知识文章事件", tags = {"知识文章事件" },  notes = "根据知识文章批量更新知识文章事件")
	@RequestMapping(method = RequestMethod.PUT, value = "/knowledgearticles/{knowledgearticle_id}/knowledgearticleincidents/batch")
    public ResponseEntity<Boolean> updateBatchByKnowledgeArticle(@PathVariable("knowledgearticle_id") String knowledgearticle_id, @RequestBody List<KnowledgeArticleIncidentDTO> knowledgearticleincidentdtos) {
        List<KnowledgeArticleIncident> domainlist=knowledgearticleincidentMapping.toDomain(knowledgearticleincidentdtos);
        for(KnowledgeArticleIncident domain:domainlist){
            domain.setKnowledgearticleid(knowledgearticle_id);
        }
        knowledgearticleincidentService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.knowledgearticleincidentService.get(#knowledgearticleincident_id),'iBizBusinessCentral-KnowledgeArticleIncident-Remove')")
    @ApiOperation(value = "根据知识文章删除知识文章事件", tags = {"知识文章事件" },  notes = "根据知识文章删除知识文章事件")
	@RequestMapping(method = RequestMethod.DELETE, value = "/knowledgearticles/{knowledgearticle_id}/knowledgearticleincidents/{knowledgearticleincident_id}")
    public ResponseEntity<Boolean> removeByKnowledgeArticle(@PathVariable("knowledgearticle_id") String knowledgearticle_id, @PathVariable("knowledgearticleincident_id") String knowledgearticleincident_id) {
		return ResponseEntity.status(HttpStatus.OK).body(knowledgearticleincidentService.remove(knowledgearticleincident_id));
    }

    @PreAuthorize("hasPermission(this.knowledgearticleincidentService.getKnowledgearticleincidentByIds(#ids),'iBizBusinessCentral-KnowledgeArticleIncident-Remove')")
    @ApiOperation(value = "根据知识文章批量删除知识文章事件", tags = {"知识文章事件" },  notes = "根据知识文章批量删除知识文章事件")
	@RequestMapping(method = RequestMethod.DELETE, value = "/knowledgearticles/{knowledgearticle_id}/knowledgearticleincidents/batch")
    public ResponseEntity<Boolean> removeBatchByKnowledgeArticle(@RequestBody List<String> ids) {
        knowledgearticleincidentService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.knowledgearticleincidentMapping.toDomain(returnObject.body),'iBizBusinessCentral-KnowledgeArticleIncident-Get')")
    @ApiOperation(value = "根据知识文章获取知识文章事件", tags = {"知识文章事件" },  notes = "根据知识文章获取知识文章事件")
	@RequestMapping(method = RequestMethod.GET, value = "/knowledgearticles/{knowledgearticle_id}/knowledgearticleincidents/{knowledgearticleincident_id}")
    public ResponseEntity<KnowledgeArticleIncidentDTO> getByKnowledgeArticle(@PathVariable("knowledgearticle_id") String knowledgearticle_id, @PathVariable("knowledgearticleincident_id") String knowledgearticleincident_id) {
        KnowledgeArticleIncident domain = knowledgearticleincidentService.get(knowledgearticleincident_id);
        KnowledgeArticleIncidentDTO dto = knowledgearticleincidentMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据知识文章获取知识文章事件草稿", tags = {"知识文章事件" },  notes = "根据知识文章获取知识文章事件草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/knowledgearticles/{knowledgearticle_id}/knowledgearticleincidents/getdraft")
    public ResponseEntity<KnowledgeArticleIncidentDTO> getDraftByKnowledgeArticle(@PathVariable("knowledgearticle_id") String knowledgearticle_id) {
        KnowledgeArticleIncident domain = new KnowledgeArticleIncident();
        domain.setKnowledgearticleid(knowledgearticle_id);
        return ResponseEntity.status(HttpStatus.OK).body(knowledgearticleincidentMapping.toDto(knowledgearticleincidentService.getDraft(domain)));
    }

    @ApiOperation(value = "根据知识文章检查知识文章事件", tags = {"知识文章事件" },  notes = "根据知识文章检查知识文章事件")
	@RequestMapping(method = RequestMethod.POST, value = "/knowledgearticles/{knowledgearticle_id}/knowledgearticleincidents/checkkey")
    public ResponseEntity<Boolean> checkKeyByKnowledgeArticle(@PathVariable("knowledgearticle_id") String knowledgearticle_id, @RequestBody KnowledgeArticleIncidentDTO knowledgearticleincidentdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(knowledgearticleincidentService.checkKey(knowledgearticleincidentMapping.toDomain(knowledgearticleincidentdto)));
    }

    @PreAuthorize("hasPermission(this.knowledgearticleincidentMapping.toDomain(#knowledgearticleincidentdto),'iBizBusinessCentral-KnowledgeArticleIncident-Save')")
    @ApiOperation(value = "根据知识文章保存知识文章事件", tags = {"知识文章事件" },  notes = "根据知识文章保存知识文章事件")
	@RequestMapping(method = RequestMethod.POST, value = "/knowledgearticles/{knowledgearticle_id}/knowledgearticleincidents/save")
    public ResponseEntity<Boolean> saveByKnowledgeArticle(@PathVariable("knowledgearticle_id") String knowledgearticle_id, @RequestBody KnowledgeArticleIncidentDTO knowledgearticleincidentdto) {
        KnowledgeArticleIncident domain = knowledgearticleincidentMapping.toDomain(knowledgearticleincidentdto);
        domain.setKnowledgearticleid(knowledgearticle_id);
        return ResponseEntity.status(HttpStatus.OK).body(knowledgearticleincidentService.save(domain));
    }

    @PreAuthorize("hasPermission(this.knowledgearticleincidentMapping.toDomain(#knowledgearticleincidentdtos),'iBizBusinessCentral-KnowledgeArticleIncident-Save')")
    @ApiOperation(value = "根据知识文章批量保存知识文章事件", tags = {"知识文章事件" },  notes = "根据知识文章批量保存知识文章事件")
	@RequestMapping(method = RequestMethod.POST, value = "/knowledgearticles/{knowledgearticle_id}/knowledgearticleincidents/savebatch")
    public ResponseEntity<Boolean> saveBatchByKnowledgeArticle(@PathVariable("knowledgearticle_id") String knowledgearticle_id, @RequestBody List<KnowledgeArticleIncidentDTO> knowledgearticleincidentdtos) {
        List<KnowledgeArticleIncident> domainlist=knowledgearticleincidentMapping.toDomain(knowledgearticleincidentdtos);
        for(KnowledgeArticleIncident domain:domainlist){
             domain.setKnowledgearticleid(knowledgearticle_id);
        }
        knowledgearticleincidentService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-KnowledgeArticleIncident-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-KnowledgeArticleIncident-Get')")
	@ApiOperation(value = "根据知识文章获取DEFAULT", tags = {"知识文章事件" } ,notes = "根据知识文章获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/knowledgearticles/{knowledgearticle_id}/knowledgearticleincidents/fetchdefault")
	public ResponseEntity<List<KnowledgeArticleIncidentDTO>> fetchKnowledgeArticleIncidentDefaultByKnowledgeArticle(@PathVariable("knowledgearticle_id") String knowledgearticle_id,KnowledgeArticleIncidentSearchContext context) {
        context.setN_knowledgearticleid_eq(knowledgearticle_id);
        Page<KnowledgeArticleIncident> domains = knowledgearticleincidentService.searchDefault(context) ;
        List<KnowledgeArticleIncidentDTO> list = knowledgearticleincidentMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-KnowledgeArticleIncident-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-KnowledgeArticleIncident-Get')")
	@ApiOperation(value = "根据知识文章查询DEFAULT", tags = {"知识文章事件" } ,notes = "根据知识文章查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/knowledgearticles/{knowledgearticle_id}/knowledgearticleincidents/searchdefault")
	public ResponseEntity<Page<KnowledgeArticleIncidentDTO>> searchKnowledgeArticleIncidentDefaultByKnowledgeArticle(@PathVariable("knowledgearticle_id") String knowledgearticle_id, @RequestBody KnowledgeArticleIncidentSearchContext context) {
        context.setN_knowledgearticleid_eq(knowledgearticle_id);
        Page<KnowledgeArticleIncident> domains = knowledgearticleincidentService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(knowledgearticleincidentMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

