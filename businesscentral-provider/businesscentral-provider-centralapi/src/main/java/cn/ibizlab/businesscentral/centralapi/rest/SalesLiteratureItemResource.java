package cn.ibizlab.businesscentral.centralapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.centralapi.dto.*;
import cn.ibizlab.businesscentral.centralapi.mapping.*;
import cn.ibizlab.businesscentral.core.sales.domain.SalesLiteratureItem;
import cn.ibizlab.businesscentral.core.sales.service.ISalesLiteratureItemService;
import cn.ibizlab.businesscentral.core.sales.filter.SalesLiteratureItemSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"销售附件" })
@RestController("CentralApi-salesliteratureitem")
@RequestMapping("")
public class SalesLiteratureItemResource {

    @Autowired
    public ISalesLiteratureItemService salesliteratureitemService;

    @Autowired
    @Lazy
    public SalesLiteratureItemMapping salesliteratureitemMapping;

    @PreAuthorize("hasPermission(this.salesliteratureitemMapping.toDomain(#salesliteratureitemdto),'iBizBusinessCentral-SalesLiteratureItem-Create')")
    @ApiOperation(value = "新建销售附件", tags = {"销售附件" },  notes = "新建销售附件")
	@RequestMapping(method = RequestMethod.POST, value = "/salesliteratureitems")
    public ResponseEntity<SalesLiteratureItemDTO> create(@RequestBody SalesLiteratureItemDTO salesliteratureitemdto) {
        SalesLiteratureItem domain = salesliteratureitemMapping.toDomain(salesliteratureitemdto);
		salesliteratureitemService.create(domain);
        SalesLiteratureItemDTO dto = salesliteratureitemMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.salesliteratureitemMapping.toDomain(#salesliteratureitemdtos),'iBizBusinessCentral-SalesLiteratureItem-Create')")
    @ApiOperation(value = "批量新建销售附件", tags = {"销售附件" },  notes = "批量新建销售附件")
	@RequestMapping(method = RequestMethod.POST, value = "/salesliteratureitems/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<SalesLiteratureItemDTO> salesliteratureitemdtos) {
        salesliteratureitemService.createBatch(salesliteratureitemMapping.toDomain(salesliteratureitemdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "salesliteratureitem" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.salesliteratureitemService.get(#salesliteratureitem_id),'iBizBusinessCentral-SalesLiteratureItem-Update')")
    @ApiOperation(value = "更新销售附件", tags = {"销售附件" },  notes = "更新销售附件")
	@RequestMapping(method = RequestMethod.PUT, value = "/salesliteratureitems/{salesliteratureitem_id}")
    public ResponseEntity<SalesLiteratureItemDTO> update(@PathVariable("salesliteratureitem_id") String salesliteratureitem_id, @RequestBody SalesLiteratureItemDTO salesliteratureitemdto) {
		SalesLiteratureItem domain  = salesliteratureitemMapping.toDomain(salesliteratureitemdto);
        domain .setSalesliteratureitemid(salesliteratureitem_id);
		salesliteratureitemService.update(domain );
		SalesLiteratureItemDTO dto = salesliteratureitemMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.salesliteratureitemService.getSalesliteratureitemByEntities(this.salesliteratureitemMapping.toDomain(#salesliteratureitemdtos)),'iBizBusinessCentral-SalesLiteratureItem-Update')")
    @ApiOperation(value = "批量更新销售附件", tags = {"销售附件" },  notes = "批量更新销售附件")
	@RequestMapping(method = RequestMethod.PUT, value = "/salesliteratureitems/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<SalesLiteratureItemDTO> salesliteratureitemdtos) {
        salesliteratureitemService.updateBatch(salesliteratureitemMapping.toDomain(salesliteratureitemdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.salesliteratureitemService.get(#salesliteratureitem_id),'iBizBusinessCentral-SalesLiteratureItem-Remove')")
    @ApiOperation(value = "删除销售附件", tags = {"销售附件" },  notes = "删除销售附件")
	@RequestMapping(method = RequestMethod.DELETE, value = "/salesliteratureitems/{salesliteratureitem_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("salesliteratureitem_id") String salesliteratureitem_id) {
         return ResponseEntity.status(HttpStatus.OK).body(salesliteratureitemService.remove(salesliteratureitem_id));
    }

    @PreAuthorize("hasPermission(this.salesliteratureitemService.getSalesliteratureitemByIds(#ids),'iBizBusinessCentral-SalesLiteratureItem-Remove')")
    @ApiOperation(value = "批量删除销售附件", tags = {"销售附件" },  notes = "批量删除销售附件")
	@RequestMapping(method = RequestMethod.DELETE, value = "/salesliteratureitems/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        salesliteratureitemService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.salesliteratureitemMapping.toDomain(returnObject.body),'iBizBusinessCentral-SalesLiteratureItem-Get')")
    @ApiOperation(value = "获取销售附件", tags = {"销售附件" },  notes = "获取销售附件")
	@RequestMapping(method = RequestMethod.GET, value = "/salesliteratureitems/{salesliteratureitem_id}")
    public ResponseEntity<SalesLiteratureItemDTO> get(@PathVariable("salesliteratureitem_id") String salesliteratureitem_id) {
        SalesLiteratureItem domain = salesliteratureitemService.get(salesliteratureitem_id);
        SalesLiteratureItemDTO dto = salesliteratureitemMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取销售附件草稿", tags = {"销售附件" },  notes = "获取销售附件草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/salesliteratureitems/getdraft")
    public ResponseEntity<SalesLiteratureItemDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(salesliteratureitemMapping.toDto(salesliteratureitemService.getDraft(new SalesLiteratureItem())));
    }

    @ApiOperation(value = "检查销售附件", tags = {"销售附件" },  notes = "检查销售附件")
	@RequestMapping(method = RequestMethod.POST, value = "/salesliteratureitems/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody SalesLiteratureItemDTO salesliteratureitemdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(salesliteratureitemService.checkKey(salesliteratureitemMapping.toDomain(salesliteratureitemdto)));
    }

    @PreAuthorize("hasPermission(this.salesliteratureitemMapping.toDomain(#salesliteratureitemdto),'iBizBusinessCentral-SalesLiteratureItem-Save')")
    @ApiOperation(value = "保存销售附件", tags = {"销售附件" },  notes = "保存销售附件")
	@RequestMapping(method = RequestMethod.POST, value = "/salesliteratureitems/save")
    public ResponseEntity<Boolean> save(@RequestBody SalesLiteratureItemDTO salesliteratureitemdto) {
        return ResponseEntity.status(HttpStatus.OK).body(salesliteratureitemService.save(salesliteratureitemMapping.toDomain(salesliteratureitemdto)));
    }

    @PreAuthorize("hasPermission(this.salesliteratureitemMapping.toDomain(#salesliteratureitemdtos),'iBizBusinessCentral-SalesLiteratureItem-Save')")
    @ApiOperation(value = "批量保存销售附件", tags = {"销售附件" },  notes = "批量保存销售附件")
	@RequestMapping(method = RequestMethod.POST, value = "/salesliteratureitems/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<SalesLiteratureItemDTO> salesliteratureitemdtos) {
        salesliteratureitemService.saveBatch(salesliteratureitemMapping.toDomain(salesliteratureitemdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesLiteratureItem-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-SalesLiteratureItem-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"销售附件" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/salesliteratureitems/fetchdefault")
	public ResponseEntity<List<SalesLiteratureItemDTO>> fetchDefault(SalesLiteratureItemSearchContext context) {
        Page<SalesLiteratureItem> domains = salesliteratureitemService.searchDefault(context) ;
        List<SalesLiteratureItemDTO> list = salesliteratureitemMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesLiteratureItem-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-SalesLiteratureItem-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"销售附件" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/salesliteratureitems/searchdefault")
	public ResponseEntity<Page<SalesLiteratureItemDTO>> searchDefault(@RequestBody SalesLiteratureItemSearchContext context) {
        Page<SalesLiteratureItem> domains = salesliteratureitemService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(salesliteratureitemMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.salesliteratureitemMapping.toDomain(#salesliteratureitemdto),'iBizBusinessCentral-SalesLiteratureItem-Create')")
    @ApiOperation(value = "根据销售宣传资料建立销售附件", tags = {"销售附件" },  notes = "根据销售宣传资料建立销售附件")
	@RequestMapping(method = RequestMethod.POST, value = "/salesliteratures/{salesliterature_id}/salesliteratureitems")
    public ResponseEntity<SalesLiteratureItemDTO> createBySalesLiterature(@PathVariable("salesliterature_id") String salesliterature_id, @RequestBody SalesLiteratureItemDTO salesliteratureitemdto) {
        SalesLiteratureItem domain = salesliteratureitemMapping.toDomain(salesliteratureitemdto);
        domain.setSalesliteratureid(salesliterature_id);
		salesliteratureitemService.create(domain);
        SalesLiteratureItemDTO dto = salesliteratureitemMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.salesliteratureitemMapping.toDomain(#salesliteratureitemdtos),'iBizBusinessCentral-SalesLiteratureItem-Create')")
    @ApiOperation(value = "根据销售宣传资料批量建立销售附件", tags = {"销售附件" },  notes = "根据销售宣传资料批量建立销售附件")
	@RequestMapping(method = RequestMethod.POST, value = "/salesliteratures/{salesliterature_id}/salesliteratureitems/batch")
    public ResponseEntity<Boolean> createBatchBySalesLiterature(@PathVariable("salesliterature_id") String salesliterature_id, @RequestBody List<SalesLiteratureItemDTO> salesliteratureitemdtos) {
        List<SalesLiteratureItem> domainlist=salesliteratureitemMapping.toDomain(salesliteratureitemdtos);
        for(SalesLiteratureItem domain:domainlist){
            domain.setSalesliteratureid(salesliterature_id);
        }
        salesliteratureitemService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "salesliteratureitem" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.salesliteratureitemService.get(#salesliteratureitem_id),'iBizBusinessCentral-SalesLiteratureItem-Update')")
    @ApiOperation(value = "根据销售宣传资料更新销售附件", tags = {"销售附件" },  notes = "根据销售宣传资料更新销售附件")
	@RequestMapping(method = RequestMethod.PUT, value = "/salesliteratures/{salesliterature_id}/salesliteratureitems/{salesliteratureitem_id}")
    public ResponseEntity<SalesLiteratureItemDTO> updateBySalesLiterature(@PathVariable("salesliterature_id") String salesliterature_id, @PathVariable("salesliteratureitem_id") String salesliteratureitem_id, @RequestBody SalesLiteratureItemDTO salesliteratureitemdto) {
        SalesLiteratureItem domain = salesliteratureitemMapping.toDomain(salesliteratureitemdto);
        domain.setSalesliteratureid(salesliterature_id);
        domain.setSalesliteratureitemid(salesliteratureitem_id);
		salesliteratureitemService.update(domain);
        SalesLiteratureItemDTO dto = salesliteratureitemMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.salesliteratureitemService.getSalesliteratureitemByEntities(this.salesliteratureitemMapping.toDomain(#salesliteratureitemdtos)),'iBizBusinessCentral-SalesLiteratureItem-Update')")
    @ApiOperation(value = "根据销售宣传资料批量更新销售附件", tags = {"销售附件" },  notes = "根据销售宣传资料批量更新销售附件")
	@RequestMapping(method = RequestMethod.PUT, value = "/salesliteratures/{salesliterature_id}/salesliteratureitems/batch")
    public ResponseEntity<Boolean> updateBatchBySalesLiterature(@PathVariable("salesliterature_id") String salesliterature_id, @RequestBody List<SalesLiteratureItemDTO> salesliteratureitemdtos) {
        List<SalesLiteratureItem> domainlist=salesliteratureitemMapping.toDomain(salesliteratureitemdtos);
        for(SalesLiteratureItem domain:domainlist){
            domain.setSalesliteratureid(salesliterature_id);
        }
        salesliteratureitemService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.salesliteratureitemService.get(#salesliteratureitem_id),'iBizBusinessCentral-SalesLiteratureItem-Remove')")
    @ApiOperation(value = "根据销售宣传资料删除销售附件", tags = {"销售附件" },  notes = "根据销售宣传资料删除销售附件")
	@RequestMapping(method = RequestMethod.DELETE, value = "/salesliteratures/{salesliterature_id}/salesliteratureitems/{salesliteratureitem_id}")
    public ResponseEntity<Boolean> removeBySalesLiterature(@PathVariable("salesliterature_id") String salesliterature_id, @PathVariable("salesliteratureitem_id") String salesliteratureitem_id) {
		return ResponseEntity.status(HttpStatus.OK).body(salesliteratureitemService.remove(salesliteratureitem_id));
    }

    @PreAuthorize("hasPermission(this.salesliteratureitemService.getSalesliteratureitemByIds(#ids),'iBizBusinessCentral-SalesLiteratureItem-Remove')")
    @ApiOperation(value = "根据销售宣传资料批量删除销售附件", tags = {"销售附件" },  notes = "根据销售宣传资料批量删除销售附件")
	@RequestMapping(method = RequestMethod.DELETE, value = "/salesliteratures/{salesliterature_id}/salesliteratureitems/batch")
    public ResponseEntity<Boolean> removeBatchBySalesLiterature(@RequestBody List<String> ids) {
        salesliteratureitemService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.salesliteratureitemMapping.toDomain(returnObject.body),'iBizBusinessCentral-SalesLiteratureItem-Get')")
    @ApiOperation(value = "根据销售宣传资料获取销售附件", tags = {"销售附件" },  notes = "根据销售宣传资料获取销售附件")
	@RequestMapping(method = RequestMethod.GET, value = "/salesliteratures/{salesliterature_id}/salesliteratureitems/{salesliteratureitem_id}")
    public ResponseEntity<SalesLiteratureItemDTO> getBySalesLiterature(@PathVariable("salesliterature_id") String salesliterature_id, @PathVariable("salesliteratureitem_id") String salesliteratureitem_id) {
        SalesLiteratureItem domain = salesliteratureitemService.get(salesliteratureitem_id);
        SalesLiteratureItemDTO dto = salesliteratureitemMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据销售宣传资料获取销售附件草稿", tags = {"销售附件" },  notes = "根据销售宣传资料获取销售附件草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/salesliteratures/{salesliterature_id}/salesliteratureitems/getdraft")
    public ResponseEntity<SalesLiteratureItemDTO> getDraftBySalesLiterature(@PathVariable("salesliterature_id") String salesliterature_id) {
        SalesLiteratureItem domain = new SalesLiteratureItem();
        domain.setSalesliteratureid(salesliterature_id);
        return ResponseEntity.status(HttpStatus.OK).body(salesliteratureitemMapping.toDto(salesliteratureitemService.getDraft(domain)));
    }

    @ApiOperation(value = "根据销售宣传资料检查销售附件", tags = {"销售附件" },  notes = "根据销售宣传资料检查销售附件")
	@RequestMapping(method = RequestMethod.POST, value = "/salesliteratures/{salesliterature_id}/salesliteratureitems/checkkey")
    public ResponseEntity<Boolean> checkKeyBySalesLiterature(@PathVariable("salesliterature_id") String salesliterature_id, @RequestBody SalesLiteratureItemDTO salesliteratureitemdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(salesliteratureitemService.checkKey(salesliteratureitemMapping.toDomain(salesliteratureitemdto)));
    }

    @PreAuthorize("hasPermission(this.salesliteratureitemMapping.toDomain(#salesliteratureitemdto),'iBizBusinessCentral-SalesLiteratureItem-Save')")
    @ApiOperation(value = "根据销售宣传资料保存销售附件", tags = {"销售附件" },  notes = "根据销售宣传资料保存销售附件")
	@RequestMapping(method = RequestMethod.POST, value = "/salesliteratures/{salesliterature_id}/salesliteratureitems/save")
    public ResponseEntity<Boolean> saveBySalesLiterature(@PathVariable("salesliterature_id") String salesliterature_id, @RequestBody SalesLiteratureItemDTO salesliteratureitemdto) {
        SalesLiteratureItem domain = salesliteratureitemMapping.toDomain(salesliteratureitemdto);
        domain.setSalesliteratureid(salesliterature_id);
        return ResponseEntity.status(HttpStatus.OK).body(salesliteratureitemService.save(domain));
    }

    @PreAuthorize("hasPermission(this.salesliteratureitemMapping.toDomain(#salesliteratureitemdtos),'iBizBusinessCentral-SalesLiteratureItem-Save')")
    @ApiOperation(value = "根据销售宣传资料批量保存销售附件", tags = {"销售附件" },  notes = "根据销售宣传资料批量保存销售附件")
	@RequestMapping(method = RequestMethod.POST, value = "/salesliteratures/{salesliterature_id}/salesliteratureitems/savebatch")
    public ResponseEntity<Boolean> saveBatchBySalesLiterature(@PathVariable("salesliterature_id") String salesliterature_id, @RequestBody List<SalesLiteratureItemDTO> salesliteratureitemdtos) {
        List<SalesLiteratureItem> domainlist=salesliteratureitemMapping.toDomain(salesliteratureitemdtos);
        for(SalesLiteratureItem domain:domainlist){
             domain.setSalesliteratureid(salesliterature_id);
        }
        salesliteratureitemService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesLiteratureItem-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-SalesLiteratureItem-Get')")
	@ApiOperation(value = "根据销售宣传资料获取DEFAULT", tags = {"销售附件" } ,notes = "根据销售宣传资料获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/salesliteratures/{salesliterature_id}/salesliteratureitems/fetchdefault")
	public ResponseEntity<List<SalesLiteratureItemDTO>> fetchSalesLiteratureItemDefaultBySalesLiterature(@PathVariable("salesliterature_id") String salesliterature_id,SalesLiteratureItemSearchContext context) {
        context.setN_salesliteratureid_eq(salesliterature_id);
        Page<SalesLiteratureItem> domains = salesliteratureitemService.searchDefault(context) ;
        List<SalesLiteratureItemDTO> list = salesliteratureitemMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-SalesLiteratureItem-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-SalesLiteratureItem-Get')")
	@ApiOperation(value = "根据销售宣传资料查询DEFAULT", tags = {"销售附件" } ,notes = "根据销售宣传资料查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/salesliteratures/{salesliterature_id}/salesliteratureitems/searchdefault")
	public ResponseEntity<Page<SalesLiteratureItemDTO>> searchSalesLiteratureItemDefaultBySalesLiterature(@PathVariable("salesliterature_id") String salesliterature_id, @RequestBody SalesLiteratureItemSearchContext context) {
        context.setN_salesliteratureid_eq(salesliterature_id);
        Page<SalesLiteratureItem> domains = salesliteratureitemService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(salesliteratureitemMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

