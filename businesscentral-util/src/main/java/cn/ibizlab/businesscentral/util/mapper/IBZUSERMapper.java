package cn.ibizlab.businesscentral.util.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.ibizlab.businesscentral.util.domain.IBZUSER;

public interface IBZUSERMapper extends BaseMapper<IBZUSER>{

}