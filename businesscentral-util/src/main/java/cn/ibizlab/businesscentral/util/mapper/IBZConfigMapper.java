package cn.ibizlab.businesscentral.util.mapper;

import cn.ibizlab.businesscentral.util.domain.IBZConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface IBZConfigMapper extends BaseMapper<IBZConfig>{

}